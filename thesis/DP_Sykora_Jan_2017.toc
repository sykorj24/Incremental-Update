\select@language {UKenglish}
\select@language {UKenglish}
\select@language {UKenglish}
\contentsline {subsection}{Citation of this thesis}{viii}{section*.1}
\contentsline {chapter}{Introduction}{1}{chapter*.5}
\contentsline {chapter}{\chapternumberline {1}Background}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Manta Flow}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Manta Flow architecture}{4}{subsection.1.1.1}
\contentsline {section}{\numberline {1.2}Graph database Titan}{4}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Titan architecture}{4}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Physical data model}{6}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Logical data model}{8}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}Data model in Manta Flow}{8}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Vertices}{8}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Edges}{11}{subsection.1.3.2}
\contentsline {section}{\numberline {1.4}Determination of a node's resource}{14}{section.1.4}
\contentsline {section}{\numberline {1.5}Graph structure}{15}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Main graph}{15}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Graph with source nodes}{16}{subsection.1.5.2}
\contentsline {subsection}{\numberline {1.5.3}Graph with revisions}{17}{subsection.1.5.3}
\contentsline {section}{\numberline {1.6}Graph creation}{17}{section.1.6}
\contentsline {section}{\numberline {1.7}Source code files}{18}{section.1.7}
\contentsline {subsection}{\numberline {1.7.1}Example}{18}{subsection.1.7.1}
\contentsline {section}{\numberline {1.8}Interpolation}{20}{section.1.8}
\contentsline {section}{\numberline {1.9}Indexing}{21}{section.1.9}
\contentsline {subsection}{\numberline {1.9.1}Standard index}{21}{subsection.1.9.1}
\contentsline {subsection}{\numberline {1.9.2}External index}{21}{subsection.1.9.2}
\contentsline {subsection}{\numberline {1.9.3}Vertex-centric index}{22}{subsection.1.9.3}
\contentsline {subsection}{\numberline {1.9.4}Indices in Manta Flow}{22}{subsection.1.9.4}
\contentsline {section}{\numberline {1.10}Version control}{24}{section.1.10}
\contentsline {subsection}{\numberline {1.10.1}Revisions}{24}{subsection.1.10.1}
\contentsline {subsection}{\numberline {1.10.2}Revision tracking in the graph}{24}{subsection.1.10.2}
\contentsline {subsection}{\numberline {1.10.3}Operations with revisions}{26}{subsection.1.10.3}
\contentsline {subsection}{\numberline {1.10.4}Merge}{28}{subsection.1.10.4}
\contentsline {section}{\numberline {1.11}Analysis of the current implementation}{29}{section.1.11}
\contentsline {section}{\numberline {1.12}Summary}{31}{section.1.12}
\contentsline {chapter}{\chapternumberline {2}Related work and inspiration}{33}{chapter.2}
\contentsline {section}{\numberline {2.1}Subversion}{33}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Working copy}{34}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Repository}{36}{subsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.2.1}Repository structure}{37}{subsubsection.2.1.2.1}
\contentsline {subsubsection}{\numberline {2.1.2.2}Bubble-up method}{38}{subsubsection.2.1.2.2}
\contentsline {section}{\numberline {2.2}Mercurial}{42}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Metadata structure and relationship}{44}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Revlog}{45}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Committing changes}{49}{subsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.3.1}First stage - top to bottom}{49}{subsubsection.2.2.3.1}
\contentsline {subsubsection}{\numberline {2.2.3.2}Second stage - bottom to top}{51}{subsubsection.2.2.3.2}
\contentsline {section}{\numberline {2.3}Incremental backups in databases}{52}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Differential incremental backup}{52}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Cumulative incremental backup}{53}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Combined incremental backup}{54}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Observation}{54}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Data changes}{54}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Repository update}{56}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Incremental backups}{56}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Summary}{57}{subsection.2.4.4}
\contentsline {chapter}{\chapternumberline {3}Analysis and Design}{59}{chapter.3}
\contentsline {section}{\numberline {3.1}Revision data representation}{59}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Closed end revision}{60}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Unclosed end revision}{62}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Two-level revision}{64}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Integer vs Decimal}{67}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Revision tree}{69}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}Full update vs incremental update}{72}{subsection.3.1.6}
\contentsline {section}{\numberline {3.2}Update}{73}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Full update}{74}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Incremental update}{76}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Input}{82}{section.3.3}
\contentsline {section}{\numberline {3.4}Customization}{84}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Inconsistency}{84}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Object creation and removal}{89}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Overflow}{90}{subsection.3.4.3}
\contentsline {section}{\numberline {3.5}Summary}{91}{section.3.5}
\contentsline {chapter}{\chapternumberline {4}Implementation}{95}{chapter.4}
\contentsline {section}{\numberline {4.1}Manta Flow server}{95}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Core}{96}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Connector}{96}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Merger}{101}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Manta Flow client}{103}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}NodeFlagTask}{103}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}IncrementalDeleteTask}{104}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}DeleteVerticesTask}{104}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Summary}{105}{section.4.3}
\contentsline {chapter}{\chapternumberline {5}Performance Testing}{107}{chapter.5}
\contentsline {section}{\numberline {5.1}Test cases}{107}{section.5.1}
\contentsline {section}{\numberline {5.2}Test data}{109}{section.5.2}
\contentsline {section}{\numberline {5.3}Measurement}{109}{section.5.3}
\contentsline {section}{\numberline {5.4}Summary}{113}{section.5.4}
\contentsline {chapter}{Conclusion}{115}{chapter*.64}
\contentsline {chapter}{Bibliography}{117}{section*.66}
\contentsline {appendix}{\chapternumberline {A}Acronyms}{121}{appendix.A}
\contentsline {appendix}{\chapternumberline {B}Contents of enclosed CD}{123}{appendix.B}
