package com.thinkaurelius.titan.diskstorage.lucene;

import java.io.Reader;

import org.apache.lucene.analysis.util.CharTokenizer;
import org.apache.lucene.util.Version;

/**
 * Tokenizer, který celý stream vezme jako jediný token.
 * @author tfechtner
 *
 */
public class MantaCharTokenizer extends CharTokenizer {

    /**
     * @param matchVersion Lucene version
     * @param input vstup k tokenizaci
     */
    public MantaCharTokenizer(Version matchVersion, Reader input) {
        super(matchVersion, input);
    }

    @Override
    protected boolean isTokenChar(int c) {
        return true;
    }
}
