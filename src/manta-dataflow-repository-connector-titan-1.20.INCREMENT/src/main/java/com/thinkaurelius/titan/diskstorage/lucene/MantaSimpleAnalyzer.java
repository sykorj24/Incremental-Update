package com.thinkaurelius.titan.diskstorage.lucene;

import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.util.Version;

/**
 * Analyzer, který vezme celý vstup jako jeden token a aplikuje {@link LowerCaseFilter}.
 * @author tfechtner
 *
 */
public class MantaSimpleAnalyzer extends Analyzer {

    private final Version matchVersion;

    /**
     * Creates a new {@link SimpleAnalyzer}.
     * @param matchVersion Lucene version
     */
    public MantaSimpleAnalyzer(Version matchVersion) {
        this.matchVersion = matchVersion;
    }

    @Override
    protected TokenStreamComponents createComponents(final String fieldName, final Reader reader) {
        Tokenizer tokenizer = new MantaCharTokenizer(matchVersion, reader);
        LowerCaseFilter filter = new LowerCaseFilter(matchVersion, tokenizer);
        return new TokenStreamComponents(tokenizer, filter);
    }
}
