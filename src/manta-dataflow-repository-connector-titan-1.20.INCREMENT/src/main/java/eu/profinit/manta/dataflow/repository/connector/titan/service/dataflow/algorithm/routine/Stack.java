package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.routine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Speciální větvený stack volání funkcí/procedur.
 * <br/>
 * Slouží pro inteligentní zařezávání rekurzivních volání a kompletní identifikaci návratových cest z rekurzivních volání.
 * Nejde o stak per-se, jde o komplikovanější strukturu.
 * <pre>
 * Stack stack = new Stack();
 * stack.push("a");
 * stack.push("b");
 * </pre>
 * Vytváří stack následujícího tvaru:
 * <pre>
 * ──root
 *   ├─b
 *   └─a
 * </pre>
 *
 *
 * @author Radomír Polách
 * @author Erik Kratochvíl
 */
public class Stack {

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(Stack.class);

    /**
     * Nadřazené volání.
     */
    private final Stack parent;

    /**
     * Všechna podřízená volání.
     */
    private final Map<String, Stack> children = new HashMap<String, Stack>();

    /**
     * Identifikátor definice funkce/procedury.
     * <br/>
     * Např. pro
     * <pre>
     * var x = sin(π);
     * function <u>sin</u>(ω) { return ...; }
     * </pre>
     * je to ID vrcholu, který reprezentuje definici funkce <u>sin</u>.
     */
    private final Object calledFunction;

    /**
     * Identifikátor volání funkce/procedury.
     * Ukládá se na hranách datových toků.
     * <br/>
     * Např. pro
     * <pre>
     * var x = <u>sin</u>(π);
     * function sin(ω) { return ...; }
     * </pre>
     * je to ID, které reprezentuje volání funkce <u>sin</u>.
     * <pre>
     *          <u>sin()</u>     sin
     *           │         │
     *   π ────> ω ──────> ω ────> ..
     *               23
     * </pre>
     */
    private final String callNumber;



    /**
     * Vytvoří nový prázdný stack.
     */
    public Stack() {
        this.parent = this; // TODO Nestandardni, proc neni pouzit tradicni: this.parent = null;
        this.callNumber = null;
        this.calledFunction = null;
    }

    /**
     * Vytvoří nový prvek na stacku.
     *
     * @param parent nadřazený prvek
     * @param callNumber identifikátor volání
     * @param calledFunction identifikátor definice funkce
     */
    private Stack(final Stack parent, final String callNumber, final Object calledFunction) {
        this.parent = parent;
        this.callNumber = callNumber;
        this.calledFunction = calledFunction;
        this.parent.children.put(callNumber, this);
    }

    /**
     * @return identifikátor volání
     */
    public String getCallNumber() {
        return this.callNumber;
    }

    /**
     * @return identifikátor definice funkce
     */
    public Object getCalledFunction() {
        return this.calledFunction;
    }

    /**
     * Dopočítaná property - odpovídá identifikátoru definice funkce nadřazeného volání.
     * @return identifikátor definice funkce, ve které je toto volání uskutečněno
     */
    public Object getInsideFunction() {
        return this.parent.calledFunction;
    }

    /**
     * Vlož pod tento prvek stacku nový prvek.
     * <br/>
     * Říká, že volané funkci reprezentované tímto prvkem stacku je volání funkce <code>calledFunction</code> s daným identifikátorem <code>callNumber</code>.
     *
     * @param callNumber identifikátor volání
     * @param calledFunction identifikátor definice funkce
     * @return nový prvek na stacku
     */
    public Stack push(final String callNumber, final Object calledFunction) {
        // pokud jsme tu uz nekdy byli, vrat se na zacatek rekurze
        final Stack prevIteration = this.parent.searchForRecursion(this.callNumber);
        if (prevIteration != null) {
            return prevIteration.push(callNumber, calledFunction);
        }

        Stack node = this.children.get(callNumber);
        if (node == null) {
            node = new Stack(this, callNumber, calledFunction);
        }

        return node;
    }

    /**
     * Zjisti, kam se z tohoto prvku stacku dá vrátit.
     * Kvůli rekurzi toto může vést hned do několika "předchozích" stavů.
     * <br/>
     * Poznámka. Prvek ze stacku nemizí, zůstává v něm navždy. Nejde tedy o "klasickou metodu pop() na tradičním stacku", logika je sofistikovanější.
     *
     * @return kolekce možných předchozích stavů (nikdy null, nikdy prázdná)
     */
    public Collection<Stack> pop() {
        final List<Stack> result = new ArrayList<Stack>();
        result.add(this.parent);
        result.addAll(this.findRecursionExits(this.getCallNumber()));
        return result;
    }

    /**
     * Vyhledá možné cesty ven z rekurze v potomcích.
     *
     * @param callNumber identifikátor hledaného volání
     * @return nalezené možné cesty ven z volání (nikdy null, může být prázdná kolekce)
     */
    private Collection<Stack> findRecursionExits(final String callNumber) {
        if (CollectionUtils.isEmpty(this.children.values())) {
            return Collections.emptyList();
        } else {
            final List<Stack> result = new ArrayList<Stack>();
            for (final Entry<String, Stack> child : this.children.entrySet()) {
                if (child.getKey().equals(callNumber)) {
                    // potomek je hledane volani, vrat jeho predka - sebe
                    result.add(this);
                } else {
                    // jine volani, prohledej rekurzivne
                    result.addAll(child.getValue().findRecursionExits(callNumber));
                }
            }
            return result;
        }
    }

    /**
     * Vyhledá rekurzivní volání.
     * V případě nalezení rekurzivního volání vrátí předchozí "iteraci" volání místo zvyšování stacku.
     *
     * @param callNumber identifikátor hledaného volání
     * @return stack pro dřívější návštěvu stejného call ID
     */
    private Stack searchForRecursion(final String callNumber) {
        Stack stack = this;
        while (stack != stack.parent) {
            if (stack.match(callNumber)) {
                return stack;
            }
            stack = stack.parent;
        }
        return null;
    }

    /**
     * Kontrola shodnosti identifikátoru volání.
     * @param callNumber identifikátor volání k porovnání
     * @return true, pokud se identifikátor volání k porovnání shoduje s tímto identifikátorem volání
     */
    private boolean match(final String callNumber) {
        return callNumber.equals(this.callNumber);
    }

    @Override
    public String toString() {
        if (this == this.parent) {
            return this.callNumber; // pro root je vždy NULL...
        }
        return this.callNumber + "->" + this.parent.toString();
    }

    /**
     * Vrací textovou reprezentaci kompletního stacku volání.
     * Aktuální pozice, kde se zrovna ve stacku nacházíme, je zvýrazněna značkou <b>&lt;&lt;&lt;</b>.
     * Každý element obsahuje jednak identifikátor volání <b>x</b> a pak identifikátor definice funkce <b>f</b>: <code>x [f]</code>.
     * <br/>
     * Příklad:
     * <pre>
     * ──root
     *   └─1 [f]
     *     ├─4 [k]
     *     │ └─3 [h]
     *     │   └─3 [h]
     *     ├─2 [g] &lt;&lt;&lt;
     *     │ └─1 [f]
     *     └─3 [h]
     *       └─1 [f]
     * </pre>
     *
     * @return textová reprezentace kompletního stacku volání
     */
    public String getCompleteStackAsString() {
        Stack root = this;
        while (root.parent != root) {
            root = root.parent;
        }
        final StringBuilder result = new StringBuilder();
        constructCompleteStackAsString(root, result, "", null, this);
        return result.toString();
    }

    /**
     * Rekurzivní vytvoření textové reprezentace kompletního stacku volání.
     *
     * @param stack aktuální prvek na stacku
     * @param result aktuální textová reprezentace stacku volání
     * @param tab znaky použité pro odsazení
     * @param isLast jde o poslední prvek v seznamu podřízených volání?
     * @param highlight jde o prvek, ve kterém se nacházíme (zvýrazněný prvek)?
     */
    private static void constructCompleteStackAsString(final Stack stack, final StringBuilder result, final String tab, final Boolean isLast,
            final Stack highlight) {
        final String entry = isLast == null ? "──" : isLast ? "└─" : "├─";
        final String subEntry = isLast == null ? "  " : isLast ? "  " : "│ ";
        final String id = stack.parent == stack ? "root" : stack.callNumber;
        result.append(tab);
        result.append(entry);
        result.append(id);
        if (stack.calledFunction != null) {
            result.append(" [");
            result.append(stack.calledFunction);
            result.append("]");
        }
        if (stack == highlight) {
            result.append(" <<<");
        }
        result.append("\n");
        int i = 0;
        for (final Stack child : stack.children.values()) {
            i++;
            constructCompleteStackAsString(child, result, tab + subEntry, stack.children.size() == i, highlight);
        }
    }

    /**
     * Zalogování obsahu stacku.
     * Pro případy, že stack je opravdu obrovský, vypisuje se rovnou do logu.
     *
     *@param stack aktuální prvek na stacku
     * @param tab znaky použité pro odsazení
     * @param isLast jde o poslední prvek v seznamu podřízených volání?
     */
    public static void logStack(final Stack stack, final String tab, final Boolean isLast) {
        final String entry = isLast == null ? "--" : "+-";
        final String subEntry = isLast == null ? "  " : isLast ? "  " : "| ";
        final String id = stack.parent == stack ? "root" : stack.callNumber;
        LOGGER.error("{}{}{} [{}]", tab, entry, id, stack.calledFunction);
        int i = 0;
        for (final Stack child : stack.children.values()) {
            i++;
            logStack(child, tab + subEntry, stack.children.size() == i);
        }
    }

    /**
     * Dohledá ten úplně první nadřazený prvek, ve kterém poprvé došlo k volání stejné funkce.
     * <br/>
     * Používá se pro detekci rekurzivního volání funkce <b>f</b>,
     * pokud totiž <code>stack.findFirstCall() != stack</code>,
     * pak jsme rekurzivně zanořeni uvnitř funkce <b>f</b>.
     * <br/>
     * Příklad:
     * <pre>
     * ──root
     *   └─0 [f]
     *     ├─4 [k]
     *     │ └─3 [h]
     *     │   └─3 [h]
     *     ├─2 [g]
     *     │ └─1 [f]  &lt;&lt;&lt;
     *     └─3 [h]
     *       └─1 [f]
     * </pre>
     * Pokud se nacházíme ve volání 1 funkce f, které je zvýrazněno &lt;&lt;&lt;,
     * pak to úplně první nadřazené volání funkce f je hned to pod kořenem stacku,
     * tj. tohle: <code>└─0 [f]</code>
     *
     * @return nadřazený prvek stacku, kde došlo vůbec poprvé k volání stejné funkce; pokud žádné nadřazené volání neexistuje, vrací se toto volání
     */
    public Stack findFirstCall() {
        Stack firstCall = this;
        for (Stack stack = this; stack != null && stack.parent != stack; stack = stack.parent) {
            if (stack.calledFunction != null && stack.calledFunction.equals(this.calledFunction)) {
                firstCall = stack;
            }
        }
        return firstCall;
    }

    public String getCalledNumberPath() {
        if (this.parent == this) {
            return "root";
        }
        return this.calledFunction + "->" + this.parent.getCalledNumberPath();
    }

    /**
     * @return nadřazený stack
     */
    public Stack getParent() {
        return this.parent;
    }

    /**
     * @return podřízené stacky
     */
    public Collection<Stack> getChildren() {
        return Collections.unmodifiableCollection(this.children.values());
    }

}