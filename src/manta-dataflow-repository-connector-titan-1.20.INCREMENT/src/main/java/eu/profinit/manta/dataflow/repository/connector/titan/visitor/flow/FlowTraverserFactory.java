package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow;

/**
 * Továrna pro tvorbu {@link FlowTraverser}.
 * @author tfechtner
 *
 */
public interface FlowTraverserFactory {
    
    /**
     * Vytvoří novou instanci {@link FlowTraverser}.
     * @return nová instance
     */
    FlowTraverser createFlowTraverser();
}
