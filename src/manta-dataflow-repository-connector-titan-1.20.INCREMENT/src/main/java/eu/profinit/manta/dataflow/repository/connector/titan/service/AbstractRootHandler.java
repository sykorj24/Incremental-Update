package eu.profinit.manta.dataflow.repository.connector.titan.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.thinkaurelius.titan.core.TitanVertex;
import com.thinkaurelius.titan.graphdb.internal.ElementLifeCycle;
import com.thinkaurelius.titan.graphdb.vertices.StandardVertex;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

/**
 * Společný předek pro root handlery.
 * @author pholecek
 *
 */
public abstract class AbstractRootHandler {
    /** Název modulu, který se používá pro práci s root uzlem. */
    public static final String MODULE_NAME = "mr_basic";
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRootHandler.class);

    /**
     * Zajistí, že v databázi je daný root. <br>
     * Pokud ještě neexistoval, tak ho vytvoří.
     * @param transaction R/W transakce pro případné vytvoření
     * @return root v databázi, nikdy null
     */
    public Vertex ensureRootExistance(TitanTransaction transaction) {
        Vertex root = tryGetRoot(transaction);

        if (root == null) {
            // neexistuje -> vytvořit a uložit si jeho id
            root = transaction.addVertex();
            root.setProperty(NodeProperty.VERTEX_TYPE.t(), getVertexType());
            ((TitanVertex) root).addProperty(getNodeProperty().t(), true);
            Object revisionRootId = root.getId();

            LOGGER.info("Creating " + getNodeProperty().t() + " root with id " + revisionRootId + ".");
        }
        return root;
    }

    /**
     * Získá Vertex root databáze více viz {@link AbstractRootHandler#getRevisionRoot(TitanTransaction)}.
     * @param databaseHolder service pro získání transakce
     * @return id rootu databáze
     */
    public Long getRoot(DatabaseHolder databaseHolder) {
        return (Long) databaseHolder.runInTransaction(TransactionLevel.READ, new TransactionCallback<Vertex>() {
            @Override
            public Vertex callMe(TitanTransaction transaction) {
                return getRoot(transaction);
            }

            @Override
            public String getModuleName() {
                return MODULE_NAME;
            }
        }).getId();
    }

    /**
     * Vrátí root databáze.
     * Přičemž si ho pro optimalizaci vnitřně udržuje.
     * Pokud ještě uzel neexistuje, vyhodí výjimku.
     * @param transaction transakce do databáze
     * @return root databáze
     */
    public Vertex getRoot(TitanTransaction transaction) {
        Vertex root = tryGetRoot(transaction);
        if (root != null) {
            return root;
        } else {
            throw new IllegalStateException("Root does not exist, incorrect db.");
        }
    }

    /**
     * Pokusí se načíst root.
     * @param transaction čtecí transakce použitá pro získání rootu
     * @return root databáze nebo null, pokud neexistuje
     */
    public Vertex tryGetRoot(TitanTransaction transaction) {
        TitanVertex rootNode = transaction.getVertex(getNodeProperty().t(), true);
        if (rootNode == null || ElementLifeCycle.isRemoved(((StandardVertex) rootNode).getLifeCycle())) {
            return null;
        } else {
            return rootNode;
        }
    }

    /**
     * @return node property definující root uzel, který tento handler drží
     */
    public abstract NodeProperty getNodeProperty();

    /**
     * @return typ root uzlu, který tento handler drží
     */
    public abstract VertexType getVertexType();

}
