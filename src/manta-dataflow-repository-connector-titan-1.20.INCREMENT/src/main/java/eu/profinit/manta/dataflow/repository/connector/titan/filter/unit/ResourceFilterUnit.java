package eu.profinit.manta.dataflow.repository.connector.titan.filter.unit;

import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Jednotkový filtr filtrující podle resource.
 * @author tfechtner
 *
 */
public class ResourceFilterUnit implements HorizontalFilterUnit {
    /** Množina resource, které jsou vyfiltrované. */
    private final Set<String> disabledResources;

    /**
     * @param disabledResources Množina resource, které jsou vyfiltrované
     */
    @JsonCreator
    public ResourceFilterUnit(@JsonProperty(value = "disabledResources") Set<String> disabledResources) {
        this.disabledResources = new HashSet<>(disabledResources);
    }

    @Override
    public boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval) {
        Vertex resource = GraphOperation.getResource(vertex);
        if (resource == null) {
            return false;
        }

        String resourceName = GraphOperation.getName(resource);
        return disabledResources.contains(resourceName);
    }

}
