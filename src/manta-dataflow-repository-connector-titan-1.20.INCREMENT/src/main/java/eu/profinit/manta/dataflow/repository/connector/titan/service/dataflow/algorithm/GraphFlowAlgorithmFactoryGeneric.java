package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.GraphFlowFilter;

/**
 * Generická továrna na flow algoritmy implementující {@link GraphFlowAlgorithm}.
 * Aby mohla továrna instance vytvářet, musí daná třída mít public default konstruktor.
 * @author tfechtner
 *
 * @param <T> typ algoritmu, který se má konstruovat
 */
public class GraphFlowAlgorithmFactoryGeneric<T extends GraphFlowAlgorithm> implements GraphFlowAlgorithmFactory {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(GraphFlowAlgorithmFactoryGeneric.class);

    /** Typ třídy k vytváření. */
    private final Class<T> algorithmClass;
    /** Seznam filtrů, které se předají nově vytvořenému algoritmu. */
    private final List<GraphFlowFilter> filters;
    /** Limit na výpočet algoritmu, specifické podle implementace. */
    private final Long limit;

    /**
     * @param algorithmClass Typ třídy k vytváření.
     * @param filters Seznam filtrů, které se předají nově vytvořenému algoritmu.
     * @param limit limit algoritmu, specifické podle implementace
     */
    public GraphFlowAlgorithmFactoryGeneric(final Class<T> algorithmClass, final List<GraphFlowFilter> filters,
            final Long limit) {
        if (algorithmClass == null) {
            throw new IllegalArgumentException("Algorithm class must not be null.");
        }
        this.algorithmClass = algorithmClass;
        this.filters = filters;
        this.limit = limit;
    }

    /**
     * @param algorithmClass Typ třídy k vytváření.
     * @param filters Seznam filtrů, které se předají nově vytvořenému algoritmu.
     */
    public GraphFlowAlgorithmFactoryGeneric(Class<T> algorithmClass, List<GraphFlowFilter> filters) {
        this(algorithmClass, filters, null);
    }

    @Override
    public T createGraphFlowAlgorithm() {
        return createGraphFlowAlgorithm(null);
    }
    
    @Override
    public T createGraphFlowAlgorithm(List<GraphFlowFilter> additionalFilters) {
        try {
            T algorithm = algorithmClass.newInstance();
            
            List<GraphFlowFilter> mergedFilters = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(filters)) {
                mergedFilters.addAll(filters);
            }
            if (CollectionUtils.isNotEmpty(additionalFilters)) {
                mergedFilters.addAll(additionalFilters);
            }
            algorithm.setFilters(mergedFilters);
            
            if (limit != null) {
                algorithm.setLimit(limit);
            }
            
            return algorithm;
        } catch (InstantiationException e) {
            LOGGER.error("Class " + algorithmClass + " cannot be instantiated.", e);
            return null;
        } catch (IllegalAccessException e) {
            LOGGER.error("The constructor for the class " + algorithmClass + " is not accessible.", e);
            return null;
        }
    }

    @Override
    public T createGraphFlowAlgorithm(boolean isFilterEdges) {
        return createGraphFlowAlgorithm(isFilterEdges, null);
    }
    
    @Override
    public T createGraphFlowAlgorithm(boolean isFilterEdges, List<GraphFlowFilter> additionalFilters) {
        T algorithm = createGraphFlowAlgorithm(additionalFilters);
        if (algorithm != null) {
            algorithm.setFilterEdgesEnabled(isFilterEdges);
        }

        return algorithm;
    }

    @Override
    public String toString() {
        return "GraphFlowAlgorithmFactoryGeneric [" + algorithmClass.getCanonicalName() + "]";
    }
}
