package eu.profinit.manta.dataflow.repository.connector.titan.filter.unit;

import java.util.Collections;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Jednotkový filtr filtrující podle regulárního výrazu na název uzlu nebo kteréhokoliv jeho předka.
 * @author tfechtner
 *
 */
public class NameFilterUnit implements HorizontalFilterUnit {
    /** Regexp pattern na porovnání jména. */
    private final Pattern pattern;
    /** Množina názvů resourců, na kterých se definovaný pattern testuje. */
    private final Set<String> impactedResources;
    /** Množina typů, na kterých se definovaný pattern testuje. */
    private final Set<String> impactedTypes;

    /**
     * @param impactedResources Množina názvů resourců, na kterých se definovaný pattern testuje
     * @param impactedTypes Množina typů, na kterých se definovaný pattern testuje
     * @param regexp Regexp pattern na porovnání jména
     */
    @JsonCreator
    public NameFilterUnit(@JsonProperty(value = "impactedResources") Set<String> impactedResources,
            @JsonProperty(value = "impactedTypes") Set<String> impactedTypes,
            @JsonProperty(value = "regexp") String regexp) {
        this.pattern = Pattern.compile(regexp);

        if (impactedTypes != null) {
            this.impactedTypes = impactedTypes;
        } else {
            this.impactedTypes = Collections.emptySet();
        }

        if (impactedResources != null) {
            this.impactedResources = impactedResources;
        } else {
            this.impactedResources = Collections.emptySet();
        }
    }

    @Override
    public boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval) {
        while (vertex != null) {
            String rName = GraphOperation.getName(GraphOperation.getResource(vertex));
            if (impactedResources.isEmpty() || impactedResources.contains(rName)) {
                String vType = GraphOperation.getType(vertex);
                if (impactedTypes.isEmpty() || impactedTypes.contains(vType)) {
                    String vName = GraphOperation.getName(vertex);
                    Matcher matcher = pattern.matcher(vName);
                    if (matcher.matches()) {
                        return true;
                    }
                }
            }
            vertex = GraphOperation.getParent(vertex);
        }

        return false;
    }

}
