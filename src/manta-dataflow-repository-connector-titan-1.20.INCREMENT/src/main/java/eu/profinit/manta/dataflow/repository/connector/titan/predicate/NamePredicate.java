package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;

/**
 * Podmínku splňují takové vrcholy, jejichž jméno je přednastavené množiny.
 * Pokud je množina prázdná, povoláí všechny.
 * @author tfechtner
 *
 */
public class NamePredicate implements VertexPredicate {

    /** Množina povolených jmen. */
    private Set<String> nameSet;

    /**
     * Default konstruktor, pro správné fungování je ještě třeba nastvit field {@link #nameSet}.
     */
    public NamePredicate() {
        super();
    }

    /**
     * @param nameSet množina jmen splňující podmínku
     */
    public NamePredicate(Collection<String> nameSet) {
        super();
        this.nameSet = new HashSet<>(nameSet);
    }

    @Override
    public boolean evaluate(Vertex vertex, RevisionInterval revisionInterval) {
        if (nameSet == null) {
            throw new IllegalStateException("The type set must not be null.");
        }

        return nameSet.isEmpty() || nameSet.contains(GraphOperation.getName(vertex));
    }

    /**
     * @param nameSet Množina povolených jmen.
     */
    public void setNameSet(Set<String> nameSet) {
        this.nameSet = nameSet;
    }

    /**
     * Přidá množinu jmen k již stávající.
     * @param nameSetToAdd množina jmen k přidání
     */
    public void addNames(Collection<String> nameSetToAdd) {
        if (nameSet != null) {
            nameSet.addAll(nameSetToAdd);
        } else {
            nameSet = new HashSet<>(nameSetToAdd);
        }
    }
}
