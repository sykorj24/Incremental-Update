package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilter;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterType;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.platform.automation.AbstractNamedBean;

/**
 * Třída udržující stav zobrazení daného flow, sem patří referenční view, seznam zneaktivněných resource atd.
 */
public class FlowState {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(FlowState.class);

    /**
     * Stav uzlu v daném view.
     * @author tfechtner
     *
     */
    public enum NodeStatus {
        /** Uzel je v tomto view neznámý = nezobrazovaný. */
        UNKNOWN,
        /** Uzel je ještě nenavštívený žádným dotazem.*/
        UNVISITED,
        /** V uzlu skončilo follow, je tedy již zobrazovaný a je z něho možno provést follow. */
        VISITED,
        /** Uzel je již plně zobrazen, jsou zobrazeny všechny jeho hrany a již z něho nemělo jít provést follow.*/
        FINISHED;
    }

    /** Mapa stavu uzlů v daném view, kde klíčem je id uzlu. */
    private final Map<Long, NodeStatus> nodes = new HashMap<Long, NodeStatus>();
    /** True, jestliže se zobrazují filter hrany. */
    private boolean isFilterEdgesShown = true;
    /** Interval  revizí, pro který daný flow stav platí. */
    private final RevisionInterval revisionInterval;
    /** Unikátní id daného flow stavu. */
    private final String guid;
    /** Množina aktivních filtrů. */
    private Set<String> activeFilters = new HashSet<>();
    /** Množina aktivních filterů, které nejsou resource. */
    private Set<String> activeCustomFilters = new HashSet<>();
    /** Providerp ro horizontální filtry. */
    private final HorizontalFilterProvider filterProvider;

    /**
     * Standardní kostruktor.
     * @param revisionInterval interval revizí pro daný stav flow
     */
    public FlowState(RevisionInterval revisionInterval, HorizontalFilterProvider filterProvider) {
        this.revisionInterval = revisionInterval;
        this.guid = AbstractNamedBean.IdGenerator.generateId();
        this.filterProvider = filterProvider;
    }

    /**
     * Copy constructor, který provede deep-copy.
     * @param other kopírovaný stav
     */
    public FlowState(FlowState other) {
        this.nodes.clear();
        for (Long id : other.getReferenceNodeIds()) {
            this.nodes.put(id, other.getNodeStatus(id));
        }

        this.activeFilters = new HashSet<String>(other.getActiveFilters());
        this.activeCustomFilters = new HashSet<String>(other.activeCustomFilters);
        this.filterProvider = other.filterProvider;
        this.isFilterEdgesShown = other.isFilterEdgesShown();
        this.revisionInterval = other.revisionInterval;
        this.guid = other.guid;
    }

    /**
     * @return Unikátní id daného flow stavu. 
     */
    public String getGuid() {
        return guid;
    }

    /**
     * @return True, jestliže se zobrazují filter hrany.
     */
    public boolean isFilterEdgesShown() {
        return isFilterEdgesShown;
    }

    /**
     * @param isFilterEdgesShownParam True, jestliže se zobrazují filter hrany.
     */
    public void setFilterEdgesShown(boolean isFilterEdgesShownParam) {
        this.isFilterEdgesShown = isFilterEdgesShownParam;
    }

    /**
     * Zjistí id všech uzlů v daném referenčním view.
     * @return množina id uzlů
     */
    public Set<Long> getReferenceNodeIds() {
        return nodes.keySet();
    }

    /**
     * @return získá množinu aktivních filtrů, nikdy null
     */
    public Set<String> getActiveFilters() {
        return Collections.unmodifiableSet(activeFilters);
    }

    /**
     * @param activeFilters nová množina aktinívch filtrů
     */
    public void setActiveFilters(Set<String> activeFilters) {
        if (activeFilters != null) {
            this.activeFilters = new HashSet<>(activeFilters);
            this.activeCustomFilters = new HashSet<>();
            for (String id : activeFilters) {
                HorizontalFilter filter = filterProvider.getFilter(id);
                if (filter != null && filter.getType() == HorizontalFilterType.CUSTOM) {
                    activeCustomFilters.add(id);
                }
            }
        } else {
            this.activeFilters = Collections.emptySet();
            this.activeCustomFilters = Collections.emptySet();
        }
    }

    /**
     * @return Interval revizí, pro který daný flow stav platí. 
     */
    public RevisionInterval getRevisionInterval() {
        return revisionInterval;
    }

    /**
     * Ověří jestli je daný vrchol vyfiltrován.
     * @param vertex zkoumaný vrchol
     * @return True, jestliže je vrchol vyfiltrován
     */
    public boolean isVertexFiltered(Vertex vertex) {
        return filterProvider.isFiltered(vertex, getRevisionInterval(), getActiveFilters());
    }

    /**
     * Ověří jestli je daný vrchol vyfiltrován custom filtry.
     * @param vertex zkoumaný vrchol
     * @return True, jestliže je vrchol vyfiltrován
     */
    public boolean isVertexCustomFiltered(Vertex vertex) {
        return filterProvider.isFiltered(vertex, getRevisionInterval(), activeCustomFilters);
    }

    // ----------------------------------------------------------------
    // Metody pro práci s referenčním view.
    // ----------------------------------------------------------------

    /**
     * Přidá uzel do view. Startovní stav je {@link NodeStatus#UNVISITED}.
     * @param nodeId id uzlu, musí být Long
     */
    public void addNode(Long nodeId) {
        synchronized (nodes) {
            nodes.put(nodeId, NodeStatus.UNVISITED);
        }
    }

    /**
     * Přidá uzel do view. Startovní stav je {@link NodeStatus#UNVISITED}.
     * @param node uzel, jehož id se má vložit
     */
    public void addNode(Vertex node) {
        addNode((Long) node.getId());
    }

    /**
     * Označí uzel jako dokončený ({@link NodeStatus#FINISHED}).
     * @param nodeId id uzlu
     */
    private void finishNode(Long nodeId) {
        synchronized (nodes) {
            nodes.put(nodeId, NodeStatus.FINISHED);
        }
    }

    /**
     * Označí uzel jako dokončený ({@link NodeStatus#FINISHED}).<br>
     * Případně rekurzivně dokončí i všechny jeho sousedy.
     * @param node uzel, jenž se má dokončit
     */
    public void finishNode(Vertex node) {
        finishNode((Long) node.getId());
        tryToFinishNeighbors(node);
    }

    private void tryToFinishNeighbors(Vertex node) {
        // zkouknout sousedy
        List<Vertex> vertices = GraphFlow.getAdjacentVerticesInFlow(node, getRevisionInterval(), isFilterEdgesShown());
        for (Vertex actualVertex : vertices) {
            NodeStatus actualStatus = getNodeStatus(actualVertex);
            // zajímají nás jen visitd uzly
            if (actualStatus == NodeStatus.VISITED) {
                tryToFinishNode(actualVertex);
            }
        }
    }

    /**
     * Zjistí jestli se nemá daný uzel dokončit, tedy jestliže nemá už všechny sousedy VISISTED a FINISHED.
     * @param vertex ověřovaný uzel
     */
    public void tryToFinishNode(Vertex vertex) {
        // dokončit uzel kvuli neexistenci sousedů platí jen pro listy, vyšší uzly musí mít finished všechny potomky
        if (GraphOperation.isList(vertex)) {
            // najít všechny přilehlé uzly, co jsou ještě nenavštívené
            List<Vertex> vertices = GraphFlow.getAdjacentVerticesInFlow(vertex, getRevisionInterval(),
                    isFilterEdgesShown());
            for (Vertex actualVertex : vertices) {
                NodeStatus nodeStatus = getNodeStatus(actualVertex);
                if (nodeStatus == NodeStatus.UNVISITED) {
                    return;
                }
            }

            // žádný takový není -> tento uzel je již finished, tzv deduced finished
            finishNode(vertex);
        }
    }

    /**
     * Označí uzel jako navštívěný ({@link NodeStatus#VISITED}). <br>
     * K změně dojde právě tehdy když je současný stav {@link NodeStatus#UNVISITED}.
     * @param nodeId id uzlu
     * @return true, pokud došlo ke změně
     */
    private boolean visitNode(Long nodeId) {
        synchronized (nodes) {
            NodeStatus actualStatus = nodes.get(nodeId);
            if (actualStatus == null) {
                LOGGER.warn("Trying to visit unknown node " + nodeId);
            } else {
                if (actualStatus == NodeStatus.UNVISITED) {
                    nodes.put(nodeId, NodeStatus.VISITED);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Označí uzel jako navštívěný ({@link NodeStatus#VISITED}). <br>
     * Případně rekurzivně dokončí i všechny jeho sousedy. <br>
     * Více viz {@link #visitNode(Long)}
     * @param node uzel, který se má nastavit jako navštívený
     * @return true, pokud došlo ke změně
     */
    public boolean visitNode(Vertex node) {
        boolean isNewlyVisited = visitNode((Long) node.getId());
        tryToFinishNode(node);

        if (isNewlyVisited) {
            tryToFinishNeighbors(node);
        }
        return isNewlyVisited;
    }

    /**
     * Získá status uzlu v daném view.
     * @param nodeId id uzlu
     * @return aktuální status uzlu, nikdy null
     */
    public NodeStatus getNodeStatus(Long nodeId) {
        NodeStatus nodeStatus = nodes.get(nodeId);
        if (nodeStatus != null) {
            return nodeStatus;
        } else {
            return NodeStatus.UNKNOWN;
        }
    }

    /**
     * Získá status uzlu v daném view.
     * @param vertex uzel, jehož status se zjišťuje
     * @return aktuální status uzlu, nikdy null
     */
    public NodeStatus getNodeStatus(Vertex vertex) {
        return getNodeStatus((Long) vertex.getId());
    }
}
