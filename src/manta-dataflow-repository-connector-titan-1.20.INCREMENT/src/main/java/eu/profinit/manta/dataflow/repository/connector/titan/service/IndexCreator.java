package eu.profinit.manta.dataflow.repository.connector.titan.service;

import com.thinkaurelius.titan.core.Mapping;
import com.thinkaurelius.titan.core.Parameter;
import com.thinkaurelius.titan.core.TitanKey;
import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseConfiguration;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;

/**
 * Třída pro vytváření indexů.
 * @author tfechtner
 *
 */
public final class IndexCreator implements TransactionCallback<Object> {

    /**
     * Konfigurace databáze ovlivňující vytvořené indexy.
     */
    private final DatabaseConfiguration databaseConfiguration;

    /**
     * @param databaseConfiguration konfigurace databáze ovlivňující vytvořené indexy
     */
    public IndexCreator(DatabaseConfiguration databaseConfiguration) {
        super();
        this.databaseConfiguration = databaseConfiguration;
    }

    @Override
    public synchronized Object callMe(TitanTransaction indexTransaction) {
        // fulltext search na název uzlu
        if (databaseConfiguration.hasNodeNameIndex()
                && !indexTransaction.getIndexedKeys(Vertex.class).contains(NodeProperty.NODE_NAME.t())) {
            indexTransaction.makeKey(NodeProperty.NODE_NAME.t()).dataType(String.class)
                    .indexed("search", Vertex.class, Parameter.of(Mapping.MAPPING_PREFIX, Mapping.TEXT)).make();
            GraphCreation.LOGGER.info("Create index for " + NodeProperty.NODE_NAME.t());
        }

        // root indexy
        createRootIndex(indexTransaction, NodeProperty.SUPER_ROOT);
        createRootIndex(indexTransaction, NodeProperty.REVISION_ROOT);
        createRootIndex(indexTransaction, NodeProperty.SOURCE_ROOT);

        // klíče pro interval transakce -> ještě je třeba založit indexy
        TitanKey tranStartKey = createTransactionKey(indexTransaction, EdgeProperty.TRAN_START);
        TitanKey tranEndKey = createTransactionKey(indexTransaction, EdgeProperty.TRAN_END);

        // indexy na hranách
        createFlowEdgeIndex(indexTransaction, tranStartKey, tranEndKey);
        createHierarchyEdgeIndex(indexTransaction, tranStartKey, tranEndKey);
        createSourceCodeEdgeIndex(indexTransaction, tranStartKey, tranEndKey);

        return null;
    }

    /**
     * Vytvoří index pro root vertex.
     * @param transaction transakce pro přístup k databázi
     * @param rootNodeProperty node property pro root, kterému se vytváří index  
     */
    private void createRootIndex(TitanTransaction transaction, NodeProperty rootNodeProperty) {
        if (!transaction.getIndexedKeys(Vertex.class).contains(rootNodeProperty.t())) {
            try {
                transaction.makeKey(rootNodeProperty.t()).dataType(Boolean.class).indexed(Vertex.class).unique().make();
                GraphCreation.LOGGER.info("Create index for " + rootNodeProperty.t());
            } catch (UnsupportedOperationException e) {
                GraphCreation.LOGGER.error("Error during creating index.", e);
            }
        }
    }

    /**
     * Vytvoří jeden intervalový konec pro definic transakce.
     * @param transaction transakce pro přístup k databázi
     * @param tranIntervalType název tohoto konce intervalu 
     * @return příslušný klíč
     */
    private TitanKey createTransactionKey(TitanTransaction transaction, EdgeProperty tranIntervalType) {
        if (!transaction.getIndexedKeys(Edge.class).contains(tranIntervalType.t())) {
            try {
                GraphCreation.LOGGER.info("Create index for " + tranIntervalType.t());
                return transaction.makeKey(tranIntervalType.t()).dataType(Double.class).indexed("search", Edge.class)
                        .make();
            } catch (UnsupportedOperationException e) {
                GraphCreation.LOGGER.error("Error during creating index.", e);
                return null;
            }
        } else {
            return transaction.getPropertyKey(tranIntervalType.t());
        }
    }

    /**
     * Vytvoří index na id cílového uzlu pro flow hrany včetně intervalu platnosti transakcí.
     * @param transaction transakce pro přístup k databázi
     * @param tranStartKey klíč pro property označující start transakce
     * @param tranEndKey klíč pro property označující konec transakce
     */
    private void createFlowEdgeIndex(TitanTransaction transaction, TitanKey tranStartKey, TitanKey tranEndKey) {
        if (!transaction.getIndexedKeys(Edge.class).contains(EdgeProperty.TARGET_ID.t())) {
            try {
                TitanKey keyTargetId = transaction.makeKey(EdgeProperty.TARGET_ID.t()).dataType(String.class)
                        .indexed(Edge.class).make();
                GraphCreation.LOGGER.info("Create key for " + EdgeProperty.TARGET_ID.t());

                createLabelWithTran(transaction, EdgeLabel.DIRECT, keyTargetId, tranStartKey, tranEndKey);
                createLabelWithTran(transaction, EdgeLabel.FILTER, keyTargetId, tranStartKey, tranEndKey);
            } catch (UnsupportedOperationException e) {
                GraphCreation.LOGGER.error("Error during creating index.", e);
            }
        }
    }

    /**
     * Vytvoří index na jméno potomka na hasResource a hasParent včetně intervalu platnosti transakcí.
     * @param transaction transakce pro přístup k databázi
     * @param tranStartKey klíč pro property označující start transakce
     * @param tranEndKey klíč pro property označující konec transakce
     */
    private void createHierarchyEdgeIndex(TitanTransaction transaction, TitanKey tranStartKey, TitanKey tranEndKey) {
        if (!transaction.getIndexedKeys(Edge.class).contains(EdgeProperty.CHILD_NAME.t())) {
            try {
                // FIXME tady by měl být long, jenže co se zpětnou kompatibilitou!?
                TitanKey childNameKey = transaction.makeKey(EdgeProperty.CHILD_NAME.t()).dataType(String.class)
                        .indexed(Edge.class).make();
                GraphCreation.LOGGER.info("Create key for " + EdgeProperty.CHILD_NAME.t());

                createLabelWithTran(transaction, EdgeLabel.HAS_RESOURCE, childNameKey, tranStartKey, tranEndKey);
                createLabelWithTran(transaction, EdgeLabel.HAS_PARENT, childNameKey, tranStartKey, tranEndKey);
            } catch (UnsupportedOperationException e) {
                GraphCreation.LOGGER.error("Error during creating index.", e);
            }
        }
    }

    /**
     * Vytvoří index na lokální název souboru pro source code hrany včetně intervalu platnosti transakcí.
     * @param transaction transakce pro přístup k databázi
     * @param tranStartKey klíč pro property označující start transakce
     * @param tranEndKey klíč pro property označující konec transakce
     */
    private void createSourceCodeEdgeIndex(TitanTransaction transaction, TitanKey tranStartKey, TitanKey tranEndKey) {
        if (!transaction.getIndexedKeys(Edge.class).contains(EdgeProperty.SOURCE_LOCAL_NAME.t())) {
            try {
                TitanKey sourceLocalNameKey = transaction.makeKey(EdgeProperty.SOURCE_LOCAL_NAME.t())
                        .dataType(String.class).indexed(Edge.class).make();
                GraphCreation.LOGGER.info("Create key for " + EdgeProperty.SOURCE_LOCAL_NAME.t());

                createLabelWithTran(transaction, EdgeLabel.HAS_SOURCE, sourceLocalNameKey, tranStartKey, tranEndKey);
            } catch (UnsupportedOperationException e) {
                GraphCreation.LOGGER.error("Error during creating index.", e);
            }
        }
    }

    /**
     * Vytvoří label tříděný podle speciální property, začátek a konec transakce. 
     * @param transaction transakce pro přístup k databázi
     * @param label label hrany
     * @param labelKey klíč pro speciální property na hraně 
     * @param tranStartKey klíč pro property pro start transakce
     * @param tranEndKey klíč pro property pro konec transakce
     */
    private void createLabelWithTran(TitanTransaction transaction, EdgeLabel label, TitanKey labelKey,
            TitanKey tranStartKey, TitanKey tranEndKey) {
        if (transaction.getType(label.t()) == null) {
            transaction.makeLabel(label.t()).sortKey(labelKey, tranEndKey, tranStartKey).make();
            GraphCreation.LOGGER.info("Create label " + label.t());
        }
    }

    @Override
    public String getModuleName() {
        return GraphCreation.MODULE_NAME;
    }
}