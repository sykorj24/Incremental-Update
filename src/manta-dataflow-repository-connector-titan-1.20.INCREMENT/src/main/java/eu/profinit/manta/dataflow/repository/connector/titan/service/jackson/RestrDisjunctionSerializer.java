package eu.profinit.manta.dataflow.repository.connector.titan.service.jackson;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.ser.std.SerializerBase;

import eu.profinit.manta.dataflow.model.restriction.nodetypes.RestrDisjunctionTypeDnf;

/**
 * Jason serializer pro predikáty.
 * @author tfechtner
 *
 */
public class RestrDisjunctionSerializer extends SerializerBase<RestrDisjunctionTypeDnf> {

    /**
     * Výchozí konstruktor registrující typ třídy. 
     */
    public RestrDisjunctionSerializer() {
        super(RestrDisjunctionTypeDnf.class);
    }

    @Override
    public void serialize(RestrDisjunctionTypeDnf value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonGenerationException {
        jgen.writeString(value.toString());
    }

}
