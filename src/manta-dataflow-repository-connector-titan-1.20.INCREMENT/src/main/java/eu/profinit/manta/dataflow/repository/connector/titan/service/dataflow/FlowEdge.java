package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;

/**
 * Datová immutable třída pro definici hrany pro klienta. <br>
 * Může jít i o cestu, pokud jsou mezi vrcholy nezobrazované (filtr, mimo referenční view.). 
 * @author tfechtner
 */
public final class FlowEdge {
    /** Id zdrojového uzlu hrany. */
    private final Long source;
    /** Id koncového uzlu hrany. */
    private final Long target;
    /** Výsledný label hrany. */
    private final EdgeLabel label;
    /** Atributy hrany. */
    private final Map<String, Object> attrs;
    /** Interval revizí platnosti hrany. */
    private final RevisionInterval revInterval;

    /**
     * @param source Id zdrojového uzlu hrany.
     * @param target Id koncového uzlu hrany.
     * @param direction směr hrany, {@link Direction#OUT} znamená {@link #source}->{@link #target}.
     * @param label Výsledný label hrany.
     * @param revisionInterval Interval revizí platnosti hrany
     */
    public FlowEdge(Vertex source, Vertex target, Direction direction, EdgeLabel label,
            RevisionInterval revisionInterval) {
        this(source, target, direction, label, revisionInterval, Collections.<String, Object> emptyMap());
    }

    /**
     * @param source Id zdrojového uzlu hrany.
     * @param target Id koncového uzlu hrany.
     * @param direction směr hrany, {@link Direction#OUT} znamená {@link #source}->{@link #target}.
     * @param label Výsledný label hrany.
     * @param revisionInterval Interval revizí platnosti hrany
     * @param attributes atributy hrany, nesmí být null
     */
    public FlowEdge(Vertex source, Vertex target, Direction direction, EdgeLabel label,
            RevisionInterval revisionInterval, Map<String, Object> attributes) {
        Validate.notNull(attributes, "Attributes map must not be null.");
        if (direction == Direction.OUT) {
            this.source = (Long) source.getId();
            this.target = (Long) target.getId();

        } else {
            this.source = (Long) target.getId();
            this.target = (Long) source.getId();
        }
        this.label = label;
        this.attrs = Collections.unmodifiableMap(attributes);
        this.revInterval = revisionInterval;
    }

    /**
     * @param edge edge zdrojová hrana v grafu
     * @param technicalAttributes držák na technické atributy
     */
    public FlowEdge(Edge edge, TechnicalAttributesHolder technicalAttributes) {
        this.source = (Long) edge.getVertex(Direction.OUT).getId();
        this.target = (Long) edge.getVertex(Direction.IN).getId();
        this.label = EdgeLabel.parseFromDbType(edge.getLabel());
        this.attrs = Collections.unmodifiableMap(fetchAttributes(edge, technicalAttributes));

        Integer edgeTranStart = edge.getProperty(EdgeProperty.TRAN_START.t());
        Integer edgeTranEnd = edge.getProperty(EdgeProperty.TRAN_END.t());
        this.revInterval = new RevisionInterval(edgeTranStart, edgeTranEnd);
    }

    /**
     * Získá atributy hrany, přičemž odfiltruje technické.
     * @param edge hrana, pro kterou se získávají atributy 
     * @param technicalAttributes držák na rozpoznání technických atributů
     * @return atributy hrany, nikdy null
     */
    public static Map<String, Object> fetchAttributes(Edge edge, TechnicalAttributesHolder technicalAttributes) {
        Validate.notNull(edge, "Edge must not be null.");
        Set<String> keys = edge.getPropertyKeys();
        Map<String, Object> attrs = new HashMap<>();
        for (String key : keys) {
            if (!technicalAttributes.isEdgeAttributeTechnical(key)) {
                attrs.put(key, edge.getProperty(key));
            }
        }

        return attrs;
    }

    /**
     * @return Id zdrojového uzlu hrany.
     */
    public Long getSource() {
        return source;
    }

    /**
     * @return Id koncového uzlu hrany.
     */
    public Long getTarget() {
        return target;
    }

    /**
     * @return Výsledný label hrany.
     */
    public EdgeLabel getLabel() {
        return label;
    }

    /**
     * @return netechnické atributy hrany 
     */
    public Map<String, Object> getAttrs() {
        return attrs;
    }

    /**
     * @return Interval revizí platnosti hrany. 
     */
    public RevisionInterval getRevInterval() {
        return revInterval;
    }

    @Override
    public String toString() {
        return source + "-" + label + "-" + target + "(" + revInterval + ")";
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(source);
        builder.append(target);
        builder.append(label);
        builder.append(revInterval);
        return builder.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        FlowEdge other = (FlowEdge) obj;
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(source, other.source);
        builder.append(target, other.target);
        builder.append(label, other.label);
        builder.append(revInterval, other.revInterval);
        return builder.isEquals();
    }
}
