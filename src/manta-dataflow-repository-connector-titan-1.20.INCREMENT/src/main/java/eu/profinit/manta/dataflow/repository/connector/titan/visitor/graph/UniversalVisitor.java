package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Interface pro návštěvy vrcholů a hran v grafu.
 * 
 * @author Erik Kratochvíl
 */
public interface UniversalVisitor {

    /**
     * Obecná metoda pro zpracování vrcholu grafu.
     * Metoda je společná pro všechny typy vrcholů.
     * 
     * @param vertex vrchol grafu
     * @param distance vzdálenost vrcholu grafu od výchozího vrcholu
     * @param visitedForTheFirstTime true, pokud je vrchol navštíven při průchodu grafem poprvé 
     *         (v opačném případě indikuje cyklus)
     * @param revisionInterval interval revizí, který se prochází
     * @return true, jestliže se má pokračovat v navštěvování
     */
    boolean visit(Vertex vertex, long distance, boolean visitedForTheFirstTime, RevisionInterval revisionInterval);

    /**
     * Obecná metoda pro zpracování hrany grafu.
     * Metoda je společná pro všechny typy hran.
     * 
     * @param edge hrana grafu
     * @param label typ (label) hrany ({@link EdgeLabel})
     * @param direction orientace hrany ({@link Direction}) z pohledu vrcholu, pro který je hrana reportována
     * @param revisionInterval interval revizí, který se prochází
     * @return true, jestliže se má pokračovat v navštěvování
     */
    boolean visit(Edge edge, EdgeLabel label, Direction direction, RevisionInterval revisionInterval);
}
