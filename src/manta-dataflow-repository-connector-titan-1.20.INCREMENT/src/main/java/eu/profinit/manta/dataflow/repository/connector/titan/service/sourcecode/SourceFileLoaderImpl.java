package eu.profinit.manta.dataflow.repository.connector.titan.service.sourcecode;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;

/**
 * Defaultni implementace {link SourceFileLoader}.
 * 
 * @author onouza
 */
public class SourceFileLoaderImpl implements SourceFileLoader {

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(SourceFileLoaderImpl.class);

    @Autowired
    private SourceRootHandler sourceRootHandler;

    @Override
    public List<String> loadSourceFile(SourceFile sourceFile) {
        Validate.notNull(sourceFile, "'sourceFile' must not be null");
        
        BufferedReader reader = null;
        List<String> result = new LinkedList<>();

        try {
            File sourceFileHandler = new File(sourceRootHandler.getSourceCodeDir(), sourceFile.getLocation());
            reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(sourceFileHandler), sourceFile.getEncoding()));
            String line;
            while ((line = reader.readLine()) != null) {
                result.add(line);
            }
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Unsupported encoding for source code script.", e);
            return null;
        } catch (FileNotFoundException e) {
            LOGGER.error("Source code script does not found.", e);
            return null;
        } catch (IOException e) {
            LOGGER.error("Error during reading source code script.", e);
            return null;
        } finally {
            IOUtils.closeQuietly(reader);
        }

        return result;
    }

}
