package eu.profinit.manta.dataflow.repository.connector.titan.connection;

/**
 * Výjimka informující o proběhlém rollback v podkladové databázi.
 * @author tfechtner
 *
 */
public class RepositoryRollbackException extends RuntimeException {

    private static final long serialVersionUID = 9076791331134812421L;

    /**
     * @param cause důvod výjimky
     */
    public RepositoryRollbackException(Throwable cause) {
        super(cause);
    }

}
