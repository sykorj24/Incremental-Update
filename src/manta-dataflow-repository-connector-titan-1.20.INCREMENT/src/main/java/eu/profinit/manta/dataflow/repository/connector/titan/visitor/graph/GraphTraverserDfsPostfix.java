package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Algoritmus procházející graf stylem do hloubky.
 * Zpracování jednoho objektu se provede až po zpracování jeho potomků a objektů se ho týkajícího.
 * Nejprve se zpracují všechny hrany a až pak uzly.
 * @author tfechtner
 *
 */
public class GraphTraverserDfsPostfix extends AbstractGraphTraverser {
    
    @Override
    protected void processResource(GraphVisitor visitor, Vertex resource, RevisionInterval revisionInterval) {        
        processResourceChildren(visitor, resource, revisionInterval);
        visitor.visitResource(resource);
    }
    
    @Override
    protected void processNode(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval) {        
        processNodeChildren(visitor, node, revisionInterval);        
        processNodeAttributes(visitor, node, revisionInterval);
        visitor.visitNode(node);
    }
    
    @Override
    protected void processNodeEdges(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval) {
        processChildrenNodesEdges(visitor, node, revisionInterval);
        processNodeDirectEdges(visitor, node, revisionInterval);
        processNodeFilterEdges(visitor, node, revisionInterval);
        processNodeMapsToEdges(visitor, node, revisionInterval);
    }
}
