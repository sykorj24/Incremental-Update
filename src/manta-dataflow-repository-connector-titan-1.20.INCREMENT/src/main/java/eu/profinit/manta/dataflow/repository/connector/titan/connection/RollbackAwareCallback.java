package eu.profinit.manta.dataflow.repository.connector.titan.connection;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;

/**
 * Callback zapouzdřující volání operace pro ošetření rollbacku.
 * @author tfechtner
 *
 * @param <T> návratová hodnota operace
 */
public class RollbackAwareCallback<T> implements TransactionCallback<T> {
    /** Název modulu pro přístup k databázi. */
    private static final String MODULE_NAME = "mr_basic";
    /** Objekt, který se přeovlá pro řešení rollbacku. */
    private final RollbackAware<T> rollbackAware;

    /**
     * @param rollbackAware Objekt, který se přeovlá pro řešení rollbacku
     */
    public RollbackAwareCallback(RollbackAware<T> rollbackAware) {
        super();
        this.rollbackAware = rollbackAware;
    }

    @Override
    public String getModuleName() {
        return MODULE_NAME;
    }

    @Override
    public T callMe(TitanTransaction transaction) {
        return rollbackAware.rollback(transaction);
    }

}
