package eu.profinit.manta.dataflow.repository.connector.titan.filter.model;

import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonSubTypes.Type;
import org.codehaus.jackson.annotate.JsonTypeInfo;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.unit.AttributeFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.unit.NameFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.unit.ResourceFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.unit.TypeFilterUnit;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Jednotkový nebo také atomický filtr.
 * @author tfechtner
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = AttributeFilterUnit.class, name = "attribute"),
        @Type(value = TypeFilterUnit.class, name = "type"),
        @Type(value = ResourceFilterUnit.class, name = "resource"),
        @Type(value = NameFilterUnit.class, name = "name") })
public interface HorizontalFilterUnit {

    /**
     * True, jestliže je daný vrchol tímto filtrem filotrván.
     * @param vertex zkoumaný vrchol
     * @param revisionInterval revize, na které se filtr provádí
     * @return true, jestliže je daný vrchol s danou revizí filtrován
     */
    boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval);
}
