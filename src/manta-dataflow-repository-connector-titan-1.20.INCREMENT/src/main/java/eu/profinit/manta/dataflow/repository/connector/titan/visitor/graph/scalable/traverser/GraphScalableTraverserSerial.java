package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser;

import java.util.Deque;
import java.util.LinkedList;

import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.AccessLevel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactory;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Konfigurovatelný traverser procházející graf ve více sekvenčně spouštěných transakcí.
 * Na základě továrny si vytvoří {@link TraverserProcessor}, jehož jednu instanci opakovaně volá
 * pro zpracování příslušného počtu elementů. Každé takové volání probíhá ve zvláštní transakci.
 * 
 * Instance {@link TraverserProcessor} se používá jediná, aby měla možnost si udržet vnitřní stav.
 * 
 * @author tfechtner
 *
 */
public class GraphScalableTraverserSerial implements GraphScalableTraverser {
    /** Služba pro přístup k databázi. */
    private final DatabaseHolder databaseHolder;
    /** Továrna na vytvoření procesoru. */
    private final TraverserProcessorFactory processorFactory;

    /**
     * @param databaseHolder Služba pro přístup k databázi.
     * @param processorFactory Továrna na vytvoření procesoru
     */
    public GraphScalableTraverserSerial(DatabaseHolder databaseHolder,
            TraverserProcessorFactory processorFactory) {
        super();
        this.databaseHolder = databaseHolder;
        this.processorFactory = processorFactory;
    }

    @Override
    public void traverse(GraphVisitor visitor, Object startVertexId, AccessLevel accessLevel,
            RevisionInterval revisionInterval) {

        Deque<Object> idsToProcess = new LinkedList<Object>();
        idsToProcess.addLast(startVertexId);

        TraverserProcessor<?> traverserProcessor = processorFactory.createTraverser(visitor, revisionInterval,
                idsToProcess);

        while (idsToProcess.size() > 0) {
            accessLevel.runInTransaction(databaseHolder, traverserProcessor);
        }
    }
}
