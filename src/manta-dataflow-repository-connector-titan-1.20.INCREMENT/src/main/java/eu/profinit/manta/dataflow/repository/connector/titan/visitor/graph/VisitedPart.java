package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

/**
 * Enum je určen k identifikaci těch částí, které mají být při průchodu grafem navštíveny visitorem.
 * @author Erik Kratochvíl
 */
public enum VisitedPart {
    /** Navštívení samotného vrcholu. */
    SELF,
    /** Navštívení následníků (potomků) vrcholu. */
    SUCCESSORS,
    /** Navštívení atributů vrcholu. */
    ATTRIBUTES,
    /** Navštívení hran vrcholu. */
    EDGES
}