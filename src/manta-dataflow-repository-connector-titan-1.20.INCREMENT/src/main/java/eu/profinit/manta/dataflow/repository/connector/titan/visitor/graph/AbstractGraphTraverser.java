package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Algoritmus procházející graf stylem do hloubky.
 * Nejprve se zpracují všechny uzly a až pak se znovu projde graf a navštěvují se hrany (direct a filter).
 * @author tfechtner
 *
 */
public abstract class AbstractGraphTraverser implements GraphTraverser {

    @Override
    public void traverse(GraphVisitor visitor, Vertex superRoot, RevisionInterval revisionInterval) {
        List<Vertex> resourceList = GraphOperation.getAdjacentVertices(superRoot, Direction.IN, revisionInterval,
                EdgeLabel.HAS_RESOURCE);

        for (Vertex resource : resourceList) {
            processLayersOfResource(visitor, resource, revisionInterval);
            processResource(visitor, resource, revisionInterval);
        }

        for (Vertex resource : resourceList) {
            processResourceEdges(visitor, resource, revisionInterval);
        }
    }

    @Override
    public void traverseOnlyOneResource(GraphVisitor visitor, Vertex resource, RevisionInterval revisionInterval) {
        processLayersOfResource(visitor, resource, revisionInterval);
        processResource(visitor, resource, revisionInterval);
        processResourceEdges(visitor, resource, revisionInterval);
    }

    /**
     * Zpracuje resource a všechny jeho potomky (uzly).
     * @param visitor používaný visitor
     * @param resource zpracovávaný resource
     * @param revisionInterval interval revizí, ve který se zpracovává
     */
    protected abstract void processResource(GraphVisitor visitor, Vertex resource, RevisionInterval revisionInterval);

    /**
     * Zpracuje uzel a všechny jeho potomky (uzly).
     * @param visitor používaný visitor
     * @param node zpracovávaný uzel
     * @param revisionInterval interval revizí, ve který se zpracovává
     */
    protected abstract void processNode(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval);

    /**
     * Zpracuje všechny direct a flow hrany v podstromu uzlu.
     * V podstromu se pohybuje pouze po hranách {@link EdgeLabel#HAS_PARENT}.
     * @param visitor používaný visitor
     * @param node zpracovávaný uzel
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    protected abstract void processNodeEdges(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval);

    /**
     * 
     * @param visitor používaný visitor
     * @param resource zpracovávaný resource
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    protected final void processResourceChildren(GraphVisitor visitor, Vertex resource,
            RevisionInterval revisionInterval) {
        List<Vertex> nodeList = GraphOperation.getAdjacentVertices(resource, Direction.IN, revisionInterval,
                EdgeLabel.HAS_RESOURCE);
        for (Vertex node : nodeList) {
            if (GraphOperation.getParent(node) == null) {
                processNode(visitor, node, revisionInterval);
            }
        }
    }

    /**
     * Převolá visitora na všechny potomky daného uzlu.
     * @param visitor používaný visitor
     * @param node zpracovávaný uzel
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    protected final void processNodeChildren(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval) {
        List<Vertex> childrenList = GraphOperation.getAdjacentVertices(node, Direction.IN, revisionInterval,
                EdgeLabel.HAS_PARENT);
        for (Vertex child : childrenList) {
            processNode(visitor, child, revisionInterval);
        }
    }

    /**
     * Převolá visitora na všechny atributy daného uzlu.
     * @param visitor používaný visitor
     * @param node zpracovávaný uzel
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    protected final void processNodeAttributes(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval) {
        List<Vertex> attrList = GraphOperation.getAdjacentVertices(node, Direction.OUT, revisionInterval,
                EdgeLabel.HAS_ATTRIBUTE);
        for (Vertex attr : attrList) {
            visitor.visitAttribute(attr);
        }
    }

    /**
     * Zpracuje všechny direct a flow hrany v podstromu resource.
     * V podstromu se pohybuje pouze po hranách {@link EdgeLabel#HAS_RESOURCE} a {@link EdgeLabel#HAS_PARENT}.
     * @param visitor používaný visitor
     * @param resource zpracovávaný resource
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    protected final void processResourceEdges(GraphVisitor visitor, Vertex resource,
            RevisionInterval revisionInterval) {
        List<Vertex> nodeList = GraphOperation.getAdjacentVertices(resource, Direction.IN, revisionInterval,
                EdgeLabel.HAS_RESOURCE);
        for (Vertex node : nodeList) {
            if (GraphOperation.getParent(node) == null) {
                processNodeEdges(visitor, node, revisionInterval);
            }
        }
    }

    /**
     * Převolá visitora na všechny direct hrany pro daný uzel.
     * @param visitor používaný visitor
     * @param node zpracovávaný uzel
     * @param revisionInterval interval revizí, ve který se zpracovává
     */
    protected final void processNodeDirectEdges(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval) {
        List<Edge> directEdges = GraphOperation.getAdjacentEdges(node, Direction.OUT, revisionInterval,
                EdgeLabel.DIRECT);
        for (Edge edge : directEdges) {
            visitor.visitEdge(edge);
        }
    }

    /**
     * Převolá visitora na všechny filter hrany pro daný uzel.
     * @param visitor používaný visitor
     * @param node zpracovávaný uzel
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    protected final void processNodeFilterEdges(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval) {
        List<Edge> filterEdges = GraphOperation.getAdjacentEdges(node, Direction.OUT, revisionInterval,
                EdgeLabel.FILTER);
        for (Edge edge : filterEdges) {
            visitor.visitEdge(edge);
        }
    }

    /**
     * Převolá visitora na všechny mapsTo hrany pro daný uzel.
     * @param visitor používaný visitor
     * @param node zpracovávaný uzel
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    protected final void processNodeMapsToEdges(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval) {
        List<Edge> mapsToEdges = GraphOperation.getAdjacentEdges(node, Direction.OUT, revisionInterval,
                EdgeLabel.MAPS_TO);
        for (Edge edge : mapsToEdges) {
            visitor.visitEdge(edge);
        }
    }
    
    /**
     * Převolá visitora na všechny hrany všech potomků daného uzlu.
     * @param visitor používaný visitor
     * @param node zpracovávaný uzel
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    protected final void processChildrenNodesEdges(GraphVisitor visitor, Vertex node,
            RevisionInterval revisionInterval) {
        List<Vertex> childrenList = GraphOperation.getAdjacentVertices(node, Direction.IN, revisionInterval,
                EdgeLabel.HAS_PARENT);
        for (Vertex child : childrenList) {
            processNodeEdges(visitor, child, revisionInterval);
        }
    }

    /**
     * Prevola visitora na vsechny vrstvy daneho resource.
     * @param visitor Pouzivany visitor
     * @param resource Resource, na jehoz vrstvach se ma visitor prevolat
     * @param revisionInterval Interval revizi, ve kterych ma traverser prochazet
     */
    protected final void processLayersOfResource(GraphVisitor visitor, Vertex resource, RevisionInterval revisionInterval) {
        Set<Object> processedLayers = new HashSet<Object>();
        List<Vertex> layerList = GraphOperation.getAdjacentVertices(resource, Direction.OUT, revisionInterval,
                EdgeLabel.IN_LAYER);
        for (Vertex layer : layerList) {
            if (!processedLayers.contains(layer.getId())) {
                visitor.visitLayer(layer);
                processedLayers.add(layer.getId());
            }
        }
    }

}
