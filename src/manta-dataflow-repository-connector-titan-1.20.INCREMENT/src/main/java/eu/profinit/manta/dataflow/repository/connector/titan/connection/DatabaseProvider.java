package eu.profinit.manta.dataflow.repository.connector.titan.connection;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanException;
import com.thinkaurelius.titan.core.TitanFactory;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.util.TitanCleanup;

/**
 * Poskytovatel připojení do titan db podle id. 
 * Třída si vnitřně udržuje otevřené databáze.
 * @author tfechtner
 *
 */
public final class DatabaseProvider {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseProvider.class);
    /** Instance singletonu. */
    public static final DatabaseProvider INSTANCE = new DatabaseProvider();
    /** Mapa databází indexovaná id databáze. */
    private Map<String, TitanGraph> databases = new HashMap<String, TitanGraph>();

    /** 
     * Singleton -> zamknout konstruktor.
     */
    private DatabaseProvider() {
    }

    /**
     * Metoda pro získání databázového připojení na základě id a configurace.
     * @param dbId id databáze
     * @param configuration konfigurace databáze
     * @return connection do databáze
     */
    public synchronized TitanGraph getDatabase(String dbId, Configuration configuration) {
        if (dbId == null) {
            throw new IllegalArgumentException("Db ID must be set.");
        }
        if (configuration == null) {
            throw new IllegalArgumentException("Configuration must be set.");
        }

        TitanGraph graphDb = databases.get(dbId);

        if (graphDb == null) {
            try {
                graphDb = TitanFactory.open(configuration);
                databases.put(dbId, graphDb);
            } catch (TitanException e) {
                LOGGER.error("Error during creating database with configuration " + configuration.toString() + ".", e);
                throw new RuntimeException("Cannot open database.", e);
            }
        } else if (!graphDb.isOpen()) {
            graphDb = TitanFactory.open(configuration);
        }

        return graphDb;
    }

    /**
     * Zavře databázi s daným id.
     * @param dbId id databáze, která se má zavřít
     */
    public synchronized void closeDatabase(String dbId) {
        if (dbId == null) {
            throw new IllegalArgumentException("Db ID must be set.");
        }
        
        TitanGraph graphDb = databases.get(dbId);
        if (graphDb != null && graphDb.isOpen()) {
            graphDb.shutdown();
            databases.remove(dbId);
        }
    }

    /**
     * Zavře databázi s daným id a smaže ji.
     * @param dbId id databáze, která se má zavřít
     */
    public synchronized void closeAndEraseDatabase(String dbId) {
        if (dbId == null) {
            throw new IllegalArgumentException("Db ID must be set.");
        }
        
        TitanGraph graphDb = databases.get(dbId);
        if (graphDb != null && graphDb.isOpen()) {
            graphDb.shutdown();
            try {
                TitanCleanup.clear(graphDb);
            } catch (Exception e) {
                LOGGER.error("Cannot delete database files. ");
            }
            databases.remove(dbId);
        }
    }

}
