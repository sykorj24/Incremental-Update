package eu.profinit.manta.dataflow.repository.connector.titan.service.sourcecode;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Rozhrani sluzby pro ziskani zdrojoveho kodu uzlu
 * 
 * @author onouza
 */
public interface SourceCodeService {

    /**
     * Vrati zdrojovy kod daneho uzlu nebo {@code null}, pokud zdrojovy kod uzlu neexistuje nebo se jej nepodari nacist.
     * 
     * @param node Zkoumany uzel
     * @param revisionInterval Interval revizi platnosti vazby zdrojoveho kodu na uzel
     * @param reduceIndent {@code true} pokud se ma odsazeni radku zredukovat na minimum, {@code false} pokud se ma zachovat
     * 
     * @return Zdrojovy kod daneho uzlu nebo {@code null}, pokud zdrojovy kod uzlu neexistuje nebo se jej nepodari nacist.
     */
    String getSourceCode(Vertex node, RevisionInterval revisionInterval, boolean reduceIndent);
}
