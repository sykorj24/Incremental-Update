package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import com.tinkerpop.blueprints.Direction;

import eu.profinit.manta.dataflow.repository.connector.titan.predicate.PreferredEdgeTypePredicate;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Továrna vytvářející universal traversery pro standardní use-cases.
 * @author tfechtner
 *
 */
public final class UniversalTraverserFactory {
    
    private UniversalTraverserFactory() {        
    };
    
    /**
     * Vytvoří traverser pro základní průchod stromovou hierarchií HAS_PARENT / HAS_RESOURCE.
     * @param revisionInterval interval revizí, které se mají projít
     * @return nový traverser
     */
    public static UniversalTraverser createHierarchical(RevisionInterval revisionInterval) {
        UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.IN);
        traverser.setEdgeLabels(EdgeLabel.HAS_RESOURCE, EdgeLabel.HAS_PARENT);
        traverser.setRevisionInterval(revisionInterval);
        traverser.setSearchOrder(new SearchOrderDfs());
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.SUCCESSORS);
        traverser.setShouldEdgeBeTraversed(PreferredEdgeTypePredicate.PREFER_PARENT_OVER_RESOURCE);
        return traverser;
    }
}
