package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable;

import java.util.Deque;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Prohledávání do hloubky.
 * @author tfechtner
 *
 */
public class SearchOrderDfs implements SearchOrder {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchOrderDfs.class);
    /** Maximální doba čekání na prvek z fronty. */
    private final int maxPollWait;

    /**
     * Výchozí kosntruktor nastavující čekání na výchozí hodotu.
     */
    public SearchOrderDfs() {
        this(DEFAULT_MAX_POLL_WAIT);
    }

    /**
     * @param maxPollWait Maximální doba čekání na prvek z fronty
     */
    public SearchOrderDfs(int maxPollWait) {
        super();
        this.maxPollWait = maxPollWait;
    }

    @Override
    public <T> T getNext(Deque<T> deque) {
        if (deque instanceof BlockingDeque) {
            try {
                return ((BlockingDeque<T>) deque).pollLast(maxPollWait, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                LOGGER.warn("Interrupted fetching next element.");
                return null;
            }
        } else {
            return deque.pollLast();
        }
    }
}
