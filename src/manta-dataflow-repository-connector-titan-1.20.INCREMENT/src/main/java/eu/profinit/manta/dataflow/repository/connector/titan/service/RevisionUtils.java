package eu.profinit.manta.dataflow.repository.connector.titan.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.Validate;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.thinkaurelius.titan.core.attribute.Cmp;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Pomocná třída pro práci nad temporálními daty.
 * @author pholecek, tfechtner
 *
 */
public final class RevisionUtils {
    /** Double in the Titan has maximum scale up to 6 decimal digits */
    public static final Integer SCALE = 6;
    /** The maximum minor revision number within one major revision */
    public static final BigDecimal MINOR_REVISION_MAXIMUM_NUMBER = new BigDecimal("0.999999").setScale(SCALE,
            RoundingMode.DOWN);

    /**
     * Zakázat konstrukci, jde o util třídu.
     */
    private RevisionUtils() {
    }

    /**
     * Metoda vyhledá pomocí indexu všechny hrany navázané na určitý uzel v rámci dané revize.
     * @param vertex uzel, k němuž se uzly vyhledávají
     * @param revInterval interval prohledávaných revizí
     * @param direction směr hran
     * @param edgeLabels typy hran, přes které se má hledat
     * @return nalezené vrcholy
     */
    public static Iterable<Edge> getAdjacentEdges(Vertex vertex, Direction direction, RevisionInterval revInterval,
            String... edgeLabels) {
        Validate.notNull(vertex);
        Validate.notNull(revInterval);
        Validate.notNull(direction);
        Validate.notEmpty(edgeLabels);

        return vertex.query().has(EdgeProperty.TRAN_START.t(), Cmp.LESS_THAN_EQUAL, revInterval.getEnd())
                .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, revInterval.getStart()).labels(edgeLabels)
                .direction(direction).edges();
    }

    /**
     * Get all adjacent edges (with any label) of a vertex valid in a specific revision interval. 
     * 
     * @param vertex  vertex, to which the searched edges are connected
     * @param revInterval  revision interval of the edges
     * @param direction  direction of the edges
     * @return edges of the vertex
     */
    public static Iterable<Edge> getAdjacentEdges(Vertex vertex, Direction direction, RevisionInterval revInterval) {
        Validate.notNull(vertex);
        Validate.notNull(revInterval);
        Validate.notNull(direction);

        return vertex.query().has(EdgeProperty.TRAN_START.t(), Cmp.LESS_THAN_EQUAL, revInterval.getEnd())
                .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, revInterval.getStart()).direction(direction)
                .edges();
    }

    /**
     * Metoda vyhledá pomocí indexu všechny uzly navázané na určitý uzel v rámci dané revize.
     * @param vertex uzel, k němuž se uzly vyhledávají
     * @param revInterval interval prohledávaných revizí
     * @param direction směr hran
     * @param edgeLabels typy hran, přes které se má hledat
     * @return nalezené vrcholy
     */
    public static Iterable<Vertex> getAdjacentVertices(Vertex vertex, RevisionInterval revInterval, Direction direction,
            String... edgeLabels) {
        Validate.notNull(vertex);
        Validate.notNull(revInterval);
        Validate.notNull(direction);
        Validate.notEmpty(edgeLabels);

        return vertex.query().has(EdgeProperty.TRAN_START.t(), Cmp.LESS_THAN_EQUAL, revInterval.getEnd())
                .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, revInterval.getStart()).labels(edgeLabels)
                .direction(direction).vertices();
    }

    /**
     * Set end revision of the vertex by setting tranEnd of its control edge(s).
     * Every vertex has always a unique set of control edges valid in one and only one revision interval.
     * In other words, no vertex can have more control edges in different revision intervals.
     * 
     * End revision can be set to any value (increased and decreased by almost any value). 
     * Thus, there is no constraint or condition for setting a new tranEnd number based on the current tranEnd number (see examples below).
     * 
     * Example 1: tranEnd can be increased from 1.999999 to 2.999999 
     * (when major revision 2.000000 was created and the vertex is present in this new major revision as well)
     * 
     * Example 2: tranEnd can be decreased from 1.999999 to 1.000001 
     * (when the vertex was removed in the revision 1.000002)
     * 
     * Example 3: tranEnd can be decreased from 2.999999 to 1.999999
     * (very special case when dealing with the overflow of minor revisions in the major revision 1) 
     * 
     * @param vertex  vertex, whose control edge(s) will have set a new end revision
     * @param newEndRevision  new value of the end revision number
     */
    public static void setVertexTransactionEnd(Vertex vertex, Double newEndRevision) {
        Validate.notNull(vertex);
        Validate.notNull(newEndRevision);

        VertexType vertexType = VertexType.getType(vertex);
        Direction direction = vertexType.getControlEdgeDirection();
        String[] edgeLabels = vertexType.getControlEdgeLabels();

        Iterator<Edge> edgeIter = vertex.query().direction(direction).labels(edgeLabels).edges().iterator();
        while (edgeIter.hasNext()) {
            setEdgeTransactionEnd(edgeIter.next(), newEndRevision);
        }
    }

    /**
     * Nastaví dané hraně transakční konec.
     * @param edge hrana, která se má nastavit
     * @param newEndRevision číslo nové revize na které se bude transakční konec hrany nastavovat
     */
    public static void setEdgeTransactionEnd(Edge edge, Double newEndRevision) {
        Validate.notNull(edge);
        Validate.notNull(newEndRevision);

        Double currentTranStart = edge.getProperty(EdgeProperty.TRAN_START.t());
        if (newEndRevision < currentTranStart) {
            throw new IllegalStateException("Cannot set transaction end smaller than transaction start.");
        }
        edge.setProperty(EdgeProperty.TRAN_END.t(), newEndRevision);
    }

    /**
     * Nastaví dané hraně transakční start.
     * @param edge hrana, která se má nastavit
     * @param newStartRevision číslo nové revize, na které se bude transakční začátek nastavovat
     */
    public static void setEdgeTransactionStart(Edge edge, Double newStartRevision) {
        Validate.notNull(edge);
        Validate.notNull(newStartRevision);

        Integer currentTranEnd = edge.getProperty(EdgeProperty.TRAN_END.t());
        if (newStartRevision > currentTranEnd) {
            throw new IllegalStateException("Cannot set transaction start greater than transaction end.");
        }
        edge.setProperty(EdgeProperty.TRAN_START.t(), newStartRevision);
    }

    /**
     * Kontrola, že daný vrchol patří do daného revizního intervalu.
     * @param vertex vrchol, který se konroluje
     * @param revInterval interval revizí, ve kterém má mít vrchol platnost
     * @return true, jestliže má vrchol platnost v daném intervalu
     */
    public static boolean isVertexInRevisionInterval(Vertex vertex, RevisionInterval revInterval) {
        Validate.notNull(vertex);
        Validate.notNull(revInterval);

        VertexType vertexType = VertexType.getType(vertex);
        Direction direction = vertexType.getControlEdgeDirection();
        if (direction == null) {
            // special nodes that are in all revisions
            return true;
        }

        String[] edgeLabels = vertexType.getControlEdgeLabels();

        Iterator<Edge> edgeIter = vertex.query().direction(direction).labels(edgeLabels).edges().iterator();

        boolean allEdgesValid = true;
        while (edgeIter.hasNext()) {
            allEdgesValid &= isEdgeInRevisionInterval(edgeIter.next(), revInterval);
        }

        return allEdgesValid;
    }

    /**
     * Kontrola, že daná hrana patří do daného revizního intervalu.
     * @param edge hrana, která se konroluje
     * @param revInterval interval revizí, ve kterém má mít vrchol platnost
     * @return true, jestliže má hrana platnost v daném intervalu
     */
    public static boolean isEdgeInRevisionInterval(Edge edge, RevisionInterval revInterval) {
        Validate.notNull(edge);
        Validate.notNull(revInterval);

        RevisionInterval edgeRevInterval = getRevisionInterval(edge);
        boolean isEdgeValid = ((revInterval.getStart() <= edgeRevInterval.getEnd())
                && (revInterval.getEnd() >= edgeRevInterval.getStart()));
        return isEdgeValid;
    }

    /**
     * Vrátí reviziní itnerval, ve kterém má hrana platnost.
     * @param edge hrana, která se zkoumá
     * @return revizní interval, kdy má hrana platnost
     * @throws IllegalStateException hrana nemá informaci o revizích
     */
    public static RevisionInterval getRevisionInterval(Edge edge) {
        Validate.notNull(edge, "Edge must not be null.");

        Double edgeTranStart = edge.getProperty(EdgeProperty.TRAN_START.t());
        Double edgeTranEnd = edge.getProperty(EdgeProperty.TRAN_END.t());

        if (edgeTranStart == null || edgeTranEnd == null) {
            throw new IllegalStateException("Edge doesn't have all transaction data.");
        }

        return new RevisionInterval(edgeTranStart, edgeTranEnd);
    }

    /**
     * Get maximum minor revision number from a specified revision number. That means, get "majorRevision.999999".
     * <p>
     * For instance, revision = 1.123456. Then, the output is "1.999999".
     * 
     * @param revision  revision number
     * @return maximum minor revision number
     */
    public static Double getMaxMinorRevisionNumber(Double revision) {
        BigDecimal integerPart = BigDecimal.valueOf(Math.floor(revision)).setScale(SCALE, RoundingMode.DOWN);
        BigDecimal maximumMinorRevision = integerPart.add(MINOR_REVISION_MAXIMUM_NUMBER);
        return maximumMinorRevision.doubleValue();
    }
}
