package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.GraphFlowAlgorithmExecutor;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmException;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Util třída pro práci s datovými toky.
 * @author tfechtner
 */
public final class GraphFlow {

    private GraphFlow() {
    }

    /**
     * Zjistí jestli má daný uzel souseda patřícího do referenčního view.
     * @param node zkoumaný uzel
     * @param flowState stav daného flow
     * @return true, jestliže má alespoň jednoho souseda v rámci referenčního view
     */
    public static boolean hasAdjacentVerticesInRefView(Vertex node, FlowState flowState) {
        List<Vertex> vertexList = getAdjacentVerticesInFlow(node, flowState.getRevisionInterval(),
                flowState.isFilterEdgesShown());
        for (Vertex actualVertex : vertexList) {
            if (flowState.getNodeStatus(actualVertex) != NodeStatus.UNKNOWN) {
                return true;
            }
        }

        return false;
    }

    /**
     * Získá iterátor na sousedy daného uzlu přes přímé a případně i filtr hrany. 
     * @param vertex vrchol, ke kterému jsou hledáni sousedi
     * @param isFilterEdgesShown jestli se mají hledat sousedi i přes filtr hrany
     * @param revisionInterval interval revizí, pro které se operace provádí
     * @return iterátor na sousedy, nikdy null
     */
    public static List<Vertex> getAdjacentVerticesInFlow(Vertex vertex, RevisionInterval revisionInterval,
            boolean isFilterEdgesShown) {
        EdgeLabel[] labels = GraphOperation.getDataflowLabels(isFilterEdgesShown);
        return GraphOperation.getAdjacentVertices(vertex, Direction.BOTH, revisionInterval, labels);
    }

    /**
     * Naplní vsechny uzly ktere jsou dostupne ze seznamu vychozich 
     * uzlu - <code>components</code>. Zaroven
     * uzly ze kterych je dostupny nektery z vychozich uzlu.     
     * 
     * @param startNodes seznam uzlu, ze kterych vychazime
     * @param dbTransaction transakce pro přístup k db
     * @param flowState stav daného flow, vvstupně/výstupní parametr
     * @param direction směr flow, pro které se tvoří referenční flow
     * @param isFilterEdgesShown true, jestliže jsou zobrazovány filter hrany
     * @param executor spouštěč algoritmů 
     * @throws GraphFlowAlgorithmException chyba při provádění výpočtu v algoritmu
     */
    public static void fillReferenceView(Collection<Vertex> startNodes, TitanTransaction dbTransaction,
            FlowState flowState, Direction direction, boolean isFilterEdgesShown, GraphFlowAlgorithmExecutor executor)
                    throws GraphFlowAlgorithmException {

        // nalezt direct flow
        Set<Object> foundNodeIds = executor.findAccessibleVertexIds(startNodes, dbTransaction, direction,
                isFilterEdgesShown, flowState.getRevisionInterval());

        // naplnit reference view
        for (Object nodeId : foundNodeIds) {
            // titan používá jako id uzlů longy
            flowState.addNode((Long) nodeId);
        }
    }

    /**
     * Vytvori novou instanci {@link Iterable}, ktera iteruje pres ID uzlu
     * v {@code vertices}.
     * 
     * @param vertices uzly, pres jejichz ID bude vytvoren {@link Iterable}
     * @return nova instance {@link Iterable} iterujici pres ID uzlu v {@code vertices}
     */
    public static Iterable<Object> getVertexIdIterator(final Iterable<Vertex> vertices) {
        return new Iterable<Object>() {

            @Override
            public Iterator<Object> iterator() {
                final Iterator<Vertex> vertexIterator = vertices.iterator();

                return new Iterator<Object>() {

                    @Override
                    public boolean hasNext() {
                        return vertexIterator.hasNext();
                    }

                    @Override
                    public Object next() {
                        return vertexIterator.next().getId();
                    }

                    @Override
                    public void remove() {
                        vertexIterator.remove();
                    }

                };
            }

        };
    }
}
