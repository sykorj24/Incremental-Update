package eu.profinit.manta.dataflow.repository.connector.titan.service;


/**
 * Interface pro operace prováděné v rámci zamčení revizí. 
 * @author tfechtner
 *
 * @param <T> typ výstupu operace
 */
public interface RevisionLockedOperation<T> {
    
    /**
     * Provedení příslušné operace se zámkem.
     * @return výsledek operace
     */
    T call();
}
