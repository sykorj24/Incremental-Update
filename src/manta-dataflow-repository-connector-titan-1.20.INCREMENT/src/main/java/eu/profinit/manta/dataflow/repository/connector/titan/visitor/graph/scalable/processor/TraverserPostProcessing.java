package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor;

/**
 * Rozhraní pro traverserery, které mají ještě postprocessing.
 * Tento prostprocessing se volá až po commitu práce traverseru.
 * @author tfechtner
 *
 */
public interface TraverserPostProcessing {
    /**
     * Provede postprocessing traverseru po commitu.
     */
    void postProcess();
}
