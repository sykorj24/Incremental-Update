package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

/**
 * Anotace pro třídy, které nejsou nikde v existujícím kódu nikdy instancovány,
 * a pro metody, které nejsou v existujícím kódu nikdy volány.
 * 
 * Anotaci lze považovat za "prekurzor" k anotaci @Deprecated
 * 
 * @author Erik Kratochvíl
 */
public @interface Unused {

    /** Jméno člověka, který na nepoužívaný konstrukt upozorňuje. */
    String author() default "n/a";
}
