package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import java.util.Set;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;

/**
 * podmínku splňují takové vrcholy, jejich předek má typ z přednastavené množiny.
 * @author tfechtner
 *
 */
public class ParentTypePredicate implements VertexPredicate {

    /** Množina povolených typů. */
    private Set<String> typeSet;

    /**
     * Default konstruktor, pro správné fungování je ještě třeba nastvit field {@link #typeSet}.
     */
    public ParentTypePredicate() {
        super();
    }

    /**
     * @param typeSet množina typů splňující podmínku
     */
    public ParentTypePredicate(Set<String> typeSet) {
        super();
        this.typeSet = typeSet;
    }
    
    @Override
    public boolean evaluate(Vertex vertex, RevisionInterval revisionInterval) {
        if (typeSet == null) {
            throw new IllegalStateException("The type set must not be null.");
        }
        
        Vertex parentVertex = GraphOperation.getParent(vertex);
        if (parentVertex != null) {
            String parentVertexType = parentVertex.getProperty(NodeProperty.NODE_TYPE.t());
            return typeSet.contains(parentVertexType);     
        } else {                
            return false;
        }
    }

    /**
     * @param typeSet Množina povolených typů.
     */
    public void setTypeSet(Set<String> typeSet) {
        this.typeSet = typeSet;
    }
}
