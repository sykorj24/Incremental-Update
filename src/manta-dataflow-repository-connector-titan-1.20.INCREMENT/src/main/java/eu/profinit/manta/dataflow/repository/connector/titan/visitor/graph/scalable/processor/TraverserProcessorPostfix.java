package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.VisitedPart;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;

/**
 * Procesor zpracovávající elementy postfixově.
 * Nejprve znovu přidá do fronty sám sebe a za to "podřízené" elementy. <br>
 * Díky tomu se po zpracování jeho podřízených dostane na řadu opět on sám. <br>
 * <b>!!!POZOR, pracuje pouze ve stylu {@link SearchOrder#DFS}!!! </b>
 * @author tfechtner
 *
 */
public class TraverserProcessorPostfix extends AbstractTraverserProcessor<Object> {
    /** Množina id vrcholů, které už jsme jednou potkali a mají se tedy příště navštívit.*/
    private final Set<Object> verticesToVisit = new HashSet<Object>();

    @Override
    protected void processResource(Vertex vertex) {
        Object actualId = vertex.getId();
        if (verticesToVisit.remove(actualId)) {
            processLayerOfResource(vertex);
            getVisitor().visitResource(vertex);
        } else {
            getIdsToProcess().addLast(actualId);

            List<Vertex> children = GraphOperation.getAdjacentVertices(vertex, Direction.IN, getRevisionInterval(),
                    EdgeLabel.HAS_RESOURCE);
            for (Vertex child : children) {
                if (GraphOperation.getParent(child) == null) {
                    getIdsToProcess().addLast(child.getId());
                }
            }
            verticesToVisit.add(actualId);
        }
    }

    @Override
    protected void processNode(Vertex vertex) {
        Object actualId = vertex.getId();
        if (verticesToVisit.remove(actualId)) {

            if (getVisitedParts().contains(VisitedPart.EDGES)) {
                List<Edge> edges = GraphOperation.getAdjacentEdges(vertex, Direction.OUT, getRevisionInterval(),
                        EdgeLabel.DIRECT, EdgeLabel.FILTER);
                for (Edge e : edges) {
                    getVisitor().visitEdge(e);
                }
            }

            getVisitor().visitNode(vertex);
        } else {
            getIdsToProcess().addLast(actualId);

            if (getVisitedParts().contains(VisitedPart.ATTRIBUTES)) {
                List<Vertex> attributes = GraphOperation.getAdjacentVertices(vertex, Direction.OUT,
                        getRevisionInterval(), EdgeLabel.HAS_ATTRIBUTE);
                for (Vertex attr : attributes) {
                    getIdsToProcess().addLast(attr.getId());
                }
            }

            List<Vertex> children = GraphOperation.getAdjacentVertices(vertex, Direction.IN, getRevisionInterval(),
                    EdgeLabel.HAS_PARENT);
            for (Vertex child : children) {
                getIdsToProcess().addLast(child.getId());
            }
            verticesToVisit.add(actualId);
        }
    }
}
