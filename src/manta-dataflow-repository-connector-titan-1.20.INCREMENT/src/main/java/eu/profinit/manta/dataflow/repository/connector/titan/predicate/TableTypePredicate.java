package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import java.util.Set;

/**
 * Evaluátor kontrolující podmínku, že uzel je sloupec v tabulce 
 * a navíc je má tato tabulka typ (atribut TABLE_TYPE) nacházející se v dané množině přípustných hodnot.
 * V případě, že má tabulka více typů, tak stačí aby alespoň jeden se rovnal některé chtěné hodnotě.
 * @author tfechtner
 *
 */
public class TableTypePredicate extends TableAttributePredicate {
    /** Název atributu obsahující typ tabulky.  */
    private static final String TABLE_TYPE_ATTRIBUTE_KEY = "TABLE_TYPE";

    /**
     * Default konstruktor, pro správné fungování je ještě třeba nastvit field {@link #typeSet}.
     */
    public TableTypePredicate() {
        super();
        setAttributeKey(TABLE_TYPE_ATTRIBUTE_KEY);
    }

    /**
     * @param typeSet množina typů splňující podmínku
     */
    public TableTypePredicate(Set<String> typeSet) {
        super(typeSet, TABLE_TYPE_ATTRIBUTE_KEY);
    }
}
