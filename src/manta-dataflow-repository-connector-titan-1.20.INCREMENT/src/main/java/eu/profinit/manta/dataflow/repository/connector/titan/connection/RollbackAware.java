package eu.profinit.manta.dataflow.repository.connector.titan.connection;

import com.thinkaurelius.titan.core.TitanTransaction;

/**
 * Rozhraní identifikující objekt, který je schopen reagovat na rollback v transakci.
 * @author tfechtner
 *
 * @param <T> návratový typ z operace řešící rollback
 */
public interface RollbackAware<T> {

    /**
     * Operace reagující na rollback.
     * @param transaction transakce pro přístup k db
     * @return návratová hodnota z dané operace
     */
    T rollback(TitanTransaction transaction);
}
