package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowTraverserFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.VisitedPart;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrder;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Generická továrna na {@link TraverserProcessor}.
 * @author tfechtner
 *
 * @param <T> typ procesoru, který se má vytvořit
 */
public class TraverserProcessorFactoryGeneric<T extends TraverserProcessor<?>> implements TraverserProcessorFactory {

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(FlowTraverserFactoryGeneric.class);
    /** Výchozí počet objektů, lteré se mají zpracovat v rámci jednoho procesoru. */
    public static final int DEFAULT_OBJECTS_PER_TRAN = 2000;
    /** Výchozí seznam navštěvovaných objektů. */
    public static final List<VisitedPart> DEFAULT_VISITED_PARTS;

    static {
        List<VisitedPart> defaultParts = new ArrayList<>();
        defaultParts.add(VisitedPart.SELF);
        defaultParts.add(VisitedPart.SUCCESSORS);
        defaultParts.add(VisitedPart.ATTRIBUTES);
        defaultParts.add(VisitedPart.EDGES);
        DEFAULT_VISITED_PARTS = Collections.unmodifiableList(defaultParts);
    }

    /** Typ třídy k vytváření. */
    private final Class<T> traverserClass;
    /** Název modelu pro kontrolu licence v rámci databáze. */
    private final String moduleName;
    /** Počet objektů, které se mají zpracovat v rámci jednoho procesoru. */
    private final int objectsPerTransaction;
    /** Styl prohledávání grafu. */
    private final SearchOrder searchOrder;
    /** Navštěvované objekty. */
    private final List<VisitedPart> visitedParts;

    /**
     * @param traverserClass Typ třídy k vytváření.
     * @param moduleName Název modulu pro kontrolu licence v rámci databáze.
     * @param searchOrder Styl prohledávání grafu
     */
    public TraverserProcessorFactoryGeneric(Class<T> traverserClass, String moduleName,
            SearchOrder searchOrder) {
        this(traverserClass, moduleName, DEFAULT_OBJECTS_PER_TRAN, searchOrder);
    }
    
    /**
     * @param traverserClass Typ třídy k vytváření.
     * @param moduleName Název modelu pro kontrolu licence v rámci databáze.
     * @param objectsPerTransaction Počet objektů, které se mají zpracovat v rámci jednoho procesoru.
     * @param searchOrder Styl prohledávání grafu
     */
    public TraverserProcessorFactoryGeneric(Class<T> traverserClass, String moduleName, int objectsPerTransaction,
            SearchOrder searchOrder) {
        this(traverserClass, moduleName, objectsPerTransaction, searchOrder, DEFAULT_VISITED_PARTS);
    }

    /**
     * @param traverserClass Typ třídy k vytváření.
     * @param moduleName Název modelu pro kontrolu licence v rámci databáze.
     * @param objectsPerTransaction Počet objektů, které se mají zpracovat v rámci jednoho procesoru.
     * @param searchOrder Styl prohledávání grafu
     * @param visitedParts navštěvované objekty
     */
    public TraverserProcessorFactoryGeneric(Class<T> traverserClass, String moduleName, int objectsPerTransaction,
            SearchOrder searchOrder, List<VisitedPart> visitedParts) {
        super();
        this.traverserClass = traverserClass;
        this.moduleName = moduleName;
        this.objectsPerTransaction = objectsPerTransaction;
        this.searchOrder = searchOrder;
        if (visitedParts != null) {
            this.visitedParts = Collections.unmodifiableList(new ArrayList<>(visitedParts));
        } else {
            this.visitedParts = Collections.emptyList();
        }
    }

    @Override
    public TraverserProcessor<?> createTraverser(GraphVisitor visitor, RevisionInterval revisionInterval,
            Deque<Object> idsToProcess) {
        
        try {
            T newInstance =  traverserClass.newInstance();
            newInstance.setIdsToProcess(idsToProcess);
            newInstance.setRevisionInterval(revisionInterval);
            newInstance.setVisitor(visitor);
            
            newInstance.setModuleName(moduleName);
            newInstance.setObjectsPerTransaction(objectsPerTransaction);
            newInstance.setSearchOrder(searchOrder);
            newInstance.setVisitedParts(visitedParts);
                        
            return newInstance;
        } catch (InstantiationException e) {
            LOGGER.error("Class " + traverserClass + " cannot be instantiated.", e);
            return null;
        } catch (IllegalAccessException e) {
            LOGGER.error("The constructor for the class " + traverserClass + " is not accessible.", e);
            return null;
        }
    }

}
