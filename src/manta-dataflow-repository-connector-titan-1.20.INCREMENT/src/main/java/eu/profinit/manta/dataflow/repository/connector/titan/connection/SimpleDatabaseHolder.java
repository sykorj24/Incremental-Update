package eu.profinit.manta.dataflow.repository.connector.titan.connection;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.PriorityOrdered;

import com.persistit.exception.RollbackException;
import com.thinkaurelius.titan.core.TitanException;
import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.connector.titan.ContextEventAwareOrder;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseConfiguration;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.platform.automation.licensing.LicenseException;
import eu.profinit.manta.platform.licensing.LicenseHolder;
import eu.profinit.manta.platform.scriptmetadata.service.ScriptMetadataService;
import eu.profinit.manta.platform.scriptprocessing.model.ScriptMetadataConfig;

/**
 * Jednoduchý holder udržující jednu jedinou instanci databáze.
 * @author tfechtner
 *
 */
public class SimpleDatabaseHolder
        implements DatabaseHolder, ApplicationListener<ContextRefreshedEvent>, PriorityOrdered {
    /** Délka sleepu v milisekundách mezi jednotlivými pokusy o smazání databáze. */
    private static final int SLEEP_BETWEEN_DELETE_TRY = 200;
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleDatabaseHolder.class);
    /** Klíč k jediné databázi. */
    private static final String PRIMARY = "primary";
    /** Maxim8lní počet pokusů o smazání databáze. */
    private static final int MAX_DELETE_TRY = 10;
    /** Umístění db properties.*/
    private static final String PROPERTY_FILE_NAME = "repository.properties";

    /** výchozí konfigurace. */
    private Configuration defaultConfiguration;
    /** Umístění databáze. */
    private String dir;
    /** R/W zámek pro zajištění správnosti přístupu. */
    private ReentrantReadWriteLock dbLock = new ReentrantReadWriteLock();
    /** R/W zámek pro zajištění správnosti přístupu pro práci se source code. */
    private ReadWriteLock dbLockSourceCode = new ReentrantReadWriteLock();
    /** Helper pro načtení licence. */
    private LicenseHolder licenseHolder;
    /** Konfigurace této instance konfigurace, jako například použití indexů apod. */
    private DatabaseConfiguration configuration;

    @Autowired
    private RevisionRootHandler revisionRootHandler;
    @Autowired
    private SuperRootHandler superRootHandler;
    @Autowired
    private SourceRootHandler sourceRootHandler;
    /** Sluzba pro praci s metadaty skriptu. */
    @Autowired
    private ScriptMetadataService scriptMetadataService;

    @Override
    public <T> T runInTransaction(TransactionLevel level, TransactionCallback<T> callback) {
        if (!validateLicense(callback)) {
            throw new LicenseException("Insufficent license for this metadata repository module.");
        }
        if (!validateScriptCount()) {
            int limit = licenseHolder.getLicense().getMaxScriptCount();
            int count = scriptMetadataService.countCommittedScripts(ScriptMetadataConfig.LAST_SCRIPT_COUNT_PERIOD, null);
            throw new LicenseException(MessageFormat.format("Maximal script count exceeded. The limit is {0} but the actual count is {1}.", limit, count));
        }
        if (defaultConfiguration == null) {
            throw new IllegalStateException("Init method must be called first.");
        }

        Lock lock = getLock(level);
        TitanTransaction transaction = null;
        try {
            if (lock != null) {
                lock.lock();
            }
            transaction = getTransaction(level);
        } catch (Throwable e) {
            System.err.println("Error during creating transaction - " + e.getMessage() + ".");
            LOGGER.error("Error during creating transaction.", e);
            if (lock != null) {
                lock.unlock();
            }
            throw e;
        }
        return executeCallback(callback, transaction, lock);
    }

    /**
     * Zvaliduje pocet zpracovanych skriptu vuci licenci.
     * @return {@code true} pokud je pocet zpracovanych skriptu vuci licenci validni, jinak {@code false}. 
     */
    private boolean validateScriptCount() {
        int lastStoredScriptCount = scriptMetadataService.countCommittedScripts(
                ScriptMetadataConfig.LAST_SCRIPT_COUNT_PERIOD, null);
        return licenseHolder.getLicense().isValidScriptCount(
                lastStoredScriptCount, ScriptMetadataConfig.SCRIPT_COUNT_EXCEED_TOLERANCE);
    }

    /**
     * Získá transakci pro danou úroveň přístupu.
     * @param level požadovaná úroveň přístupu 
     * @return příslušná transakce pro danný přístup
     */
    private TitanTransaction getTransaction(TransactionLevel level) {
        switch (level) {
        case READ:
        case READ_NOT_BLOCKING:
            return getReadTransaction();
        case WRITE_EXCLUSIVE:
        case WRITE_SHARE:
        case WRITE_SOURCE_CODE:
            return getWriteTransaction();
        default:
            throw new IllegalArgumentException("Unknown transaction level " + level + ".");
        }
    }

    /**
     * Získá zámek pro danou úroveň přístupu.
     * @param level požadovaná úroveň přístupu 
     * @return příslušný zámek pro danný přístup
     */
    private Lock getLock(TransactionLevel level) {
        switch (level) {
        case READ:
            return dbLock.readLock();
        case READ_NOT_BLOCKING:
            return null;
        case WRITE_EXCLUSIVE:
            return dbLock.writeLock();
        case WRITE_SHARE:
            return dbLock.readLock();
        case WRITE_SOURCE_CODE:
            return dbLockSourceCode.writeLock();
        default:
            throw new IllegalArgumentException("Unknown transaction level " + level + ".");
        }
    }

    /**
     * Zvaliduje, jestli licence podporuje modul daného kódu. 
     * @param callback kontrolovaný kód 
     * @return true, jestliže je licence v pořádku
     */
    private boolean validateLicense(TransactionCallback<?> callback) {
        String moduleName = callback.getModuleName();
        if (moduleName != null) {
            return licenseHolder.getLicense().isValidProduct(moduleName);
        } else {
            return false;
        }
    }

    /**
     * Provede call back v dané transakci. <br>
     * Transakci po skončení korektně uzavře, při chybě zaloguje a přehodí výjimku. 
     * @param callback provádený callback kód
     * @param transaction transakce, ve které se kód vykonává
     * @param lock instance zámku použitého pro přístup k db
     * @return
     */
    private <T> T executeCallback(TransactionCallback<T> callback, TitanTransaction transaction, Lock lock) {
        T response = null;

        try {
            response = callback.callMe(transaction);
            transaction.commit();
        } catch (UnsupportedOperationException e) {
            transaction.rollback();
            throw e;
        } catch (TitanException e) {
            if (e.getCause() instanceof TitanException && e.getCause().getCause() instanceof RollbackException) {
                throw new RepositoryRollbackException(e);
            }
            LOGGER.error("Error during transaction.", e);
            if (transaction != null && transaction.isOpen()) {
                transaction.rollback();
            }
            throw e;
        } catch (Throwable e) {
            System.err.println("Error during transaction - " + e.getMessage() + ".");
            LOGGER.error("Error during transaction.", e);
            if (transaction != null && transaction.isOpen()) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }

        return response;
    }

    /**
     * Získá zapisovací zámek a vytvoří transakci pro R/W operace.
     * @return transakce pro R/W operace
     */
    private TitanTransaction getWriteTransaction() {
        return DatabaseProvider.INSTANCE.getDatabase(PRIMARY, defaultConfiguration).newTransaction();
    }

    /**
     * Získá čtecí zámek a vytvoří čtecí transakci.
     * @return transakce pro čtení
     */
    private TitanTransaction getReadTransaction() {
        return DatabaseProvider.INSTANCE.getDatabase(PRIMARY, defaultConfiguration).buildTransaction().readOnly()
                .start();
    }

    @Override
    public void init() {
        if (dir == null) {
            throw new IllegalStateException("Dir must be set before init.");
        }

        defaultConfiguration = new BaseConfiguration();
        defaultConfiguration.setProperty("storage.directory", dir);

        Properties properties = new Properties();
        try {
            InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(PROPERTY_FILE_NAME);
            if (resourceAsStream == null) {
                throw new IllegalStateException("File with repository properties does not exist.");
            }
            properties.load(resourceAsStream);
        } catch (IOException e) {
            throw new IllegalStateException("File with repository properties cannot be readed.", e);
        }
        Set<String> names = properties.stringPropertyNames();
        for (String key : names) {
            defaultConfiguration.setProperty(key, properties.getProperty(key));
        }

        defaultConfiguration.setProperty("storage.index.search.backend", "lucene");
        defaultConfiguration.setProperty("storage.index.search.directory", dir + "/searchindex");

        // Doplneni custom serializeru:        
        defaultConfiguration.setProperty("attributes.attribute1",
                "eu.profinit.manta.dataflow.model.restriction.nodetypes.RestrDisjunctionTypeDnf");
        defaultConfiguration.setProperty("attributes.serializer1", "eu.profinit.manta.dataflow.repository.connector"
                + ".titan.service.dataflow.algorithm.restriction.RestrPropositionalNodeSerializer");
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (superRootHandler != null && revisionRootHandler != null) {
            LOGGER.info("Checking status of the metadata repository.");
            try {
                GraphCreation.initDatabase(this, superRootHandler, revisionRootHandler, sourceRootHandler);
            } catch (Exception e) {
                LOGGER.error("The metadata repository initialization failed.", e);
            }
        } else if (superRootHandler == null) {
            LOGGER.error("Cannot initialize metadata repository, the super root handler is null.");
        } else {
            LOGGER.error("Cannot initialize metadata repository, the revision root handler is null.");
        }
    }

    /**
     * @param defaultConfiguration konfigurace databáze
     */
    protected void setDefaultConfiguration(Configuration defaultConfiguration) {
        this.defaultConfiguration = defaultConfiguration;
    }

    /**
     * Zruší instanci database holderu, což znamená, že zavře databázi.
     */
    public void destroy() {
        // výpis rovnou na výstup, protože log framework už může být dealokovaný.
        System.out.println("Destroying SimpleDatabaseHolder instance.");
        closeDatabase();
    }

    @Override
    public void closeDatabase() {
        DatabaseProvider.INSTANCE.closeDatabase(PRIMARY);
    }

    @Override
    public void truncateDatabase() {
        Lock lock = getLock(TransactionLevel.WRITE_EXCLUSIVE);
        try {
            lock.lock();
            DatabaseProvider.INSTANCE.closeAndEraseDatabase(PRIMARY);

            File dbFile = new File(dir);
            if (dbFile.isDirectory()) {
                int i = 0;
                while (dbFile.exists() && !FileUtils.deleteQuietly(dbFile)) {
                    if (++i > MAX_DELETE_TRY) {
                        LOGGER.error("Cannot delete directory with database.");
                        break;
                    }
                    try {
                        Thread.sleep(SLEEP_BETWEEN_DELETE_TRY);
                    } catch (InterruptedException e) {
                        LOGGER.error("Interruption during sleep between trying of deleteing db directory.");
                        break;
                    }
                }
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * @param dir Umístění databáze. 
     */
    public void setDir(String dir) {
        this.dir = dir;
    }

    /**
     * @param licenseHolder držák na licenci
     */
    public void setLicenseHolder(LicenseHolder licenseHolder) {
        this.licenseHolder = licenseHolder;
    }

    @Override
    public DatabaseConfiguration getConfiguration() {
        return configuration;
    }

    /**
     * @param configuration Konfigurace této instance konfigurace, jako například použití indexů apod. 
     */
    public void setConfiguration(DatabaseConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * @return držák na revision root
     */
    public RevisionRootHandler getRevisionRootHandler() {
        return revisionRootHandler;
    }

    /**
     * @param revisionRootHandler držák na revision root
     */
    public void setRevisionRootHandler(RevisionRootHandler revisionRootHandler) {
        this.revisionRootHandler = revisionRootHandler;
    }

    /**
     * @return držák na super root
     */
    public SuperRootHandler getSuperRootHandler() {
        return superRootHandler;
    }

    /**
     * @param superRootHandler držák na super root
     */
    public void setSuperRootHandler(SuperRootHandler superRootHandler) {
        this.superRootHandler = superRootHandler;
    }

    /**
     * @param scriptMetadataService Sluzba pro praci s metadaty skriptu.
     */
    public void setScriptMetadataService(ScriptMetadataService scriptMetadataService) {
        this.scriptMetadataService = scriptMetadataService;        
    }

    @Override
    public int getOrder() {
        return ContextEventAwareOrder.DATABASE_HOLDER.getOrder();
    }
}
