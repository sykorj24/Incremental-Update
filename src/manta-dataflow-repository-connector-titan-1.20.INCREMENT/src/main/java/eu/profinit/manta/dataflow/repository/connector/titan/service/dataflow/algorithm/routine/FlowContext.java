package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.routine;

import com.tinkerpop.blueprints.Edge;

/**
 * Položka fronty ke zpracování.
 * 
 * Obsahuje kontext volání (položku pro visitor) a hranu, po které jsme přišli.
 * 
 * @author rpolach
 */
class FlowContext {
    
    /** Položka k navštívení. */
    private final CallContext callContext;
    
    /** Hrana, po které jsme přišli. */
    private final Edge edge;

    /**
     * @param callContex Položka k navštívení.
     * @param edge Hrana, po které jsme přišli.
     */
    FlowContext(CallContext callContex, Edge edge) {
        this.callContext = callContex;
        this.edge = edge;
    }

    /**
     * @return Položka k navštívení.
     */
    public CallContext getCallContext() {
        return callContext;
    }

    /**
     * @return Hrana, po které jsme přišli.
     */
    public Edge getEdge() {
        return edge;
    }

}