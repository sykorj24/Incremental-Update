package eu.profinit.manta.dataflow.repository.connector.titan.service.collection;

import com.thinkaurelius.titan.core.TitanTransaction;

/**
 * Callback pro zpracování kolekcí v databázové transakci.
 * @author tfechtner
 *
 * @param <E> typ elementu
 * @param <R> typ výsledku
 */
public interface CollectionProcessorCallback<E, R> {

    /**
     * Zpracuje element.
     * @param titanTransaction transakce pro přístup k databázi
     * @param element element ke zpracování
     * @param resultHolder třída pro zaznamenávání výsledků zpracování
     */
    void processElement(TitanTransaction titanTransaction, E element, R resultHolder);
}
