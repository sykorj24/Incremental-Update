package eu.profinit.manta.dataflow.repository.connector.titan.service.sourcecode;

import java.util.List;

/**
 * Rozhrani komponenty pro nacitani souboru zdrojoveho kodu.
 * 
 * @author onouza
 *
 */
public interface SourceFileLoader {

    /**
     * Nacte soubor zdrojoveho kodu z daneho umisteni.
     * Vysledkem je seznam radku nacteneho souboru.
     * Pokud pri nacitani dojde k chybe, vrati {@code null}.
     * 
     * @param sourceFile Umisteni souboru zdrojoveho kodu.
     * 
     * @return Soubor zdrojoveho kodu nebo {@code null}, pokud pri nacitani doslo k chybe.  
     */
    List<String> loadSourceFile(SourceFile sourceFile);

}
