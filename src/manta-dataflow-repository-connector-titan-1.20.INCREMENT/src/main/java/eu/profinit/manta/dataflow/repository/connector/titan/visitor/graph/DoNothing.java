package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Visitor, který nedělá vůbec nic.
 * 
 * @author Erik Kratochvíl
 */
public class DoNothing implements UniversalVisitor {

    @Override
    public boolean visit(final Vertex vertex, final long distance, final boolean visitedForTheFirstTime,
            RevisionInterval revisionInterval) {
        // Nic.
        return true;
    }

    @Override
    public boolean visit(final Edge edge, final EdgeLabel label, final Direction direction,
            RevisionInterval revisionInterval) {
        // Nic.
        return true;
    }

}
