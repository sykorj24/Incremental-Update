package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.collector;

import com.tinkerpop.blueprints.Vertex;

/**
 * Rozhraní pro filtrování uzlů ve třídě {@link CollectVerticesVisitor}.
 * @author tfechtner
 *
 */
public interface CollectingFilter {

    /** Vrací, jestliže má být daný uzel sebrán.
     * @param node dotazovaný uzel
     * @return true, jestliže je filt splněn
     */
    boolean shouldCollect(Vertex node);
}
