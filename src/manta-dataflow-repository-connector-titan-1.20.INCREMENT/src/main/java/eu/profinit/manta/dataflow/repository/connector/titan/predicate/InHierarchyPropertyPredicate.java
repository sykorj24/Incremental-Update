package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;

/**
 * Evaluátor kontrolující podmínku, že někde v hierarchii testovaného vrcholu je vrchol daného typu
 * s danou property mající danou hodnotu. Pro urychlení se nejprve kontroluje, jestli je resource daného 
 * vrcholu určitého typu.
 * @author tfechtner
 *
 */
public class InHierarchyPropertyPredicate implements VertexPredicate {
    /** Množina typů vrcholů, na kterých se hledá daná property. */
    private Set<String> vertexTypeSet = Collections.emptySet();
    /** Hledaná property. */
    private String propertyKey = "";
    /** Množina hodnot, z nichž některou property musí mít. */
    private Set<String> propertyValueSet = Collections.emptySet();

    @Override
    public boolean evaluate(final Vertex vertex, final RevisionInterval revisionInterval) {
        Vertex actualVertex = vertex;
        while (actualVertex != null) {
            String actualType = GraphOperation.getType(actualVertex);
            if (vertexTypeSet.contains(actualType)) {
                List<Object> attributes = GraphOperation.getNodeAttribute(actualVertex, propertyKey, revisionInterval);
                for (Object attr : attributes) {
                    if (propertyValueSet.contains(attr)) {
                        return true;
                    }
                }
            }

            actualVertex = GraphOperation.getParent(actualVertex);
        }

        return false;
    }
    
    /**
     * @param vertexTypeSet Množina typů vrcholů, na kterých se hledá daná property. 
     */
    public void setVertexTypeSet(Set<String> vertexTypeSet) {
        this.vertexTypeSet = vertexTypeSet;
    }

    /**
     * @param propertyKey Hledaná property. 
     */
    public void setPropertyKey(String propertyKey) {
        this.propertyKey = propertyKey;
    }

    /**
     * @param propertyValueSet Množina hodnot, z nichž některou property musí mít. 
     */
    public void setPropertyValueSet(Set<String> propertyValueSet) {
        this.propertyValueSet = propertyValueSet;
    }
    
    
}
