package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;

import eu.profinit.manta.dataflow.repository.core.model.EdgePredicate;

/**
 * Predikát propouštějící každou hranu.
 *
 * @author Erik Kratochvíl
 */
public final class TrueEdgePredicate implements EdgePredicate {

    /**
     * Sdílená instance predikátu.
     * Nastavena na public, třída je immutable a nejde rozbít.
     */
    public static final TrueEdgePredicate INSTANCE = new TrueEdgePredicate();

    /**
     * Privátní konstruktor singletonu.
     */
    private TrueEdgePredicate() {
    }

    /**
     * @return true, vždycky
     */
    @Override
    public boolean evaluate(final Edge edge, final Direction direction) {
        return true;
    }

    /**
     *
     * @return sdílená instance predikátu
     */
    public static TrueEdgePredicate getInstance() {
        return INSTANCE;
    }

}
