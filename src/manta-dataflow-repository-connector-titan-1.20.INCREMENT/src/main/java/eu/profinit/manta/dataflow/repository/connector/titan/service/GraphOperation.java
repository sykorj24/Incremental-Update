package eu.profinit.manta.dataflow.repository.connector.titan.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanIndexQuery.Result;
import com.thinkaurelius.titan.core.TitanTransaction;
import com.thinkaurelius.titan.core.TitanVertex;
import com.thinkaurelius.titan.core.TitanVertexQuery;
import com.thinkaurelius.titan.core.attribute.Cmp;
import com.thinkaurelius.titan.graphdb.relations.RelationIdentifier;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.EdgeIdentification;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.utils.NodeTypeDef;
import eu.profinit.manta.dataflow.repository.utils.OccuranceCountHolder;

/**
 * Service poskytující základní operace nad dataflow grafem.
 * @author tfechtner
 *
 */
public final class GraphOperation {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(GraphOperation.class);
    /** Maximální délka pro hodnotu na hraně property {@link EdgeProperty#CHILD_NAME}. */
    private static final int CHILD_NAME_MAXIMAL = 200;
    /** Pocet casti kvalifikovaneho nazvu (cesta k uzlu, typ uzlu, zdroj) */
    private static final int QUALIFIED_NAME_PARTS = 3;
    /** Decimal format with 6 decimal digits for printing */
    private static NumberFormat formatter = new DecimalFormat("#0.000000");

    private GraphOperation() {
    }

    /**
     * Získá předka uzlu.
     * @param node uzel, pro který se hledá předek
     * @return předek uzlu nebo null, pokud předka nemá
     */
    public static Vertex getParent(Vertex node) {
        if (node == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }

        Iterator<Vertex> parentIter = node.getVertices(Direction.OUT, EdgeLabel.HAS_PARENT.t()).iterator();
        if (parentIter.hasNext()) {
            return parentIter.next();
        } else {
            return null;
        }
    }

    /**
     * Vrati nejblizsi uzel pozadovaneho typu v hierarchii predku vstupniho uzlu.
     * Vstupni uzel je uvazovat take, cili pokud je vstupni uzel daneho typu, bude vracen on.
     * 
     * @param node Vstupni uzel
     * @param type Pozadovany typ
     * @return Nalezeny uzel nebo {@code null}, pokud se zadny nalezt nepodari
     */
    public static Vertex getClosestNodeByType(Vertex node, String type) {
        Validate.notNull(node, "'node' must not be null");
        Validate.notNull(type, "'type' must not be null");
        while (node != null) {
            if (type.equals(getType(node))) {
                return node;
            }
            node = getParent(node);
        }
        return null;
    }
    
    /**
     * Získá seznam hodnot konkrétního atributu uzlu.
     * @param node uzel, pro který se hledá atribut
     * @param key klíč atributu
     * @param revInterval itnerval revizí, pro který se operace provádí
     * @return list hodnot atributů, nikdy null
     */
    public static List<Object> getNodeAttribute(Vertex node, String key, RevisionInterval revInterval) {
        if (node == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }
        if (key == null) {
            throw new IllegalArgumentException("The argument key was null.");
        }
        if (revInterval == null) {
            throw new IllegalArgumentException("The argument revInterval must not be null.");
        }

        List<Object> resultList = new ArrayList<Object>();

        Iterable<Vertex> attributes = RevisionUtils.getAdjacentVertices(node, revInterval, Direction.OUT,
                EdgeLabel.HAS_ATTRIBUTE.t());
        for (Vertex attribute : attributes) {
            Object attribueName = attribute.getProperty(NodeProperty.ATTRIBUTE_NAME.t());
            if (key.equals(attribueName)) {
                resultList.add(attribute.getProperty(NodeProperty.ATTRIBUTE_VALUE.t()));
            }
        }

        return resultList;
    }

    /**
     * Získá hodnotu atributu pro daný uzel.
     * @param node uzel, pro který se hledá atribut
     * @param key klíč atributu
     * @param revInterval itnerval revizí, pro který se operace provádí
     * @return hodnota daného atributu, nebo null, pokud uzel daný atribut nemá
     * @throws IllegalStateException jestliže má uzel více atributů s daným klíčem
     */
    public static Object getFirstNodeAttribute(Vertex node, String key, RevisionInterval revInterval) {
        if (node == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }
        if (key == null) {
            throw new IllegalArgumentException("The argument key was null.");
        }
        if (revInterval == null) {
            throw new IllegalArgumentException("The argument revInterval must not be null.");
        }

        Object result = null;

        Iterable<Vertex> attributes = RevisionUtils.getAdjacentVertices(node, revInterval, Direction.OUT,
                EdgeLabel.HAS_ATTRIBUTE.t());
        for (Vertex attribute : attributes) {
            Object attribueName = attribute.getProperty(NodeProperty.ATTRIBUTE_NAME.t());
            if (key.equals(attribueName)) {
                if (result == null) {
                    result = attribute.getProperty(NodeProperty.ATTRIBUTE_VALUE.t());
                } else {
                    throw new IllegalStateException("The node " + node + " has more attributes with key " + key + ".");
                }
            }
        }

        return result;
    }

    /**
     * Vrátí klíč daného atributu.
     * @param attribute vrchol představující atribut
     * @return klíč atributu
     */
    public static String getAttributeKey(Vertex attribute) {
        if (VertexType.getType(attribute) != VertexType.ATTRIBUTE) {
            throw new IllegalStateException("Vertex {} isn't attribute.");
        }
        return attribute.getProperty(NodeProperty.ATTRIBUTE_NAME.t());
    }

    /**
     * Vrátí hodnotu daného atributu.
     * @param attribute vrchol představující atribut
     * @return hodnota atributu
     */
    public static Object getAttributeValue(Vertex attribute) {
        if (VertexType.getType(attribute) != VertexType.ATTRIBUTE) {
            throw new IllegalStateException("Vertex {} isn't attribute.");
        }
        return attribute.getProperty(NodeProperty.ATTRIBUTE_VALUE.t());
    }

    /**
     * Získá všechny atributy uzlu.
     * @param node uzel, pro který se hledají atributy
     * @param revInterval itnerval revizí, pro který se operace provádí
     * @return mapa atributů, nikdy null
     */
    public static Map<String, List<Object>> getAllNodeAttributes(Vertex node, RevisionInterval revInterval) {
        if (node == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }
        if (revInterval == null) {
            throw new IllegalArgumentException("The argument revInterval must not be null.");
        }

        Map<String, List<Object>> attributes = new HashMap<String, List<Object>>();

        Iterable<Vertex> attributeVertices = RevisionUtils.getAdjacentVertices(node, revInterval, Direction.OUT,
                EdgeLabel.HAS_ATTRIBUTE.t());
        for (Vertex attribute : attributeVertices) {
            String attributeName = attribute.getProperty(NodeProperty.ATTRIBUTE_NAME.t()).toString();
            Object attributeValue = attribute.getProperty(NodeProperty.ATTRIBUTE_VALUE.t());
            List<Object> savedAttributes = attributes.get(attributeName);
            if (savedAttributes == null) {
                savedAttributes = new ArrayList<Object>();
                attributes.put(attributeName, savedAttributes);
            }

            savedAttributes.add(attributeValue);
        }

        return attributes;
    }

    /**
     * Získá seznam vrcholů připojených k danému uzlu hranou s daným labelem. <br>
     * Směr {@link Direction#OUT} znamená hrany, které z výchozího uzlu odcházejí do výsledných/vracených uzlů.
     * @param node výchozí uzel
     * @param direction směr hran
     * @param revInterval interval revizi, v kterých se sousedi hledají
     * @param labels label prohledávaných hran
     * @return seznam uzlů odpovídajících podmínkám, nikdy null
     */
    public static List<Vertex> getAdjacentVertices(Vertex node, Direction direction, RevisionInterval revInterval,
            EdgeLabel... labels) {
        if (labels == null) {
            throw new IllegalArgumentException("The argument label was null.");
        }

        String[] stringLabels = new String[labels.length];
        for (int i = 0; i < labels.length; i++) {
            stringLabels[i] = labels[i].t();
        }

        return IteratorUtils
                .toList(RevisionUtils.getAdjacentVertices(node, revInterval, direction, stringLabels).iterator());
    }

    /**
     * Ziska seznam vrcholu v dane vrstve pripojenych k danemu uzlu hranou s danym labelem.
     * @param node Vychozi uzel
     * @param direction Smer hran
     * @param revInterval Interval revizi, ve kterem se sousedi hledaji
     * @param layerName Nazev vrstvy, ve ktere se sousedi hledaji
     * @param labels Label prohledavanych hran
     * @return Seznam uzlu vyhovujicich danym podminkam, nikdy {@code null}.
     */
    public static List<Vertex> getAdjacentVerticesInLayer(Vertex node, Direction direction, RevisionInterval revInterval,
            String layerName, EdgeLabel... labels) {
        if (layerName == null) {
            throw new IllegalArgumentException("The argument layerName was null.");
        }
        List<Vertex> adjacentVertices = getAdjacentVertices(node, direction, revInterval, labels);
        List<Vertex> adjacentVerticesInLayer = new ArrayList<>();
        for (Vertex adjacentVertex : adjacentVertices) {
            Vertex layer = getLayer(adjacentVertex);
            if (layer != null && layerName.equals(getName(layer))) {
                adjacentVerticesInLayer.add(adjacentVertex);
            }
        }
        return adjacentVertices;
    }
    
    /**
     * Získá seznam hran připojených k danému uzlu hranou s daným labelem. <br>
     * Směr {@link Direction#OUT} znamená hrany, které z výchozího uzlu odcházejí do výsledných/vracených uzlů.
     * @param node výchozí uzel
     * @param direction směr hran
     * @param revInterval interval revizi, v kterých se sousedi hledají
     * @param labels label prohledávaných hran
     * @return senzam hran odpovídajících podmínkám, nikdy null
     */
    public static List<Edge> getAdjacentEdges(Vertex node, Direction direction, RevisionInterval revInterval,
            EdgeLabel... labels) {
        if (labels == null) {
            throw new IllegalArgumentException("The argument label was null.");
        }

        String[] stringLabels = new String[labels.length];
        for (int i = 0; i < labels.length; i++) {
            stringLabels[i] = labels[i].t();
        }

        return IteratorUtils
                .toList(RevisionUtils.getAdjacentEdges(node, direction, revInterval, stringLabels).iterator());

    }

    /**
     * Získá seznam všech listových potomků daného uzlu. <br>
     * Potomci se berou ve smyslu hrany {@link EdgeLabel#HAS_PARENT}.
     * @param node uzel, pro který se hledají listy
     * @param revInterval itnerval revizí, pro který se operace provádí
     * @return seznam listů uzlu, nikdy null
     */
    public static List<Vertex> getAllLists(Vertex node, RevisionInterval revInterval) {
        if (node == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }
        if (revInterval == null) {
            throw new IllegalArgumentException("The argument revInterval must not be null.");
        }

        List<Vertex> listList = new ArrayList<Vertex>();

        /** Pokud uzel neexistuje v revizi, vyhodim vyjimku */
        if (!RevisionUtils.isVertexInRevisionInterval(node, revInterval)) {
            throw new IllegalStateException(
                    "The argument node does not exist in the desired revision " + revInterval.toString() + ".");
        }

        Deque<Vertex> nodeDeque = new LinkedList<Vertex>();
        nodeDeque.add(node);

        while (nodeDeque.size() > 0) {
            Vertex actualNode = nodeDeque.pollLast();

            List<Vertex> children = GraphOperation.getAdjacentVertices(actualNode, Direction.IN, revInterval,
                    EdgeLabel.HAS_PARENT);
            if (children.size() == 0) {
                listList.add(actualNode);
            } else {
                nodeDeque.addAll(children);
            }
        }

        return listList;
    }

    /**
     * Získá všchny uzly v podstromě daného uzlu definovaného vztahem {@link EdgeLabel#HAS_PARENT}.
     * @param rootNode kořen, pro který se hledá podstrom
     * @param revInterval interval revizí, pro který se operace provádí
     * @return seznam uzlů v podstromu, nikdy null
     */
    public static List<Vertex> getWholeSubtree(Vertex rootNode, RevisionInterval revInterval) {
        if (rootNode == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }
        if (revInterval == null) {
            throw new IllegalArgumentException("The argument revInterval must not be null.");
        }

        List<Vertex> subtreeList = new ArrayList<Vertex>();

        Boolean vertexIsRevisionValid = false;

        switch (VertexType.getType(rootNode)) {
        case NODE:
            vertexIsRevisionValid = RevisionUtils.isVertexInRevisionInterval(rootNode, revInterval);
            break;
        case RESOURCE:
            vertexIsRevisionValid = RevisionUtils.isVertexInRevisionInterval(rootNode, revInterval);
            break;
        case ATTRIBUTE:
            vertexIsRevisionValid = RevisionUtils.isVertexInRevisionInterval(rootNode, revInterval);
            break;
        case SUPER_ROOT:
            vertexIsRevisionValid = Boolean.TRUE;
            break;
        default:
            LOGGER.warn("Unknown item type " + VertexType.getType(rootNode) + ".");
            throw new IllegalArgumentException("Unknown node type.");
        }

        if (!vertexIsRevisionValid) {
            throw new IllegalStateException("The argument rootNode does not exist in given revision.");
        }

        Deque<Vertex> nodeDeque = new LinkedList<Vertex>();
        nodeDeque.add(rootNode);

        while (nodeDeque.size() > 0) {
            Vertex actualNode = nodeDeque.pollLast();
            if (!(actualNode.getId().equals(rootNode.getId()))) {
                subtreeList.add(actualNode);
            }

            List<Vertex> children = GraphOperation.getAdjacentVertices(actualNode, Direction.IN, revInterval,
                    EdgeLabel.HAS_PARENT);
            if (children.size() > 0) {
                nodeDeque.addAll(children);
            }
        }

        return subtreeList;
    }

    /**
     * Get direct children of a vertex. Can be applied on vertex types: node, resource, superRoot and sourceRoot.
     *  
     * @param rootNode uzel, pro který se potomci hledají
     * @param revInterval itnerval revizí, pro který se operace provádí
     * @return seznam potomků, nikdy null
     */
    public static List<Vertex> getDirectChildren(Vertex rootNode, RevisionInterval revInterval) {
        if (rootNode == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }
        if (revInterval == null) {
            throw new IllegalArgumentException("The argument revInterval must not be null.");
        }

        List<Vertex> children = new ArrayList<Vertex>();
        Iterable<Vertex> childIterable = null;

        VertexType type = VertexType.getType(rootNode);

        switch (type) {
        case NODE:
            childIterable = GraphOperation.getAdjacentVertices(rootNode, Direction.IN, revInterval,
                    EdgeLabel.HAS_PARENT);
            break;
        case RESOURCE:
        case SUPER_ROOT:
            childIterable = GraphOperation.getAdjacentVertices(rootNode, Direction.IN, revInterval,
                    EdgeLabel.HAS_RESOURCE);
            break;
        case ATTRIBUTE:
            // atribut potomky nemá
            break;
        case SOURCE_ROOT:
            childIterable = GraphOperation.getAdjacentVertices(rootNode, Direction.OUT, revInterval,
                    EdgeLabel.HAS_SOURCE);
            break;
        case SOURCE_NODE:
            // source node does not have children
            break;
        default:
            throw new IllegalArgumentException("Vertex " + rootNode + " has unknown type " + type + ".");
        }

        if (childIterable != null) {
            for (Vertex child : childIterable) {
                children.add(child);
            }
        }

        return children;
    }

    /**
     * Vrátí seznam všech předků uzlu v posloupnosti od nejbližšího. <br>
     * Resource se nebere jako předek.
     * @param node uzel, pro který se hledají předci
     * @return seznam setříděných předků, nikdy null
     */
    public static List<Vertex> getAllParent(Vertex node) {
        if (node == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }

        List<Vertex> parentList = new ArrayList<Vertex>();

        Vertex actualNode = node;
        while ((actualNode = GraphOperation.getParent(actualNode)) != null) {
            parentList.add(actualNode);
        }

        return parentList;
    }

    /**
     * Zjistí resource nejvyššího předka daného uzlue. <br>
     * Pokud uzel nemá předka, tak se výsledek rovná metodě {@link #getResource(Vertex)}
     * @param node uzel, pro který se hledá resource. Může být node nebo resource.
     * @return nalezený resource
     * @throw IllegalStateException jestliže uzel nemá resource
     * @throw IllegalArgumentException jestliže je vrchol null, super root nebo neznámý typ
     */
    public static Vertex getTopParentResource(Vertex node) {
        if (node == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }

        VertexType type = VertexType.getType(node);
        switch (type) {
        case SUPER_ROOT:
            throw new IllegalArgumentException("The argument vertex was super root.");
        case RESOURCE:
            return node;
        case NODE:
            Vertex actualNode = node;
            Vertex parent = null;
            while ((parent = GraphOperation.getParent(actualNode)) != null) {
                actualNode = parent;
            }

            Vertex resourceNode = getDirectResource(actualNode);
            if (resourceNode != null) {
                return resourceNode;
            } else {
                LOGGER.warn("Vertex " + node + " does not have resource.");
                throw new IllegalStateException("Vertex " + node + " does not have resource.");
            }
        case ATTRIBUTE:
            return getTopParentResource(getAttributeOwner(node));
        default:
            throw new IllegalArgumentException("Vertex " + node + " has unknwon type " + type + ".");
        }
    }

    /**
     * Zjistí resource pro daný node. <br>
     * Bere se první resource nalezený po cestě včetně takzvaných odboček.
     * @param node uzel, pro který se hledá resource. Může být node nebo resource.
     * @return nalezený resource
     * @throws IllegalStateException jestliže uzel nemá resource
     * @throws IllegalArgumentException jestliže je vrchol null, super root nebo neznámý typ
     */
    public static Vertex getResource(Vertex node) {
        if (node == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }

        VertexType type = VertexType.getType(node);
        switch (type) {
        case SUPER_ROOT:
            throw new IllegalArgumentException("The argument vertex was super root.");
        case RESOURCE:
            return node;
        case NODE:
            Vertex actualNode = node;
            Vertex resourceNode = null;

            do {
                resourceNode = getDirectResource(actualNode);
                if (resourceNode != null) {
                    break;
                }
                actualNode = GraphOperation.getParent(actualNode);
            } while (actualNode != null);

            if (resourceNode != null) {
                return resourceNode;
            } else {
                LOGGER.warn("Vertex " + node + " does not have resource.");
                throw new IllegalStateException("Vertex " + node + " does not have resource.");
            }
        case ATTRIBUTE:
            return getResource(getAttributeOwner(node));
        default:
            throw new IllegalArgumentException("Vertex " + node + " has unknwon type " + type + ".");
        }
    }
    
    /**
     * Zjisti vrstvu pro dany uzel.
     * Pokud je uzel na vstupu vrstva, bude vracena ona.
     * V ostatnich pripadech bude vracena vrstva, ve ktere je resource daneho uzlu zjisteny pomoci {@link #getResource(Vertex)}.
     * @param node Uzel, pro ktery se hleda vrstva. Muze byt vrstva, zdroj, uzel nebo atribut uzlu.
     * @return Nalezena vrstva. Nikdy {@code null}.
     * @throws IllegalStateException Jestlize uzel nema vrstvu.
     * @throws IllegalArgumentException Jestlize uzel neni vrstva, zdroj, uzel ani atribut uzlu.
     */
    public static Vertex getLayer(Vertex node) {
        Validate.notNull(node, "The argument 'node' was null.");
        VertexType type = VertexType.getType(node);
        switch (type) {
        case LAYER:
            return node;
        case RESOURCE:
            Vertex layer = getDirectLayer(node);
            if (layer != null) {
                return layer;
            } else {
                throw new IllegalStateException("Vertex '" + node + "' is not in any layer.");
            }
        case NODE:
            return getLayer(getResource(node));
        case ATTRIBUTE:
            return getLayer(getAttributeOwner(node));
        default:
            throw new IllegalArgumentException("Vertex " + node + " has unsupported type '" + type + "' for retrieving layer.");
        }
    }

    /**
     * Vrátí uzel, ke kterému patří daný atribut.
     * @param attributeVertex vertex atributu, pro nějž se hledá majitel
     * @return vlastnický uzel daného atributu, nikdy null
     * @throws IllegalArgumentException vrchol není atribut nebo je null
     * @throws IllegalStateException vrchol nemá právě jednoho vlastníka
     */
    public static Vertex getAttributeOwner(Vertex attributeVertex) {
        if (attributeVertex == null) {
            throw new IllegalArgumentException("The parameter is null.");
        }
        if (VertexType.getType(attributeVertex) != VertexType.ATTRIBUTE) {
            throw new IllegalArgumentException("The parameter " + attributeVertex + " is not attribute.");
        }

        Iterator<Vertex> iterOwners = attributeVertex.getVertices(Direction.IN, EdgeLabel.HAS_ATTRIBUTE.t()).iterator();
        if (iterOwners.hasNext()) {
            Vertex owner = iterOwners.next();
            if (!iterOwners.hasNext()) {
                return owner;
            } else {
                throw new IllegalStateException("The attribute " + attributeVertex + " has more than one owner.");
            }
        } else {
            throw new IllegalStateException("The attribute " + attributeVertex + " has not owner.");
        }
    }

    /**
     * Zjistí jestli daný uzel má přímé spojení na resource. <br>
     * Pokud ano, tak tento resource vrátí.
     * @param node zkoumaný uzel
     * @return vertex odpovídající přímému resource nebo null, pokud takový uzel neexistuje
     */
    private static Vertex getDirectResource(Vertex node) {
        Iterator<Vertex> resIter = node.getVertices(Direction.OUT, EdgeLabel.HAS_RESOURCE.t()).iterator();
        if (resIter.hasNext()) {
            return resIter.next();
        } else {
            return null;
        }
    }

    /**
     * Zjisti vrstvu, ve ktere je dany resource.
     * @param resource Zkoumany resource.
     * @return Vrstva, ve ktere je resource, nebo {@code null}, pokud neni v zadne.
     */
    private static Vertex getDirectLayer(Vertex resource) {
        Iterator<Vertex> layerIter = resource.getVertices(Direction.OUT, EdgeLabel.IN_LAYER.t()).iterator();
        if (layerIter.hasNext()) {
            return layerIter.next();
        } else {
            return null;
        }
    }

    /**
     * Zjistí jestli je daný uzel list grafu. Listem může být pouze a jenom {@link VertexType#NODE}.
     * @param node uzel, pro který se zjišťuje jestli je listem
     * @return true, jestliže je uzel list
     * 
     * FIXME a co revize?
     */
    public static boolean isList(Vertex node) {
        if (node == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }

        VertexType type = VertexType.getType(node);
        switch (type) {
        case SUPER_ROOT:
            return false;
        case RESOURCE:
            return false;
        case NODE:
            return !(node.getVertices(Direction.IN, EdgeLabel.HAS_PARENT.t()).iterator().hasNext());
        case ATTRIBUTE:
            return false;
        default:
            throw new IllegalArgumentException("Vertex " + node + " has unknwon type " + type + ".");
        }
    }

    /**
     * Zjistí, jestli je daný uzel list grafu.
     * 
     * Listem může být pouze a jenom {@link VertexType#NODE}.
     * 
     * @param node uzel, pro který se zjišťuje, jestli je listem
     * @param revisionInterval interval revizí, na které se metoda ptá
     * 
     * @return true, jestliže je uzel list
     */
    public static boolean isLeaf(Vertex node, RevisionInterval revisionInterval) {
    	if (node == null) {
    		throw new IllegalArgumentException("The argument vertex was null.");
    	}
    	
    	VertexType type = VertexType.getType(node);
    	switch (type) {
    	case SUPER_ROOT:
    		return false;
    	case RESOURCE:
    		return false;
    	case NODE:
    		return !(RevisionUtils.getAdjacentVertices(node, revisionInterval, Direction.IN, EdgeLabel.HAS_PARENT.t())
    				.iterator().hasNext());
    	case ATTRIBUTE:
    		return false;
    	default:
    		throw new IllegalArgumentException("Vertex " + node + " has unknwon type " + type + ".");
    	}
    }
    
    /**
     * Vrátí množinu labelů hran pro datový tok.
     * @param isFilterEdgesIncluded zahrnut i label pro filtr hrany
     * @return pole labelů hran pro datové toky
     */
    public static String[] getDataflowLabelAsStrings(boolean isFilterEdgesIncluded) {
        String[] labels;
        if (isFilterEdgesIncluded) {
            labels = new String[] { EdgeLabel.DIRECT.t(), EdgeLabel.FILTER.t() };
        } else {
            labels = new String[] { EdgeLabel.DIRECT.t() };
        }
        return labels;
    }

    /**
     * Vrátí množinu labelů hran pro datový tok.
     * @param isFilterEdgesIncluded zahrnut i label pro filtr hrany
     * @return pole labelů hran pro datové toky
     */
    public static EdgeLabel[] getDataflowLabels(boolean isFilterEdgesIncluded) {
        EdgeLabel[] labels;
        if (isFilterEdgesIncluded) {
            labels = new EdgeLabel[] { EdgeLabel.DIRECT, EdgeLabel.FILTER };
        } else {
            labels = new EdgeLabel[] { EdgeLabel.DIRECT };
        }
        return labels;
    }

    /**
     * Vrátí název vertexu podle jeho typu.
     * - node - jméno uzlu <br>
     * - resource - jméno resource <br>
     * - atribut uzlu - klíč atributu <br>
     * - super root - SUPER_ROOT
     * @param vertex vrchol, jehož typ se hledá
     * @return název vertexu
     * @throws IllegalArgumentException vrchol je null
     * @throws IllegalStateException neznámý typ vrcholu
     */
    public static String getName(Vertex vertex) {
        if (vertex == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }

        VertexType type = VertexType.getType(vertex);
        switch (type) {
        case LAYER:
            return vertex.getProperty(NodeProperty.LAYER_NAME.t());
        case NODE:
            return vertex.getProperty(NodeProperty.NODE_NAME.t());
        case RESOURCE:
            return vertex.getProperty(NodeProperty.RESOURCE_NAME.t());
        case ATTRIBUTE:
            return vertex.getProperty(NodeProperty.ATTRIBUTE_NAME.t());
        case SUPER_ROOT:
            return VertexType.SUPER_ROOT.toString();
        case REVISION_ROOT:
            return VertexType.REVISION_ROOT.toString();
        case REVISION_NODE:
            return "revision[" + formatter.format(vertex.getProperty(NodeProperty.REVISION_NODE_REVISION.t())) + "]";
        case SOURCE_ROOT:
            return VertexType.SOURCE_ROOT.toString();
        case SOURCE_NODE:
            return vertex.getProperty(NodeProperty.SOURCE_NODE_LOCAL.t());
        default:
            throw new IllegalStateException("Uknown type of vertex " + type + ".");
        }
    }

    /**
     * Nalezne vrcholy spojené s daným vrcholem příchozí hranou a mající hledané jméno.
     * Pro hledání se používá index přes {@link EdgeProperty.CHILD_NAME}.
     * @param vertex vrchol, jehož potomci se hledají
     * @param childrenName jméno hledaných potomků
     * @param revisionInterval interval revizí, na které se metoda ptá
     * @return seznam nalezených vrcholů, nikdy null
     */
    public static List<Vertex> getChildrenWithName(Vertex vertex, String childrenName,
            RevisionInterval revisionInterval) {
        if (vertex == null) {
            throw new IllegalArgumentException("Vertex must not be null.");
        }
        if (StringUtils.isEmpty(childrenName)) {
            throw new IllegalArgumentException("Children name must not be empty.");
        }

        TitanVertexQuery query = ((TitanVertex) vertex).query()
                .has(EdgeProperty.CHILD_NAME.t(), GraphOperation.processValueForChildName(childrenName))
                .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, revisionInterval.getStart())
                .has(EdgeProperty.TRAN_START.t(), Cmp.LESS_THAN_EQUAL, revisionInterval.getEnd())
                .direction(Direction.IN);

        Iterator<Vertex> iterator = query.vertices().iterator();
        if (iterator.hasNext()) {
            List<Vertex> children = new ArrayList<Vertex>();

            while (iterator.hasNext()) {
                children.add(iterator.next());
            }

            return children;
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * Ošetří hodnotu pro použití v rámci {@link EdgeProperty#CHILD_NAME}.
     * @param value řetězec ke kontrole/zpracování
     * @return upravený řetězec
     */
    public static String processValueForChildName(String value) {
        if (value != null) {
            if (value.length() > CHILD_NAME_MAXIMAL) {
                return value.substring(0, CHILD_NAME_MAXIMAL);
            } else {
                return value;
            }
        } else {
            return null;
        }
    }

    /**
     * Ošetří hodnotu pro použití v rámci {@link EdgeProperty#SOURCE_LOCAL_NAME}.
     * @param localName řetězec ke kontrole/zpracování
     * @return upravený řetězec
     */
    public static String processValueForLocalName(String localName) {
        return String.valueOf(localName.toLowerCase().hashCode());
    }

    /**
     * Vrátí typ vertexu podle jeho typu.
     * - node - typ uzlu <br>
     * - resource - typ resource <br>
     * - atribut uzlu - klíč atributu <br>
     * - super root - SUPER_ROOT
     * - revision root - REVISION_ROOT
     * - revision vrchol - REVISION_NODE
     * @param vertex vrchol, jehož typ se hledá
     * @return typ vertexu
     * @throws IllegalArgumentException vrchol je null
     * @throws IllegalStateException neznámý typ vrcholu
     */
    public static String getType(Vertex vertex) {
        if (vertex == null) {
            throw new IllegalArgumentException("The argument vertex must not be null.");
        }

        VertexType type = VertexType.getType(vertex);
        switch (type) {
        case LAYER:
            return vertex.getProperty(NodeProperty.LAYER_TYPE.t());
        case NODE:
            return vertex.getProperty(NodeProperty.NODE_TYPE.t());
        case RESOURCE:
            return vertex.getProperty(NodeProperty.RESOURCE_TYPE.t());
        case ATTRIBUTE:
            return vertex.getProperty(NodeProperty.ATTRIBUTE_NAME.t());
        case SUPER_ROOT:
            return VertexType.SUPER_ROOT.toString();
        case REVISION_ROOT:
            return VertexType.REVISION_ROOT.toString();
        case REVISION_NODE:
            return VertexType.REVISION_NODE.toString();
        case SOURCE_ROOT:
            return VertexType.SOURCE_ROOT.toString();
        case SOURCE_NODE:
            return VertexType.SOURCE_NODE.toString();
        default:
            throw new IllegalStateException("Uknown type of vertex " + type + ".");
        }
    }

    /**
     * Spočítá typy uzlů v dané kolekci.
     * @param transaction transakce pro přístup k db
     * @param vertexIdCollection kolekce id vrcholů k spočítání
     * @return mapa typů uzlů a jejich počtů v dané kolekci
     */
    public static Map<NodeTypeDef, Integer> getNodeTypeCounts(TitanTransaction transaction,
            Collection<Long> vertexIdCollection) {
        OccuranceCountHolder<NodeTypeDef> typeMap = new OccuranceCountHolder<NodeTypeDef>();

        for (Long id : vertexIdCollection) {
            if (id != null) {
                Vertex v = transaction.getVertex(id);
                if (v != null) {
                    String type = getType(v);
                    String resource = getType(GraphOperation.getResource(v));
                    typeMap.addOccurance(new NodeTypeDef(type, resource));
                }
            }
        }

        return typeMap.getCounts();
    }

    /**
     * Získá kontrolní hranu vrstvy.
     * To znamená mezi vrstvou a resource.
     * @param layer vrstva, pro kterou se hrana hledá
     * @return řídící hrana vrstvy
     * @throws IllegalStateException vrchol nemá řídící hranu
     */
    public static Edge getLayerControlEdge(Vertex layer) {
        List<Edge> edges = GraphOperation.getAdjacentEdges(layer, Direction.IN,
                RevisionRootHandler.EVERY_REVISION_INTERVAL, EdgeLabel.IN_LAYER);
        if (!edges.isEmpty()) {
            if (edges.size() > 1) {
                LOGGER.warn("The vertex {} has more inLayer edges.", layer);
            }
            return edges.get(0);
        } else {
            throw new IllegalStateException("Vertex has not any inLayer edge.");
        }
    }

    /**
     * Získá kontrolní hranu resource.
     * To znamená mezi super root a resource.
     * @param resource resource, pro který se hrana hledá
     * @return řídící hrana resource
     * @throws IllegalStateException vrchol nemá řídící hranu
     */
    public static Edge getResourceControlEdge(Vertex resource) {
        List<Edge> edges = GraphOperation.getAdjacentEdges(resource, Direction.OUT,
                RevisionRootHandler.EVERY_REVISION_INTERVAL, EdgeLabel.HAS_RESOURCE);
        if (!edges.isEmpty()) {
            if (edges.size() > 1) {
                LOGGER.warn("The vertex {} has more hasResource edges.", resource);
            }
            return edges.get(0);
        } else {
            throw new IllegalStateException("Vertex has not any hasResource edge.");
        }
    }

    /**
     * Získá kontrolní hranu uzlu.
     * To znamená mezi buďto has_parent nebo has_resource hranu vedoucí z uzlu.
     * @param node uzel, jehož kontrolní hrana se hledá
     * @return řídící hrana uzlu
     * @throws IllegalStateException vrchol nemá řídící hranu
     */
    public static Edge getNodeControlEdge(Vertex node) {
        List<Edge> edges = GraphOperation.getAdjacentEdges(node, Direction.OUT,
                RevisionRootHandler.EVERY_REVISION_INTERVAL, EdgeLabel.HAS_PARENT);
        if (!edges.isEmpty()) {
            if (edges.size() > 1) {
                LOGGER.warn("The vertex {} has more hasParent edges.", node);
            }
            return edges.get(0);
        } else {
            return getResourceControlEdge(node);
        }
    }

    /**
     * Získá kontrolní hranu atributu.
     * To znamená mezi uzlem a atributem.
     * @param attribute atribut, jehož kontrolní hrana se hledá
     * @return řídící hrana atributu
     * @throws IllegalStateException vrchol nemá řídící hranu
     */
    public static Edge getAttributeControlEdge(Vertex attribute) {
        List<Edge> edges = GraphOperation.getAdjacentEdges(attribute, Direction.IN,
                RevisionRootHandler.EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE);
        if (edges.size() == 1) {
            if (edges.size() > 1) {
                LOGGER.warn("The attribute vertex {} has more hasAttribute edges.", attribute);
            }
            return edges.get(0);
        } else {
            throw new IllegalStateException("Attribute vertex has not any hasAttribute edge.");
        }
    }

    /**
     * Získá kontrolní hranu pro daný vrchol.
     * @param vertex vrchol, pro který se hrana hledá
     * @return kontrolní hrana
     * @throws IllegalArgumentException pokud typ daného vrcholu nemá kontrolní hranu
     */
    public static Edge getControlEdge(Vertex vertex) {
        VertexType type = VertexType.getType(vertex);
        switch (type) {
        case RESOURCE:
            return getResourceControlEdge(vertex);
        case NODE:
            return getNodeControlEdge(vertex);
        case ATTRIBUTE:
            return getAttributeControlEdge(vertex);
        default:
            throw new IllegalArgumentException("Vertex type " + type + " doesn't have control edge.");
        }
    }

    /**
     * Získá množinu vrcholů, do kterých vede cesta z daného uzlu přes vyfiltrované vrcholy.
     * @param vertex výchozí uzel
     * @param startVertices startovní vrcholy referenčního view, protože se nikdy nefiltrují
     * @param flowState flowState, odpovídající danému referenčnímu view
     * @param direction směr hran
     * @return mapa vrcholů, kde hodnota je label hrany, jakou se dá dodaného vrcholu dostat
     */
    public static Map<Vertex, EnumSet<EdgeLabel>> getAdjacentUnfilteredVertices(Vertex vertex,
            Set<Vertex> startVertices, FlowState flowState, Direction direction) {
        return getAdjacentUnfilteredVertices(vertex, startVertices, flowState, direction,
                flowState.getRevisionInterval());
    }

    /**
     * Získá množinu vrcholů, do kterých vede cesta z daného uzlu přes vyfiltrované vrcholy.
     * @param vertex výchozí uzel
     * @param startVertices startovní vrcholy referenčního view, protože se nikdy nefiltrují
     * @param flowState flowState, odpovídající danému referenčnímu view
     * @param direction směr hran
     * @param revInterval interval revizí, po kterém se pohybuje
     * @return mapa vrcholů, kde hodnota je label hrany, jakou se dá dodaného vrcholu dostat
     */
    public static Map<Vertex, EnumSet<EdgeLabel>> getAdjacentUnfilteredVertices(Vertex vertex,
            Set<Vertex> startVertices, FlowState flowState, Direction direction, RevisionInterval revInterval) {
        Map<Vertex, EnumSet<EdgeLabel>> result = new HashMap<Vertex, EnumSet<EdgeLabel>>();
        Set<Long> directlyVisitedVertices = new HashSet<Long>();
        Set<Long> indirectlyVisitedVertices = new HashSet<Long>();
        directlyVisitedVertices.add((Long) vertex.getId());
        Queue<Vertex> verticesToProcess = new LinkedList<Vertex>();
        verticesToProcess.add(vertex);
        while (!verticesToProcess.isEmpty()) {
            Vertex currentVertex = verticesToProcess.poll();
            boolean currentDirectPath = directlyVisitedVertices.contains(currentVertex.getId());
            boolean currentIndirectPath = indirectlyVisitedVertices.contains(currentVertex.getId());
            Iterable<Edge> edges = GraphOperation.getAdjacentEdges(currentVertex, direction, revInterval,
                    GraphOperation.getDataflowLabels(flowState.isFilterEdgesShown()));
            for (Edge edge : edges) {
                boolean direct = EdgeLabel.DIRECT.t().equals(edge.getLabel());
                Vertex adjacentVertex = edge.getVertex(direction.opposite());
                if (flowState.getReferenceNodeIds().contains(adjacentVertex.getId())) {
                    boolean edgeDirectPath = currentDirectPath && direct;
                    boolean edgeIndirectPath = (currentDirectPath && !direct) || currentIndirectPath;
                    if (flowState.isVertexFiltered(adjacentVertex) && !startVertices.contains(adjacentVertex)) {
                        boolean adjacentDirectPath = directlyVisitedVertices.contains(adjacentVertex.getId());
                        boolean adjacentIndirectPath = indirectlyVisitedVertices.contains(adjacentVertex.getId());
                        boolean process = false;
                        if (!adjacentDirectPath && edgeDirectPath) {
                            directlyVisitedVertices.add((Long) adjacentVertex.getId());
                            process = true;
                        }
                        if (!adjacentIndirectPath && edgeIndirectPath) {
                            indirectlyVisitedVertices.add((Long) adjacentVertex.getId());
                            process = true;
                        }
                        if (process) {
                            verticesToProcess.add(adjacentVertex);
                        }
                    } else {
                        if (!result.containsKey(adjacentVertex)) {
                            result.put(adjacentVertex, EnumSet.noneOf(EdgeLabel.class));
                        }
                        Set<EdgeLabel> flowSet = result.get(adjacentVertex);
                        if (edgeDirectPath) {
                            flowSet.add(EdgeLabel.DIRECT);
                        }
                        if (edgeIndirectPath) {
                            flowSet.add(EdgeLabel.FILTER);
                        }
                    }
                }
            }
        }

        return result;
    }

    /**
     * Nalezne ekvivalentní vrcholy v dané revizi.
     * Vrchol je ekvivalentní, když má ekvivalentní předky, resource, stejný název a typ a má platnost v hledané revizi.
     * @param originalVertices originální vrhcoly, pro které se hledají ekvivalence
     * @param originalRevision originální revize
     * @param otherRevision revize, ve které se hledají ekvivalentní uzly
     * @return množina ekvivalentních vrcholů, neobsahuje originální uzly, nikdy null
     */
    public static Set<Vertex> findEquivalentVertices(Collection<Vertex> originalVertices, double originalRevision,
            double otherRevision) {
        Validate.notNull(originalVertices, "Original vertices must not be null");

        if (otherRevision <= 0.000000 || originalRevision <= 0.000000) {
            return Collections.emptySet();
        }

        if (otherRevision == originalRevision) {
            return Collections.emptySet();
        }

        if (originalVertices.isEmpty()) {
            return Collections.emptySet();
        }

        HashSet<Vertex> equivalentVertices = new HashSet<>();

        RevisionInterval otherRevisionInterval = new RevisionInterval(otherRevision);
        for (Vertex originalVertex : originalVertices) {
            Vertex equivalentVertex = findEquivalentVertexInRev(originalVertex, otherRevisionInterval);
            if (equivalentVertex != null && !(equivalentVertex.getId().equals(originalVertex.getId()))) {
                equivalentVertices.add(equivalentVertex);
            }
        }

        return equivalentVertices;
    }

    /**
     * Nalezne ekvivalentní uzel v dané revizi.
     * Vrchol je ekvivalentní, když má ekvivalentní předky, resource, stejný název a typ a má platnost v hledané revizi.
     * @param originalVertex originální vrchol
     * @param otherRevisionInterval hledaný interval, ve které se ekvivalent hledá
     * @return
     * <ul>
     *      <li>ekvivalentní uzel</li>
     *      <li>originální uzel, když má platnost i v hledané revizi</li>
     *      <li>null, když takový exvivalentní uzel neexistuje a originál nemá platnost v požadované revizi</li>
     * </ul>
     */
    public static Vertex findEquivalentVertexInRev(Vertex originalVertex, RevisionInterval otherRevisionInterval) {
        if (VertexType.getType(originalVertex) == VertexType.SUPER_ROOT) {
            return originalVertex;
        }

        Edge controlEdge = GraphOperation.getNodeControlEdge(originalVertex);
        if (RevisionUtils.isEdgeInRevisionInterval(controlEdge, otherRevisionInterval)) {
            return originalVertex;
        }

        Vertex parentVertex = findEquivalentVertexInRev(controlEdge.getVertex(Direction.IN), otherRevisionInterval);
        if (parentVertex == null) {
            return null;
        }

        String originalName = GraphOperation.getName(originalVertex);
        String originalType = GraphOperation.getType(originalVertex);
        List<Vertex> children = GraphOperation.getDirectChildren(parentVertex, otherRevisionInterval);
        for (Vertex child : children) {
            if (originalName.equals(GraphOperation.getName(child))
                    && originalType.equals(GraphOperation.getType(child))) {
                return child;
            }
        }

        return null;
    }

    /**
     * Nalezne vrcholy s danym jmenem.
     * @param transaction transakce
     * @param name jmeno vrcholu
     * @param rvi interval revizi
     * @return seznam vrcholu
     */
    public static List<Vertex> getVerticesWithName(TitanTransaction transaction, String name, RevisionInterval rvi,
            int limit) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("The argument name must not be empty.");
        }
        if (rvi == null) {
            throw new IllegalArgumentException("The argument revision interval must not be null.");
        }

        String escaped = processValueForSearchName(name);
        Iterable<Result<Vertex>> vertices = transaction
                .indexQuery("search", "v." + NodeProperty.NODE_NAME.t() + ":(/" + escaped + "/)").limit(limit)
                .vertices();

        List<Vertex> result = new ArrayList<Vertex>();
        for (Result<Vertex> v : vertices) {
            if (RevisionUtils.isVertexInRevisionInterval(v.getElement(), rvi)) {
                result.add(v.getElement());
            }
        }
        return result;
    }

    /**
     * Osetri retezec pro pouziti v query.
     * @param s vstupni retezec
     * @return osetreny retezec
     */
    private static String processValueForSearchName(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            // These characters are part of the query syntax and must be escaped
            if (c == '\\' || c == '+' || c == '-' || c == '!' || c == '(' || c == ')' || c == ':' || c == '^'
                    || c == '[' || c == ']' || c == '\"' || c == '{' || c == '}' || c == '~' || c == '*' || c == '?'
                    || c == '|' || c == '&' || c == '.' || c == '$' || c == '/' || c == '<' || c == '>' || c == '\'') {
                sb.append('\\');
            }
            sb.append(c);
        }
        return sb.toString();
    }

    /**
     * Nalezne hranu podle daného id.
     * @param transaction transakce úrp úřéstup k db
     * @param edgeId id hrany
     * @param revisionInterval interval revizí, kde se hrana hledá
     * @return titan object hledané hrany, nebo null, když taková hrana neexistuje
     */
    public static Edge getEdge(TitanTransaction transaction, EdgeIdentification edgeId,
            RevisionInterval revisionInterval) {
        Validate.notNull(transaction, "Transaction must not be null.");
        Validate.notNull(edgeId, "Edge identification must not be null.");
        Validate.notNull(revisionInterval, "Revision interval must not be null.");

        Vertex sourceVertex = transaction.getVertex(edgeId.getStartId());
        if (sourceVertex == null) {
            LOGGER.warn("Source vertex for queried edge doesn't exist.");
            return null;
        }

        Iterator<Edge> existedEdges;
        if (edgeId.getLabel() == EdgeLabel.DIRECT || edgeId.getLabel() == EdgeLabel.FILTER) {
            // lze použít index
            existedEdges = sourceVertex.query().has(EdgeProperty.TARGET_ID.t(), edgeId.getEndId())
                    .has(EdgeProperty.TRAN_START.t(), Cmp.LESS_THAN_EQUAL, revisionInterval.getEnd())
                    .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, revisionInterval.getStart())
                    .direction(Direction.OUT).labels(edgeId.getLabel().t()).edges().iterator();

        } else {
            // ostatní hrany index nemají
            existedEdges = sourceVertex.query()
                    .has(EdgeProperty.TRAN_START.t(), Cmp.LESS_THAN_EQUAL, revisionInterval.getEnd())
                    .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, revisionInterval.getStart())
                    .direction(Direction.OUT).labels(edgeId.getLabel().t()).edges().iterator();
        }

        while (existedEdges.hasNext()) {
            Edge existedEdge = existedEdges.next();
            if (existedEdge.getVertex(Direction.IN).getId().equals(edgeId.getEndId())) {
                if (edgeId.getRelationId() == null || edgeId.getRelationId()
                        .equals(((RelationIdentifier) existedEdge.getId()).getLongRepresentation()[2])) {
                    return existedEdge;
                }
            }
        }

        return null;
    }
    
    /**
     * Vrati uzel podle kvalifikovaneho jmena.
     * @param root kořen databáze
     * @param qn plně kvalifikované jméno uzlu
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return Nalezeny uzel nebo {@code null}, pokud se jej najit nepodari.
     */
    public static Vertex getVertexByQualifiedName(Vertex root, List<List<String>> qn,
            RevisionInterval revisionInterval) {
        List<Vertex> vertexPath = getVertexPathFromQualifiedName(root, qn, revisionInterval);
        if (vertexPath == null || vertexPath.isEmpty()) {
            return null;
        }
        return vertexPath.get(vertexPath.size() - 1);
    }
    
    /**
     * Vrací seznam uzlů v cestě od kořene k uzlu reprezentovaném plně kvalifikovaným jménem.
     * @param root kořen databáze
     * @param qn plně kvalifikované jméno uzlu
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return seznam uzlů grafové databáze
     */
    public static List<Vertex> getVertexPathFromQualifiedName(Vertex root, List<List<String>> qn,
            RevisionInterval revisionInterval) {
        if (qn == null || qn.isEmpty()) {
            return null;
        }
        List<Vertex> result = new ArrayList<Vertex>();
        if (qn.get(0).size() != QUALIFIED_NAME_PARTS) {
            return null;
        }
        Vertex currentVertex = findResource(root, qn.get(0).get(2), revisionInterval);
        if (currentVertex == null) {
            return null;
        }
        for (int index = 0; index < qn.size(); ++index) {
            if (qn.get(index).size() != QUALIFIED_NAME_PARTS) {
                return null;
            }
            currentVertex = findChildNode(currentVertex, qn.get(index).get(0), qn.get(index).get(1),
                    qn.get(index).get(2), revisionInterval);
            
            if (currentVertex != null) {
                result.add(currentVertex);
            } else {
                return null;
            }
        }
        return result;
    }
    
    /**
     * Nalezne resource s daným jménem a typem.
     * @param root kořen databáze
     * @param name jméno resource
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return nalezený resource nebo <code>null</code>
     */
    public static Vertex findResource(Vertex root, String name, RevisionInterval revisionInterval) {
        List<Vertex> children = GraphOperation.getDirectChildren(root, revisionInterval);
        for (Vertex child : children) {
            if (StringUtils.equalsIgnoreCase((String) child.getProperty(NodeProperty.RESOURCE_NAME.t()), name)) {
                return child;
            }
        }
        return null;
    }

    /**
     * Nalezne potomka s daným jménem a typem.
     * @param node předek, jehož potomek se hledá
     * @param name jméno potomka
     * @param type typ potomka
     * @param resource resource potomka
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return nalezený potomek nebo <code>null</code>
     */
    private static Vertex findChildNode(Vertex node, String name, String type, String resourceName,
            RevisionInterval revisionInterval) {
        boolean isRootResource = node.getProperty(NodeProperty.RESOURCE_NAME.t()) != null;

        List<Vertex> foundExactVertices = new ArrayList<>();
        List<Vertex> foundInsensitiveVertices = new ArrayList<>();

        List<Vertex> children = GraphOperation.getDirectChildren(node, revisionInterval);
        for (Vertex child : children) {
            Vertex childResource = GraphOperation.getResource(child);
            String childResourceName = (String) childResource.getProperty(NodeProperty.RESOURCE_NAME.t());
            String childName = (String) child.getProperty(NodeProperty.NODE_NAME.t());
            String childType = (String) child.getProperty(NodeProperty.NODE_TYPE.t());
            
            if (StringUtils.equals(childName, name)
                    && (StringUtils.isEmpty(type) || StringUtils.equals(childType, type))
                    && StringUtils.equals(childResourceName, resourceName)) {
                if (!isRootResource || GraphOperation.getParent(child) == null) {
                    foundExactVertices.add(child);
                }
            } else if (StringUtils.equalsIgnoreCase(childName, name)
                    && (StringUtils.isEmpty(type) || StringUtils.equalsIgnoreCase(childType, type))
                    && StringUtils.equalsIgnoreCase(childResourceName, resourceName)) {
                if (!isRootResource || GraphOperation.getParent(child) == null) {
                    foundInsensitiveVertices.add(child);
                }
            }
        }

        if (foundExactVertices.size() == 1) {
            return foundExactVertices.get(0);
        } else if (foundExactVertices.size() > 1) {
            LOGGER.warn("There exist {} vertices for identification {} {} {}.", foundExactVertices.size(), name, type,
                    resourceName);
            return null;
        } else if (foundInsensitiveVertices.size() == 1) {
            return foundInsensitiveVertices.get(0);
        } else if (foundInsensitiveVertices.size() > 1) {
            LOGGER.warn("There exist {} vertices for identification {} {} {}.", foundInsensitiveVertices.size(), name, type,
                    resourceName);
            return null;
        } else {
            return null;
        }
    }
    
    
    /**
     * Vypíše hierarchickou cestu k vrcholu.
     * 
     * Pro účely ladění a logování.
     *
     * @param vertex vrchol grafu (not null)
     * 
     * @return cesta k vrcholu v hierarchii
     */
    public static String logVertex(Vertex vertex) {
        Vertex parent;
        VertexType vertexType = VertexType.getType(vertex);
        switch (vertexType) {
            case ATTRIBUTE:
                parent = getParent(vertex);
                break;
            case NODE:
                parent = getParent(vertex);
                if (parent == null) {
                    parent = getResource(vertex);
                }
                break;
            case LAYER:
            case RESOURCE:
            case REVISION_NODE:
            case REVISION_ROOT:
            case SOURCE_NODE:
            case SOURCE_ROOT:
            case SUPER_ROOT:
                parent = null;
                break;
            default:
                throw new IllegalArgumentException(
                    "Unexpected vertex type: " + vertexType + " for vertex: " + getName(vertex));
        }
        
        StringBuilder sb = new StringBuilder();
        if (parent != null) {
            sb.append(logVertex(parent));
            sb.append(".");
        }

        sb.append(getName(vertex)).append("[").append(getType(vertex)).append("]");
        
        return sb.toString();
    }
    
    /**
     * Delete all vertices and edges (regardless their revision validity) in a subtree defined by a root node.
     * Root node is removed as well.
     * 
     * @param rootNode  root node of the subtree
     */
    public static void deleteSubtree(Vertex rootNode) {
        Iterable<Vertex> directChildren = GraphOperation.getDirectChildren(rootNode, RevisionRootHandler.EVERY_REVISION_INTERVAL);
        
        for (Vertex childNode : directChildren) {
            deleteSubtree(childNode);
        }
        
        Iterable<Edge> edges = RevisionUtils.getAdjacentEdges(rootNode, Direction.BOTH, RevisionRootHandler.EVERY_REVISION_INTERVAL);
        for (Edge edge : edges) {
            if(EdgeLabel.HAS_ATTRIBUTE.t().equals(edge.getLabel())) {
                Vertex nodeAttribute = edge.getVertex(Direction.IN);
                nodeAttribute.remove();
            }
            edge.remove();
        }
        
        rootNode.remove();
        
    }
    
    /**
     * Set transaction end of a subtree. All vertices in the subtree (and their edges) valid in a specific (walk-through)
     * revision will have their endRevision set to a new value. Subtree is specified by its root vertex. Vertices and edges 
     * absent in the specified (walk-through) revision are ignored.
     * 
     * WARNIGN: If vertex' or edge's new end revision (new tranEnd) is less than its start revision (tranStart),
     * the vertex or the edge is REMOVED from the repository (together with its whole subtree). In case of hasAttribute
     * edge, not only the edge but also the nodeAttribute is removed.
     * 
     * Example: 
     * _________________________________________________________________________________________________________
     * <1.000000, 1.999999>                 <1.000000, 1.999999>
     *        nodeA-----------hasAttribute-------->attrA
     *          |
     *          +-------------------------------+
     *          |                               |
     * <1.000000, 1.111111>            <1.000000, 1.999999>    <1.000000, 1.999999>      <1.000000, 1.999999>
     *        nodeB                           nodeC----------------directFlow------------------>nodeD
     *        
     * _________________________________________________________________________________________________________
     *                                                  Before
     * 
     * -----------------------------------------------------
     *  setSubtreeTransactionEnd(nodeA, 1.555555, 1.555555)
     * -----------------------------------------------------        
     * _________________________________________________________________________________________________________
     * <1.000000, 1.555555>                 <1.000000, 1.555555>
     *        nodeA-----------hasAttribute-------->attrA
     *          |
     *          +-------------------------------+
     *          |                               |
     * <1.000000, 1.111111>            <1.000000, 1.555555>    <1.000000, 1.555555>      <1.000000, 1.999999>
     *        nodeB                           nodeC----------------directFlow------------------>nodeD       
     * _________________________________________________________________________________________________________
     *                                                  After       
     * 
     * Notice from the example above:
     * 1) nodeB absent in the revision 1.555555 is ignored (its tranEnd remains unchanged)
     * 2) directFlow between nodeC and nodeD has its tranEnd changed (since it is connected to the affected nodeC)
     * 3) nodeD is ignored, since it is NOT in the subtree with the root nodeA
     * 
     * @param rootVertex
     * @param newEndRevision
     */
    public static void setSubtreeTransactionEnd(Vertex rootVertex, Double walkthroughRevision, Double newEndRevision) {
        if (rootVertex == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }
        if (walkthroughRevision == null) {
            throw new IllegalArgumentException("The argument walkthroughRevision must not be null.");
        }
        if (newEndRevision == null) {
            throw new IllegalArgumentException("The argument newEndRevision must not be null.");
        }

        RevisionInterval walkthroughRevInterval = new RevisionInterval(walkthroughRevision, walkthroughRevision);

        if (!RevisionUtils.isVertexInRevisionInterval(rootVertex, walkthroughRevInterval)) {
            throw new IllegalStateException(
                    "The argument rootNode does not exist in the given revision " + walkthroughRevision);
        }

        VertexType vertexType = VertexType.getType(rootVertex);
        if (vertexType.equals(VertexType.SUPER_ROOT) || vertexType.equals(VertexType.SOURCE_ROOT)
                || vertexType.equals(VertexType.REVISION_ROOT)) {
            // do nothing (root vertices cannot be changed or deleted)
        } else {
            Edge controlEdge = getControlEdge(rootVertex);
            Double startRevision = controlEdge.getProperty(EdgeProperty.TRAN_START.t());

            // if tranStart < tranEnd then delete the whole subtree (all vertices and edges regardless their revision validity)
            if (newEndRevision < startRevision) {
                deleteSubtree(rootVertex);
                return;
            }
        }

        // do the same for all child nodes of the rootNode
        Iterable<Vertex> directChildren = GraphOperation.getDirectChildren(rootVertex, walkthroughRevInterval);
        for (Vertex childNode : directChildren) {
            setSubtreeTransactionEnd(childNode, walkthroughRevision, newEndRevision);
        }

        // set tranEnd of all the rootNode's incoming and outcoming edges
        // if edge's tranStart is less than the new tranEnd then delete the edge
        Iterable<Edge> edges = RevisionUtils.getAdjacentEdges(rootVertex, Direction.BOTH, walkthroughRevInterval);
        for (Edge edge : edges) {
            Double edgeTranStart = edge.getProperty(EdgeProperty.TRAN_START.t());
            if (newEndRevision < edgeTranStart) {
                if (EdgeLabel.HAS_ATTRIBUTE.t().equals(edge.getLabel())) {
                    // if it is hasAttribute edge, then delete the nodeAttribute as well
                    Vertex nodeAttribute = edge.getVertex(Direction.IN);
                    nodeAttribute.remove();
                }
                edge.remove();
                continue;
            }
            RevisionUtils.setEdgeTransactionEnd(edge, newEndRevision);
        }
    }

    /**
     * Perform transition from old revision to a new revision in the whole graph structure.
     * 
     * All nodes and edges in the main graph and in the source code graph have
     * their end revision (tranEnd) set to a new end revision. 
     * 
     * @param oldRevision
     * @param newRevision
     */
    public static void performTransition(Double oldRevision, Double newRevision, TitanTransaction transaction) {
        SuperRootHandler superRootHandler = new SuperRootHandler();
        SourceRootHandler sourceRootHandler = new SourceRootHandler();

        Vertex superRoot = superRootHandler.getRoot(transaction);
        Vertex sourceRoot = sourceRootHandler.getRoot(transaction);

        setSubtreeTransactionEnd(superRoot, oldRevision, newRevision);
        setSubtreeTransactionEnd(sourceRoot, oldRevision, newRevision);
    }

}
