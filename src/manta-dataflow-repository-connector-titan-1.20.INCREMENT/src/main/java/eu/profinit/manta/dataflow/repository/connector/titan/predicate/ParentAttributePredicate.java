package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import java.util.List;
import java.util.Set;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;

/**
 * Evaluátor kontrolující podmínku, že předek uzlu má atribut s daným klíčem nacházející se v dané množině přípustných
 * hodnot.
 * 
 * V případě, že má předek více hodnot atributu, stačí, aby alespoň jedna se rovnala některé požadované hodnotě.
 *
 * @author tfechtner, jtousek
 */
public class ParentAttributePredicate implements VertexPredicate {
    /** Klíč ověřovaného atributu. */
    private String attributeKey;
    
    /** Množina povolených hodnot. */
    private Set<String> allowedValues;
    
    /**
     * Default konstruktor.
     */
    public ParentAttributePredicate() {
    }

    /**
     * @param attributeKey klíč ověřovaného atributu
     * @param allowedValues množina povolených hodnot
     */
    public ParentAttributePredicate(String attributeKey, Set<String> allowedValues) {
        this.allowedValues = allowedValues;
        this.attributeKey = attributeKey;
    }

    @Override
    public boolean evaluate(Vertex vertex, RevisionInterval revisionInterval) {
        if (allowedValues == null) {
            throw new IllegalStateException("The allowed values set must not be null.");
        }

        Vertex parentVertex = GraphOperation.getParent(vertex);
        if (parentVertex == null) {
            return false;
        }
        
        List<Object> actualValues = GraphOperation.getNodeAttribute(parentVertex, attributeKey, revisionInterval);
        for (Object value : actualValues) {
            if (allowedValues.contains(value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param attributeKey Klíč ověřovaného atributu
     */
    public void setAttributeKey(String attributeKey) {
        this.attributeKey = attributeKey;
    }

    /**
     * @param allowedValues množina povolených hodnot
     */
    public void setAllowedValues(Set<String> allowedValues) {
        this.allowedValues = allowedValues;
    }
    
    
}

