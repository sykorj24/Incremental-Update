package eu.profinit.manta.dataflow.repository.connector.titan.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.platform.automation.licensing.LicenseException;
import eu.profinit.manta.platform.licensing.LicenseHolder;
import eu.profinit.manta.platform.scriptmetadata.service.ScriptMetadataService;
import eu.profinit.manta.platform.scriptprocessing.model.ScriptMetadataConfig;

/**
 * Class for the management of revision root and revision tree in the database.
 * 
 * @author pholecek
 * @author tfechtner
 *
 */
public class RevisionRootHandler extends AbstractRootHandler {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(RevisionRootHandler.class);
    /** Hláška pro případ neexistence žádné commitnuté verze.*/
    public static final String NO_REVISION_IN_REP = "There is not any commited version in the repository yet.";
    /** Double in Titan database has maximum scale up to 6 decimal digits 
     * (double can be serialized only up to 6 decimal digits)
     */
    public static final Integer SCALE = 6;
    /** Rounding mode used when cutting rest of the decimal digits */
    public static final RoundingMode ROUNDING_MODE = RoundingMode.DOWN;
    /** Technical revision number 0.000000. Technical revision is the first revision (=minimum revision number). */
    public static final double TECHNICAL_REVISION_NUMBER = new BigDecimal("0.000000").setScale(SCALE, ROUNDING_MODE)
            .doubleValue();

    //    /** Revision max value is the maximum possible double value stored in the Titan database (serialized). 
    //     *  Can NOT use Double.MAX_VALUE -> out of range.
    //     *  Computation of the max serialized double value is taken from the Titan DoubleSerializer class.
    //     */
    //    public static long MULTIPLIER = 0;
    //    static {
    //        MULTIPLIER = 1;
    //        for (int i = 0; i < SCALE; i++) MULTIPLIER *= 10;
    //    }
    //    public static final double REVISION_MAX_NUMBER = Long.MAX_VALUE*1.0 / (MULTIPLIER+1);

    // FIXME cannot use higher value greater than Integer.MAX_VALUE due to some unknown reason in the failing test Dev1731Test (dataflow.routine)
    public static final double REVISION_MAX_NUMBER = Integer.MAX_VALUE;

    /** Interval zahnující všechny revize. */
    public static final RevisionInterval EVERY_REVISION_INTERVAL = new RevisionInterval(TECHNICAL_REVISION_NUMBER,
            REVISION_MAX_NUMBER);
    /** The increment value when creating a new minor revision */
    public static final BigDecimal MINOR_REVISION_DELTA = new BigDecimal("0.000001").setScale(SCALE, ROUNDING_MODE);
    /** The maximum minor revision number within one major revision */
    public static final BigDecimal MINOR_REVISION_MAXIMUM_NUMBER = new BigDecimal("0.999999").setScale(SCALE,
            ROUNDING_MODE);
    /** Decimal format with 6 decimal digits for printing */
    private NumberFormat formatter = new DecimalFormat("#0.000000");

    /** Zámek pro zamknutí přístupu k revizním vrcholům. */
    private final ReadWriteLock revisionsLock = new ReentrantReadWriteLock();
    /** Zámek pro zamknutí přístupu k reviznímu rootu. */
    private final Object rootLock = new Object();
    /** True, jestliže se má použít v systému verzování. */
    private boolean isVersionSystemOn = true;
    /** True, jestliže se smít používat additional merge. */
    private boolean isAdditionalMergeOn = false;
    /** Sluzba pro praci s metadaty skriptu.  */
    private ScriptMetadataService scriptMetadataService;
    /** Drzak licence. */
    private LicenseHolder licenseHolder;

    @Override
    public NodeProperty getNodeProperty() {
        return NodeProperty.REVISION_ROOT;
    }

    @Override
    public VertexType getVertexType() {
        return VertexType.REVISION_ROOT;
    }

    @Override
    public Vertex ensureRootExistance(TitanTransaction transaction) {
        synchronized (rootLock) {
            return super.ensureRootExistance(transaction);
        }
    }

    @Override
    public Long getRoot(DatabaseHolder databaseHolder) {
        synchronized (rootLock) {
            return super.getRoot(databaseHolder);
        }
    }

    @Override
    public Vertex getRoot(TitanTransaction transaction) {
        synchronized (rootLock) {
            return super.getRoot(transaction);
        }
    }

    /**
     * Create new major revision. Latest revision has to be committed. 
     * New major revision is always greater by 1 than the previous major revision.
     * New major revision has always minor revision .000000
     * <p>
     * Example 1: latestCommittedRevision = 1.000000 => newMajorRevision = 2.000000
     * Example 2: latestCommittedRevision = 1.123456 => newMajorRevision = 2.000000
     * 
     * @param transaction  transaction for the access to the database
     * @return newly created revision number
     */
    public Double createMajorRevision(TitanTransaction transaction) {
        revisionsLock.writeLock().lock();
        try {
            Vertex revisionRootNode = getRoot(transaction);
            Double latestCommRevNumber = getLatestCommittedRevisionNumber(transaction);
            Double latestUncommRevNumber = getLatestUncommittedRevisionNumber(transaction);
            Double newRevNumber = getNextMajorRevNumber(latestCommRevNumber, latestUncommRevNumber);

            LOGGER.info("Creating major revision {}.", formatter.format(newRevNumber));
            createRevisionNode(revisionRootNode, newRevNumber, latestCommRevNumber, transaction);
            revertUncommittedScripts();
            return newRevNumber;
        } finally {
            revisionsLock.writeLock().unlock();
        }
    }

    /**
     * Create new minor revision. Latest revision has to be committed.
     * New minor revision is always greater by 0.000001 than the previous revision.
     * <p>
     * New minor revision is always created in the same major revision as the previous minor revision.
     * The only exception is, when the number of minor revisions within one major revision is reached.
     * Then, a new major revision is created instead.
     * (the maximum minor revision within a major revision is .999999)
     * <p>
     * Example 1: latestCommittedRevision = 1.000000 => newMinorRevision = 1.000001
     * Example 2: latestCommittedRevision = 1.123456 => newMinorRevision = 1.123457
     * Example 3: latestCommittedRevision = 1.999999 => transition to 2.999999, newMinorRevision = newMajorRevision = 2.000000
     * 
     * @param transaction  transaction for the access to the database
     * @return newly created revision number
     */
    public Double createMinorRevision(TitanTransaction transaction) {
        revisionsLock.writeLock().lock();
        try {
            Vertex revisionRootNode = getRoot(transaction);
            Double latestCommRevNumber = getLatestCommittedRevisionNumber(transaction);
            Double latestUncommRevNumber = getLatestUncommittedRevisionNumber(transaction);
            Double newRevNumber;
            try {
                newRevNumber = getNextMinorRevNumber(latestCommRevNumber, latestUncommRevNumber);
            } catch (IllegalStateException e) {
                // Maximum minor revision reached. Cannot perform incremental update without setting the whole repository
                // to the next major revision context
                LOGGER.warn(e.getMessage());
                Double nextMaxRevNumber = RevisionUtils.getMaxMinorRevisionNumber(Math.ceil(latestCommRevNumber));
                LOGGER.info("Performing transition to the next max revision {}", nextMaxRevNumber);
                GraphOperation.performTransition(latestCommRevNumber, nextMaxRevNumber, transaction);
                newRevNumber = Math.ceil(latestCommRevNumber);
            }

            LOGGER.info("Creating minor revision {}.", formatter.format(newRevNumber));
            createRevisionNode(revisionRootNode, newRevNumber, latestCommRevNumber, transaction);
            revertUncommittedScripts();
            return newRevNumber;
        } finally {
            revisionsLock.writeLock().unlock();
        }
    }

    /**
     * Get next major revision number
     * 
     * @param latestCommRevNumber  latest committed revision number
     * @param latestUncommRevNumber latest uncommitted revision number
     * @return  next major revision number or technical revision number if no revision exists
     */
    private Double getNextMajorRevNumber(Double latestCommRevNumber, Double latestUncommRevNumber) {
        if (latestCommRevNumber == null && latestUncommRevNumber == null) {
            return TECHNICAL_REVISION_NUMBER;
        } else if (latestCommRevNumber == null
                || (latestUncommRevNumber != null && latestUncommRevNumber > latestCommRevNumber)) {
            throw new RevisionException("Cannot create a new revision without committing the previous one.");
        } else {
            return Math.floor(latestCommRevNumber + 1);
        }
    }

    /**
     * Get next minor revision number
     * 
     * @param latestCommRevNumber  latest committed revision number
     * @param latestUncommRevNumber  latest uncommitted revision number
     * @return next minor revision number or technical revision number if no revision exists
     */
    private Double getNextMinorRevNumber(Double latestCommRevNumber, Double latestUncommRevNumber) {
        if (latestCommRevNumber == null && latestUncommRevNumber == null) {
            return TECHNICAL_REVISION_NUMBER;
        } else if (latestCommRevNumber == null
                || (latestUncommRevNumber != null && latestUncommRevNumber > latestCommRevNumber)) {
            throw new RevisionException("Cannot create a new revision without committing the previous one.");
        } else {
            BigDecimal integerPart = BigDecimal.valueOf(Math.floor(latestCommRevNumber)).setScale(SCALE, ROUNDING_MODE);
            BigDecimal maximumMinorRevision = integerPart.add(MINOR_REVISION_MAXIMUM_NUMBER);
            BigDecimal latestCommittedRevisionBigDecimal = BigDecimal.valueOf(latestCommRevNumber).setScale(SCALE,
                    ROUNDING_MODE);
            if (maximumMinorRevision.equals(latestCommittedRevisionBigDecimal)) {
                throw new IllegalStateException(
                        "Maximum minor revision reached (latestCommittedRevision=" + latestCommRevNumber + ").");
            } else {
                return latestCommittedRevisionBigDecimal.add(MINOR_REVISION_DELTA).doubleValue();
            }
        }
    }

    /**
     * Create new revision node
     * 
     * @param revisionRootNode  revision root node
     * @param newRevNumber  revision number of the revision node to-be-created
     * @param latestCommRevNumber  latest committed revision number
     * @param transaction  transaction for the access to the database
     */
    private void createRevisionNode(Vertex revisionRootNode, Double newRevNumber, Double latestCommRevNumber,
            TitanTransaction transaction) {
        // get the previous revision node
        Vertex prevRevNode;
        if (newRevNumber == TECHNICAL_REVISION_NUMBER) {
            // technical revision is always the first revision -> has no preceding revision
            prevRevNode = null;
        } else {
            // previous revision is always the latest committed revision
            prevRevNode = getSpecificVertex(latestCommRevNumber, revisionRootNode);
        }

        // newly created revision node is the latest revision -> has no following revision
        Vertex nextRevNode = null;

        GraphCreation.createRevisionNode(transaction, revisionRootNode, newRevNumber, prevRevNode, nextRevNode);
    }

    /**
     * Revert uncommitted scripts
     */
    private void revertUncommittedScripts() {
        // Pro jistotu revertujeme informace o skriptech, ktere jeste nebyly commitnuty
        int revertedScriptCount = scriptMetadataService.revertUncommittedScripts();
        LOGGER.info(MessageFormat.format("{0} script hash(es) reverted.", revertedScriptCount));
    }

    /**
     * Commit a specific revision. Revision can be committed only if it is the latest uncommitted revision.
     * When the latest uncommitted revision is committed, the latest uncommitted revision is set to (-1), 
     * representing that no revision is uncommitted.
     * 
     * @param transaction  transaction for the access to the database
     * @param revision  number of the revision to be committed
     */
    public void commitRevision(TitanTransaction transaction, Double revision) {
        revisionsLock.writeLock().lock();
        try {
            if (revision == null) {
                throw new RevisionException("Revision cannot be null.");
            }
            Vertex revisionRootNode = getRoot(transaction);
            // Najdu uzel se zadaným cislem revize
            Vertex revisionVertex = getSpecificVertex(revision, revisionRootNode);
            if (revisionVertex == null) {
                throw new RevisionException("Revision " + formatter.format(revision) + " does not exist.");
            }
            RevisionModel revisionModel = new RevisionModel(revisionVertex);

            if (revisionModel.isCommitted()) {
                throw new RevisionException("Trying to commit an already committed revision.");
            }

            // Zkusime commitnout informace o commitnutych skriptech
            int storageResult = scriptMetadataService.commitScripts(
                    licenseHolder.getLicense().getMaxScriptCount(ScriptMetadataConfig.SCRIPT_COUNT_EXCEED_TOLERANCE),
                    ScriptMetadataConfig.LAST_SCRIPT_COUNT_PERIOD, null);
            if (storageResult != 0) {
                // Pokud se to nepovede, vyhodime vyjimku 
                int limit = licenseHolder.getLicense().getMaxScriptCount();
                throw new LicenseException(MessageFormat.format(
                        "Scripts cannot be processed because script count ({0}) would exceed the limit {1}.",
                        storageResult, limit));
            }

            // Není commitnutý - nastavím příznak na true a čas commitu
            revisionVertex.setProperty(NodeProperty.REVISION_NODE_COMMIT_TIME.t(), new Date());
            revisionVertex.setProperty(NodeProperty.REVISION_NODE_COMMITTED.t(), true);

            revisionRootNode.setProperty(NodeProperty.LATEST_COMMITTED_REVISION.t(), revisionModel.getRevision());
            revisionRootNode.setProperty(NodeProperty.LATEST_UNCOMMITTED_REVISION.t(), -1.0);
            LOGGER.info("Revision {} committed.", formatter.format(revision));
        } finally {
            revisionsLock.writeLock().unlock();
        }
    }

    /**
     * Commit the latest revision (revision with the highest number).
     * Latest revision can be either committed or uncommitted. Committed revision cannot be committed again.
     * 
     * @param transaction  transaction for the access to the database
     */
    public void commitLatestRevision(TitanTransaction transaction) {
        revisionsLock.writeLock().lock();
        try {
            Vertex revisionRootNode = getRoot(transaction);
            Double revisionNumber = revisionRootNode.getProperty(NodeProperty.LATEST_UNCOMMITTED_REVISION.t());
            if (revisionNumber == null || revisionNumber == -1) {
                throw new RevisionException("Latest revision is already committed.");
            }

            Vertex revisionVertex = getSpecificVertex(revisionNumber, revisionRootNode);
            if (revisionVertex == null) {
                throw new RevisionException("Revision vertex " + formatter.format(revisionNumber) + " does not exist.");
            }
            RevisionModel revisionModel = new RevisionModel(revisionVertex);
            if (revisionModel.isCommitted()) {
                throw new RevisionException("Trying to commit already committed revision.");
            }

            // Zkusime commitnout informace o commitnutych skriptech
            int storageResult = scriptMetadataService.commitScripts(
                    licenseHolder.getLicense().getMaxScriptCount(ScriptMetadataConfig.SCRIPT_COUNT_EXCEED_TOLERANCE),
                    ScriptMetadataConfig.LAST_SCRIPT_COUNT_PERIOD, null);
            if (storageResult != 0) {
                // Pokud se to nepovede, vyhodime vyjimku 
                int limit = licenseHolder.getLicense().getMaxScriptCount();
                throw new LicenseException(MessageFormat.format(
                        "Scripts cannot be processed because script count ({0}) would exceed the limit {1}.",
                        storageResult, limit));
            }

            // Není commitnutý - nastavím příznak na true a čas commitu
            revisionVertex.setProperty(NodeProperty.REVISION_NODE_COMMIT_TIME.t(), new Date());
            revisionVertex.setProperty(NodeProperty.REVISION_NODE_COMMITTED.t(), true);

            revisionRootNode.setProperty(NodeProperty.LATEST_COMMITTED_REVISION.t(), revisionNumber);
            revisionRootNode.setProperty(NodeProperty.LATEST_UNCOMMITTED_REVISION.t(), -1.0);

            LOGGER.info("Revision {} committed.", formatter.format(revisionNumber));
        } finally {
            revisionsLock.writeLock().unlock();
        }
    }

    /**
     * Provede callback s držením R zámku pro revize {@link #revisionsLock}.
     * @param operation operace k vykonání
     * @return výsledek operace
     * 
     * @param <T> návrratová hodnota callbacku 
     */
    public <T> T executeRevisionReadOperation(RevisionLockedOperation<T> operation) {
        revisionsLock.readLock().lock();
        try {
            return operation.call();
        } finally {
            revisionsLock.readLock().unlock();
        }
    }

    /**
     * Provede callback s držením R/W zámku pro revize {@link #revisionsLock}.
     * @param operation operace k vykonání
     * @return výsledek operace
     * 
     * @param <T> návrratová hodnota callbacku 
     */
    public <T> T executeRevisionWriteOperation(RevisionLockedOperation<T> operation) {
        revisionsLock.writeLock().lock();
        try {
            return operation.call();
        } finally {
            revisionsLock.writeLock().unlock();
        }
    }

    /**
     * Check, whether it is possible to perform merge operation in the specified revision.
     * Merge can be performed only in the latest uncommitted revision. Cannot merge into an 
     * already committed revision.
     * 
     * @param transaction  transaction for the access to the database
     * @param revision  number of the checked revision
     * @throws RevisionException  revision does not meet the conditions
     */
    public void checkMergeConditions(TitanTransaction transaction, Double revision) {
        if (revision == null) {
            throw new RevisionException("Revision must not be null.");
        }

        Double latestUncommittedRevision = getLatestUncommittedRevisionNumber(transaction);

        if (latestUncommittedRevision == null) {
            throw new RevisionException(
                    "No uncommitted revision exists. Can merge only to the latest uncommitted revision.");
        }
        if (!latestUncommittedRevision.equals(revision)) {
            throw new RevisionException("Cannot merge to the revision " + revision + ". "
                    + "Can merge only to the latest uncommitted revision " + latestUncommittedRevision);
        }
    }

    /**
     * Check, whether it is possible to perform delete operation in the specified revision.
     * Delete can be performed only in the latest uncommitted revision. Cannot delete in an
     * already committed revision.
     * 
     * @param transaction  transaction for the access to the database
     * @param revision  number of the checked revision
     * @throws RevisionException  revision does not meet the conditions
     */
    public void checkDeleteConditions(TitanTransaction transaction, Double revision) {
        if (revision == null) {
            throw new RevisionException("Revision must not be null.");
        }

        Double latestUncommittedRevision = getLatestUncommittedRevisionNumber(transaction);

        if (latestUncommittedRevision == null) {
            throw new RevisionException(
                    "No uncommitted revision exists. Can delete only in the latest uncommitted revision.");
        }
        if (!latestUncommittedRevision.equals(revision)) {
            throw new RevisionException("Cannot delete in the revision " + revision + ". "
                    + "Can delete only in the latest uncommitted revision " + latestUncommittedRevision);
        }
    }

    /**
     * Remove revision node. It is possible to remove only an uncommitted revision.   
     * 
     * @param databaseHolder  database holder
     * @param revisionNumber  number of the revision to be destroyed
     */
    public void removeRevisionNode(DatabaseHolder databaseHolder, final Double revisionNumber) {
        revisionsLock.writeLock().lock();
        try {
            databaseHolder.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TransactionCallback<Object>() {
                @Override
                public Object callMe(TitanTransaction transaction) {
                    removeRevisionNode(transaction, revisionNumber);
                    // Revertujeme informace o skriptech, ktere jeste nebyly commitnuty
                    int revertedScriptCount = scriptMetadataService.revertUncommittedScripts();
                    LOGGER.info(MessageFormat.format("{0} script hash(es) reverted.", revertedScriptCount));
                    return null;
                }

                @Override
                public String getModuleName() {
                    return MODULE_NAME;
                }

            });
        } finally {
            revisionsLock.writeLock().unlock();
        }
    }

    /**
     * Remove revision node. It is possible to remove only an uncommitted revision.
     * 
     * @param transaction  transaction for the access to the database
     * @param revisionNumber  number of the revision to be destroyed
     */
    public void removeRevisionNode(TitanTransaction transaction, Double revisionNumber) {
        revisionsLock.writeLock().lock();
        try {
            if (revisionNumber == null) {
                throw new RevisionException("The revision number must not be null.");
            }
            Vertex revisionRootNode = getRoot(transaction);
            Vertex revisionVertex = getSpecificVertex(revisionNumber, revisionRootNode);
            if (revisionVertex != null) {
                if (BooleanUtils
                        .isNotTrue((Boolean) revisionVertex.getProperty(NodeProperty.REVISION_NODE_COMMITTED.t()))) {
                    // set next revision of the previous revision to the next revision of the current revision
                    Double previousRevisionNumber = revisionVertex
                            .getProperty(NodeProperty.REVISION_NODE_PREVIOUS_REVISION.t());
                    Vertex previousRevisionVertex = getSpecificVertex(previousRevisionNumber, revisionRootNode);
                    Double nextRevisionNumber = revisionVertex
                            .getProperty(NodeProperty.REVISION_NODE_NEXT_REVISION.t());
                    previousRevisionVertex.setProperty(NodeProperty.REVISION_NODE_NEXT_REVISION.t(),
                            nextRevisionNumber);

                    // reset the latest uncommitted revision number in the revision root, latest committed revision remains the same
                    revisionRootNode.setProperty(NodeProperty.LATEST_UNCOMMITTED_REVISION.t(), -1.0);

                    // finally, remove the revision node
                    revisionVertex.remove();
                } else {
                    throw new RevisionException("Cannot remove committed revision.");
                }
            }
        } finally {
            revisionsLock.writeLock().unlock();
        }
    }

    /**
     * Remove revision interval.
     * 
     * @param databaseHolder  database holder
     * @param revisionInterval  interval of revisions that are to be removed
     */
    public void removeRevisionInterval(DatabaseHolder databaseHolder, final RevisionInterval revisionInterval) {
        revisionsLock.writeLock().lock();
        try {
            databaseHolder.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TransactionCallback<Object>() {
                @Override
                public Object callMe(TitanTransaction transaction) {
                    removeRevisionInterval(transaction, revisionInterval);
                    return null;
                }

                @Override
                public String getModuleName() {
                    return MODULE_NAME;
                }

            });
        } finally {
            revisionsLock.writeLock().unlock();
        }
    }

    /**
     * Remove revision interval.
     * 
     * @param transaction  transaction for the access to the database
     * @param revisionInterval  interval of revisions that are to be removed
     */
    public void removeRevisionInterval(TitanTransaction transaction, RevisionInterval revisionInterval) {
        revisionsLock.writeLock().lock();
        try {
            Vertex revisionRootNode = getRoot(transaction);
            List<Vertex> revisions = GraphOperation.getAdjacentVertices(revisionRootNode, Direction.OUT,
                    revisionInterval, EdgeLabel.HAS_REVISION);
            for (Vertex rev : revisions) {
                rev.remove();
            }
        } finally {
            revisionsLock.writeLock().unlock();
        }
    }

    /**
     * Get the latest committed revision number
     * 
     * @param databaseHolder  database holder
     * @return latest committed revision number or null, if no revision was committed yet
     */
    public Double getLatestCommittedRevisionNumber(DatabaseHolder databaseHolder) {
        return databaseHolder.runInTransaction(TransactionLevel.READ, new TransactionCallback<Double>() {
            @Override
            public String getModuleName() {
                return MODULE_NAME;
            }

            @Override
            public Double callMe(TitanTransaction transaction) {
                return getLatestCommittedRevisionNumber(transaction);
            }
        });
    }

    /**
     * Get the latest committed revision number
     * 
     * @param transaction  transaction for the access to the database
     * @return latest committed revision number or null, if no revision was committed yet
     */
    public Double getLatestCommittedRevisionNumber(TitanTransaction transaction) {
        Vertex revisionRootNode = getRoot(transaction);
        return revisionRootNode.getProperty(NodeProperty.LATEST_COMMITTED_REVISION.t());
    }

    /**
     * Get the latest uncommitted revision number
     * 
     * @param databaseHolder  database holder
     * @return latest uncommitted revision number or null, if no uncommitted revision exists
     */
    public Double getLatestUncommittedRevisionNumber(DatabaseHolder databaseHolder) {
        return databaseHolder.runInTransaction(TransactionLevel.READ, new TransactionCallback<Double>() {
            @Override
            public String getModuleName() {
                return MODULE_NAME;
            }

            @Override
            public Double callMe(TitanTransaction transaction) {
                return getLatestUncommittedRevisionNumber(transaction);
            }
        });
    }

    /**
     * Get the latest uncommitted revision number
     * 
     * @param transaction  transaction for the access to the database
     * @return latest uncommitted revision number or null, if no uncommitted revision exists
     */
    public Double getLatestUncommittedRevisionNumber(TitanTransaction transaction) {
        Vertex revisionRootNode = getRoot(transaction);
        Double latestUncommittedRevision = revisionRootNode.getProperty(NodeProperty.LATEST_UNCOMMITTED_REVISION.t());
        if (latestUncommittedRevision == null || latestUncommittedRevision == -1.0) {
            return null;
        } else {
            return latestUncommittedRevision;
        }
    }

    /**
     * Get the latest committed revision number.
     * 
     * @param databaseHolder  database holder
     * @return latest committed revision number or null, if no revision was committed yet
     */
    public Double getHeadNumber(DatabaseHolder databaseHolder) {
        return getLatestCommittedRevisionNumber(databaseHolder);
    }

    /**
     * Get the latest committed revision number.
     * 
     * @param transaction  transaction to access the database
     * @return latest committed revision number or null, if no revision was committed yet
     */
    public Double getHeadNumber(TitanTransaction transaction) {
        return getLatestCommittedRevisionNumber(transaction);
    }

    /**
     * Get the latest revision number, regardless of the commit status.
     * 
     * @param transaction  transaction for the access to the database
     * @return latest revision number or null, if no revision was created yet
     */
    public Double getLatestRevisionNumber(TitanTransaction transaction) {
        Vertex revisionRootNode = getRoot(transaction);
        Double latestCommittedRevision = revisionRootNode.getProperty(NodeProperty.LATEST_COMMITTED_REVISION.t());
        Double latestUncommittedRevision = revisionRootNode.getProperty(NodeProperty.LATEST_UNCOMMITTED_REVISION.t());

        if (latestCommittedRevision == null && latestUncommittedRevision == null) {
            return null;
        } else if (latestCommittedRevision == null || latestUncommittedRevision > latestCommittedRevision) {
            return latestUncommittedRevision;
        } else {
            return latestCommittedRevision;
        }
    }

    /**
     * Get the latest revision number, regardless of the commit status.
     * 
     * @param databaseHolder  database holder
     * @return latest revision number or null, if no revision was created yet
     */
    public Double getLatestRevisionNumber(DatabaseHolder databaseHolder) {
        return databaseHolder.runInTransaction(TransactionLevel.READ, new TransactionCallback<Double>() {
            @Override
            public String getModuleName() {
                return MODULE_NAME;
            }

            @Override
            public Double callMe(TitanTransaction transaction) {
                return getLatestRevisionNumber(transaction);
            }
        });
    }

    /**
     * Vrátí nejstarší commitnutou revizi.
     * @param databaseHolder držák na databázi
     * @return nejstarší revize nebo null, když není žádní commitnutá
     */
    public RevisionModel getOldestModel(DatabaseHolder databaseHolder) {
        return databaseHolder.runInTransaction(TransactionLevel.READ, new TransactionCallback<RevisionModel>() {

            @Override
            public RevisionModel callMe(TitanTransaction transaction) {
                return getOldestModel(transaction);
            }

            @Override
            public String getModuleName() {
                return MODULE_NAME;
            }
        });
    }

    /**
     * Vrátí nejstarší commitnutou revizi.
     * @param transaction transakce pro přístup k db
     * @return nejstarší revize nebo null, když není žádní commitnutá
     */
    public RevisionModel getOldestModel(TitanTransaction transaction) {
        Vertex revisionRootNode = getRoot(transaction);
        List<Vertex> revisionVertices = GraphOperation.getAdjacentVertices(revisionRootNode, Direction.OUT,
                EVERY_REVISION_INTERVAL, EdgeLabel.HAS_REVISION);

        Double lowestRevisionNumber = REVISION_MAX_NUMBER;
        Vertex oldestRevisionVertex = null;
        for (Vertex currentVertex : revisionVertices) {
            Double currentVertexRevision = currentVertex.getProperty(NodeProperty.REVISION_NODE_REVISION.t());
            if (currentVertexRevision == null) {
                throw new RevisionException("Revision node property revision not set.");
            }

            Boolean isCommited = currentVertex.getProperty(NodeProperty.REVISION_NODE_COMMITTED.t());
            if (BooleanUtils.isTrue(isCommited) && currentVertexRevision < lowestRevisionNumber) {
                lowestRevisionNumber = currentVertexRevision;
                oldestRevisionVertex = currentVertex;
            }
        }

        if (oldestRevisionVertex != null) {
            return new RevisionModel(oldestRevisionVertex);
        } else {
            return null;
        }
    }

    /**
     * Získá model pro specifickou revizi.
     * @param transaction transakce pro přístup k db
     * @param revisionNumber číslo revize, který se má najít
     * @return model pro specifickou revizi, nebo null pokud neexistuje
     */
    public RevisionModel getSpecificModel(TitanTransaction transaction, Double revisionNumber) {
        if (revisionNumber == null) {
            throw new RevisionException("The revision number must not be null.");
        }

        Vertex revisionRootNode = getRoot(transaction);
        Vertex revisionVertex = getSpecificVertex(revisionNumber, revisionRootNode);
        if (revisionVertex != null) {
            return new RevisionModel(revisionVertex);
        } else {
            return null;
        }
    }

    /**
     * Získá model pro specifickou revizi.
     * @param databaseHolder držák na databáze
     * @param revisionNumber číslo revize, který se má najít
     * @return model pro specifickou revizi, nebo null pokud neexistuje
     */
    public RevisionModel getSpecificModel(DatabaseHolder databaseHolder, final Double revisionNumber) {
        return databaseHolder.runInTransaction(TransactionLevel.READ, new TransactionCallback<RevisionModel>() {

            @Override
            public String getModuleName() {
                return MODULE_NAME;
            }

            @Override
            public RevisionModel callMe(TitanTransaction transaction) {
                return getSpecificModel(transaction, revisionNumber);
            }
        });
    }

    /**
     * Pokusí se nalézt vertex odpovídjící konkrétní revizi.
     * 
     * @param revisionNumber číslo revize k vyhledání
     * @param revisionRootNode revision root
     * @return odpovídající vertex nebo null
     */
    private Vertex getSpecificVertex(Double revisionNumber, Vertex revisionRootNode) {
        RevisionInterval revInterval = new RevisionInterval(revisionNumber, revisionNumber);
        List<Vertex> revisionVertices = GraphOperation.getAdjacentVertices(revisionRootNode, Direction.OUT, revInterval,
                EdgeLabel.HAS_REVISION);

        if (revisionVertices.size() == 1) {
            return revisionVertices.get(0);
        } else if (revisionVertices.isEmpty()) {
            return null;
        } else {
            throw new RevisionException("There are more (" + revisionVertices.size()
                    + ") revision vertices for one revision (" + revisionNumber + ").");
        }
    }

    public Double getPreviousRevisionNumber(DatabaseHolder databaseHolder, final Double revisionNumber) {
        return databaseHolder.runInTransaction(TransactionLevel.READ, new TransactionCallback<Double>() {

            @Override
            public String getModuleName() {
                return MODULE_NAME;
            }

            @Override
            public Double callMe(TitanTransaction transaction) {
                return getPreviousRevisionNumber(transaction, revisionNumber);
            }
        });
    }

    /**
     * Get previous revision number (preceding revision with the highest revision number) of another revision number.
     * Previous revision number is saved as a property in every revision node.
     * <p>
     * Example:
     * Revisions: 0.000000, 1.000000, 1.000001, 1.000002, 2.000000, 2.000001
     * previousRevisionNumber(2.000001) -> 2.000000
     * previousRevisionNumber(2.000000) -> 1.000002
     * previousRevisionNumber(0.000000) -> null
     * 
     * @param transaction  transaction for the access to the database
     * @param revisionNumber  number of the revision for which the previous revision number is searched
     * @return previous revision number or null, if no previous revision exists
     */
    public Double getPreviousRevisionNumber(TitanTransaction transaction, Double revisionNumber) {
        Vertex revisionRootNode = getRoot(transaction);
        Vertex revisionNode = getSpecificVertex(revisionNumber, revisionRootNode);

        Double previousRevisionNumber = revisionNode.getProperty(NodeProperty.REVISION_NODE_PREVIOUS_REVISION.t());

        if (previousRevisionNumber == null || previousRevisionNumber == -1.0) {
            return null;
        } else {
            return previousRevisionNumber;
        }
    }

    /**
     * @return True, jestliže se má použít v systému verzování. 
     */
    public boolean isVersionSystemOn() {
        return isVersionSystemOn;
    }

    /**
     * @param shouldVersionSystemOn True, jestliže se má použít v systému verzování. 
     */
    public void setVersionSystemOn(boolean shouldVersionSystemOn) {
        this.isVersionSystemOn = shouldVersionSystemOn;
    }

    /**
     * @return true, jestliže se smít používat additional merge
     */
    public boolean isAdditionalMergeOn() {
        return isAdditionalMergeOn;
    }

    /**
     * @param shouldAdditionalMergeOn true, jestliže se smít používat additional merge
     */
    public void setAdditionalMergeOn(boolean shouldAdditionalMergeOn) {
        this.isAdditionalMergeOn = shouldAdditionalMergeOn;
    }

    /**
     * @param scriptMetadataService Sluzba pro praci s metadaty skriptu.
     */
    public void setScriptMetadataService(ScriptMetadataService scriptMetadataService) {
        this.scriptMetadataService = scriptMetadataService;
    }

    /**
     * @param licenseHolder Drzak licence.
     */
    public void setLicenseHolder(LicenseHolder licenseHolder) {
        this.licenseHolder = licenseHolder;
    }
}
