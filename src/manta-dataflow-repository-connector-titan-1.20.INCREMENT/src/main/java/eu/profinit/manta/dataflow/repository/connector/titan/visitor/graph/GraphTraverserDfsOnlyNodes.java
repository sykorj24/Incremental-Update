package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import java.util.List;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Algoritmus procházející graf stylem do hloubky. <br>
 * Zpracování jednoho objektu se provede dříve než zpracování jeho potomků a objektů se ho týkajícího.<br>
 * Algoritmus navštěvuje visitorem pouze vertexy typu resource a node. Nic jiného a ani žádné hrany.
 * @author tfechtner
 *
 */
public class GraphTraverserDfsOnlyNodes extends AbstractGraphTraverser {

    @Override
    public void traverse(GraphVisitor visitor, Vertex superRoot, RevisionInterval revisionInterval) {
        List<Vertex> resourceList = GraphOperation.getAdjacentVertices(superRoot, Direction.IN, revisionInterval,
                EdgeLabel.HAS_RESOURCE);
        for (Vertex resource : resourceList) {
            processLayersOfResource(visitor, resource, revisionInterval);
            processResource(visitor, resource, revisionInterval);
        }
    }

    @Override
    public void traverseOnlyOneResource(GraphVisitor visitor, Vertex resource, RevisionInterval revisionInterval) {
        processResource(visitor, resource, revisionInterval);
    }
    
    @Override
    protected void processResource(GraphVisitor visitor, Vertex resource, RevisionInterval revisionInterval) {        
        visitor.visitResource(resource);
        processResourceChildren(visitor, resource, revisionInterval);        
    }
    
    @Override
    protected void processNode(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval) {        
        visitor.visitNode(node);
        processNodeChildren(visitor, node, revisionInterval);        
    }
    
    @Override
    protected void processNodeEdges(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval) {
        // NOOP     
    }
}
