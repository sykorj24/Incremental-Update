package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor;

import java.util.List;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.VisitedPart;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;

/**
 * Procesor, který zpracovává elementy prefixově.
 * To znamená, že se nejprve navštíví samotný element a až se přidají do fronty jeho 
 * "podřízené" elementy.
 * @author tfechtner
 *
 */
public class TraverserProcessorPrefix extends AbstractTraverserProcessor<Object> {

    @Override
    protected void processResource(Vertex vertex) {
        processLayerOfResource(vertex);
        getVisitor().visitResource(vertex);

        List<Vertex> children = GraphOperation.getAdjacentVertices(vertex, Direction.IN, getRevisionInterval(),
                EdgeLabel.HAS_RESOURCE);
        for (Vertex child : children) {
            if (GraphOperation.getParent(child) == null) {
                getIdsToProcess().addLast(child.getId());
            }
        }
    }

    @Override
    protected void processNode(Vertex vertex) {
        getVisitor().visitNode(vertex);

        if (getVisitedParts().contains(VisitedPart.EDGES)) {
            List<Edge> edges = GraphOperation.getAdjacentEdges(vertex, Direction.OUT, getRevisionInterval(),
                    EdgeLabel.DIRECT, EdgeLabel.FILTER);
            for (Edge e : edges) {
                getVisitor().visitEdge(e);
            }
        }

        if (getVisitedParts().contains(VisitedPart.ATTRIBUTES)) {
            List<Vertex> attributes = GraphOperation.getAdjacentVertices(vertex, Direction.OUT, getRevisionInterval(),
                    EdgeLabel.HAS_ATTRIBUTE);
            for (Vertex attr : attributes) {
                getIdsToProcess().addLast(attr.getId());
            }
        }

        List<Vertex> children = GraphOperation.getAdjacentVertices(vertex, Direction.IN, getRevisionInterval(),
                EdgeLabel.HAS_PARENT);
        for (Vertex child : children) {
            getIdsToProcess().addLast(child.getId());
        }

    }
}