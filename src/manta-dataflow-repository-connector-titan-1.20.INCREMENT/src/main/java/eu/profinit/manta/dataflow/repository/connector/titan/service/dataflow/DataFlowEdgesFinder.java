package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow;

import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;

/**
 * Třída pro hledání dataflow hran. <br>
 * Hrany se hledají postupně do šířky, přičemž se jedná o upravený algoritmus pro hledání nejkratší cesty.
 * Úprava spočívá v počítání vzdálenosti, kde se promítá filtrace. 
 * @author tfechtner
 */
public class DataFlowEdgesFinder {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(DataFlowEdgesFinder.class);
    /** Mapa s nejkratšími vzdáleností, s kterou se dá dostat uzlu s daným klíčem. */
    private final Map<Long, Integer> nodeDistanceMap = new HashMap<Long, Integer>();
    /** Fronta uzlů ke zpracování. */
    private final Deque<Long> queueToProcess = new LinkedList<Long>();
    /** Nalezené hrany. */
    private final Set<FlowEdge> collectedEdges = new HashSet<FlowEdge>();
    /** Nově navštívené uzly, ze kterých se nepouštělo follow. */
    private Set<Vertex> newVisitedNodes;
    /** Mapa pro udržení informace, které uzly byly již zpracovány s jakou vzdáleností od startu. */
    private Map<Long, Integer> processedVetexIdsWithDistance = new HashMap<>();
    /** Transakce pro práci s db. */
    private final TitanTransaction transaction;
    /** Aktuální stav flow. */
    private final FlowState flowState;
    /** Držák na technické atributy. */
    private final TechnicalAttributesHolder technicalAttributes;

    /**
     * @param transaction Transakce pro práci s db.
     * @param flowState Aktuální stav flow.
     * @param technicalAttributes držák na technické atributy
     */
    public DataFlowEdgesFinder(TitanTransaction transaction, FlowState flowState,
            TechnicalAttributesHolder technicalAttributes) {
        Validate.notNull(technicalAttributes, "Holder for technical attributes must not be null.");
        this.transaction = transaction;
        this.flowState = flowState;
        this.technicalAttributes = technicalAttributes;
    }

    /**
     * @param startNodes množina startovních uzlů follow
     * @param direction směr follow
     * @param radius délka follow
     * @param newVisitedNodesOutput výstupní parametr pro nově navštívené uzly, smí být null, pak se nepoužívá
     * @return množina nalezených hran
     */
    public synchronized FlowFinderResult findEdges(final Set<Vertex> startNodes, Direction direction, int radius,
            Set<Vertex> newVisitedNodesOutput) {
        queueToProcess.clear();
        nodeDistanceMap.clear();
        collectedEdges.clear();
        newVisitedNodes = newVisitedNodesOutput;
        processedVetexIdsWithDistance.clear();

        for (Vertex v : startNodes) {
            Long id = (Long) v.getId();
            queueToProcess.add(id);
            nodeDistanceMap.put(id, 0);
            flowState.visitNode(v);
        }

        processDeque(direction, radius);
        return new FlowFinderResult(collectedEdges, processedVetexIdsWithDistance.keySet());
    }

    /**
     * Postupně zpracuje frontu.
     * @param direction směr pohybu
     * @param radius maximální vzdálenost follow
     */
    private void processDeque(Direction direction, int radius) {
        while (queueToProcess.size() > 0) {
            Long id = queueToProcess.pollFirst();

            Vertex actualVertex = transaction.getVertex(id);
            if (actualVertex != null) {
                processNeighborhoodOfVertex(direction, radius, actualVertex);
            } else {
                LOGGER.warn("Vertex with id " + id + " does not exist.");
            }
        }
    }

    /**
     * Zkontroluje, jestli nebylo dosazeno nějaké záklopky.
     * @param nodeStatus status navštěvovaného uzlu
     */
    protected void checkLimits(NodeStatus nodeStatus) {
        // NOOP
    }

    /**
     * Zpracuje sousedy daného uzlu.
     * <ul>
     *   <li>- pokud se nejedná o {@link NodeStatus#UNKNOWN} souseda, tak se zapamatuje hrana mezi nimi.</li>
     *   <li>- {@link NodeStatus#VISITED} a {@link NodeStatus#UNVISITED} sousedi se ověří na vložení do fronty</li>
     * </ul>
     * @param direction směr pohybu
     * @param radius maximální velikost follow
     * @param actualVertex uzel, pro nějž se zkoumá okolí
     */
    private void processNeighborhoodOfVertex(Direction direction, int radius, Vertex actualVertex) {
        Integer actualDistance = nodeDistanceMap.get(actualVertex.getId());
        if (actualDistance == null) {
            LOGGER.warn("Vertex " + actualVertex + " does not have record in the distance map.");
            return;
        }

        Integer processedDistance = processedVetexIdsWithDistance.get(actualVertex.getId());
        if (processedDistance != null && processedDistance <= actualDistance) {
            return;
        } else {
            processedVetexIdsWithDistance.put((Long) actualVertex.getId(), actualDistance);
        }

        // již jsme dosáhli na daném uzlu maxima follow -> ukončit follow a uzel poznačit
        if (newVisitedNodes != null && actualDistance >= radius) {
            newVisitedNodes.add(actualVertex);
            return;
        }

        // pro všechny sousedy v daném směru a s danými hranami
        Iterable<Edge> edgeSet = GraphOperation.getAdjacentEdges(actualVertex, direction,
                flowState.getRevisionInterval(), GraphOperation.getDataflowLabels(flowState.isFilterEdgesShown()));
        for (Edge edge : edgeSet) {
            Vertex adjacentVertex = edge.getVertex(direction.opposite());
            NodeStatus nodeStatus = flowState.getNodeStatus(adjacentVertex);

            Map<String, Object> attrs;
            checkLimits(nodeStatus);
            switch (nodeStatus) {
            case UNKNOWN:
                // není v referenčním view -> ignorovat
                continue;
            case UNVISITED:
            case VISITED:
                // ještě nebyl z daného uzlu spuštěn follow -> zpracovat a uložit hranu
                insertVertexIntoQueue(adjacentVertex, actualVertex);
                attrs = FlowEdge.fetchAttributes(edge, technicalAttributes);
                rememberEdge(actualVertex, adjacentVertex, direction, edge.getLabel(), attrs,
                        RevisionUtils.getRevisionInterval(edge));
                break;
            case FINISHED:
                // uzel již byl někdy v minulosti dokončen, nemá již cenu z něho posílat další
                // -> jen poznačit hranu
                attrs = FlowEdge.fetchAttributes(edge, technicalAttributes);
                rememberEdge(actualVertex, adjacentVertex, direction, edge.getLabel(), attrs,
                        RevisionUtils.getRevisionInterval(edge));
                break;
            default:
                throw new IllegalStateException("Vertex " + adjacentVertex + " has unknown state: " + nodeStatus + ".");
            }
        }
    }

    /**
     * Vloží uzel do fronty, pokud je splněna podmínka na vzdálenost. <br>
     * To znamená, že tento uzel ještě nebyl navštíven nebo se dá do něj přes zdrojový uzel
     * dostat v ostře menší vzdálenosti.
     * @param actualVertex aktuálně vkládaný uzel
     * @param sourceVertex zdroj, odkud se do něj přišlo
     */
    private void insertVertexIntoQueue(Vertex actualVertex, Vertex sourceVertex) {
        Integer sourceDistance = nodeDistanceMap.get(sourceVertex.getId());
        if (sourceDistance == null) {
            LOGGER.warn("Vertex " + sourceVertex + " does not have record in the distance map.");
            return;
        }

        Integer actualDistance = nodeDistanceMap.get(actualVertex.getId());
        Integer newDistance = sourceDistance + calcDeltaDistance(actualVertex, sourceVertex);
        // poize pokud ještě nebyl zpracován nebo se k němu tudy dostaneme kratší cestou
        if (actualDistance == null || newDistance.compareTo(actualDistance) < 0) {
            Long id = (Long) actualVertex.getId();
            nodeDistanceMap.put(id, newDistance);
            queueToProcess.addLast(id);
        }
    }

    /**
     * Spočte vzdálenost mezi dvěma uzly s ohledem na filtry.
     * @param actualVertex aktuální cílový uzel
     * @param sourceVertex zdrojový uzel
     * @return přímá vzdálenost mezi uzly s ohledem na filtr
     */
    protected Integer calcDeltaDistance(Vertex actualVertex, Vertex sourceVertex) {
        return flowState.isVertexFiltered(actualVertex) ? 0 : 1;
    }

    /**
     * @return Aktuální stav flow.
     */
    protected FlowState getFlowState() {
        return flowState;
    }

    /**
     * Zapamatuje hranu.
     * @param startVertex start hrany
     * @param endVertex konec hrany
     * @param direction směr hrany
     * @param label label hrany
     * @param attributes atributy hrany
     * @param revisionInterval interval platnosti hrany
     */
    private void rememberEdge(Vertex startVertex, Vertex endVertex, Direction direction, String label,
            Map<String, Object> attributes, RevisionInterval revisionInterval) {
        collectedEdges
                .add(new FlowEdge(startVertex, endVertex, direction, EdgeLabel.parseFromDbType(label), revisionInterval,
                        attributes));
    }

    /**
     * Třída držící výsledek z hledání hran.
     * @author tfechtner
     *
     */
    public static class FlowFinderResult {
        /** Nalezené hrany. */
        private final Set<FlowEdge> edges;
        /** Zpracované uzly, které byly algoritmem navštíveny. */
        private final Set<Long> processedVertices;

        /**
         * @param edges nalezené hrany
         * @param processedVertices id uzlů, které byly algoritmem navštíveny
         */
        public FlowFinderResult(Set<FlowEdge> edges, Set<Long> processedVertices) {
            super();
            this.edges = new HashSet<>(edges);
            this.processedVertices = new HashSet<>(processedVertices);
        }

        /**
         * @return nalezené hrany
         */
        public Set<FlowEdge> getEdges() {
            return edges;
        }

        /**
         * Provede visit operaci na všech uzlech, které byly v rámci počítání tohoto výsledku 
         * navštíveny algoritmem.
         * @param transaction transakce pro přístup k db
         * @param flowState aktuální flow state
         */
        public void visitProcessedVertices(TitanTransaction transaction, FlowState flowState) {
            for (Long id : processedVertices) {
                Vertex vertex = transaction.getVertex(id);
                flowState.visitNode(vertex);
            }
        }
    }

}
