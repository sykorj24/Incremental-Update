package eu.profinit.manta.dataflow.repository.connector.titan.filter.provider;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Identifikátor pro trojici vrchol-filtr-revize.
 * @author tfechtner
 *
 */
public class VertexFilterId {
    /** Id vrcholu. */
    private final Long vertexId;
    /** Interval platných revizí. */
    private final RevisionInterval revisionInterval;
    /** Id filtru. */
    private final String filterId;

    /**
     * @param vertexId Id vrcholu
     * @param revisionInterval Interval platných revizí
     * @param filterId Id filtru
     */
    public VertexFilterId(Long vertexId, RevisionInterval revisionInterval, String filterId) {
        Validate.notNull(vertexId, "Vertex ID must not be null.");
        Validate.notNull(revisionInterval, "Revision interval must not be null.");
        Validate.notNull(filterId, "Filter ID must not be null.");

        this.vertexId = vertexId;
        this.revisionInterval = revisionInterval;
        this.filterId = filterId;
    }

    /**
     * @return Id vrcholu
     */
    public Long getVertexId() {
        return vertexId;
    }

    /**
     * @return Interval platných revizí
     */
    public RevisionInterval getRevisionInterval() {
        return revisionInterval;
    }

    /**
     * @return Id filtru
     */
    public String getFilterId() {
        return filterId;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(vertexId);
        builder.append(revisionInterval);
        builder.append(filterId);
        return builder.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        VertexFilterId other = (VertexFilterId) obj;
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(vertexId, other.vertexId);
        builder.append(revisionInterval, other.revisionInterval);
        builder.append(filterId, other.filterId);
        return builder.isEquals();
    }
}
