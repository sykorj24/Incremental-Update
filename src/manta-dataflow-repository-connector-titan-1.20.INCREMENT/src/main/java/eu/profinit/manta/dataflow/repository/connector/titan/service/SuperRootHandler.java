package eu.profinit.manta.dataflow.repository.connector.titan.service;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;

/**
 * Třída pro správu super root uzlu databáze.
 * @author tfechtner
 *
 */
public class SuperRootHandler extends AbstractRootHandler {

    @Override
    public NodeProperty getNodeProperty() {
        return NodeProperty.SUPER_ROOT;
    }

    @Override
    public VertexType getVertexType() {
        return VertexType.SUPER_ROOT;
    }
}
