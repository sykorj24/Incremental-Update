package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Rozrhaní pro třídy pohybující se po grafu a volající visitor na jednotlivých objektech.
 * @author tfechtner
 *
 */
@ReplaceByClass(name = "UniversalTraverser")
public interface GraphTraverser {
    /**
     * Provede průchod grafem a na jednotlivých objektech volá visitora.
     * @param visitor používaný visitor pro návštěvu jednotlivých objektů
     * @param superRoot start prohledávání v superrootu grafu
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    void traverse(GraphVisitor visitor, Vertex superRoot, RevisionInterval revisionInterval);

    /**
     * Provede průchod grafem a na jednotlivých objektech volá visitora.
     * Neprochází se celý strom, ale pouze podstrom daného resource. 
     * @param visitor používaný visitor pro návštěvu jednotlivých objektů
     * @param resource resource, jehož podstrom se navštíví, včetně daného resource
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    @Unused(author = "Erik Kratochvíl")
    void traverseOnlyOneResource(GraphVisitor visitor, Vertex resource, RevisionInterval revisionInterval);
}
