package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.GraphFlowFilter;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Rozhraní pro algoritmy hledající uzly přes datové toky.
 * Algoritmy mohou mít přídavné filtry omezující výsledek.
 * Algoritmy mohou mít vlastní stav, počítá se s tím, že je jedna instance použitelná jen jednou.
 * @author tfechtner
 *
 */
public interface GraphFlowAlgorithm {

    /**
     * @return true, jestliže se má algoritmus pohybovat i po filter hranách
     */
    boolean getFilterEdgesEnabled();

    /**
     * @param value true, jestliže se má algoritmus pohybovat i po filter hranách
     */
    void setFilterEdgesEnabled(boolean value);

    /**
     * Nalezne vrcholy v grafu odpovídající algoritmu. Algoritmu lze omezit pomocí parametru
     * {@link allowedNodes} vrcholy po kterých se smí pohybovat. Pokud je tento parametr null
     * tak se pohybuje po všech.
     * @param startNodes Startovní uzly. <b>Read-only</b> kolekce, vstupní argument.
     * @param allowedNodes Povolené id uzlů, pouze na kterých se algoritmus může pohybovat.
     * @param dbTransaction Titan transakce.
     * @param direction Směr procházení musí být {@link Direction.IN} nebo {@link Direction.OUT}.
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     * @return množina nalezených id uzlů algoritmem
     * @throws GraphFlowAlgorithmException chyba v algoritmu
     */
    Set<Object> findNodesByAlgorithm(Collection<Vertex> startNodes, Set<Object> allowedNodes,
            TitanTransaction dbTransaction, Direction direction, RevisionInterval revisionInterval)
            throws GraphFlowAlgorithmException;

    /**
     * Nastaví seznam filtrů, přes které se kontroluje, jestli je vertex ok a jestli se má pokračovat.
     * @param filters seznam filtrů
     */
    void setFilters(List<GraphFlowFilter> filters);

    /**
     * Nastaví limit zpracování algoritmu, typicky je to maximální velikost fronty zpracování.
     * @param limit nový limit algoritmu
     */
    void setLimit(long limit);

}
