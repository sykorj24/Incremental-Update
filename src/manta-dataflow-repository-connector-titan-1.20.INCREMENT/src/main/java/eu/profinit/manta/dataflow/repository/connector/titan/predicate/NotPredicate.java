package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;


/**
 * Predikát pro negaci vnitřního predikátu.
 * @author tfechtner
 *
 */
public class NotPredicate implements VertexPredicate {

    private VertexPredicate vertexPredicate;

    /**
     * Default konstruktor, pro správné fungování je ještě třeba nastvit field {@link #vertexPredicate}.
     */
    public NotPredicate() {
        super();
    }

    /**
     * @param vertexPredicate predikát, jehož výsledek se neguje
     */
    public NotPredicate(VertexPredicate vertexPredicate) {
        super();
        this.vertexPredicate = vertexPredicate;
    }

    @Override
    public boolean evaluate(Vertex vertex, RevisionInterval revisionInterval) {
        if (vertexPredicate == null) {
            throw new IllegalStateException("The inner vertexPredicate must not be null.");
        }

        return !vertexPredicate.evaluate(vertex, revisionInterval);
    }

    /**
     * @param vertexPredicate predikát, jehož výsledek se neguje
     */
    public void setVertexPredicate(VertexPredicate vertexPredicate) {
        this.vertexPredicate = vertexPredicate;
    }
}
