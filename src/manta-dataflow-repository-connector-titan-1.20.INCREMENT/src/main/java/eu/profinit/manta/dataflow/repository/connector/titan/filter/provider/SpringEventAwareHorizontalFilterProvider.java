package eu.profinit.manta.dataflow.repository.connector.titan.filter.provider;

import java.util.List;
import java.util.Set;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.PriorityOrdered;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.ContextEventAwareOrder;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilter;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Decorater provider horizontálních filtrů, který provede refresh na vnitřním provideru při načtení spring contextu.
 * @author tfechtner
 *
 */
public class SpringEventAwareHorizontalFilterProvider
        implements HorizontalFilterProvider, ApplicationListener<ContextRefreshedEvent>,
        PriorityOrdered {

    /** Dekorovaný provider. */
    private HorizontalFilterProvider innerProvider;

    @Override
    public int getOrder() {
        return ContextEventAwareOrder.HORIZONTAL_FILTERS.getOrder();
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        innerProvider.refreshFilters();
    }

    @Override
    public HorizontalFilter getFilter(String filterId) {
        return innerProvider.getFilter(filterId);
    }

    @Override
    public boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval, Set<String> activeFilters) {
        return innerProvider.isFiltered(vertex, revisionInterval, activeFilters);
    }

    @Override
    public boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval, String filterId) {
        return innerProvider.isFiltered(vertex, revisionInterval, filterId);
    }

    @Override
    public void addFilters(Set<HorizontalFilter> filterSet) {
        innerProvider.addFilters(filterSet);
    }

    @Override
    public int refreshFilters() {
        return innerProvider.refreshFilters();
    }

    @Override
    public List<HorizontalFilter> getFilters() {
        return innerProvider.getFilters();
    }

    /**
     * @param innerProvider vnitřní provider
     */
    public void setInnerProvider(HorizontalFilterProvider innerProvider) {
        this.innerProvider = innerProvider;
    }
}
