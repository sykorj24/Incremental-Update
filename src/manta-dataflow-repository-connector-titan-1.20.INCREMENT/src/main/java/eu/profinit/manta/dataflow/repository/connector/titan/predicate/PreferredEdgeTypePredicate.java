package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import org.apache.commons.lang3.Validate;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;

import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.UniversalTraverser;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.EdgePredicate;

/**
 * <p>
 * Hranový predikát vrací false, pokud daná hrana není preferovaného typu
 * a přitom do tohoto vrcholu vede jiná hrana preferovaného typu.
 * </p><p>
 * Predikát je určen primárně pro případy, kdy je nutno projít hierarchii vrcholů pomocí
 * {@link UniversalTraverser} - v hierarchii existují vrcholy, které mají jiný resource než jejich rodič.
 * Do těchto vrcholů je žádoucí vstoupit přes hranu typu HAS_PARENT (preferovaný typ),
 * zatímco hranu typu HAS_RESOURCE je v tomto případě žádoucí ignorovat.
 * </p>
 *
 * @author Erik Kratochvíl
 */
public final class PreferredEdgeTypePredicate implements EdgePredicate {

    /**
     * Predikát odmítající vejít do vrcholu přes hranu typu HAS_RESOURCE,
     * pokud do vrcholu lze vstoupit i přes hrana typu HAS_PARENT.
     */
    public static final PreferredEdgeTypePredicate PREFER_PARENT_OVER_RESOURCE = new PreferredEdgeTypePredicate(
            EdgeLabel.HAS_PARENT);

    /**
     * Preferovaný typ hrany.
     */
    private final String preferredEdgeLabel;

    /**
     * Predikát odmítající vejít do vrcholu přes jiný typ hrany,
     * pokud je do vrcholu možno vstoupit přes preferovaný typ hrany.
     *
     * @param preferredEdgeLabel preferovaný typ hrany
     */
    public PreferredEdgeTypePredicate(final EdgeLabel preferredEdgeLabel) {
        Validate.notNull(preferredEdgeLabel,
                "Preferred edge label must be defined. Hint: check parameters of the constructor.");
        this.preferredEdgeLabel = preferredEdgeLabel.t();
    }

    @Override
    public boolean evaluate(final Edge edge, final Direction direction) {
        // Opačný směr.
        final Direction oppositeDirection = direction.opposite();

        // Pokud přecházíme po preferovaném typu hrany, není co řešit.
        if (preferredEdgeLabel.equals(edge.getLabel())) {
            return true;
        }

        // V tento moment víme, že přecházíme po nepreferovaném typu hrany.
        // To smíme udělat pouze v případě, že do vrcholu nevede současně ještě preferovaný typ hrany.
        return !edge.getVertex(oppositeDirection).getEdges(oppositeDirection, preferredEdgeLabel).iterator().hasNext();
    }

}
