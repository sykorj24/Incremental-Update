package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import java.util.Set;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;

/**
 * Evaluátor kontrolující typ vrcholů oproti zadané množině přípustných hodnot.
 * @author tfechtner
 *
 */
public class VertexTypePredicate extends VertexPropertyPredicate {

    /**
     * Default konsturktor, nutno nastavit field {@link #propertyName} pro 
     * správné fungování třídy. 
     */
    public VertexTypePredicate() {
        super();
        setPropertyName(NodeProperty.NODE_TYPE.t());
    }

    /**
     * @param valueSet Název property vrcholu k ověření.
     */
    public VertexTypePredicate(Set<String> valueSet) {
        super(valueSet, NodeProperty.NODE_TYPE.t());
    }

}
