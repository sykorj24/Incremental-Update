package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import java.util.Arrays;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;

/**
 * Evaluátor kontrolující podmínku, že zkoumaný vrchol nemá odchozí hrany daných typů.
 * @author tfechtner
 *
 */
public class SinkPredicate implements VertexPredicate {
    /** Pole labelů, jejichž hrany se hledají.  */
    private EdgeLabel[] labels = new EdgeLabel[0];
    
    @Override
    public boolean evaluate(Vertex vertex, RevisionInterval revisionInterval) {
        return GraphOperation.getAdjacentEdges(vertex, Direction.OUT, revisionInterval, labels).isEmpty();
    }

    /**
     * @param labels Pole labelů, jejichž hrany se hledají.
     */
    public void setLabels(EdgeLabel[] labels) {
        if (labels != null) {
            this.labels = Arrays.copyOf(labels, labels.length);
        } else {
            this.labels = new EdgeLabel[0];
        }
    }
}
