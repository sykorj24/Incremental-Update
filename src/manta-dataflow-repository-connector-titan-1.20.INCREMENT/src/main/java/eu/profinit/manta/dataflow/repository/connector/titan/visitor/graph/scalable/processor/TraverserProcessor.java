package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor;

import java.util.Deque;
import java.util.List;

import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.VisitedPart;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrder;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverser;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;

/**
 * Rozhraní pro traverzovací procesor v rámci {@link GraphScalableTraverser}.
 * Vrcholy ke zpracování se ukládájí do fornty sdílené s {@link GraphScalableTraverser},
 * díky čemuž zpracování celého grafu může probíhat ve více sekvenčních transakcích.
 * @author tfechtner
 *
 * @param <T> návratová hodnota procesoru
 */
public interface TraverserProcessor<T> extends TransactionCallback<T>  {
    /**
     * @param visitor Visitor navštěvující jednotlivé elementy v grafu.
     */
    void setVisitor(GraphVisitor visitor);

    /**
     * @param revisionInterval interval revizí, které procesor prochází
     */
    void setRevisionInterval(RevisionInterval revisionInterval);

    /**
     * @param idsToProcess sdílená fronta id vrcholů ke zpracování
     */
    void setIdsToProcess(Deque<Object> idsToProcess);

    /**
     * @param objectsPerTransaction počet objektů ke zpracování v rámci této transakce
     */
    void setObjectsPerTransaction(int objectsPerTransaction);

    /**
     * @param moduleName název modelu pro kontrolu licence v rámci databáze 
     */
    void setModuleName(String moduleName);

    /**
     * @param searchOrder Styl prohledávání grafu 
     */
    void setSearchOrder(SearchOrder searchOrder);

    /**
     * @return Styl prohledávání grafu 
     */
    SearchOrder getSearchOrder();

    /**
     * @param visitedParts navštěvované objekty při průchodu stromem
     */
    void setVisitedParts(List<VisitedPart> visitedParts);
}
