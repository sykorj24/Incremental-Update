package eu.profinit.manta.dataflow.repository.connector.titan.predicate;


import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;

/**
 * Evaluátor kontrolující podmínku, že uzel je list.
 * 
 * @author jtousek
 */
public class IsLeafPredicate implements VertexPredicate {

    @Override
    public boolean evaluate(Vertex vertex, RevisionInterval revisionInterval) {
        return GraphOperation.isLeaf(vertex, revisionInterval);
    }

}

