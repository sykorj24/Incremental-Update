package eu.profinit.manta.dataflow.repository.connector.titan.service;

/**
 * Výjimka pro případ chyby v rámci reviziního systému metadat.
 * Například commitování comitnuté verze.
 * @author tfechtner
 *
 */
public class RevisionException extends RuntimeException {

    private static final long serialVersionUID = -7038959416462821973L;

    /**
     * @param message text výjimky
     * @param cause důvod výjimky
     */
    public RevisionException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message text výjimky
     */
    public RevisionException(String message) {
        super(message);
    }

}
