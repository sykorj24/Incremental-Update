package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Algoritmus procházející dataflow DFS stylem.
 * Nepoužívá rekurzi, je tedy vhodný i pro dlouhé toky.
 * 
 * @author tfechtner
 */
public class FlowTraverserDfs extends AbstractFlowTraverser {
    /** Maximální velikost fronty pro zpracování. */
    private static final int DEQUE_MAX_SIZE = 30000;

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(FlowTraverserDfs.class);

    private Deque<FlowItem> stack;

    private Set<Object> visitedIds = new HashSet<Object>();

    /**
     * Default konstruktor inicializující vnitřní struktury.
     */
    public FlowTraverserDfs() {
        super();
        initQueue();
    }

    @Override
    protected void traverseOneDirection(TitanTransaction transaction, Iterable<Object> startNodeIds,
            EdgeTypeTraversing edgeTraversing, Direction direction, RevisionInterval revisionInterval) {

        Iterator<Object> startNodeIdsIterator = startNodeIds.iterator();

        initQueue();

        // dokud neni fronta prazda, vyjmi posledni par (hrana, uzel)
        // a navstiv je
        while (!isQueueEmpty() || startNodeIdsIterator.hasNext()) {
            FlowItem item = null;

            // Pokud je fronta prazna, vyber dalsi startovaci uzel
            // ze iteratoru startovacich uzlu. Jinak vyber dalsi uzel
            // z fronty. Toto ma zabranit nutnosti zbytecne kopirovat
            // vstupni uzly na frontu.
            if (isQueueEmpty()) {
                item = new FlowItem(null, (Long) startNodeIdsIterator.next());
            } else {
                item = removeFromQueue();
            }

            Vertex node = transaction.getVertex(item.getVertexId());

            // pokud byl jiz uzel navstiven (nezavisle na prichozi hrane)
            if (!shouldProcess(item)) {
                continue;
            }

            // pokud vizitori neschvali tento uzel,
            // tak dal nepokracuj (naslednici nebudou vlozeni do fronty)
            if (!visit(node, item).shouldContinue()) {
                continue;
            }

            // ziskat hrany incidujici s aktualnim uzlem ve zvolenem smeru
            long time = System.currentTimeMillis();
            Iterator<Edge> edges = edgeTraversing.getNextEdges(node, direction, revisionInterval).iterator();
            if (System.currentTimeMillis() - time > TOO_LONG_ADJACENT_VERTICES) {
                LOGGER.info("Fetching adjacent(" + edgeTraversing + ") vertices for node " + node.getId() + " last "
                        + (System.currentTimeMillis() - time) + "ms.");
            }

            // nasypat nové vrcholy do fronty
            while (edges.hasNext()) {
                Edge outgoingEdge = edges.next();
                Vertex neighbour = outgoingEdge.getVertex(direction.opposite());

                // kontrola na velikost fronty
                if (getQueueSize() > DEQUE_MAX_SIZE) {
                    LOGGER.warn("Cannot insert vertex into the deque due the capacity restrictions.");
                    return;
                }

                // vložit do fronty
                if (!addToQueue(new FlowItem(outgoingEdge, (Long) neighbour.getId(), item))) {
                    LOGGER.warn("Cannot insert vertex into the deque due the capacity restrictions.");
                }
            }
        }
        visitedIds.clear();
    }

    private boolean addToQueue(FlowItem item) {
        // kontrola na velikost fronty
        if (getQueueSize() > DEQUE_MAX_SIZE) {
            LOGGER.warn("Cannot insert vertex into the deque due the capacity restrictions.");
            return false;
        }

        // vložit do fronty
        if (!stack.offerLast(item)) {
            LOGGER.warn("Cannot insert vertex into the deque due the capacity restrictions.");
            return false;
        }

        return true;
    }

    private FlowItem removeFromQueue() {
        return stack.pollLast();
    }

    /**
     * Jestli se má zpracovat daný item.
     * @param item ověřovaný item
     * @return true, jestliže se má zpracovat
     */
    protected boolean shouldProcess(FlowItem item) {
        return visitedIds.add(item.getVertexId());
    }

    private void initQueue() {
        stack = new ArrayDeque<FlowItem>();
    }

    private boolean isQueueEmpty() {
        return stack.isEmpty();
    }

    private int getQueueSize() {
        return stack.size();
    }

    /**
     * @return množina idéček již navštívených objektu
     */
    protected Set<Object> getVisitedIds() {
        return visitedIds;
    }    
}
