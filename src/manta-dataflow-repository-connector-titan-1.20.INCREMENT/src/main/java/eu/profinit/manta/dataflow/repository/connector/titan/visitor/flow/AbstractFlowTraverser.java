package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow;

import java.util.ArrayList;
import java.util.List;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterResult;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Abstraktni trida pro traversery.
 * 
 * <p>
 * Zakladni funkcionalita, kterou tato trida poskytuje:
 * 
 * <ul>
 *  <li>seznam visitoru a metody pro jeho manipulaci ({@link #addVisitor(FlowVisitor)},
 *      {@link #removeVisitor(FlowVisitor)}, atp.),
 *  <li>zakladni implementaci {@link traverse()} metody, ktera vola
 *      {@link #traverseOneDirection()} v obou smerech nebo jenom jednom
 *      v zavislosti na parametru {@code direction}.
 * </ul>
 * 
 * @author Jan Milik <milikjan@fit.cvut.cz>
 */
public abstract class AbstractFlowTraverser implements FlowTraverser {
    /** Limit pro logování příliš dlouhého získání připojených uzlů. */
    public static final int TOO_LONG_ADJACENT_VERTICES = 100;

    private List<FlowVisitor> visitors = new ArrayList<FlowVisitor>();

    /**
     * @param node uzel k navštívení
     * @param item item reprezentující context zpracování
     * @return výsledek jestli se má pokračovat
     */
    protected FilterResult visit(Vertex node, FlowItem item) {
        FilterResult result = FilterResult.OK_CONTINUE;
        for (FlowVisitor visitor : visitors) {
            result = result.sumWith(visitor.visitAndContinue(node, item));
        }
        return result;
    }

    @Override
    public void addVisitor(FlowVisitor visitor) {
        visitors.add(visitor);
    }

    @Override
    public void removeVisitor(FlowVisitor visitor) {
        visitors.remove(visitor);
    }

    @Override
    public void traverse(TitanTransaction transaction, Iterable<Object> startNodeIds, EdgeTypeTraversing edgeType,
            Direction direction, RevisionInterval revisionInterval) {
        if (direction == Direction.BOTH) {
            // oba směry od startovnách uzlů
            traverseOneDirection(transaction, startNodeIds, edgeType, Direction.IN, revisionInterval);
            traverseOneDirection(transaction, startNodeIds, edgeType, Direction.OUT, revisionInterval);
        } else {
            // pouze jeden směr
            traverseOneDirection(transaction, startNodeIds, edgeType, direction, revisionInterval);
        }
    }

    /**
     * Projde flow jedním směrem.
     * 
     * @param transaction transakce pro přístup k db
     * @param startNodeIds id počátečních uzl;, z kterých se flow hledají 
     * @param edgeTraversing způsob procházení přes uzly
     * @param direction směr procházení
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    protected abstract void traverseOneDirection(TitanTransaction transaction, Iterable<Object> startNodeIds,
            EdgeTypeTraversing edgeTraversing, Direction direction, RevisionInterval revisionInterval);

    @Override
    public void traverse(FlowVisitor visitor, TitanTransaction transaction, Iterable<Object> startNodes,
            EdgeTypeTraversing edgeType, Direction direction, RevisionInterval revisionInterval) {
        addVisitor(visitor);
        try {
            traverse(transaction, startNodes, edgeType, direction, revisionInterval);
        } finally {
            removeVisitor(visitor);
        }
    }
}
