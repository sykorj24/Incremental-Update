package eu.profinit.manta.dataflow.repository.connector.titan.service.collection;

import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.connector.titan.service.ExecutorServiceTerminator;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

/**
 * Procesor zpracovávající paralelně kolekci v rámci transakcí v databázi.
 * @author tfechtner
 *
 */
public class CollectionParallelProcessor implements CollectionProcessor {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(CollectionParallelProcessor.class);

    /** Název modulu pro přístup k databázi. */
    private static final String MODULE_NAME = "mr_basic";
    /** výchozí počet zpracovaných elementů za transakci. */
    private static final int DEFAULT_ELEMENTS_PER_TRANSACTION = 500;

    /** Počet paralelních vláken. */
    private int threadNumber = 4;
    /** Počet zpracovaných elementů za transakci. */
    private int elementPerTransaction = DEFAULT_ELEMENTS_PER_TRANSACTION;

    @Override
    public <E, R> void processCollection(DatabaseHolder databaseHolder, TransactionLevel transactionLevel,
            Collection<E> collection, CollectionProcessorCallback<E, R> callback, R resultHolder) {

        Validate.notNull(databaseHolder, "Database holder mustn't be null.");
        Validate.notNull(collection, "Collection mustn't be null.");
        Validate.notNull(callback, "Processor callback mustn't be null.");

        // Fronta elementů ke zrpacování
        Queue<E> queue = new ConcurrentLinkedQueue<>(collection);

        // závora udržující informaci, kolik pracovních vláken ještě definitivně neskončilo
        CountDownLatch shutdownLatch = new CountDownLatch(threadNumber);

        // executor pro správu vláken, má threadNumber výkoných vláken a jedno ukončovací.
        ExecutorService executorService = Executors.newFixedThreadPool(threadNumber + 1);

        // nastarovat zastavovač, který čeká, až skončí pracovní vlákna
        executorService.submit(new ExecutorServiceTerminator(executorService, shutdownLatch));

        // nastartovat pracovní vlákna
        for (int i = 0; i < threadNumber; i++) {
            CallbackLauncher<E, R> launcher = new CallbackLauncher<>(databaseHolder, transactionLevel, queue, callback,
                    resultHolder, shutdownLatch, elementPerTransaction);
            executorService.submit(launcher);
        }

        // čekáme až vše doběhne
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.warn("Executor Service interrupted.", e);
            List<Runnable> threads = executorService.shutdownNow();
            for (Runnable thread : threads) {
                LOGGER.warn("Processor has not been started: " + thread.toString());
            }
            Thread.currentThread().interrupt();
        }
    }

    /**
     * @return Počet paralelních vláken
     */
    public int getThreadNumber() {
        return threadNumber;
    }

    /**
     * @param threadNumber Počet paralelních vláken
     */
    public void setThreadNumber(int threadNumber) {
        this.threadNumber = threadNumber;
    }

    /**
     * @return počet zpracovaných elementů za transakci
     */
    public int getElementPerTransaction() {
        return elementPerTransaction;
    }

    /**
     * @param elementPerTransaction počet zpracovaných elementů za transakci
     */
    public void setElementPerTransaction(int elementPerTransaction) {
        this.elementPerTransaction = elementPerTransaction;
    }

    /**
     * Pracovní vlákno provádějící samotné zpracovávání elementů včetně průběžného commitování transakcí.
     * @author tfechtner
     *
     * @param <E> typ elementu
     * @param <R> výsledek zpracování
     */
    private static class CallbackLauncher<E, R> implements Runnable, TransactionCallback<Integer> {
        /** Držák na databázi. */
        private final DatabaseHolder databaseHolder;
        /**Požadovaná úroveň přístupu k databázi. */
        private final TransactionLevel transactionLevel;
        /** Callback, který se volá na každý element. */
        private final CollectionProcessorCallback<E, R> callback;
        /** Výsledek zpracování. */
        private final R resultHolder;
        /** Fronta elementů ke zpracování. */
        private final Queue<E> queue;
        /** Závora udržující informaci, kolik pracovních vláken ještě definitivně neskončilo. */
        private final CountDownLatch shutdownLatch;
        /** Počet zpracovaných elementů za transakci. */
        private final int elementPerTransaction;

        /**
         * @param databaseHolder držák na databázi
         * @param transactionLevel požadovaná úroveň přístupu k databázi
         * @param queue Fronta elementů ke zpracování
         * @param callback callback, který se volá na každý element
         * @param resultHolder výsledek zpracování
         * @param shutdownLatch závora udržující informaci, kolik pracovních vláken ještě definitivně neskončilo
         * @param elementPerTransaction Počet zpracovaných elementů za transakci
         */
        CallbackLauncher(DatabaseHolder databaseHolder, TransactionLevel transactionLevel, Queue<E> queue,
                CollectionProcessorCallback<E, R> callback, R resultHolder, CountDownLatch shutdownLatch,
                int elementPerTransaction) {
            super();
            this.callback = callback;
            this.resultHolder = resultHolder;
            this.queue = queue;
            this.shutdownLatch = shutdownLatch;
            this.databaseHolder = databaseHolder;
            this.transactionLevel = transactionLevel;
            this.elementPerTransaction = elementPerTransaction;
        }

        @Override
        public void run() {
            try {
                int processed;
                do {
                    processed = databaseHolder.runInTransaction(transactionLevel, this);
                } while (processed > 0);
            } catch (Throwable t) {
                LOGGER.error("Error during processing.", t);
            } finally {
                shutdownLatch.countDown();
            }
        }

        @Override
        public Integer callMe(TitanTransaction transaction) {
            int processedElements = 0;
            E element;
            while ((element = queue.poll()) != null) {
                callback.processElement(transaction, element, resultHolder);
                if (processedElements++ >= elementPerTransaction) {
                    break;
                }
            }

            return processedElements;
        }

        @Override
        public String getModuleName() {
            return MODULE_NAME;
        }
    }
}
