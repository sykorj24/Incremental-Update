package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

/**
 * Anotace pro třídy a metody, které jsou navrženy k nahrazení jinou metodou.
 * Tj. jde o jakýsi doplněk k anotaci @Deprecated
 * 
 * @author Erik Kratochvíl
 */
public @interface ReplaceByClass {
    /**
     * @return jméno třídy, kterou má být tato třída nahrazena
     */
    String name() default "n/a";

    /**
     * @return jméno metody, kterou má být tato metoda nahrazena
     */
    String method() default "n/a";

    /**
     * @return přesný způsob volání, kterým má být metoda nahrazena
     */
    String usage() default "n/a";
}
