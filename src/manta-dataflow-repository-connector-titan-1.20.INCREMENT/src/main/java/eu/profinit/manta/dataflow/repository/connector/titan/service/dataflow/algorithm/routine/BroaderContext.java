package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.routine;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.tinkerpop.blueprints.Vertex;

/**
 * Tzv. širší kontext (širší oproti {@link CallContext}, protože postihuje širší spektrum volání) pro detekci rekruzivních volání.
 * CallContext uvažuje pozici call number ve stacku volání,
 * BroaderContext uvažuje pouze rekurzivní call number v daném vnějším volání.
 *
 * Lépe vysvětleno v
 * https://mantatools.atlassian.net/browse/DEV-1713
 *
 * @author Erik Kratochvíl
 */
public class BroaderContext {

    /** Identifikátor prvního (vnějšího) volání funkce - nutno identifikovat stackem. */
    private final Stack outerCallStack;

    /** Identifikátor rekurzivního (vnitřního) volání funkce - nutno identifikovat číslem = šířeji. */
    private final String callNumber;

    /** Vrchol. */
    private final Vertex vertex;

    /**
     * @param outerCallStack identifikátor prvního (vnějšího) volání funkce - stackem
     * @parma callNumber identifikátor rekurzivního (vnitřního) volání funkce - číslem volání
     * @param vertex řešený vrchol
     */
    public BroaderContext(final Stack outerCallStack, final String callNumber, final Vertex vertex) {
        Validate.notNull(outerCallStack);
        Validate.notNull(callNumber);
        Validate.notNull(vertex);

        this.callNumber = callNumber;
        this.outerCallStack = outerCallStack;
        this.vertex = vertex;
    }

    /**
     * @return identifikátor rekurzivního volání
     */
    public String getCallNumber() {
        return this.callNumber;
    }

    /**
     * @return identifikátor vnějšího volání, kterým jsme do rekurze vstoupili
     */
    public Stack getOuterCallStack() {
        return this.outerCallStack;
    }

    /**
     * @return řešený vrchol
     */
    public Vertex getVertex() {
        return this.vertex;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.callNumber).append(this.outerCallStack).append(this.vertex).toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }

        final BroaderContext other = (BroaderContext) obj;
        return new EqualsBuilder().append(this.callNumber, other.callNumber).append(this.outerCallStack, other.outerCallStack)
                .append(this.vertex, other.vertex).isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("callNumber", this.callNumber).append("outerCallStack", this.outerCallStack).append("vertex", this.vertex)
                .toString();
    }

}
