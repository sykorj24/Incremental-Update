package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model;

import java.util.Set;

/**
 * Výjimka pro případ chyby v rámci zpracování algoritmu.
 * @author tfechtner
 *
 */
public class GraphFlowAlgorithmException extends Exception {

    private static final long serialVersionUID = 4447379101236389556L;
    /** Id vrcholů, které byly nalezeny algoritmem před chybou. */
    private Set<Object> outputNodes;

    /**
     * @param outputNodes Id vrcholů, které byly nalezeny algoritmem před chybou.
     * @param message zpráva výjimky
     */
    public GraphFlowAlgorithmException(Set<Object> outputNodes, String message) {
        super(message);
        this.outputNodes = outputNodes;
    }
    
    /**
     * @param outputNodes Id vrcholů, které byly nalezeny algoritmem před chybou.
     * @param message zpráva výjimky
     * @param t cause výjimka
     */
    public GraphFlowAlgorithmException(Set<Object> outputNodes, String message, Throwable t) {
        super(message, t);
        this.outputNodes = outputNodes;
    }

    /**
     * @return Id vrcholů, které byly nalezeny algoritmem před chybou.
     */
    public Set<Object> getOutputNodes() {
        return outputNodes;
    }
}
