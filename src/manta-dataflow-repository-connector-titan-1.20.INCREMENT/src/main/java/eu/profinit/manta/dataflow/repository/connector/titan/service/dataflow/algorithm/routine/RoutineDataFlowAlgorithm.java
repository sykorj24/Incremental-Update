package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.routine;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.AttributeNames;
import eu.profinit.manta.dataflow.model.NodeType;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.AbstractGraphFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterResult;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.EdgeTypeTraversing;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Tento algoritmus implementuje průchod grafem přes datové toky.
 *
 *
 * @author Jan Milik <milikjan@fit.cvut.cz>
 * @author Radomír Polách
 * @author Jiří Toušek
 * @author Erik Kratochvíl
 */
public class RoutineDataFlowAlgorithm extends AbstractGraphFlowAlgorithm {

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(RoutineDataFlowAlgorithm.class);

    /** Maximální velikost fronty vrcholů pro zpracování. */
    private static final int DEFAULT_DEQUE_MAX_SIZE = 250000;

    /** Typy vrcholů, jejichž potomci jsou považováni za lokální kontext volání. */
    private static final Set<String> LOCAL_CONTEXT_TYPES = new HashSet<String>(Arrays.asList(
            NodeType.PROCEDURE.getId(),
            NodeType.FUNCTION.getId(),
            NodeType.MACRO.getId(),
            NodeType.TEMPORARY_MACRO.getId(),
            /*
             * Kvůli chování Teradata konektoru byl přidán ještě BTEQ_SCRIPT.
             * --------------------------------------------------------------
             * Teradata konektor totiž objekty, které vznikají uvnitř procedur/funkcí automaticky vkládá do hierarchie rovnou pod skript
             * a nikoli pod danou proceduru/funkci. RoutineDataFlowAlgorithm (RDFA) si pak při průchodu přes tyto objekty myslí,
             * že opouští lokální kontext procedury/funkce, přepne se na globální stack (stackRoot),
             * čímž efektivně zahodí všechny informace o vnořených voláních a dál pak už pokračuje nerušeně všemi východy.
             * Oracle ani MSSQL se to netýká, protože tam jsou automaticky inkriminované objekty linkovány přímo pod proceduru/funkci,
             * takže RDFA korektně detekuje, že volání zůstává lokální.
             * --------------------------------------------------------------
             * Detailní popis problému:
             * https://mantatools.atlassian.net/browse/DEV-1731
             */
            NodeType.BTEQ_SCRIPT.getId()
            ));

    private long dequeMaxSize = DEFAULT_DEQUE_MAX_SIZE;

    /**
     * Přepínač tzv. verbose režimu.
     * Ve verbose režimu je rozsah zpráv v logu výrazně bohatší.
     */
    private boolean verboseMode = LOGGER.isTraceEnabled();

    /**
     * Konstruktor předefinovávající typ hran na {@link EdgeTypeTraversing.DIRECT}.
     */
    public RoutineDataFlowAlgorithm() {
        super();
        this.setEdgeType(EdgeTypeTraversing.DIRECT);
    }

    /**
     * @param verboseMode aktivuje verbose režim, ve kterém je výrazně bohatší rozsah zpráv zapisovaných do logu
     */
    public void setVerboseMode(final boolean verboseMode) {
        this.verboseMode = verboseMode;
    }

    @Override
    protected Set<Object> findNodesByAlgorithmInternal(final Collection<Vertex> startNodes, final Set<Object> allowedNodes,
            final TitanTransaction dbTransaction, final Direction direction, final RevisionInterval revisionInterval)
                    throws GraphFlowAlgorithmException {
        return this.collectNodesInDirection(direction, startNodes, allowedNodes, revisionInterval);
    }

    /**
     * Inicializace fronty vrcholů pro zpracování - vložení všech vstupních vrcholů.
     *
     * @param startNodes výchozí vrcholy
     * @param stack sdílený stack volání funkcí
     * @return nová fronta vrcholů pro zpracování naplněná výchozími vrcholy
     */
    private Queue<FlowContext> initQueue(final Collection<Vertex> startNodes, final Stack stack) {
        final Queue<FlowContext> queue = new ArrayDeque<FlowContext>();
        for (final Vertex vertex : startNodes) {
            queue.add(new FlowContext(new CallContext(stack, vertex), null));
        }
        return queue;
    }

    /**
     * Najde přípustné uzly v zadaném směru.
     *
     * @param direction směr
     * @param startNodes startovní uzly
     * @param allowedNodes povolené ID uzlů z předchozích algoritmů nebo null, pokud smíme kamkoliv
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     *
     * @return množina ID uzlů, které jsou dle tohoto algoritmu přípustné
     * @throws GraphFlowAlgorithmException chyba v algoritmu
     */
    private Set<Object> collectNodesInDirection(final Direction direction, final Collection<Vertex> startNodes,
            final Set<Object> allowedNodes, final RevisionInterval revisionInterval) throws GraphFlowAlgorithmException {

        // Globální stack volání, společný pro průchod všemi vrcholy.
        final Stack stackRoot = new Stack();
        // Fronta pro zpracování "vrcholů" (ve skutečnosti se zvažuje trojice <vrchol, vstupní hrana, stack>).
        final Queue<FlowContext> queue = this.initQueue(startNodes, stackRoot);


        // Seznam všech navštívených vrcholů (s daným stackem).
        final Set<CallContext> visited = new HashSet<CallContext>();


        // Výsledek - seznam všech nalezených vrcholů.
        final Set<Object> outputNodes = new HashSet<Object>();


        // Statistika návštěv vrcholů.
        // Plní se pouze ve verboseMode, v ostrém provozu by zabírala příliš mnoho místa, proto zůstává prázdná.
        final Map<String, Integer> numberOfVisits = new HashMap<>();

        // Pouze pro tzv. return vertexy v rekurzivních voláních kvůli včasnému zařezávání rekurze.
        final Set<BroaderContext> visitedRecursive = new HashSet<>();

        // PRŮCHOD GRAFEM
        while (!queue.isEmpty()) {
            final FlowContext item = queue.poll();
            final Edge edge = item.getEdge();
            final Stack stack = item.getCallContext().getStack();
            final Vertex vertex = item.getCallContext().getVertex();

            if (allowedNodes != null && !allowedNodes.contains(vertex.getId())) {
                // Vrchol se nachází se mimo povolenou zónu.
                continue;
            }

            // Jméno vrcholu zapisované do logu.
            // V debuggovacím režimu jméno obsahuje výrazně více informací.
            final String vertexName = this.verboseMode ? qualifiedNameOf(vertex) : "v[" + vertex.getId() + "]";

            if (this.verboseMode) {
                // Zvýšení počtu návštěv daného vrcholu.
                Integer i = numberOfVisits.get(vertexName);
                if (i == null) {
                    i = 0;
                }
                numberOfVisits.put(vertexName, i + 1);

                LOGGER.debug("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
            }

            LOGGER.debug("In: {}", vertexName); // vrchol, ve kterém se nacházíme

            if (this.verboseMode) {
                LOGGER.debug("Stack:\n{}", stack.getCompleteStackAsString()); // aktuální vzhled stacku, výstup může být velmi rozsáhlý!
            }


            /* -- BUG FIX PRO DEV-1713 -- */
            if (this.isReturn(vertex)) {
                LOGGER.debug("Attention! {} is a return vertex, checking for possible recursive re-visit.", vertexName);

                // Najdi prvni volani (tomu rikame vnejsi) dane funkce.
                final Stack outerCall = stack.findFirstCall();
                if (stack == outerCall) {
                    // Nejde o rekurzivni volani, protoze prvni volani dane funkce jsme my sami, "nad nami" nikdo neni.
                    LOGGER.debug("Not a recursion.");
                } else {
                    // Zde provadime kroky k zariznuti rekurze.
                    LOGGER.debug("Recursion detected.");
                    final BroaderContext broaderContext = new BroaderContext(outerCall, stack.getCallNumber(), vertex);
                    LOGGER.debug("Remembering context {}", broaderContext);
                    final boolean isNew = visitedRecursive.add(broaderContext);
                    if (!isNew) {
                        // Do vrcholu, který reprezentuje parametr volání funkce, vstupujeme v rámci rekurzivního volání již podruhé.
                        // Není třeba pokračovat.
                        LOGGER.debug("BAN! Recursive entry to {} in function [{}]", vertexName, stack.getCalledFunction());
                        continue;
                    }
                }
            }
            /* -- BUG FIX PRO DEV-1713 -- */

            final FilterResult filterResult = this.testVertexWithFilters(vertex, edge);
            if (filterResult.isVertexOk()) {
                LOGGER.debug("{} added to output.", vertexName);
                outputNodes.add(vertex.getId());
            }

            if (!filterResult.shouldContinue()) {
                continue;
            }


            final boolean isCallContextNotPresent = visited.add(item.getCallContext()); // true if this set did not already contain the specified element
            if (!isCallContextNotPresent) {
                LOGGER.debug("{} already visited.", vertexName);
                continue;
            }


            // nasypat nové vrcholy do fronty
            final Iterable<Edge> edges = this.getEdgeType().getNextEdges(vertex, direction, revisionInterval);
            for (final Edge outgoingEdge : edges) {
                final Vertex outgoingVertex = outgoingEdge.getVertex(direction.opposite());
                final String outgoingVertexName = this.verboseMode ? qualifiedNameOf(outgoingVertex) : "v[" + outgoingVertex.getId() + "]";
                LOGGER.debug("To: {}", outgoingVertexName);

                // Kontrola na velikost fronty.
                // Pokud je kapacita fronty překročena, algoritmus končí.
                if (queue.size() >= this.dequeMaxSize) {
                    List<String> startNames = new ArrayList<>();
                    for (Vertex startNode : startNodes) {
                        startNames.add(qualifiedNameOf(startNode));
                    }
                    LOGGER.error("Queue capacity limit ({}) exceeded when running from:\r\n{}",
                        queue.size(), StringUtils.join(startNames, "\r\n"));
                    
                    // Spočítej frekvenci jednotlivých vrcholů, abychom věděli, které jsou nejčastější.
                    final Multiset<Vertex> frequencies = HashMultiset.create();
                    for (final FlowContext element : queue) {
                        frequencies.add(element.getCallContext().getVertex());
                    }

                    // Najdi 15 nejnavštěvovanějších vrcholů a vypiš je.
                    StringBuilder topFreq = new StringBuilder();
                    topFreq.append("15 most repeated vertices in the queue:\r\n");
                    final List<Multiset.Entry<Vertex>> top15 = findObjectsWithTopFrequencies(frequencies, 15);
                    for (final Multiset.Entry<Vertex> entry : top15) {
                        final String topVertexName = qualifiedNameOf(entry.getElement()); // plné jméno vrcholu, 15x je to ok
                        topFreq.append(topVertexName).append("- repeated: ").append(entry.getCount()).append("x\r\n");
                    }
                    LOGGER.error(topFreq.toString());

                    // Vypiš aktuální tvar stacku.
                    Stack.logStack(stackRoot, "", null);

                    throw new GraphFlowAlgorithmException(outputNodes,
                        "Too many vertices found. Exceeded queue capacity (" + this.dequeMaxSize
                            + ") when running from:\r\n" + StringUtils.join(startNames, "\r\n"));
                } else if (this.verboseMode) {
                    LOGGER.debug("Queue size: {}", queue.size());
                }

                final String callNumber = (String) outgoingEdge.getProperty(AttributeNames.EDGE_CALL_ID);
                LOGGER.debug("Over: callNumber={}", callNumber);

                if (callNumber != null) {
                    Vertex typeVertex = outgoingVertex;
                    boolean callVertex = false;
                    boolean returnVertex = false;
                    while (typeVertex != null) {
                        callVertex = this.isCall(typeVertex);
                        returnVertex = this.isReturn(typeVertex);
                        if (callVertex || returnVertex) {
                            break;
                        }
                        typeVertex = GraphOperation.getParent(typeVertex);
                    }

                    LOGGER.debug("IsCallVertex: {}, IsReturnVertex: {}", callVertex, returnVertex);

                    if (typeVertex == null) {
                        LOGGER.error("Vertex {} or its ancestors are neither call, nor return. Type: {}", outgoingVertexName, getNodeType(outgoingVertex));
                        continue;
                    }

                    if (callVertex) {
                        final Vertex functionDef = GraphOperation.getParent(typeVertex);
                        if (functionDef == null) {
                            LOGGER.error("Call vertex {} ({}) does not have a parent node!",
                                    qualifiedNameOf(typeVertex), getNodeType(typeVertex));
                            continue;
                        }
                        if (this.verboseMode) {
                            LOGGER.debug("Entering function: {}", qualifiedNameOf(functionDef));
                        }
                        final Stack newCallStack = stack.push(callNumber, functionDef.getId());
                        //LOGGER.debug("\n{}", newCallStack.getCompleteStackAsString());
                        queue.add(new FlowContext(new CallContext(newCallStack, outgoingVertex), outgoingEdge));
                    } else if (returnVertex) {
                        if (stack.getCallNumber() == null) {
                            LOGGER.debug("Empty stack");
                            queue.add(new FlowContext(new CallContext(stack, outgoingVertex), outgoingEdge));
                        } else if (callNumber.equals(stack.getCallNumber())) {
                            LOGGER.debug("Match");
                            final Collection<Stack> outerStacks = stack.pop();
                            LOGGER.debug("Possible exits: {}", outerStacks.size());
                            for (final Stack outerStack : outerStacks) {
                                LOGGER.debug("Exit {}", outerStack.getCallNumber());
                                queue.add(new FlowContext(new CallContext(outerStack, outgoingVertex), outgoingEdge));
                            }
                        } else {
                            LOGGER.debug("Mismatch");
                            continue;
                        }
                    }
                } else {
                    if (this.isLocal(outgoingVertex)) {
                        LOGGER.debug("Local edge");
                        queue.add(new FlowContext(new CallContext(stack, outgoingVertex), outgoingEdge));
                    } else {
                        LOGGER.debug("Global edge");
                        queue.add(new FlowContext(new CallContext(stackRoot, outgoingVertex), outgoingEdge));
                    }
                }
            }
        }

        if (this.verboseMode) {
            LOGGER.debug("=========================================================================");
            LOGGER.debug("Stack:");
            Stack.logStack(stackRoot, "", null);
            LOGGER.debug("Visit statistics:");
            for (final Entry<String, Integer> row : numberOfVisits.entrySet()) {
                LOGGER.debug(String.format("%-15s: %d", row.getKey(), row.getValue()));
            }
        }

        return outputNodes;
    }

    /**
     * Určí, zda uzel je lokální objekt uvnitř rutiny.
     *
     * Lokální objekty žijí jen v rámci daného volání, globální mají potenciál ovlivnit jiná volání.
     *
     * @param vertex zadaný uzel
     *
     * @return true, jde-li o lokální objekt, false pro globální objekty (tabulka, package proměnná apod.)
     */
    private boolean isLocal(final Vertex vertex) {
        Vertex currentVertex = GraphOperation.getParent(vertex);

        while (currentVertex != null) {
            if (LOCAL_CONTEXT_TYPES.contains(getNodeType(currentVertex))) {
                return true;
            }
            currentVertex = GraphOperation.getParent(currentVertex);
        }

        return false;
    }

    /**
     * Určí, zda jdeme po označené hraně (číslem volání) dovnitř rutiny.
     *
     * Při sledování toků dopředu jdeme dovnitř input / inout parametrem,
     * při sledování toků zpět jdeme dovnitř návratovou hodnotou nebo output / inout parametrem.
     *
     * @param node cílový uzel označené hrany (cíl hrany při směru dopředu, zdroj při směru zpět)
     *
     * @return true, jde-li o zanoření dovnitř rutiny
     */
    private boolean isCall(final Vertex node) {
        final String nodeType = getNodeType(node);
        return NodeType.PARAMETER.getId().equals(nodeType) || NodeType.ROUTINE_RETURN_VALUE.getId().equals(nodeType);
    }

    /**
     * Určí, zda jdeme po označené hraně (číslem volání) ven z rutiny.
     *
     * Při sledování toků dopředu jdeme ven návratovou hodnotou nebo output / inout parametrem,
     * při sledování toků zpět jdeme ven input / inout parametrem.
     *
     * @param node cílový uzel označené hrany (cíl hrany při směru dopředu, zdroj při směru zpět)
     *
     * @return true, jde-li o vynoření ven z rutiny
     */
    private boolean isReturn(final Vertex node) {
        final String nodeType = getNodeType(node);
        return NodeType.CALL_ACTUAL_PARAMETER.getId().equals(nodeType) || NodeType.CALL_RESULT.getId().equals(nodeType);
    }

    /**
     * Získá typ uzlu.
     *
     * @param node uzel
     * @return typ uzlu
     */
    private static String getNodeType(final Vertex node) {
        return node.getProperty(NodeProperty.NODE_TYPE.t());
    }

    @Override
    public void setLimit(final long limit) {
        this.dequeMaxSize = limit;
    }

    /**
     *
     * @param vertex vrchol
     * @return kvalifikované jméno vrcholu (do nadřazené úrovně), obsahuje i ID vrcholu
     */
    private static String qualifiedNameOf(final Vertex vertex) {
        return String.format("%s, v[%s]", GraphOperation.logVertex(vertex), vertex.getId());
    }

    /**
     *
     * @param frequencies frekvence (počet) výskytu objektů
     * @param n počet objektů s nejvyšším počtem výskytů
     * @return prvních n objektů s nejvyšším počtem výskytů
     */
    private static <T> List<Multiset.Entry<T>> findObjectsWithTopFrequencies(final Multiset<T> frequencies, final int n) {
        final LinkedList<Multiset.Entry<T>> top = new LinkedList<>();
        int treshold = -1; // práh pro vstup do seznamu nejpočetnějších elementů
        final Comparator<Multiset.Entry<T>> byCountDescending = new Comparator<Multiset.Entry<T>>() {
            @Override
            public int compare(final Multiset.Entry<T> o1, final Multiset.Entry<T> o2) {
                return Integer.compare(o2.getCount(), o1.getCount());
            }
        };
        for (final Multiset.Entry<T> entry : frequencies.entrySet()) {
            final int count = entry.getCount();
            if(count > treshold) {
                top.add(entry);
                Collections.sort(top, byCountDescending);
                if (top.size() > n) {
                    top.removeLast();
                    treshold = top.getLast().getCount(); // máme plno, zvýšíme práh pro vniknutí do seznamu nejpočetnějších elementů
                }
            }
        }
        return top;
    }



}
