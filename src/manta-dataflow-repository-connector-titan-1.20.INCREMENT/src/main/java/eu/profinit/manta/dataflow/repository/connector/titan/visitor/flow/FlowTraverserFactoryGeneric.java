package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generická továrna pro tvorbu tříd implementující {@link FlowTraverser}.
 * Aby mohla továrna instance vytvářet, musí daná třída mít public default konstruktor.
 * @author tfechtner
 *
 * @param <T> typ traverseru, který se má vytvářet
 */
public class FlowTraverserFactoryGeneric<T extends FlowTraverser> implements FlowTraverserFactory {

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(FlowTraverserFactoryGeneric.class);

    /** Typ třídy k vytváření. */
    private final Class<T> traverserClass;

    /**
     * @param traverserClass Typ třídy k vytváření.
     */
    public FlowTraverserFactoryGeneric(final Class<T> traverserClass) {
        this.traverserClass = traverserClass;
    }

    @Override
    public T createFlowTraverser() {
        try {
            return traverserClass.newInstance();
        } catch (InstantiationException e) {
            LOGGER.error("Class " + traverserClass + " cannot be instantiated.", e);
            return null;
        } catch (IllegalAccessException e) {
            LOGGER.error("The constructor for the class " + traverserClass + " is not accessible.", e);
            return null;
        }
    }

}
