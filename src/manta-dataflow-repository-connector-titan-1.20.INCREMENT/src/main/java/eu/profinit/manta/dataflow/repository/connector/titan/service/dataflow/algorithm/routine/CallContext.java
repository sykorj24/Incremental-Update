package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.routine;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.tinkerpop.blueprints.Vertex;


/**
 * Kontext volání rutiny - uzel a stack volání.
 *
 * Tato kombinace je algoritmem unikátně navštěvována, tj. každý uzel lze navštívit znovu s jiným stackem volání, ale
 * s jedním stackem volání jen jednou.
 *
 * @author rpolach
 */
class CallContext {
    private final Stack stack;
    private final Vertex vertex;

    /**
     * @param stack stack volání
     * @param vertex řešený vrchol
     */
    CallContext(final Stack stack, final Vertex vertex) {
        Validate.notNull(stack);
        Validate.notNull(vertex);

        this.stack = stack;
        this.vertex = vertex;
    }

    /**
     * @return stack volání
     */
    public Stack getStack() {
        return this.stack;
    }

    /**
     * @return řešený vrchol
     */
    public Vertex getVertex() {
        return this.vertex;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.stack)
                .append(this.vertex)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }

        final CallContext other = (CallContext) obj;
        return new EqualsBuilder()
                .append(this.stack, other.stack)
                .append(this.vertex, other.vertex)
                .isEquals();
    }

}