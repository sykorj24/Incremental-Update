package eu.profinit.manta.dataflow.repository.connector.titan.filter.group;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterGroup;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterGroupProvider;

/**
 * Implementace providera pro skupiny filtrů, která konfiguraci načítá ze souboru.
 * @author tfechtner
 *
 */
public class HorizontalFilterGroupProviderImpl implements HorizontalFilterGroupProvider {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(HorizontalFilterGroupProviderImpl.class);
    /** Kódování souboru s konfigurací. */
    private static final String ENCODING = "utf-8";

    /** Množina držených skupin filtrů. */
    private Set<HorizontalFilterGroup> groupSet = new HashSet<>();
    /** Soubor s konfiguracemi. */
    private File configurationFile;

    @Override
    public HorizontalFilterGroup findGroup(String groupId) {
        for (HorizontalFilterGroup group : groupSet) {
            if (group.getId().equals(groupId)) {
                return group;
            }
        }
        return null;
    }

    @Override
    public Set<HorizontalFilterGroup> getGroups() {
        return Collections.unmodifiableSet(groupSet);
    }

    @Override
    public int refreshGroups() {
        if (configurationFile == null || !configurationFile.exists()) {
            LOGGER.warn("Configuration file for horziontal filter groups doesn't exist.");
            return 0;
        }

        String content;
        try {
            content = FileUtils.readFileToString(configurationFile, ENCODING);
        } catch (IOException e) {
            LOGGER.error("Error during reading configuration file with groups.", e);
            return 0;
        }

        ObjectMapper mapper = new ObjectMapper();
        JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, HorizontalFilterGroupImpl.class);
        try {
            List<HorizontalFilterGroup> parsedGroups = mapper.readValue(content, type);
            groupSet = new HashSet<>(parsedGroups);
        } catch (JsonParseException e) {
            LOGGER.error("Error during parsing groups configuration.", e);
        } catch (JsonMappingException e) {
            LOGGER.error("Error during mapping groups configuration.", e);
        } catch (IOException e) {
            LOGGER.error("IO exception during processing groups configuration.", e);
        }

        int newGroupSize = groupSet.size();
        LOGGER.info("Loaded {} groups.", newGroupSize);
        return newGroupSize;
    }

    /**
     * @return Soubor s konfiguracemi. 
     */
    public File getConfigurationFile() {
        return configurationFile;
    }

    /**
     * @param configurationFile Soubor s konfiguracemi. 
     */
    public void setConfigurationFile(File configurationFile) {
        this.configurationFile = configurationFile;
    }

}
