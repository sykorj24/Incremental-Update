package eu.profinit.manta.dataflow.repository.connector.titan.service.sourcecode;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.platform.cache.CustomEhcacheFactory;
import eu.profinit.manta.platform.cache.CustomEhcacheFactory.CacheCreationException;
import eu.profinit.manta.platform.cache.EhcacheWrapper;

/**
 * Implementace {link SourceFileLoader} s podporou cache.
 * Cache je realizovana na urovni celych zdrojovych souboru
 * (nikoliv pouze casti zdrojoveho kodu vazanych na jednotlive uzly) 
 * 
 * @author onouza
 */
public class SourceFileLoaderCached implements SourceFileLoader {

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(SourceFileLoaderCached.class);
    /** Nazev cache */
    private static final String CACHE_NAME = "SOURCE_CODE_CACHE";

    /** Ehcache souboru zdrojovych kodu, klicem je umisteni souboru (viz {@link SourceFile}). */
    private EhcacheWrapper<SourceFile, List<String>> ehcache;
    /** Tovarna na tvorbu cache. */
    private CustomEhcacheFactory ehcacheFactory;
    /** Maximalni pocet souboru v cachi */
    private int maxFilesInCache;
    /** Zivotnost jednotlivych objektu v cachi */
    private int timeToLiveSeconds;

    /**
     * Komponenta pro nacitani souboru zdrojoveho kodu
     * pouzita v pripade, kdy hledany soubor neni v cachi nebo cache neni vytvorena.
     */
    private SourceFileLoader sourceFileLoader;

    /**
     * Inicializuje komponentu.
     * Pokusi se vytvorit ehcache nactenych souboru na zaklade atributu komponenty.
     * Pokud se to nepovede, se soubory se nebudou cachovat, cili budou se pokazde nacitat.
     */
    public void init() {
        EhcacheWrapper<SourceFile, List<String>> tempCacheVariable = null;
        try {
            ehcacheFactory.insertIfNotContains(CACHE_NAME, maxFilesInCache, false, timeToLiveSeconds);
            tempCacheVariable = ehcacheFactory.createCacheWrapper(CACHE_NAME, SourceFile.class, (Class<List<String>>)null);
        } catch (CacheCreationException e) {
            LOGGER.error("Cannot create cache for source codes.", e);
        }
        ehcache = tempCacheVariable;
    }
    
    @Override
    public List<String> loadSourceFile(SourceFile sourceFile) {
        if (ehcache == null) {
            // cache neni vytvorena => nacteme soubor
            return sourceFileLoader.loadSourceFile(sourceFile);
        }
        List<String> sourceCode = ehcache.get(sourceFile);
        if (sourceCode == null) {
            // soubor neni v cache => nacteme jej
            sourceCode = sourceFileLoader.loadSourceFile(sourceFile);
            ehcache.put(sourceFile, sourceCode);
        }
        return sourceCode;
    }

    // gettery / settery
    
    /**
     * @param ehcacheFactory Tovarna na tvorbu cache.
     */
    public void setEhcacheFactory(CustomEhcacheFactory ehcacheFactory) {
        this.ehcacheFactory = ehcacheFactory;
    }

    /**
     * @param maxFilesInCache Maximalni pocet souboru v cachi 
     */
    public void setMaxFilesInCache(int maxFilesInCache) {
        this.maxFilesInCache = maxFilesInCache;
    }

    /**
     * @param timeToLiveSeconds Zivotnost jednotlivych objektu v cachi 
     */
    public void setTimeToLiveSeconds(int timeToLiveSeconds) {
        this.timeToLiveSeconds = timeToLiveSeconds;
    }

    /**
     * @param sourceFileLoader Komponenta pro nacitani souboru zdrojoveho kodu
     *              pouzita v pripade, kdy hledany soubor neni v cachi nebo cache neni vytvorena.
     */
    public void setSourceFileLoader(SourceFileLoader sourceFileLoader) {
        this.sourceFileLoader = sourceFileLoader;
    }
    
}
