package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import org.apache.commons.lang3.Validate;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.NodeType;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;

/**
 * Pomocná informační třída pro Vertex pro účely snadného zpracování.
 * Obsahuje základní "lidsky snadno pochopitelné" informace o vrcholu (hlavně jméno a typ), 
 * aniž by bylo potřeba dokola převolávat různé getType/getProperty metody. 
 * Samotný vrchol zapamatován není, předpokládá se následující použití:
 * <pre>
 * Vertex vertex = ...;
 * final AboutVertex info = new AboutVertex(vertex);
 * if(info.name == "MS SQL DB") { ... }
 * </pre>
 * Jméno a typ vrcholu se konstruují různě pro různé {@link VertexType} a {@link NodeType}.
 * <table>
 * <tr><th>VertexType</th><th>name</th><th>type</th></tr>
 * <tr><td>SUPER_ROOT</td><td>SUPER_ROOT</td><td>SUPER_ROOT</td></tr>
 * <tr><td>REVISION_ROOT</td><td>REVISION_ROOT</td><td>REVISION_ROOT</td></tr>
 * <tr><td>REVISION_NODE</td><td>REVISION_NODE</td><td>REVISION_NODE</td></tr>
 * <tr><td>RESOURCE</td><td>resourceName</td><td>RESOURCE</td></tr>
 * <tr><td>ATTRIBUTE</td><td>attributeName=attributeValue</td><td>ATTRIBUTE</td></tr>
 * <tr><td>NODE</td><td>nodeName</td><td>nodeType</td></tr>
 * </table>
 * <br/>
 * Property třídy jsou vystaveny bez zapouzdření k přímému použití (jde o final public properties u datových
 * typů, které jsou immutable, a zapouzdřující třída je final).
 * 
 * @author Erik Kratochvíl
 */
public final class AboutVertex {

    /** Id vrcholu. */
    public final long id;

    /** Typ vrcholu (vertex). */
    public final VertexType vertexType;

    /** Typ uzlu (pokud je vertex = Node), tj. může být null. */
    public final NodeType nodeType;

    /** Lidsky čitelný a pochopitelný typ vrcholu. */
    public final String type;

    /** Lidsky čitelné a pochopitelné jméno vrcholu. */
    public final String name;

    /**
     * Vytvoří nový wrapper objekt se naplněnými fieldy. 
     * 
     * @param vertex wrappovaný vertex
     */
    public AboutVertex(final Vertex vertex) {
        Validate.notNull(vertex, "Vertex cannot be null. Hint: supply a valid vertex.");
        Validate.isTrue(vertex.getId() instanceof Long,
                "Vertex id must be a long. Hint: check the Titan database schema.");
        this.id = (Long) vertex.getId();
        this.vertexType = VertexType.getType(vertex);
        final String subtype = vertex.getProperty(NodeProperty.NODE_TYPE.t());
        this.nodeType = (subtype != null ? NodeType.getType(subtype) : null);
        this.name = nameOf(vertex, vertexType);
        this.type = (nodeType == null ? vertexType.toString() : nodeType.toString());
    }

    /**
     * Vygenerování lidsky snadno pochopitelného jména vrcholu.
     * 
     * @param vertex vrchol
     * @param vertexType typ vrcholu
     * @return speciálně zkonstruované lidsky čitelné jméno vrcholu na základě typu vrcholu/uzlu
     */
    private static String nameOf(final Vertex vertex, final VertexType vertexType) {
        switch (vertexType) {
        case SUPER_ROOT:
            return "SUPER_ROOT";
        case REVISION_ROOT:
            return "REVISION_ROOT";
        case REVISION_NODE:
            return "REVISION_NODE";
        case RESOURCE:
            return vertex.getProperty(NodeProperty.RESOURCE_NAME.t());
        case ATTRIBUTE:
            return vertex.getProperty(NodeProperty.ATTRIBUTE_NAME.t()) + "="
                    + vertex.getProperty(NodeProperty.ATTRIBUTE_VALUE.t());
        case NODE:
            return vertex.getProperty(NodeProperty.NODE_NAME.t());
        default:
            return null;
        }
    }

    @Override
    public String toString() {
        return type + " '" + name + "' (" + id + ")";
    }
}
