package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.restriction;

import java.io.IOException;

import com.thinkaurelius.titan.core.AttributeSerializer;
import com.thinkaurelius.titan.diskstorage.ScanBuffer;
import com.thinkaurelius.titan.diskstorage.WriteBuffer;

import eu.profinit.manta.dataflow.model.restriction.nodetypes.RestrDisjunctionTypeDnf;
import eu.profinit.manta.dataflow.repository.utils.Base64AttributeCodingHelper;

/**
 * Zkopírováno podle třídy BasePredicateNodeSerializer od Pavla Jaromerského.
 */
public class RestrPropositionalNodeSerializer implements AttributeSerializer<RestrDisjunctionTypeDnf> {

    @Override
    public void verifyAttribute(RestrDisjunctionTypeDnf value) {
        // NOOP
    }

    @Override
    public RestrDisjunctionTypeDnf convert(Object value) {
        return null;
    }

    @Override
    public RestrDisjunctionTypeDnf read(ScanBuffer buffer) {
        int length = buffer.getInt();
        byte[] objBytes = readBytes(buffer, length);
        // Provest standardni deserializaci objektu pomoci prostredku java
        try {
            return (RestrDisjunctionTypeDnf) Base64AttributeCodingHelper.deserializeObject(objBytes);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Error during serialization of RestrDisjunctionTypeDnf.", e);
        } catch (IOException e) {
            throw new RuntimeException("Error during serialization of RestrDisjunctionTypeDnf.", e);
        }
    }

    /**
     * Nacteni pole bytu ze vstupniho streamu (musi se cist po jednotlivych byte).
     * @param buffer
     * @param length
     * @return
     */
    private static byte[] readBytes(ScanBuffer buffer, int length) {
        byte[] result = new byte[length];
        for (int i = 0; i < length; i++) {
            result[i] = buffer.getByte();
        }
        return result;
    }

    @Override
    public void writeObjectData(WriteBuffer buffer, RestrDisjunctionTypeDnf attribute) {
        // Provest standardni serializaci objektu pomoci prostredku java. 
        try {
            byte[] objBytes = Base64AttributeCodingHelper.serializeObject(attribute);
            buffer.putInt(objBytes.length);
            writeBytes(buffer, objBytes);
        } catch (IOException e) {
            throw new RuntimeException("Error during serialization of RestrDisjunctionTypeDnf.", e);
        }
    }
    
    /**
     * Zapis pole bytu do vystupniho streamu. 
     * @param buffer
     * @param objBytes
     */
    private static void writeBytes(WriteBuffer buffer, byte[] objBytes) {
        for (Byte bt : objBytes) {
            buffer.putByte(bt);
        }
    }
    
}
