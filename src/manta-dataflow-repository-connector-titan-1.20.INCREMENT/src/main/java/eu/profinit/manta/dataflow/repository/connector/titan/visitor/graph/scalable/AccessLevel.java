package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable;

import eu.profinit.manta.dataflow.repository.connector.titan.connection.RollbackAware;
import eu.profinit.manta.dataflow.repository.connector.titan.connection.RollbackAwareCallback;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

/**
 * Úroveň přístupu pro práci s databází.
 * @author tfechtner
 *
 */
public enum AccessLevel {
    /** Pouze čtecí přístup. */
    READ {
        @Override
        public <T> T runInTransaction(DatabaseHolder databaseHolder, TransactionCallback<T> callback) {
            return databaseHolder.runInTransaction(TransactionLevel.READ, callback);
        }

        @Override
        public <T> T runRollback(DatabaseHolder databaseHolder, RollbackAware<T> rollbackAware) {
            return databaseHolder.runInTransaction(TransactionLevel.READ, new RollbackAwareCallback<T>(rollbackAware));
        }
    },
    /** Čtecí i zapisovací přístup. */
    WRITE {
        @Override
        public <T> T runInTransaction(DatabaseHolder databaseHolder, TransactionCallback<T> callback) {
            return databaseHolder.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, callback);
        }

        @Override
        public <T> T runRollback(DatabaseHolder databaseHolder, RollbackAware<T> rollbackAware) {
            return databaseHolder.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new RollbackAwareCallback<T>(rollbackAware));
        }
    },
    /** Čtecí i zapisovací přístup pro merge. */
    WRITE_SHARE {
        @Override
        public <T> T runInTransaction(DatabaseHolder databaseHolder, TransactionCallback<T> callback) {
            return databaseHolder.runInTransaction(TransactionLevel.WRITE_SHARE, callback);
        }

        @Override
        public <T> T runRollback(DatabaseHolder databaseHolder, RollbackAware<T> rollbackAware) {
            return databaseHolder.runInTransaction(TransactionLevel.WRITE_SHARE,
                    new RollbackAwareCallback<T>(rollbackAware));
        }
    },
    /** Pouze čtecí a navíc umožňuje zároveň jiné transakci zapisovat. */
    READ_NON_EXCLUSIVE {
        @Override
        public <T> T runInTransaction(DatabaseHolder databaseHolder, TransactionCallback<T> callback) {
            return databaseHolder.runInTransaction(TransactionLevel.READ_NOT_BLOCKING, callback);
        }

        @Override
        public <T> T runRollback(DatabaseHolder databaseHolder, RollbackAware<T> rollbackAware) {
            return databaseHolder.runInTransaction(TransactionLevel.READ_NOT_BLOCKING,
                    new RollbackAwareCallback<T>(rollbackAware));
        }
    };

    /**
     * Spustí callback v příslušné transakci.
     * @param databaseHolder služba pro databázi pro tvorbu transakcí
     * @param callback callback k vykonání
     * @return výstup callbacku
     * 
     * @param <T> návratový typ callbacku
     */
    public abstract <T> T runInTransaction(DatabaseHolder databaseHolder, TransactionCallback<T> callback);

    /**
     * Spustí rollback v příslušné transakci.
     * @param databaseHolder služba pro databázi pro tvorbu transakcí
     * @param rollbackAware instance, která se má rollbackovat
     * @return výstup rollbacku
     * 
     * @param <T> návratový typ callbacku
     */
    public abstract <T> T runRollback(DatabaseHolder databaseHolder, RollbackAware<T> rollbackAware);
}