package eu.profinit.manta.dataflow.repository.connector.titan.service;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Třída, která má na starosti zastavení executor služby.
 * Čeká na závoře a pak jednoduše zavolá shutdown.
 * @author tfechtner
 *
 */
public class ExecutorServiceTerminator implements Runnable {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ExecutorServiceTerminator.class);

    /** Služba, která se má zastavit. */
    private final ExecutorService executorService;
    /** Závora, která určuje, jestli už všechny pracovní vlákna skončily. */
    private final CountDownLatch shutdownLatch;

    /**
     * @param executorService Služba, která se má zastavit
     * @param shutdownLatch Závora, která určuje, jestli už všechny pracovní vlákna skončily
     */
    public ExecutorServiceTerminator(ExecutorService executorService, CountDownLatch shutdownLatch) {
        super();
        this.executorService = executorService;
        this.shutdownLatch = shutdownLatch;
    }

    @Override
    public void run() {
        try {
            shutdownLatch.await();
        } catch (InterruptedException e) {
            LOGGER.info("Executor terminator has been interupted.");
        } finally {
            executorService.shutdown();
        }
    }
}