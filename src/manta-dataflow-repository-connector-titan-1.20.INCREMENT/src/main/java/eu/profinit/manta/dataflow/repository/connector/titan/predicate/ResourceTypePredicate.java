package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import java.util.Set;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;

/**
 * Evaluátor kontrolující typ resource oproti zadané množině přípustných hodnot.
 * @author tfechtner
 *
 */
public class ResourceTypePredicate extends VertexPropertyPredicate {
    /**
     * Default konsturktor, nutno nastavit field {@link #propertyName} pro 
     * správné fungování třídy. 
     */
    public ResourceTypePredicate() {
        super();
        setPropertyName(NodeProperty.RESOURCE_TYPE.t());
    }

    /**
     * @param valueSet Název property vrcholu k ověření.
     */
    public ResourceTypePredicate(Set<String> valueSet) {
        super(valueSet, NodeProperty.RESOURCE_TYPE.t());
    }
}
