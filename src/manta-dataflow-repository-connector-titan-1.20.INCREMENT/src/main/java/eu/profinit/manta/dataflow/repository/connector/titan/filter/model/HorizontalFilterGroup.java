package eu.profinit.manta.dataflow.repository.connector.titan.filter.model;

import java.util.Set;

/**
 * Skupina horizontálních filtrů.
 * Například filtr pro vyfiltrování všech nedatabázových resource.
 * @author tfechtner
 *
 */
public interface HorizontalFilterGroup {

    /**
     * Vrací id skupiny filtrů.
     * @return id skupiny filtrů
     */
    String getId();

    /**
     * Vrací název skupiny filtrů.
     * @return název skupiny filtrů
     */
    String getName();

    /**
     * Vrací pořadí skupiny filtrů. 
     * 0 je první skupina, N je poslední skupina.
     * @return pořadí skupiny filtrů
     */
    int getOrder();

    /**
     * Vrací množinu id obsažených filtrů.
     * @return id reprezentovaných filtrů, nikdy null
     */
    Set<String> getFilterIds();
}
