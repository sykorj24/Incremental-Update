package eu.profinit.manta.dataflow.repository.connector.titan.service;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;

/**
 * Třída pro správu source root uzlu databáze.
 * @author tfechtner
 *
 */
public class SourceRootHandler extends AbstractRootHandler {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SourceRootHandler.class);

    /** Adresář obsahující soubory reprezentující zdrojový kód. */
    private File sourceCodeDir = new File("sourceCode");

    @Override
    public NodeProperty getNodeProperty() {
        return NodeProperty.SOURCE_ROOT;
    }

    @Override
    public VertexType getVertexType() {
        return VertexType.SOURCE_ROOT;
    }

    /**
     * @return Adresář obsahující soubory reprezentující zdrojový kód.
     */
    public File getSourceCodeDir() {
        return sourceCodeDir;
    }

    /**
     * @param sourceCodeDir Adresář obsahující soubory reprezentující zdrojový kód.
     */
    public void setSourceCodeDir(File sourceCodeDir) {
        this.sourceCodeDir = sourceCodeDir;
    }

    /**
     * Získá id pro source code soubor reprezentovaný daným vrcholem.
     * @param vertex vrchol, který reprezentuje dotazovaný source code soubor
     * @return id source code souboru
     * @throws IllegalStateException vrchol nemá příslušnou property 
     */
    public String getSourceNodeId(Vertex vertex) {
        Object property = vertex.getProperty(NodeProperty.SOURCE_NODE_ID.t());
        if (property != null) {
            return property.toString();
        } else {
            throw new IllegalStateException("The vertex " + GraphOperation.getName(vertex) + " does not have "
                    + NodeProperty.SOURCE_NODE_ID.t() + " property.");
        }
    }

    /**
     * Odstraní vrchol reprezentující source code soubor včetně příslušného souboru.
     * @param vertex vrchol k odstranění
     */
    public void removeSourceCodeNode(Vertex vertex) {
        Validate.isTrue(VertexType.getType(vertex) == VertexType.SOURCE_NODE, "The vertex is not source code node.");
        String id = getSourceNodeId(vertex);
        File file = new File(sourceCodeDir, id);
        if (file.exists() && !file.delete()) {
            LOGGER.warn("Cannot delete source code file " + file.getName() + ".");
        }
        vertex.remove();
    }

    /**
     * Smaže všechny source code soubory.
     */
    public void truncateSourceCodeFiles() {
        try {      
            if (getSourceCodeDir().exists()) {
                FileUtils.cleanDirectory(getSourceCodeDir());
            }
        } catch (IOException e) {
            LOGGER.error("Cannot clean source code directory", e);
        }
        
    }
}
