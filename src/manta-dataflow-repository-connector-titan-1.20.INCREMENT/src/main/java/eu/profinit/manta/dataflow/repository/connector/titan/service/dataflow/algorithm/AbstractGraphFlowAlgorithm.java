package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterResult;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.GraphFlowFilter;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.EdgeTypeTraversing;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Společný předek pro obecné graph flow algoritmy.
 * @author tfechtner
 *
 */
public abstract class AbstractGraphFlowAlgorithm implements GraphFlowAlgorithm {

    /** Typ procházených hran.  */
    private EdgeTypeTraversing edgeType = EdgeTypeTraversing.BOTH;

    /** Seznam filtrů pro omezení výsledků. */
    private List<GraphFlowFilter> filters = Collections.emptyList();

    /**
     * @return Typ procházených hran.
     */
    public EdgeTypeTraversing getEdgeType() {
        return edgeType;
    }

    /**
     * @param edgeType Typ procházených hran.
     */
    protected void setEdgeType(EdgeTypeTraversing edgeType) {
        this.edgeType = edgeType;
    }

    @Override
    public boolean getFilterEdgesEnabled() {
        return edgeType == EdgeTypeTraversing.BOTH || edgeType == EdgeTypeTraversing.FILTER;
    }

    @Override
    public void setFilterEdgesEnabled(boolean filterEdgesEnabled) {
        if (filterEdgesEnabled) {
            setEdgeType(EdgeTypeTraversing.BOTH);
        } else {
            setEdgeType(EdgeTypeTraversing.DIRECT);
        }
    }

    /**
     * @return Seznam filtrů pro omezení výsledků. 
     */
    public List<GraphFlowFilter> getFilters() {
        return filters;
    }

    @Override
    public void setFilters(List<GraphFlowFilter> filters) {
        if (filters != null) {
            this.filters = filters;
        }
    }

    @Override
    public final Set<Object> findNodesByAlgorithm(Collection<Vertex> startNodes, Set<Object> allowedNodes,
            TitanTransaction dbTransaction, Direction direction, RevisionInterval revisionInterval)
            throws GraphFlowAlgorithmException {
        if (direction == Direction.BOTH) {
            throw new IllegalArgumentException("The direction must not be BOTH.");
        }
        return findNodesByAlgorithmInternal(startNodes, allowedNodes, dbTransaction, direction, revisionInterval);
    }

    /**
     * Nalezne vrcholy v grafu odpovídající algoritmu.
     * @param startNodes Startovní uzly. 
     * @param allowedNodes Povolené id uzlů, pouze na kterých se algoritmus může pohybovat.
     * @param dbTransaction Titan transakce.
     * @param direction Směr procházení musí být {@link Direction.IN} nebo {@link Direction.OUT}.
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     * @return množina id nalezených uzlů algoritmem
     * @throws GraphFlowAlgorithmException chyba v algoritmu
     */
    protected abstract Set<Object> findNodesByAlgorithmInternal(Collection<Vertex> startNodes,
            Set<Object> allowedNodes, TitanTransaction dbTransaction, Direction direction,
            RevisionInterval revisionInterval) throws GraphFlowAlgorithmException;

    /**
     * Otestuje daný vertex s příslušnou příchozí hranou přes všehcny filtry algoritmu.
     * @param vertex testovaný vertex
     * @param incomingEdge příslušná hrana, odkud se do tohoto vertexu přišlo
     * @return společný AND výsledek přes všechny filtry
     */
    protected final FilterResult testVertexWithFilters(Vertex vertex, Edge incomingEdge) {
        FilterResult result = FilterResult.OK_CONTINUE;
        for (GraphFlowFilter filter : filters) {
            result = result.sumWith(filter.testVertex(vertex, incomingEdge));
        }
        return result;
    }

    /**
     * Ztransformuje kolekci vrcholů na množinu jejich id.
     * @param vertices kolekce vrcholu k transformaci
     * @return množina id vrcholů, nikdy null
     */
    protected final Set<Object> transformVerticesToIds(Collection<Vertex> vertices) {
        Set<Object> idSet = new HashSet<Object>();
        for (Vertex v : vertices) {
            if (v != null) {
                idSet.add(v.getId());
            }
        }
        return idSet;
    }
}
