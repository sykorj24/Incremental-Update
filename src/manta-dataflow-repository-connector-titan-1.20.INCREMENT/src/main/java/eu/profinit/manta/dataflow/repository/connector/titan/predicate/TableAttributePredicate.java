package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import java.util.List;
import java.util.Set;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;

/**
 * Evaluátor kontrolující podmínku, že uzel je sloupec v tabulce 
 * a navíc má tato tabulka atribut s daným klíčem nacházející se v dané množině přípustných hodnot.
 * V případě, že má tabulka více typů, tak stačí aby alespoň jeden se rovnal některé chtěné hodnotě.
 * @author tfechtner
 *
 */
public class TableAttributePredicate implements VertexPredicate {
    /** Hodnota property typu uzlu značící tabulku. */
    private static final String NODE_TYPE_PROPERTY_TABLE = "Table";

    /** Množina povolených typů. */
    private Set<String> typeSet;
    /** Klíč ověřovaného atributu. */
    private String attributeKey;

    /**
     * Default konstruktor, pro správné fungování je ještě třeba nastvit field {@link #typeSet}.
     */
    public TableAttributePredicate() {
        super();
    }

    /**
     * @param typeSet množina typů splňující podmínku
     * @param attributeKey klíč ověřovaného atributu
     */
    public TableAttributePredicate(Set<String> typeSet, String attributeKey) {
        super();
        this.typeSet = typeSet;
        this.attributeKey = attributeKey;
    }

    @Override
    public boolean evaluate(Vertex vertex, RevisionInterval revisionInterval) {
        if (typeSet == null) {
            throw new IllegalStateException("The type set must not be null.");
        }

        Vertex tableVertex = GraphOperation.getParent(vertex);
        if (tableVertex == null) {
            return false;
        }
        String tableVertexType = tableVertex.getProperty(NodeProperty.NODE_TYPE.t());
        if (!NODE_TYPE_PROPERTY_TABLE.equals(tableVertexType)) {
            return false;
        }

        List<Object> tableTypes = GraphOperation.getNodeAttribute(tableVertex, attributeKey, revisionInterval);
        for (Object type : tableTypes) {
            if (typeSet.contains(type)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param typeSet Množina povolených typů.
     */
    public void setTypeSet(Set<String> typeSet) {
        this.typeSet = typeSet;
    }

    /**
     * @param attributeKey Klíč ověřovaného atributu
     */
    public void setAttributeKey(String attributeKey) {
        this.attributeKey = attributeKey;
    }
}

