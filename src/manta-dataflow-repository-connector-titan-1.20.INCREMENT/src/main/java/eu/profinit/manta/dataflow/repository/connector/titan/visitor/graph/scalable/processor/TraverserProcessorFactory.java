package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor;

import java.util.Deque;

import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Továrna na vytváření procesorů pro zpracování elementů v rámci scalable traverseru.  
 * @author tfechtner
 *
 */
public interface TraverserProcessorFactory {
    
    /**
     * Vytvoří příslušnou instanci.
     * @param visitor visitor navštěvující jednotlivé elementy v grafu.
     * @param revisionInterval interval revizí, které procesor prochází
     * @param idsToProcess sdílená fronta id vrcholů ke zpracování
     * @return nově vytvořená instance
     */
    TraverserProcessor<?> createTraverser(GraphVisitor visitor, RevisionInterval revisionInterval,
            Deque<Object> idsToProcess);
}
