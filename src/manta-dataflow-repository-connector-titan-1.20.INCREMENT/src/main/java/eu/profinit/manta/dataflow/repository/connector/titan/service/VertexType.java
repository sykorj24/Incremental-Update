package eu.profinit.manta.dataflow.repository.connector.titan.service;

import java.util.Arrays;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;

/**
 * Typ vrcholu v Manta Repostiory DB.
 * @author tfechtner
 *
 */
public enum VertexType {
    /** Super root databáze, v korektní db musí být právě jeden.*/
    SUPER_ROOT(null, null),
    /** Revision root databáze, v korektní db musí být právě jeden.*/
    REVISION_ROOT(null, null),
    /** Resource, vrchol odpovídající resource/technologii. */
    RESOURCE(Direction.OUT, new String[] { EdgeLabel.HAS_RESOURCE.t() }),
    /** Standardní uzel odpovídající každému prvku grafu datových toků. */
    NODE(Direction.OUT, new String[] { EdgeLabel.HAS_RESOURCE.t(), EdgeLabel.HAS_PARENT.t() }),
    /** Atribut {@link #NODE}. */
    ATTRIBUTE(Direction.IN, new String[] { EdgeLabel.HAS_ATTRIBUTE.t() }),
    /** Uzel reprezentujici revizi grafu. */
    REVISION_NODE(Direction.IN, new String[] { EdgeLabel.HAS_REVISION.t() }),
    /** Source root databáze, v korektní db musí být právě jeden.*/
    SOURCE_ROOT(null, null),
    /** Uzel reprezentujici source code file. */
    SOURCE_NODE(Direction.IN, new String[] { EdgeLabel.HAS_SOURCE.t() }),
    /** Uzel reprezentujici vrstvu datoveho toku. */
    LAYER(Direction.IN, new String[] {EdgeLabel.IN_LAYER.t()});

    /** Směr kontrolních hran pro tento typ .*/
    private final Direction controlEdgeDirection;
    /** Labely kontrolních hran pro tento typ. */
    private final String[] controlEdgeLabels;

    /**
     * @param controlEdgeDirection směr kontrolních hran pro tento typ
     * @param controlEdgeLabels labely kontrolních hran pro tento typ
     */
    VertexType(Direction controlEdgeDirection, String[] controlEdgeLabels) {
        this.controlEdgeDirection = controlEdgeDirection;
        this.controlEdgeLabels = controlEdgeLabels;
    }

    /**
     * @return směr kontrolních hran pro tento typ
     */
    public Direction getControlEdgeDirection() {
        return controlEdgeDirection;
    }

    /**
     * @return labely kontrolních hran pro tento typ
     */
    public String[] getControlEdgeLabels() {
        return Arrays.copyOf(controlEdgeLabels, controlEdgeLabels.length);
    }

    /**
     * Zjistí typ daného vrcholu.
     * @param vertex vrchol, jehož typ se zjišťuje
     * @return typ vrcholu, nikdy null
     * @throws IllegalArgumentException vrcholu je null
     * @throws IllegalStateException neznámý typ vrcholu
     */
    public static VertexType getType(Vertex vertex) {
        if (vertex == null) {
            throw new IllegalArgumentException("The argument is null.");
        }

        VertexType type = vertex.getProperty(NodeProperty.VERTEX_TYPE.t());
        if (type != null) {
            return type;
        } else {
            throw new IllegalStateException(
                    "Unknown type of vertex. Id: " + vertex.getId() + "; attrs: " + vertex.getPropertyKeys() + ".");
        }
    }
}
