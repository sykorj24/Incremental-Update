package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.restriction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.AttributeNames;
import eu.profinit.manta.dataflow.model.restriction.RestrBinaryRelation;
import eu.profinit.manta.dataflow.model.restriction.RestrResult;
import eu.profinit.manta.dataflow.model.restriction.RestrSolver;
import eu.profinit.manta.dataflow.model.restriction.nodetypes.RestrDisjunctionTypeDnf;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.AbstractGraphFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterResult;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.EdgeIdentification;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Procházení grafem s restrikcemi - Otevřený model: 
 * Známe toky v databázi, ale nevíme, jestli to jsou všechny toky. Nevíme, jaká data už byla v databázi zapsaná
 * před spuštěním našich skriptů a jestli tam někdo další data nedopisuje ručně.
 * @author mdvorak2
 * @author tfechtner
 */
public class RestrGraphFlowAlgorithm extends AbstractGraphFlowAlgorithm {

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(RestrGraphFlowAlgorithm.class);

    /** Seznam typů (pro věci jako třeba tabulky), ve kterých může končit datový tok - neobsahuje transformace. */
    private Set<String> databaseResources;

    /** Maximální čas pro výpočet v ms.*/
    private long maxExecutionTime = Integer.MAX_VALUE;

    @Override
    protected Set<Object> findNodesByAlgorithmInternal(Collection<Vertex> startNodes, Set<Object> allowedNodes,
            TitanTransaction transaction, Direction direction, RevisionInterval revisionInterval)
                    throws GraphFlowAlgorithmException {

        // naplnit frontu startovními vrcholy
        Deque<QueueItem> queue = new LinkedList<QueueItem>();
        for (Vertex startNode : startNodes) {
            queue.add(new QueueItem(startNode, null, RestrSolver.RestrTrue, null, new TreeStack()));
        }

        // zpracovat frontu
        Set<Object> result = processQueue(queue, allowedNodes, direction, revisionInterval, transaction);
        return result;
    }

    /**
     * Metoda postupně zpracuje frontu vrcholů a všech z nich dosažitelných sousedů odpovídající algoritmu.
     * Metoda kontroluje, jestli na zpracovávaném vrcholu nedojde ke kontradikci.
     * @param queue fronta ke zpracování
     * @param allowedNodes množina ID povolených vrcholů
     * @param direction směr prohledávání
     * @param revisionInterval interval revizí, které se berou v potaz
     * @param transaction transakce pro přístup k db
     * @return množina ID vrcholů odpovídajících algoritmu
     * @throws GraphFlowAlgorithmException algoritmus trval příliš dlouho
     */
    private Set<Object> processQueue(Deque<QueueItem> queue, Set<Object> allowedNodes, Direction direction,
            RevisionInterval revisionInterval, TitanTransaction transaction) throws GraphFlowAlgorithmException {

        // ID nalezených uzlů, přes které jsme se dostali až do nějakého resourcu, který chceme zobrazit
        Set<Object> returnVertexIds = new HashSet<Object>();

        // ID uzlů a k nim DNF formule, se kterými byli už navštíveny  
        Map<Long, List<RestrDisjunctionTypeDnf>> verticesRestrs = new HashMap<Long, List<RestrDisjunctionTypeDnf>>();

        long startTime = System.currentTimeMillis();

        // postupně zpracovat frontu
        while (!queue.isEmpty()) {
            if (System.currentTimeMillis() - startTime > maxExecutionTime) {
                LOGGER.warn("Algorithm for predicates takes too long - {} ms. Current queue size is {}.",
                        System.currentTimeMillis() - startTime, queue.size());
                if (allowedNodes != null) {
                    return allowedNodes;
                } else {
                    throw new GraphFlowAlgorithmException(returnVertexIds, "Algorithm for predicates takes too long.");
                }
            }

            QueueItem currentItem = queue.pollFirst();
            Vertex currentVertex = transaction.getVertex(currentItem.getVertexId());

            // pokud není vrchol povolen -> přeskočit
            if (allowedNodes != null && (!allowedNodes.contains(currentItem.getVertexId()))) {
                continue;
            }

            // ověřit, jestli projde vrchol přes zásuvné filtry, jinak přeskočit
            Edge incomingEdge;
            EdgeIdentification incomingEdgeId = currentItem.getIncomingEdgeId();
            if (incomingEdgeId != null) {
                incomingEdge = GraphOperation.getEdge(transaction, incomingEdgeId, revisionInterval);
            } else {
                incomingEdge = null;
            }

            FilterResult filterResult = testVertexWithFilters(currentVertex, incomingEdge);
            if (filterResult == FilterResult.NOK_STOP) {
                continue;
            }

            // spojit restrikci s potencionálním omezením na tomto stromě
            RestrDisjunctionTypeDnf restrictionOfTree = getRestrictionInHirearachy(currentVertex, revisionInterval);
            RestrDisjunctionTypeDnf intersection;
            if (restrictionOfTree == null) {
                intersection = currentItem.getRestriction();
            } else {
                // přibývá nová podmínka
                intersection = RestrSolver.conjunctionDNF(restrictionOfTree, currentItem.getRestriction());
            }

            // jestliže je kombinace nastřádaných podmínek nesplnitelná -> přeskočit
            if (RestrSolver.evaluateDNF(intersection) == RestrResult.IMPOSSIBLE) {
                // pouze v tomto případě nepřidáváme cestu do výsledku
                continue;
            }

            // zjistíme, v jakém typu resource jsme
            Vertex currentResource = GraphOperation.getResource(currentVertex);
            if (currentResource == null) {
                LOGGER.error("Vertex " + GraphOperation.getName(currentVertex) + " has no resource!");
                break;
            }
            String resourceType = GraphOperation.getType(currentResource);
            TreeStack treeStack = null;
            // pokud jsme došli až do tabulky -> zapíšeme, přes které vrcholy vedla cesta do tabulky
            if (isInterestingResource(resourceType)) {
                addVisitedPathToResult(returnVertexIds, currentItem);
                // přemažeme seznam ID, které čekaly na přidání do výsledku
                treeStack = new TreeStack();
            } else {
                // nedošli jsme do tabulky, tak dáme současné ID do seznamu ID, 
                // které se mají přidat do výsledku, pokud se po této cestě někdy do tabulky dojde
                treeStack = currentItem.getScriptResourcesToAdd().addNew(currentItem.getVertexId());
            }

            // značí, jestli je už pro tenhle uzel táhle nebo méně restriktivní formule navštívena
            if (!verticesRestrs.containsKey(currentItem.getVertexId())) {
                List<RestrDisjunctionTypeDnf> restrList = new ArrayList<RestrDisjunctionTypeDnf>();
                restrList.add(currentItem.getRestriction());
                verticesRestrs.put(currentItem.getVertexId(), restrList);
            } else {
                List<RestrDisjunctionTypeDnf> restrList = verticesRestrs.get(currentItem.getVertexId());
                // rozhodneme, jestli můžeme přeskočit aktuální uzel vzhledem k jeho kontextu 
                boolean canBeSkipped = false;
                for (RestrDisjunctionTypeDnf restr : restrList) {
                    RestrBinaryRelation restrRelation = RestrSolver.getRelation(currentItem.getRestriction(), restr);
                    if (restrRelation == RestrBinaryRelation.EQUAL
                            || restrRelation == RestrBinaryRelation.FIRST_MORE_RESTRICTIVE) {
                        canBeSkipped = true;
                        break;
                    }
                }

                // jestli byl navštíven tenhle uzel s více obecný (míň restriktivní) formulí, tak netreba vejít do nej znova  
                // z pohledu logiky = modely formule currentItem.restriciton se nacházeli i mezi modelama nejakeho uzlu z restrSet, který už byl navštíven predtím 
                if (canBeSkipped) {
                    addVisitedPathToResult(returnVertexIds, currentItem);
                    continue;
                }

                restrList.add(currentItem.getRestriction());
            }

            // v tomto vrcholu již algoritmus byl při průchodu této větve, jde o smyčku -> nevyhodnocovat dále
            if (currentItem.getHistory().contains(currentItem.getVertexId())) {
                addVisitedPathToResult(returnVertexIds, currentItem);
                continue;
            }

            // zásuvné filtry zakazují pokračovat dál -> přeskočit zpracovávání sousedů
            if (filterResult == FilterResult.OK_STOP) {
                addVisitedPathToResult(returnVertexIds, currentItem);
                continue;
            }

            Set<Long> newHistory = new HashSet<Long>(currentItem.getHistory());
            newHistory.add(currentItem.getVertexId());

            // přidat do fronty sousedy s direct hranou 
            List<Edge> adjacentDirectEdges = GraphOperation.getAdjacentEdges(currentVertex, direction, revisionInterval,
                    EdgeLabel.DIRECT);
            if (adjacentDirectEdges.size() > 0) {
                for (Edge edge : adjacentDirectEdges) {
                    queue.add(new QueueItem(edge.getVertex(direction.opposite()), edge, intersection, newHistory,
                            treeStack));
                }
            } else {
                addVisitedPathToResult(returnVertexIds, currentItem);
            }

            // pořešit sousedy s filter hranou
            if (getFilterEdgesEnabled()) {
                List<Edge> adjacentFilterEdges = GraphOperation.getAdjacentEdges(currentVertex, direction,
                        revisionInterval, EdgeLabel.FILTER);
                for (Edge edge : adjacentFilterEdges) {
                    // přidat vrchol jestliže ho zásuvné filtry schválí
                    Vertex testedVertex = edge.getVertex(direction.opposite());
                    if (testVertexWithFilters(testedVertex, edge) != FilterResult.NOK_STOP) {
                        // zjistíme, jestli nemáme zviditelnit nějaké transformace, protože jsme došli takto do tabulky
                        Vertex filterResource = GraphOperation.getResource(testedVertex);
                        resourceType = GraphOperation.getType(filterResource);
                        if (isInterestingResource(resourceType)) {
                            addVisitedPathToResult(returnVertexIds, currentItem);
                            returnVertexIds.add(testedVertex.getId());
                        }
                    }
                }
            }

        }
        return returnVertexIds;
    }

    /**
     * Přidá navštívené vrcholy v této cestě do výsledku.
     * @param returnVertexIds množina pro uchovávání výsledku
     * @param currentItem uzel, jehož cesta se má přidat
     */
    private void addVisitedPathToResult(Set<Object> returnVertexIds, QueueItem currentItem) {
        Iterator<Long> iterator = currentItem.getScriptResourcesToAdd().iterator();
        while (iterator.hasNext()) {
            returnVertexIds.add(iterator.next());
        }
        returnVertexIds.add(currentItem.getVertexId());
    }

    /**
     * Získá případné omezení v rámci hierarchie daného vrcholu.
     * Začne hledat v daném vrcholu a pak pokračuje od přímého předka.
     * @param vertex vrchol, jehož hierarchie se prohledává.
     * @param revisionInterval interval revizí, které se berou v potaz
     * @return první nalezené omezení nebo null, pokud žádné neexistuje
     */
    private RestrDisjunctionTypeDnf getRestrictionInHirearachy(Vertex vertex, RevisionInterval revisionInterval) {
        Vertex currentVertex = vertex;

        RestrDisjunctionTypeDnf restriction = null;
        while (currentVertex != null) {
            boolean restrictionFound = false;
            List<Object> attributeValueList = GraphOperation.getNodeAttribute(currentVertex,
                    AttributeNames.NODE_RESTRICTION_TYPE, revisionInterval);
            for (Object attributeValue : attributeValueList) {
                if (attributeValue instanceof RestrDisjunctionTypeDnf) {
                    restrictionFound = true;
                    if (restriction == null) {
                        restriction = (RestrDisjunctionTypeDnf) attributeValue;
                    } else {
                        restriction = RestrSolver.conjunctionDNF(restriction, (RestrDisjunctionTypeDnf) attributeValue);
                    }
                }
            }

            // zastavime na prvni nalezene restrikci
            if (restriction != null && !restrictionFound) {
                break;
            }

            currentVertex = GraphOperation.getParent(currentVertex);
        }

        return restriction;
    }

    /**
     * Ověří jestli daný resource je zajímavý.
     * Vrací true, když nejsou definovány databázové resource.
     * @param resourceType typ resource k ověření
     * @return true, jestliže je daný resource zajímavý, nebo nejsou definovány db resources 
     */
    private boolean isInterestingResource(String resourceType) {
        if (databaseResources != null) {
            return databaseResources.contains(resourceType);
        } else {
            return true;
        }
    }

    /**
     * @return množina databázových resource
     */
    public Set<String> getDatabaseResources() {
        return databaseResources;
    }

    /**
     * @param databaseResources množina databázových resource
     */
    public void setDatabaseResources(Set<String> databaseResources) {
        this.databaseResources = databaseResources;
    }

    /**
     * @return Maximální čas pro výpočet v ms.
     */
    public long getMaxExecutionTime() {
        return maxExecutionTime;
    }

    /**
     * @param maxExecutionTime Maximální čas pro výpočet v ms.
     */
    public void setMaxExecutionTime(long maxExecutionTime) {
        this.maxExecutionTime = maxExecutionTime;
    }

    /**
     * Prvek frony v daném algoritmu.
     * @author tfechtner
     */
    private static final class QueueItem {
        /** ID vrcholu ke zpracování. */
        private final Long vertexId;
        /** ID hrany po níž se do vrcholu došlo. */
        private final EdgeIdentification incomingEdgeId;
        /** Omezení nashromážděné v rámci předhozího procházení flow.*/
        private final RestrDisjunctionTypeDnf restriction;
        /** ID vrcholů na cestě zpracování flow. */
        private final Set<Long> history;
        /** ID vrcholů nashromážděné od posledního navštívení skutečné tabulky / databázového zdorje. */
        private final TreeStack scriptResourcesToAdd;

        /**
         * @param vertex Vrchol ke zpracování
         * @param incomingEdge Hrana po níž se do vrcholu došlo.
         * @param restriction Omezení nashromážděné v rámci předhozího procházení flow.
         * @param history ID vrcholů na cestě zpracování flow.
         */
        QueueItem(Vertex vertex, Edge incomingEdge, RestrDisjunctionTypeDnf restriction, Set<Long> history,
                TreeStack scriptResourcesToAdd) {
            this.vertexId = (Long) vertex.getId();
            if (incomingEdge != null) {
                this.incomingEdgeId = new EdgeIdentification(incomingEdge);
            } else {
                this.incomingEdgeId = null;
            }
            this.restriction = restriction;
            if (history != null) {
                this.history = Collections.unmodifiableSet(history);
            } else {
                this.history = Collections.emptySet();
            }
            this.scriptResourcesToAdd = scriptResourcesToAdd;
        }

        /**
         * @return ID vrcholu ke zpracování.
         */
        public Long getVertexId() {
            return vertexId;
        }

        /**
        * @return ID hrany po níž se do vrcholu došlo. 
        */
        public EdgeIdentification getIncomingEdgeId() {
            return incomingEdgeId;
        }

        /**
         * @return Omezení nashromážděné v rámci předhozího procházení flow.
         */
        public RestrDisjunctionTypeDnf getRestriction() {
            return restriction;
        }

        /**
         * @return ID vrcholů na cestě zpracování flow.
         */
        public Set<Long> getHistory() {
            return history;
        }

        /**
         * @return ID vrcholů nashromážděné od posledního navštívení skutečné tabulky / databázového zdorje.
         */
        public TreeStack getScriptResourcesToAdd() {
            return scriptResourcesToAdd;
        }
    }

    /**
     * Zásobník, který se může větvit.
     * @author mdvorak2
     */
    private static final class TreeStack implements Iterable<Long> {
        private TreeStackItem start;

        /**
         * Vytvoří nový prázdný zásobník.
         */
        TreeStack() {
            start = null;
        }

        /**
         * Vytvoří nový zásobník, který začíná novým prvkem a navazuje na něj původní zásobník.
         * Původní zásobník je však nezměněn.
         * @param vertexId přidané ID uzlu
         * @return nový zásobník
         */
        public TreeStack addNew(Long vertexId) {
            TreeStackItem newItem = new TreeStackItem(vertexId, start);
            TreeStack newStack = new TreeStack();
            newStack.start = newItem;
            return newStack;
        }

        /**
         * Prvek spojového seznamu Longů.
         * @author mdvorak2
         */
        private static final class TreeStackItem {
            private final Long id;
            private final TreeStackItem previous;

            TreeStackItem(Long vertexId, TreeStackItem previousItem) {
                id = vertexId;
                previous = previousItem;
            }
        }

        /**
         * Vrátí iterátor, aby bylo možné projít seznam Longů.
         */
        @Override
        public Iterator<Long> iterator() {
            return new Iterator<Long>() {

                private TreeStackItem pointer;

                @Override
                public boolean hasNext() {
                    if (pointer == null) {
                        return start != null;
                    }
                    return pointer.previous != null;
                }

                @Override
                public Long next() {
                    if (pointer == null) {
                        pointer = start;
                    } else {
                        pointer = pointer.previous;
                    }

                    if (pointer == null) {
                        throw new NoSuchElementException();
                    }

                    return pointer.id;
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }
    }

    @Override
    public void setLimit(long limit) {
        setMaxExecutionTime(maxExecutionTime);
    }
}
