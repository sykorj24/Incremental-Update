package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;

/**
 * Evaluator, který povolí všechny vrcholy.
 * @author tfechtner
 *
 */
public final class TruePredicate  implements VertexPredicate {
    private static final TruePredicate INSTANCE = new TruePredicate();

    private TruePredicate() {
    }

    /**
     * @return singleton instance 
     */
    public static TruePredicate getInstance() {
        return INSTANCE;
    }

    @Override
    public boolean evaluate(Vertex vertex, RevisionInterval revisionInterval) {
        return true;
    }
}