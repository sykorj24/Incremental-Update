package eu.profinit.manta.dataflow.repository.connector.titan.filter.provider;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilter;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterType;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.unit.HorizontalFilterImpl;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.unit.ResourceFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

/**
 * Provider horizontálních filtrů načítající konfiguraci ze souboru.
 * @author tfechtner
 *
 */
public class FileHorizontalFilterProvider implements HorizontalFilterProvider {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FileHorizontalFilterProvider.class);
    /** Kódování souboru s konfiguracemi. */
    private static final String ENCODING = "utf-8";
    /** Licence vyžadována k fungování.*/
    protected static final String MODULE_NAME = "mr_basic";

    /** Mapa drženýcj filtrů, klíč mapy je id filtru. */
    private Map<String, HorizontalFilter> filterMap = new HashMap<>();
    /** Soubor s konfiguracemi. */
    private File configurationFile;
    /** Holder pro přístup k databázi. */
    private DatabaseHolder databaseHolder;
    /** Holder pro ppřístup k super rootu. */
    private SuperRootHandler superRootHandler;

    @Override
    public HorizontalFilter getFilter(String filterId) {
        return filterMap.get(filterId);
    }

    @Override
    public boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval, Set<String> activeFilters) {
        for (String filterId : activeFilters) {
            if (isFiltered(vertex, revisionInterval, filterId)) {
                return true;
            }
        }

        return false;
    }


    @Override
    public boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval, String filterId) {
        if (filterId == null) {
            LOGGER.warn("Filter ID was null.");
            return false;
        }
        HorizontalFilter filter = getFilter(filterId);
        if (filter == null) {
            LOGGER.debug("Provider doesn't know filter with ID {}.", filterId);
            return false;
        }
        return filter.isFiltered(vertex, revisionInterval);
    }

    @Override
    public synchronized void addFilters(Set<HorizontalFilter> filterSet) {
        for (HorizontalFilter filter : filterSet) {
            if (!filterMap.containsKey(filter.getId())) {
                filterMap.put(filter.getId(), filter);
            } else {
                throw new IllegalArgumentException("There are several filters with ID " + filter.getId() + ".");
            }
        }
    }

    @Override
    public List<HorizontalFilter> getFilters() {
        return Collections.unmodifiableList(new ArrayList<>(filterMap.values()));
    }

    @Override
    public synchronized int refreshFilters() {
        filterMap.clear();
        addFilters(loadFiltersFromConfig());
        addFilters(loadResourceFilters());
        return filterMap.size();
    }

    /**
     * Vytvoří generované filtry pro resource.
     * @return množina resource filtrů, nikdy null
     */
    private Set<HorizontalFilter> loadResourceFilters() {
        return databaseHolder.runInTransaction(TransactionLevel.READ, new TransactionCallback<Set<HorizontalFilter>>() {
            @Override
            public String getModuleName() {
                return MODULE_NAME;
            }

            @Override
            public Set<HorizontalFilter> callMe(TitanTransaction transaction) {
                Set<HorizontalFilter> filters = new HashSet<>();

                Vertex superRoot = superRootHandler.getRoot(transaction);
                LOGGER.info("Super root = {}", superRoot.toString());
                List<Vertex> resources = GraphOperation.getAdjacentVertices(superRoot, Direction.IN,
                        RevisionRootHandler.EVERY_REVISION_INTERVAL, EdgeLabel.HAS_RESOURCE);

                int order = 0;
                Set<String> alreadyProcessedNames = new HashSet<>();
                for (Vertex resourceVertex : resources) {
                    String name = GraphOperation.getName(resourceVertex);
                    if (!alreadyProcessedNames.add(name)) {
                        continue;
                    }

                    ResourceFilterUnit filterUnit = new ResourceFilterUnit(Collections.singleton(name));
                    HorizontalFilterImpl filter = new HorizontalFilterImpl();
                    filter.setId(name);
                    filter.setName(name);
                    filter.setOrder(order++);
                    filter.setType(HorizontalFilterType.RESOURCE);
                    filter.setFilterUnits(Collections.<HorizontalFilterUnit> singletonList(filterUnit));
                    Vertex layerVertex = GraphOperation.getLayer(resourceVertex);
                    filter.setLayer(layerVertex.getProperty(NodeProperty.LAYER_NAME.t()).toString());
                    filters.add(filter);
                    LOGGER.debug("Added filter {}", name);
                }

                LOGGER.info("Loaded {} resource filters.", filters.size());
                return filters;
            }
        });
    }

    /**
     * Načte filtry z konfigurace.
     * @return množina načtených filtrů, nikdy null.
     */
    private Set<HorizontalFilter> loadFiltersFromConfig() {
        if (configurationFile == null || !configurationFile.exists()) {
            LOGGER.warn("Configuration file for horizontal filters doesn't exist.");
            return Collections.emptySet();
        }

        String content;
        try {
            content = FileUtils.readFileToString(configurationFile, ENCODING);
        } catch (IOException e) {
            LOGGER.error("Error during reading configuration file with filters.", e);
            return Collections.emptySet();
        }

        ObjectMapper mapper = new ObjectMapper();
        JavaType type = mapper.getTypeFactory().constructCollectionType(Set.class, HorizontalFilterImpl.class);
        try {
            Set<HorizontalFilter> customFilters = mapper.readValue(content, type);
            LOGGER.info("Loaded {} custom filters.", customFilters.size());
            return customFilters;
        } catch (JsonParseException e) {
            LOGGER.error("Error during parsing groups configuration.", e);
            return Collections.emptySet();
        } catch (JsonMappingException e) {
            LOGGER.error("Error during mapping groups configuration.", e);
            return Collections.emptySet();
        } catch (IOException e) {
            LOGGER.error("IO exception during processing groups configuration.", e);
            return Collections.emptySet();
        }
    }

    /**
     * @return Soubor s konfiguracemi
     */
    public File getConfigurationFile() {
        return configurationFile;
    }

    /**
     * @param configurationFile Soubor s konfiguracemi
     */
    public void setConfigurationFile(File configurationFile) {
        this.configurationFile = configurationFile;
    }

    /**
     * @return Holder pro přístup k databázi
     */
    public DatabaseHolder getDatabaseHolder() {
        return databaseHolder;
    }

    /**
     * @param databaseHolder Holder pro přístup k databázi
     */
    public void setDatabaseHolder(DatabaseHolder databaseHolder) {
        this.databaseHolder = databaseHolder;
    }

    /**
     * @return Holder pro ppřístup k super rootu
     */
    public SuperRootHandler getSuperRootHandler() {
        return superRootHandler;
    }

    /**
     * @param superRootHandler Holder pro ppřístup k super rootu
     */
    public void setSuperRootHandler(SuperRootHandler superRootHandler) {
        this.superRootHandler = superRootHandler;
    }
}
