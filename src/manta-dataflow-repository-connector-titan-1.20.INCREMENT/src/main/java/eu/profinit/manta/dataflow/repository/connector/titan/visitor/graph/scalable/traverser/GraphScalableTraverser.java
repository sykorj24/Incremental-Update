package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser;

import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.AccessLevel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Rozhraní pro traverser procházející graf ve více sekvenčně spouštěných transakcích.
 * Díky tomu je při správném nastavení odolný vůči vyčerpání veškeré paměti kvůli
 * přílišnému cachování ze strany Titána v jedné transakci.
 * @author tfechtner
 *
 */
public interface GraphScalableTraverser {

    /**
     * Provede traverzování grafem.
     * @param visitor visitor navštěvující jednotlivé elementy
     * @param startVertexId id vrcholu, jehož podstrom se prochází, může být jakéhokoliv typu
     * @param accessLevel vyžadovaný přístup do databáze
     * @param revisionInterval interval revizí, pro který se graf prochází
     */
    void traverse(GraphVisitor visitor, Object startVertexId, AccessLevel accessLevel,
            RevisionInterval revisionInterval);

}
