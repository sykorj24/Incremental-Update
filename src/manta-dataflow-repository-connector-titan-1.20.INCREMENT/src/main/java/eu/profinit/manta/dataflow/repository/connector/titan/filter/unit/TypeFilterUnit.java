package eu.profinit.manta.dataflow.repository.connector.titan.filter.unit;

import java.util.Collections;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.predicate.VertexTypePredicate;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Jednotkový filtr filtrující podle typu uzlu nebo kteréhokoliv jeho předka.
 * @author tfechtner
 *
 */
public class TypeFilterUnit implements HorizontalFilterUnit {
    /** Predikát používaný pro samotný výpočet filtru. */
    private final VertexTypePredicate predicate;
    /** Množina názvů resourců, na kterých se definovaný pattern testuje. */
    private final Set<String> impactedResources;

    /**
     * @param impactedResources Množina názvů resourců, na kterých se definovaný pattern testuje
     * @param disabledTypes množina typů, jejichž přítomnost způsobí vyfiltrování
     */
    @JsonCreator
    public TypeFilterUnit(@JsonProperty(value = "impactedResources") Set<String> impactedResources,
            @JsonProperty(value = "disabledTypes") Set<String> disabledTypes) {
        if (impactedResources != null) {
            this.impactedResources = impactedResources;
        } else {
            this.impactedResources = Collections.emptySet();
        }

        predicate = new VertexTypePredicate(disabledTypes);
    }

    @Override
    public boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval) {
        while (vertex != null) {
            String rName = GraphOperation.getName(GraphOperation.getResource(vertex));
            if (impactedResources.isEmpty() || impactedResources.contains(rName)) {
                if (predicate.evaluate(vertex, revisionInterval)) {
                    return true;
                }
            }
            vertex = GraphOperation.getParent(vertex);
        }

        return false;
    }

}
