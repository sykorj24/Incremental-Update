package eu.profinit.manta.dataflow.repository.connector.titan;

/**
 * Pořadí, podle kterého se vykonávají context aware třídy.
 * @author tfechtner
 *
 */
public enum ContextEventAwareOrder {
    /** Načtení licence. */
    LICENSE_LOADER(0),
    /** Inicializace databáze. */
    DATABASE_HOLDER(1),
    /** Načtení horizontálních filtrů. */
    HORIZONTAL_FILTERS(2);

    /** Pořadí vykonání - 0 je první, N poslední. */
    private final int order;

    /**
     * @param order Pořadí vykonání - 0 je první, N poslední
     */
    private ContextEventAwareOrder(int order) {
        this.order = order;
    }

    /**
     * @return Pořadí vykonání - 0 je první, N poslední
     */
    public int getOrder() {
        return order;
    }
}
