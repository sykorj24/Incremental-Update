package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import java.util.Set;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;

/**
 * Evaluátor ověřující jestli hodnota property vrcholu patří do zadané množiny.
 * Pokud danou property nemá, tak nesplňuje kritérium.
 * @author tfechtner
 *
 */
public class VertexPropertyPredicate implements VertexPredicate {
    /** Množina povolených hodnot. */
    private Set<String> valueSet;
    /** Název property vrcholu k ověření. */
    private String propertyName;
    
    /**
     * Default konsturktor, nutno nastavit fieldy {@link #valueSet} a {@link #propertyName} pro 
     * správné fungování třídy. 
     */
    public VertexPropertyPredicate() {
    }

    /**
     * @param valueSet  Množina povolených hodnot.
     * @param propertyName Název property vrcholu k ověření.
     */
    public VertexPropertyPredicate(Set<String> valueSet, String propertyName) {
        super();
        this.valueSet = valueSet;
        this.propertyName = propertyName;
    }

    @Override
    public boolean evaluate(Vertex vertex, RevisionInterval revisionInterval) {
        if (propertyName == null) {
            throw new IllegalStateException("Property name must not be null.");
        }
        if (valueSet == null) {
            throw new IllegalStateException("Value set must not be null.");
        }
        if (vertex == null) {
            throw new IllegalStateException("Vertex must not be null.");
        }
        
        String type = vertex.getProperty(propertyName);
        if (type != null) {
            return valueSet.contains(type);
        } else {
            return false;
        }
    }

    /**
     * @return Množina povolených hodnot
     */
    public Set<String> getValueSet() {
        return valueSet;
    }

    /**
     * @param valueSet Množina povolených hodnot
     */
    public void setValueSet(Set<String> valueSet) {
        this.valueSet = valueSet;
    }

    /**
     * @return  Název property vrcholu k ověření.
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName  Název property vrcholu k ověření.
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
}
