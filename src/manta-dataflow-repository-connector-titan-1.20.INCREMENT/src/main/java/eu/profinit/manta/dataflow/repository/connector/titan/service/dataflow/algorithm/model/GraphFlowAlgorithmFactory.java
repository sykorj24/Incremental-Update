package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model;

import java.util.List;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.GraphFlowFilter;

/**
 * Továrna pro tvorbu {@link GraphFlowAlgorithm}.
 * @author tfechtner
 *
 */
public interface GraphFlowAlgorithmFactory {

    /**
     * Vytvoří novou instanci {@link GraphFlowAlgorithm}.
     * @return nová instance
     */
    GraphFlowAlgorithm createGraphFlowAlgorithm();

    /**
     * Vytvoří novou instanci {@link GraphFlowAlgorithm}.
     * Algoritmu nastaví flag pro filter hrany.
     * @param isFilterEdges true, jestli se mají procházet i filter hrany
     * @return nová instance
     */
    GraphFlowAlgorithm createGraphFlowAlgorithm(boolean isFilterEdges);
    
    /**
     * Vytvoří novou instanci {@link GraphFlowAlgorithm}.
     * Algoritmu nastaví případné předané filtry.
     * 
     * @param additionalFilters filtry, které se mají v algoritmu použít (může být null)
     * 
     * @return nová instance
     */
    GraphFlowAlgorithm createGraphFlowAlgorithm(List<GraphFlowFilter> additionalFilters);
    
    /**
     * Vytvoří novou instanci {@link GraphFlowAlgorithm}.
     * Algoritmu nastaví flag pro filter hrany a případné předané filtry.
     * 
     * @param isFilterEdges true, jestli se mají procházet i filter hrany
     * @param additionalFilters filtry, které se mají v algoritmu použít (může být null)
     * 
     * @return nová instance
     */
    GraphFlowAlgorithm createGraphFlowAlgorithm(boolean isFilterEdges, List<GraphFlowFilter> additionalFilters);
}

