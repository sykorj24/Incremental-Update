package eu.profinit.manta.dataflow.repository.connector.titan.filter.model;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Rozhraní pro definici horizontálního filtru.
 * Horizontální filtry jsou jak genererované pro resourcy (1 resource jeden filtr),
 * tak custom filtry předdefinované i custom. 
 * @author tfechtner
 *
 */
public interface HorizontalFilter {

    /**
     * Ověří jestli je daný vrchol vyfiltrován tímto filtrem.
     * @param vertex testovaný vrchol
     * @param revisionInterval interval revizí, ve které se zkoumá
     * @return true, jestliže je vrchol vyfiltrován
     */
    boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval);

    /**
     * Vrací id filtru.
     * @return id filtru
     */
    String getId();

    /**
     * Vrací název filtru.
     * @return název filtru
     */
    String getName();

    /**
     * Vrací pořadí filtru. 
     * 0 je první filtr, N je poslední filtr.
     * @return pořadí filtru
     */
    int getOrder();

    /**
     * Typ filtru, jestli jde o generovaný resource filtr nebo custom.
     * @return typ fitru
     */
    HorizontalFilterType getType();

    /**
     * @param layer Vrstva, pro kterou lze tento filtr nastavit. Pokud je {@code null}, pak lze nastavit pro vsechny vrstvy
     */
    String getLayer();
}
