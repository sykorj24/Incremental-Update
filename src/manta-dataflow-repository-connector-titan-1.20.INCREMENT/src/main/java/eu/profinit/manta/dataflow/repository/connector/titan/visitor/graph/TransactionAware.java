package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import com.thinkaurelius.titan.core.TitanTransaction;

/**
 * Rozhraní pro třídy, které by rády získali od traverseru odkaz na transakci.
 * @author tfechtner
 *
 */
public interface TransactionAware {

    /**
     * Nastaví aktuálně používanou transakci.
     * @param transaction používaná transakce
     */
    void setTransaction(TitanTransaction transaction);
}
