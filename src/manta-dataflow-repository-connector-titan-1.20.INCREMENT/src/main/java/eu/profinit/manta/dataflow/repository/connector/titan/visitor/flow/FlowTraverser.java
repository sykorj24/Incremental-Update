package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Rozhraní pro procházení data flow.
 * @author tfechtner
 *
 */
public interface FlowTraverser {

    /**
     * Prida visitora do seznamu visitoru tohoto traverseru, ktere
     * budou volany pri zavolani metody 
     * {@link #traverse(FlowVisitor, TitanTransaction, Collection, EdgeTypeTraversing, Direction)}.
     * 
     * @param visitor visitor, ktery bude pridany do seznamu
     * 
     * @see #removeVisitor(FlowVisitor)
     */
    void addVisitor(FlowVisitor visitor);

    /**
     * Odstrani visitora ze seznamu visitoru. 
     * 
     * @param visitor visitor, ktery bude odebrany ze seznamu
     * 
     * @see #addVisitor(FlowVisitor)
     */
    void removeVisitor(FlowVisitor visitor);

    /**
     * Započne traverzování po data flow. Navštíví i startovní uzly.
     * 
     * <p>
     * <b>Pozor:</b> Opakované volání této metody se stejnými parametry
     * by mělo být idempotentní - tzn. pokaždé by mělo projít ty samé
     * uzly i pokud je tato metoda volaná nad tou samou instancí traverseru.
     * 
     * @param transaction transakce, pro přístup k db
     * @param startNodesIds počáteční uzly
     * @param edgeType styl procházení
     * @param direction směr procházení. {@link Direction#OUT} = hrany vycházející z daného uzlu
     * @param revInterval interval revizí, ve kterých má traverser procházet
     */
    void traverse(TitanTransaction transaction, Iterable<Object> startNodesIds, EdgeTypeTraversing edgeType,
            Direction direction, RevisionInterval revInterval);

    /**
     * Započne traverzování po data flow. Navštíví i startovní uzly.
     * 
     * <p>
     * Semantika teto metody je stejna jako volani:
     * 
     * <pre>
     * traverser.addVisitor(visitor);
     * traverser.traverse(transaction, startNodes, edgeType, direction);
     * traverser.removeVisitor(visitor);
     * </pre>
     * 
     * @param visitor visitor volány pro navštívení uzlů a rozhodnutí dalšího cestování
     * @param transaction transakce, pro přístup k db
     * @param startNodesIds počáteční uzly
     * @param edgeType styl procházení
     * @param direction směr procházení. {@link Direction#OUT} = hrany vycházející z daného uzlu
     * @param revInterval interval revizí, ve kterých má traverser procházet
     */
    void traverse(FlowVisitor visitor, TitanTransaction transaction, Iterable<Object> startNodesIds,
            EdgeTypeTraversing edgeType, Direction direction, RevisionInterval revInterval);
}
