package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable;

import java.util.Deque;

/**
 * Styl procházení grafu.
 * @author tfechtner
 *
 */
public interface SearchOrder {

    final int DEFAULT_MAX_POLL_WAIT = 1000;

    /**
     * Vybere další element ke zpracování na základě stylu.
     * Počítá s tím, že nové elementy se přidávají na konec fronty.
     * @param deque fronta, z níž se odebíra element
     * @return element ke zpracování
     * 
     * @param <T> typ elementu ve frontě
     */
    <T> T getNext(Deque<T> deque);
}
