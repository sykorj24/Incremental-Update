package eu.profinit.manta.dataflow.repository.connector.titan.service;

import java.util.Date;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;

/**
 * Datová třídá o revizi. <br />
 * Porovnávání probíhá přes číslo revize vzestupně.
 * @author tfechtner
 * @author jsykora
 *
 */
public class RevisionModel implements Comparable<RevisionModel> {
    /** Číslo revize. */
    private final Double revision;
    /** Previous revision number. */
    private final Double previousRevision;
    /** Next revision number. */
    private final Double nextRevision;
    /** Čas commitnutí. */
    private final Date commitTime;
    /** True, jestliže je revize commitnutá. */
    private final boolean isCommitted;

    /**
     * @param vertex vertex, pro který se vytváří 
     */
    public RevisionModel(Vertex vertex) {
        revision = vertex.getProperty(NodeProperty.REVISION_NODE_REVISION.t());
        if (revision == null) {
            throw new IllegalStateException("The revision node does not have defined a revision number.");
        }
        
        Double previousRevisionRetrieved = vertex.getProperty(NodeProperty.REVISION_NODE_PREVIOUS_REVISION.t());
        if (previousRevisionRetrieved == -1.0) {
            previousRevision = null;
        } else {
            previousRevision = previousRevisionRetrieved;
        }
        
        Double nextRevisionRetrieved = vertex.getProperty(NodeProperty.REVISION_NODE_NEXT_REVISION.t());
        if (nextRevisionRetrieved == -1.0) {
            nextRevision = null;
        } else {
            nextRevision = nextRevisionRetrieved;
        }

        Boolean isCommittedObject = vertex.getProperty(NodeProperty.REVISION_NODE_COMMITTED.t());
        if (isCommittedObject == null) {
            throw new IllegalStateException("The revision node does not have defined flag about commit.");
        }
        isCommitted = isCommittedObject;

        commitTime = vertex.getProperty(NodeProperty.REVISION_NODE_COMMIT_TIME.t());
    }

    /**
     * @return Číslo revize.
     */
    public Double getRevision() {
        return revision;
    }
    
    
    /**
     * 
     * @return Previous revision number or null, if there is no previous revision
     */
    public Double getPreviousRevision() {
        return previousRevision;
    }

    /**
     * 
     * @return Next revision number or null, if there is no next revision
     */
    public Double getNextRevision() {
        return nextRevision;
    }

    /**
     * @return Čas commitnutí.
     */
    public Date getCommitTime() {
        return new Date(commitTime.getTime());
    }

    /**
     * @return True, jestliže je revize commitnutá. 
     */
    public boolean isCommitted() {
        return isCommitted;
    }

    @Override
    public String toString() {
        return "Revision [#" + revision + "," + commitTime + "," + isCommitted + "]";
    }

    @Override
    public int compareTo(RevisionModel o) {
        return this.getRevision().compareTo(o.getRevision());
    }

    @Override
    public int hashCode() {
        return (revision == null) ? 0 : revision.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        RevisionModel other = (RevisionModel) obj;
        if (revision == null) {
            if (other.revision != null) {
                return false;
            }
        } else if (!revision.equals(other.revision)) {
            return false;
        }
        return true;
    }    
    
}