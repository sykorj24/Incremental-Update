package eu.profinit.manta.dataflow.repository.connector.titan.service.sourcecode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.AttributeNames;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Defaultni implementace {@link SourceCodeService}.
 * 
 * @author onouza
 */
public class SourceCodeServiceImpl implements SourceCodeService {

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(SourceCodeServiceImpl.class);
    /** Defaultni kodovani skriptu */
    private static final String ENCODING = "utf-8";
    /** Nazev atributu, kde je ulozeno umisteni skriptu. */
    static final String SOURCE_LOCATION = "sourceLocation";
    /** Nazev atributu, kde je ulozeno kodovani skriptu. */
    static final String SOURCE_ENCODING = "sourceEncoding";
    /** Pattern pro naparsovani pozice ve skriptu z atributu. */
    private static final Pattern ATTRIBUTE_POSITION_PATTERN = Pattern.compile("\\s*<?\\s*([0-9]+)\\s*,\\s*([0-9]+)\\s*>?\\s*");
    
    /**
     * Komponenta pro cteni souboru zdrojoveho kodu
     */
    private SourceFileLoader sourceFileLoader;
    
    @Override
    public String getSourceCode(Vertex node, RevisionInterval revisionInterval, boolean reduceIndent) {
        Validate.notNull(node, "'node' must not be null");
        Validate.notNull(revisionInterval, "'revisionInterval' must not be null");
        
        // Inverzni cesta k uzlu - od uzlu ke koreni
        List<Vertex> reversePath = new ArrayList<>(Collections.singletonList(node));
        reversePath.addAll(GraphOperation.getAllParent(node));
        
        String vertexName = GraphOperation.logVertex(node);
        
        // Pocatecni pozice fragmentu zdrojoveho kodu v souboru
        Position startPosition = findPosition(reversePath, AttributeNames.NODE_SCRIPT_POSITION, revisionInterval);
        if (startPosition == null) {
            LOGGER.error("Unable to find source start position of node {}", vertexName);
            return null;
        }

        // Koncova pozice fragmentu zdrojoveho kodu v souboru
        Position endPosition = findPosition(reversePath, AttributeNames.NODE_SCRIPT_STOP_POSITION, revisionInterval);
        if (endPosition == null) {
            LOGGER.error("Unable to find source end position of node {}", vertexName);
            return null;
        }
        
        if (startPosition.compareTo(endPosition) >= 0) {
            LOGGER.error("Source start position {} is qreater than or equal to end position {} for node {}", startPosition, endPosition, vertexName);
            return null;
        }

        // Umisteni souboru zdrojoveho kodu
        SourceFile sourceFile = findSourceLocation(reversePath, revisionInterval);
        if (sourceFile == null) {
            LOGGER.error("Unable to find source location for node {}", vertexName);
            return null;
        }

        // Obsah souboru zdrojoveho kodu
        List<String> sourceContent = sourceFileLoader.loadSourceFile(sourceFile);
        
        if (sourceContent == null) {
            LOGGER.error("Unable to get source content for node {}", vertexName);
            return null;
        }
        
        if (sourceContent.size() < startPosition.getLine()) {
            LOGGER.error("Start line {} is out of source line count {} for node {}", startPosition.getLine(), sourceContent.size(), vertexName);
            return null;
        }
        
        if (sourceContent.size() < endPosition.getLine() - 1) {
            LOGGER.error("End line {} is out of source line count {} for node {}", endPosition.getLine(), sourceContent.size(), vertexName);
            return null;
        }
        
        // Z obsahu zdrojoveho souboru zjistime cast, ktera se vaze na zkoumany uzel.
        // To provedeme na zaklade pocatecni a koncove pozice zdrojoveho kodu uzlu.
        List<String> nodeSource = new LinkedList<>();
        ListIterator<String> sourceIterator = sourceContent.listIterator(startPosition.getLine() - 1);
        int currentLine = startPosition.getLine();
        while (sourceIterator.hasNext()) {
            String line = sourceIterator.next();
            if (currentLine == endPosition.getLine()) {
                // Pokud jsme na poslednim radku, vezmene pouze jeho cast, urcenou koncovou pozici
                if (line.length() < endPosition.getCol() - 1) {
                    LOGGER.error("End column {} is out line {} length ({}) for node {}", endPosition.getCol(), currentLine, line.length(), vertexName);
                    return null;
                }
                line = line.substring(0, endPosition.getCol() - 1);
            }
            if (currentLine == startPosition.getLine()) {
                // Pokud jsme na prvnim radku, vezmene pouze jeho cast, urcenou pocatecni pozici.
                // Znaky do pocatecni pozice nahradime mezerami.
                if (line.length() < startPosition.getCol()) {
                    LOGGER.error("Start column {} is out line {} length ({}) for node {}", startPosition.getCol(), currentLine, line.length(), vertexName);
                    return null;
                }
                String firstLineIndent = StringUtils.repeat(" ", startPosition.getCol() - 1);
                line = firstLineIndent + line.substring(startPosition.getCol() - 1);
            }
            nodeSource.add(line);
            if (currentLine == endPosition.getLine()) {
                break;
            }
            currentLine++;
        }
        
        if (reduceIndent) {
            nodeSource = reduceLineIndent(nodeSource);
        }

        // Vysledek vratime jako String
        StringBuilder result = new StringBuilder();
        for (String line : nodeSource) {
            result.append(line).append(System.lineSeparator());
        }
        return result.toString();
    }

    /**
     * Provede redukci odsazeni v danem zdrojovem kodu
     * @param lines Zdrojovy kod, kde se ma redukce odsazeni provest.
     * @return Zdrojovy kod se zredukovanym odsazenim
     */
    private List<String> reduceLineIndent(List<String> lines) {
        // minimalni odsazeni neprazdneho radku
        int minIndent = Integer.MAX_VALUE;
        for (String line : lines) {
            // Zajimaji nas pouze neprazdne radky
            if (!line.trim().isEmpty()) {
                int indent = 0;
                for (char character : line.toCharArray()) {
                    if (!Character.isWhitespace(character)) {
                        break;
                    }
                    indent++;
                }
                minIndent = Math.min(indent, minIndent);
            }
        }
        
        // Zkratime odsazeni radku o zjistene minimum
        List<String> result = new LinkedList<>();
        for (String line : lines) {
            result.add(line.length() > minIndent ? line.substring(minIndent) : "");
        }
        return result;
    }

    /**
     * Nalezne pozici prikazu ve skriptu. Postupne prozkouma vsechny uzly na inverzni ceste ke zkoumanemu uzlu
     * @param reversePath Inverzni cesta ke zkoumanemu uzlu (cili s razenim od zkoumaneho uzlu ke koreni)
     * @param positionAttribute Nazev atributu , kde je pozice ulozena
     * @param revisionInterval interval revizi platnosti hodnoty pozicniho atributu
     * @return pozice ve skriptu nebo {@code null}, pokud nebyla pozice nalezena
     */
    private Position findPosition(List<Vertex> reversePath, String positionAttribute, RevisionInterval revisionInterval) {
        for (Vertex entry : reversePath) {
            Position pos = fetchPosition(entry, positionAttribute, revisionInterval);
            if (pos != null) {
                return pos;
            }
        }
        return null;
    }

    /**
     * Ziska pozici prikazu ve skriptu pro dany uzel.
     * 
     * @param vertex Zkoumany uzel
     * @param positionAttribute Nazev atributu , kde je pozice ulozena
     * @param revisionInterval interval revizi platnosti hodnoty pozicniho atributu
     * 
     * @return pozice ve skriptu nebo {@code null}, pokud uzel informaci o pozici neobsahuje
     */
    private Position fetchPosition(Vertex vertex, String positionAttribute, RevisionInterval revisionInterval) {
        // try to fetch position from attribute
        Object posAttribute = getUniqueNodeAttribute(vertex, positionAttribute, revisionInterval);
        if (posAttribute instanceof String) {
            Matcher attrMatcher = ATTRIBUTE_POSITION_PATTERN.matcher((String) posAttribute);
            if (attrMatcher.matches()) {
                int line = Integer.parseInt(attrMatcher.group(1));
                int col = Integer.parseInt(attrMatcher.group(2));

                return new Position(line, col);
            }
        }
        return null;
    }

    /**
     * Ziska umisteni skriptu. Postupne prozkouma vsechny uzly na inverzni ceste ke zkoumanemu uzlu.
     * @param reversePath Inverzni cesta ke zkoumanemu uzlu (cili s razenim od zkoumaneho uzlu ke koreni)
     * @param revisionInterval interval revizi platnosti umisteni skriptu
     * @return umisteni skriptu nebo {@code null} pokud se umisteni nepodarilo nalezt
     */
    private SourceFile findSourceLocation(List<Vertex> reversePath, RevisionInterval revisionInterval) {
        for (Vertex entry : reversePath) {
            Object sourceLocation = getUniqueNodeAttribute(entry, SOURCE_LOCATION, revisionInterval);
            if (sourceLocation != null) {
                return new SourceFile(sourceLocation.toString(), findSourceEncoding(entry, revisionInterval));
            }
        }
        return null;
    }

    /**
     * Metoda se pokusi najit kodovani zdrojoveho skriptu daneho uzlu.
     * Pokud metoda nenajde prave jeden odpovidajici atribut, vrati vychozi hodnotu.
     * @param node Zkoumany uzel
     * @param revisionInterval interval revizi platnosti kodovani
     * @return kodovani zdroje nebo vychozi, pokud neexistuje
     */
    private String findSourceEncoding(Vertex node, RevisionInterval revisionInterval) {
        Object encoding = getUniqueNodeAttribute(node, SOURCE_ENCODING, revisionInterval);
        if (encoding != null) {
            return encoding.toString();
        } else {
            return ENCODING;
        }
    }

    /**
     * Vrati hodnotu atributu daneho uzlu v pripade, ze je unikatni. V opacnem pripade vrati {@code null}.
     * 
     * <p>Podrobneji:</p>
     * <ul>
     *   <li>Pokud pro dany atribut existuje prave jedna hodnota, vrati tuto hodnotu.</li>
     *   <li>Pokud pro dany atribut hodnota neexistuje, vrati {@code null}.</li>
     *   <li>Pokud pro dany atribut existuje hodnot vice, precte hodnoty platne pro posledni verzi zkoumaneho intervalu.</li>
     *   <ul>
     *     <li>Pokud pro dany atribut existuje prave jedna hodnota, vrati tuto hodnotu.</li>
     *     <li>Pokud pro dany atribut hodnota neexistuje nebo jich existuje vice, vrati {@code null}.</li>
     *   </ul>
     * </ul>
     * 
     * @param node Zkoumany uzel.
     * @param attributeName Nazev atributu, jehoz hodnota nas zajima
     * @param revisionInterval Interval revizi platnosti hodnoty atributu
     * @return Hodnotu atributu daneho uzlu, pokud je unikatni; jinak {@code null}.
     */
    private Object getUniqueNodeAttribute(Vertex node, String attributeName, RevisionInterval revisionInterval) {
        List<Object> attrValues = GraphOperation.getNodeAttribute(node, attributeName, revisionInterval);
        if (attrValues.size() >= 2) {
            attrValues = GraphOperation.getNodeAttribute(node, attributeName, new RevisionInterval(revisionInterval.getEnd()));
        }
        if (attrValues.size() == 1) {
            return attrValues.get(0);
        }
        if (attrValues.size() >= 2) {
            String vertexName = GraphOperation.logVertex(node);
            LOGGER.error(String.format("There are more values of attribute %s of node %s: %s", attributeName, vertexName, attrValues));
        }
        return null;
    }

    // gettery / settery
    
    public void setSourceFileLoader(SourceFileLoader sourceFileLoader) {
        this.sourceFileLoader = sourceFileLoader;
    }

    // vnitrni tridy

    /**
     * Immutable datovas trida pro reprezentaci pozice ve skriptu.
     * 
     * @author tfechtner
     * @author onouza
     */
    private static class Position implements Comparable<Position> {
        /** Radek ve skriptu.*/
        private final int line;
        /** Sloupec ve skriptu.*/
        private final int col;

        /**
         * @param line radek ve skriptu
         * @param col sloupec ve skriptu
         */
        public Position(int line, int col) {
            this.line = line;
            this.col = col;
        }
        
        // prekryti / implementace

        @Override
        public int compareTo(Position other) {
            if (other == null) {
                throw new NullPointerException("Cannot compare to null value.");
            }
            if (this.line < other.line) {
                return -1;
            } else if (this.line > other.line) {
                return 1;
            } else {
                if (this.col < other.col) {
                    return -1;
                } else if (this.col > other.col) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + col;
            result = prime * result + line;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Position other = (Position) obj;
            if (col != other.col) {
                return false;
            }
            if (line != other.line) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "<" + line + "," + col + ">";
        }

        // gettery / settery

        /**
         * @return radek ve skriptu
         */
        public int getLine() {
            return line;
        }
        
        /**
         * @return sloupec ve skriptu
         */
        public int getCol() {
            return col;
        }
    }
}
