package eu.profinit.manta.dataflow.repository.connector.titan.filter.unit;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Jednotkový filtr filtrující podle hodnoty atributu na daném uzlu nebo kteréhokoliv jeho předka.  
 * @author tfechtner
 *
 */
public class AttributeFilterUnit implements HorizontalFilterUnit {
    /** Klíč ověřovaného atributu. */
    private final String key;
    /** Množina hodnot, jejichž přítomnost způsobí vyfiltrování. */
    private final Set<String> disabledValues;
    /** Množina názvů resourců, na kterých se definovaný pattern testuje. */
    private final Set<String> impactedResources;
    /** Množina typů, na kterých se definovaný pattern testuje. */
    private final Set<String> impactedTypes;

    /**
     * @param key klíč atributu
     * @param disabledValues množina hodnot, jejichž přítomnost způsobí vyfiltrování
     */
    @JsonCreator
    public AttributeFilterUnit(@JsonProperty(value = "impactedResources") Set<String> impactedResources,
            @JsonProperty(value = "impactedTypes") Set<String> impactedTypes, @JsonProperty(value = "key") String key,
            @JsonProperty(value = "disabledValues") Set<String> disabledValues) {

        if (impactedTypes != null) {
            this.impactedTypes = impactedTypes;
        } else {
            this.impactedTypes = Collections.emptySet();
        }

        if (impactedResources != null) {
            this.impactedResources = impactedResources;
        } else {
            this.impactedResources = Collections.emptySet();
        }

        this.key = key;
        this.disabledValues = disabledValues;
    }

    @Override
    public boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval) {
        while (vertex != null) {
            String rName = GraphOperation.getName(GraphOperation.getResource(vertex));
            if (impactedResources.isEmpty() || impactedResources.contains(rName)) {
                String vType = GraphOperation.getType(vertex);
                if (impactedTypes.isEmpty() || impactedTypes.contains(vType)) {
                    List<Object> attrs = GraphOperation.getNodeAttribute(vertex, key, revisionInterval);
                    for (String disable : disabledValues) {
                        if (attrs.contains(disable)) {
                            return true;
                        }
                    }
                }
            }
            vertex = GraphOperation.getParent(vertex);
        }

        return false;
    }
}
