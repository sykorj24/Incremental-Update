package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser;

import java.util.Deque;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.dataflow.repository.connector.titan.connection.RepositoryRollbackException;
import eu.profinit.manta.dataflow.repository.connector.titan.connection.RollbackAware;
import eu.profinit.manta.dataflow.repository.connector.titan.service.ExecutorServiceTerminator;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.AccessLevel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrder;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserPostProcessing;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactory;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Konfigurovatelný traverser procházející graf ve více sekvenčně spouštěných transakcí ve více vláknech paralelně.
 * Na základě továrny si vytvoří {@link TraverserProcessor}, jehož instance se opakovaně volají v 
 * paralelních vláknech pro zpracování příslušného počtu elementů. Každé takové volání probíhá ve zvláštní transakci.
 * <br /><br />
 * Jestliže visitor provádí úpravy nad databází je třeba se zamyslet nad synchronizací. 
 * <b>Hlavně pozor na rollbacky.</b>
 * 
 * <h3>Design třídy</h3>
 * Traverser spouští dva druhy vláken:
 * <ul>
 * <li>terminator - pouze čeká na závoře dokud neskončí všechna pracovní vlákna a pak ukončí ExecutorService;</li>
 * <li>launcher/pracovní vlákno - provádí samotný průchod grafem a zpracování vrcholů, svou činnost končí v momentě, 
 * kdy všechna ostatní vlákna čekají ve fronte vrcholů ke zpracování nebo definitivně skončily.</li>
 * </ul>
 * Všechny pracovní vlákna sdílejí následující synchronizační struktury:
 * <ul>
 * <li>idsToProcess - blokující fronta obsahující id vrcholů ke zpracování, kam pracovní vlákna vkládají vrcholy 
 * ke zpracování a zas odtud odebírají to, které budou zpracovávat;</li>
 * <li>workingCounter - počet pracovních vláken, které aktuálně pracují. Vlákno nepracuje pokud
 *      <ul>
 *          <li>čeká ve frontě idsToProcess na další vrchol ke zpracování;</li>
 *          <li>znovu startuje transakci;</li>
 *          <li>definitivně skončilo.</li>
 *      </ul>
 * </li>
 * </ul>
 * Algoritmus používá zámek pro zjištění přístupu k workingCounter. Tento zámek je read-write a spojuje oblasti:
 * <ol>
 *      <li>získání dalšího vrcholu z fronty (read);
 *      <li>kontroly počtu pracujících vláken a případného spuštění další transakce (write).
 * </ol>      
 * To s sebou přináší omezení, že pracovní vlákno nezačne další práci v nové transakci, dokud alespoň 
 * jedno jiné vlákno čeká na data. Zajišťuje to však, že v okamžiku znovuspouštění transace ostatní 
 * vlákna skutečně pracují. Díky tomu nedojde k zaseknutí na konci zpracování, kdyby se střídavě v 
 * cyklu spouštěla v jednom vláknu nová transakce a druhé vlákno by naprázdno čekalo. 
 *  
 * @author tfechtner
 *
 */
public class GraphScalableTraverserParallel implements GraphScalableTraverser {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(GraphScalableTraverserParallel.class);

    /** Služba pro přístup k databázi. */
    private final DatabaseHolder databaseHolder;
    /** Továrna na vytvoření procesoru. */
    private final TraverserProcessorFactory processorFactory;
    /** Počet paralelních vláken. */
    private final int threadNumber;

    /**
     * @param databaseHolder Služba pro přístup k databázi.
     * @param processorFactory Továrna na vytvoření procesoru
     * @param threadNumber Počet paralelních vláken.
     */
    public GraphScalableTraverserParallel(DatabaseHolder databaseHolder, TraverserProcessorFactory processorFactory,
            int threadNumber) {
        super();
        this.databaseHolder = databaseHolder;
        this.processorFactory = processorFactory;
        this.threadNumber = threadNumber;
    }

    @Override
    public void traverse(GraphVisitor visitor, Object startVertexId, AccessLevel accessLevel,
            RevisionInterval revisionInterval) {
        // Fronta na vrcholy ke zpracování.
        BlockingDeque<Object> idsToProcess = new LinkedBlockingDeque<>();
        idsToProcess.addLast(startVertexId);
        // Zámek pro vymezení přístupu na čítač aktuálně pracujících vláken.
        ReentrantReadWriteLock controlLock = new ReentrantReadWriteLock();
        // čítač počtu aktuálně pracujících vláken
        AtomicInteger workingCounter = new AtomicInteger(threadNumber);
        // závora udržující informaci, kolik pracovních vláken ještě definitivně neskončilo
        CountDownLatch shutdownLatch = new CountDownLatch(threadNumber);

        // executor pro správu vláken, má threadNumber výkoných vláken a jedno ukončovací.
        ExecutorService executorService = Executors.newFixedThreadPool(threadNumber + 1);

        // nastarovat zastavovač, který čeká, až skončí pracovní vlákna
        executorService.submit(new ExecutorServiceTerminator(executorService, shutdownLatch));

        // nastartovat pracovní vlákna
        for (int i = 0; i < threadNumber; i++) {
            TraverserProcessor<?> processor = processorFactory.createTraverser(visitor, revisionInterval, idsToProcess);
            processor.setSearchOrder(
                    new ParallelSearchOrder(processor.getSearchOrder(), controlLock.readLock(), workingCounter));

            TraverserProcessorLauncher launcher = new TraverserProcessorLauncher(databaseHolder, processor, accessLevel,
                    i, workingCounter, controlLock, shutdownLatch);

            executorService.submit(launcher);
        }

        // čekáme až vše doběhne
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.warn("Executor Service interrupted.", e);
            List<Runnable> threads = executorService.shutdownNow();
            for (Runnable thread : threads) {
                LOGGER.warn("Processor has not been started: " + thread.toString());
            }
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Launcher pro spouštění procesorů.
     * Činnost spočívá ve spuštění procesoru v transakci s danou úrovní přístupu a po dokončení své práce
     * se launcher sám přidá do fronty čekajících vláken.
     * @author tfechtner
     *
     */
    private static class TraverserProcessorLauncher implements Runnable {
        /** Číslo launcheru pro identifikaci v logách. */
        private final int number;
        /** Držák na databázi pro spouštění transakcí.*/
        private final DatabaseHolder databaseHolder;
        /** Procesor, který se vykonává v daném vlákně. */
        private final TraverserProcessor<?> traverserProcessor;
        /** Požadovaný access level k databázi. */
        private final AccessLevel accessLevel;
        /** Čítač počtu aktuálně pracujících vláken. */
        private final AtomicInteger workingCounter;
        /** Řídící zámek pro přístup k čítači vláken. */
        private final ReentrantReadWriteLock controlLock;
        /** Závora, udržující informaci kolik vláken ještě definitivně neskončilo. */
        private final CountDownLatch shutdownLatch;

        /**
         * @param databaseHolder Držák na databázi pro spouštění transakcí
         * @param processor Procesor, který se vykonává v daném vlákně
         * @param accessLevel Požadovaný access level k databázi
         * @param number Číslo launcheru pro identifikaci v logách.
         * @param workingCounter Čítač počtu aktuálně pracujících vláken
         * @param controlLock Řídící zámek pro přístup k čítači vláken
         * @param shutdownLatch Závora, udržující informaci kolik vláken ještě definitivně neskončilo
         */
        TraverserProcessorLauncher(DatabaseHolder databaseHolder, TraverserProcessor<?> processor,
                AccessLevel accessLevel, int number, AtomicInteger workingCounter, ReentrantReadWriteLock controlLock,
                CountDownLatch shutdownLatch) {
            super();
            this.databaseHolder = databaseHolder;
            this.accessLevel = accessLevel;
            this.traverserProcessor = processor;
            this.number = number;
            this.workingCounter = workingCounter;
            this.controlLock = controlLock;
            this.shutdownLatch = shutdownLatch;
        }

        @Override
        public void run() {
            try {
                while (true) {
                    try {
                        accessLevel.runInTransaction(databaseHolder, traverserProcessor);
                    } catch (RepositoryRollbackException e) {
                        LOGGER.info("Rollback in launcher " + this.toString() + ".");
                        if (traverserProcessor instanceof RollbackAware) {
                            accessLevel.runRollback(databaseHolder, (RollbackAware<?>) traverserProcessor);
                        }
                    } catch (Throwable e) {
                        LOGGER.error("Exception during processing in launcher " + this.toString() + ".", e);
                    }
                    if (traverserProcessor instanceof TraverserPostProcessing) {
                        ((TraverserPostProcessing) traverserProcessor).postProcess();
                    }

                    try {
                        controlLock.writeLock().lock();
                        int currentWorkingThread;

                        if (((ParallelSearchOrder) traverserProcessor.getSearchOrder()).getLastValue() == null) {
                            // procesor skončil, protože se nedočkal další hodnoty 
                            // = již nezačal znovu pracovat -> nesnižovat counter
                            currentWorkingThread = workingCounter.get();
                        } else {
                            // procesor skončil, protože zpracoval již maximum pro jednu transakci -> snížit counter
                            currentWorkingThread = workingCounter.decrementAndGet();
                        }

                        if (currentWorkingThread < 1) {
                            // nikdo nepracuje = všichni ostatní čekají nebo už definitivně skončili -> končíme taky
                            break;
                        }
                        workingCounter.incrementAndGet();
                    } finally {
                        controlLock.writeLock().unlock();
                    }
                }
            } catch (Throwable t) {
                LOGGER.error("Error during processing", t);
            } finally {
                shutdownLatch.countDown();
            }
        }

        @Override
        public String toString() {
            return "tpl(" + number + " / " + Thread.currentThread().getName() + ")";
        }
    }

    /**
    * Proxy pro obalení přístupu k frontě.
    * Slouží pro zajištění zamykání čekní ve frontě.
    * @author tfechtner
    *
    */
    private static class ParallelSearchOrder implements SearchOrder {
        /** Vnitřní přístup ke frontě, na který se deleguje. */
        private final SearchOrder innerSearchOrder;
        /** Zámek pro synchronizaci čekání na frontě vrcholů a spouštění nových vláken. */
        private final Lock controlLock;
        /** Počítadlo aktuálně pracujících procesorů. */
        private final AtomicInteger workingCounter;
        /** Hodnota poslední navrácenéhp prvku fronty. */
        private Object lastValue;

        /**
         * @param innerSearchOrder Vnitřní přístup ke frontě, na který se deleguje
         * @param controlLock Zámek pro synchronizaci čekání na frontě vrcholů a spouštění nových vláken
         * @param waitingCounter Počítadlo aktuálně pracujících procesorů
         */
        ParallelSearchOrder(SearchOrder innerSearchOrder, Lock controlLock, AtomicInteger waitingCounter) {
            super();
            this.innerSearchOrder = innerSearchOrder;
            this.controlLock = controlLock;
            this.workingCounter = waitingCounter;
        }

        @SuppressWarnings("unchecked")
        @Override
        public <T> T getNext(Deque<T> deque) {
            try {
                controlLock.lock();
                workingCounter.decrementAndGet();
                lastValue = innerSearchOrder.getNext(deque);
                if (lastValue != null) {
                    workingCounter.incrementAndGet();
                }
                return (T) lastValue;
            } finally {
                controlLock.unlock();
            }
        }

        /**
         * @return Hodnota poslední navrácenéhp prvku fronty
         */
        public Object getLastValue() {
            return lastValue;
        }

    }
}
