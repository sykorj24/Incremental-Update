package eu.profinit.manta.dataflow.repository.connector.titan.filter.group;

import java.util.Set;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterGroup;

/**
 * DTO pro reprezentaci skupiny filtrů.
 * @author tfechtner
 *
 */
public class HorizontalFilterGroupImpl implements HorizontalFilterGroup {
    /** Id skupiny, */
    private String id;
    /** Název skupiny */
    private String name;
    /** Pořadí skupiny. */
    private int order;
    /** Množina id obsažených filtrů. */
    private Set<String> filterIds;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getOrder() {
        return order;
    }

    @Override
    public Set<String> getFilterIds() {
        return filterIds;
    }

    /**
     * @param id Id skupiny
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @param name Název skupiny 
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param order Pořadí skupiny
     */
    public void setOrder(int order) {
        this.order = order;
    }

    /**
     * @param filterIds Množina id obsažených filtrů
     */
    public void setFilterIds(Set<String> filterIds) {
        this.filterIds = filterIds;
    }

}
