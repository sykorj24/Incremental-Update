package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import java.util.Set;

/**
 * Evaluátor kontrolující podmínku, že uzel je sloupec v tabulce 
 * a navíc má tato tabulka source (atribut OBJECT_SOURCE_TYPE) nacházející se v dané množině přípustných hodnot.
 * V případě, že má tabulka více typů, tak stačí aby alespoň jeden se rovnal některé chtěné hodnotě.
 * @author tfechtner
 *
 */
public class TableSourcePredicate extends TableAttributePredicate {
    /** Název atributu obsahující typ tabulky.  */
    private static final String TABLE_SOURCE_ATTRIBUTE_KEY = "OBJECT_SOURCE_TYPE";

    /**
     * Default konstruktor, pro správné fungování je ještě třeba nastvit field {@link #typeSet}.
     */
    public TableSourcePredicate() {
        super();
        setAttributeKey(TABLE_SOURCE_ATTRIBUTE_KEY);
    }

    /**
     * @param typeSet množina typů splňující podmínku
     */
    public TableSourcePredicate(Set<String> typeSet) {
        super(typeSet, TABLE_SOURCE_ATTRIBUTE_KEY);
    }
}
