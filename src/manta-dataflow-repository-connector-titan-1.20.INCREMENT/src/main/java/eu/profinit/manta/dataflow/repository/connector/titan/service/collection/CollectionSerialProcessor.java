package eu.profinit.manta.dataflow.repository.connector.titan.service.collection;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

/**
 * Procesor zpracovávající sériově kolekci v rámci transakcí v databázi.
 * @author tfechtner
 *
 */
public class CollectionSerialProcessor implements CollectionProcessor {
    /** Název modulu pro přístup k databázi. */
    private static final String MODULE_NAME = "mr_basic";
    /** výchozí počet zpracovaných elementů za transakci. */
    private static final int DEFAULT_ELEMENTS_PER_TRANSACTION = 500;

    /** Počet zpracovaných elementů za transakci. */
    private int elementPerTransaction = DEFAULT_ELEMENTS_PER_TRANSACTION;

    @Override
    public <E, R> void processCollection(DatabaseHolder databaseHolder, TransactionLevel transactionLevel,
            Collection<E> collection, CollectionProcessorCallback<E, R> callback, R resultHolder) {

        Queue<E> queue = new LinkedList<>(collection);
        CallbackLauncher<E, R> launcher = new CallbackLauncher<>(callback, resultHolder, queue, elementPerTransaction);

        int processed;
        do {
            processed = databaseHolder.runInTransaction(transactionLevel, launcher);
        } while (processed > 0);
    }

    /**
     * @return počet zpracovaných elementů za transakci
     */
    public int getElementPerTransaction() {
        return elementPerTransaction;
    }

    /**
     * @param elementPerTransaction počet zpracovaných elementů za transakci
     */
    public void setElementPerTransaction(int elementPerTransaction) {
        this.elementPerTransaction = elementPerTransaction;
    }

    /**
     * Třída volající callback na každý element v kolekci, dokud nedosáhne limitu na transakci.
     * @author tfechtner
     * 
     * <E> typ elementu
     * <R> typ třídy držící výstup
     */
    private static class CallbackLauncher<E, R> implements TransactionCallback<Integer> {
        /** Callback, který se volá na každý element. */
        private final CollectionProcessorCallback<E, R> callback;
        /** Výsledek zpracování. */
        private final R resultHolder;
        /** Fronta elementů ke zpracování. */
        private final Queue<E> queue;
        /** Počet zpracovaných elementů za transakci. */
        private final int elementPerTransaction;

        /**
         * @param queue Fronta elementů ke zpracování
         * @param callback callback, který se volá na každý element
         * @param resultHolder výsledek zpracování
         * @param elementPerTransaction Počet zpracovaných elementů za transakci
         */
        CallbackLauncher(CollectionProcessorCallback<E, R> callback, R resultHolder, Queue<E> queue,
                int elementPerTransaction) {
            super();
            this.callback = callback;
            this.resultHolder = resultHolder;
            this.queue = queue;
            this.elementPerTransaction = elementPerTransaction;
        }

        @Override
        public Integer callMe(TitanTransaction transaction) {
            int processedElements = 0;
            E element;
            while ((element = queue.poll()) != null) {
                callback.processElement(transaction, element, resultHolder);
                if (processedElements++ >= elementPerTransaction) {
                    break;
                }
            }

            return processedElements;
        }

        @Override
        public String getModuleName() {
            return MODULE_NAME;
        }
    }
}
