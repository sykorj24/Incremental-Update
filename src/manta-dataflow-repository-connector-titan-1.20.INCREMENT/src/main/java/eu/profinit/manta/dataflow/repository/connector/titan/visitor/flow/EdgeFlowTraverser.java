package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow;

import com.tinkerpop.blueprints.Edge;

/**
 * Traverser, ktery kontroluje navstivene hrany, a ne navstivene uzly.
 * 
 * @author Pavel Jaromersky.
 */
public class EdgeFlowTraverser extends FlowTraverserDfs {

    @Override
    protected boolean shouldProcess(FlowItem item) {
        Edge inEdge = item.getIncomingEdge();
        if (inEdge == null) {
            // Startovni uzly nemaji definovanou hranu.
            return true;
        } else {
            return getVisitedIds().add(inEdge.getId());
        }
    }
}
