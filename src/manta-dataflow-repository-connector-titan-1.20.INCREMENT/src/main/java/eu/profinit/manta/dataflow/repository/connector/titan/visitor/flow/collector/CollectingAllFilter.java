package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.collector;

import com.tinkerpop.blueprints.Vertex;

/**
 * Filter schvalující všechny uzly.
 * @author tfechtner
 *
 */
public final class CollectingAllFilter implements CollectingFilter {

    /**  Singleton isntance třídy. */
    public static final CollectingAllFilter INSTANCE = new CollectingAllFilter();

    /** Zakázání konstrukce, kvůli singleton povaze třídy.*/
    private CollectingAllFilter() {
    }

    @Override
    public boolean shouldCollect(Vertex node) {
        return true;
    }
}
