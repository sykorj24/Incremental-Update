package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.predicate.TrueEdgePredicate;
import eu.profinit.manta.dataflow.repository.connector.titan.predicate.TruePredicate;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrder;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.EdgePredicate;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;

/**
 * Obecný konfigurovatelný průchod grafem.
 *
 * @author Erik Kratochvíl
 */
public final class UniversalTraverser {

    // ------- Privátní vnitřní třídy -------------------------------------------------------------

    /**
     * DTO třída pro zapouzdření instrukcí k průchodu grafem.
     *
     * @author Erik Kratochvíl
     */
    private static class Instruction {

        /**
         * Typ instrukce, co se má s vrcholem provést.
         *
         * Kvůli potřebě umět navštěvovat vrcholy grafu způsobem POST-ORDER
         * je nutné rozlišit, kdy má být vrchol grafu pouze zpracován (PROCESS)
         * a kdy má být navštíven (VISIT).
         *
         * Pro návštěvy typu PRE-ORDER lze instrukci VISIT ignorovat,
         * protože vrchol je navštíven hned během jeho zpracování (PROCESS).
         *
         * @author Erik Kratochvíl
         */
        private enum Type {
            PROCESS, VISIT;
        }

        /** Vrchol ke zpracování. */
        private final Vertex vertex;

        /** Vzdálenost vrcholu od výchozího vrcholu. */
        private final long distance;

        /** Typ instrukce, co se má s vrcholem provést. */
        private final Type type;

        /**
         * Bude vrchol navštíven poprvé?
         * Jde o skutečné navštívení, nikoli zpracování algoritmem průchodu.
         * Tento příznak má význam pouze tehdy, je-li {@link #type} instrukce VISIT.
         */
        private final boolean visitForTheFirstTime;

        Instruction(final Type type, final Vertex vertex, final long distance, final boolean visitForTheFirstTime) {
            Validate.notNull(vertex,
                    "Vertex cannot be NULL, the instruction would make no sense. Hint: supply a valid vertex.");
            Validate.isTrue(distance >= 0, "Distance from the source cannot be negative, it does not make any sense. "
                    + "Hint: set a non-negative number.");
            Validate.isTrue(type == Type.VISIT || !visitForTheFirstTime,
                    "VisitForTheFirstTime must be set to false if the instruction is PROCESS. "
                            + "Hint: use the other constructor instead.");
            this.vertex = vertex;
            this.distance = distance;
            this.type = type;
            this.visitForTheFirstTime = visitForTheFirstTime;
        }

        Instruction(final Type type, final Vertex vertex, final long distance) {
            this(type, vertex, distance, false);
        }

        @Override
        public String toString() {
            return type + "(" + vertex.getProperty(NodeProperty.NODE_NAME.t()) + ")";
        }
    }

    // ------- Privátní atributy ------------------------------------------------------------------

    /** Logger pro logování logovatelných věcí, které je logicky potřeba logovat do logu. */
    private static final Logger LOGGER = LoggerFactory.getLogger(UniversalTraverser.class);

    /**
     * Výchozí vrcholy, ze kterých se začne s průchodem.
     * Tyto vrcholy jsou také reportovány.
     */
    private Iterable<Vertex> sources;

    /**
     * Visitor navštěvující jednotlivé elementy grafu (vrcholy, hrany, atributy, ...).
     * Nesmí být null. Pokud visitor není potřeba, je žádoucí jej explicitně nastavit např. na {@link DoNothing}.
     * Ukazuje se, že takové případy mohou dávat smysl, třeba proto,
     * že všechna práce je odvedena v predikátu {@link #shouldVertexBeProcessed}.
     * Implicitní hodnota je null a uživatel je nucen explicitně visitor nastavit.
     */
    private UniversalVisitor visitor;

    /**
     * Interval revizí, přes které se prochází.
     * Nesmí být null.
     */
    private RevisionInterval revisionInterval;

    /**
     * Styl průchodu grafem - do hloubky nebo do šířky.
     * Implicitní styl průchodu je do hloubky (DFS).
     */
    private SearchOrder searchOrder = new SearchOrderDfs();

    /**
     * Predikát, který říká, kterými vrcholy je či není žádoucí procházet.
     * Očekává se, že predikát vrací true, jestliže vrchol má být dále zpracován,
     * v opačném případě vrací false a vrchol je ze zpracování vynechán.
     * Implicitní filtrovací predikát je {@link TruePredicate}, tj. prochází se všechny vrcholy.
     * Nesmí být null.
     */
    private VertexPredicate shouldVertexBeProcessed = TruePredicate.getInstance();

    /**
     * <p>
     * Predikát, který říká, kterými hranami je či není žádoucí procházet.
     * Očekává se, že predikát vrací true, jestliže hrana má být projita
     * a cílový vrchol (cílový z pohledu průchodu hranou) má být zařazen do zpracování.
     * V opačném případě vrací false, cílový vrchol (cílový z pohledu průchodu hranou) je ze zpracování vynechán.
     * Implicitní filtrovací predikát je , tj. prochází se všemi hranami.
     * Nesmí být null.
     * </p><p>
     * Predikát je doplňkem k {@link #shouldVertexBeProcessed}.
     * {@link #shouldEdgeBeTraversed} je mocnější predikát než {@link #shouldVertexBeProcessed},
     * protože má k dispozici úplnější kontext, ale jeho použití je typicky výpočetně dražší,
     * protože práce s kontextem, tj. vstupním a výstupním vrcholem, je komplikovanější;
     * ve většině případů lze vystačit pouze s predikátem {@link #shouldVertexBeProcessed}.
     * Predikát {@link #shouldEdgeBeTraversed} je použit pouze při hledání následníků,
     * tj. není aplikován na výchozí vrcholy.
     * </p>
     */
    private EdgePredicate shouldEdgeBeTraversed = TrueEdgePredicate.getInstance();

    /**
     * Pořadí částí vrcholu, ve kterém jsou předhozeny visitoru.
     * Implicitní pořadí je [SELF, ATTRIBUTES, EDGES, SUCCESSORS].
     * Pokud nejsou některé části nastaveny, nenavštěvují se vůbec.
     * Například při pořadí [SELF, SUCCESSORS] nejsou reportovány ani hrany ani atributy vrcholu.
     * Pomocí vhodného pořadí jednotlivých částí lze docílit tzv. POST-ORDER návštěv vrcholů stromu.
     * Chování je následující:
     * <ol>
     * <li>cokoli je před SUCCESSORS, navštěvuje se ihned</li>
     * <li>cokoli je po SUCCESSORS, navštěvuje se až po zpracování všech potomků</li>
     * </ol>
     *
     * Například pro pořadí [EDGES, SUCCESSORS, SELF] jsou navštíveny všechny hrany (EDGES) vrcholu A,
     * pak jsou stejným způsobem zpracováni všichni následovníci (SUCCESSORS) vrcholu A,
     * a nakonec je navštíven samotný (SELF) vrchol A.
     * <br/>
     *
     * <b>Omezení</b>
     * <ol>
     * <li>pro typ průchodu BFS musí být SUCCESSORS vždy na posledním místě</li>
     * <li>každá "část" se smí v seznamu objevit nejvýše jednou, průchod by se choval nepredikovatelně</li>
     * </ol>
     */
    private List<VisitedPart> visitOrder = Arrays.asList(VisitedPart.SELF, VisitedPart.ATTRIBUTES, VisitedPart.EDGES, VisitedPart.SUCCESSORS);

    /**
     * Povolené typy hran (labely), přes které se smí procházet.
     * Implicitně nastaveno na HAS_PARENT a HAS_RESOURCE (v tomto pořadí).
     * Musí být nastaven alespoň jeden typ hrany.
     */
    private EdgeLabel[] edgeLabels = { EdgeLabel.HAS_PARENT, EdgeLabel.HAS_RESOURCE };

    /**
     * Typy hran, které jsou pro každý vrchol předhozeny (reportovány) visitoru.
     * Implicitně nastaveno na DIRECT a FILTER (v tomto pořadí).
     * Musí být nastaven alespoň jeden typ hrany.
     */
    private EdgeLabel[] reportedEdgeLabels = { EdgeLabel.DIRECT, EdgeLabel.FILTER };

    /**
     * Orientace (směr) hran, ve které se smí procházet.
     * Implicitně nastaveno na IN.
     * Smí být buď IN anebo OUT.
     */
    private Direction direction = Direction.IN;

    /**
     * Orientace (směr) hran, které mají být reportovány visitoru.
     * Implicitně nastaveno na BOTH.
     * Smí být buď IN nebo OUT nebo BOTH.
     */
    private Direction reportedEdgeDirection = Direction.BOTH;

    /**
     * Maximální vzdálenost od nejbližšího zdrojového vrcholu (zdrojový vrchol má sám od sebe vzdálenost 0),
     * do které se smí jít.
     * Všechny vrcholy, které jsou dále, jsou vynechány.
     * Implicitně nastaveno na Long.MAX_VALUE (cca 9 trilionů).
     */
    private long maxDistance = Long.MAX_VALUE;

    /**
     * Maximální počet vrcholů, které jsou zpracovány.
     * Počítá se každý unikátní zpracovaný vrchol, tzn.
     * že pokud je vrchol při průchodu nalezen podruhé (lze se do něj dostat dvěma cestami),
     * nezapočítává se znovu. Stejně tak se nezapočítávají instrukce VISIT
     * zvlášť, tzn. pokud je styl návštěv post-order a vrchol je nejprve zpracován
     * a později navštíven, návštěva se nezapočítává znovu.
     * <br/>
     * Je-li dosaženo maximálního počtu vrcholů, průchod je ukončen.
     * Implicitně nastaveno na Long.MAX_VALUE (cca 9 trilionů).
     */
    private long maxVertexCount = Long.MAX_VALUE;

    // ------- Veřejné gettery a settery  ---------------------------------------------------------

    /**
     * @param sources Výchozí vrcholy, ze kterých se začne s průchodem. Tyto vrcholy jsou také reportovány.
     */
    public void setSources(final Collection<Vertex> sources) {
        Validate.notNull(sources, "Sources cannot be null. Without a source, traversing the graph makes no sense.");
        Validate.isTrue(sources.iterator().hasNext(),
                "At least one source must be set. Without a source, traversing the graph makes no sense.");
        // Defenzivní kopie pro případ, kdyby s dodanou kolekcí někdo venku něco začal dělat.
        this.sources = new ArrayList<Vertex>(sources);
    }

    /**
     * @param sources Výchozí vrcholy, ze kterých se začne s průchodem. Tyto vrcholy jsou také reportovány.
     */
    public void setSources(final Vertex... sources) {
        Validate.notEmpty(sources,
                "At least one source must be set. Without a source, traversing the graph makes no sense.");
        this.sources = Arrays.asList(sources);
    }

    /**
     * @param visitor Visitor navštěvující jednotlivé elementy grafu. Nesmí být null. 
     */
    public void setVisitor(final UniversalVisitor visitor) {
        Validate.notNull(visitor, "Visitor cannot be null. Without a visitor, traversing the graph makes no sense.");
        this.visitor = visitor;
    }

    /**
     * @param revisionInterval Interval revizí, přes které se prochází. Nesmí být null.
     */
    public void setRevisionInterval(final RevisionInterval revisionInterval) {
        Validate.notNull(revisionInterval, "RevisionInterval cannot be null.");
        this.revisionInterval = revisionInterval;
    }

    /**
     * @param searchOrder Styl průchodu grafem
     */
    public void setSearchOrder(final SearchOrder searchOrder) {
        this.searchOrder = searchOrder;
    }

    /**
     * @param shouldVertexBeProcessed Predikát, který říká, kterými vrcholy je či není žádoucí procházet.
     */
    public void setShouldVertexBeProcessed(final VertexPredicate shouldVertexBeProcessed) {
        Validate.notNull(shouldVertexBeProcessed,
                "Vertex predicate cannot be null. Hint: use TruePredicate if all vertices should be processed.");
        this.shouldVertexBeProcessed = shouldVertexBeProcessed;
    }

    /**
     * @param shouldEdgeBeTraversed Predikát, který říká, kterými hranami je či není žádoucí procházet. 
     */
    public void setShouldEdgeBeTraversed(final EdgePredicate shouldEdgeBeTraversed) {
        Validate.notNull(shouldEdgeBeTraversed,
                "Edge predicate cannot be null. Hint: use TrueEdgePredicate if all edges should be traversed.");
        this.shouldEdgeBeTraversed = shouldEdgeBeTraversed;
    }

    /**
     * @param visitOrder Pořadí částí vrcholu, ve kterém jsou předhozeny visitoru. 
     */
    public void setVisitOrder(final VisitedPart... visitOrder) {
        Validate.isTrue(!ArrayUtils.isEmpty(visitOrder),
                "Visit order cannot be empty. With no parts to visit, traversing the graph makes no sense. "
                        + "Hint: define at least one part.");
        Validate.isTrue(hasUniqueElements(visitOrder),
                "Visit order cannot contain duplicate values. The traversal would be too complicated to comprehend. "
                        + "Hint: remove excessive duplicates from the array.");
        this.visitOrder = Arrays.asList(visitOrder);
    }

    /**
     * @param edgeLabels Povolené typy hran (labely), přes které se smí procházet. 
     */
    public void setEdgeLabels(final EdgeLabel... edgeLabels) {
        Validate.notEmpty(edgeLabels,
                "Edge labels cannot be empty. With no labels, traversing the graph makes no sense. "
                        + "Hint: define at least one edge label.");
        this.edgeLabels = edgeLabels;
    }

    /**
     * @param reportedEdgeLabels Typy hran, které jsou pro každý vrchol předhozeny (reportovány) visitoru.
     */
    public void setReportedEdgeLabels(final EdgeLabel... reportedEdgeLabels) {
        Validate.notEmpty(reportedEdgeLabels,
                "Reported edge labels cannot be empty. With no labels, reporting edges makes no sense. "
                        + "Hint: define at least one edge label.");
        this.reportedEdgeLabels = reportedEdgeLabels;
    }

    /**
     * @param direction Orientace (směr) hran, ve které se smí procházet. 
     */
    public void setDirection(final Direction direction) {
        Validate.isTrue(direction == Direction.IN || direction == Direction.OUT, "Unsupported direction of traversal: "
                + direction + ". Hint: use either Direction.IN or Direction.OUT.");
        this.direction = direction;
    }

    /**
     * @param reportedEdgeDirection Orientace (směr) hran, které mají být reportovány visitoru. 
     */
    public void setReportedEdgeDirection(final Direction reportedEdgeDirection) {
        this.reportedEdgeDirection = reportedEdgeDirection;
    }

    /**
     * @param maxDistance Maximální vzdálenost od nejbližšího zdrojového vrcholu, do které se smí jít.
     */
    public void setMaxDistance(final long maxDistance) {
        Validate.isTrue(maxDistance > 0,
                "Maximum distance must be greater than 0. Traversing makes no sense otherwise. "
                        + "Hint: use values 1 and greater.");
        this.maxDistance = maxDistance;
    }

    /**
     * @param maxVertexCount Maximální počet vrcholů, které jsou zpracovány.
     */
    public void setMaxVertexCount(final long maxVertexCount) {
        Validate.isTrue(maxVertexCount > 0,
                "Maximum vertex count must be greater than 0. Traversing makes no sense otherwise. "
                        + "Hint: use values 1 and greater.");
        this.maxVertexCount = maxVertexCount;
    }

    // ------- Veřejné API ------------------------------------------------------------------------

    /**
     * Projde grafem dle zadaných parametrů.
     * Explicitně přenastaví výchozí vrcholy dle požadavků.
     *
     * @param sourcesToGo výchozí vrcholy
     */
    public void go(final Vertex... sourcesToGo) {
        setSources(sourcesToGo);
        go();
    }

    /**
     * Projde grafem dle zadaných parametrů.
     */
    public void go() {
        // Zopakování validace vstupů, nastaven musí být alespoň visitor a zdrojové vrcholy.
        // Všechno ostatní má implicitní hodnoty.
        Validate.notNull(visitor, "Visitor cannot be null. Without a visitor, traversing the graph makes no sense. "
                + "Hint: make sure you did not forget to set the visitor.");
        Validate.notNull(sources, "Sources must be set. Without sources, traversing the graph makes no sense. "
                + "Hint: make sure you did not forget to set the sources.");
        Validate.isTrue(sources.iterator().hasNext(),
                "At least one source must be set. Without a source, traversing the graph makes no sense. "
                        + "Hint: make sure you did not forget to set the sources.");

        // Množina ID zpracovaných vrcholů.
        // Zabraňuje zacyklení při průchodu grafem.
        // Pokud procházíme pouze HAS_RESOURCE a HAS_PARENT hrany, nemělo by to nastat, ale nikdy nevíme.
        // Má význam zejména v případě, že je UniversalTraverser použit pro průchod přes DIRECT a FILTER hrany.
        final Set<Object> processedVertexIds = new HashSet<Object>();

        // Počet navštívených vrcholů.
        long vertexCount = 0;

        // Příznak, že následníci (z pohledu stylu průchodu) vrcholu
        // mají být visitoru předhozeny dřív než vlastní vrchol nebo jeho hrany či atributy.
        // Jinými slovy: pokud následníci vrcholu nejsou na posledním místě v pořadí návštěv,
        // musíme ve frontě rozlišovat mezi zpracováním (PROCESS) a návštěvou (VISIT) vrcholu.
        // V takovém případě jde o formu (částečného) post-orderu.
        final boolean postOrder = !isLastInList(visitOrder, VisitedPart.SUCCESSORS);

        // Post-order návštěvy dávají smysl pouze pro průchod typu DFS.
        // Průchod grafem stylem BFS spolu s návštěvami POST-ORDER nejsou implementovány,
        // protože by to bylo příliš komplikované a nikde v kódu Manta toto chování není potřeba.
        Validate.isTrue(searchOrder instanceof SearchOrderDfs || !postOrder,
                "POST-ORDER visits are not supported for BFS search. " + "Hint: either use DFS search instead of BFS, "
                        + "or make SUCCESSORS the last entry in the visit order.");

        // Fronta pro instrukce k průchodu.
        // Dle stylu přidávání a odebírání instrukcí se realizuje DFS/BFS průchod grafem.
        final Deque<Instruction> queue = new LinkedList<Instruction>();

        // Inicializace nultého kroku = instrukce pro zpracování výchozích vrcholů.
        for (final Vertex source : sources) {
            final Instruction instruction = new Instruction(Instruction.Type.PROCESS, source, 0);
            queue.addLast(instruction);
        }

        // Vlastní průchod grafem a navštěvování vrcholů ve zvoleném pořadí.
        while (vertexCount < maxVertexCount && !queue.isEmpty()) {
            // Vyber další instrukci z fronty.
            // Způsob odebírání z fronty definovaný v searchOrder určuje, zda jde o BFS nebo DFS.
            final Instruction instruction = searchOrder.getNext(queue);
            // Shortcut pro psaní kódu: vertex místo instruction.vertex.
            final Vertex vertex = instruction.vertex;

            // Máme vrchol vůbec zpracovávat?
            if (!shouldVertexBeProcessed.evaluate(vertex, revisionInterval)) {
                continue;
            }

            // Zpracováváme vrchol poprvé?
            // True, pokud ano, false, pokud ne, tj. pokud jsme při průchodu narazili na cyklus.
            final boolean processedForTheFirstTime = processedVertexIds.add(vertex.getId());

            // Aktualizuj počet vrcholů, které dosud jsme zpracovali,
            // pokud máme instrukci vrchol zpracovat a zpracováváme ho poprvé v životě.
            if (instruction.type == Instruction.Type.PROCESS && processedForTheFirstTime) {
                ++vertexCount;
            }

            // Přepínač, který určuje, zda má být visitor volán či blokován
            // (protože bude volán později nebo proto, že už volán byl).
            // Záleží totiž na instrukci, kterou dostaneme:
            // a) pokud je instrukce PROCESS, pak je visitor volán jen do okamžiku, než jsou zpracováni následníci;
            //    všechno ostatní po následnících bude navštíveno, až s instrukcí VISIT
            // b) pokud je instrukce VISIT, pak je visitor volán až od okamžiku, co jsou projiti následníci;
            //    všechno předtím už navštíveno bylo v instrukci PROCESS
            boolean isTimeToVisit = (instruction.type == Instruction.Type.PROCESS);

            boolean shouldContinueFromVisitor = true;
            // Volání visitorů na jednotlivé "části" vrcholu.
            // Volání visitorů se řídí dle přepínače #isTimeToVisit.
            for (final VisitedPart part : visitOrder) {
                switch (part) {

                case SUCCESSORS:
                    if (!shouldContinueFromVisitor) {
                        break;
                    }

                    if (isTimeToVisit && instruction.distance < maxDistance) {
                        // Pokud procházíme stylem post-order, zapamatuj, si, že je potřeba vrchol znovu navštívit,
                        // jakmile budou zpracováni všichni potomci. Tzn. vlož instrukci typu VISIT pro daný vrchol.
                        // V žádných jiných případech tato instrukce potřeba není, protože všechny části vrcholu budou
                        // navštíveny rovnou při zpracování.
                        if (postOrder) {
                            // Je potřeba si zapamatovat, zda vrchol zpracováváme poprvé, protože u instrukce VISIT
                            // už bude ID vrcholu v seznamu zpracovaných, takže se nemůžeme spolehlivě dotázat.
                            final Instruction self = new Instruction(Instruction.Type.VISIT, vertex,
                                    instruction.distance, processedForTheFirstTime);
                            queue.addLast(self);
                        }

                        // Následníci vrcholu jsou zpracováni pouze při instrukci PROCESS
                        // a současně pouze tehdy, nebyli-li již zpracování dříve,
                        // a není-li překročena maximální vzdálenost od výchozího vrcholu.
                        if (processedForTheFirstTime) {
                            // Sběr následníků a založení příkazů pro jejich zpracování.
                            // Přechod do následníků probíhá přes hrany, protože je potřeba aplikovat dodatečný filtr,
                            // který rozhodne, zda z aktuálního vrcholu je žádoucí do následníka projít po dané hraně.
                            final Collection<Edge> edges = GraphOperation.getAdjacentEdges(instruction.vertex,
                                    direction, revisionInterval, edgeLabels);
                            // Poznámka: pro DFS průchod níže uvedený způsob přidávání zapříčiní to, že
                            // následníci budou při průchodu probíráni v opačném pořadí,
                            // než v jakém jsou vráceny z GraphOperation.getAdjacentVertices/Edges(),
                            // ale vzhledem k tomu, že uspořádání následníků není požadováno řešit, je to jedno.
                            for (final Edge edge : edges) {
                                if (shouldEdgeBeTraversed.evaluate(edge, direction)) {
                                    final Vertex successor = edge.getVertex(direction.opposite());
                                    final Instruction nextInstruction = new Instruction(Instruction.Type.PROCESS,
                                            successor, instruction.distance + 1);
                                    queue.addLast(nextInstruction);
                                }
                            }
                        }
                    }
                    // Zpracování následníků (SUCCESSORS) přepíná příznak #isTimeToVisit,
                    // tzn. pokud na začátku volání visitorů povoleno nebylo, odteď už bude a naopak.
                    isTimeToVisit = !isTimeToVisit;
                    break;

                case SELF:
                    if (isTimeToVisit) {
                        // Návštěva vrcholu.
                        shouldContinueFromVisitor &= visitor.visit(instruction.vertex, instruction.distance,
                                processedForTheFirstTime || instruction.visitForTheFirstTime, revisionInterval);
                    }
                    break;

                case ATTRIBUTES:
                    if (isTimeToVisit) {
                        // Sběr atributů vrcholu, které jsou v jiných vrcholech, a jejich postupné navštěvování.
                        final Collection<Vertex> attributes = GraphOperation.getAdjacentVertices(instruction.vertex,
                                Direction.OUT, revisionInterval, EdgeLabel.HAS_ATTRIBUTE);
                        for (final Vertex attribute : attributes) {
                            shouldContinueFromVisitor &= visitor.visit(attribute, instruction.distance + 1,
                                    processedForTheFirstTime || instruction.visitForTheFirstTime, revisionInterval);
                        }
                    }
                    break;

                case EDGES:
                    if (isTimeToVisit) {
                        // Sběr požadovaných typů hran vrcholu a jejich postupné navštěvování.
                        final Collection<Edge> edges = GraphOperation.getAdjacentEdges(instruction.vertex,
                                reportedEdgeDirection, revisionInterval, reportedEdgeLabels);
                        for (final Edge edge : edges) {
                            final EdgeLabel label = EdgeLabel.parseFromDbType(edge.getLabel());
                            final Direction relativeDirection = (vertex
                                    .equals(edge.getVertex(Direction.IN)) ? Direction.IN : Direction.OUT);
                            shouldContinueFromVisitor &= visitor.visit(edge, label, relativeDirection,
                                    revisionInterval);
                        }
                    }
                    break;

                default:
                    LOGGER.error(part + " is not covered in switch, "
                            + "please cover it in a separate case to prevent potential errors.");
                    throw new IllegalArgumentException(part + " is not supported.");
                }
            }
        }
    }

    // ------- Pomocné privátní metody ------------------------------------------------------------

    /**
     *
     * @param list seznam prvků, smí být i null
     * @param element prvek, který má být na posledním místě, nesmí být null
     * @return true, pokud element je v seznamu prvků na posledním místě; ve všech ostatních případech false
     */
    private static <E> boolean isLastInList(final List<E> list, final E element) {
        Validate.notNull(element, "Cannot search for NULL element. Hint: ask for another element.");
        final int n = (list != null ? list.size() : -1);
        return n > 0 && element.equals(list.get(n - 1));
    }

    /**
     *
     * @param array pole prvků
     * @return true, pokud pole obsahuje unikátní prvky, tj. žádný prvek se v něm nevyskytuje více než jednou;
     *          jinak false
     */
    private static <E> boolean hasUniqueElements(final E[] array) {
        Validate.notNull(array, "Array cannot be null.");
        final Set<E> elements = new HashSet<E>(Arrays.asList(array));
        return elements.size() == array.length;
    }

}
