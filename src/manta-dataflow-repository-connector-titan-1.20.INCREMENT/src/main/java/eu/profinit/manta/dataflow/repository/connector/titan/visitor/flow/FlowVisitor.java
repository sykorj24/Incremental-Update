package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterResult;

/**
 * Rozhraní pro visitor pro navštěvování uzlů v rámci procházení data flow.
 * @author tfechtner, Jan Milík, Radomír Polách
 */
public interface FlowVisitor {

    /**
     * Provede navšítevní uzlu a rozhodne, jestliže se má pokračovat dalšími toky.
     * @param node navštěvovaný uzel
     * @param flowItem item reprezentující okolnosti zpracování v rámci traverseru
     * @return OK_CONTINUE, jestliže se má pokračovat sousedy daného uzlu
     */
    FilterResult visitAndContinue(Vertex node, FlowItem flowItem);

}
