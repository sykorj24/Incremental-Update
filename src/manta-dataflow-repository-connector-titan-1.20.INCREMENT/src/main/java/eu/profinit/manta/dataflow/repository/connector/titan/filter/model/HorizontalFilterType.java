package eu.profinit.manta.dataflow.repository.connector.titan.filter.model;

/**
 * Typ filtru.
 * @author tfechtner
 *
 */
public enum HorizontalFilterType {
    /** Generovaný filtr pro resource.*/
    RESOURCE,
    /** User-custom nebo předdefinovaný filtr. */
    CUSTOM;
}
