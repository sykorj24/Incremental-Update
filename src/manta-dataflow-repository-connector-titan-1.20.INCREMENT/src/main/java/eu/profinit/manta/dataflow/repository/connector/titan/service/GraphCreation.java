package eu.profinit.manta.dataflow.repository.connector.titan.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.thinkaurelius.titan.core.TitanVertex;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

/**
 * Třída pro vytrváření obejktů v grafu.
 * @author tfechtner
 *
 */
public final class GraphCreation {
    /** Identifikace modulu, pod kterým graphcreation pracuje. */
    public static final String MODULE_NAME = "mr_basic";
    /** SLF4J logger.*/
    static final Logger LOGGER = LoggerFactory.getLogger(GraphCreation.class);

    private GraphCreation() {
    }

    /**
     * Provede incializaci databáze, založí indexy a vytvoří rooty a založí technickou revizi. <br>
     * Metodu lze volat opakovaně.
     * @param databaseService service poskytující připojení k db
     * @param superRootHandler držák na super root
     * @param sourceRootHandler držák na source code root
     * @param revisionRootHandler držák na revision root
     */
    public static void initDatabase(DatabaseHolder databaseService, final SuperRootHandler superRootHandler,
            final RevisionRootHandler revisionRootHandler, final SourceRootHandler sourceRootHandler) {
        initDatabaseInner(databaseService, superRootHandler, revisionRootHandler, sourceRootHandler, true);
    }

    /**
     * Provede incializaci databáze, založí indexy a vytvoří rooty. <br>
     * Pozor metoda nezakládá technickou revizi, je tedy třeba ji založit manuálně <br>
     * Metodu lze volat opakovaně.
     * @param databaseService service poskytující připojení k db
     * @param superRootHandler držák na super root
     * @param sourceRootHandler držák na source code root
     * @param revisionRootHandler držák na revision root
     */
    public static void initDatabaseWithoutRevision(DatabaseHolder databaseService,
            final SuperRootHandler superRootHandler, final RevisionRootHandler revisionRootHandler,
            final SourceRootHandler sourceRootHandler) {
        initDatabaseInner(databaseService, superRootHandler, revisionRootHandler, sourceRootHandler, false);
    }

    /**
     * Provede inicializaci databáze.
     * @param databaseService service poskytující připojení k db
     * @param superRootHandler držák na super root
     * @param revisionRootHandler držák na revision root
     * @param sourceRootHandler držák na source code root
     * @param createTechnicalRevision true, jestliže se má založit technická revize 
     */
    private static synchronized void initDatabaseInner(DatabaseHolder databaseService,
            final SuperRootHandler superRootHandler, final RevisionRootHandler revisionRootHandler,
            final SourceRootHandler sourceRootHandler, boolean createTechnicalRevision) {

        boolean isInit = isDbInitialized(databaseService, superRootHandler, revisionRootHandler, sourceRootHandler);
        if (!isInit) {
            LOGGER.info("Initializing metadata repository.");
            initIndices(databaseService);
            initRoots(databaseService, superRootHandler, revisionRootHandler, sourceRootHandler);
            if (createTechnicalRevision) {
                createTechnicalRevision(databaseService, revisionRootHandler);
            }
        }
    }

    /**
     * @param databaseService service poskytující připojení k db
     * @param superRootHandler držák na super root
     * @param revisionRootHandler držák na revision root
     * @param sourceRootHandler držák na source code root
     * @return true, jestliže je db inicializovaná
     */
    private static boolean isDbInitialized(DatabaseHolder databaseService, final SuperRootHandler superRootHandler,
            final RevisionRootHandler revisionRootHandler, final SourceRootHandler sourceRootHandler) {
        return databaseService.runInTransaction(TransactionLevel.READ, new TransactionCallback<Boolean>() {
            @Override
            public Boolean callMe(TitanTransaction transaction) {
                Vertex superRoot = superRootHandler.tryGetRoot(transaction);
                Vertex revisionRoot = revisionRootHandler.tryGetRoot(transaction);
                Vertex sourceRoot = sourceRootHandler.tryGetRoot(transaction);
                return superRoot != null && revisionRoot != null && sourceRoot != null;
            }

            @Override
            public String getModuleName() {
                return MODULE_NAME;
            }
        });
    }

    /**
     * Založí a commitne první revizi.
     * @param databaseService service poskytující připojení k db
     * @param revisionRootHandler držák na revision root
     * @return číslo této nové revizi
     */
    public static synchronized Double createTechnicalRevision(DatabaseHolder databaseService,
            final RevisionRootHandler revisionRootHandler) {
        return databaseService.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TransactionCallback<Double>() {
            @Override
            public Double callMe(TitanTransaction transaction) {
                Double revision = revisionRootHandler.createMajorRevision(transaction);
                revisionRootHandler.commitRevision(transaction, revision);
                return revision;
            }

            @Override
            public String getModuleName() {
                return MODULE_NAME;
            }
        });

    }

    /**
     * @param databaseService service poskytující připojení k db
     * @param superRootHandler držák na super root
     * @param revisionRootHandler držák na revision root
     * @param sourceRootHandler držák na source code root
     */
    private static void initRoots(DatabaseHolder databaseService, final SuperRootHandler superRootHandler,
            final RevisionRootHandler revisionRootHandler, final SourceRootHandler sourceRootHandler) {
        databaseService.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TransactionCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                superRootHandler.ensureRootExistance(transaction);
                revisionRootHandler.ensureRootExistance(transaction);
                sourceRootHandler.ensureRootExistance(transaction);
                return null;
            }

            @Override
            public String getModuleName() {
                return MODULE_NAME;
            }
        });
    }

    /**
     * Založí indexy.
     * @param databaseService držák pro databázi
     */
    private static void initIndices(DatabaseHolder databaseService) {
        databaseService.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE,
                new IndexCreator(databaseService.getConfiguration()));
    }

    /**
     * Vytvori vrstvu.
     * @param transaction Transakce do DB
     * @param name Nazev vrstvy
     * @param type Typ vrstvy
     * @return Nove vytvorena vrstva.
     */
    public static Vertex createLayer(TitanTransaction transaction, String name, String type) {
        TitanVertex newLayer = transaction.addVertex();
        newLayer.setProperty(NodeProperty.VERTEX_TYPE.t(), VertexType.LAYER);
        newLayer.setProperty(NodeProperty.LAYER_NAME.t(), name);
        newLayer.setProperty(NodeProperty.LAYER_TYPE.t(), type);
        return newLayer;
    }
    
    /**
     * Create resource valid in revision interval <startRevision, maxMinorRevision>.
     * 
     * @param transaction transakce do db
     * @param root root uzel, ke kterému se napojí nový resource
     * @param name název resource
     * @param type typ resource
     * @param description popis resource
     * @param startRevision  start revision number
     * @return nově vytvořený vertex
     */
    public static Vertex createResource(TitanTransaction transaction, Vertex root, String name, String type,
            String description, Vertex layer, Double startRevision) {
        Double endRevision = RevisionUtils.getMaxMinorRevisionNumber(startRevision);
        return createResource(transaction, root, name, type, description, layer, new RevisionInterval(startRevision, endRevision));
    }

    /**
     * Vytvořit resource.
     * @param transaction transakce do db
     * @param root root uzel, ke kterému se napojí nový resource
     * @param name název resource
     * @param type typ resource
     * @param description popis resource
     * @param layer Vrstva, ve ktere se ma zdroj vytvorit
     * @param revisionInterval interval platnosti nového resource
     * @return nově vytvořený vertex
     */
    public static Vertex createResource(TitanTransaction transaction, Vertex root, String name, String type,
            String description, Vertex layer, RevisionInterval revisionInterval) {
        Validate.notNull(root, "The agrument 'root' was null.");
        Validate.notNull(layer, "The agrument 'layer' was null.");
        Validate.notNull(revisionInterval, "The agrument 'revisionInterval' was null.");

        TitanVertex newNode = transaction.addVertex();
        newNode.setProperty(NodeProperty.VERTEX_TYPE.t(), VertexType.RESOURCE);
        newNode.setProperty(NodeProperty.RESOURCE_NAME.t(), name);
        newNode.setProperty(NodeProperty.RESOURCE_TYPE.t(), type);
        newNode.setProperty(NodeProperty.RESOURCE_DESCRIPTION.t(), description);

        Edge newRootEdge = newNode.addEdge(EdgeLabel.HAS_RESOURCE.t(), root);
        newRootEdge.setProperty(EdgeProperty.CHILD_NAME.t(), GraphOperation.processValueForChildName(name));
        newRootEdge.setProperty(EdgeProperty.TRAN_START.t(), revisionInterval.getStart());
        newRootEdge.setProperty(EdgeProperty.TRAN_END.t(), revisionInterval.getEnd());
        
        // Propojime zdroj s vrstvou
        Edge newLayerEdge = newNode.addEdge(EdgeLabel.IN_LAYER.t(), layer);
        newLayerEdge.setProperty(EdgeProperty.TRAN_START.t(), revisionInterval.getStart());
        newLayerEdge.setProperty(EdgeProperty.TRAN_END.t(), revisionInterval.getEnd());
        
        return newNode;
    }

    /**
     * Create new node valid in revision interval <startRevision, maxMinorRevision>.
     * 
     * @param transaction transakce do db
     * @param parent předek, ke kterému se uzel připojí, může být i resource
     * @param name název uzlu
     * @param type typ uzlu
     * @param startRevision  start revision number
     * @return nově vytvořený vertex
     */
    public static Vertex createNode(TitanTransaction transaction, Vertex parent, String name, String type,
            Double startRevision) {
        Double endRevision = RevisionUtils.getMaxMinorRevisionNumber(startRevision);
        return createNode(transaction, parent, name, type, new RevisionInterval(startRevision, endRevision));
    }

    /**
     * Vytvořit uzel.
     * @param transaction transakce do db
     * @param parent předek, ke kterému se uzel připojí, může být i resource
     * @param name název uzlu
     * @param type typ uzlu
     * @param revisionInterval interval platnosti nového resource
     * @return nově vytvořený vertex
     */
    public static Vertex createNode(TitanTransaction transaction, Vertex parent, String name, String type,
            RevisionInterval revisionInterval) {
        if (parent == null) {
            throw new IllegalArgumentException("The argument parent was null.");
        }

        if (revisionInterval == null) {
            throw new IllegalArgumentException("The argument revision was null.");
        }

        Vertex newNode = transaction.addVertex(null);
        newNode.setProperty(NodeProperty.VERTEX_TYPE.t(), VertexType.NODE);
        newNode.setProperty(NodeProperty.NODE_NAME.t(), name);
        newNode.setProperty(NodeProperty.NODE_TYPE.t(), type);

        EdgeLabel parentEdgeType = null;
        if (VertexType.getType(parent) == VertexType.RESOURCE) {
            parentEdgeType = EdgeLabel.HAS_RESOURCE;
        } else {
            parentEdgeType = EdgeLabel.HAS_PARENT;
        }
        Edge newEdge = newNode.addEdge(parentEdgeType.t(), parent);
        newEdge.setProperty(EdgeProperty.CHILD_NAME.t(), GraphOperation.processValueForChildName(name));
        newEdge.setProperty(EdgeProperty.TRAN_START.t(), revisionInterval.getStart());
        newEdge.setProperty(EdgeProperty.TRAN_END.t(), revisionInterval.getEnd());

        return newNode;
    }

    /**
     * Create node valid in revision interval <startRevision, maxMinorRevision>.
     * 
     * @param transaction transakce do db
     * @param parent předek uzlu, musí být uzel
     * @param resource resource uzlu, musí být resource
     * @param name název uzlu
     * @param type typ uzlu
     * @param startRevision  start revision number
     * @return nově vytvořený vertex
     */
    public static Vertex createNode(TitanTransaction transaction, Vertex parent, Vertex resource, String name,
            String type, Double startRevision) {
        Double endRevision = RevisionUtils.getMaxMinorRevisionNumber(startRevision);
        return createNode(transaction, parent, resource, name, type, new RevisionInterval(startRevision, endRevision));
    }

    /**
     * Vytvořit uzel.
     * @param transaction transakce do db
     * @param parent předek uzlu, musí být uzel
     * @param resource resource uzlu, musí být resource
     * @param name název uzlu
     * @param type typ uzlu
     * @param revision interval platnosti nového uzlu
     * @return nově vytvořený vertex
     */
    public static Vertex createNode(TitanTransaction transaction, Vertex parent, Vertex resource, String name,
            String type, RevisionInterval revision) {
        if (parent == null || VertexType.getType(parent) != VertexType.NODE) {
            throw new IllegalArgumentException("The parent vertex is not node.");
        }

        if (resource == null || VertexType.getType(resource) != VertexType.RESOURCE) {
            throw new IllegalArgumentException("The resource vertex is not resource.");
        }

        if (revision == null) {
            throw new IllegalArgumentException("The argument revision was null.");
        }

        Vertex newNode = transaction.addVertex(null);
        newNode.setProperty(NodeProperty.VERTEX_TYPE.t(), VertexType.NODE);
        newNode.setProperty(NodeProperty.NODE_NAME.t(), name);
        newNode.setProperty(NodeProperty.NODE_TYPE.t(), type);

        Edge newEdgeParent = newNode.addEdge(EdgeLabel.HAS_PARENT.t(), parent);
        newEdgeParent.setProperty(EdgeProperty.CHILD_NAME.t(), GraphOperation.processValueForChildName(name));
        newEdgeParent.setProperty(EdgeProperty.TRAN_START.t(), revision.getStart());
        newEdgeParent.setProperty(EdgeProperty.TRAN_END.t(), revision.getEnd());
        Edge newEdgeResource = newNode.addEdge(EdgeLabel.HAS_RESOURCE.t(), resource);
        newEdgeResource.setProperty(EdgeProperty.CHILD_NAME.t(), GraphOperation.processValueForChildName(name));
        newEdgeResource.setProperty(EdgeProperty.TRAN_START.t(), revision.getStart());
        newEdgeResource.setProperty(EdgeProperty.TRAN_END.t(), revision.getEnd());

        return newNode;
    }

    /**
     * Create edge valid in revision interval <startRevision, maxMinorRevision>.
     * 
     * @param nodeSource zdrojový uzel hrany
     * @param nodeTarget cílový uzel hrany
     * @param type typ hrany
     * @param startRevision  start revision number
     * @return nově vytvořená hrana
     */
    public static Edge createEdge(Vertex nodeSource, Vertex nodeTarget, EdgeLabel type, Double startRevision) {
        return createEdge(nodeSource, nodeTarget, type, false, startRevision);
    }

    /**
     * Create edge valid in revision interval <startRevision, maxMinorRevision>.
     * 
     * @param nodeSource zdrojový uzel hrany
     * @param nodeTarget cílový uzel hrany
     * @param type typ hrany
     * @param interpolated příznak, zda je hrana interpolována
     * @param startRevision  start revision number
     * @return nově vytvořená hrana
     */
    public static Edge createEdge(Vertex nodeSource, Vertex nodeTarget, EdgeLabel type, boolean interpolated, Double startRevision) {
        if (startRevision == null) {
            throw new IllegalArgumentException("The revision was null.");
        }
        Double endRevision = RevisionUtils.getMaxMinorRevisionNumber(startRevision);
        return createEdge(nodeSource, nodeTarget, type, interpolated, new RevisionInterval(startRevision, endRevision));
    }

    /**
     * Vytvořit hranu.
     * @param sourceVertex zdrojový uzel hrany
     * @param targetVertex cílový uzel hrany
     * @param label typ hrany
     * @param revisionInterval interval platnosti nové hrany
     * @return nově vytvořená hrana
     */
    public static Edge createEdge(Vertex sourceVertex, Vertex targetVertex, EdgeLabel label,
            RevisionInterval revisionInterval) {
        return createEdge(sourceVertex, targetVertex, label, false, revisionInterval);
    }
    
    /**
     * Vytvořit hranu.
     * @param sourceVertex zdrojový uzel hrany
     * @param targetVertex cílový uzel hrany
     * @param label typ hrany
     * @param interpolated příznak, zda je hrana interpolována
     * @param revisionInterval interval platnosti nové hrany
     * @return nově vytvořená hrana
     */
    public static Edge createEdge(Vertex sourceVertex, Vertex targetVertex, EdgeLabel label,
            boolean interpolated, RevisionInterval revisionInterval) {
        if (sourceVertex == null) {
            throw new IllegalArgumentException("The source vertex was null.");
        }
        if (targetVertex == null) {
            throw new IllegalArgumentException("The target vertex was null.");
        }
        if (revisionInterval == null) {
            throw new IllegalArgumentException("The revision was null.");
        }

        if (!isEdgeLabelCorrect(sourceVertex, targetVertex, label)) {
            VertexType sourceType = VertexType.getType(sourceVertex);
            String sourceName = GraphOperation.getName(sourceVertex);

            VertexType targetType = VertexType.getType(targetVertex);
            String targetName = GraphOperation.getName(targetVertex);
            LOGGER.error("The source {}[{}] and target {}[{}] cannot be connected with edge {}.", sourceName,
                    sourceType, targetName, targetType, label);
        }

        Edge edge = sourceVertex.addEdge(label.t(), targetVertex);
        edge.setProperty(EdgeProperty.TRAN_START.t(), revisionInterval.getStart());
        edge.setProperty(EdgeProperty.TRAN_END.t(), revisionInterval.getEnd());

        if (label == EdgeLabel.DIRECT || label == EdgeLabel.FILTER) {
            // pro direct a filter je třeba nastavit TARGET_ID index
            Long targetId = (Long) targetVertex.getId();
            edge.setProperty(EdgeProperty.TARGET_ID.t(), targetId);
            edge.setProperty(EdgeProperty.INTERPOLATED.t(), interpolated);
        } else if (label == EdgeLabel.HAS_RESOURCE || label == EdgeLabel.HAS_PARENT) {
            // pro resource a parent hranu je třeba nastavit CHILD_NAME index
            String childName = GraphOperation.getName(sourceVertex);
            edge.setProperty(EdgeProperty.CHILD_NAME.t(), GraphOperation.processValueForChildName(childName));
        } else if (label == EdgeLabel.HAS_SOURCE) {
            // pro source code, je to index s lokálním názvem souboru
            String localName = targetVertex.getProperty(NodeProperty.SOURCE_NODE_LOCAL.t());
            edge.setProperty(EdgeProperty.SOURCE_LOCAL_NAME.t(), GraphOperation.processValueForLocalName(localName));
        }
        return edge;
    }

    /**
     * Ověří jestli mezi danými vrcholy může existovat hrana s daným labelem.
     * @param sourceVertex zdrojový vrchol
     * @param targetVertex cílový vrchol
     * @param label ověřovaný label
     * @return true, jestliže je taková hrana povolena
     */
    public static boolean isEdgeLabelCorrect(Vertex sourceVertex, Vertex targetVertex, EdgeLabel label) {
        VertexType sourceType = VertexType.getType(sourceVertex);
        VertexType targetType = VertexType.getType(targetVertex);

        switch (label) {
        case DIRECT:
        case FILTER:
        case HAS_PARENT:
        case MAPS_TO:
            if (sourceType == VertexType.NODE && targetType == VertexType.NODE) {
                return true;
            }
            break;
        case HAS_ATTRIBUTE:
            if (sourceType == VertexType.NODE && targetType == VertexType.ATTRIBUTE) {
                return true;
            }
            break;
        case HAS_RESOURCE:
            if ((sourceType == VertexType.NODE && targetType == VertexType.RESOURCE)
                    || (sourceType == VertexType.RESOURCE && targetType == VertexType.SUPER_ROOT)) {
                return true;
            }
            break;
        case HAS_REVISION:
            if (sourceType == VertexType.REVISION_ROOT && targetType == VertexType.REVISION_NODE) {
                return true;
            }
            break;
        case HAS_SOURCE:
            if (sourceType == VertexType.SOURCE_ROOT && targetType == VertexType.SOURCE_NODE) {
                return true;
            }
            break;
        case IN_LAYER:
            if (sourceType == VertexType.RESOURCE && targetType == VertexType.LAYER) {
                return true;
            }
            break;
        default:
            throw new IllegalStateException("Unknown label " + label + ".");
        }
        return false;
    }

    /**
     * Create node attribute valid in revision interval <startRevision, maxMinorRevision>.
     * 
     * @param transaction transaction transakce do db
     * @param node uzel, pro který se přidává atribut
     * @param key klíč atributu
     * @param value hodnota atributu 
     * @param startRevision  start revision number
     * @return vertex představující nově vytvořený atribut
     */
    public static Vertex createNodeAttribute(TitanTransaction transaction, Vertex node, String key, Object value,
            Double startRevision) {
        Double endRevision = RevisionUtils.getMaxMinorRevisionNumber(startRevision);
        return createNodeAttribute(transaction, node, key, value, new RevisionInterval(startRevision, endRevision));
    }

    /**
     * Vytvoří pro uzel nový atribut.
     * @param transaction transaction transakce do db
     * @param node uzel, pro který se přidává atribut
     * @param key klíč atributu
     * @param value hodnota atributu 
     * @param revisionInterval interval platnosti nového atributu
     * @return vertex představující nově vytvořený atribut
     */
    public static Vertex createNodeAttribute(TitanTransaction transaction, Vertex node, String key, Object value,
            RevisionInterval revisionInterval) {
        if (node == null) {
            throw new IllegalArgumentException("The argument vertex was null.");
        }
        if (revisionInterval == null) {
            throw new IllegalArgumentException("The revision was null.");
        }

        Vertex newNode = transaction.addVertex();
        newNode.setProperty(NodeProperty.VERTEX_TYPE.t(), VertexType.ATTRIBUTE);
        newNode.setProperty(NodeProperty.ATTRIBUTE_NAME.t(), key);
        newNode.setProperty(NodeProperty.ATTRIBUTE_VALUE.t(), value);
        Edge edge = node.addEdge(EdgeLabel.HAS_ATTRIBUTE.t(), newNode);
        edge.setProperty(EdgeProperty.TRAN_START.t(), revisionInterval.getStart());
        edge.setProperty(EdgeProperty.TRAN_END.t(), revisionInterval.getEnd());

        return newNode;
    }

    /**
     * Vytvoří uzel pro revizi.
     * @param transaction transkace pro přístup k db
     * @param revisionRoot revision root
     * @param revisionNumber číslo nově vytvářené revize
     * @return vrchol odpovídající nové revizi
     */
    public static Vertex createRevisionNode(TitanTransaction transaction, Vertex revisionRoot, Double revisionNumber,
            Vertex previousRevisionNode, Vertex nextRevisionNode) {
        return createRevisionNode(transaction, revisionRoot, revisionNumber, previousRevisionNode, nextRevisionNode, Boolean.FALSE, new Date());
    }

    /**
     * Vytvoří uzel pro revizi.
     * @param transaction transkace pro přístup k db
     * @param revisionRoot revision root
     * @param revisionNumber číslo nově vytvářené revize
     * @param isCommited true, jestliže je nově zakládaná revize již commitnutá
     * @param time čas commitnutí nově zakládané revize
     * @return vrchol odpovídající nové revizi
     */
    public static Vertex createRevisionNode(TitanTransaction transaction, Vertex revisionRoot, Double revisionNumber,
            Vertex previousRevisionNode, Vertex nextRevisionNode, Boolean isCommitted, Date time) {
        if (revisionRoot == null) {
            throw new IllegalArgumentException("The argument revisionRootNode was null.");
        }
        if (revisionNumber == null) {
            throw new IllegalArgumentException("The argument revision was null.");
        }

        Vertex newRevisionNode = transaction.addVertex(null);
        newRevisionNode.setProperty(NodeProperty.VERTEX_TYPE.t(), VertexType.REVISION_NODE);

        // Create edge between revision node and revision root
        Edge newEdge = revisionRoot.addEdge(EdgeLabel.HAS_REVISION.t(), newRevisionNode);
        newEdge.setProperty(EdgeProperty.TRAN_START.t(), revisionNumber);
        newEdge.setProperty(EdgeProperty.TRAN_END.t(), revisionNumber);

        newRevisionNode.setProperty(NodeProperty.REVISION_NODE_REVISION.t(), revisionNumber);
        newRevisionNode.setProperty(NodeProperty.REVISION_NODE_COMMITTED.t(), isCommitted);
        newRevisionNode.setProperty(NodeProperty.REVISION_NODE_COMMIT_TIME.t(), time);
        
        // set previous revision number
        if(previousRevisionNode == null) {
            // no previous revision exists, set -1.0 indicating null revision
            newRevisionNode.setProperty(NodeProperty.REVISION_NODE_PREVIOUS_REVISION.t(), -1.0);
        } else {
            newRevisionNode.setProperty(NodeProperty.REVISION_NODE_PREVIOUS_REVISION.t(), 
                    previousRevisionNode.getProperty(NodeProperty.REVISION_NODE_REVISION.t()));
            // set next revision on the previous revision node
            previousRevisionNode.setProperty(NodeProperty.REVISION_NODE_NEXT_REVISION.t(), revisionNumber);
        }
        
        // set next revision number
        if(nextRevisionNode == null) {
            // no next revision exists, set -1.0 indicating null revision
            newRevisionNode.setProperty(NodeProperty.REVISION_NODE_NEXT_REVISION.t(), -1.0);
        } else {
            newRevisionNode.setProperty(NodeProperty.REVISION_NODE_PREVIOUS_REVISION.t(), 
                    nextRevisionNode.getProperty(NodeProperty.REVISION_NODE_REVISION.t()));
            // set previous revision number on the next revision node
            nextRevisionNode.setProperty(NodeProperty.REVISION_NODE_PREVIOUS_REVISION.t(), revisionNumber);
        }
        
        if(isCommitted) {
            // Practically should never happen. When a new revision is created, it is always uncommitted
            // because revision is committed after the merge is performed (after the update is completed)
            revisionRoot.setProperty(NodeProperty.LATEST_COMMITTED_REVISION.t(), revisionNumber);
        } else {
            revisionRoot.setProperty(NodeProperty.LATEST_UNCOMMITTED_REVISION.t(), revisionNumber);
        }

        return newRevisionNode;
    }

    /**
     * Založí nový source code vertex.
     * @param transaction transkace pro přístup k db
     * @param sourceRoot source code root vertex
     * @param revision revize nového vertexu
     * @param localName lokální jméno skriptu
     * @param fileHash hash souboru
     * @return nově vytvořený vertex
     */
    public static Vertex createSourceCode(TitanTransaction transaction, Vertex sourceRoot, Double revision,
            String localName, String fileHash) {
        if (sourceRoot == null) {
            throw new IllegalArgumentException("The sourceRoot must not be null.");
        }
        if (revision == null) {
            throw new IllegalArgumentException("The revision  must not be null.");
        }

        Vertex newSourceVertex = transaction.addVertex(null);
        newSourceVertex.setProperty(NodeProperty.VERTEX_TYPE.t(), VertexType.SOURCE_NODE);
        
        
        Double endRevision = RevisionUtils.getMaxMinorRevisionNumber(revision);
        
        // Provazani hranou s kořenovým uzlem
        Edge newEdge = sourceRoot.addEdge(EdgeLabel.HAS_SOURCE.t(), newSourceVertex);
        newEdge.setProperty(EdgeProperty.TRAN_START.t(), revision);
        newEdge.setProperty(EdgeProperty.TRAN_END.t(),  endRevision);
        newEdge.setProperty(EdgeProperty.SOURCE_LOCAL_NAME.t(), GraphOperation.processValueForLocalName(localName));

        String id = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        newSourceVertex.setProperty(NodeProperty.SOURCE_NODE_ID.t(), id);
        newSourceVertex.setProperty(NodeProperty.SOURCE_NODE_LOCAL.t(), localName);
        newSourceVertex.setProperty(NodeProperty.SOURCE_NODE_HASH.t(), fileHash);

        return newSourceVertex;
    }

    /**
     * Vytvoří kopii dané hrany vedoucí mezi danými vrcholy a platnou v dané revizi.
     * @param originalEdge původní hrana
     * @param startVertex startovní vrchol pro novou hranu
     * @param endVertex koncový vrchol pro novou hranu
     * @param revisionInterval interval revizí, ve kterém může maximálně platit
     * @return nově vytvořená hrana
     */
    public static Edge copyEdge(Edge originalEdge, Vertex startVertex, Vertex endVertex,
            RevisionInterval revisionInterval) {

        RevisionInterval sourceRevisionInterval = RevisionUtils.getRevisionInterval(originalEdge);
        RevisionInterval newRevisionInterval = RevisionInterval.createIntersection(sourceRevisionInterval,
                revisionInterval);
        if (newRevisionInterval == null) {
            LOGGER.error("Intersection of revision interval for copied attribute is null: {} {}.",
                    sourceRevisionInterval, revisionInterval);
            return null;
        }
        EdgeLabel label = EdgeLabel.parseFromDbType(originalEdge.getLabel());
        Edge newEdge = createEdge(startVertex, endVertex, label, newRevisionInterval);

        Set<String> technicalProperties = EdgeProperty.collectPropertyNames();
        for (String propertyKey : originalEdge.getPropertyKeys()) {
            if (technicalProperties.contains(propertyKey)) {
                continue;
            }
            newEdge.setProperty(propertyKey, originalEdge.getProperty(propertyKey));
        }

        return newEdge;
    }

    /**
     * Zkopíruje podstrom včetně všech hran a atributů. 
     * @param transaction transakce pro přístup k db
     * @param sourceVertex originální vrchol, jehož podstrom se kopíruje
     * @param targetParent předek, pod který se má vrchol zavěsit, může být null
     * @param revisionInterval interval revizí pro kterou se podstrom buduje
     * @return root nového podstromu
     */
    public static Vertex copySubTree(TitanTransaction transaction, Vertex sourceVertex, Vertex targetParent,
            RevisionInterval revisionInterval) {
        VertexType sourceVertexType = VertexType.getType(sourceVertex);
        if (sourceVertexType != VertexType.NODE) {
            throw new IllegalArgumentException("The source root must be node, but it is " + sourceVertexType + ".");
        }

        Map<Vertex, Vertex> vertexMap = new HashMap<>();
        Vertex newVertex = copySubTreeVertices(transaction, sourceVertex, targetParent, revisionInterval, vertexMap);
        copySubTreeEdges(sourceVertex, revisionInterval, vertexMap);

        return newVertex;
    }

    /**
     * Zkopíruje vrcholy a atributy podstromu.
     * @param transaction transakce pro přístup k db
     * @param sourceVertex originální vrchol, jehož podstrom se kopíruje
     * @param targetParent předek, pod který se má vrchol zavěsit, může být null
     * @param revisionInterval interval revizí pro kterou se podstrom buduje
     * @param vertexMap mapa nových vrcholů klíčovaných jejich originály
     * @return root nového podstromu
     */
    private static Vertex copySubTreeVertices(TitanTransaction transaction, Vertex sourceVertex, Vertex targetParent,
            RevisionInterval revisionInterval, Map<Vertex, Vertex> vertexMap) {

        Edge sourceControlEdge = GraphOperation.getControlEdge(sourceVertex);
        RevisionInterval sourceRevisionInterval = RevisionUtils.getRevisionInterval(sourceControlEdge);

        RevisionInterval newRevisionInterval = RevisionInterval.createIntersection(sourceRevisionInterval,
                revisionInterval);
        if (newRevisionInterval == null) {
            LOGGER.error("Intersection of revision interval for copied node is null: {} {}.", sourceRevisionInterval,
                    revisionInterval);
            return null;
        }

        String name = GraphOperation.getName(sourceVertex);
        String type = GraphOperation.getType(sourceVertex);
        Vertex newVertex = GraphCreation.createNode(transaction, targetParent, name, type, newRevisionInterval);
        vertexMap.put(sourceVertex, newVertex);

        List<Vertex> sourceChildren = GraphOperation.getDirectChildren(sourceVertex, revisionInterval);
        for (Vertex sourceChild : sourceChildren) {
            copySubTreeVertices(transaction, sourceChild, newVertex, revisionInterval, vertexMap);
        }

        List<Vertex> sourceAttributes = GraphOperation.getAdjacentVertices(sourceVertex, Direction.OUT,
                revisionInterval, EdgeLabel.HAS_ATTRIBUTE);
        for (Vertex attr : sourceAttributes) {
            copyNodeAttribute(transaction, attr, newVertex, revisionInterval);
        }
        return newVertex;
    }

    /**
     * Zkopíruje hrany do podstromu
     * @param sourceVertex originální vrchol, jehož podstrom se kopíruje
     * @param revisionInterval interval revizí pro kterou se podstrom buduje
     * @param vertexMap mapa nových vrcholů klíčovaných jejich originály
     */
    private static void copySubTreeEdges(Vertex sourceVertex, RevisionInterval revisionInterval,
            Map<Vertex, Vertex> vertexMap) {
        List<Vertex> sourceChildren = GraphOperation.getDirectChildren(sourceVertex, revisionInterval);
        for (Vertex sourceChild : sourceChildren) {
            copySubTreeEdges(sourceChild, revisionInterval, vertexMap);
        }

        Vertex targetVertex = vertexMap.get(sourceVertex);
        if (targetVertex == null) {
            LOGGER.error("Source vertex {} doesn't have copy.", sourceVertex);
            return;
        }

        List<Edge> inEdges = GraphOperation.getAdjacentEdges(sourceVertex, Direction.IN, revisionInterval,
                EdgeLabel.DIRECT, EdgeLabel.FILTER);
        for (Edge edge : inEdges) {
            Vertex otherVertex = edge.getVertex(Direction.OUT);
            Vertex otherVertexMapped = vertexMap.get(otherVertex);
            if (otherVertexMapped != null) {
                copyEdge(edge, otherVertexMapped, targetVertex, revisionInterval);
            } else {
                copyEdge(edge, otherVertex, targetVertex, revisionInterval);
            }
        }

        List<Edge> outEdges = GraphOperation.getAdjacentEdges(sourceVertex, Direction.OUT, revisionInterval,
                EdgeLabel.DIRECT, EdgeLabel.FILTER);
        for (Edge edge : outEdges) {
            Vertex otherVertex = edge.getVertex(Direction.IN);
            if (!vertexMap.containsKey(otherVertex)) {
                copyEdge(edge, targetVertex, otherVertex, revisionInterval);
            }
        }
    }

    /**
     * Zkopíruje atribut novémyu uzlu.
     * @param transaction transakce pro přístup k db
     * @param sourceAttribute kopírovaný atribut
     * @param targetVertex nový vrchol, pro který se atribut vytváří
     * @param revisionInterval revize, v které se kopie vytváří
     * @return nový vrchol představující atribut
     */
    public static Vertex copyNodeAttribute(TitanTransaction transaction, Vertex sourceAttribute, Vertex targetVertex,
            RevisionInterval revisionInterval) {

        VertexType sourceAttributeType = VertexType.getType(sourceAttribute);
        if (sourceAttributeType != VertexType.ATTRIBUTE) {
            throw new IllegalArgumentException(
                    "The source attribute vertex isn't attribute, but is " + sourceAttributeType + ".");
        }

        VertexType targetVertexType = VertexType.getType(targetVertex);
        if (targetVertexType != VertexType.NODE) {
            throw new IllegalArgumentException("The target vertex must be node, but it is " + targetVertexType + ".");
        }

        Edge sourceControlEdge = GraphOperation.getAttributeControlEdge(sourceAttribute);
        RevisionInterval sourceRevisionInterval = RevisionUtils.getRevisionInterval(sourceControlEdge);
        RevisionInterval newRevisionInterval = RevisionInterval.createIntersection(sourceRevisionInterval,
                revisionInterval);
        if (newRevisionInterval == null) {
            LOGGER.error("Intersection of revision interval for copied attribute is null: {} {}.",
                    sourceRevisionInterval, revisionInterval);
            return null;
        }

        String key = GraphOperation.getAttributeKey(sourceAttribute);
        Object value = GraphOperation.getAttributeValue(sourceAttribute);
        return GraphCreation.createNodeAttribute(transaction, targetVertex, key, value, newRevisionInterval);
    }
}
