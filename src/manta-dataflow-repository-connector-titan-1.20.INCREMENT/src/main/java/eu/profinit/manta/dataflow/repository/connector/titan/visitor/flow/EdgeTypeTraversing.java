package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Enum určující styl procházení při traverzování po data flow.
 * @author tfechtner
 *
 */
public enum EdgeTypeTraversing {
    /** Pouze přímé toky. */
    DIRECT {
        @Override
        public Iterable<Vertex> getNextVertices(Vertex node, Direction direction, RevisionInterval revisionInterval) {
            return GraphOperation.getAdjacentVertices(node, direction, revisionInterval, EdgeLabel.DIRECT);
        }

        @Override
        public Iterable<Edge> getNextEdges(Vertex node, Direction direction, RevisionInterval revisionInterval) {
            return GraphOperation.getAdjacentEdges(node, direction, revisionInterval, EdgeLabel.DIRECT);
        }
    },
    /** Pouze nepříme/filter toky. */
    FILTER {
        @Override
        public Iterable<Vertex> getNextVertices(Vertex node, Direction direction, RevisionInterval revisionInterval) {
            return GraphOperation.getAdjacentVertices(node, direction, revisionInterval, EdgeLabel.FILTER);
        }

        @Override
        public Iterable<Edge> getNextEdges(Vertex node, Direction direction, RevisionInterval revisionInterval) {
            return GraphOperation.getAdjacentEdges(node, direction, revisionInterval, EdgeLabel.FILTER);
        }
    },
    /** Přes všechny toky. */
    BOTH {
        @Override
        public Iterable<Vertex> getNextVertices(Vertex node, Direction direction, RevisionInterval revisionInterval) {
            return GraphOperation.getAdjacentVertices(node, direction, revisionInterval, EdgeLabel.FILTER,
                    EdgeLabel.DIRECT);
        }

        @Override
        public Iterable<Edge> getNextEdges(Vertex node, Direction direction, RevisionInterval revisionInterval) {
            return GraphOperation.getAdjacentEdges(node, direction, revisionInterval, EdgeLabel.FILTER,
                    EdgeLabel.DIRECT);
        }
    };

    /**
     * Získá další sousedy podle zvolené politiky.
     * @param node výchozí uzel
     * @param direction směr procházení
     * @param revisionInterval interval revizí, které se mají procházet
     * @return seznam sousedů splňující podmínky
     */
    public abstract Iterable<Vertex> getNextVertices(Vertex node, Direction direction,
            RevisionInterval revisionInterval);

    /**
     * Získá další hrany podle zvolené politiky.
     * @param node výchozí uzel
     * @param direction směr procházení
     * @param revisionInterval interval revizí, které se mají procházet
     * @return seznam hran splňující podmínky
     */
    public abstract Iterable<Edge> getNextEdges(Vertex node, Direction direction, RevisionInterval revisionInterval);
}
