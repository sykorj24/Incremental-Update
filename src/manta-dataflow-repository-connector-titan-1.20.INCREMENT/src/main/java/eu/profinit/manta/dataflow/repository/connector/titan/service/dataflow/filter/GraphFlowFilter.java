package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

/**
 * Rozhraní pro filtraci v rámci datový toků.
 * Je vyžadováno, aby třídy neměly vnitřní stav, minimálně metoda {@link #testVertex(Vertex, Edge)}
 * musí být konzistentní.
 * @author tfechtner
 *
 */
public interface GraphFlowFilter {

    /**
     * Otestuje daný vertex s příslušnou příchozí hranou.
     * Metoda musí být konzistentí = pro danou dvojici vrchol hrana vždy vracet stejný výsledek.
     * @param vertex testovaný vertex
     * @param incomingEdge příslušná hrana, odkud se do tohoto vertexu přišlo
     * @return výsledek, jeslti vrchol odpovídá fitru 
     */
    FilterResult testVertex(Vertex vertex, Edge incomingEdge);
}
