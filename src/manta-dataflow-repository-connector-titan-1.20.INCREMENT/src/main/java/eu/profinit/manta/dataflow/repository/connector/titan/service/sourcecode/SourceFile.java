package eu.profinit.manta.dataflow.repository.connector.titan.service.sourcecode;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Datova trida pro reprezentaci zdrojového skriptu.
 * @author tfechtner
 * @author onouza
 *
 */
class SourceFile {
    /** Umisteni skriptu. */
    private final String location;
    /** Kodovani skriptu. */
    private final String encoding;

    /**
     * @param location Umisteni skriptu
     * @param encoding Kodovani skriptu
     */
    public SourceFile(String location, String encoding) {
        super();
        this.location = location;
        this.encoding = encoding;
    }
    
    // prekryti / implementace

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(location);
        builder.append(encoding);
        return builder.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        SourceFile other = (SourceFile) obj;
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(location, other.location);
        builder.append(encoding, other.encoding);
        return builder.isEquals();
    }
    
    // gettery / settery
    
    /**
     * @return Umisteni skriptu
     */
    public String getLocation() {
        return location;
    }

    /**
     * @return Kodovani skriptu
     */
    public String getEncoding() {
        return encoding;
    }
    
}


