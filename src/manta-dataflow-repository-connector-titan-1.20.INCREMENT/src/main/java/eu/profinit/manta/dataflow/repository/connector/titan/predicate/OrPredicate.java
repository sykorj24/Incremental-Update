package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import java.util.Collections;
import java.util.List;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;

/**
 * Evaluátor pro OR operátor.
 * Je splněn, právě tehdy když je splněn alespoň jeden jeho potomek. 
 * @author tfechtner
 *
 */
public class OrPredicate implements VertexPredicate {
    /** Množina evaluátorů, z nichž musí být některý splněn. */
    private List<VertexPredicate> children;

    /**
     * Default konstruktor s prázdnou množinou potomků. 
     */
    public OrPredicate() {
        super();
        children = Collections.emptyList();
    }

    /**
     * @param children Množina evaluátorů, z nichž musí být některý splněn.
     */
    public OrPredicate(List<VertexPredicate> children) {
        this.children = children;
    }

    @Override
    public boolean evaluate(Vertex vertex, RevisionInterval revisionInterval) {
        if (children != null) {
            for (VertexPredicate evaluator : children) {
                if (evaluator.evaluate(vertex, revisionInterval)) {
                    return true;
                }

            }
        }

        return false;
    }

    /**
     * @param children Množina evaluátorů, z nichž musí být některý splněn.
     */
    public void setChildren(List<VertexPredicate> children) {
        this.children = children;
    }
}
