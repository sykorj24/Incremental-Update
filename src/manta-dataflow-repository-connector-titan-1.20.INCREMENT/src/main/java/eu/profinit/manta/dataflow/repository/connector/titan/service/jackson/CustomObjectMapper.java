package eu.profinit.manta.dataflow.repository.connector.titan.service.jackson;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ser.CustomSerializerFactory;

import eu.profinit.manta.dataflow.model.restriction.nodetypes.RestrDisjunctionTypeDnf;


/**
 * Custom object mapper pro registraci všech speciálních jason serializátorů.
 * @author tfechtner
 *
 */
public class CustomObjectMapper extends ObjectMapper {
    
    /**
     * Výchozí konstruktor registrující serializátory.
     */
    public CustomObjectMapper() {
        CustomSerializerFactory sf = new CustomSerializerFactory();
        sf.addSpecificMapping(RestrDisjunctionTypeDnf.class, new RestrDisjunctionSerializer());
        this.setSerializerFactory(sf);
    }
}
