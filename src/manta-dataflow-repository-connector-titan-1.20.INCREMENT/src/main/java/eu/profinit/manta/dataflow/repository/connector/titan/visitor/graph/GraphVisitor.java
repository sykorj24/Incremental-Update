package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

/**
 * Rozhraní pro visitor navštěvující dataflow graf.
 * @author tfechtner
 *
 */
public interface GraphVisitor {

    /**
     * Navštíví vrstvu.
     * @param resource navštívená vrstva
     */
    void visitLayer(Vertex layer);
    /**
     * Navštíví resource.
     * @param resource navštívený resource
     */
    void visitResource(Vertex resource);

    /**
     * Navštíví uzel.
     * @param node navštívený uzel
     */
    void visitNode(Vertex node);

    /**
     * Navštíví atribut uzlu.
     * @param attribute navštívený atribut uzlu
     */
    void visitAttribute(Vertex attribute);

    /**
     * Navštíví hranu.
     * @param edge navštívená hrana
     */
    void visitEdge(Edge edge);

}
