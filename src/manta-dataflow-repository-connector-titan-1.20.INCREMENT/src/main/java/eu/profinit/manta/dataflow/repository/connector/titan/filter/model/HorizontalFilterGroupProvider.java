package eu.profinit.manta.dataflow.repository.connector.titan.filter.model;

import java.util.Set;

/**
 * Rozhraní pro třídu obhospoadřující skupiny filtrů.
 * @author tfechtner
 *
 */
public interface HorizontalFilterGroupProvider {

    /**
     * Vrátí množinu držených skupin filtrů.
     * @return množina skupin, nikdy null
     */
    Set<HorizontalFilterGroup> getGroups();

    /**
     * Pokusí se nalézet skupinu filtrů s daným id.
     * @param groupId id hledané skupiny
     * @return skupina s daným id, nebo null
     */
    HorizontalFilterGroup findGroup(String groupId);

    /**
     * Načte skupiny filtrů.
     * @return počet nalezených skupin
     */
    int refreshGroups();
}
