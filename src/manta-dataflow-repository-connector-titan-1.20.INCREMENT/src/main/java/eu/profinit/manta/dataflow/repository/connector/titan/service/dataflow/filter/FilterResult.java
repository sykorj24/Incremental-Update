package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter;

/**
 * Výsledek filtrů udávající další práci.
 * @author tfechtner
 *
 */
public enum FilterResult {
    /** Vrchol je v pořádku a mají se navštívit i jeho sousedi v daném směru. */
    OK_CONTINUE(true, true) {
        @Override
        public FilterResult sumWith(FilterResult other) {
            return other;
        }
    },
    /** Vrchol je v pořádku, ale sousedi se již nemají navštívit. */
    OK_STOP(true, false) {
        @Override
        public FilterResult sumWith(FilterResult other) {
            if (other.isVertexOk()) {
                return FilterResult.OK_STOP;
            } else {
                return NOK_STOP;
            }
        }
    },
    /** Vrchol není v pořádku, ale mají se navštívit jeho sousedi v daném směru.  */
    NOK_CONTINUE(false, true) {
        @Override
        public FilterResult sumWith(FilterResult other) {
            if (other.shouldContinue()) {
                return FilterResult.NOK_CONTINUE;
            } else {
                return NOK_STOP;
            }
        }
    },
    /** Vrchol není v pořádku a sousedi se již nemají navštívit. */
    NOK_STOP(false, false) {
        @Override
        public FilterResult sumWith(FilterResult other) {
            return NOK_STOP;
        }
    };
    

    /**
     * Sečte hodnotu instance a parametru a vrací více restriktivní výsledek.
     * @param other instance k sečtení
     * @return více restriktivní z dvojice
     */
    public abstract FilterResult sumWith(FilterResult other);

    /**
     * @return Jestli je vrchol ok.
     */
    public boolean isVertexOk() {
        return isOk;
    }

    /**
     * @return Jestli se má pokračovat k sousedům.
     */
    public boolean shouldContinue() {
        return isContinue;
    }
    
    /** Jestli je vrchol ok. */
    private final boolean isOk;
    /** Jestli se má pokračovat k sousedům. */
    private final boolean isContinue;

    /**
     * @param isOk Jestli je vrchol ok.
     * @param isContinue Jestli se má pokračovat k sousedům.
     */
    FilterResult(boolean isOk, boolean isContinue) {
        this.isOk = isOk;
        this.isContinue = isContinue;
    }
}
