package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable;

import java.util.Deque;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Prohledávání do šířky.
 * @author tfechtner
 *
 */
public class SearchOrderBfs implements SearchOrder {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchOrderBfs.class);
    /** Maximální doba čekání na prvek z fronty. */
    private final int maxPollWait;

    /**
     * Výchozí kosntruktor nastavující čekání na výchozí hodotu.
     */
    public SearchOrderBfs() {
        this(DEFAULT_MAX_POLL_WAIT);
    }

    /**
     * @param maxPollWait Maximální doba čekání na prvek z fronty
     */
    public SearchOrderBfs(int maxPollWait) {
        super();
        this.maxPollWait = maxPollWait;
    }

    @Override
    public <T> T getNext(Deque<T> deque) {
        if (deque instanceof BlockingDeque) {
            try {
                return ((BlockingDeque<T>) deque).pollFirst(maxPollWait, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                LOGGER.warn("Interrupted fetching next element.");
                return null;
            }
        } else {
            return deque.pollFirst();
        }
    }
}
