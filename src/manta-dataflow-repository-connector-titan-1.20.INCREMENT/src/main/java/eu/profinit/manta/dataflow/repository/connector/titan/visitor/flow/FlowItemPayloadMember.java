package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow;

/**
 * Rozrhaní definující členy payladu, který si tahne {@link FlowItem} s sebou v rámci datových toků.
 * @author tfechtner
 *
 */
public interface FlowItemPayloadMember {
    
    /**
     * Vytvoří deep copy od aktivní instance a tuto kopii vrátí.
     * @return deep copy instanci
     */
    FlowItemPayloadMember createDeepCopy();
}
