package eu.profinit.manta.dataflow.repository.connector.titan.filter.model;

import java.util.List;
import java.util.Set;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Rozhraní pro třídu obhospoadřující horizontální filtry.
 * @author tfechtner
 *
 */
public interface HorizontalFilterProvider {

    /**
     * Pokusí se nalézet filtr s daným id.
     * @param filterId id hledaného filtru
     * @return hledaný filtr, nebo null
     */
    HorizontalFilter getFilter(String filterId);

    /**
     * Ověří jestli je daný vrchol filtrován danými filtry.
     * @param vertex ověřovaný vrchol
     * @param revisionInterval interval revizí, které se zkoumá
     * @param activeFilters množina id filtrů, které se berou v platnost
     * @return true, jestliže je vrchol filtrován
     */
    boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval, Set<String> activeFilters);

    /**
     * Ověří jestli je daný vrchol filtrován daným filtrem.
     * @param vertex ověřovaný vrchol
     * @param revisionInterval interval revizí, které se zkoumá
     * @param filterId id použitého filtru
     * @return true, jestliže je vrchol filtrován
     */
    boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval, String filterId);

    /**
     * Přidá množinu filtrů.
     * @param filterSet množina filtrů, která se přidává do provideru
     */
    void addFilters(Set<HorizontalFilter> filterSet);

    /**
     * Obnoví filtry.
     * @return počet přidaných filtrů
     */
    int refreshFilters();

    /**
     * Vrátí množinu držených filtrů.
     * @return množina filtrů, nikdy null
     */
    List<HorizontalFilter> getFilters();
}
