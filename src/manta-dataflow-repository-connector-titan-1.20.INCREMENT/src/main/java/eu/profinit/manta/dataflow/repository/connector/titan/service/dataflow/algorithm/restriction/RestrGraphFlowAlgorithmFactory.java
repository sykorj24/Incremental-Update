package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.restriction;

import java.util.List;
import java.util.Set;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.GraphFlowFilter;

/**
 * Továrna pro algoritmus pro ořezávání přes predikáty.
 *
 * @author tfechtner
 */
public class RestrGraphFlowAlgorithmFactory implements GraphFlowAlgorithmFactory {
    /** Výchozí hodnota pro maximální dobu výpočtu - 15 minut.*/
    private static final long DEFAULT_MAXIMAL_EXEC_TIME = 900000;
    
    /** Seznam typů (pro věci jako třeba tabulky), ve kterých může končit datový tok - neobsahuje transformace. */
    private Set<String> databaseResources;
    
    /** Maximální čas pro výpočet v ms.*/
    private long maxExecutionTime = DEFAULT_MAXIMAL_EXEC_TIME;

    @Override
    public RestrGraphFlowAlgorithm createGraphFlowAlgorithm() {
        return createGraphFlowAlgorithm(null);
    }
    
    @Override
    public RestrGraphFlowAlgorithm createGraphFlowAlgorithm(List<GraphFlowFilter> additionalFilters) {
        RestrGraphFlowAlgorithm predicateGraphFlowAlgorithm = new RestrGraphFlowAlgorithm();
        if (databaseResources != null) {
            predicateGraphFlowAlgorithm.setDatabaseResources(databaseResources);
        }
        predicateGraphFlowAlgorithm.setMaxExecutionTime(maxExecutionTime);
        predicateGraphFlowAlgorithm.setFilters(additionalFilters);
        
        return predicateGraphFlowAlgorithm;
    }

    @Override
    public RestrGraphFlowAlgorithm createGraphFlowAlgorithm(boolean isFilterEdges) {
        return createGraphFlowAlgorithm(isFilterEdges, null);
    }
    
    @Override
    public RestrGraphFlowAlgorithm createGraphFlowAlgorithm(boolean isFilterEdges,
            List<GraphFlowFilter> additionalFilters) {
        
        RestrGraphFlowAlgorithm algorithm = createGraphFlowAlgorithm(additionalFilters);
        algorithm.setFilterEdgesEnabled(isFilterEdges);

        return algorithm;
    }

    /**
     * @return Seznam typů (pro věci jako třeba tabulky), ve kterých může končit datový tok
     */
    public Set<String> getDatabaseResources() {
        return databaseResources;
    }

    /**
     * @param databaseResources Seznam typů (pro věci jako třeba tabulky), ve kterých může končit datový tok
     */
    public void setDatabaseResources(Set<String> databaseResources) {
        this.databaseResources = databaseResources;
    }

    /**
     * @return Maximální čas pro výpočet v ms.
     */
    public long getMaxExecutionTime() {
        return maxExecutionTime;
    }

    /**
     * @param maxExecutionTime Maximální čas pro výpočet v ms.
     */
    public void setMaxExecutionTime(long maxExecutionTime) {
        this.maxExecutionTime = maxExecutionTime;
    }
    
    
}
