package eu.profinit.manta.dataflow.repository.connector.titan.service.collection;

import java.util.Collection;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

/**
 * Rozhraní pro procesory zpracovávající kolekci v databázové transakci.
 * Přičemž zajišťují korektní práci s transakcí.
 * @author tfechtner
 *
 */
public interface CollectionProcessor {

    /**
     * Zpracuje kolekci.
     * @param databaseHolder držák na databázi
     * @param transactionLevel požadovaná úroveň přístupu k databázi
     * @param collection kolekce dat ke zpracování
     * @param callback callback, který se volá na každý element
     * @param resultHolder výsledek zpracování
     * 
     * <E> typ elementu
     * <R> typ třídy držící výstup
     */
    <E, R> void processCollection(DatabaseHolder databaseHolder, TransactionLevel transactionLevel,
            Collection<E> collection, CollectionProcessorCallback<E, R> callback, R resultHolder);
}
