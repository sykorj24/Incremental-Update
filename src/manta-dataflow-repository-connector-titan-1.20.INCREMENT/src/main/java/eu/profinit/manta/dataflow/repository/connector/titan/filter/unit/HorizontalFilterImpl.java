package eu.profinit.manta.dataflow.repository.connector.titan.filter.unit;

import java.util.List;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilter;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterType;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterUnit;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * DTO pro reprezentaci jednoho filtru.
 * @author tfechtner
 *
 */
public class HorizontalFilterImpl implements HorizontalFilter {
    /** ID filtru. */
    private String id;
    /** Název filtru. */
    private String name;
    /** Typ filtru. */
    private HorizontalFilterType type;
    /** Pořadí filtru. */
    private int order;
    /** Jednotkové filtry, které tento filtr reprezentuje. */
    private List<HorizontalFilterUnit> filterUnits;
    /** Vrstva, pro kterou lze tento filtr nastavit. Pokud je {@code null}, pak lze nastavit pro vsechny vrstvy */
    private String layer;

    @Override
    public boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval) {
        if (filterUnits == null) {
            return false;
        }

        for (HorizontalFilterUnit filter : filterUnits) {
            if (filter.isFiltered(vertex, revisionInterval)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public HorizontalFilterType getType() {
        return type;
    }

    @Override
    public int getOrder() {
        return order;
    }

    /**
     * @return Jednotkové filtry, které tento filtr reprezentuje. 
     */
    public List<HorizontalFilterUnit> getFilterUnits() {
        return filterUnits;
    }

    /**
     * @param filterUnits Jednotkové filtry, které tento filtr reprezentuje. 
     */
    public void setFilterUnits(List<HorizontalFilterUnit> filterUnits) {
        this.filterUnits = filterUnits;
    }

    /**
     * @param id ID filtru.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @param name Název filtru
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param type Typ filtru
     */
    public void setType(HorizontalFilterType type) {
        this.type = type;
    }

    /**
     * @param order Pořadí filtru.
     */
    public void setOrder(int order) {
        this.order = order;
    }
    
    /**
     * @return Vrstva, pro kterou lze tento filtr nastavit. Pokud je {@code null}, pak lze nastavit pro vsechny vrstvy
     */
    public String getLayer() {
        return layer;
    }

    /**
     * @param layer Vrstva, pro kterou lze tento filtr nastavit. Pokud je {@code null}, pak lze nastavit pro vsechny vrstvy
     */
    public void setLayer(String layer) {
        this.layer = layer;
    }
    
}
