package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import java.util.Collections;
import java.util.List;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;

/**
 * Evaluátor pro AND operátor. 
 * Je splněn právě tehdy když jsou splěni všichni potomci.  
 * @author tfechtner
 *
 */
public class AndPredicate implements VertexPredicate {
    /** Množina evaluátorů, kteří musí být všichni splněni. */
    private List<VertexPredicate> children;

    /**
     * Default konstruktor s prázdnou množinou potomků. 
     */
    public AndPredicate() {
        super();
        children = Collections.emptyList();
    }

    /**
     * @param children Množina evaluátorů, kteří musí být všichni splněni
     */
    public AndPredicate(List<VertexPredicate> children) {
        this.children = children;
    }

    @Override
    public boolean evaluate(Vertex vertex, RevisionInterval revisionInterval) {
        if (children != null) {
            for (VertexPredicate evaluator : children) {
                if (!evaluator.evaluate(vertex, revisionInterval)) {
                    return false;
                }
            }
            return true;
        }

        return false;
    }

    /**
     * @param children množina evaluátorů, kteří musí být všichni splněni
     */
    public void setChildren(List<VertexPredicate> children) {
        this.children = children;
    }
}
