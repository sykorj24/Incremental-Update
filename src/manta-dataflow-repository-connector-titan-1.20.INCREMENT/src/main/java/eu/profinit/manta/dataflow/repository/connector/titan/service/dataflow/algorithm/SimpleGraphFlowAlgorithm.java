package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.NotImplementedException;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterResult;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.EdgeFlowTraverser;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowItem;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowTraverser;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowTraverserFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowTraverserFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowVisitor;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Jednoduchý algoritmus procházející všechny dostupné uzly ze startovních.
 * @author tfechtner
 *
 */
public class SimpleGraphFlowAlgorithm extends AbstractGraphFlowAlgorithm implements FlowVisitor {
    /** Množina id vrcholů, po kterých se algoritmus může pohybovat. */
    private Set<Object> allowedNodeSet;
    /** Množina id vrcholů, které byly nalezeny v rámci běhu algoritmu. */
    private Set<Object> outputNodes;

    private FlowTraverserFactory flowTraverserFactory = new FlowTraverserFactoryGeneric<EdgeFlowTraverser>(
            EdgeFlowTraverser.class);

    @Override
    protected Set<Object> findNodesByAlgorithmInternal(Collection<Vertex> startNodes, Set<Object> allowedNodes,
            TitanTransaction dbTransaction, Direction direction, RevisionInterval revisionInterval) {
        if (allowedNodes != null) {
            allowedNodeSet = new HashSet<Object>(allowedNodes);
        }

        outputNodes = new HashSet<Object>();
        Iterable<Object> startNodesIds = transformVerticesToIds(startNodes);

        FlowTraverser flowTraverser = flowTraverserFactory.createFlowTraverser();
        flowTraverser.traverse(this, dbTransaction, startNodesIds, getEdgeType(), direction, revisionInterval);

        return outputNodes;
    }

    @Override
    public FilterResult visitAndContinue(Vertex node, FlowItem flowItem) {
        if (outputNodes == null) {
            throw new IllegalStateException("Output nodes must not be null.");
        }

        if (allowedNodeSet == null || allowedNodeSet.contains(node.getId())) {
            FilterResult result = testVertexWithFilters(node, flowItem.getIncomingEdge());
            if (result.isVertexOk()) {
                outputNodes.add(node.getId());
            }
            return result;
        } else {
            return FilterResult.NOK_STOP;
        }
    }

    /**
     * @return továrna vytvářející traversery používané algoritmem
     */
    public FlowTraverserFactory getFlowTraverserFactory() {
        return flowTraverserFactory;
    }

    /**
     * @param flowTraverserFactory továrna vytvářející traversery používané algoritmem
     */
    public void setFlowTraverserFactory(FlowTraverserFactory flowTraverserFactory) {
        this.flowTraverserFactory = flowTraverserFactory;
    }

    @Override
    public void setLimit(long limit) {
        throw new NotImplementedException((String) null);
    }
}
