package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Traverser pro vertikální procházení grafu, přičemž jde pouze po uzlech a pouze v daném podstromě
 * definovaném resource a případně databází.
 * @author tfechtner
 *
 */
public class SubGraphOnlyNodesTraverser implements GraphTraverser {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(SubGraphOnlyNodesTraverser.class);
    /** Jméno resource, kde se bude traverzovat. */
    private final Collection<String> resourceNames;
    /** Jméno datanáze, kde se bude traverzovat, může být null. */
    private final String databaseName;
    /** Kolece názvů schémat, kde se bude traverzovat, může být null. */
    private final Collection<String> schemaNames;

    /**
     * @param resourceName jméno resource, kde se bude traverzovat
     * @param databaseName jméno databáze, kde se bude traverzovat, může být null
     * @param schemaNames Kolece názvů schémat, kde se bude traverzovat, může být null.
     */
    public SubGraphOnlyNodesTraverser(String resourceName, String databaseName, Collection<String> schemaNames) {
        super();
        this.resourceNames = Collections.singleton(resourceName);
        this.databaseName = databaseName;
        if (schemaNames != null) {
            this.schemaNames = schemaNames;
        } else {
            this.schemaNames = Collections.emptySet();
        }
    }

    /**
     * @param resourceNames kolekce názvů resourců, které se budou traverzovat
     */
    public SubGraphOnlyNodesTraverser(Collection<String> resourceNames) {
        Validate.notEmpty(resourceNames, "Resource name collection mustn't be empty.");
        this.resourceNames = new HashSet<>(resourceNames);
        this.databaseName = null;
        this.schemaNames = Collections.emptySet();
    }

    @Override
    public void traverse(GraphVisitor visitor, Vertex superRoot, RevisionInterval revisionInterval) {
        // filtr na resource
        Set<Vertex> resources = new HashSet<>();
        for (String name : resourceNames) {
            resources.addAll(GraphOperation.getChildrenWithName(superRoot, name, revisionInterval));
        }

        if (resources.size() > 0) {
            for (Vertex resourceVertex : resources) {
                processDatabases(visitor, resourceVertex, revisionInterval);
            }
        } else {
            LOGGER.warn("Resource vertex with names " + resourceNames.toString() + " does not exist.");
        }
    }

    /**
     * Zpracuje databáze.
     * @param visitor visitor, který se má použít pro navštívejí
     * @param resourceVertex resource vertex, jehož databáze se mají zpracovat
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    public void processDatabases(GraphVisitor visitor, Vertex resourceVertex, RevisionInterval revisionInterval) {
        // případný filtr na databázi
        if (StringUtils.isNotBlank(databaseName)) {
            List<Vertex> databases = GraphOperation.getChildrenWithName(resourceVertex, databaseName, revisionInterval);
            if (databases.size() > 0) {
                for (Vertex databaseVertex : databases) {
                    processSchemas(visitor, databaseVertex, revisionInterval);
                }
            } else {
                LOGGER.warn("Database vertex with name " + databaseName + " does not exist.");
            }
        } else {
            List<Vertex> databaseVertices = GraphOperation.getAdjacentVertices(resourceVertex, Direction.IN,
                    revisionInterval, EdgeLabel.HAS_RESOURCE);
            for (Vertex databaseVertex : databaseVertices) {
                if (GraphOperation.getParent(databaseVertex) == null) {
                    processSchemas(visitor, databaseVertex, revisionInterval);
                }
            }
        }
    }

    /**
     * Zpracuje schémata.
     * @param visitor visitor, který se má použít pro navštívejí
     * @param databaseVertex databáze, jejíž schémata se mají zpracovat
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    public void processSchemas(GraphVisitor visitor, Vertex databaseVertex, RevisionInterval revisionInterval) {
        List<Vertex> schemaVertices = GraphOperation.getAdjacentVertices(databaseVertex, Direction.IN, revisionInterval,
                EdgeLabel.HAS_PARENT);
        for (Vertex schemaVertex : schemaVertices) {
            String name = GraphOperation.getName(schemaVertex);
            if (schemaNames.isEmpty() || schemaNames.contains(name)) {
                processNode(visitor, schemaVertex, revisionInterval);
            }
        }
    }

    /**
     * Zpracuje uzel a všechny jeho potomky (uzly).
     * @param visitor používaný visitor
     * @param node zpracovávaný uzel
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    protected void processNode(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval) {
        visitor.visitNode(node);

        List<Vertex> children = GraphOperation.getAdjacentVertices(node, Direction.IN, revisionInterval,
                EdgeLabel.HAS_PARENT);
        for (Vertex child : children) {
            processNode(visitor, child, revisionInterval);
        }
    }

    @Override
    public void traverseOnlyOneResource(GraphVisitor visitor, Vertex resource, RevisionInterval revisionInterval) {
        throw new UnsupportedOperationException();
    }

}
