package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingDeque;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.TransactionAware;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.VisitedPart;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrder;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Společná implementace pro procesory.
 * Implementuje samotnou smyčlu zpracování, která postupně zpracovává jednotlivé vrcholy z fronty.
 * Jakmile zpracuje příslušný počet ({@link #objectsPerTransaction}), tak se smyčka zastaví a 
 * zpracovávání skončí. 
 * Nové vrcholy vkládá do fronty podle zvoleného stylu prohledávání {@link #searchOrder}.
 * 
 * @author tfechtner
 *
 * @param <T> návratový typ procesoru
 */
public abstract class AbstractTraverserProcessor<T> implements TraverserProcessor<T> {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(TraverserProcessorPrefix.class);

    /** Visitor navštěvující jednotlivé elementy v grafu. */
    private GraphVisitor visitor;
    /** interval revizí, které procesor prochází. */
    private RevisionInterval revisionInterval;
    /** fronta id vrcholů ke zpracování. */
    private Deque<Object> idsToProcess = new LinkedList<Object>();
    /** počet objektů ke zpracování v rámci této transakce. */
    private int objectsPerTransaction;
    /** název modelu pro kontrolu licence v rámci databáze. */
    private String moduleName;
    /** Styl prohledávání grafu. */
    private SearchOrder searchOrder = new SearchOrderDfs();
    /** Navštěvované objekty. */
    private List<VisitedPart> visitedParts = Collections.emptyList();

    @Override
    public void setVisitor(GraphVisitor visitor) {
        this.visitor = visitor;
    }

    @Override
    public void setRevisionInterval(RevisionInterval revisionInterval) {
        this.revisionInterval = revisionInterval;
    }

    @Override
    public void setIdsToProcess(Deque<Object> idsToProcess) {
        this.idsToProcess = idsToProcess;
    }

    @Override
    public void setObjectsPerTransaction(int objectsPerTransaction) {
        this.objectsPerTransaction = objectsPerTransaction;
    }

    @Override
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    @Override
    public void setSearchOrder(SearchOrder searchOrder) {
        this.searchOrder = searchOrder;
    }

    @Override
    public void setVisitedParts(List<VisitedPart> visitedParts) {
        if (visitedParts != null) {
            this.visitedParts = Collections.unmodifiableList(new ArrayList<>(visitedParts));
        } else {
            this.visitedParts = Collections.emptyList();
        }
    }

    /**
     * @return Visitor navštěvující jednotlivé elementy v grafu
     */
    protected GraphVisitor getVisitor() {
        return visitor;
    }

    /**
     * @return interval revizí, které procesor prochází
     */
    protected RevisionInterval getRevisionInterval() {
        return revisionInterval;
    }

    /**
     * @return fronta id vrcholů ke zpracování. 
     */
    protected Deque<Object> getIdsToProcess() {
        return idsToProcess;
    }

    /**
     * @return počet objektů ke zpracování v rámci této transakce. 
     */
    protected int getObjectsPerTransaction() {
        return objectsPerTransaction;
    }

    /**
     * @return Styl prohledávání grafu 
     */
    @Override
    public SearchOrder getSearchOrder() {
        return searchOrder;
    }

    /**
    * @return Navštěvované objekty 
    */
    public List<VisitedPart> getVisitedParts() {
        return visitedParts;
    }

    @Override
    public T callMe(TitanTransaction transaction) {
        if (getVisitor() instanceof TransactionAware) {
            ((TransactionAware) getVisitor()).setTransaction(transaction);
        }

        int objectsProcessed = 0;
        while (objectsProcessed < objectsPerTransaction
                && (getIdsToProcess() instanceof BlockingDeque<?> || !getIdsToProcess().isEmpty())) {
            Object vertexId = getSearchOrder().getNext(getIdsToProcess());
            if (vertexId == null) {
                break;
            }

            Vertex vertex = transaction.getVertex(vertexId);

            if (vertex != null) {
                processVertex(vertex);
            } else {
                LOGGER.warn("Vertex with id {} does not exist.", vertexId);
            }

            objectsProcessed++;
        }

        return null;
    }

    /**
     * Zpracuje jeden vrchol na základě jeho typu.
     * @param vertex vrchol ke zpracování
     */
    protected final void processVertex(Vertex vertex) {
        VertexType vertexType = VertexType.getType(vertex);
        switch (vertexType) {
        case ATTRIBUTE:
            processAttribute(vertex);
            break;
        case NODE:
            processNode(vertex);
            break;
        case RESOURCE:
            processResource(vertex);
            break;
        case SUPER_ROOT:
            processSuperRoot(vertex);
            break;
        case SOURCE_NODE:
        case SOURCE_ROOT:
        case REVISION_NODE:
        case REVISION_ROOT:
        case LAYER:
            LOGGER.warn("Traverser found {}, which is not expected. ", vertexType);
            break;
        default:
            throw new IllegalStateException("Unknown vertex type " + vertexType + ".");
        }

    }

    /**
     * Zpracuje super root a všechny jeho zdroje.
     * @param superRoot super root ke zpracování
     */
    protected void processSuperRoot(Vertex superRoot) {
        List<Vertex> children = GraphOperation.getAdjacentVertices(superRoot, Direction.IN, revisionInterval,
                EdgeLabel.HAS_RESOURCE);
        for (Vertex child : children) {
            idsToProcess.addLast(child.getId());
        }
    }

    /**
     * Navštíví resource a všechny k němu připojené uzly.
     * @param resource resource ke zpracování
     */
    protected abstract void processResource(Vertex resource);

    /**
     * Navštíví uzel, jeho atributy, hrany toků a rekurzivně všechny potomky.
     * @param node uzel ke zpracování
     */
    protected abstract void processNode(Vertex node);
    
    protected void processLayerOfResource(Vertex resource) {
        List<Vertex> layers = GraphOperation.getAdjacentVertices(resource, Direction.OUT, getRevisionInterval(),
                EdgeLabel.IN_LAYER);
        if (layers.isEmpty()) {
            LOGGER.warn("Resource with ID {} has no layer for revision {}.", resource.getId(), getRevisionInterval());
        } else if (layers.size() > 1) {
            LOGGER.warn("Resource with ID {} has more than one layer ({}) for revisions {}.", resource.getId(), layers.size(), getRevisionInterval());
        }
        for (Vertex layer : layers) {
            getVisitor().visitLayer(layer);
        }
    }

    /**
     * Navštíví atribut.
     * @param attribute atribut k navštívění
     */
    protected void processAttribute(Vertex attribute) {
        getVisitor().visitAttribute(attribute);
    }

    @Override
    public String getModuleName() {
        return moduleName;
    }
}
