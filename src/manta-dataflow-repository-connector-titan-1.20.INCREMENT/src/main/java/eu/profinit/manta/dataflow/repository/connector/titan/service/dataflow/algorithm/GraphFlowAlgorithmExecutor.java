package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.GraphFlowFilter;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Třída zajišťující spouštění algortmů.
 *
 * @author tfechtner
 */
public class GraphFlowAlgorithmExecutor {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(GraphFlowAlgorithmExecutor.class);
    
    /** Seznam továren algortimů, které se mají spustit. */
    private List<GraphFlowAlgorithmFactory> algorithmFactories;

    /**
     * @return Seznam továren algortimů, které se mají spustit.
     */
    public List<GraphFlowAlgorithmFactory> getAlgorithmFactories() {
        return Collections.unmodifiableList(algorithmFactories);
    }

    /**
     * @param algorithmFactories Seznam továren algortimů, které se mají spustit.
     */
    public void setAlgorithmFactories(List<GraphFlowAlgorithmFactory> algorithmFactories) {
        this.algorithmFactories = new ArrayList<GraphFlowAlgorithmFactory>(algorithmFactories);
    }

    /**
     * Nalezne dostupné vrcholy na základě zvolených algoritmů.
     * Algoritmy se spouštějí postupně sekvenčně, přičemž každý další pouze filtruje
     * výsledky předchozích.
     * 
     * @param startNodes startovní vrcholy
     * @param dbTransaction transakce pro práci s databází
     * @param direction směr hledání
     * @param isFilterEdges true, jestliže se smí chodit přes filtr hrany
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     * 
     * @return množina dostupných uzlů, nikdy null
     * @throws GraphFlowAlgorithmException chyba v prováděném algoritmu
     */
    public Set<Object> findAccessibleVertexIds(Collection<Vertex> startNodes, TitanTransaction dbTransaction,
        Direction direction, boolean isFilterEdges, RevisionInterval revisionInterval)
            throws GraphFlowAlgorithmException {
        
        return findAccessibleVertexIds(startNodes, dbTransaction, direction, isFilterEdges, null, revisionInterval);
    }
    
    /**
     * Nalezne dostupné vrcholy na základě zvolených algoritmů.
     * Algoritmy se spouštějí postupně sekvenčně, přičemž každý další pouze filtruje
     * výsledky předchozích.
     * 
     * @param startNodes startovní vrcholy
     * @param dbTransaction transakce pro práci s databází
     * @param direction směr hledání
     * @param isFilterEdges true, jestliže se smí chodit přes filtr hrany
     * @param additionalFilters filtry, které se mají v algoritmu použít (může být null)
     * 
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     * 
     * @return množina dostupných uzlů, nikdy null
     * @throws GraphFlowAlgorithmException chyba v prováděném algoritmu
     */
    public Set<Object> findAccessibleVertexIds(Collection<Vertex> startNodes, TitanTransaction dbTransaction,
            Direction direction, boolean isFilterEdges, List<GraphFlowFilter> additionalFilters,
            RevisionInterval revisionInterval)
            throws GraphFlowAlgorithmException {
        if (algorithmFactories == null) {
            LOGGER.error("Algorithm factory list is null.");
            return Collections.emptySet();
        }
        
        Collection<Vertex> readOnlyStartNodes = Collections.unmodifiableCollection(startNodes);

        Set<Object> allowedNodes = null;
        Set<Object> outputNodes = null;
        
        // Projdi vsechny graph flow algoritmy a jeden po druhem je spust.
        // Vystupni data jednoho algoritmu jsou povolene uzly nasledujiciho.
        for (GraphFlowAlgorithmFactory factory : algorithmFactories) {
            outputNodes = runAlgorithmFactory(factory, readOnlyStartNodes, allowedNodes, dbTransaction, direction,
                    isFilterEdges, additionalFilters, revisionInterval);
            allowedNodes = outputNodes;
        }

        return outputNodes;
    }

    /**
     * Nalezne dostupné vrcholy pomocí daného algoritmu.
     * @param factory továrna pro tvorbu algoritmu, který se použije
     * @param startNodes startovní vrcholy
     * @param dbTransaction transakce pro práci s databází
     * @param direction směr hledání
     * @param isFilterEdges true, jestliže se smí chodit přes filtr hrany
     * @param additionalFilters filtry, které se mají v algoritmu použít (může být null)
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     * @return množina dostupných uzlů, nikdy null
     * @throws GraphFlowAlgorithmException chyba v prováděném algoritmu
     */
    private Set<Object> runAlgorithmFactory(GraphFlowAlgorithmFactory factory, Collection<Vertex> readOnlyStartNodes,
            Set<Object> allowedNodes, TitanTransaction dbTransaction, Direction direction, boolean isFilterEdges,
            List<GraphFlowFilter> additionalFilters, RevisionInterval revisionInterval)
            throws GraphFlowAlgorithmException {

        Set<Object> outputNodes = new HashSet<Object>();

        if (direction == Direction.OUT || direction == Direction.BOTH) {
            GraphFlowAlgorithm algorithm = factory.createGraphFlowAlgorithm(isFilterEdges, additionalFilters);
            outputNodes.addAll(algorithm.findNodesByAlgorithm(readOnlyStartNodes, allowedNodes, dbTransaction,
                    Direction.OUT, revisionInterval));
        }

        if (direction == Direction.IN || direction == Direction.BOTH) {
            GraphFlowAlgorithm algorithm = factory.createGraphFlowAlgorithm(isFilterEdges, additionalFilters);
            outputNodes.addAll(algorithm.findNodesByAlgorithm(readOnlyStartNodes, allowedNodes, dbTransaction,
                    Direction.IN, revisionInterval));
        }

        return outputNodes;
    }
}
