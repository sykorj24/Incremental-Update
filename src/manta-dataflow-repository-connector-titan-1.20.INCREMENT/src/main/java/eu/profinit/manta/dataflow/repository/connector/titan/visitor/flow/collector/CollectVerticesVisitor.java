package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.collector;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterResult;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowItem;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowVisitor;

/**
 * Visitor sbírající uzly během procházení přes data flow. <br>
 * Pro rozhodnutí, jestli má být konkrétní uzel sebrán využívá {@link CollectingFilter}.
 * @author tfechtner
 *
 */
public class CollectVerticesVisitor implements FlowVisitor {
    /** Id sebraných uzlů, splňující filtraci. */
    private Set<Object> collectedVertexIds = new HashSet<Object>();
    /** Filtr pro určení, které uzly se mají sesbírat. */
    private CollectingFilter filter;

    /**
     * @param filter Filtr pro určení, které uzly se mají sesbírat. 
     */
    public CollectVerticesVisitor(CollectingFilter filter) {
        super();
        this.filter = filter;
    }

    @Override
    public FilterResult visitAndContinue(Vertex node, FlowItem flowItem) {
        if (filter.shouldCollect(node)) {
            collectedVertexIds.add(node.getId());
        }
        return FilterResult.OK_CONTINUE;
    }

    /**
     * @return id všech sesbíraných uzlů
     */
    public Set<Object> getCollectedVertexIds() {
        return Collections.unmodifiableSet(collectedVertexIds);
    }
}
