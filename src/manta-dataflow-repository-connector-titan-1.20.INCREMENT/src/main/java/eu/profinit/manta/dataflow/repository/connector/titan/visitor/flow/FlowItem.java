package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

/**
 * Trida prestavujici polozku ve FlowTraverser fronte.
 * 
 * <p>
 * Tato je urcena ciste pro ulozeni informaci o:
 * <ul>
 *  <li>hrane, po ktere se k uzlu doslo,</li>
 *  <li>ID uzlu</li>
 *  <li>genericka data, ktera visitori mohou pouzit k ukladani dalsich informaci 
 * stavu algoritmu, ktery implementuji.</li>
 * </ul>
 * 
 * 
 * @author Jan Milik <milikjan@fit.cvut.cz>
 */
public class FlowItem {
    // TODO Edge je dost velká struktura, bylo by třeba změnit na její id
    private final Edge incomingEdge;

    private final Long vertexId;

    private Map<Class<?>, FlowItemPayloadMember> payload;

    /**
     * @param incomingEdge příchozí hrana
     * @param vertexId id vrcholu
     */
    public FlowItem(Edge incomingEdge, Long vertexId) {
        this(incomingEdge, vertexId, null);
    }

    /**
     * @param incomingEdge příchozí hrana
     * @param vertexId id vrcholu
     * @param incomingItem příchozí flow item
     */
    public FlowItem(Edge incomingEdge, Long vertexId, FlowItem incomingItem) {
        super();
        this.incomingEdge = incomingEdge;
        this.vertexId = vertexId;

        if (incomingItem != null) {
            Map<Class<?>, FlowItemPayloadMember> incomingPayload = incomingItem.getPayload();
            if (incomingPayload != null) {
                this.payload = new HashMap<Class<?>, FlowItemPayloadMember>();

                Set<Entry<Class<?>, FlowItemPayloadMember>> entrySet = incomingPayload.entrySet();
                for (Entry<Class<?>, FlowItemPayloadMember> entry : entrySet) {
                    payload.put(entry.getKey(), entry.getValue().createDeepCopy());
                }
            }
        }
    }

    /**
     * Vrati hranu, pres kterou byl uzel reprezentovany touto
     * instanci dosazen. <b>Toto není "vstupní hrana" z hlediska
     * teorie grafů.</b> Toto je hrana, pres kterou traverser dosel k uzlu,
     * coz muze byt v zavislosti na argumentu {@code direction} metody
     * {@link FlowTraverser#traverse(TitanTransaction, Iterable, EdgeTypeTraversing, Direction)}
     * vstupní, <b>nebo</b> výstupní hrana.
     * 
     * <p>
     * <b>NB:</b> Muze vracet {@code null}, pokud tato polozka reprezentuje startovni uzel.
     * 
     * @return hrana vedouci do aktualniho uzlu, muze byt {@code null}
     */
    public Edge getIncomingEdge() {
        return incomingEdge;
    }

    /**
     * @return id vrcholu
     */
    public Object getVertexId() {
        return vertexId;
    }

    /**
     * Pridej typovana data k teto polozce.
     * 
     * <p>
     * Tato metoda existuje pouze pro to, aby implementatori
     * {@link FlowVisitor} interface mohli k prochazenym uzlum a
     * hranam pridavat data potrebna k svoji implementaci.
     * 
     * <p>
     * Data pridana k polozce touto metodou lze opet ziskat metodou
     * {@link #get(Class)}. Tyto dve metody se chovaji jako mapa,
     * ktera mapuje tridy na jejich instance. Ucelem tohoto reseni
     * je zajisteni typove bezpecnosti.
     * 
     * @param cls třída přidávané hodnoty
     * @param value přidávaná hodnoty
     * @return vrátí přidávanou hodnotu
     *  
     * @param <T> typ přidávané hodnoty
     * 
     * @see #get(Class) 
     */
    public <T extends FlowItemPayloadMember> T add(Class<T> cls, T value) {
        if (payload == null) {
            payload = new HashMap<Class<?>, FlowItemPayloadMember>();
        }

        payload.put(cls, value);

        return value;
    }

    /**
     * Vrati prilozena data dane tridy. Pro vice informaci viz {@link #add(Class, Object)}.
     * 
     * @param cls typ prilozenych dat, ktere maji byt vracena
     * @return prilozena data typu {@code cls}
     * 
     * @param <T> typ přidávané hodnoty
     * 
     * @see #add(Class, Object)
     */
    public <T> T get(Class<T> cls) {
        if (payload == null) {
            return null;
        }

        return cls.cast(payload.get(cls));
    }

    /**
     * @return data, která flowitem obsahuje
     */
    public Map<Class<?>, FlowItemPayloadMember> getPayload() {
        if (payload != null) {
            return Collections.unmodifiableMap(payload);
        } else {
            return null;
        }
    }

    @Override
    public int hashCode() {
        int result = 1;
        if (vertexId != null) {
            result *= vertexId.hashCode();
        }
        if (incomingEdge != null) {
            Vertex inVertex = incomingEdge.getVertex(Direction.IN);
            Vertex outVertex = incomingEdge.getVertex(Direction.OUT);
            result *= (inVertex.hashCode() * outVertex.hashCode());
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof FlowItem)) {
            return false;
        }
        FlowItem other = (FlowItem) obj;
        return !((vertexId != null && other.vertexId != null && !vertexId.equals(other.vertexId))
                || (incomingEdge != null && other.incomingEdge != null && !incomingEdge.equals(other.incomingEdge)));
    }
}
