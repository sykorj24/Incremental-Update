package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;

/**
 * Algoritmus pro odfiltrování vrcholů v datovém toku, ke kterým se jde dostat pouze přes více jak 
 * jednu filtr hranu.
 * @author tfechtner
 *
 */
public class FilterEdgeGraphFlowFilter implements GraphFlowFilter {

    @Override
    public FilterResult testVertex(Vertex vertex, Edge incomingEdge) {
        if (incomingEdge != null && incomingEdge.getLabel().equals(EdgeLabel.FILTER.t())) {
            return FilterResult.OK_STOP;
        } else {
            return FilterResult.OK_CONTINUE;
        }
    }

}
