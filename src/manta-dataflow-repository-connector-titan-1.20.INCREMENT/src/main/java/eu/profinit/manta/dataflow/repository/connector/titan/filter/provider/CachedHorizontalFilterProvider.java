package eu.profinit.manta.dataflow.repository.connector.titan.filter.provider;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilter;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.platform.cache.CustomEhcacheFactory;
import eu.profinit.manta.platform.cache.CustomEhcacheFactory.CacheCreationException;
import eu.profinit.manta.platform.cache.EhcacheWrapper;

/**
 * Decorater provider horizontálních filtrů, který cachuje výsledky vnitřního provideru.
 * @author tfechtner
 *
 */
public class CachedHorizontalFilterProvider implements HorizontalFilterProvider {
    /** Název cache pro vysledky filtru. */
    private static final String CACHE_NAME = "VERTEX_FILTER_CACHE";
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(CachedHorizontalFilterProvider.class);

    /** Vnitřní provider, který je dekorován. */
    private HorizontalFilterProvider innerProvider;
    /** Ehcache, pro cache výsledků. */
    private EhcacheWrapper<VertexFilterId, Boolean> ehCache;
    /** Továrna na tvorbu cache. */
    private CustomEhcacheFactory ehCacheFactory;
    /** Maximální počet uchovávaných výsledků. */
    private int maxCachedResults;

    /**
     * Inicializace cache.
     */
    public void init() {
        EhcacheWrapper<VertexFilterId, Boolean> tempCacheVariable = null;
        try {
            ehCacheFactory.insertIfNotContains(CACHE_NAME, maxCachedResults, true, 0);
            tempCacheVariable = ehCacheFactory.createCacheWrapper(CACHE_NAME, VertexFilterId.class, Boolean.class);
        } catch (CacheCreationException e) {
            LOGGER.error("Cannot create cache for filtering vertices, using direct access instead.", e);
        }
        ehCache = tempCacheVariable;
    }

    @Override
    public HorizontalFilter getFilter(String filterId) {
        return innerProvider.getFilter(filterId);
    }

    @Override
    public boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval, Set<String> activeFilters) {
        for (String filterId : activeFilters) {
            if (isFiltered(vertex, revisionInterval, filterId)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isFiltered(Vertex vertex, RevisionInterval revisionInterval, String filterId) {
        if (ehCache != null) {
            VertexFilterId key = new VertexFilterId((Long) vertex.getId(), revisionInterval, filterId);
            Boolean result = ehCache.get(key);
            if (result == null) {
                result = innerProvider.isFiltered(vertex, revisionInterval, filterId);
                ehCache.put(key, result);
            }
            return result;
        } else {
            return innerProvider.isFiltered(vertex, revisionInterval, filterId);
        }
    }

    @Override
    public void addFilters(Set<HorizontalFilter> filterSet) {
        innerProvider.addFilters(filterSet);
    }

    @Override
    public List<HorizontalFilter> getFilters() {
        return innerProvider.getFilters();
    }

    @Override
    public int refreshFilters() {
        ehCache.getCache().removeAll();
        return innerProvider.refreshFilters();
    }

    /**
     * @param innerProvider vnitřní provider, který je dekorován
     */
    public void setInnerProvider(HorizontalFilterProvider innerProvider) {
        this.innerProvider = innerProvider;
    }

    /**
     * @param ehCacheFactory Továrna na tvorbu cache
     */
    public void setEhCacheFactory(CustomEhcacheFactory ehCacheFactory) {
        this.ehCacheFactory = ehCacheFactory;
    }

    /**
     * @param maxCachedResults Maximální počet uchovávaných výsledků
     */
    public void setMaxCachedResults(int maxCachedResults) {
        this.maxCachedResults = maxCachedResults;
    }
}
