package eu.profinit.manta.dataflow.repository.connector.titan.service;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.connection.SimpleDatabaseHolder;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.provider.FileHorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseConfiguration;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.platform.licensing.LicenseHolder;
import eu.profinit.manta.platform.licensing.LicenseLoaderImpl;
import eu.profinit.manta.platform.scriptmetadata.service.ScriptMetadataService;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;

/**
 * Předek pro testy, umožňující práci s titan db.
 * @author tfechtner
 * @author jsykora
 *
 */
public abstract class TestTitanDatabaseProvider {
    protected static final Logger LOGGER = LoggerFactory.getLogger(TestTitanDatabaseProvider.class);
    private NumberFormat formatter = new DecimalFormat("#0.000000");
    
    // uzly +super root + vrstva + revision root + source root + dve revize
    private static final int NODE_COUNT = 16;

    /** super root, revision root, source root, technical revision 0.000000 */
    public static final int INIT_GRAPH_VERTEX_COUNT = 4;
    /** The number of layers */
    public static final int LAYER_COUNT = 1;

    public static final String DEFAULT_DB_DIR = "target/db";
   
    
    /** Revision numbers frequently used during testing */
    
    /** Revision number 0.000001 */
    public static final Double REVISION_0_000001 = 0.000001d;
    /** Revision number 0.000002 */
    public static final Double REVISION_0_000002 = 0.000002d;
    /** Revision number 1.000000 */
    public static final Double REVISION_1_000000 = 1.000000d;
    /** Revision number 1.000001 */
    public static final Double REVISION_1_000001 = 1.000001d; 
    /** Revision number 1.000002 */
    public static final Double REVISION_1_000002 = 1.000002d;
    /** Revision number 1.999999 */
    public static final Double REVISION_1_999999 = 1.999999d;
    /** Revision number 2.000000 */
    public static final Double REVISION_2_000000 = 2.000000d;
    /** Revision number 2.999999 */
    public static final Double REVISION_2_999999 = 2.999999d;
    /** Revision number 3.000000 */
    public static final Double REVISION_3_000000 = 3.000000d;
    /** Revision number 3.999999 */
    public static final Double REVISION_3_999999 = 3.999999d;
    /** Revision number 4.000000 */
    public static final Double REVISION_4_000000 = 4.000000d;
    /** Revision number 4.000000 */
    public static final Double REVISION_5_000000 = 5.000000d;
    /** Revision number 6.000000 */
    public static final Double REVISION_6_000000 = 6.000000d;
    
    /** Revision intervals frequently used during testing */
    
    /** All possible revision intervals: <0.000000, max> */
    public static final RevisionInterval EVERY_REVISION_INTERVAL = new RevisionInterval(RevisionRootHandler.TECHNICAL_REVISION_NUMBER, RevisionRootHandler.REVISION_MAX_NUMBER);
    /** Revision interval <1.000000, 1.000000> */
    public static final RevisionInterval REVISION_INTERVAL_1_000000_1_000000 = new RevisionInterval(REVISION_1_000000, REVISION_1_000000);
    /** Revision interval <1.000000, 1.999999> */
    public static final RevisionInterval REVISION_INTERVAL_1_000000_1_999999 = new RevisionInterval(REVISION_1_000000, REVISION_1_999999);
    /** Revision interval <1.000000, 2.999999> */
    public static final RevisionInterval REVISION_INTERVAL_1_000000_2_999999 = new RevisionInterval(REVISION_1_000000, REVISION_2_999999);
    /** Revision interval <1.000000, 3.000000> */
    public static final RevisionInterval REVISION_INTERVAL_1_000000_3_000000 = new RevisionInterval(REVISION_1_000000, REVISION_3_000000);
    /** Revision interval <2.000000, 2.000000> */
    public static final RevisionInterval REVISION_INTERVAL_2_000000_2_000000 = new RevisionInterval(REVISION_2_000000, REVISION_2_000000);
    /** Revision interval <2.000000, 2.999999> */
    public static final RevisionInterval REVISION_INTERVAL_2_000000_2_999999 = new RevisionInterval(REVISION_2_000000, REVISION_2_999999);
    /** Revision interval <2.000000, 3.000000> */
    public static final RevisionInterval REVISION_INTERVAL_2_000000_3_000000 = new RevisionInterval(REVISION_2_000000, REVISION_3_000000);
    /** Revision interval <3.000000, 3.000000> */
    public static final RevisionInterval REVISION_INTERVAL_3_000000_3_000000 = new RevisionInterval(REVISION_3_000000, REVISION_3_000000);
    /** Revision interval <3.000000, 3.999999> */
    public static final RevisionInterval REVISION_INTERVAL_3_000000_3_999999 = new RevisionInterval(REVISION_3_000000, REVISION_3_999999);
    /** Interval for revision comparison  */
    public static final RevisionInterval REVISION_COMPARE_INTERVAL = new RevisionInterval(REVISION_1_000000, REVISION_3_999999);
    
    /** Revision interval <1.000000, 1.999999> */
    public static final RevisionInterval REVISION_INTERVAL_1_0 = new RevisionInterval(REVISION_1_000000);
    

    /**
     * SUPER ROOT & REVISION ROOT + prvni revize.
     */
    public static final int INITIAL_NODE_COUNT = 3;

    private final LicenseHolder licenseHolder;
    private final SimpleDatabaseHolder databaseHolder;
    private final SuperRootHandler superRootHandler = new SuperRootHandler();
    private final RevisionRootHandler revisionRootHandler = new RevisionRootHandler();
    private final SourceRootHandler sourceRootHandler = new SourceRootHandler();
    private final UsageStatsCollector usageStatsCollector = new DummyUsageStatsCollector();
    private final TechnicalAttributesHolder terchnicalAttributes = new DummyTechnicalAttributesHolder();
    private FileHorizontalFilterProvider horizontalFilterProvider;

    /**
     * Defaultní konstruktor použije pro umístění db {@link #DEFAULT_DB_DIR}.
     */
    public TestTitanDatabaseProvider() {
        this(DEFAULT_DB_DIR, true);
    }

    /**
     * @param dir adresář s db
     * @param cleanDir true, jestli se má adresář nejprve vymazat
     */
    public TestTitanDatabaseProvider(String dir, boolean cleanDir) {
        databaseHolder = new SimpleDatabaseHolder();
        DatabaseConfiguration configuration = new DatabaseConfiguration();
        configuration.setHasNodeNameIndex(true);
        databaseHolder.setConfiguration(configuration);
        databaseHolder.setDir(dir);
        licenseHolder = new LicenseHolder(2);
        licenseHolder.loadLicense(new LicenseLoaderImpl().loadFromProperty());
        databaseHolder.setLicenseHolder(licenseHolder);
        databaseHolder.setScriptMetadataService(new DummyScriptMetadataService());
        databaseHolder.init();

        sourceRootHandler.setSourceCodeDir(new File("target/sourcecode"));

        if (cleanDir) {
            databaseHolder.truncateDatabase();
        }

        revisionRootHandler.setScriptMetadataService(new DummyScriptMetadataService());
        revisionRootHandler.setLicenseHolder(licenseHolder);

        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());

        horizontalFilterProvider = new FileHorizontalFilterProvider();
        horizontalFilterProvider.setSuperRootHandler(getSuperRootHandler());
        horizontalFilterProvider.setDatabaseHolder(getDatabaseHolder());
        horizontalFilterProvider.refreshFilters();
    }

    /**
     * Create basic graph and commit it.
     */
    public void createBasicGraph() {
        createBasicGraph(true);
    }

    /**
     * Create basic graph.
     * All vertices and data flows have revision validity set to <1.000000, 1.999999>.
     * The latest committed revision is either 0.000000 or 1.000000 (depending on the input parameter commitRevision).
     * 
     * 
     * revisions:    0.000000 (committed)
     *               1.000000 (committed/uncommitted)
     * 
     * 
     *      super root 
     *          |                     
     * <1.000000, 1.999999>                
     *       resource ---------------->layer
     *          |
     * <1.000000, 1.999999>
     *       database
     *          |
     *          +-----------------------------------------------+
     *          |                                               |
     * <1.000000, 1.999999>   <1.000000, 1.999999>     <1.000000, 1.999999>   <1.000000, 1.999999>
     *        table1----------->tableType(TABLE)             table2------------->tableType(VIEW)
     *          |                                               |                        
     *          +------------------------+                      +-------------------------+         
     *          |                        |                      |                         |
     * <1.000000, 1.999999>     <1.000000, 1.999999>   <1.000000, 1.999999>      <1.000000, 1.999999>
     *        t1c1                      t1c2                   t2c1--+                  t2c2
     *         | |                      A |                    A A   |                    A 
     *         | | <1.000000, 1.999999> | |                    | |   |<1.000000, 1.999999>| 
     *         | +---------direct-------+ |<1.000000, 1.999999>| |   +-------direct-------+ 
     *         |                          +--------direct------+ |   
     *         |                                                 |
     *         |           <1.000000, 1.999999>                  |   
     *         +------------------filter-------------------------+   
     *    
     * @param commitRevision  true, when the revision 1.000000 should be committed
     */
    public void createBasicGraph(final boolean commitRevision) {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        databaseHolder.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = superRootHandler.ensureRootExistance(transaction);

                revisionRootHandler.createMajorRevision(transaction);

                Vertex teradataLayer = GraphCreation.createLayer(transaction, "Physical", "Physical");

                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "Teradata",
                        "Description", teradataLayer, REVISION_1_000000);

                Vertex db = GraphCreation.createNode(transaction, teradataRes, "db", "Database", REVISION_1_000000);

                Vertex table1 = GraphCreation.createNode(transaction, db, "table1", "Table", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, table1, "TABLE_TYPE", "TABLE", REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                Vertex t1c2 = GraphCreation.createNode(transaction, table1, "t1c2", "Column", REVISION_1_000000);

                Vertex table2 = GraphCreation.createNode(transaction, db, "table2", "Table", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, table2, "TABLE_TYPE", "VIEW", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
                Vertex t2c2 = GraphCreation.createNode(transaction, table2, "t2c2", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t1c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c2, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t2c2, EdgeLabel.DIRECT, REVISION_1_000000);

                if (commitRevision) {
                    revisionRootHandler.commitRevision(transaction, REVISION_1_000000);
                }
                LOGGER.info("Latest committed revision in root after revision committed: {}", 
                        formatter.format(revisionRootHandler.getLatestCommittedRevisionNumber(transaction)));
                return null;
            }
        });
    }

    
    public Map<String, Long> createBasicComapringGraph() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        return databaseHolder.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Map<String, Long>>() {

            @Override
            public Map<String, Long> callMe(TitanTransaction transaction) {
                Vertex root = superRootHandler.ensureRootExistance(transaction);

                Map<String, Long> ids = new HashMap<>();
                ids.putAll(rev1(transaction, root));
                ids.putAll(rev2(transaction, root, ids));
                ids.putAll(rev3(transaction, root, ids));
                return ids;
            }

            private Map<String, Long> rev1(TitanTransaction transaction, Vertex root) {
                Double rev = revisionRootHandler.createMajorRevision(transaction);

                Vertex teradataLayer = GraphCreation.createLayer(transaction, "Physical", "Physical");

                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "Teradata",
                        "Description", teradataLayer, REVISION_COMPARE_INTERVAL);
                Vertex db = GraphCreation.createNode(transaction, teradataRes, "db", "Database",
                        REVISION_COMPARE_INTERVAL);

                Vertex table1 = GraphCreation.createNode(transaction, db, "table1", "Table", REVISION_COMPARE_INTERVAL);
                GraphCreation.createNodeAttribute(transaction, table1, "TABLE_TYPE", "TABLE",
                        REVISION_COMPARE_INTERVAL);
                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", rev);
                Vertex t1c2 = GraphCreation.createNode(transaction, table1, "t1c2", "Column",
                        REVISION_COMPARE_INTERVAL);

                Vertex table2 = GraphCreation.createNode(transaction, db, "table2", "Table", REVISION_COMPARE_INTERVAL);
                GraphCreation.createNodeAttribute(transaction, table2, "TABLE_TYPE", "VIEW", REVISION_COMPARE_INTERVAL);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", rev);
                Vertex t2c2 = GraphCreation.createNode(transaction, table2, "t2c2", "Column",
                        REVISION_COMPARE_INTERVAL);

                Vertex table3 = GraphCreation.createNode(transaction, db, "table3", "Table", REVISION_COMPARE_INTERVAL);
                GraphCreation.createNodeAttribute(transaction, table3, "TABLE_TYPE", "VIEW", REVISION_COMPARE_INTERVAL);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column",
                        REVISION_COMPARE_INTERVAL);
                Vertex t3c2 = GraphCreation.createNode(transaction, table3, "t3c2", "Column",
                        REVISION_COMPARE_INTERVAL);

                Vertex table4 = GraphCreation.createNode(transaction, db, "table4", "Table", REVISION_COMPARE_INTERVAL);
                GraphCreation.createNodeAttribute(transaction, table4, "TABLE_TYPE", "VIEW", REVISION_COMPARE_INTERVAL);
                Vertex t4c1 = GraphCreation.createNode(transaction, table4, "t4c1", "Column",
                        REVISION_COMPARE_INTERVAL);

                GraphCreation.createEdge(t1c1, t1c2, EdgeLabel.DIRECT, rev);
                GraphCreation.createEdge(t1c2, t2c1, EdgeLabel.DIRECT, rev);
                GraphCreation.createEdge(t2c1, t2c2, EdgeLabel.DIRECT, rev);
                GraphCreation.createEdge(t2c2, t3c1, EdgeLabel.DIRECT, REVISION_COMPARE_INTERVAL);
                GraphCreation.createEdge(t3c1, t3c2, EdgeLabel.DIRECT, REVISION_COMPARE_INTERVAL);
                GraphCreation.createEdge(t3c2, t4c1, EdgeLabel.FILTER, rev);

                revisionRootHandler.commitRevision(transaction, rev);

                Map<String, Long> ids = new HashMap<>();
                ids.put("Teradata", (Long) teradataRes.getId());
                ids.put("db", (Long) db.getId());
                ids.put("table1", (Long) table1.getId());
                ids.put("t1c2", (Long) t1c2.getId());
                ids.put("table2", (Long) table2.getId());
                ids.put("t2c2", (Long) t2c2.getId());
                ids.put("table3", (Long) table3.getId());
                ids.put("t3c1", (Long) t3c1.getId());
                ids.put("t3c2", (Long) t3c2.getId());
                ids.put("table4", (Long) table4.getId());
                ids.put("t4c1", (Long) t4c1.getId());

                ids.put("t1c1r1", (Long) t1c1.getId());
                ids.put("t2c1r1", (Long) t2c1.getId());
                return ids;
            }

            private Map<String, Long> rev2(TitanTransaction transaction, Vertex root, Map<String, Long> ids) {
                Double rev = revisionRootHandler.createMajorRevision(transaction);
                revisionRootHandler.commitRevision(transaction, rev);

                Vertex table3 = transaction.getVertex(ids.get("table3"));
                Vertex t3c3 = GraphCreation.createNode(transaction, table3, "t3c3", "Column", rev);

                Vertex t3c2 = transaction.getVertex(ids.get("t3c2"));
                GraphCreation.createEdge(t3c2, t3c3, EdgeLabel.DIRECT, rev);

                Map<String, Long> newIds = new HashMap<>();
                newIds.put("t3c3r2", (Long) t3c3.getId());
                return newIds;
            }

            private Map<String, Long> rev3(TitanTransaction transaction, Vertex root, Map<String, Long> ids) {
                Double rev = revisionRootHandler.createMajorRevision(transaction);

                Vertex table1 = transaction.getVertex(ids.get("table1"));
                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", rev);

                Vertex t1c2 = transaction.getVertex(ids.get("t1c2"));
                Vertex t2c2 = transaction.getVertex(ids.get("t2c2"));
                GraphCreation.createEdge(t1c1, t1c2, EdgeLabel.DIRECT, rev);
                GraphCreation.createEdge(t1c2, t2c2, EdgeLabel.DIRECT, rev);
                revisionRootHandler.commitRevision(transaction, rev);

                Map<String, Long> newIds = new HashMap<>();
                newIds.put("t1c1r3", (Long) t1c1.getId());
                return newIds;
            }
        });
    }
    

    
    /**
     * Creates basic graph extended of the revision 2.000000.
     * 
     * revisions:    0.000000 (committed)
     *               1.000000 (committed)
     *               2.000000 (committed)
     * 
     * 
     *      super root              
     *          |                     
     * <1.000000, 2.999999>                
     *       resource ---------------->layer
     *          |
     * <1.000000, 2.999999>
     *       database
     *          |
     *          +---------------------------------------------+-----------------------------------------------+
     *          |                                             |                                               |
     * <1.000000, 2.999999>   <1.000000, 2.999999>   <1.000000, 2.999999>   <1.000000, 2.999999>     <2.000000, 2.999999>   <2.000000, 2.999999>
     *        table1------------>tableType(TABLE)          table2------------->tableType(VIEW)             table3------------->tableType(TABLE)
     *          |                                             |                                               |
     *          +---------------------+                       +------------------------+                      +------------------------+
     *          |                     |                       |                        |                      |                        |
     * <1.000000, 2.999999>  <1.000000, 2.999999>    <1.000000, 2.999999>     <1.000000, 1.999999>   <2.000000, 2.999999>     <2.000000, 2.999999>
     *        t1c1                  t1c2                    t2c1--+                  t2c2                   t3c1                      t3c2
     *        | |                    A |                    A A   |                    A                      A |                      A
     *        | |<1.000000, 2.999999>| |                    | |   |<1.000000, 1.999999>|                      | | <2.000000, 2.999999> |
     *        | +-------direct-------+ |<1.000000, 1.999999>| |   +--------direct------+                      | +---------filter-------+
     *        |                        +--------direct------+ |   |                                           |
     *        |         <1.000000, 2.999999>                  |   |               <2.000000, 2.999999>        |
     *        +----------------filter-------------------------+   +----------------------direct---------------+
     */
    public void createExtendedGraph() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        databaseHolder.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                // Create major revision 1 (revision 1.000000)
                getRevisionRootHandler().createMajorRevision(transaction);
                
                // Add vertices and edges to the new revision 1.000000 (i.e. tranStart=1.000000, tranEnd=1.999999)
                Vertex root = superRootHandler.ensureRootExistance(transaction);

                Vertex teradataLayer = GraphCreation.createLayer(transaction, "Physical", "Physical");

                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "Teradata",
                        "Description", teradataLayer, REVISION_1_000000);
                Vertex db = GraphCreation.createNode(transaction, teradataRes, "db", "Database", REVISION_1_000000);

                Vertex table1 = GraphCreation.createNode(transaction, db, "table1", "Table", REVISION_1_000000);
                Vertex table1Type = GraphCreation.createNodeAttribute(transaction, table1, "TABLE_TYPE", "TABLE",
                        REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                Vertex t1c2 = GraphCreation.createNode(transaction, table1, "t1c2", "Column", REVISION_1_000000);

                Vertex table2 = GraphCreation.createNode(transaction, db, "table2", "Table", REVISION_1_000000);
                Vertex table2Type = GraphCreation.createNodeAttribute(transaction, table2, "TABLE_TYPE", "VIEW",
                        REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
                Vertex t2c2 = GraphCreation.createNode(transaction, table2, "t2c2", "Column", REVISION_1_000000);

                Edge t1c1ToT1c2 = GraphCreation.createEdge(t1c1, t1c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c2, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                Edge t1c1ToT2c1 = GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t2c2, EdgeLabel.DIRECT, REVISION_1_000000);

                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);
                
                // Create major revision 2 (revision 2.000000)
                getRevisionRootHandler().createMajorRevision(transaction);
                
                // Vertices and edges remaining in the revision 2.000000 need to set their tranEnd to 2.999999
                // Removed vertices and edges keep their tranEnd at 1.999999
                RevisionUtils.setVertexTransactionEnd(teradataRes, REVISION_2_999999);
                RevisionUtils.setVertexTransactionEnd(db, REVISION_2_999999);

                RevisionUtils.setVertexTransactionEnd(table1, REVISION_2_999999);
                RevisionUtils.setVertexTransactionEnd(table1Type, REVISION_2_999999);
                RevisionUtils.setVertexTransactionEnd(t1c1, REVISION_2_999999);
                RevisionUtils.setVertexTransactionEnd(t1c2, REVISION_2_999999);

                RevisionUtils.setVertexTransactionEnd(table2, REVISION_2_999999);
                RevisionUtils.setVertexTransactionEnd(table2Type, REVISION_2_999999);
                RevisionUtils.setVertexTransactionEnd(t2c1, REVISION_2_999999);
                // Removed in the revision 2.000000
                // RevisionUtils.incrementNodeTransactionEnd(t2c2, REVISION_1);

                RevisionUtils.setEdgeTransactionEnd(t1c1ToT1c2, REVISION_2_999999);
                // Removed in the revision 2.000000
                // RevisionUtils.incrementEdgeTransactionEnd(t1c2_t2c1);
                RevisionUtils.setEdgeTransactionEnd(t1c1ToT2c1, REVISION_2_999999);

                // Removed in the revision 2.000000 
                // RevisionUtils.incrementEdgeTransactionEnd(t2c1_t2c2);

                // New vertices and edges in the revision 2.000000

                Vertex table3 = GraphCreation.createNode(transaction, db, "table3", "Table", REVISION_2_000000);
                GraphCreation.createNodeAttribute(transaction, table3, "TABLE_TYPE", "TABLE", REVISION_2_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_2_000000);
                Vertex t3c2 = GraphCreation.createNode(transaction, table3, "t3c2", "Column", REVISION_2_000000);

                GraphCreation.createEdge(t3c1, t3c2, EdgeLabel.FILTER, REVISION_2_000000);
                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.DIRECT, REVISION_2_000000);

                //GraphTransactionEnding.endEdge(t1c2_t2c1, REVISION_1);
                //GraphTransactionEnding.endNode(t2c2, REVISION_1);

                getRevisionRootHandler().commitRevision(transaction, REVISION_2_000000);
                
                return null;
            }
        });
    }

    /**
     * @return počet uzlů v základnim grafu
     */
    public int getBasicGraphVertexCount() {
        return NODE_COUNT;
    }

    /**
     * Vymazat všechny uzly a hrany. 
     */
    public void cleanGraph() {
        databaseHolder.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Iterable<Vertex> vIterable = transaction.getVertices();
                for (Vertex v : vIterable) {
                    transaction.removeVertex(v);
                }
                return null;
            }
        });
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
    }
    
    public LicenseHolder getLicenseHolder() {
        return licenseHolder;
    }

    /**
     * @return service pro práci s db
     */
    public SimpleDatabaseHolder getDatabaseHolder() {
        return databaseHolder;
    }

    /**
     * @return držák na super root
     */
    public SuperRootHandler getSuperRootHandler() {
        return superRootHandler;
    }

    /**
     * @return držák na revision root
     */
    public RevisionRootHandler getRevisionRootHandler() {
        return revisionRootHandler;
    }

    /**
     * @return drák na source root
     */
    public SourceRootHandler getSourceRootHandler() {
        return sourceRootHandler;
    }

    public FileHorizontalFilterProvider getHorizontalFilterProvider() {
        return horizontalFilterProvider;
    }

    /**
     * @return sběrač statistik použití
     */
    public UsageStatsCollector getUsageStatsCollector() {
        return usageStatsCollector;
    }

    /**
     * @return držák technický atributů
     */
    public TechnicalAttributesHolder getTerchnicalAttributes() {
        return terchnicalAttributes;
    }

    /**
     * Vrací seznam všech uzlů viditelných v dané transakci.
     * @param transaction zkoumaná transakce
     * @return počet uzlů
     */
    public int getVertexCount(TitanTransaction transaction) {
        Iterable<Vertex> vIterable = transaction.getVertices();
        int count = 0;
//        TestTitanDatabaseProvider.LOGGER.info("########## Printing graph -- start ##########");
        for (@SuppressWarnings("unused")
        Vertex v : vIterable) {
//            TestTitanDatabaseProvider.LOGGER.info("[{}]: {}({})", VertexType.getType(v), GraphOperation.getName(v), GraphOperation.getType(v));
            count++;
        }
//        TestTitanDatabaseProvider.LOGGER.info("########## Printing graph -- end ##########");
        return count;
    }

    /**
     * Získá seznam jmen vertexů. 
     * @param vertices uzly ke zjištění
     * @return seznam jmen, nikdy null
     */
    public Set<String> getVertexNames(Iterable<Vertex> vertices) {
        Set<String> nameSet = new HashSet<String>();
        if (vertices != null) {
            for (Vertex v : vertices) {
                String name = v.getProperty(NodeProperty.NODE_NAME.t());
                if (name != null) {
                    nameSet.add(name);
                }
            }
        }
        return nameSet;
    }

    /**
     * Vypise dve kolekce retezcu ve dvou sloupcich vedle sebe. Kolekce
     * nemusi mit stejny pocet prvku. Sirka prvniho sloupce je rovna delce nejdelsiho
     * retezce v prvnim sloupci.
     * 
     * @param first Kolekce retezcu pro prvni sloupec.
     * @param second Kolekce retezcu pro druhy sloupec.
     */
    public void printInTwoColumns(Collection<String> first, Collection<String> second) {
        List<String> firstList = new ArrayList<String>(first);
        List<String> secondList = new ArrayList<String>(second);

        Collections.sort(firstList);
        Collections.sort(secondList);

        Iterator<String> iterator1 = firstList.iterator();
        Iterator<String> iterator2 = secondList.iterator();

        int columnWidth = 0;
        for (String s : first) {
            if (s.length() > columnWidth) {
                columnWidth = s.length();
            }
        }

        while (iterator1.hasNext() && iterator2.hasNext()) {
            String a = iterator1.next();
            String b = iterator2.next();

            System.out.print(a);
            for (int i = a.length(); i < columnWidth; i++) {
                System.out.print(" ");
            }
            System.out.print(" - ");
            System.out.println(b);
        }

        while (iterator1.hasNext()) {
            String c = iterator1.next();
            System.out.println(c);
        }

        while (iterator2.hasNext()) {
            String c = iterator2.next();
            for (int i = 0; i < columnWidth; i++) {
                System.out.print(" ");
            }
            System.out.print("   ");
            System.out.println(c);
        }
    }

    /**
     * Vytvoří novou revizi.
     * @return číslo nové revize
     */
    protected Double createMajorRevision() {
        return getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Double>() {
            @Override
            public Double callMe(TitanTransaction transaction) {
                return getRevisionRootHandler().createMajorRevision(transaction);
            }
        });
    }

    /**
     * Commitne revizi s daným číslem.
     * @param revision číslo revize ke commitu
     */
    protected void commitRevision(final Double revision) {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Integer>() {
            @Override
            public Integer callMe(TitanTransaction transaction) {
                getRevisionRootHandler().commitRevision(transaction, revision);
                return null;
            }
        });
    }

    /**
     * Přeloží idéčka vrcholů na jejich názvy.
     * @param idSet množin id vrcholů
     * @param transaction transakce pro přístup k db
     * @return množina názvů vrcholů, nikdy null
     */
    public Set<String> translateIdsToNames(Set<Object> idSet, TitanTransaction transaction) {
        Set<String> nameSet = new HashSet<String>();
        for (Object id : idSet) {
            Vertex v = transaction.getVertex(id);
            if (v != null) {
                nameSet.add(GraphOperation.getName(v));
            }
        }
        return nameSet;
    }

    /**
     * Přeloží množinu vrcholů na jejich názvy.
     * @param vertexSet množina vrcholů
     * @return množina názvů vrcholů, nikdy null
     */
    public Set<String> translateVerticesToNames(Collection<Vertex> vertexSet) {
        Set<String> nameSet = new HashSet<String>();
        for (Vertex v : vertexSet) {
            nameSet.add(GraphOperation.getName(v));
        }
        return nameSet;
    }

    /**
     * Převede pole na množinu.
     * @param items pole k převedení
     * @return nová množina, nikdy null
     * @param <T> typ prvku
     */
    @SafeVarargs
    public final <T> Set<T> createSet(T... items) {
        return new HashSet<>(Arrays.asList(items));
    }

    /**
     * Callback pro testy.
     * @author tfechtner
     *
     * @param <T> typ odpovedi
     */
    protected abstract static class TestCallback<T> implements TransactionCallback<T> {
        @Override
        public String getModuleName() {
            return "mr_basic";
        }
    }

    /**
     * Dummy holder obsahujici transakcni atributy.
     * @author tfechtner
     *
     */
    public static class DummyTechnicalAttributesHolder implements TechnicalAttributesHolder {

        private static final List<String> TRAN_ATTRIBUTES = Arrays.asList("tranStart", "tranEnd", "targetId", "interpolated");

        @Override
        public boolean isNodeAttributeTechnical(String attr) {
            return TRAN_ATTRIBUTES.contains(attr);
        }

        @Override
        public boolean isEdgeAttributeTechnical(String attr) {
            return TRAN_ATTRIBUTES.contains(attr);
        }

    }

    /**
     * Dummy stats collector, který nic nedělá.
     * @author tfechtner
     *
     */
    private static class DummyUsageStatsCollector implements UsageStatsCollector {

        @Override
        public void saveAction(String user, String action) {
            // NOOP
        }

        @Override
        public void saveAction(String user, String action, Map<String, Object> params) {
            // NOOP            
        }

        @Override
        public void saveAction(String user, String action, Object param) {
            // NOOP
        }

    }
    
    /**
     * Dummy implementace {@link ScriptMetadataService}.
     * 
     * @author ONouza
     *
     */
    public static class DummyScriptMetadataService implements ScriptMetadataService {

        @Override
        public int countCommittedScripts(int days,
                Transformer<Collection<String>, Collection<String>> foundHashesTransformer) {
            return 0;
        }

        @Override
        public void storeScripts(Collection<String> scriptHashes) {
            // nic nedela
        }

        @Override
        public int commitScripts(int limit, int days,
                Transformer<Collection<String>, Collection<String>> foundHashesTransformer) {
            return 0;
        }

        @Override
        public int revertUncommittedScripts() {
            return 0;
        }
    }

}
