package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Algoritmus procházející graf stylem do hloubky.
 * Zpracování jednoho objektu se provede dříve než zpracování jeho potomků a objektů se ho týkajícího.
 * Nejprve se zpracují všechny uzly a až pak se znovu projde graf a navštěvují se hrany (direct a filter).
 * @author tfechtner
 *
 */
public class GraphTraverserDfsPrefix extends AbstractGraphTraverser {
    
    @Override
    protected void processResource(GraphVisitor visitor, Vertex resource, RevisionInterval revisionInterval) {
        visitor.visitResource(resource);
        processResourceChildren(visitor, resource, revisionInterval);
    }
    
    @Override
    protected void processNode(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval) {
        visitor.visitNode(node);
        processNodeChildren(visitor, node, revisionInterval);        
        processNodeAttributes(visitor, node, revisionInterval);
    }
    
    @Override
    protected void processNodeEdges(GraphVisitor visitor, Vertex node, RevisionInterval revisionInterval) {
        processNodeDirectEdges(visitor, node, revisionInterval);
        processNodeFilterEdges(visitor, node, revisionInterval);
        processNodeMapsToEdges(visitor, node, revisionInterval);
        processChildrenNodesEdges(visitor, node, revisionInterval);
    }
}
