package eu.profinit.manta.dataflow.repository.connector.titan.predicate;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;

/**
 * Evaluator, který zamítne všechny vrcholy.
 * @author tfechtner
 *
 */
public final class FalsePredicate implements VertexPredicate {
    private static final FalsePredicate INSTANCE = new FalsePredicate();

    private FalsePredicate() {
    }

    /**
     * @return singleton instance 
     */
    public static FalsePredicate getInstance() {
        return INSTANCE;
    }

    @Override
    public boolean evaluate(Vertex vertex, RevisionInterval revisionInterval) {
        return false;
    }
}
