package eu.profinit.manta.dataflow.repository.connector.titan.filter.unit;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

public class TypeFilterUnitTest extends TestTitanDatabaseProvider {
    @Test
    public void testAllTypes() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                NameFilterUnit filterT1 = new NameFilterUnit(null, null, "t1.*");

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Assert.assertTrue(filterT1.isFiltered(t1c1, RevisionRootHandler.EVERY_REVISION_INTERVAL));

                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Assert.assertFalse(filterT1.isFiltered(t2c1, RevisionRootHandler.EVERY_REVISION_INTERVAL));

                NameFilterUnit filterTable1 = new NameFilterUnit(null, null, "table1");
                Assert.assertTrue(filterTable1.isFiltered(t1c1, RevisionRootHandler.EVERY_REVISION_INTERVAL));
                Assert.assertFalse(filterTable1.isFiltered(t2c1, RevisionRootHandler.EVERY_REVISION_INTERVAL));

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Assert.assertTrue(filterTable1.isFiltered(table1, RevisionRootHandler.EVERY_REVISION_INTERVAL));

                return null;
            }
        });
    }

    @Test
    public void testOnlyTables() {
        cleanGraph();
        final double revision = createMajorRevision();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata", "Teradata", "", layer, revision);
                Vertex db = GraphCreation.createNode(transaction, resource, "db", "Database", revision);
                Vertex table = GraphCreation.createNode(transaction, db, "users1", "Table", revision);
                Vertex view = GraphCreation.createNode(transaction, db, "users2", "View", revision);
                Vertex t1c1 = GraphCreation.createNode(transaction, table, "t1c1", "Column", revision);
                Vertex v1c1 = GraphCreation.createNode(transaction, view, "v1c1", "Column", revision);

                NameFilterUnit filter = new NameFilterUnit(null, Collections.singleton("Table"), "users.*");

                Assert.assertTrue(filter.isFiltered(t1c1, RevisionRootHandler.EVERY_REVISION_INTERVAL));
                Assert.assertTrue(filter.isFiltered(table, RevisionRootHandler.EVERY_REVISION_INTERVAL));

                Assert.assertFalse(filter.isFiltered(v1c1, RevisionRootHandler.EVERY_REVISION_INTERVAL));
                Assert.assertFalse(filter.isFiltered(view, RevisionRootHandler.EVERY_REVISION_INTERVAL));

                return null;
            }
        });
    }

    @Test
    public void testOnlyResources() {
        cleanGraph();
        final double revision = createMajorRevision();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex resource1 = GraphCreation.createResource(transaction, root, "Teradata", "Teradata", "",
                        layer, revision);
                Vertex db1 = GraphCreation.createNode(transaction, resource1, "db", "Database", revision);
                GraphCreation.createNodeAttribute(transaction, db1, "a1", "v1", 1.000000);

                Vertex resource2 = GraphCreation.createResource(transaction, root, "Oracle", "Oracle", "", layer, revision);
                Vertex db2 = GraphCreation.createNode(transaction, resource2, "db", "Database", revision);
                GraphCreation.createNodeAttribute(transaction, db2, "a1", "v1", 1.000000);

                NameFilterUnit filter = new NameFilterUnit(Collections.singleton("Teradata"), null, "db.*");

                Assert.assertTrue(filter.isFiltered(db1, RevisionRootHandler.EVERY_REVISION_INTERVAL));
                Assert.assertFalse(filter.isFiltered(db2, RevisionRootHandler.EVERY_REVISION_INTERVAL));
                return null;
            }
        });
    }
}
