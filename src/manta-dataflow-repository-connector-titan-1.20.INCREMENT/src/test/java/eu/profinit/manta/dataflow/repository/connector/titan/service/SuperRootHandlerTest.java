package eu.profinit.manta.dataflow.repository.connector.titan.service;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

/**
 * Test držáku super rootu..
 * @author tfechtner
 *
 */
public class SuperRootHandlerTest extends TestTitanDatabaseProvider {
    /**
     * Test super rootu.
     */
    @Test
    public void testGetRoot() {
        cleanGraph();
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT, getVertexCount(transaction));
                getSuperRootHandler().ensureRootExistance(transaction);
                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT, getVertexCount(transaction));
                getSuperRootHandler().ensureRootExistance(transaction);
                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT, getVertexCount(transaction));
                return null;
            }
        });
    }

    /**
     * Test vyjimky pri neexistenci super rootu.
     */
    @Test(expected = IllegalStateException.class)
    public void testGetRootNotExist() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Iterable<Vertex> vIterable = transaction.getVertices();
                for (Vertex v : vIterable) {
                    transaction.removeVertex(v);
                }
                return null;
            }
        });

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getSuperRootHandler().getRoot(transaction);
                return null;
            }
        });
    }
}
