package eu.profinit.manta.dataflow.repository.connector.titan.connection;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

/**
 * Test pripojeni db.
 * @author tfechtner
 *
 */
public class DatabaseProviderTest extends TestTitanDatabaseProvider {

    /**
     * Otestovat základní setup db.
     */
    @Test
    public void testInitSetup() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(getBasicGraphVertexCount(), getVertexCount(transaction));
                return null;
            }
        });
    }
}
