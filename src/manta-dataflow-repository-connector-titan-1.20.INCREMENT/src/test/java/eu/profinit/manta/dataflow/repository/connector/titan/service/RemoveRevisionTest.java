package eu.profinit.manta.dataflow.repository.connector.titan.service;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider.TestCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

public class RemoveRevisionTest extends TestTitanDatabaseProvider {
    @Test
    public void testRemoveRevisions() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                // TODO add checking neighbors (previous/next revision)
                RevisionRootHandler revisionRootHandler = getRevisionRootHandler();
                Double latestCommittedRevisionNumber;
                Double latestUncommittedRevisionNumber;
                
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);
                
                revisionRootHandler.createMinorRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER, latestCommittedRevisionNumber);
                Assert.assertEquals(REVISION_0_000001, latestUncommittedRevisionNumber);
                
                revisionRootHandler.commitLatestRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(REVISION_0_000001, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);
                
                try {
                    revisionRootHandler.removeRevisionNode(transaction, REVISION_0_000001);
                } catch(RevisionException exception) {
                    // cannot remove an already committed revision
                }
                
                revisionRootHandler.createMinorRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(REVISION_0_000001, latestCommittedRevisionNumber);
                Assert.assertEquals(REVISION_0_000002, latestUncommittedRevisionNumber);
                
                revisionRootHandler.removeRevisionNode(transaction, REVISION_0_000002);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(REVISION_0_000001, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);
                
                revisionRootHandler.createMinorRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(REVISION_0_000001, latestCommittedRevisionNumber);
                Assert.assertEquals(REVISION_0_000002, latestUncommittedRevisionNumber);
                
                return null;
            }
        });
    }

}
