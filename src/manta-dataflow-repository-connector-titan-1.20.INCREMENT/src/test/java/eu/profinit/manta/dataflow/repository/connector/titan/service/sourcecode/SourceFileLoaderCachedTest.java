package eu.profinit.manta.dataflow.repository.connector.titan.service.sourcecode;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.profinit.manta.platform.cache.CustomEhcacheFactory;
import net.sf.ehcache.CacheManager;

/**
 * Unit testy {@link SourceFileLoaderCached}.
 * 
 * @author onouza
 */
public class SourceFileLoaderCachedTest {

    /**
     * Instance testovane tridy
     */
    private SourceFileLoaderCached sourceFileLoaderCached;
    
    @Before
    public void setUp() {
        sourceFileLoaderCached = new SourceFileLoaderCached();
        CustomEhcacheFactory ehcacheFactory = new CustomEhcacheFactory();
        ehcacheFactory.setCacheManager(new CacheManager());
        sourceFileLoaderCached.setEhcacheFactory(ehcacheFactory );
        sourceFileLoaderCached.init();
    }
    
    /**
     * Otestuje {@link SourceFileLoaderCached#loadSourceFile(SourceFile)}.
     */
    @Test
    public void testLoadSourceFile() {
        TestSourceFileLoader sourceFileLoader = new TestSourceFileLoader();
        sourceFileLoaderCached.setSourceFileLoader(sourceFileLoader);

        SourceFile sourceFile = new SourceFile("barvy.txt", "UTF-8");
        
        // Prvni cteni => zatim ne z cache
        List<String> lines = sourceFileLoaderCached.loadSourceFile(sourceFile);
        int loadCount = sourceFileLoader.counter.get(sourceFile);
        Assert.assertEquals("Ahoj babi!", lines.get(0));
        Assert.assertEquals(1, loadCount);

        // Druhe cteni => melo by byt z cache
        lines = sourceFileLoaderCached.loadSourceFile(sourceFile);
        loadCount = sourceFileLoader.counter.get(sourceFile);
        Assert.assertEquals("Ahoj babi!", lines.get(0));
        Assert.assertEquals("Nebyla pouzita cache pri nacitani dat oproti ocekavani.", 1, loadCount);
    }
    
    // vnitrni tridy

    /**
     * Testovaci komponenta pro cteni zdrojoveho kodu.
     * Pro ucely testovani uchovava informaci o poctu nacteni souboru v danem umisteni
     * 
     * @author onouza
     *
     */
    private static class TestSourceFileLoader implements SourceFileLoader {
        /**
         * Pocitadlo nacitani z jednotlivych umisteni.
         */
        private Map<SourceFile, Integer> counter = new HashMap<>();
        
        @Override
        public List<String> loadSourceFile(SourceFile sourceFile) {
            // Zvysime pocitadlo o 1
            Integer count = counter.get(sourceFile);
            if (count == null) {
                count = 1;
            } else {
                count++;
            }
            counter.put(sourceFile, count);
            // a vratime vysledek
            return Collections.singletonList("Ahoj babi!");
        }
    }

}
