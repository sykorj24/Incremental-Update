package eu.profinit.manta.dataflow.repository.connector.titan.service;

import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

/** 
 * Testování vytváření objektů.
 * @author tfechtner
 *
 */
public class GraphCreationTest extends TestTitanDatabaseProvider {

    /** Tvorba resource. */
    @Test
    public void testCreateResource() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT, getVertexCount(transaction));

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                GraphCreation.createResource(transaction, root, "resName", "resType", "resDesc", layer, REVISION_1_000000);

                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT + LAYER_COUNT + 1, getVertexCount(transaction));

                Vertex newNode = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "resName").iterator().next();
                Assert.assertEquals("resName", newNode.getProperty(NodeProperty.RESOURCE_NAME.t()));
                Assert.assertEquals("resType", newNode.getProperty(NodeProperty.RESOURCE_TYPE.t()));
                Assert.assertEquals("resDesc", newNode.getProperty(NodeProperty.RESOURCE_DESCRIPTION.t()));

                Iterator<Vertex> vIter = root.getVertices(Direction.IN, EdgeLabel.HAS_RESOURCE.t()).iterator();
                Assert.assertTrue(vIter.hasNext());
                Assert.assertEquals(newNode, vIter.next());
                return null;
            }

        });
    }

    /** Test tvorby uzlu pod resource. */
    @Test
    public void testCreateNodeUnderResource() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT, getVertexCount(transaction));

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex resource = GraphCreation.createResource(transaction, root, "resName", "resType", "resDesc", layer, REVISION_1_000000);
                GraphCreation.createNode(transaction, resource, "nodeName", "nodeType", REVISION_1_000000);

                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT + LAYER_COUNT + 1 + 1, getVertexCount(transaction));

                Vertex newNode = transaction.getVertices(NodeProperty.NODE_NAME.t(), "nodeName").iterator().next();
                Assert.assertEquals("nodeName", newNode.getProperty(NodeProperty.NODE_NAME.t()));
                Assert.assertEquals("nodeType", newNode.getProperty(NodeProperty.NODE_TYPE.t()));

                Iterator<Vertex> vIter = resource.getVertices(Direction.IN, EdgeLabel.HAS_RESOURCE.t()).iterator();
                Assert.assertTrue(vIter.hasNext());
                Assert.assertEquals(newNode, vIter.next());
                return null;
            }
        });
    }

    /** Test tvorby uzlu pod uzel. */
    @Test
    public void testCreateNodeUnderNode() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT, getVertexCount(transaction));

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex resource = GraphCreation.createResource(transaction, root, "resName", "resType", "resDesc", layer, REVISION_1_000000);
                Vertex node = GraphCreation.createNode(transaction, resource, "nodeName", "nodeType", REVISION_1_000000);
                GraphCreation.createNode(transaction, node, "nodeName2", "nodeType2", REVISION_1_000000);

                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT + LAYER_COUNT + 1 + 1 + 1, getVertexCount(transaction));

                Vertex newNode = transaction.getVertices(NodeProperty.NODE_NAME.t(), "nodeName2").iterator().next();
                Assert.assertEquals("nodeName2", newNode.getProperty(NodeProperty.NODE_NAME.t()));
                Assert.assertEquals("nodeType2", newNode.getProperty(NodeProperty.NODE_TYPE.t()));

                Iterator<Vertex> vIter = node.getVertices(Direction.IN, EdgeLabel.HAS_PARENT.t()).iterator();
                Assert.assertTrue(vIter.hasNext());
                Assert.assertEquals(newNode, vIter.next());
                return null;
            }
        });
    }

    /** Test tvorby uzlu s jinym resource nez ma jeho predek. */
    @Test
    public void testCreateNodeWithDifferentResource() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT, getVertexCount(transaction));

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex resource = GraphCreation.createResource(transaction, root, "resName", "resType", "resDesc", layer, REVISION_1_000000);
                Vertex resource2 = GraphCreation.createResource(transaction, root, "resName2", "resType", "resDesc", layer, REVISION_1_000000);
                Vertex parent = GraphCreation.createNode(transaction, resource, "parentName", "parentType", REVISION_1_000000);
                GraphCreation.createNode(transaction, parent, resource2, "newNode", "newNodeType", REVISION_1_000000);

                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT + LAYER_COUNT + 1 + 1 + 1 + 1, getVertexCount(transaction));

                Vertex newNode = transaction.getVertices(NodeProperty.NODE_NAME.t(), "newNode").iterator().next();
                Assert.assertEquals("newNode", newNode.getProperty(NodeProperty.NODE_NAME.t()));
                Assert.assertEquals("newNodeType", newNode.getProperty(NodeProperty.NODE_TYPE.t()));

                Iterator<Vertex> vIter = newNode.getVertices(Direction.OUT, EdgeLabel.HAS_PARENT.t()).iterator();
                Assert.assertTrue(vIter.hasNext());
                Assert.assertEquals(parent, vIter.next());

                vIter = newNode.getVertices(Direction.OUT, EdgeLabel.HAS_RESOURCE.t()).iterator();
                Assert.assertTrue(vIter.hasNext());
                Assert.assertEquals(resource2, vIter.next());
                return null;
            }
        });
    }

    /** Test tvorby atributu uzlu. */
    @Test
    public void testCreateNodeAttribute() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT, getVertexCount(transaction));

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex resource = GraphCreation.createResource(transaction, root, "resName", "resType", "resDesc", layer, REVISION_1_000000);
                Vertex node = GraphCreation.createNode(transaction, resource, "nodeName", "nodeType", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, node, "attrKey", "attrValue", REVISION_1_000000);

                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT + LAYER_COUNT + 1 + 1 + 1, getVertexCount(transaction));

                Vertex newNode = transaction.getVertices(NodeProperty.ATTRIBUTE_NAME.t(), "attrKey").iterator().next();
                Assert.assertEquals("attrKey", newNode.getProperty(NodeProperty.ATTRIBUTE_NAME.t()));
                Assert.assertEquals("attrValue", newNode.getProperty(NodeProperty.ATTRIBUTE_VALUE.t()));

                Iterator<Vertex> vIter = node.getVertices(Direction.OUT, EdgeLabel.HAS_ATTRIBUTE.t()).iterator();
                Assert.assertTrue(vIter.hasNext());
                Assert.assertEquals(newNode, vIter.next());
                return null;
            }
        });
    }

    /** Test tvorby hrany. */
    @Test
    public void testCreateEdge() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT, getVertexCount(transaction));

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex resource = GraphCreation.createResource(transaction, root, "resName", "resType", "resDesc", layer, REVISION_1_000000);
                Vertex node1 = GraphCreation.createNode(transaction, resource, "nodeName1", "nodeType", REVISION_1_000000);
                Vertex node2 = GraphCreation.createNode(transaction, resource, "nodeName2", "nodeType", REVISION_1_000000);
                GraphCreation.createEdge(node1, node2, EdgeLabel.DIRECT, REVISION_1_000000);

                Iterator<Vertex> vIter = node1.getVertices(Direction.OUT, EdgeLabel.DIRECT.t()).iterator();
                Assert.assertTrue(vIter.hasNext());
                Assert.assertEquals(node2, vIter.next());
                return null;
            }
        });
    }

    /** Inicializace databaze. */
    @Test
    public void testInitDb() {
        cleanGraph();
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT, getVertexCount(transaction));
                Assert.assertTrue(transaction.getIndexedKeys(Vertex.class).contains(NodeProperty.NODE_NAME.t()));
                Assert.assertTrue(transaction.getIndexedKeys(Vertex.class).contains(NodeProperty.SUPER_ROOT.t()));

                GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                        getSourceRootHandler());
                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT, getVertexCount(transaction));
                Assert.assertTrue(transaction.getIndexedKeys(Vertex.class).contains(NodeProperty.NODE_NAME.t()));
                Assert.assertTrue(transaction.getIndexedKeys(Vertex.class).contains(NodeProperty.SUPER_ROOT.t()));
                return null;
            }
        });
    }

    @Test
    public void testCopyEdge() {
        createBasicGraph(false);
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                RevisionInterval revInterval = new RevisionInterval(1);

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                List<Edge> edges = GraphOperation.getAdjacentEdges(t1c1, Direction.OUT, revInterval, EdgeLabel.DIRECT);
                Edge originalEdge = edges.get(0);

                originalEdge.setProperty("a1", "v1");
                originalEdge.setProperty("a2", "v2");

                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Edge newEdge = GraphCreation.copyEdge(originalEdge, t2c1, t1c2, revInterval);

                Assert.assertEquals("v1", newEdge.getProperty("a1"));
                Assert.assertEquals("v2", newEdge.getProperty("a2"));
                Assert.assertEquals(originalEdge.getLabel(), newEdge.getLabel());
                Assert.assertEquals(t2c1, newEdge.getVertex(Direction.OUT));
                Assert.assertEquals(t1c2, newEdge.getVertex(Direction.IN));

                return null;
            }
        });
    }

    @Test
    public void testCopyNodeAttribute() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                RevisionInterval revInterval = new RevisionInterval(1);

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                List<Vertex> attrs = GraphOperation.getAdjacentVertices(table1, Direction.OUT, revInterval,
                        EdgeLabel.HAS_ATTRIBUTE);
                Vertex originalAttr = attrs.get(0);

                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex newAttr = GraphCreation.copyNodeAttribute(transaction, originalAttr, t2c1, revInterval);

                Assert.assertEquals(GraphOperation.getAttributeKey(originalAttr),
                        GraphOperation.getAttributeKey(newAttr));
                Assert.assertEquals(GraphOperation.getAttributeValue(originalAttr),
                        GraphOperation.getAttributeValue(newAttr));
                Assert.assertEquals(VertexType.ATTRIBUTE, VertexType.getType(newAttr));
                Edge edge = GraphOperation.getAttributeControlEdge(newAttr);
                Assert.assertEquals(t2c1, edge.getVertex(Direction.OUT));
                Assert.assertEquals(newAttr, edge.getVertex(Direction.IN));
                Assert.assertEquals(EdgeLabel.HAS_ATTRIBUTE.t(), edge.getLabel());

                return null;
            }
        });
    }

    @Test
    public void testCopySubTree() {
        createBasicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                RevisionInterval revInterval = new RevisionInterval(1, 2);

                Vertex resource = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator().next();
                Vertex db2 = GraphCreation.createNode(transaction, resource, "db2", "Database", revInterval);

                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();

                Vertex newTable1 = GraphCreation.copySubTree(transaction, table1, db2, revInterval);
                Assert.assertEquals(db2, GraphOperation.getParent(newTable1));
                Assert.assertEquals("table1", GraphOperation.getName(newTable1));
                Assert.assertEquals("TABLE",
                        GraphOperation.getFirstNodeAttribute(newTable1, "TABLE_TYPE", revInterval));

                List<Vertex> t1columns = GraphOperation.getDirectChildren(newTable1, revInterval);
                Assert.assertEquals(2, t1columns.size());
                Vertex t1c2 = t1columns.get(1);
                Assert.assertEquals("t1c2", GraphOperation.getName(t1c2));

                // hrana do nekopirovaneho grafu
                List<Edge> edges = GraphOperation.getAdjacentEdges(t1c2, Direction.OUT, revInterval, EdgeLabel.DIRECT);
                Assert.assertEquals(1, edges.size());
                Edge edgeOutter = edges.get(0);
                Vertex target = edgeOutter.getVertex(Direction.IN);
                Assert.assertEquals("t2c1", GraphOperation.getName(target));
                Assert.assertEquals(createSet("table2", "db"),
                        translateVerticesToNames(GraphOperation.getAllParent(target)));

                // hrana do kopirovaneho grafu
                edges = GraphOperation.getAdjacentEdges(t1c2, Direction.IN, revInterval, EdgeLabel.DIRECT);
                Assert.assertEquals(1, edges.size());
                Edge edgeInner = edges.get(0);
                target = edgeInner.getVertex(Direction.OUT);
                Assert.assertEquals("t1c1", GraphOperation.getName(target));
                Assert.assertEquals(createSet("table1", "db2"),
                        translateVerticesToNames(GraphOperation.getAllParent(target)));

                return null;
            }
        });
    }
}
