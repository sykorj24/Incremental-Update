package eu.profinit.manta.dataflow.repository.connector.titan.service.sourcecode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.AttributeNames;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

/**
 * Unnit testy {@link SourceCodeServiceImpl}.
 * 
 * @author onouza
 */
public class SourceCodeServiceTest extends TestTitanDatabaseProvider {
    
    /**
     * Instance testovane tridy
     */
    private SourceCodeServiceImpl sourceCodeService;
    
    @Before
    public void setUp() {
        sourceCodeService = new SourceCodeServiceImpl();
        sourceCodeService.setSourceFileLoader(new TestSourceFileLoader());
    }
    
    /**
     * Otestuje {@link SourceCodeServiceImpl#getSourceCode(Vertex, RevisionInterval, boolean)}.
     */
    @Test
    public void testGetSourceCode() {
        cleanGraph();
        final double revision = createMajorRevision();
        final RevisionInterval revisionInterval = new RevisionInterval(revision);
        createCommonDatabase(revision);
        
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex vertexOdstin = GraphOperation.getVerticesWithName(transaction, "odstin", revisionInterval, 1).get(0);
                String sourceCode = sourceCodeService.getSourceCode(vertexOdstin, revisionInterval, true);

                Assert.assertNotNull(sourceCode);

                String ls = System.lineSeparator();
                String expectedSourceCode = "INSERT INTO modra" + ls
                        + "values" + ls
                        + "(" + ls
                        + "  odstin," + ls
                        + "  odlesk," + ls
                        + "  pruhlednost" + ls
                        + ");" + ls;
                Assert.assertEquals(expectedSourceCode, sourceCode);
                return null;
            }
        });
    }
    
    /**
     * Otestuje {@link SourceCodeServiceImpl#findPosition(List, String, RevisionInterval)}.
     */
    @Test
    public void testFindPosition() {
        cleanGraph();
        final double revision = createMajorRevision();
        final RevisionInterval revisionInterval = new RevisionInterval(revision);
        createCommonDatabase(revision);
        
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex vertexOdstin = GraphOperation.getVerticesWithName(transaction, "odstin", revisionInterval, 1).get(0);
                Vertex vertexInsert = GraphOperation.getVerticesWithName(transaction, "<2,4>Insert", revisionInterval, 1).get(0);
                Vertex vertexInsertStatement = GraphOperation.getVerticesWithName(transaction, "<2,4>SQL_insert_stmt", revisionInterval, 1).get(0);
                
                List<Vertex> reversePath = new ArrayList<>(Arrays.asList(vertexOdstin, vertexInsert, vertexInsertStatement));
                Object position = ReflectionTestUtils.invokeMethod(sourceCodeService, "findPosition", reversePath, AttributeNames.NODE_SCRIPT_POSITION, revisionInterval);
                
                Assert.assertNotNull(position);
                Assert.assertEquals(2, ReflectionTestUtils.invokeGetterMethod(position, "line"));
                Assert.assertEquals(4, ReflectionTestUtils.invokeGetterMethod(position, "col"));
                
                return null;
            }
        });

    }
    
    /**
     * Otestuje {@link SourceCodeServiceImpl#findSourceLocation(List, RevisionInterval)}.
     */
    @Test
    public void testFindSourceLocation() {
        cleanGraph();
        final double revision = createMajorRevision();
        final RevisionInterval revisionInterval = new RevisionInterval(revision);
        createCommonDatabase(revision);
        
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex vertexOdstin = GraphOperation.getVerticesWithName(transaction, "odstin", revisionInterval, 1).get(0);
                Vertex vertexInsert = GraphOperation.getVerticesWithName(transaction, "<2,4>Insert", revisionInterval, 1).get(0);
                Vertex vertexInsertStatement = GraphOperation.getVerticesWithName(transaction, "<2,4>SQL_insert_stmt", revisionInterval, 1).get(0);
                
                List<Vertex> reversePath = new ArrayList<>(Arrays.asList(vertexOdstin, vertexInsert, vertexInsertStatement));
                SourceFile sourceFile = ReflectionTestUtils.invokeMethod(sourceCodeService, "findSourceLocation", reversePath, revisionInterval);
                
                Assert.assertNotNull(sourceFile);
                Assert.assertEquals("modra.txt", sourceFile.getLocation());
                Assert.assertEquals("UTF-8", sourceFile.getEncoding());
                return null;
            }
        });

    }
    
    /**
     * Otestuje {@link SourceCodeServiceImpl#reduceLineIndent(List)}.
     */
    @Test
    public void testReduceLineIndent() {
        List<String> lines;
        List<String> formattedLines;

        // zadne radky
        lines = new ArrayList<>();
        formattedLines = ReflectionTestUtils.invokeMethod(sourceCodeService, "reduceLineIndent", lines);
        Assert.assertNotNull(formattedLines);
        Assert.assertEquals(0, formattedLines.size());

        // pouze prazdne radky
        lines = new ArrayList<>();
        lines.add("    ");
        lines.add("\r\n");
        lines.add("\t\t\t\t\t\t\t\t");
        formattedLines = ReflectionTestUtils.invokeMethod(sourceCodeService, "reduceLineIndent", lines);
        Assert.assertNotNull(formattedLines);
        Assert.assertEquals(3, formattedLines.size());
        Assert.assertEquals("", formattedLines.get(0));
        Assert.assertEquals("", formattedLines.get(1));
        Assert.assertEquals("", formattedLines.get(2));

        // neprazdne radky s minimalnim odsazenim 3 + prazdne radky o ruznem poctu bilych znaku 
        lines = new ArrayList<>();
        lines.add("    ");
        lines.add("   Ahoj babi");
        lines.add("\r\n");
        lines.add("\t    Jak se vede?");
        lines.add("\r\n end of Ahoj babi   ");
        lines.add("\t\t\t\t\t\t\t\t");
        formattedLines = ReflectionTestUtils.invokeMethod(sourceCodeService, "reduceLineIndent", lines);
        Assert.assertNotNull(formattedLines);
        Assert.assertEquals(6, formattedLines.size());
        Assert.assertEquals(" ", formattedLines.get(0));
        Assert.assertEquals("Ahoj babi", formattedLines.get(1));
        Assert.assertEquals("", formattedLines.get(2));
        Assert.assertEquals("  Jak se vede?", formattedLines.get(3));
        Assert.assertEquals("end of Ahoj babi   ", formattedLines.get(4));
        Assert.assertEquals("\t\t\t\t\t", formattedLines.get(5));
    }
    
    // pomocne privatni metody
    
    /**
     * Vytvori databazi spolecnou pro vsechny uni testy.
     * 
     * @param revision Cislo pracovni revize.
     */
    private void createCommonDatabase(final Double revision) {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                // Fyzicka vrstva
                
                Vertex physicalLayer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex resourceTeradataBteq = GraphCreation.createResource(transaction, root, "Teradata BTEQ", "Teradata scripts", "desc",
                        physicalLayer, revision);
                Vertex nodeDirecotryBarvy = GraphCreation.createNode(transaction, resourceTeradataBteq, "Barvy", "Directory", revision);
                Vertex nodeScriptModra = GraphCreation.createNode(transaction, nodeDirecotryBarvy, "modra.txt", "BTEQ Script", revision);
                
                Vertex nodeInsertStatement = GraphCreation.createNode(transaction, nodeScriptModra, "<2,4>SQL_insert_stmt", "BTEQ Statement", revision);
                GraphCreation.createNodeAttribute(transaction, nodeInsertStatement, AttributeNames.NODE_SCRIPT_POSITION, "2,4", revision);
                GraphCreation.createNodeAttribute(transaction, nodeInsertStatement, AttributeNames.NODE_SCRIPT_STOP_POSITION, "8,6", revision);
                GraphCreation.createNodeAttribute(transaction, nodeInsertStatement, SourceCodeServiceImpl.SOURCE_ENCODING, "UTF-8", revision);
                GraphCreation.createNodeAttribute(transaction, nodeInsertStatement, SourceCodeServiceImpl.SOURCE_LOCATION, "modra.txt", revision);
                
                Vertex nodeInsert = GraphCreation.createNode(transaction, nodeInsertStatement, "<2,4>Insert", "BTEQ Insert", revision);
                GraphCreation.createNode(transaction, nodeInsert, "odstin", "BTEQ ColumnFlow", revision);
                return null;
            }
        });
    }
    
    // vnitrni tridy
    
    /**
     * Testovaci implementace {@link SourceFileLoader}.
     * 
     * @author onouza
     */
    private class TestSourceFileLoader implements SourceFileLoader {

        @Override
        public List<String> loadSourceFile(SourceFile sourceFile) {
            return Arrays.asList(
                    " * Modra *",
                    "   INSERT INTO modra",
                    "   values",
                    "   (",
                    "     odstin,",
                    "     odlesk,",
                    "     pruhlednost",
                    "   );* STOP *",
                    " * End of Modra *"
                    );
        }
        
    }

}
