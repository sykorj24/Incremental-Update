package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.AccessLevel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorPostfix;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorPrefix;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverserSerial;

public class GraphScalableTraverserCustomisableTest extends TestTitanDatabaseProvider {
    @Test
    public void testPrefixAll() {
        cleanGraph();
        createBasicGraph(false);

        TestGraphVisitor visitor = runTraverser(TraverserProcessorFactoryGeneric.DEFAULT_VISITED_PARTS,
                5, TraverserProcessorPrefix.class);

        Assert.assertEquals(1, visitor.getLayerCount());
        Assert.assertEquals(1, visitor.getResourceCount());
        Assert.assertEquals(7, visitor.getNodeCount());
        Assert.assertEquals(2, visitor.getAttributeCount());
        Assert.assertEquals(4, visitor.getEdgeCount());
    }

    @Test
    public void testPrefixWithoutAttrAndEdges() {
        cleanGraph();
        createBasicGraph(false);

        List<VisitedPart> visitedParts = new ArrayList<>();
        visitedParts.add(VisitedPart.SELF);
        visitedParts.add(VisitedPart.SUCCESSORS);
        visitedParts.add(VisitedPart.SUCCESSORS);

        TestGraphVisitor visitor = runTraverser(visitedParts, 5, TraverserProcessorPrefix.class);

        Assert.assertEquals(1, visitor.getLayerCount());
        Assert.assertEquals(1, visitor.getResourceCount());
        Assert.assertEquals(7, visitor.getNodeCount());
        Assert.assertEquals(0, visitor.getAttributeCount());
        Assert.assertEquals(0, visitor.getEdgeCount());
    }

    @Test
    public void testPostifxAll() {
        cleanGraph();
        createBasicGraph(false);

        TestGraphVisitor visitor = runTraverser(TraverserProcessorFactoryGeneric.DEFAULT_VISITED_PARTS, 5,
                TraverserProcessorPostfix.class);

        Assert.assertEquals(1, visitor.getLayerCount());
        Assert.assertEquals(1, visitor.getResourceCount());
        Assert.assertEquals(7, visitor.getNodeCount());
        Assert.assertEquals(2, visitor.getAttributeCount());
        Assert.assertEquals(4, visitor.getEdgeCount());
    }

    @Test
    public void testPostifxWithoutAttrAndEdges() {
        cleanGraph();
        createBasicGraph(false);

        List<VisitedPart> visitedParts = new ArrayList<>();
        visitedParts.add(VisitedPart.SELF);
        visitedParts.add(VisitedPart.SUCCESSORS);
        visitedParts.add(VisitedPart.SUCCESSORS);

        TestGraphVisitor visitor = runTraverser(visitedParts, 5, TraverserProcessorPostfix.class);

        Assert.assertEquals(1, visitor.getLayerCount());
        Assert.assertEquals(1, visitor.getResourceCount());
        Assert.assertEquals(7, visitor.getNodeCount());
        Assert.assertEquals(0, visitor.getAttributeCount());
        Assert.assertEquals(0, visitor.getEdgeCount());
    }

    private <T extends TraverserProcessor<?>> TestGraphVisitor runTraverser(List<VisitedPart> visitedParts,
            int objectsPerTran, Class<T> processorClass) {
        TraverserProcessorFactory processorFactory = new TraverserProcessorFactoryGeneric<>(processorClass, "mr_basic",
                objectsPerTran, new SearchOrderDfs(), visitedParts);

        GraphScalableTraverserSerial traverser = new GraphScalableTraverserSerial(getDatabaseHolder(),
                processorFactory);

        TestGraphVisitor visitor = new TestGraphVisitor();
        Long rootId = getSuperRootHandler().getRoot(getDatabaseHolder());
        traverser.traverse(visitor, rootId, AccessLevel.READ, REVISION_INTERVAL_1_0);
        return visitor;
    }
}
