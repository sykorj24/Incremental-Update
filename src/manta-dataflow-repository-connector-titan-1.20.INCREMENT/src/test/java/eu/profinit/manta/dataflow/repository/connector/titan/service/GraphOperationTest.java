package eu.profinit.manta.dataflow.repository.connector.titan.service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.EdgeIdentification;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

/**
 * Testování operací nad grafem.
 * @author tfechtner
 *
 */
public class GraphOperationTest extends TestTitanDatabaseProvider {
    /**
     * Test parent.
     */
    @Test
    public void testGetParent() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Assert.assertEquals("table1", GraphOperation.getParent(t1c1).getProperty(NodeProperty.NODE_NAME.t()));

                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Assert.assertNull(GraphOperation.getParent(db));
                return null;
            }
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetParentNull() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertNull(GraphOperation.getParent(null));
                return null;
            }
        });
    }

    /**
     * Test atributu uzlu.
     */
    @Test
    public void testGetNodeAttribute() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex t1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Assert.assertEquals("TABLE",
                        GraphOperation.getFirstNodeAttribute(t1, "TABLE_TYPE", REVISION_INTERVAL_1_0));

                Assert.assertNull(GraphOperation.getFirstNodeAttribute(t1, "nic", REVISION_INTERVAL_1_0));

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                GraphCreation.createNodeAttribute(transaction, t1c1, "attr", "val1", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, t1c1, "attr", "val2", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, t1c1, "attr", "val3", REVISION_1_000000);

                List<Object> list = GraphOperation.getNodeAttribute(t1c1, "attr", REVISION_INTERVAL_1_0);

                Assert.assertEquals(3, list.size());
                Assert.assertTrue(list.contains("val1"));
                Assert.assertTrue(list.contains("val2"));
                Assert.assertTrue(list.contains("val3"));

                return null;
            }
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetNodeAttributeNullKey() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Assert.assertNull(GraphOperation.getNodeAttribute(t1, null, REVISION_INTERVAL_1_000000_1_000000));
                return null;
            }
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetNodeAttributeNullnode() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertNull(GraphOperation.getNodeAttribute(null, "nic", REVISION_INTERVAL_1_0));
                return null;
            }
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetNodeAttributeNullBoth() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertNull(GraphOperation.getNodeAttribute(null, null, REVISION_INTERVAL_1_0));
                return null;
            }
        });
    }

    /**
     * Test atributu uzlu.
     */
    @Test
    public void testGetAllNodeAttributes() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex t1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                GraphCreation.createNodeAttribute(transaction, t1, "attr", "val1", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, t1, "attr", "val2", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, t1, "attr", "val3", REVISION_1_000000);

                Map<String, List<Object>> attrs = GraphOperation.getAllNodeAttributes(t1, REVISION_INTERVAL_1_0);
                Assert.assertEquals(2, attrs.size());
                Assert.assertEquals(Collections.singletonList("TABLE"), attrs.get("TABLE_TYPE"));
                List<Object> list = attrs.get("attr");
                Assert.assertEquals(3, list.size());
                Assert.assertTrue(list.contains("val1"));
                Assert.assertTrue(list.contains("val2"));
                Assert.assertTrue(list.contains("val3"));

                return null;
            }
        });
    }

    /**
     * Test sousedů uzlu.
     */
    @Test
    public void testGetAdjacentVertices() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex t1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();

                List<Vertex> adjacents = GraphOperation.getAdjacentVertices(t1, Direction.OUT, REVISION_INTERVAL_1_0,
                        EdgeLabel.DIRECT);
                Assert.assertEquals(1, adjacents.size());
                Assert.assertEquals("t2c2", adjacents.get(0).getProperty(NodeProperty.NODE_NAME.t()));

                adjacents = GraphOperation.getAdjacentVertices(t1, Direction.IN, REVISION_INTERVAL_1_0, EdgeLabel.DIRECT);
                Assert.assertEquals(1, adjacents.size());
                Assert.assertEquals("t1c2", adjacents.get(0).getProperty(NodeProperty.NODE_NAME.t()));

                adjacents = GraphOperation.getAdjacentVertices(t1, Direction.BOTH, REVISION_INTERVAL_1_0,
                        EdgeLabel.DIRECT);
                Assert.assertEquals(2, adjacents.size());

                adjacents = GraphOperation.getAdjacentVertices(t1, Direction.BOTH, REVISION_INTERVAL_1_0,
                        EdgeLabel.FILTER);
                Assert.assertEquals(1, adjacents.size());
                return null;
            }
        });
    }

    /**
     * Test listových potomků.
     */
    @Test
    public void testGetAllLists() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                List<Vertex> vertices = GraphOperation.getAllLists(db, REVISION_INTERVAL_1_0);
                Assert.assertEquals(2 + 2, vertices.size());

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                vertices = GraphOperation.getAllLists(table1, REVISION_INTERVAL_1_0);
                Assert.assertEquals(2, vertices.size());
                return null;
            }
        });
    }

    /**
     * Test předků.
     */
    @Test
    public void testGetAllParents() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                List<Vertex> parents = GraphOperation.getAllParent(t1c1);
                Assert.assertEquals(2, parents.size());
                Assert.assertEquals("table1", parents.get(0).getProperty(NodeProperty.NODE_NAME.t()));
                Assert.assertEquals("db", parents.get(1).getProperty(NodeProperty.NODE_NAME.t()));

                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                parents = GraphOperation.getAllParent(db);
                Assert.assertEquals(0, parents.size());
                return null;
            }
        });
    }

    /**
     * Test najít resource.
     */
    @Test
    public void testGetResource() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex teradata = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator().next();
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();

                Vertex root = getSuperRootHandler().getRoot(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex teradataDDL = GraphCreation.createResource(transaction, root, "Teradata DDL", "t", "d", layer, 1.000000);
                Vertex t1cspec = GraphCreation.createNode(transaction, db, teradataDDL, "t1spec", "Column", 1.000000);

                Assert.assertEquals(teradata, GraphOperation.getResource(teradata));
                Assert.assertEquals(teradata, GraphOperation.getResource(db));
                Assert.assertEquals(teradata, GraphOperation.getResource(t1c1));
                Assert.assertEquals(teradataDDL, GraphOperation.getResource(t1cspec));
                return null;
            }
        });
    }

    @Test(expected = IllegalStateException.class)
    public void testGetResourceMalicous() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex malicous = transaction.addVertex();
                Assert.assertEquals(null, GraphOperation.getResource(malicous));
                return null;
            }
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetResourceNull() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(null, GraphOperation.getResource(null));
                return null;
            }
        });
    }

    /**
     * Test najít resource top předka.
     */
    @Test
    public void testGetTopParentResource() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex teradata = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator().next();
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();

                Vertex root = getSuperRootHandler().getRoot(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex teradataDDL = GraphCreation.createResource(transaction, root, "Teradata DDL", "t", "d", layer, 1.000000);
                Vertex t1cspec = GraphCreation.createNode(transaction, db, teradataDDL, "t1spec", "Column", 1.000000);

                Assert.assertEquals(teradata, GraphOperation.getTopParentResource(teradata));
                Assert.assertEquals(teradata, GraphOperation.getTopParentResource(db));
                Assert.assertEquals(teradata, GraphOperation.getTopParentResource(t1c1));
                Assert.assertEquals(teradata, GraphOperation.getTopParentResource(t1cspec));
                return null;
            }
        });
    }

    @Test(expected = IllegalStateException.class)
    public void testGetTopParentResourceMalicous() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex malicous = transaction.addVertex();
                Assert.assertEquals(null, GraphOperation.getTopParentResource(malicous));
                return null;
            }
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTopParentResourceNull() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(null, GraphOperation.getTopParentResource(null));
                return null;
            }
        });
    }

    /**
     * Najít všechny uzly v podstromě.
     */
    @Test
    public void testGetWholeSubtree() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                List<Vertex> vertices = GraphOperation.getWholeSubtree(db, REVISION_INTERVAL_1_0);
                Assert.assertEquals(2 + 2 + 2, vertices.size());

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                vertices = GraphOperation.getWholeSubtree(table1, REVISION_INTERVAL_1_0);
                Assert.assertEquals(2, vertices.size());

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                vertices = GraphOperation.getWholeSubtree(t1c1, REVISION_INTERVAL_1_0);
                Assert.assertEquals(0, vertices.size());
                return null;
            }
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetWholeSubtreeNull() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                List<Vertex> vertices = GraphOperation.getWholeSubtree(null, REVISION_INTERVAL_1_0);
                Assert.assertEquals(0, vertices.size());
                return null;
            }
        });
    }

    /** Přímí potomci. */
    @Test
    public void testGetDirectChildren() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = transaction.getVertices(NodeProperty.SUPER_ROOT.t(), true).iterator().next();
                List<Vertex> vertices = GraphOperation.getDirectChildren(root, REVISION_INTERVAL_1_0);
                Assert.assertEquals(1, vertices.size());

                Vertex teradata = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator().next();
                vertices = GraphOperation.getDirectChildren(teradata, REVISION_INTERVAL_1_0);
                Assert.assertEquals(1, vertices.size());

                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                vertices = GraphOperation.getDirectChildren(db, REVISION_INTERVAL_1_0);
                Assert.assertEquals(2, vertices.size());

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                vertices = GraphOperation.getDirectChildren(table1, REVISION_INTERVAL_1_0);
                Assert.assertEquals(2, vertices.size());
                return null;
            }
        });
    }

    /** Test jestli je uzel list. */
    @Test
    public void testIsList() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex node = transaction.getVertices(NodeProperty.SUPER_ROOT.t(), true).iterator().next();
                Assert.assertFalse(GraphOperation.isList(node));

                node = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator().next();
                Assert.assertFalse(GraphOperation.isList(node));

                node = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Assert.assertFalse(GraphOperation.isList(node));

                node = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Assert.assertFalse(GraphOperation.isList(node));

                node = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Assert.assertTrue(GraphOperation.isList(node));

                node = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();
                Assert.assertTrue(GraphOperation.isList(node));

                return null;
            }
        });
    }

    /**
     * Test majitele atributu.
     */
    @Test
    public void testGetAttributeOwner() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex attr = GraphCreation.createNodeAttribute(transaction, t1c1, "attr", "value", 1.000000);
                Assert.assertEquals(t1c1, GraphOperation.getAttributeOwner(attr));
                return null;
            }
        });
    }

    /**
     * Test atributu bez majitele.
     */
    @Test(expected = IllegalStateException.class)
    public void testGetAttributeOwnerNoOwner() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex attr = GraphCreation.createNodeAttribute(transaction, t1c1, "attr", "value", 1.000000);
                t1c1.remove();
                Assert.assertEquals(t1c1, GraphOperation.getAttributeOwner(attr));
                return null;
            }
        });
    }

    /**
     * Test atributu s vice majiteli.
     */
    @Test(expected = IllegalStateException.class)
    public void testGetAttributeOwnerMoreOwner() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                Vertex attr = GraphCreation.createNodeAttribute(transaction, t1c1, "attr", "value", 1.000000);
                t1c2.addEdge(EdgeLabel.HAS_ATTRIBUTE.t(), attr);
                Assert.assertEquals(t1c1, GraphOperation.getAttributeOwner(attr));
                return null;
            }
        });
    }

    /**
     * Test atributu, co neni atribut.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetAttributeOwnerNull() {
        Assert.assertEquals(null, GraphOperation.getAttributeOwner(null));
    }

    /**
     * Test atributu s null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetAttributeOwnerNotAttr() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Assert.assertEquals(t1c1, GraphOperation.getAttributeOwner(t1c1));
                return null;
            }
        });
    }

    /**
     * Test potomku, pricemz predek je null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetChildrenWithNameNullVertex() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(null, GraphOperation.getChildrenWithName(null, "name", REVISION_INTERVAL_1_0));
                return null;
            }
        });
    }

    /**
     * Test potomku, pricemz nazev je null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetChildrenWithNameNullName() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Assert.assertEquals(t1c1, GraphOperation.getChildrenWithName(t1c1, null, REVISION_INTERVAL_1_0));
                return null;
            }
        });
    }

    /**
     * Test potomku resource.
     */
    @Test
    public void testGetChildrenWithNameOfResource() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex teradataRes = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator()
                        .next();

                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Assert.assertEquals(Collections.singletonList(db),
                        GraphOperation.getChildrenWithName(teradataRes, "db", REVISION_INTERVAL_1_0));
                return null;
            }
        });
    }

    /**
     * Test potomku uzlu.
     */
    @Test
    public void testGetChildrenWithNameOfNode() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();

                Assert.assertEquals(Collections.singletonList(table1),
                        GraphOperation.getChildrenWithName(db, "table1", REVISION_INTERVAL_1_0));
                return null;
            }
        });
    }

    /**
     * Test neexistujiciho potomka.
     */
    @Test
    public void testGetChildrenWithNameNotExist() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();

                Assert.assertEquals(Collections.emptyList(),
                        GraphOperation.getChildrenWithName(db, "bbbb", REVISION_INTERVAL_1_0));
                return null;
            }
        });
    }

    /**
     * Test vice potomku uzlu.
     */
    @Test
    public void testGetChildrenWithNameMultipleResults() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex table1View = GraphCreation.createNode(transaction, db, "table1", "Vies", 1.000000);

                Set<Vertex> results = new HashSet<Vertex>();
                results.add(table1);
                results.add(table1View);
                Assert.assertEquals(results,
                        new HashSet<Vertex>(GraphOperation.getChildrenWithName(db, "table1", REVISION_INTERVAL_1_0)));
                return null;
            }
        });
    }
    
    /**
     * Test vyhledani vrcholu podle jmena - pozitivni.
     */
    @Test
    public void testGetVerticesWithNameSingleResult() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
            	Set<Vertex> expected = new HashSet<Vertex>();
            	expected.add(transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next());
            	
                Assert.assertEquals(expected, 
                		new HashSet<Vertex>(GraphOperation.getVerticesWithName(transaction, "table1", REVISION_INTERVAL_1_0, 100)));
                return null;
            }
        });
    }
    
    /**
     * Test vyhledani vrcholu podle jmena - negativni.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetVerticesWithNameBlankName() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
            	
                Assert.assertEquals(null, GraphOperation.getVerticesWithName(transaction, " ", REVISION_INTERVAL_1_0, 100));
                return null;
            }
        });
    }

    /**
     *   super root 
     *     |
     *     +--------------------------------+------------------------+
     *     |                                |                        |      
     * <1.0, 1.9>                       <4.0, 6.9>               <2.0, 6.9>
     *    res1                             res1                     res2
     *     |                                |                        |
     *     +                                +                        +---------------+
     *     |                                |                        |               |
     * <1.0, 1.9>                       <4.0, 6.9>               <2.0, 2.9>      <4.0, 6.9>
     *    db1                              db1                      db2             db2
     *     |                                |                        |               |
     *     |                                |                        |               +-------------+
     *     |                                |                        |               |             |
     * <1.0, 1.9>                       <4.0, 6.9>               <2.0, 2.9>      <4.0, 4.9>    <6.0, 6.9>
     *   table1                           table1                   table2          table2        table2
     *     |                                |                        |               |             |
     *     +----------+                     +------------+           |               |             |
     *     |          |                     |            |           |               |             |
     * <1.0, 1.9>  <1.0, 1.9>           <4.0, 6.9>   <4.0, 6.9>  <2.0, 2.9>      <4.0, 4.9>    <6.0, 6.9>
     *    t1c1        t1c2                t1c1         t1c2         t2c1            t2c1          t2c1
     *    
     */
    @Test
    public void testFindEquivalentVertexInRev() {
        cleanGraph();
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex r1_res1 = GraphCreation.createResource(transaction, root, "res1", "r1", "", layer, 1.000000);
                Vertex r1_db1 = GraphCreation.createNode(transaction, r1_res1, "db1", "Database", 1.000000);
                Vertex r1_t1 = GraphCreation.createNode(transaction, r1_db1, "table1", "Table", 1.000000);
                Vertex r1_t1c1 = GraphCreation.createNode(transaction, r1_t1, "t1c1", "Column", 1.000000);
                Vertex r1_t1c2 = GraphCreation.createNode(transaction, r1_t1, "t1c2", "Column", 1.000000);

                RevisionInterval revInt4_6 = new RevisionInterval(4.000000, 6.999999);
                Vertex r46_res1 = GraphCreation.createResource(transaction, root, "res1", "r1", "", layer, revInt4_6);
                Vertex r46_db1 = GraphCreation.createNode(transaction, r46_res1, "db1", "Database", revInt4_6);
                Vertex r46_t1 = GraphCreation.createNode(transaction, r46_db1, "table1", "Table", revInt4_6);
                Vertex r46_t1c1 = GraphCreation.createNode(transaction, r46_t1, "t1c1", "Column", revInt4_6);
                Vertex r46_t1c2 = GraphCreation.createNode(transaction, r46_t1, "t1c2", "Column", revInt4_6);

                RevisionInterval revInt2_6 = new RevisionInterval(2.000000, 6.999999);
                Vertex r26_res2 = GraphCreation.createResource(transaction, root, "res2", "r2", "", layer, revInt2_6);
                Vertex r2_db2 = GraphCreation.createNode(transaction, r26_res2, "db2", "Database", 2.000000);
                Vertex r2_t2 = GraphCreation.createNode(transaction, r2_db2, "table2", "Table", 2.000000);
                Vertex r2_t2c1 = GraphCreation.createNode(transaction, r2_t2, "t2c1", "Column", 2.000000);

                Vertex r46_db2 = GraphCreation.createNode(transaction, r26_res2, "db2", "Database", revInt4_6);
                Vertex r4_t2 = GraphCreation.createNode(transaction, r46_db2, "table2", "Table", 4.000000);
                Vertex r4_t2c1 = GraphCreation.createNode(transaction, r4_t2, "t2c1", "Column", 4.000000);
                Vertex r6_t2 = GraphCreation.createNode(transaction, r46_db2, "table2", "Table", 6.000000);
                Vertex r6_t2c1 = GraphCreation.createNode(transaction, r6_t2, "t2c1", "Column", 6.000000);

                Assert.assertEquals(null, GraphOperation.findEquivalentVertexInRev(r6_t2c1, new RevisionInterval(1.000000)));
                Assert.assertEquals(r2_t2c1,
                        GraphOperation.findEquivalentVertexInRev(r6_t2c1, new RevisionInterval(2.000000)));
                Assert.assertEquals(null, GraphOperation.findEquivalentVertexInRev(r6_t2c1, new RevisionInterval(3.000000)));
                Assert.assertEquals(r4_t2c1,
                        GraphOperation.findEquivalentVertexInRev(r6_t2c1, new RevisionInterval(4.000000)));
                Assert.assertEquals(null, GraphOperation.findEquivalentVertexInRev(r6_t2c1, new RevisionInterval(5.000000)));
                Assert.assertEquals(r6_t2c1,
                        GraphOperation.findEquivalentVertexInRev(r6_t2c1, new RevisionInterval(6.000000)));

                Assert.assertEquals(r1_t1c1,
                        GraphOperation.findEquivalentVertexInRev(r46_t1c1, new RevisionInterval(1.000000)));
                Assert.assertEquals(null, GraphOperation.findEquivalentVertexInRev(r46_t1c1, new RevisionInterval(2.000000)));
                Assert.assertEquals(null, GraphOperation.findEquivalentVertexInRev(r46_t1c1, new RevisionInterval(3.000000)));
                Assert.assertEquals(r46_t1c1,
                        GraphOperation.findEquivalentVertexInRev(r46_t1c1, new RevisionInterval(4.000000)));
                Assert.assertEquals(r46_t1c1,
                        GraphOperation.findEquivalentVertexInRev(r46_t1c1, new RevisionInterval(5.000000)));
                Assert.assertEquals(r46_t1c1,
                        GraphOperation.findEquivalentVertexInRev(r46_t1c1, new RevisionInterval(6.000000)));

                Assert.assertEquals(r1_t1c2,
                        GraphOperation.findEquivalentVertexInRev(r46_t1c2, new RevisionInterval(1.000000)));

                Assert.assertEquals(r4_t2, GraphOperation.findEquivalentVertexInRev(r6_t2, new RevisionInterval(4.000000)));

                Assert.assertEquals(r2_db2, GraphOperation.findEquivalentVertexInRev(r46_db2, new RevisionInterval(2.000000)));

                Assert.assertEquals(r1_res1,
                        GraphOperation.findEquivalentVertexInRev(r46_res1, new RevisionInterval(1.000000)));

                Set<Vertex> originalVertices = new HashSet<>();
                originalVertices.add(r26_res2);
                originalVertices.add(r46_db2);
                originalVertices.add(r6_t2);
                originalVertices.add(r6_t2c1);

                Set<Vertex> expectedVertices = new HashSet<>();
                expectedVertices.add(r4_t2);
                expectedVertices.add(r4_t2c1);

                Assert.assertEquals(expectedVertices, GraphOperation.findEquivalentVertices(originalVertices, 6.000000, 4.000000));

                return null;
            }
        });
    }

    @Test
    public void testGetEdgeFromNodes() {
        createBasicGraph();
        final EdgeIdentification edgeT1c1T1c2 = getDatabaseHolder()
                .runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<EdgeIdentification>() {
                    @Override
                    public EdgeIdentification callMe(TitanTransaction transaction) {
                        Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                        Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                        EdgeIdentification edgeT1c1T1c2 = new EdgeIdentification((Long) t1c1.getId(),
                                (Long) t1c2.getId(), EdgeLabel.DIRECT);

                        Edge edge1 = GraphOperation.getEdge(transaction, edgeT1c1T1c2,
                                new RevisionInterval(0, Integer.MAX_VALUE));
                        Assert.assertEquals(t1c1, edge1.getVertex(Direction.OUT));
                        Assert.assertEquals(t1c2, edge1.getVertex(Direction.IN));
                        Assert.assertEquals("directFlow", edge1.getLabel());

                        Assert.assertNull(GraphOperation.getEdge(transaction, edgeT1c1T1c2,
                                new RevisionInterval(5, Integer.MAX_VALUE)));

                        Assert.assertEquals(edge1,
                                GraphOperation.getEdge(transaction, edgeT1c1T1c2, new RevisionInterval(1)));
                        Assert.assertEquals(edge1,
                                GraphOperation.getEdge(transaction, edgeT1c1T1c2, new RevisionInterval(1, 2)));

                        return edgeT1c1T1c2;
                    }
                });

        // a jeste v jine transakci
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                Edge edge1 = GraphOperation.getEdge(transaction, edgeT1c1T1c2,
                        new RevisionInterval(0, Integer.MAX_VALUE));
                Assert.assertEquals(t1c1, edge1.getVertex(Direction.OUT));
                Assert.assertEquals(t1c2, edge1.getVertex(Direction.IN));
                Assert.assertEquals("directFlow", edge1.getLabel());

                Assert.assertNull(
                        GraphOperation.getEdge(transaction, edgeT1c1T1c2, new RevisionInterval(5, Integer.MAX_VALUE)));

                Assert.assertEquals(edge1, GraphOperation.getEdge(transaction, edgeT1c1T1c2, new RevisionInterval(1)));
                Assert.assertEquals(edge1,
                        GraphOperation.getEdge(transaction, edgeT1c1T1c2, new RevisionInterval(1, 2)));

                return null;
            }
        });
    }

    @Test
    public void testGetEdgeFromEdge() {
        createBasicGraph();
        final EdgeIdentification edgeT1c1T1c2 = getDatabaseHolder()
                .runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<EdgeIdentification>() {
                    @Override
                    public EdgeIdentification callMe(TitanTransaction transaction) {
                        Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                        Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                        List<Edge> edges = GraphOperation.getAdjacentEdges(t1c1, Direction.OUT,
                                RevisionRootHandler.EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT);
                        Assert.assertEquals(1, edges.size());

                        EdgeIdentification edgeT1c1T1c2 = new EdgeIdentification(edges.get(0));

                        Edge edge1 = GraphOperation.getEdge(transaction, edgeT1c1T1c2,
                                RevisionRootHandler.EVERY_REVISION_INTERVAL);
                        Assert.assertEquals(t1c1, edge1.getVertex(Direction.OUT));
                        Assert.assertEquals(t1c2, edge1.getVertex(Direction.IN));
                        Assert.assertEquals("directFlow", edge1.getLabel());

                        Assert.assertNull(GraphOperation.getEdge(transaction, edgeT1c1T1c2,
                                new RevisionInterval(5, Integer.MAX_VALUE)));

                        Assert.assertEquals(edge1,
                                GraphOperation.getEdge(transaction, edgeT1c1T1c2, new RevisionInterval(1)));
                        Assert.assertEquals(edge1,
                                GraphOperation.getEdge(transaction, edgeT1c1T1c2, new RevisionInterval(1, 2)));

                        return edgeT1c1T1c2;
                    }
                });

        // a jeste v jine transakci
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<EdgeIdentification>() {
            @Override
            public EdgeIdentification callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                List<Edge> edges = GraphOperation.getAdjacentEdges(t1c1, Direction.OUT,
                        RevisionRootHandler.EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT);
                Assert.assertEquals(1, edges.size());

                Edge edge1 = GraphOperation.getEdge(transaction, edgeT1c1T1c2,
                        RevisionRootHandler.EVERY_REVISION_INTERVAL);
                Assert.assertEquals(t1c1, edge1.getVertex(Direction.OUT));
                Assert.assertEquals(t1c2, edge1.getVertex(Direction.IN));
                Assert.assertEquals("directFlow", edge1.getLabel());

                Assert.assertNull(
                        GraphOperation.getEdge(transaction, edgeT1c1T1c2, new RevisionInterval(5, Integer.MAX_VALUE)));

                Assert.assertEquals(edge1, GraphOperation.getEdge(transaction, edgeT1c1T1c2, new RevisionInterval(1)));
                Assert.assertEquals(edge1,
                        GraphOperation.getEdge(transaction, edgeT1c1T1c2, new RevisionInterval(1, 2)));

                return null;
            }
        });
    }
}
