package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import static eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.StepTracer.Properties.DIRECTION;
import static eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.StepTracer.Properties.NAME;
import static eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.StepTracer.Properties.REPEAT_INDICATOR;
import static eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.StepTracer.Properties.STEP;
import static eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.StepTracer.Properties.TYPE;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderBfs;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;
import junit.framework.Assert;

/**
 * Třída seskupující jUnit testy pro UniversalTraverser.
 * Jednotlivé metody testují UniversalTraverser s různými nastaveními nad malými umělými grafy,
 * jejichž struktura je naznačena v komentáři, aby bylo možno i rychle pohledově ověřit, zda se traverser chová správně.
 * 
 * @author Erik Kratochvíl
 */
public class UniversalTraverserTest extends TestTitanDatabaseProvider {

    /**
     * Třída k minireprezentaci uzlů v grafu pro rychlejší definice grafu.
     * Každý uzel má jméno a následníky, čímž dobře definuje graf 
     * pro účely testování průchodu pomocí ({@link UniversalTraverser}).
     * 
     * @author Erik Kratochvíl
     */
    static class Node {
        /** Jméno uzlu pro snadnou identifikaci. */
        final String name;
        /** Pole následníků uzlu. */
        final Node[] next;

        Node(final String name, final Node... next) {
            this.name = name;
            this.next = (next != null ? next : new Node[] {});
        }
    }

    /**
     * Syntaktický cukr, abych nemusel všude psát new Node("X"), ale jenom node("X").
     * Java coding conventions trpí, ale líp se mi takto grafy definují.
     * 
     * @param name jméno uzlu
     * @param nodes následníci uzlu
     * @return nový uzel, ekvivalent volání <code>new Node(name, nodes)</code>
     */
    static Node node(final String name, final Node... nodes) {
        return new Node(name, nodes);
    }

    /**
     * Zapouzdření pro překlad grafu definovaného v minireprezentaci pomocí třídy Node do grafu v Titanu.
     * Provádí se tak, že všechny uzly node jsou převedeny na 
     * vertexy s property <code>nodeName</code> = <code>node.name</code>
     * a pověšeny přímo pod vertex SUPER_ROOT.
     * Hrany mezi node a node.next jsou definovány jako directFlow.
     * Odpovídajícím způsobem je tedy nutné nakonfigurovat UniversalTraverser, 
     * tj. aby chodil po directFlow hranách.
     * <br/>
     * Korektně ošetřuje situace, kdy je v grafu referencován uzel ze dvou jiných různých uzlů.
     * <br/>
     * Příklad použití:
     * <pre>
     * final Node e = node("E", node("X"));
     * createGraph(node("A", node("B", node("D"), e), node("C", e, node("F"))));
     * </pre>	
     * Vygeneruje graf následujícího tvaru:
     * <pre>
     *       A
     *      / \
     *     B   C
     *    / \ / \ 
     *   D   E   F
     *       |
     *       X	 
     * </pre>
     * Vertex <code>E</code> je v grafu sdílený/dostupný z vrcholů <code>B</code> a <code>C</code>.
     * 
     * @param node kořen minireprezentace grafu pro překlad do Titan grafu
     */
    void createGraph(final Node node) {
        // Vytvoření transakce pro vytváření objektů v Titan databázi.
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                final Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                final Double revision = getRevisionRootHandler().createMajorRevision(transaction);
                // Překlad grafu.
                convertToGraph(node, null, root, new HashMap<String, Vertex>(), transaction, revision);
                return null;
            }
        });
    }

    /**
     * Vlastní překlad node na vertexy a vytváření hran.
     * Rekurzivně projede všechny node a vytvoří odpovídající graf, tj. vertexy a hrany v Titan databázi.
     * 
     * @param node uzel, který má být v Titanu přeložen na vertex a zakomponován do grafu hranami
     * @param source zdrojový vertex, tj. předchůdce tohoto uzlu
     * @param superRoot obecný SUPER_ROOT v Titan grafu
     * @param memory tabulka uzlů, které už byly přeloženy na vertex (pamatuje se podle jména, case-sensitive)
     * @param transaction write-transakce v Titanu pro přístup do grafu
     * @param revision revize pro vertexy a hrany v Titanu
     */
    private void convertToGraph(final Node node, final Vertex source, final Vertex superRoot,
            final Map<String, Vertex> memory, final TitanTransaction transaction, final Double revision) {
        Vertex vertex = memory.get(node.name);
        // Zapamatuj si, zda vidíme uzel poprvé.
        boolean vertexSeenForTheFirstTime = (vertex == null);
        if (vertexSeenForTheFirstTime) {
            // Přelož node na vertex a zapamatuj si ho podle jména do budoucna.
            vertex = GraphCreation.createNode(transaction, superRoot, node.name, "Column", revision);
            memory.put(node.name, vertex);
        }
        if (source != null) {
            // Udělej mezi source a vertex DIRECT hranu.
            GraphCreation.createEdge(source, vertex, EdgeLabel.DIRECT, revision);
        }
        if (vertexSeenForTheFirstTime) {
            // Rekurzivně zpracuj následníky uzlu.
            for (Node next : node.next) {
                convertToGraph(next, vertex, superRoot, memory, transaction, revision);
            }
        }
    }

    @Test
    public void testTraverseDfsPreOrder() {
        cleanGraph();
        createGraph(node("A", node("B", node("D"), node("E")), node("C", node("F"), node("G"))));
        /*
         *       A
         *      / \
         *     /   \ 
         *    B     C
         *   / \   / \ 
         *  D   E F   G
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.OUT);
        traverser.setEdgeLabels(EdgeLabel.DIRECT);
        traverser.setSearchOrder(new SearchOrderDfs());
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.SUCCESSORS); // pre-order
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        final StepTracer tracer = new StepTracer(STEP, NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "A");
        Assert.assertEquals("[0:A][1:C][2:G][3:F][4:B][5:E][6:D]", tracer.toString());
    }

    @Test
    public void testTraverseDfsPostOrder() {
        cleanGraph();
        createGraph(node("A", node("B", node("D"), node("E")), node("C", node("F"), node("G"))));
        /*
         *       A
         *      / \
         *     /   \ 
         *    B     C
         *   / \   / \ 
         *  D   E F   G
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.OUT);
        traverser.setEdgeLabels(EdgeLabel.DIRECT);
        traverser.setSearchOrder(new SearchOrderDfs());
        traverser.setVisitOrder(VisitedPart.SUCCESSORS, VisitedPart.SELF); // post-order
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        final StepTracer tracer = new StepTracer(STEP, NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "A");
        // System.out.println(tracer);
        Assert.assertEquals("[0:G][1:F][2:C][3:E][4:D][5:B][6:A]", tracer.toString());
    }

    @Test
    public void testTraverseBfsPreOrder() {
        cleanGraph();
        createGraph(node("A", node("B", node("D"), node("E")), node("C", node("F"), node("G"))));
        /*
         *       A
         *      / \
         *     /   \ 
         *    B     C
         *   / \   / \ 
         *  D   E F   G
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.OUT);
        traverser.setEdgeLabels(EdgeLabel.DIRECT);
        traverser.setSearchOrder(new SearchOrderBfs());
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.SUCCESSORS); // pre-order
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        final StepTracer tracer = new StepTracer(STEP, NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "A");
        // System.out.println(tracer);
        Assert.assertEquals("[0:A][1:B][2:C][3:D][4:E][5:F][6:G]", tracer.toString());
    }

    @Test
    public void testTraverseDfsPreOrderWithCycle() {
        cleanGraph();
        final Node e = node("E", node("X"));
        createGraph(node("A", node("B", node("D"), e), node("C", e, node("F"))));
        /*
         *       A
         *      / \
         *     B   C
         *    / \ / \ 
         *   D   E   F
         *       |
         *       X
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.OUT);
        traverser.setEdgeLabels(EdgeLabel.DIRECT);
        traverser.setSearchOrder(new SearchOrderDfs());
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.SUCCESSORS); // pre-order
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        final StepTracer tracer = new StepTracer(STEP, NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "A");
        // System.out.println(tracer);
        Assert.assertEquals("[0:A][1:C][2:F][3:E][4:X][5:B][6:E*][7:D]", tracer.toString());
    }

    @Test
    public void testTraverseDfsPostOrderWithCycle() {
        cleanGraph();
        final Node e = node("E", node("X"));
        createGraph(node("A", node("B", node("D"), e), node("C", e, node("F"))));
        /*
         *       A
         *      / \
         *     B   C
         *    / \ / \ 
         *   D   E   F
         *       |
         *       X
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.OUT);
        traverser.setEdgeLabels(EdgeLabel.DIRECT);
        traverser.setSearchOrder(new SearchOrderDfs());
        traverser.setVisitOrder(VisitedPart.SUCCESSORS, VisitedPart.SELF); // post-order
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        final StepTracer tracer = new StepTracer(STEP, NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "A");
        // System.out.println(tracer);
        Assert.assertEquals("[0:F][1:X][2:E][3:C][4:E*][5:D][6:B][7:A]", tracer.toString());
    }

    @Test
    public void testTraverseBfsPreOrderWithCycle() {
        cleanGraph();
        final Node e = node("E", node("X"));
        createGraph(node("A", node("B", node("D"), e), node("C", e, node("F"))));
        /*
         *       A
         *      / \
         *     B   C
         *    / \ / \ 
         *   D   E   F
         *       |
         *       X
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.OUT);
        traverser.setEdgeLabels(EdgeLabel.DIRECT);
        traverser.setSearchOrder(new SearchOrderBfs());
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.SUCCESSORS); // pre-order
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        final StepTracer tracer = new StepTracer(STEP, NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "A");
        // System.out.println(tracer);
        Assert.assertEquals("[0:A][1:B][2:C][3:D][4:E][5:E*][6:F][7:X]", tracer.toString());
    }

    @Test
    public void testTraverseDfsSelfAttributesSuccessor() {
        cleanGraph();
        createBasicGraph();
        /*
         * [Database:"db"]
         *  |
         *  +--------------------------------------------------+
         *  |                                                  |
         * [Table:"table1"] --- [Attribute:TABLE_TYPE=TABLE]  [Table:"table2"] --- [Attribute:TABLE_TYPE=VIEW]
         *  |                                                  |
         *  +----------------+                                 +----------------+
         *  |                |                                 |                |
         * [Column:"t1c1"]  [Column:"t1c2"]                   [Column:"t2c1"]  [Column:"t2c2"]
         *        |  |          ^     |                           ^  ^  |          ^
         *        |  |          |     |                           |  |  |          |
         *        |  +--direct--+     +--direct-------------------+  |  +--direct--+
         *        |                                                  |
         *        +--filter------------------------------------------+
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.IN); // hasResource a hasParent jsou takto natočeny
        traverser.setEdgeLabels(EdgeLabel.HAS_RESOURCE, EdgeLabel.HAS_PARENT);
        traverser.setSearchOrder(new SearchOrderDfs());
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.ATTRIBUTES, VisitedPart.SUCCESSORS); // pre-order
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        final StepTracer tracer = new StepTracer(TYPE, NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "db");
        // System.out.println(tracer);
        Assert.assertEquals(
                "[DATABASE:db][TABLE:table2][ATTRIBUTE:TABLE_TYPE=VIEW][COLUMN:t2c2][COLUMN:t2c1][TABLE:table1][ATTRIBUTE:TABLE_TYPE=TABLE][COLUMN:t1c2][COLUMN:t1c1]",
                tracer.toString());
    }

    @Test
    public void testTraverseDfsSelfSuccessorAttributes() {
        cleanGraph();
        createBasicGraph();
        /*
         * [Database:"db"]
         *  |
         *  +--------------------------------------------------+
         *  |                                                  |
         * [Table:"table1"] --- [Attribute:TABLE_TYPE=TABLE]  [Table:"table2"] --- [Attribute:TABLE_TYPE=VIEW]
         *  |                                                  |
         *  +----------------+                                 +----------------+
         *  |                |                                 |                |
         * [Column:"t1c1"]  [Column:"t1c2"]                   [Column:"t2c1"]  [Column:"t2c2"]
         *        |  |          ^     |                           ^  ^  |          ^
         *        |  |          |     |                           |  |  |          |
         *        |  +--direct--+     +--direct-------------------+  |  +--direct--+
         *        |                                                  |
         *        +--filter------------------------------------------+
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.IN); // hasResource a hasParent jsou takto natočeny
        traverser.setEdgeLabels(EdgeLabel.HAS_RESOURCE, EdgeLabel.HAS_PARENT);
        traverser.setSearchOrder(new SearchOrderDfs());
        // Sám sebe reportuj pre-order, ale atributy vrcholů post-order.
        // Může dávat smysl, pokud např. potřebuji nejprve něco udělat s vrcholy
        // a pak až jsou všechny upraveny, tak udělat něco s jejich atributy nebo hranami.
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.SUCCESSORS, VisitedPart.ATTRIBUTES); // mix-order
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        final StepTracer tracer = new StepTracer(NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "db");
        // System.out.println(tracer);
        Assert.assertEquals("[db][table2][t2c2][t2c1][TABLE_TYPE=VIEW][table1][t1c2][t1c1][TABLE_TYPE=TABLE]",
                tracer.toString());
    }

    @Test
    public void testTraverseBfsEdgesSuccessor() {
        cleanGraph();
        createBasicGraph();
        /*
         * [Database:"db"]
         *  |
         *  +--------------------------------------------------+
         *  |                                                  |
         * [Table:"table1"] --- [Attribute:TABLE_TYPE=TABLE]  [Table:"table2"] --- [Attribute:TABLE_TYPE=VIEW]
         *  |                                                  |
         *  +----------------+                                 +----------------+
         *  |                |                                 |                |
         * [Column:"t1c1"]  [Column:"t1c2"]                   [Column:"t2c1"]  [Column:"t2c2"]
         *        |  |          ^     |                           ^  ^  |          ^
         *        |  |          |     |                           |  |  |          |
         *        |  +--direct--+     +--direct-------------------+  |  +--direct--+
         *        |                                                  |
         *        +--filter------------------------------------------+
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.IN); // hasResource a hasParent jsou takto natočeny
        traverser.setEdgeLabels(EdgeLabel.HAS_RESOURCE, EdgeLabel.HAS_PARENT);
        traverser.setSearchOrder(new SearchOrderBfs());
        traverser.setVisitOrder(VisitedPart.EDGES, VisitedPart.SUCCESSORS); // pre-order
        traverser.setReportedEdgeLabels(EdgeLabel.DIRECT, EdgeLabel.FILTER);
        traverser.setReportedEdgeDirection(Direction.OUT);
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        final StepTracer tracer = new StepTracer(TYPE, DIRECTION, NAME);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "db");
        Assert.assertEquals(
                "[directFlow:OUT:t1c1->t1c2][filterFlow:OUT:t1c1->t2c1][directFlow:OUT:t1c2->t2c1][directFlow:OUT:t2c1->t2c2]",
                tracer.toString());
    }

    @Test
    public void testTraverseBfsSelfEdgesSuccessor() {
        cleanGraph();
        createBasicGraph();
        /*
         * [Database:"db"]
         *  |
         *  +--------------------------------------------------+
         *  |                                                  |
         * [Table:"table1"] --- [Attribute:TABLE_TYPE=TABLE]  [Table:"table2"] --- [Attribute:TABLE_TYPE=VIEW]
         *  |                                                  |
         *  +----------------+                                 +----------------+
         *  |                |                                 |                |
         * [Column:"t1c1"]  [Column:"t1c2"]                   [Column:"t2c1"]  [Column:"t2c2"]
         *        |  |          ^     |                           ^  ^  |          ^
         *        |  |          |     |                           |  |  |          |
         *        |  +--direct--+     +--direct-------------------+  |  +--direct--+
         *        |                                                  |
         *        +--filter------------------------------------------+
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.IN); // hasResource a hasParent jsou takto natočeny
        traverser.setEdgeLabels(EdgeLabel.HAS_RESOURCE, EdgeLabel.HAS_PARENT);
        traverser.setSearchOrder(new SearchOrderBfs());
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.EDGES, VisitedPart.SUCCESSORS); // pre-order
        traverser.setReportedEdgeLabels(EdgeLabel.DIRECT, EdgeLabel.FILTER); // reportuj visitoru DIRECT a FILTER hrany
        traverser.setReportedEdgeDirection(Direction.OUT); // reportuj visitoru pouze OUT hrany
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        final StepTracer tracer = new StepTracer(TYPE, NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "db");
        // System.out.println(tracer);
        Assert.assertEquals(
                "[DATABASE:db][TABLE:table1][TABLE:table2][COLUMN:t1c1][directFlow:t1c1->t1c2][filterFlow:t1c1->t2c1][COLUMN:t1c2][directFlow:t1c2->t2c1][COLUMN:t2c1][directFlow:t2c1->t2c2][COLUMN:t2c2]",
                tracer.toString());
    }

    @Test
    public void testShouldBeProcessed() {
        cleanGraph();
        final Node e = node("E", node("X"));
        createGraph(node("A", node("B", node("D"), e), node("C", e, node("F"))));
        /*
         *       A
         *      / \
         *     B   C
         *    / \ / \ 
         *   D   E   F
         *       |
         *       X
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.OUT);
        traverser.setEdgeLabels(EdgeLabel.DIRECT);
        traverser.setSearchOrder(new SearchOrderBfs());
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.SUCCESSORS); // pre-order
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        // Vynechej vrcholy obsahující ve jméně E.
        // Jinými slovy, pokud má vrchol ve jméně E, pak ho přeskoč.
        traverser.setShouldVertexBeProcessed(new VertexPredicate() {
            @Override
            public boolean evaluate(Vertex vertex, RevisionInterval revisionInterval) {
                final AboutVertex v = new AboutVertex(vertex);
                return !StringUtils.contains(v.name, "E");
            }
        });
        final StepTracer tracer = new StepTracer(STEP, NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "A");
        Assert.assertEquals("[0:A][1:B][2:C][3:D][4:F]", tracer.toString());
    }

    @Test
    public void testLimitedVertexCount1() {
        cleanGraph();
        final Node e = node("E", node("X"));
        createGraph(node("A", node("B", node("D"), e), node("C", e, node("F"))));
        /*
         *       A
         *      / \
         *     B   C
         *    / \ / \ 
         *   D   E   F
         *       |
         *       X
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.OUT);
        traverser.setEdgeLabels(EdgeLabel.DIRECT);
        traverser.setSearchOrder(new SearchOrderBfs());
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.SUCCESSORS); // pre-order
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        traverser.setMaxVertexCount(6);
        final StepTracer tracer = new StepTracer(STEP, NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "A");
        // [5:E*] se nezapočítává, jde o druhou návštěvu už dříve zpracovaného vrcholu
        Assert.assertEquals("[0:A][1:B][2:C][3:D][4:E][5:E*][6:F]", tracer.toString());
    }

    @Test
    public void testLimitedVertexCount2() {
        cleanGraph();
        final Node e = node("E", node("X"));
        createGraph(node("A", node("B", node("D"), e), node("C", e, node("F"))));
        /*
         *       A
         *      / \
         *     B   C
         *    / \ / \ 
         *   D   E   F
         *       |
         *       X
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.OUT);
        traverser.setEdgeLabels(EdgeLabel.DIRECT);
        traverser.setSearchOrder(new SearchOrderBfs());
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.SUCCESSORS); // pre-order
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        traverser.setMaxVertexCount(3);
        final StepTracer tracer = new StepTracer(STEP, NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "A");
        Assert.assertEquals("[0:A][1:B][2:C]", tracer.toString());
    }

    @Test
    public void testLimitedDistance() {
        cleanGraph();
        final Node e = node("E", node("X"));
        createGraph(node("A", node("B", node("D"), e), node("C", e, node("F"))));
        /*
         *       A
         *      / \
         *     B   C
         *    / \ / \ 
         *   D   E   F
         *       |
         *       X
         */

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.OUT);
        traverser.setEdgeLabels(EdgeLabel.DIRECT);
        traverser.setSearchOrder(new SearchOrderBfs());
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.SUCCESSORS); // pre-order
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        traverser.setMaxDistance(2);
        final StepTracer tracer = new StepTracer(NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser, "A");
        Assert.assertEquals("[A][B][C][D][E][E*][F]", tracer.toString());
    }

    /**
     * Spustí traverser od vertexu daného jména.
     * Procházení běží v <b>read-transakci</b> Titanu!
     * Tzn. nepokoušet se ve visitoru zapisovat.
     * 
     * @param traverser traverser ke spuštění
     * @param nodeName jméno (obsah property "nodeName") výchozího uzlu
     */
    void startTraverser(final UniversalTraverser traverser, final String nodeName) {
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                final Vertex source = transaction.getVertices(NodeProperty.NODE_NAME.t(), nodeName).iterator().next();
                traverser.go(source);
                return null;
            }
        });
    }

    /**
     * Ujištění se, že jsem po sobě uklidili.
     */
    @After
    public void tearDown() {
        cleanGraph();
    }

}
