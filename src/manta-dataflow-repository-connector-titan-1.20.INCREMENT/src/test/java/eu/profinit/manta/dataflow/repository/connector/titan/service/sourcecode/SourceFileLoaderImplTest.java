package eu.profinit.manta.dataflow.repository.connector.titan.service.sourcecode;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import org.junit.Assert;

/**
 * Unit testy {@link SourceFileLoaderImpl}.
 * 
 * @author onouza
 */
public class SourceFileLoaderImplTest {

    /**
     * Instance testovane tridy
     */
    private SourceFileLoaderImpl sourceFileLoaderImpl;
    
    @Before
    public void setUp() throws URISyntaxException {
        sourceFileLoaderImpl = new SourceFileLoaderImpl();
        
        // Urcime koren zdrojovych souboru na zaklade umisteni jednoho z nich
        URL sourceFileUrl = SourceCodeServiceImpl.class.getResource("barvy.txt");
        File sourceFile = new File(sourceFileUrl.toURI());

        SourceRootHandler sourceRootHandler = new SourceRootHandler();
        sourceRootHandler.setSourceCodeDir(sourceFile.getParentFile());
        ReflectionTestUtils.setField(sourceFileLoaderImpl, "sourceRootHandler", sourceRootHandler );
    }

    /**
     * Otestuje {@link SourceFileLoaderImpl#loadSourceFile(SourceFile)}.
     */
    @Test
    public void testLoadSourceFile() {
        SourceFile sourceFile = new SourceFile("barvy.txt", "UTF-8");
        List<String> lines = sourceFileLoaderImpl.loadSourceFile(sourceFile);
        
        // Kontroly spravneho nacteni obsahu souboru "barvy.txt"  
        Assert.assertNotNull(lines);
        Assert.assertEquals("ruzovoucka", lines.get(3));
        Assert.assertEquals(5, lines.size());
    }
}
