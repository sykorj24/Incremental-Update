package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

/**
 * Testovací visitor, spočítající objekty.
 * @author tfechtner
 *
 */
class TestGraphVisitor implements GraphVisitor {

    private int layerCount;
    private int nodeCount;
    private int edgeCount;
    private int resourceCount;
    private int attributeCount;

    @Override
    public void visitLayer(Vertex layer) {
        layerCount++;        
    }

    @Override
    public void visitResource(Vertex resource) {
        resourceCount++;        
    }

    @Override
    public void visitNode(Vertex node) {
        nodeCount++;
    }

    @Override
    public void visitAttribute(Vertex attribute) {
        attributeCount++;        
    }

    @Override
    public void visitEdge(Edge edge) {
        edgeCount++;
    }

    /**
     * @return pocet vrstev
     */
    public int getLayerCount() {
        return layerCount;
    }

    /**
     * @return počet uzlů
     */
    public int getNodeCount() {
        return nodeCount;
    }

    /**
     * @return počet hran
     */
    public int getEdgeCount() {
        return edgeCount;
    }

    /**
     * @return počet resource
     */
    public int getResourceCount() {
        return resourceCount;
    }

    /**
     * @return počet atributů
     */
    public int getAttributeCount() {
        return attributeCount;
    }
    
}