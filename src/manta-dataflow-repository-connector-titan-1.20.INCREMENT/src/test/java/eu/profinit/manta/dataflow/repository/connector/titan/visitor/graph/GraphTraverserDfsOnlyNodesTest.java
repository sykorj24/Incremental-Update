package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

/**
 * Test dfs traverseru procházející jen uzly.
 * @author tfechtner
 *
 */
public class GraphTraverserDfsOnlyNodesTest extends TestTitanDatabaseProvider {
    private static final int NODE_COUNT = 7;

    /**
     * Test procházení grafem.
     */
    @Test
    public void testTraverse() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex teraDdl = GraphCreation.createResource(transaction, root, "Teradata DDL", "tt", "td", layer, REVISION_1_000000);
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                GraphCreation.createNode(transaction, table1, teraDdl, "tspec", "Column", REVISION_1_000000);

                GraphTraverser traverser = new GraphTraverserDfsOnlyNodes();

                TestGraphVisitor visitor = new TestGraphVisitor();
                traverser.traverse(visitor, root, REVISION_INTERVAL_1_0);
                Assert.assertEquals(NODE_COUNT + 1, visitor.getNodeCount());
                Assert.assertEquals(0, visitor.getEdgeCount());
                Assert.assertEquals(2, visitor.getLayerCount());
                Assert.assertEquals(2, visitor.getResourceCount());
                Assert.assertEquals(0, visitor.getAttributeCount());
                return null;
            }
        });
    }
}
