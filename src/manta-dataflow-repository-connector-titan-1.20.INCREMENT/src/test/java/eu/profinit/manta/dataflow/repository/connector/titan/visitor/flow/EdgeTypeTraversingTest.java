package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

/**
 * Test hledaná dalších hran při traverzování.
 * @author tfechtner
 *
 */
public class EdgeTypeTraversingTest extends TestTitanDatabaseProvider {
    /**
     * Direkt hrany.
     */
    @Test
    public void testGetNextVerticesDirect() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Assert.assertEquals("t1c2", EdgeTypeTraversing.DIRECT.getNextVertices(t2c1, Direction.IN, REVISION_INTERVAL_1_0).iterator()
                        .next().getProperty(NodeProperty.NODE_NAME.t()));
                return null;
            }
        });

    }

    /**
     * Filter hrany.
     */
    @Test
    public void testGetNextVerticesFilter() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Assert.assertEquals("t1c1", EdgeTypeTraversing.FILTER.getNextVertices(t2c1, Direction.IN, REVISION_INTERVAL_1_0).iterator()
                        .next().getProperty(NodeProperty.NODE_NAME.t()));
                return null;
            }
        });
    }

    /**
     * Všechny hrany.
     */
    @Test
    public void testGetNextVerticesBoth() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                int i = 0;
                Iterable<Vertex> vertices = EdgeTypeTraversing.BOTH.getNextVertices(t2c1, Direction.IN, REVISION_INTERVAL_1_0);
                for (@SuppressWarnings("unused")
                Vertex v : vertices) {
                    i++;
                }
                Assert.assertEquals(2, i);
                return null;
            }
        });

    }
}
