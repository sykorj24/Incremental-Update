package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow;

import java.util.Collections;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

/**
 * 
 * @author tfechtner
 *
 */
public class FlowTraverserDfsTest extends TestTitanDatabaseProvider {
    /**
     * Testování procházení.
     */
    @Test
    public void testTraverse() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();

                FlowTraverser traverser = new FlowTraverserDfs();

                TestFlowVisitor visitor = new TestFlowVisitor();
                traverser.traverse(visitor, transaction, Collections.singleton(t2c1.getId()), EdgeTypeTraversing.BOTH,
                        Direction.IN, REVISION_INTERVAL_1_0);
                Assert.assertEquals(2 + 1, visitor.getNodeCount());

                visitor = new TestFlowVisitor();
                traverser.traverse(visitor, transaction, Collections.singleton(t2c1.getId()), EdgeTypeTraversing.DIRECT,
                        Direction.IN, REVISION_INTERVAL_1_0);
                Assert.assertEquals(2 + 1, visitor.getNodeCount());

                visitor = new TestFlowVisitor();
                traverser.traverse(visitor, transaction, Collections.singleton(t2c1.getId()), EdgeTypeTraversing.DIRECT,
                        Direction.BOTH, REVISION_INTERVAL_1_0);
                Assert.assertEquals(2 + 2 + 1, visitor.getNodeCount());

                visitor = new TestFlowVisitor();
                traverser.traverse(visitor, transaction, Collections.singleton(t2c1.getId()), EdgeTypeTraversing.FILTER,
                        Direction.IN, REVISION_INTERVAL_1_0);
                Assert.assertEquals(1 + 1, visitor.getNodeCount());

                visitor = new TestFlowVisitor();
                traverser.traverse(visitor, transaction, Collections.singleton(t2c1.getId()), EdgeTypeTraversing.BOTH,
                        Direction.OUT, REVISION_INTERVAL_1_0);
                Assert.assertEquals(1 + 1, visitor.getNodeCount());
                return null;
            }
        });
    }
}
