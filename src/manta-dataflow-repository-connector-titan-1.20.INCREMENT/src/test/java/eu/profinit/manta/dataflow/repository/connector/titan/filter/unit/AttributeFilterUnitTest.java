package eu.profinit.manta.dataflow.repository.connector.titan.filter.unit;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

public class AttributeFilterUnitTest extends TestTitanDatabaseProvider {
    @Test
    public void testAllTypes() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                AttributeFilterUnit attrFilter = new AttributeFilterUnit(null, null, "TABLE_TYPE",
                        Collections.singleton("VIEW"));

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Assert.assertFalse(attrFilter.isFiltered(t1c1, RevisionRootHandler.EVERY_REVISION_INTERVAL));

                Vertex t1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Assert.assertFalse(attrFilter.isFiltered(t1, RevisionRootHandler.EVERY_REVISION_INTERVAL));

                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Assert.assertTrue(attrFilter.isFiltered(t2c1, RevisionRootHandler.EVERY_REVISION_INTERVAL));

                Vertex t2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                Assert.assertTrue(attrFilter.isFiltered(t2, RevisionRootHandler.EVERY_REVISION_INTERVAL));

                return null;
            }
        });
    }

    @Test
    public void testOnlyTables() {
        cleanGraph();
        final double revision = createMajorRevision();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata", "Teradata", "", layer, revision);
                Vertex db = GraphCreation.createNode(transaction, resource, "db", "Database", revision);
                Vertex table = GraphCreation.createNode(transaction, db, "table", "Table", revision);
                Vertex view = GraphCreation.createNode(transaction, db, "view", "View", revision);
                Vertex t1c1 = GraphCreation.createNode(transaction, table, "t1c1", "Column", revision);
                Vertex v1c1 = GraphCreation.createNode(transaction, view, "v1c1", "Column", revision);

                GraphCreation.createNodeAttribute(transaction, table, "a1", "v1", 1.000000);
                GraphCreation.createNodeAttribute(transaction, view, "a1", "v1", 1.000000);

                AttributeFilterUnit attrFilter = new AttributeFilterUnit(null, Collections.singleton("Table"), "a1",
                        Collections.singleton("v1"));

                Assert.assertTrue(attrFilter.isFiltered(t1c1, RevisionRootHandler.EVERY_REVISION_INTERVAL));
                Assert.assertTrue(attrFilter.isFiltered(table, RevisionRootHandler.EVERY_REVISION_INTERVAL));

                Assert.assertFalse(attrFilter.isFiltered(v1c1, RevisionRootHandler.EVERY_REVISION_INTERVAL));
                Assert.assertFalse(attrFilter.isFiltered(view, RevisionRootHandler.EVERY_REVISION_INTERVAL));

                return null;
            }
        });
    }

    @Test
    public void testOnlyResources() {
        cleanGraph();
        final double revision = createMajorRevision();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex resource1 = GraphCreation.createResource(transaction, root, "Teradata", "Teradata", "",
                        layer, revision);
                Vertex db1 = GraphCreation.createNode(transaction, resource1, "db", "Database", revision);
                GraphCreation.createNodeAttribute(transaction, db1, "a1", "v1", 1.000000);

                Vertex resource2 = GraphCreation.createResource(transaction, root, "Oracle", "Oracle", "", layer, revision);
                Vertex db2 = GraphCreation.createNode(transaction, resource2, "db", "Database", revision);
                GraphCreation.createNodeAttribute(transaction, db2, "a1", "v1", 1.000000);

                AttributeFilterUnit attrFilter = new AttributeFilterUnit(Collections.singleton("Teradata"), null, "a1",
                        Collections.singleton("v1"));

                Assert.assertTrue(attrFilter.isFiltered(db1, RevisionRootHandler.EVERY_REVISION_INTERVAL));
                Assert.assertFalse(attrFilter.isFiltered(db2, RevisionRootHandler.EVERY_REVISION_INTERVAL));
                return null;
            }
        });
    }
}
