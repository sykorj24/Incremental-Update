package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import static eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.StepTracer.Properties.NAME;
import static eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.StepTracer.Properties.REPEAT_INDICATOR;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.predicate.PreferredEdgeTypePredicate;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderBfs;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

/**
 * jUnit testy pro {@link PreferredEdgeTypePredicate}.
 *
 * @author Erik Kratochvíl
 */
public class PreferredEdgeTypePredicateTest extends TestTitanDatabaseProvider {

    /**
     * Vytvoří graf, kde uzel "X" má hranu HAS_PARENT i HAS_RESOURCE.
     * Průchod přes hranu HAS_RESOURCE z RESOURCE "B" do uzlu "X"
     * by měl být predikátem {@link PreferredEdgeTypePredicate} zablokován.
     * <pre>
     *  SUPER_ROOT
     *   |
     *   +----------------+
     *   |                |
     *  RESOURCE "A"     RESOURCE "B"
     *   |                ^
     *  Database "D"      |
     *   |                |
     *  Table "X" --------+
     * </pre>
     */
    void createGraph() {
        // Vytvoření transakce pro vytváření objektů v Titan databázi.
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(final TitanTransaction transaction) {
                final Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                getRevisionRootHandler().createMajorRevision(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                final Vertex resourceA = GraphCreation.createResource(transaction, root, "A", "", "", layer, 1.000000);
                final Vertex resourceB = GraphCreation.createResource(transaction, root, "B", "", "", layer, 1.000000);
                final Vertex database = GraphCreation.createNode(transaction, resourceA, "D", "Database", 1.000000);
                final Vertex x = GraphCreation.createNode(transaction, database, "X", "Table", 1.000000);
                GraphCreation.createEdge(x, resourceB, EdgeLabel.HAS_RESOURCE, 1.000000);

                return null;
            }
        });
    }

    /**
     * Průchod přes hranu HAS_RESOURCE z RESOURCE "A" do uzlu "D"
     * by měl být predikátem {@link PreferredEdgeTypePredicate} povolen.
     * <pre>
     *  SUPER_ROOT
     *   |
     *   +----------------+
     *   |                |
     *  RESOURCE "A"     RESOURCE "B"
     *   |                ^
     *  Database "D"      |
     *   |                |
     *  Table "X" --------+
     * </pre>
     */
    @Test
    public void allow() {
        cleanGraph();
        createGraph();
        Assert.assertTrue(go("A"));

    }

    /**
     * Průchod přes hranu HAS_RESOURCE z RESOURCE "B" do uzlu "X"
     * by měl být predikátem {@link PreferredEdgeTypePredicate} zablokován.
     * <pre>
     *  SUPER_ROOT
     *   |
     *   +----------------+
     *   |                |
     *  RESOURCE "A"     RESOURCE "B"
     *   |                ^
     *  Database "D"      |
     *   |                |
     *  Table "X" --------+
     * </pre>
     */
    @Test
    public void block() {
        cleanGraph();
        createGraph();
        Assert.assertTrue(!go("B"));
    }

    /**
     * Spustí {@link PreferredEdgeTypePredicate} na první hranu vedoucí z resource daného jména.
     *
     * @param resourceName jméno resource
     * @return true nebo false, podle toho, zda predikát na první hraně vedoucí z resource daného jména uspěje nebo ne
     */
    boolean go(final String resourceName) {
        return getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Boolean>() {
            @Override
            public Boolean callMe(final TitanTransaction transaction) {
                final Vertex resource = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), resourceName).iterator()
                        .next();

                final Edge edge = resource.getEdges(Direction.IN, EdgeLabel.HAS_RESOURCE.t()).iterator().next();
                return PreferredEdgeTypePredicate.PREFER_PARENT_OVER_RESOURCE.evaluate(edge, Direction.IN);
            }
        });
    }

    /**
     * DFS průchod grafem ze SUPER_ROOT s použitím predikátu
     * PreferredEdgeTypePredicate.PREFER_PARENT_OVER_RESOURCE-
     *
     * <pre>
     *  SUPER_ROOT
     *   |
     *   +----------------+
     *   |                |
     *  RESOURCE "A"     RESOURCE "B"
     *   |                ^
     *  Database "D"      |
     *   |                |
     *  Table "X" --------+
     * </pre>
     */
    @Test
    public void testTraverserWithPrefferedEdgeTypePredicateDfs() {
        cleanGraph();
        createGraph();

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.IN); // hasResource a hasParent jsou takto natočeny
        traverser.setEdgeLabels(EdgeLabel.HAS_RESOURCE, EdgeLabel.HAS_PARENT);
        traverser.setSearchOrder(new SearchOrderDfs());
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.SUCCESSORS);
        traverser.setShouldEdgeBeTraversed(PreferredEdgeTypePredicate.PREFER_PARENT_OVER_RESOURCE);
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        final StepTracer tracer = new StepTracer(NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser);
        System.out.println(tracer);
        Assert.assertEquals("[SUPER_ROOT][B][A][D][X]", tracer.toString());
    }

    /**
     * BFS průchod grafem ze SUPER_ROOT s použitím predikátu
     * PreferredEdgeTypePredicate.PREFER_PARENT_OVER_RESOURCE-
     *
     * <pre>
     *  SUPER_ROOT
     *   |
     *   +----------------+
     *   |                |
     *  RESOURCE "A"     RESOURCE "B"
     *   |                ^
     *  Database "D"      |
     *   |                |
     *  Table "X" --------+
     * </pre>
     */
    @Test
    public void testTraverserWithPrefferedEdgeTypePredicateBfs() {
        cleanGraph();
        createGraph();

        final UniversalTraverser traverser = new UniversalTraverser();
        traverser.setDirection(Direction.IN); // hasResource a hasParent jsou takto natočeny
        traverser.setEdgeLabels(EdgeLabel.HAS_RESOURCE, EdgeLabel.HAS_PARENT);
        traverser.setSearchOrder(new SearchOrderBfs());
        traverser.setVisitOrder(VisitedPart.SELF, VisitedPart.SUCCESSORS);
        traverser.setShouldEdgeBeTraversed(PreferredEdgeTypePredicate.PREFER_PARENT_OVER_RESOURCE);
        traverser.setRevisionInterval(new RevisionInterval(0, 1000));
        final StepTracer tracer = new StepTracer(NAME, REPEAT_INDICATOR);
        traverser.setVisitor(tracer);
        startTraverser(traverser);
        System.out.println(tracer);
        Assert.assertEquals("[SUPER_ROOT][A][B][D][X]", tracer.toString());
    }

    /**
     * Spustí traverser ze SUPER_ROOTu.
     * Procházení běží v <b>read-transakci</b> Titanu!
     * Tzn. nepokoušet se ve visitoru zapisovat.
     *
     * @param traverser traverser ke spuštění
     */
    void startTraverser(final UniversalTraverser traverser) {
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(final TitanTransaction transaction) {
                final Vertex superRoot = getSuperRootHandler().getRoot(transaction);
                traverser.go(superRoot);
                return null;
            }
        });
    }

}
