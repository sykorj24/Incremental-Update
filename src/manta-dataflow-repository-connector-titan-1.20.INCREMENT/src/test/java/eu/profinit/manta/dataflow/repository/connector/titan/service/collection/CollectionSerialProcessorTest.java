package eu.profinit.manta.dataflow.repository.connector.titan.service.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

public class CollectionSerialProcessorTest extends TestTitanDatabaseProvider {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(CollectionParallelProcessorTest.class);

    @Test
    public void testProcessing() {
        cleanGraph();
        createBasicGraph();

        CollectionSerialProcessor processor = new CollectionSerialProcessor();
        processor.setElementPerTransaction(2);

        Collection<String> collection = new ArrayList<>();
        collection.add("t1c1");
        collection.add("t1c2");
        collection.add("t2c1");
        collection.add("t2c2");
        collection.add("table1");
        collection.add("table2");

        TestResultHolder resultHolder = new TestResultHolder();
        CollectionProcessorCallback<String, TestResultHolder> callback = new CollectionProcessorCallback<String, TestResultHolder>() {

            @Override
            public void processElement(TitanTransaction titanTransaction, String element,
                    TestResultHolder resultHolder) {
                Vertex vertex = titanTransaction.getVertices(NodeProperty.NODE_NAME.t(), element).iterator().next();
                resultHolder.add(element, (Long) vertex.getId());
                LOGGER.info("thread {} in transaction {} processed vertex {}.", Thread.currentThread().getName(),
                        System.identityHashCode(titanTransaction), element);
            }
        };

        processor.processCollection(getDatabaseHolder(), TransactionLevel.READ, collection, callback, resultHolder);
        Assert.assertEquals(6, resultHolder.getMap().size());

    }

    private static class TestResultHolder {
        private Map<String, Long> map = new HashMap<>();

        public void add(String name, Long id) {
            map.put(name, id);
        }

        public Map<String, Long> getMap() {
            return map;
        }
    }
}
