package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.AttributeNames;
import eu.profinit.manta.dataflow.model.restriction.ComparisonOperator;
import eu.profinit.manta.dataflow.model.restriction.nodetypes.RestrComparisonNode;
import eu.profinit.manta.dataflow.model.restriction.nodetypes.RestrConjunctionElementary;
import eu.profinit.manta.dataflow.model.restriction.nodetypes.RestrDisjunctionTypeDnf;
import eu.profinit.manta.dataflow.model.restriction.nodetypes.RestrEnumNode;
import eu.profinit.manta.dataflow.model.restriction.nodetypes.RestrIsNullNode;
import eu.profinit.manta.dataflow.model.restriction.variabletypes.RestrVariableAbstract;
import eu.profinit.manta.dataflow.model.restriction.variabletypes.RestrVariableSegments;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.restriction.RestrGraphFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterResult;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.GraphFlowFilter;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

public class RestrGraphFlowAlgorithmTest extends TestTitanDatabaseProvider {

    private RestrGraphFlowAlgorithm algorithm;

    @Before
    public void initAlgorithm() {
        algorithm = new RestrGraphFlowAlgorithm();
        algorithm.setDatabaseResources(new HashSet<String>(Arrays.asList("Oracle", "Teradata")));
    }

    @Test
    public void testBasicAlgorithm1() {
        cleanGraph();
        createRestrBasicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex client = transaction.getVertices(NodeProperty.NODE_NAME.t(), "CLIENT").iterator().next();
                Vertex employee = transaction.getVertices(NodeProperty.NODE_NAME.t(), "EMPLOYEE").iterator().next();
                Vertex report1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "REPORT_TABLE_1").iterator()
                        .next();
                Vertex report2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "REPORT_TABLE_2").iterator()
                        .next();
                Vertex party = transaction.getVertices(NodeProperty.NODE_NAME.t(), "PARTY").iterator().next();

                Set<Object> allowedNodes = null;

                Set<Vertex> startNodes = new HashSet<Vertex>();
                startNodes.add(GraphOperation.getDirectChildren(client, REVISION_INTERVAL_1_0).iterator().next());
                startNodes.add(GraphOperation.getDirectChildren(employee, REVISION_INTERVAL_1_0).iterator().next());
                Set<Object> outputNodes = new HashSet<Object>();
                try {
                    outputNodes.addAll(algorithm.findNodesByAlgorithm(startNodes, allowedNodes, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0));
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(5 + 4, outputNodes.size());
                Assert.assertTrue(outputNodes.contains(
                        GraphOperation.getDirectChildren(party, REVISION_INTERVAL_1_0).iterator().next().getId()));
                Assert.assertTrue(outputNodes.contains(
                        GraphOperation.getDirectChildren(report1, REVISION_INTERVAL_1_0).iterator().next().getId()));
                Assert.assertFalse(outputNodes.contains(
                        GraphOperation.getDirectChildren(report2, REVISION_INTERVAL_1_0).iterator().next().getId()));

                return null;
            }
        });
    }

    @Test
    public void testBasicAlgorithm2() {
        cleanGraph();
        createRestrBasicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex client = transaction.getVertices(NodeProperty.NODE_NAME.t(), "CLIENT").iterator().next();
                Vertex branch = transaction.getVertices(NodeProperty.NODE_NAME.t(), "BRANCH").iterator().next();
                Vertex report2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "REPORT_TABLE_2").iterator()
                        .next();
                Vertex party = transaction.getVertices(NodeProperty.NODE_NAME.t(), "PARTY").iterator().next();

                Set<Object> allowedNodes = null;

                Set<Vertex> startNodes = new HashSet<Vertex>();
                startNodes.add(GraphOperation.getDirectChildren(report2, REVISION_INTERVAL_1_0).iterator().next());
                Set<Object> outputNodes = new HashSet<Object>();
                try {
                    outputNodes.addAll(algorithm.findNodesByAlgorithm(startNodes, allowedNodes, transaction,
                            Direction.IN, REVISION_INTERVAL_1_0));
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(4 + 3, outputNodes.size());
                Assert.assertTrue(outputNodes.contains(
                        GraphOperation.getDirectChildren(party, REVISION_INTERVAL_1_0).iterator().next().getId()));
                Assert.assertTrue(outputNodes.contains(
                        GraphOperation.getDirectChildren(branch, REVISION_INTERVAL_1_0).iterator().next().getId()));
                Assert.assertFalse(outputNodes.contains(
                        GraphOperation.getDirectChildren(client, REVISION_INTERVAL_1_0).iterator().next().getId()));

                return null;
            }
        });
    }

    @Test
    public void testBasicAlgorithmAllNodes() {
        cleanGraph();
        createRestrBasicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex party = transaction.getVertices(NodeProperty.NODE_NAME.t(), "PARTY").iterator().next();

                Set<Object> allowedNodes = new HashSet<Object>();
                for (Vertex v : transaction.getVertices()) {
                    allowedNodes.add(v.getId());
                }

                Set<Vertex> startNodes = new HashSet<Vertex>();
                startNodes.add(GraphOperation.getDirectChildren(party, REVISION_INTERVAL_1_0).iterator().next());
                Set<Object> outputNodes = new HashSet<Object>();
                try {
                    outputNodes.addAll(algorithm.findNodesByAlgorithm(startNodes, allowedNodes, transaction,
                            Direction.IN, REVISION_INTERVAL_1_0));
                    outputNodes.addAll(algorithm.findNodesByAlgorithm(startNodes, allowedNodes, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0));
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(9 + 8, outputNodes.size());

                return null;
            }
        });
    }

    @Test
    public void testBasicAlgorithmSomeNodes() {
        cleanGraph();
        createRestrBasicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex party = transaction.getVertices(NodeProperty.NODE_NAME.t(), "PARTY").iterator().next();
                Vertex employee = transaction.getVertices(NodeProperty.NODE_NAME.t(), "EMPLOYEE").iterator().next();
                Vertex branchTable = transaction.getVertices(NodeProperty.NODE_NAME.t(), "BRANCH_TABLE").iterator()
                        .next();
                Vertex report1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "REPORT_TABLE_1").iterator()
                        .next();

                Set<Object> allowedNodes = new HashSet<Object>();
                allowedNodes
                        .add(GraphOperation.getDirectChildren(party, REVISION_INTERVAL_1_0).iterator().next().getId());
                allowedNodes
                        .add(GraphOperation.getDirectChildren(employee, REVISION_INTERVAL_1_0).iterator().next().getId());
                allowedNodes.add(
                        GraphOperation.getDirectChildren(branchTable, REVISION_INTERVAL_1_0).iterator().next().getId());
                // zamerne vynechana EMPL_TABLE
                allowedNodes.add(GraphOperation.getDirectChildren(report1, REVISION_INTERVAL_1_0).iterator().next());
                for (Vertex v : transaction.getVertices()) {
                    if (GraphOperation.getName(v).startsWith("prenos")) {
                        allowedNodes.add(v.getId());
                    }
                }

                Set<Vertex> startNodes = new HashSet<Vertex>();
                startNodes.add(GraphOperation.getDirectChildren(employee, REVISION_INTERVAL_1_0).iterator().next());
                Set<Object> outputNodes = new HashSet<Object>();
                try {
                    outputNodes.addAll(algorithm.findNodesByAlgorithm(startNodes, allowedNodes, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0));
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(2 + 1, outputNodes.size());

                return null;
            }
        });
    }

    @Ignore
    @Test
    public void testAdvancedGraph() {
        cleanGraph();
        createRestrAdvancedGraph();
        // rozšířený algoritmus zatím nemáme, takže tohle jen zobrazit v prohlížeči
    }

    @Test
    public void testMeaninglessGraph1() {
        cleanGraph();
        createRestrMeaninglessGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex a = transaction.getVertices(NodeProperty.NODE_NAME.t(), "a").iterator().next();

                Set<Object> allowedNodes = null;

                Set<Vertex> startNodes = new HashSet<Vertex>();
                startNodes.add(a);
                Set<Object> outputNodes = new HashSet<Object>();
                try {
                    outputNodes.addAll(algorithm.findNodesByAlgorithm(startNodes, allowedNodes, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0));
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(3, outputNodes.size());

                return null;
            }
        });
    }

    @Test
    public void testMeaninglessGraph2() {
        cleanGraph();
        createRestrMeaninglessGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex c = transaction.getVertices(NodeProperty.NODE_NAME.t(), "c").iterator().next();

                Set<Object> allowedNodes = null;

                Set<Vertex> startNodes = new HashSet<Vertex>();
                startNodes.add(c);
                Set<Object> outputNodes = new HashSet<Object>();
                try {
                    outputNodes.addAll(algorithm.findNodesByAlgorithm(startNodes, allowedNodes, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0));
                    outputNodes.addAll(algorithm.findNodesByAlgorithm(startNodes, allowedNodes, transaction,
                            Direction.IN, REVISION_INTERVAL_1_0));
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(7, outputNodes.size());

                return null;
            }
        });
    }

    @Test
    public void testMoreTransformationsInRow1() {
        cleanGraph();
        createRestrGraphMoreTransformationsInRow(false, false);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();

                Set<Object> outputNodes;
                try {
                    outputNodes = algorithm.findNodesByAlgorithm(Collections.singletonList(t1c1), null, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0);
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(5, outputNodes.size());
                Assert.assertEquals(createSet("t1c1", "i1c1", "i2c1", "i3c1", "t2c1"),
                        translateIdsToNames(outputNodes, transaction));

                return null;
            }
        });
    }

    @Test
    public void testMoreTransformationsInRow2() {
        cleanGraph();
        createRestrGraphMoreTransformationsInRow(true, false);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ_NOT_BLOCKING, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();

                Set<Object> outputNodes;
                try {
                    outputNodes = algorithm.findNodesByAlgorithm(Collections.singletonList(t1c1), null, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0);
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(6, outputNodes.size());
                Assert.assertEquals(6, translateIdsToNames(outputNodes, transaction).size());
                Assert.assertEquals(createSet("t1c1", "i1c1", "i2c1", "i3c1", "t2c1", "i4c1"),
                        translateIdsToNames(outputNodes, transaction));

                return null;
            }
        });
    }

    @Test
    public void testMoreTransformationsInRow3() {
        cleanGraph();
        createRestrGraphMoreTransformationsInRow(false, true);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();

                Set<Object> outputNodes;
                try {
                    outputNodes = algorithm.findNodesByAlgorithm(Collections.singletonList(t1c1), null, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0);
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(6, outputNodes.size());
                Assert.assertEquals(createSet("t1c1", "i1c1", "i2c1", "i3c1", "t2c1", "t3c1"),
                        translateIdsToNames(outputNodes, transaction));

                return null;
            }
        });
    }

    @Test
    public void testMoreTransformationsInRow4() {
        cleanGraph();
        createRestrGraphMoreTransformationsInRow(true, true);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ_NOT_BLOCKING, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();

                Set<Object> outputNodes;
                try {
                    outputNodes = algorithm.findNodesByAlgorithm(Collections.singletonList(t1c1), null, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0);
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(7, outputNodes.size());
                Assert.assertEquals(createSet("t1c1", "i1c1", "i2c1", "i3c1", "t2c1", "i4c1", "t3c1"),
                        translateIdsToNames(outputNodes, transaction));

                return null;
            }
        });
    }

    @Test
    public void testMoreTransformationsWithBranching() {
        cleanGraph();
        createRestrGraphBranchingTransformationsSeq();

        getDatabaseHolder().runInTransaction(TransactionLevel.READ_NOT_BLOCKING, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();

                Set<Object> outputNodes;
                try {
                    outputNodes = algorithm.findNodesByAlgorithm(Collections.singletonList(t1c1), null, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0);
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(5, outputNodes.size());
                Assert.assertEquals(createSet("t1c1", "i1c1", "i2c1", "i3c1", "t2c1"),
                        translateIdsToNames(outputNodes, transaction));

                return null;
            }
        });
    }

    @Test
    public void testCyclicAlgorithmWithoutFilterEdges() {
        cleanGraph();
        createRestrCyclicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();

                algorithm.setFilterEdgesEnabled(false);

                Set<Object> outputNodes;
                try {
                    outputNodes = algorithm.findNodesByAlgorithm(Collections.singletonList(t1c1), null, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0);
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(9, outputNodes.size());
                Assert.assertEquals(createSet("t1c1", "i1c1", "t2c1", "i3c1", "i4c1", "t3c1", "i5c1", "t4c1", "i2c1"),
                        translateIdsToNames(outputNodes, transaction));

                return null;
            }
        });
    }

    @Test
    public void testCyclicAlgorithmWithFilterEdges() {
        cleanGraph();
        createRestrCyclicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();

                Set<Object> outputNodes;
                try {
                    outputNodes = algorithm.findNodesByAlgorithm(Collections.singletonList(t1c1), null, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0);
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(10, outputNodes.size());
                Assert.assertEquals(
                        createSet("t1c1", "i1c1", "t2c1", "i3c1", "i4c1", "t3c1", "i5c1", "t4c1", "i2c1", "t5c1"),
                        translateIdsToNames(outputNodes, transaction));

                return null;
            }
        });
    }

    @Test
    public void testCyclicAlgorithmWithAdditionalFilter() {
        cleanGraph();
        createRestrCyclicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();

                GraphFlowFilter filter = new GraphFlowFilter() {
                    @Override
                    public FilterResult testVertex(Vertex vertex, Edge incomingEdge) {
                        String name = GraphOperation.getName(vertex);
                        if (name.equals("t3t1")) {
                            return FilterResult.OK_STOP;
                        } else if (name.equals("i2c1")) {
                            return FilterResult.NOK_STOP;
                        } else {
                            return FilterResult.OK_CONTINUE;
                        }
                    }
                };
                algorithm.setFilters(Collections.singletonList(filter));

                Set<Object> outputNodes;
                try {
                    outputNodes = algorithm.findNodesByAlgorithm(Collections.singletonList(t1c1), null, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0);
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(6, outputNodes.size());
                Assert.assertEquals(createSet("t1c1", "i1c1", "t2c1", "i3c1", "i4c1", "t3c1"),
                        translateIdsToNames(outputNodes, transaction));

                return null;
            }
        });
    }

    @Test
    public void testCyclicAlgorithmSpecial() {
        cleanGraph();
        createRestrCyclicSpecialGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Set<Object> outputNodes;
                try {
                    outputNodes = algorithm.findNodesByAlgorithm(Collections.singletonList(t1c1), null, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0);
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                // Assert.assertEquals(6, outputNodes.size());
                Assert.assertEquals(createSet("t1c1", "t2c1", "i1c1", "i2c1", "i3c1", "i4c1", "i5c1", "i6c1", "i7c1",
                        "i8c1", "i10c1"), translateIdsToNames(outputNodes, transaction));

                return null;
            }
        });
    }
    
    @Test
    public void testMoreRestrFalse() {
        cleanGraph();
        createRestrMoreRestrGraph(false);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Set<Object> outputNodes;
                try {
                    outputNodes = algorithm.findNodesByAlgorithm(Collections.singletonList(t1c1), null, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0);
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                // Assert.assertEquals(6, outputNodes.size());
                Assert.assertEquals(createSet("t1c1", "t2c1", "t3c1", "i1c1", "i2c1", "i3c1", "i4c1"),
                        translateIdsToNames(outputNodes, transaction));

                return null;
            }
        });
    }
    
    @Test
    public void testMoreRestrTrue() {
        cleanGraph();
        createRestrMoreRestrGraph(true);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Set<Object> outputNodes;
                try {
                    outputNodes = algorithm.findNodesByAlgorithm(Collections.singletonList(t1c1), null, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0);
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                // Assert.assertEquals(6, outputNodes.size());
                Assert.assertEquals(createSet("t1c1", "t2c1", "t3c1", "t4c1", "i1c1", "i2c1", "i3c1", "i4c1", "i5c1"),
                        translateIdsToNames(outputNodes, transaction));

                return null;
            }
        });
    }

    @Ignore
    @Test
    public void testTimeComplexity() {
        cleanGraph();
        // od velikosti 16 nahoru narůstá čas běhu exponenciálně
        // pro velikosti pod 16 převládá overhead Titana
        createRestrExponentialGraph(16);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ_NOT_BLOCKING, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex start = transaction.getVertices(NodeProperty.NODE_NAME.t(), "0a").iterator().next();
                try {
                    algorithm.findNodesByAlgorithm(Collections.singletonList(start), null, transaction, Direction.OUT,
                            REVISION_INTERVAL_1_0);
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }
                return null;
            }
        });
    }

    /**
     * Vytvoří graf rozšířený o restrikce. Příklad podle Pavla Jaromerského, dokument 
     * "Rozšíření analýzy datových toků o vyhodnocování predikátové logiky", kapitola 2.2
     */
    private void createRestrBasicGraph() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                getRevisionRootHandler().createMajorRevision(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex oracleRes = GraphCreation.createResource(transaction, root, "Oracle", "Oracle", "Description",
                        layer, REVISION_1_000000);
                Vertex oraclaDDL = GraphCreation.createResource(transaction, root, "Oracle PLSQL", "Oracle Scripts",
                        "Description", layer, REVISION_1_000000);
                Vertex db = GraphCreation.createNode(transaction, oracleRes, "db", "Database", REVISION_1_000000);

                Vertex client = GraphCreation.createNode(transaction, db, "CLIENT", "Table", REVISION_1_000000);
                Vertex clientCol = GraphCreation.createNode(transaction, client, "src_syst_cd", "Column", REVISION_1_000000);
                Vertex employee = GraphCreation.createNode(transaction, db, "EMPLOYEE", "Table", REVISION_1_000000);
                Vertex employeeCol = GraphCreation.createNode(transaction, employee, "src_syst_cd", "Column",
                        REVISION_1_000000);
                Vertex branch = GraphCreation.createNode(transaction, db, "BRANCH", "Table", REVISION_1_000000);
                Vertex branchCol = GraphCreation.createNode(transaction, branch, "src_syst_cd", "Column", REVISION_1_000000);
                Vertex supplier = GraphCreation.createNode(transaction, db, "SUPPLIER", "Table", REVISION_1_000000);
                Vertex supplierCol = GraphCreation.createNode(transaction, supplier, "src_syst_cd", "Column",
                        REVISION_1_000000);
                Vertex party = GraphCreation.createNode(transaction, db, "PARTY", "Table", REVISION_1_000000);
                Vertex partyCol = GraphCreation.createNode(transaction, party, "src_syst_cd", "Column", REVISION_1_000000);
                Vertex emplTable = GraphCreation.createNode(transaction, db, "EMPL_TABLE", "Table", REVISION_1_000000);
                Vertex emplTableCol = GraphCreation.createNode(transaction, emplTable, "src_syst_cd", "Column",
                        REVISION_1_000000);
                Vertex reportTable1 = GraphCreation.createNode(transaction, db, "REPORT_TABLE_1", "Table", REVISION_1_000000);
                Vertex reportTable1Col = GraphCreation.createNode(transaction, reportTable1, "src_syst_cd", "Column",
                        REVISION_1_000000);
                Vertex branchTable = GraphCreation.createNode(transaction, db, "BRANCH_TABLE", "Table", REVISION_1_000000);
                Vertex branchTableCol = GraphCreation.createNode(transaction, branchTable, "src_syst_cd", "Column",
                        REVISION_1_000000);
                Vertex reportTable2 = GraphCreation.createNode(transaction, db, "REPORT_TABLE_2", "Table", REVISION_1_000000);
                Vertex reportTable2Col = GraphCreation.createNode(transaction, reportTable2, "src_syst_cd", "Column",
                        REVISION_1_000000);

                Vertex krabicka1 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka1", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos1 = GraphCreation.createNode(transaction, krabicka1, "prenos1", "insert", REVISION_1_000000);
                Vertex krabicka2 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka2", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos2 = GraphCreation.createNode(transaction, krabicka2, "prenos2", "insert", REVISION_1_000000);
                Vertex krabicka3 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka3", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos3 = GraphCreation.createNode(transaction, krabicka3, "prenos3", "insert", REVISION_1_000000);
                Vertex krabicka4 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka4", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos4 = GraphCreation.createNode(transaction, krabicka4, "prenos4", "insert", REVISION_1_000000);
                Vertex krabicka5 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka5", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos5 = GraphCreation.createNode(transaction, krabicka5, "prenos5", "insert", REVISION_1_000000);
                Vertex krabicka6 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka6", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos6 = GraphCreation.createNode(transaction, krabicka6, "prenos6", "insert", REVISION_1_000000);
                Vertex krabicka7 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka7", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos7 = GraphCreation.createNode(transaction, krabicka7, "prenos7", "insert", REVISION_1_000000);
                Vertex krabicka8 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka8", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos8 = GraphCreation.createNode(transaction, krabicka8, "prenos8", "insert", REVISION_1_000000);

                RestrVariableAbstract var_syst_cd = new RestrVariableSegments("db", "", "src_syst_cd");
                GraphCreation
                        .createNodeAttribute(transaction, krabicka1, AttributeNames.NODE_RESTRICTION_TYPE,
                                new RestrDisjunctionTypeDnf(
                                        new RestrComparisonNode(var_syst_cd, ComparisonOperator.EQUAL_TO, 10)),
                                REVISION_1_000000);
                GraphCreation
                        .createNodeAttribute(transaction, krabicka2, AttributeNames.NODE_RESTRICTION_TYPE,
                                new RestrDisjunctionTypeDnf(
                                        new RestrComparisonNode(var_syst_cd, ComparisonOperator.EQUAL_TO, 20)),
                                REVISION_1_000000);
                GraphCreation
                        .createNodeAttribute(transaction, krabicka3, AttributeNames.NODE_RESTRICTION_TYPE,
                                new RestrDisjunctionTypeDnf(
                                        new RestrComparisonNode(var_syst_cd, ComparisonOperator.EQUAL_TO, 30)),
                                REVISION_1_000000);
                GraphCreation
                        .createNodeAttribute(transaction, krabicka4, AttributeNames.NODE_RESTRICTION_TYPE,
                                new RestrDisjunctionTypeDnf(
                                        new RestrComparisonNode(var_syst_cd, ComparisonOperator.EQUAL_TO, 40)),
                                REVISION_1_000000);
                GraphCreation
                        .createNodeAttribute(transaction, krabicka5, AttributeNames.NODE_RESTRICTION_TYPE,
                                new RestrDisjunctionTypeDnf(
                                        new RestrComparisonNode(var_syst_cd, ComparisonOperator.EQUAL_TO, 20)),
                                REVISION_1_000000);
                GraphCreation
                        .createNodeAttribute(transaction, krabicka6, AttributeNames.NODE_RESTRICTION_TYPE,
                                new RestrDisjunctionTypeDnf(
                                        new RestrComparisonNode(var_syst_cd, ComparisonOperator.EQUAL_TO, 30)),
                                REVISION_1_000000);

                GraphCreation.createEdge(clientCol, prenos1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(prenos1, partyCol, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(employeeCol, prenos2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(prenos2, partyCol, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(branchCol, prenos3, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(prenos3, partyCol, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(supplierCol, prenos4, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(prenos4, partyCol, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(partyCol, prenos5, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(prenos5, emplTableCol, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(partyCol, prenos6, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(prenos6, branchTableCol, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(emplTableCol, prenos7, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(prenos7, reportTable1Col, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(branchTableCol, prenos8, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(prenos8, reportTable2Col, EdgeLabel.DIRECT, REVISION_1_000000);

                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);

                return null;
            }
        });
    }

    /**
     * Vytvoří graf rozšířený o restrikce s JOINy. Příklad podle Pavla Jaromerského, dokument
     * "Rozšíření analýzy datových toků o vyhodnocování predikátové logiky", kapitola 2.4
     */
    private void createRestrAdvancedGraph() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                getRevisionRootHandler().createMajorRevision(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Oracle", "Oracle", "Description",
                        layer, REVISION_1_000000);
                Vertex oraclaDDL = GraphCreation.createResource(transaction, root, "Oracle PLSQL", "Oracle Scripts",
                        "Description", layer, REVISION_1_000000);
                Vertex db = GraphCreation.createNode(transaction, teradataRes, "db", "Database", REVISION_1_000000);

                Vertex cont10 = GraphCreation.createNode(transaction, db, "CONT_10", "Table", REVISION_1_000000);
                Vertex cont10col = GraphCreation.createNode(transaction, cont10, "src_syst_cd", "Column", REVISION_1_000000);
                Vertex cont20 = GraphCreation.createNode(transaction, db, "CONT_20", "Table", REVISION_1_000000);
                Vertex cont20col = GraphCreation.createNode(transaction, cont20, "src_syst_cd", "Column", REVISION_1_000000);
                Vertex cont30 = GraphCreation.createNode(transaction, db, "CONT_30", "Table", REVISION_1_000000);
                Vertex cont30col = GraphCreation.createNode(transaction, cont30, "src_syst_cd", "Column", REVISION_1_000000);
                Vertex cont = GraphCreation.createNode(transaction, db, "CONT", "Table", REVISION_1_000000);
                Vertex contCol = GraphCreation.createNode(transaction, cont, "src_syst_cd", "Column", REVISION_1_000000);
                Vertex prepare = GraphCreation.createNode(transaction, db, "PREPARE_TABLE", "Table", REVISION_1_000000);
                Vertex prepareCol = GraphCreation.createNode(transaction, prepare, "src_syst_cd", "Column", REVISION_1_000000);
                Vertex contBal10 = GraphCreation.createNode(transaction, db, "CONT_BAL_10", "Table", REVISION_1_000000);
                Vertex contBal10col = GraphCreation.createNode(transaction, contBal10, "src_syst_cd", "Column",
                        REVISION_1_000000);
                Vertex contBal20 = GraphCreation.createNode(transaction, db, "CONT_BAL_20", "Table", REVISION_1_000000);
                Vertex contBal20col = GraphCreation.createNode(transaction, contBal20, "src_syst_cd", "Column",
                        REVISION_1_000000);
                Vertex contBal30 = GraphCreation.createNode(transaction, db, "CONT_BAL_30", "Table", REVISION_1_000000);
                Vertex contBal30col = GraphCreation.createNode(transaction, contBal30, "src_syst_cd", "Column",
                        REVISION_1_000000);
                Vertex contBal = GraphCreation.createNode(transaction, db, "CONT_BAL", "Table", REVISION_1_000000);
                Vertex contBalCol = GraphCreation.createNode(transaction, contBal, "src_syst_cd", "Column", REVISION_1_000000);
                Vertex report = GraphCreation.createNode(transaction, db, "REPORT_TABLE", "Table", REVISION_1_000000);
                Vertex reportCol = GraphCreation.createNode(transaction, report, "src_syst_cd", "Column", REVISION_1_000000);

                Vertex krabicka1 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka1", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos1 = GraphCreation.createNode(transaction, krabicka1, "prenos1", "insert", REVISION_1_000000);
                Vertex krabicka2 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka2", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos2 = GraphCreation.createNode(transaction, krabicka2, "prenos2", "insert", REVISION_1_000000);
                Vertex krabicka3 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka3", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos3 = GraphCreation.createNode(transaction, krabicka3, "prenos3", "insert", REVISION_1_000000);
                Vertex krabicka4 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka4", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos4 = GraphCreation.createNode(transaction, krabicka4, "prenos4", "insert", REVISION_1_000000);
                Vertex krabicka5 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka5", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos5 = GraphCreation.createNode(transaction, krabicka5, "prenos5", "insert", REVISION_1_000000);
                Vertex krabicka6 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka6", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos6 = GraphCreation.createNode(transaction, krabicka6, "prenos6", "insert", REVISION_1_000000);
                Vertex krabicka7 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka7", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos7 = GraphCreation.createNode(transaction, krabicka7, "prenos7", "insert", REVISION_1_000000);
                Vertex krabicka8 = GraphCreation.createNode(transaction, oraclaDDL, "krabicka8", "SQL_insert_stmt",
                        REVISION_1_000000);
                Vertex prenos8 = GraphCreation.createNode(transaction, krabicka8, "prenos8", "insert", REVISION_1_000000);

                RestrVariableAbstract var_syst_cd = new RestrVariableSegments("db", "", "src_syst_cd");
                RestrVariableAbstract var_meas_cd = new RestrVariableSegments("db", "", "meas_cd");

                GraphCreation
                        .createNodeAttribute(transaction, krabicka1, AttributeNames.NODE_RESTRICTION_TYPE,
                                new RestrDisjunctionTypeDnf(
                                        new RestrComparisonNode(var_syst_cd, ComparisonOperator.EQUAL_TO, 10)),
                                REVISION_1_000000);
                GraphCreation
                        .createNodeAttribute(transaction, krabicka2, AttributeNames.NODE_RESTRICTION_TYPE,
                                new RestrDisjunctionTypeDnf(
                                        new RestrComparisonNode(var_syst_cd, ComparisonOperator.EQUAL_TO, 20)),
                                REVISION_1_000000);
                GraphCreation
                        .createNodeAttribute(transaction, krabicka3, AttributeNames.NODE_RESTRICTION_TYPE,
                                new RestrDisjunctionTypeDnf(
                                        new RestrComparisonNode(var_syst_cd, ComparisonOperator.EQUAL_TO, 30)),
                                REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, krabicka4, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(
                                new RestrEnumNode(var_syst_cd, new HashSet<Object>(Arrays.asList(10, 20)))),
                        REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, krabicka5, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrConjunctionElementary(
                                new RestrComparisonNode(var_syst_cd, ComparisonOperator.EQUAL_TO, 10),
                                new RestrComparisonNode(var_meas_cd, ComparisonOperator.EQUAL_TO, 1))),
                        REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, krabicka6, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrConjunctionElementary(
                                new RestrComparisonNode(var_syst_cd, ComparisonOperator.EQUAL_TO, 20),
                                new RestrComparisonNode(var_meas_cd, ComparisonOperator.EQUAL_TO, 2))),
                        REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, krabicka7, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrConjunctionElementary(
                                new RestrComparisonNode(var_syst_cd, ComparisonOperator.EQUAL_TO, 30),
                                new RestrComparisonNode(var_meas_cd, ComparisonOperator.EQUAL_TO, 1))),
                        REVISION_1_000000);
                GraphCreation
                        .createNodeAttribute(transaction, krabicka8, AttributeNames.NODE_RESTRICTION_TYPE,
                                new RestrDisjunctionTypeDnf(
                                        new RestrComparisonNode(var_meas_cd, ComparisonOperator.EQUAL_TO, 2)),
                                REVISION_1_000000);

                GraphCreation.createEdge(cont10col, prenos1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(prenos1, contCol, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(cont20col, prenos2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(prenos2, contCol, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(cont30col, prenos3, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(prenos3, contCol, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(contCol, prenos4, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(prenos4, prepareCol, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(contBal10col, prenos5, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(contCol, prenos5, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(prenos5, contBalCol, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(contBal20col, prenos6, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(contCol, prenos6, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(prenos6, contBalCol, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(contBal30col, prenos7, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(contCol, prenos7, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(prenos7, contBalCol, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(prepareCol, prenos8, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(contBalCol, prenos8, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(prenos8, reportCol, EdgeLabel.DIRECT, REVISION_1_000000);

                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);

                return null;
            }
        });
    }

    /**
     * Vytvoří hloupý nereálný graf s restrikcemi - pouze na otestování ukládání disjunkcí.
     */
    private void createRestrMeaninglessGraph() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                getRevisionRootHandler().createMajorRevision(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex oracleRes = GraphCreation.createResource(transaction, root, "Oracle", "Oracle", "Description",
                        layer, REVISION_1_000000);
                Vertex db = GraphCreation.createNode(transaction, oracleRes, "db", "Database", REVISION_1_000000);

                Vertex A = GraphCreation.createNode(transaction, db, "A", "Table", REVISION_1_000000);
                Vertex B = GraphCreation.createNode(transaction, db, "B", "Table", REVISION_1_000000);
                Vertex C = GraphCreation.createNode(transaction, db, "C", "Table", REVISION_1_000000);
                Vertex D = GraphCreation.createNode(transaction, db, "D", "Table", REVISION_1_000000);
                Vertex E = GraphCreation.createNode(transaction, db, "E", "Table", REVISION_1_000000);
                Vertex K = GraphCreation.createNode(transaction, db, "K", "Table", REVISION_1_000000);
                Vertex L = GraphCreation.createNode(transaction, db, "L", "Table", REVISION_1_000000);
                Vertex a = GraphCreation.createNode(transaction, A, "a", "Column", REVISION_1_000000);
                Vertex b = GraphCreation.createNode(transaction, B, "b", "Column", REVISION_1_000000);
                Vertex c = GraphCreation.createNode(transaction, C, "c", "Column", REVISION_1_000000);
                Vertex d = GraphCreation.createNode(transaction, D, "d", "Column", REVISION_1_000000);
                Vertex e = GraphCreation.createNode(transaction, E, "e", "Column", REVISION_1_000000);
                Vertex k = GraphCreation.createNode(transaction, K, "k", "Column", REVISION_1_000000);
                Vertex l = GraphCreation.createNode(transaction, L, "l", "Column", REVISION_1_000000);

                RestrVariableAbstract x = new RestrVariableSegments("db", "", "x");
                RestrVariableAbstract y = new RestrVariableSegments("db", "", "y");
                RestrVariableAbstract z = new RestrVariableSegments("db", "", "z");
                GraphCreation.createNodeAttribute(transaction, B, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(
                                new RestrConjunctionElementary(
                                        new RestrComparisonNode(x, ComparisonOperator.LESS_EQUALS, 10)),
                                new RestrConjunctionElementary(
                                        new RestrComparisonNode(y, ComparisonOperator.GREATER_EQUALS, 50),
                                        new RestrComparisonNode(z, ComparisonOperator.EQUAL_TO, 99))),
                        REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, C, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(x, ComparisonOperator.GREATER_THAN, 20)),
                        REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, D, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(y, ComparisonOperator.LESS_THAN, 40)),
                        REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, K, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrIsNullNode(z)), REVISION_1_000000);

                GraphCreation.createEdge(a, b, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(b, c, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c, d, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(d, e, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c, k, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(k, l, EdgeLabel.DIRECT, REVISION_1_000000);
                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);

                return null;
            }
        });
    }

    private void createRestrCyclicGraph() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                getRevisionRootHandler().createMajorRevision(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex oracle = GraphCreation.createResource(transaction, root, "Oracle", "Oracle", "Description",
                        layer, REVISION_1_000000);
                Vertex db = GraphCreation.createNode(transaction, oracle, "db", "Database", REVISION_1_000000);
                Vertex t1 = GraphCreation.createNode(transaction, db, "t1", "Table", REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, t1, "t1c1", "Column", REVISION_1_000000);
                Vertex t2 = GraphCreation.createNode(transaction, db, "t2", "Table", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, t2, "t2c1", "Column", REVISION_1_000000);
                Vertex t3 = GraphCreation.createNode(transaction, db, "t3", "Table", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, t3, "t3c1", "Column", REVISION_1_000000);
                Vertex t4 = GraphCreation.createNode(transaction, db, "t4", "Table", REVISION_1_000000);
                Vertex t4c1 = GraphCreation.createNode(transaction, t4, "t4c1", "Column", REVISION_1_000000);
                Vertex t5 = GraphCreation.createNode(transaction, db, "t5", "Table", REVISION_1_000000);
                Vertex t5c1 = GraphCreation.createNode(transaction, t5, "t5c1", "Column", REVISION_1_000000);

                Vertex oraclaDDL = GraphCreation.createResource(transaction, root, "Oracle PLSQL", "Oracle Scripts",
                        "Description", layer, REVISION_1_000000);
                Vertex script = GraphCreation.createNode(transaction, oraclaDDL, "script", "PLSQL Script", REVISION_1_000000);
                Vertex i1 = GraphCreation.createNode(transaction, script, "i1", "PLSQL Insert", REVISION_1_000000);
                Vertex i1c1 = GraphCreation.createNode(transaction, i1, "i1c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i2 = GraphCreation.createNode(transaction, script, "i2", "PLSQL Insert", REVISION_1_000000);
                Vertex i2c1 = GraphCreation.createNode(transaction, i2, "i2c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i3 = GraphCreation.createNode(transaction, script, "i3", "PLSQL Insert", REVISION_1_000000);
                Vertex i3c1 = GraphCreation.createNode(transaction, i3, "i3c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i4 = GraphCreation.createNode(transaction, script, "i4", "PLSQL Insert", REVISION_1_000000);
                Vertex i4c1 = GraphCreation.createNode(transaction, i4, "i4c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i5 = GraphCreation.createNode(transaction, script, "i5", "PLSQL Insert", REVISION_1_000000);
                Vertex i5c1 = GraphCreation.createNode(transaction, i5, "i5c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i6 = GraphCreation.createNode(transaction, script, "i6", "PLSQL Insert", REVISION_1_000000);
                Vertex i6c1 = GraphCreation.createNode(transaction, i6, "i6c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i7 = GraphCreation.createNode(transaction, script, "i7", "PLSQL Insert", REVISION_1_000000);
                Vertex i7c1 = GraphCreation.createNode(transaction, i7, "i7c1", "PLSQL ColumnFlow", REVISION_1_000000);

                RestrVariableAbstract var_txc1 = new RestrVariableSegments("db", "tx", "txc1");

                GraphCreation.createNodeAttribute(transaction, i1, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 1)),
                        REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, i2, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 2)),
                        REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, i5, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 2)),
                        REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, i6, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 3)),
                        REVISION_1_000000);

                GraphCreation.createEdge(t1c1, i1c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, i3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i3c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, i4c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i4c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, i5c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i5c1, t4c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, i6c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i6c1, t4c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c1, i2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i2c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i5c1, t5c1, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(t5c1, i7c1, EdgeLabel.DIRECT, REVISION_1_000000);

                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);

                return null;
            }
        });
    }

    private void createRestrCyclicSpecialGraph() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                getRevisionRootHandler().createMajorRevision(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex oracle = GraphCreation.createResource(transaction, root, "Oracle", "Oracle", "Description",
                        layer, REVISION_1_000000);
                Vertex db = GraphCreation.createNode(transaction, oracle, "db", "Database", REVISION_1_000000);
                Vertex t1 = GraphCreation.createNode(transaction, db, "t1", "Table", REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, t1, "t1c1", "Column", REVISION_1_000000);
                Vertex t2 = GraphCreation.createNode(transaction, db, "t2", "Table", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, t2, "t2c1", "Column", REVISION_1_000000);

                Vertex oraclaDDL = GraphCreation.createResource(transaction, root, "Oracle PLSQL", "Oracle Scripts",
                        "Description", layer, REVISION_1_000000);
                Vertex script = GraphCreation.createNode(transaction, oraclaDDL, "script", "PLSQL Script", REVISION_1_000000);
                Vertex i1 = GraphCreation.createNode(transaction, script, "i1", "PLSQL Insert", REVISION_1_000000);
                Vertex i1c1 = GraphCreation.createNode(transaction, i1, "i1c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i2 = GraphCreation.createNode(transaction, script, "i2", "PLSQL Insert", REVISION_1_000000);
                Vertex i2c1 = GraphCreation.createNode(transaction, i2, "i2c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i3 = GraphCreation.createNode(transaction, script, "i3", "PLSQL Insert", REVISION_1_000000);
                Vertex i3c1 = GraphCreation.createNode(transaction, i3, "i3c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i4 = GraphCreation.createNode(transaction, script, "i4", "PLSQL Insert", REVISION_1_000000);
                Vertex i4c1 = GraphCreation.createNode(transaction, i4, "i4c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i5 = GraphCreation.createNode(transaction, script, "i5", "PLSQL Insert", REVISION_1_000000);
                Vertex i5c1 = GraphCreation.createNode(transaction, i5, "i5c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i6 = GraphCreation.createNode(transaction, script, "i6", "PLSQL Insert", REVISION_1_000000);
                Vertex i6c1 = GraphCreation.createNode(transaction, i6, "i6c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i7 = GraphCreation.createNode(transaction, script, "i7", "PLSQL Insert", REVISION_1_000000);
                Vertex i7c1 = GraphCreation.createNode(transaction, i7, "i7c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i8 = GraphCreation.createNode(transaction, script, "i8", "PLSQL Insert", REVISION_1_000000);
                Vertex i8c1 = GraphCreation.createNode(transaction, i8, "i8c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i9 = GraphCreation.createNode(transaction, script, "i9", "PLSQL Insert", REVISION_1_000000);
                Vertex i9c1 = GraphCreation.createNode(transaction, i9, "i9c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i10 = GraphCreation.createNode(transaction, script, "i10", "PLSQL Insert", REVISION_1_000000);
                Vertex i10c1 = GraphCreation.createNode(transaction, i10, "i10c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i11 = GraphCreation.createNode(transaction, script, "i11", "PLSQL Insert", REVISION_1_000000);
                Vertex i11c1 = GraphCreation.createNode(transaction, i11, "i11c1", "PLSQL ColumnFlow", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, i1c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i1c1, i2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i2c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);

                GraphCreation.createEdge(t2c1, i3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i3c1, i4c1, EdgeLabel.DIRECT, REVISION_1_000000);

                GraphCreation.createEdge(t2c1, i5c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i5c1, i6c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i6c1, i2c1, EdgeLabel.DIRECT, REVISION_1_000000);

                GraphCreation.createEdge(t2c1, i7c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i7c1, i8c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i8c1, i10c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i10c1, i7c1, EdgeLabel.DIRECT, REVISION_1_000000);

                GraphCreation.createEdge(i8c1, i9c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i9c1, i11c1, EdgeLabel.DIRECT, REVISION_1_000000);

                RestrVariableAbstract var_txc1 = new RestrVariableSegments("db", "tx", "txc1");

                GraphCreation.createNodeAttribute(transaction, i1, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 1)),
                        REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, i11, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 2)),
                        REVISION_1_000000);

                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);

                return null;
            }
        });
    }
    
    private void createRestrMoreRestrGraph(final boolean moreRestr) {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                getRevisionRootHandler().createMajorRevision(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex oracle = GraphCreation.createResource(transaction, root, "Oracle", "Oracle", "Description",
                        layer, REVISION_1_000000);
                Vertex db = GraphCreation.createNode(transaction, oracle, "db", "Database", REVISION_1_000000);
                Vertex t1 = GraphCreation.createNode(transaction, db, "t1", "Table", REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, t1, "t1c1", "Column", REVISION_1_000000);
                Vertex t2 = GraphCreation.createNode(transaction, db, "t2", "Table", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, t2, "t2c1", "Column", REVISION_1_000000);
                Vertex t3 = GraphCreation.createNode(transaction, db, "t3", "Table", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, t3, "t3c1", "Column", REVISION_1_000000);
                Vertex t4 = GraphCreation.createNode(transaction, db, "t4", "Table", REVISION_1_000000);
                Vertex t4c1 = GraphCreation.createNode(transaction, t4, "t4c1", "Column", REVISION_1_000000);

                Vertex oraclaDDL = GraphCreation.createResource(transaction, root, "Oracle PLSQL", "Oracle Scripts",
                        "Description", layer, REVISION_1_000000);
                Vertex script = GraphCreation.createNode(transaction, oraclaDDL, "script", "PLSQL Script", REVISION_1_000000);
                Vertex i1 = GraphCreation.createNode(transaction, script, "i1", "PLSQL Insert", REVISION_1_000000);
                Vertex i1c1 = GraphCreation.createNode(transaction, i1, "i1c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i2 = GraphCreation.createNode(transaction, script, "i2", "PLSQL Insert", REVISION_1_000000);
                Vertex i2c1 = GraphCreation.createNode(transaction, i2, "i2c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i3 = GraphCreation.createNode(transaction, script, "i3", "PLSQL Insert", REVISION_1_000000);
                Vertex i3c1 = GraphCreation.createNode(transaction, i3, "i3c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i4 = GraphCreation.createNode(transaction, script, "i4", "PLSQL Insert", REVISION_1_000000);
                Vertex i4c1 = GraphCreation.createNode(transaction, i4, "i4c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i5 = GraphCreation.createNode(transaction, script, "i5", "PLSQL Insert", REVISION_1_000000);
                Vertex i5c1 = GraphCreation.createNode(transaction, i5, "i5c1", "PLSQL ColumnFlow", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, i1c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i1c1, i2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i2c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                
                GraphCreation.createEdge(i1c1, i3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i3c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);

                GraphCreation.createEdge(t2c1, i4c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i4c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                
                GraphCreation.createEdge(t2c1, i5c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i5c1, t4c1, EdgeLabel.DIRECT, REVISION_1_000000);

                RestrVariableAbstract var_txc1 = new RestrVariableSegments("db", "tx", "txc1");

                if (moreRestr) {
                    GraphCreation.createNodeAttribute(transaction, i2, AttributeNames.NODE_RESTRICTION_TYPE,
                            new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 1)),
                            REVISION_1_000000);
                } else {
                    GraphCreation.createNodeAttribute(transaction, i1, AttributeNames.NODE_RESTRICTION_TYPE,
                            new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 1)),
                            REVISION_1_000000);
                }
                GraphCreation.createNodeAttribute(transaction, i4, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 1)),
                        REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, i5, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 2)),
                        REVISION_1_000000);

                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);

                return null;
            }
        });
    }

    private void createRestrExponentialGraph(final int segmentsCount) {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                getRevisionRootHandler().createMajorRevision(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex oracle = GraphCreation.createResource(transaction, root, "Oracle", "Oracle", "Description",
                        layer, REVISION_1_000000);

                Vertex[] vertices = new Vertex[segmentsCount * 3];
                for (int i = 0; i < segmentsCount; i++) {
                    vertices[3 * i] = GraphCreation.createNode(transaction, oracle, i + "a", "Column", REVISION_1_000000);
                    vertices[3 * i + 1] = GraphCreation.createNode(transaction, oracle, i + "b", "Column", REVISION_1_000000);
                    vertices[3 * i + 2] = GraphCreation.createNode(transaction, oracle, i + "c", "Column", REVISION_1_000000);
                    GraphCreation.createEdge(vertices[3 * i], vertices[3 * i + 1], EdgeLabel.DIRECT, REVISION_1_000000);
                    GraphCreation.createEdge(vertices[3 * i], vertices[3 * i + 2], EdgeLabel.DIRECT, REVISION_1_000000);
                    if (i > 0) {
                        GraphCreation.createEdge(vertices[3 * i - 2], vertices[3 * i], EdgeLabel.DIRECT, REVISION_1_000000);
                        GraphCreation.createEdge(vertices[3 * i - 1], vertices[3 * i], EdgeLabel.DIRECT, REVISION_1_000000);
                    }
                }

                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);

                return null;
            }
        });
    }

    private void createRestrGraphMoreTransformationsInRow(final boolean addBackEdge, final boolean addCrossEdge) {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                getRevisionRootHandler().createMajorRevision(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex oracle = GraphCreation.createResource(transaction, root, "Oracle", "Oracle", "Description",
                        layer, REVISION_1_000000);
                Vertex db = GraphCreation.createNode(transaction, oracle, "db", "Database", REVISION_1_000000);
                Vertex t1 = GraphCreation.createNode(transaction, db, "t1", "Table", REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, t1, "t1c1", "Column", REVISION_1_000000);
                Vertex t2 = GraphCreation.createNode(transaction, db, "t2", "Table", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, t2, "t2c1", "Column", REVISION_1_000000);
                Vertex t3 = GraphCreation.createNode(transaction, db, "t3", "Table", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, t3, "t3c1", "Column", REVISION_1_000000);

                Vertex oraclaDDL = GraphCreation.createResource(transaction, root, "Oracle PLSQL", "Oracle Scripts",
                        "Description", layer, REVISION_1_000000);
                Vertex script = GraphCreation.createNode(transaction, oraclaDDL, "script", "PLSQL Script", REVISION_1_000000);
                Vertex i1 = GraphCreation.createNode(transaction, script, "i1", "PLSQL Insert", REVISION_1_000000);
                Vertex i1c1 = GraphCreation.createNode(transaction, i1, "i1c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i2 = GraphCreation.createNode(transaction, script, "i2", "PLSQL Insert", REVISION_1_000000);
                Vertex i2c1 = GraphCreation.createNode(transaction, i2, "i2c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i3 = GraphCreation.createNode(transaction, script, "i3", "PLSQL Insert", REVISION_1_000000);
                Vertex i3c1 = GraphCreation.createNode(transaction, i3, "i3c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i4 = GraphCreation.createNode(transaction, script, "i4", "PLSQL Insert", REVISION_1_000000);
                Vertex i4c1 = GraphCreation.createNode(transaction, i4, "i4c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i5 = GraphCreation.createNode(transaction, script, "i5", "PLSQL Insert", REVISION_1_000000);
                Vertex i5c1 = GraphCreation.createNode(transaction, i5, "i5c1", "PLSQL ColumnFlow", REVISION_1_000000);

                RestrVariableAbstract var_txc1 = new RestrVariableSegments("db", "tx", "txc1");

                GraphCreation.createNodeAttribute(transaction, i1, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 1)),
                        REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, i5, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 2)),
                        REVISION_1_000000);

                GraphCreation.createEdge(t1c1, i1c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i1c1, i2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i2c1, i3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i3c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, i4c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i4c1, i5c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, i5c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i5c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                if (addBackEdge) {
                    GraphCreation.createEdge(i4c1, t1c1, EdgeLabel.FILTER, REVISION_1_000000);
                }
                if (addCrossEdge) {
                    GraphCreation.createEdge(i1c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                }

                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);

                return null;
            }
        });
    }

    private void createRestrGraphBranchingTransformationsSeq() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                getRevisionRootHandler().createMajorRevision(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                Vertex oracle = GraphCreation.createResource(transaction, root, "Oracle", "Oracle", "Description",
                        layer, REVISION_1_000000);
                Vertex db = GraphCreation.createNode(transaction, oracle, "db", "Database", REVISION_1_000000);
                Vertex t1 = GraphCreation.createNode(transaction, db, "t1", "Table", REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, t1, "t1c1", "Column", REVISION_1_000000);
                Vertex t2 = GraphCreation.createNode(transaction, db, "t2", "Table", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, t2, "t2c1", "Column", REVISION_1_000000);
                Vertex t3 = GraphCreation.createNode(transaction, db, "t3", "Table", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, t3, "t3c1", "Column", REVISION_1_000000);

                Vertex oraclaDDL = GraphCreation.createResource(transaction, root, "Oracle PLSQL", "Oracle Scripts",
                        "Description", layer, REVISION_1_000000);
                Vertex script = GraphCreation.createNode(transaction, oraclaDDL, "script", "PLSQL Script", REVISION_1_000000);
                Vertex i1 = GraphCreation.createNode(transaction, script, "i1", "PLSQL Insert", REVISION_1_000000);
                Vertex i1c1 = GraphCreation.createNode(transaction, i1, "i1c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i2 = GraphCreation.createNode(transaction, script, "i2", "PLSQL Insert", REVISION_1_000000);
                Vertex i2c1 = GraphCreation.createNode(transaction, i2, "i2c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i3 = GraphCreation.createNode(transaction, script, "i3", "PLSQL Insert", REVISION_1_000000);
                Vertex i3c1 = GraphCreation.createNode(transaction, i3, "i3c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i4 = GraphCreation.createNode(transaction, script, "i4", "PLSQL Insert", REVISION_1_000000);
                Vertex i4c1 = GraphCreation.createNode(transaction, i4, "i4c1", "PLSQL ColumnFlow", REVISION_1_000000);
                Vertex i5 = GraphCreation.createNode(transaction, script, "i5", "PLSQL Insert", REVISION_1_000000);
                Vertex i5c1 = GraphCreation.createNode(transaction, i5, "i5c1", "PLSQL ColumnFlow", REVISION_1_000000);

                RestrVariableAbstract var_txc1 = new RestrVariableSegments("db", "tx", "txc1");

                GraphCreation.createNodeAttribute(transaction, i1, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 1)),
                        REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, i5, AttributeNames.NODE_RESTRICTION_TYPE,
                        new RestrDisjunctionTypeDnf(new RestrComparisonNode(var_txc1, ComparisonOperator.EQUAL_TO, 2)),
                        REVISION_1_000000);

                GraphCreation.createEdge(t1c1, i1c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i1c1, i2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i2c1, i3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i3c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i2c1, i4c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i4c1, i5c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(i5c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);

                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);

                return null;
            }
        });
    }

}
