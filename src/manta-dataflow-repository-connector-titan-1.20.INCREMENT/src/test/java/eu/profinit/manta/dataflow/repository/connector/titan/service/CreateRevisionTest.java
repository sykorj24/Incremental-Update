package eu.profinit.manta.dataflow.repository.connector.titan.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import junit.framework.Assert;

/**
 * Testing proper creation of revision nodes and correct values of the latest 
 * committed and uncommitted revisions in the revision root node.
 * <p>
 * Revision root node always holds numbers (properties) of the latest committed and uncommitted revision. 
 * This way, we avoid searching through all revision nodes to find the latest one.
 * <p>
 * At the beginning, both properties are null. Following, these properties are rewritten according to the changes.
 * <p>
 * In the whole graph is always either none or one uncommitted revision. 
 * When none revision is uncommitted (i.e. all revisions are committed), (-1.0) value is saved in the revision root property. 
 * 
 * Example:
 * <ol>
 *  <li>No revision is present in the tree: latestCommittedRevision==null && latestUncommittedRevision==null.</li>
 *  <li>First (technical) revision is created, but not committed yet: latestCommittedRevision==null && latestUncommittedRevision==technicalRevision.</li>
 *  <li>First (technical) revision is committed: latestCommittedRevision==technicalRevision && latestUncommittedRevision==(-1.0).</li>
 *  <li>New revision 1.000000 is created but not committed yet: latestCommittedRevision==technicalRevision && latestUncommittedRevision==1.000000
 *  <li>Revision 1.000000 is committed: latestCommittedRevision==1.000000 && latestUncommittedRevision==(-1.0)
 * </ol>
 * 
 * @author jsykora
 *
 */
public class CreateRevisionTest extends TestTitanDatabaseProvider {

    @Test
    public void testMinorRevisions() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                RevisionRootHandler revisionRootHandler = getRevisionRootHandler();
                Double latestCommittedRevisionNumber;
                Double latestUncommittedRevisionNumber;
                RevisionModel technicalRevision;
                RevisionModel revision_0_000001;
                RevisionModel revision_0_000002;

                // 0.000000 (committed)
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                technicalRevision = revisionRootHandler.getSpecificModel(transaction,
                        RevisionRootHandler.TECHNICAL_REVISION_NUMBER);
                // check revision root properties
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);
                // check neighbors of technical revision
                Assert.assertEquals(null, technicalRevision.getPreviousRevision());
                Assert.assertEquals(null, technicalRevision.getNextRevision());

                // 0.000000 (committed) <-> 0.000001 (uncommitted)
                revisionRootHandler.createMinorRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                technicalRevision = revisionRootHandler.getSpecificModel(transaction,
                        RevisionRootHandler.TECHNICAL_REVISION_NUMBER);
                revision_0_000001 = revisionRootHandler.getSpecificModel(transaction, REVISION_0_000001);
                // check revision root properties
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER, latestCommittedRevisionNumber);
                Assert.assertEquals(REVISION_0_000001, latestUncommittedRevisionNumber);
                // check neighbors of technical revision
                Assert.assertEquals(null, technicalRevision.getPreviousRevision());
                Assert.assertEquals(REVISION_0_000001, technicalRevision.getNextRevision());
                // check neighbors of revision 0.000001
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER,
                        revision_0_000001.getPreviousRevision());
                Assert.assertEquals(null, revision_0_000001.getNextRevision());

                // 0.000000 (committed) <-> 0.000001 (committed)
                revisionRootHandler.commitLatestRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                // check revision root properties
                Assert.assertEquals(REVISION_0_000001, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);

                // 0.000000 (committed) <-> 0.000001 (committed) <-> 0.000002 (uncommitted)
                revisionRootHandler.createMinorRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                revision_0_000001 = revisionRootHandler.getSpecificModel(transaction, REVISION_0_000001);
                revision_0_000002 = revisionRootHandler.getSpecificModel(transaction, REVISION_0_000002);
                // check revision root properties
                Assert.assertEquals(REVISION_0_000001, latestCommittedRevisionNumber);
                Assert.assertEquals(REVISION_0_000002, latestUncommittedRevisionNumber);
                // check neighbors of revision 0.000001
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER,
                        revision_0_000001.getPreviousRevision());
                Assert.assertEquals(REVISION_0_000002, revision_0_000001.getNextRevision());
                // check neighbors of revision 0.000002
                Assert.assertEquals(REVISION_0_000001, revision_0_000002.getPreviousRevision());
                Assert.assertEquals(null, revision_0_000002.getNextRevision());

                // 0.000000 (committed) <-> 0.000001 (committed) <-> 0.000002 (committed)
                revisionRootHandler.commitLatestRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                // check revision root properties
                Assert.assertEquals(REVISION_0_000002, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);

                return null;
            }
        });
    }

    @Test
    public void testMajorRevisions() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                RevisionRootHandler revisionRootHandler = getRevisionRootHandler();
                Double latestCommittedRevisionNumber;
                Double latestUncommittedRevisionNumber;
                RevisionModel technicalRevision;
                RevisionModel revision_1_000000;
                RevisionModel revision_2_000000;

                // 0.000000 (committed)
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                technicalRevision = revisionRootHandler.getSpecificModel(transaction,
                        RevisionRootHandler.TECHNICAL_REVISION_NUMBER);
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);
                Assert.assertEquals(null, technicalRevision.getPreviousRevision());
                Assert.assertEquals(null, technicalRevision.getNextRevision());

                // 0.000000 (committed) <-> 1.000000 (uncommitted)
                revisionRootHandler.createMajorRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                technicalRevision = revisionRootHandler.getSpecificModel(transaction,
                        RevisionRootHandler.TECHNICAL_REVISION_NUMBER);
                revision_1_000000 = revisionRootHandler.getSpecificModel(transaction, REVISION_1_000000);
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER, latestCommittedRevisionNumber);
                Assert.assertEquals(REVISION_1_000000, latestUncommittedRevisionNumber);
                Assert.assertEquals(null, technicalRevision.getPreviousRevision());
                Assert.assertEquals(REVISION_1_000000, technicalRevision.getNextRevision());
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER,
                        revision_1_000000.getPreviousRevision());
                Assert.assertEquals(null, revision_1_000000.getNextRevision());

                // 0.000000 (committed) <-> 1.000000 (committed)
                revisionRootHandler.commitLatestRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(REVISION_1_000000, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);

                // 0.000000 (committed) <-> 1.000000 (committed) <-> 2.000000 (uncommitted)
                revisionRootHandler.createMajorRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                revision_1_000000 = revisionRootHandler.getSpecificModel(transaction, REVISION_1_000000);
                revision_2_000000 = revisionRootHandler.getSpecificModel(transaction, REVISION_2_000000);
                Assert.assertEquals(REVISION_1_000000, latestCommittedRevisionNumber);
                Assert.assertEquals(REVISION_2_000000, latestUncommittedRevisionNumber);
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER,
                        revision_1_000000.getPreviousRevision());
                Assert.assertEquals(REVISION_2_000000, revision_1_000000.getNextRevision());
                Assert.assertEquals(REVISION_1_000000, revision_2_000000.getPreviousRevision());
                Assert.assertEquals(null, revision_2_000000.getNextRevision());

                // 0.000000 (committed) -> 1.000000 (committed) -> 2.000000 (committed)
                revisionRootHandler.commitLatestRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(REVISION_2_000000, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);

                return null;
            }
        });
    }

    @Test
    public void testMajorAndMinorRevisions() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                RevisionRootHandler revisionRootHandler = getRevisionRootHandler();
                Double latestCommittedRevisionNumber;
                Double latestUncommittedRevisionNumber;
                RevisionModel technicalRevision;
                RevisionModel revision_0_000001;
                RevisionModel revision_1_000000;
                RevisionModel revision_1_000001;
                RevisionModel revision_1_000002;
                RevisionModel revision_2_000000;

                // 0.000000 (committed)
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                technicalRevision = revisionRootHandler.getSpecificModel(transaction,
                        RevisionRootHandler.TECHNICAL_REVISION_NUMBER);
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);
                Assert.assertEquals(null, technicalRevision.getPreviousRevision());
                Assert.assertEquals(null, technicalRevision.getNextRevision());

                // 0.000000 (committed) <-> 0.000001 (uncommitted)
                revisionRootHandler.createMinorRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                technicalRevision = revisionRootHandler.getSpecificModel(transaction,
                        RevisionRootHandler.TECHNICAL_REVISION_NUMBER);
                revision_0_000001 = revisionRootHandler.getSpecificModel(transaction, REVISION_0_000001);
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER, latestCommittedRevisionNumber);
                Assert.assertEquals(REVISION_0_000001, latestUncommittedRevisionNumber);
                Assert.assertEquals(null, technicalRevision.getPreviousRevision());
                Assert.assertEquals(REVISION_0_000001, technicalRevision.getNextRevision());
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER,
                        revision_0_000001.getPreviousRevision());
                Assert.assertEquals(null, revision_0_000001.getNextRevision());

                // 0.000000 (committed) <-> 0.000001 (committed)
                revisionRootHandler.commitLatestRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(REVISION_0_000001, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);

                // 0.000000 (committed) <-> 0.000001 (committed) <-> 1.000000 (uncommitted)
                revisionRootHandler.createMajorRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                revision_0_000001 = revisionRootHandler.getSpecificModel(transaction, REVISION_0_000001);
                revision_1_000000 = revisionRootHandler.getSpecificModel(transaction, REVISION_1_000000);
                Assert.assertEquals(REVISION_0_000001, latestCommittedRevisionNumber);
                Assert.assertEquals(REVISION_1_000000, latestUncommittedRevisionNumber);
                Assert.assertEquals(RevisionRootHandler.TECHNICAL_REVISION_NUMBER,
                        revision_0_000001.getPreviousRevision());
                Assert.assertEquals(REVISION_1_000000, revision_0_000001.getNextRevision());
                Assert.assertEquals(REVISION_0_000001, revision_1_000000.getPreviousRevision());
                Assert.assertEquals(null, revision_1_000000.getNextRevision());

                // 0.000000 (committed) <-> 0.000001 (committed) <-> 1.000000 (committed)
                revisionRootHandler.commitLatestRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(REVISION_1_000000, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);

                // 0.000000 (committed) <-> 0.000001 (committed) <-> 1.000000 (committed) <-> 1.000001 (uncommitted)
                revisionRootHandler.createMinorRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                revision_1_000000 = revisionRootHandler.getSpecificModel(transaction, REVISION_1_000000);
                revision_1_000001 = revisionRootHandler.getSpecificModel(transaction, REVISION_1_000001);
                Assert.assertEquals(REVISION_1_000000, latestCommittedRevisionNumber);
                Assert.assertEquals(REVISION_1_000001, latestUncommittedRevisionNumber);
                Assert.assertEquals(REVISION_0_000001, revision_1_000000.getPreviousRevision());
                Assert.assertEquals(REVISION_1_000001, revision_1_000000.getNextRevision());
                Assert.assertEquals(REVISION_1_000000, revision_1_000001.getPreviousRevision());
                Assert.assertEquals(null, revision_1_000001.getNextRevision());

                // 0.000000 (committed) <-> 0.000001 (committed) <-> 1.000000 (committed) <-> 1.000001 (committed)
                revisionRootHandler.commitLatestRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(REVISION_1_000001, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);

                // 0.000000 (committed) <-> 0.000001 (committed) <-> 1.000000 (committed) <-> 1.000001 (committed) <-> 
                // <-> 1.000002 (uncommitted)
                revisionRootHandler.createMinorRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                revision_1_000001 = revisionRootHandler.getSpecificModel(transaction, REVISION_1_000001);
                revision_1_000002 = revisionRootHandler.getSpecificModel(transaction, REVISION_1_000002);
                Assert.assertEquals(REVISION_1_000001, latestCommittedRevisionNumber);
                Assert.assertEquals(REVISION_1_000002, latestUncommittedRevisionNumber);
                Assert.assertEquals(REVISION_1_000000, revision_1_000001.getPreviousRevision());
                Assert.assertEquals(REVISION_1_000002, revision_1_000001.getNextRevision());
                Assert.assertEquals(REVISION_1_000001, revision_1_000002.getPreviousRevision());
                Assert.assertEquals(null, revision_1_000002.getNextRevision());

                // 0.000000 (committed) <-> 0.000001 (committed) <-> 1.000000 (committed) <-> 1.000001 (committed) <-> 
                // <-> 1.000002 (committed)
                revisionRootHandler.commitLatestRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(REVISION_1_000002, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);

                // 0.000000 (committed) <-> 0.000001 (committed) <-> 1.000000 (committed) <-> 1.000001 (committed) <-> 
                // <-> 1.000002 (committed) <-> 2.000000 (uncommitted)
                revisionRootHandler.createMajorRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                revision_1_000002 = revisionRootHandler.getSpecificModel(transaction, REVISION_1_000002);
                revision_2_000000 = revisionRootHandler.getSpecificModel(transaction, REVISION_2_000000);
                Assert.assertEquals(REVISION_1_000002, latestCommittedRevisionNumber);
                Assert.assertEquals(REVISION_2_000000, latestUncommittedRevisionNumber);
                Assert.assertEquals(REVISION_1_000001, revision_1_000002.getPreviousRevision());
                Assert.assertEquals(REVISION_2_000000, revision_1_000002.getNextRevision());
                Assert.assertEquals(REVISION_1_000002, revision_2_000000.getPreviousRevision());
                Assert.assertEquals(null, revision_2_000000.getNextRevision());

                // 0.000000 (committed) <-> 0.000001 (committed) <-> 1.000000 (committed) <-> 1.000001 (committed) <-> 
                // <-> 1.000002 (committed) <-> 2.000000 (committed)
                revisionRootHandler.commitLatestRevision(transaction);
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(REVISION_2_000000, latestCommittedRevisionNumber);
                Assert.assertEquals(null, latestUncommittedRevisionNumber);

                return null;
            }
        });
    }

    /**
     * When the maximum number of minor revisions within one major revision is reached, a new major revision 
     * has to be created in order to prevent the overflow.
     * 
     * Also, a transition to the new major revision has to be performed so the following incremental update 
     * is performed correctly as if it was performed in a minor revision.
     */
    @Test
    public void testMinorRevisionOverflow() {
        cleanGraph();
        createBasicGraph(true);

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                RevisionRootHandler revisionRootHandler = getRevisionRootHandler();
                Vertex revisionRoot = revisionRootHandler.getRoot(transaction);
                Double latestCommittedRevisionNumber;
                Double latestUncommittedRevisionNumber;

                // create revision 1.000001 and commit it
                revisionRootHandler.createMinorRevision(transaction);
                revisionRootHandler.commitLatestRevision(transaction);

                // get revision node representing revision 1.000001
                List<Vertex> revisionVertices = GraphOperation.getAdjacentVertices(revisionRoot, Direction.OUT,
                        new RevisionInterval(REVISION_1_000001), EdgeLabel.HAS_REVISION);
                Assert.assertEquals(1, revisionVertices.size());
                Vertex revisionNode = revisionVertices.get(0);

                // Change revision 1.000001 to 1.999999 (at both revision node and revision root)
                revisionNode.setProperty(NodeProperty.REVISION_NODE_REVISION.t(), REVISION_1_999999);
                revisionRoot.setProperty(NodeProperty.LATEST_COMMITTED_REVISION.t(), REVISION_1_999999);
                revisionRoot.setProperty(NodeProperty.LATEST_UNCOMMITTED_REVISION.t(), -1.0);
                // latest committed revision is now 1.999999

                // Try to create new minor revision -> overflow expected
                revisionRootHandler.createMinorRevision(transaction);

                // New revision has to be 2.000000
                latestCommittedRevisionNumber = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                latestUncommittedRevisionNumber = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                Assert.assertEquals(REVISION_1_999999, latestCommittedRevisionNumber);
                Assert.assertEquals(REVISION_2_000000, latestUncommittedRevisionNumber);

                // check that transition from 1.999999 to 2.999999 was performed
                // all vertices and edges valid in 1.999999 (tranEnd=1.999999) should be newly valid in 2.999999 (tranEnd=2.999999)
                RevisionInterval walkthroughRevisionInterval = new RevisionInterval(REVISION_1_999999);
                Double expectedRevision = REVISION_2_999999;
                
                // first check the main graph (starting with super root)
                Vertex superRoot = getSuperRootHandler().getRoot(transaction);
                checkTranEndRecursively(superRoot, walkthroughRevisionInterval, expectedRevision);
                
                //second check source code graph (starting with source root)
                Vertex sourceRoot = getSourceRootHandler().getRoot(transaction);
                checkTranEndRecursively(sourceRoot, walkthroughRevisionInterval, expectedRevision);
                
                return null;
            }
        });
    }

    private void checkTranEndRecursively(Vertex vertex, RevisionInterval walkthroughRevisionInterval,
            Double expectedRevision) {
        // get all possible adjacent edges (except hasRevision)
        List<Edge> adjacentEdges = GraphOperation.getAdjacentEdges(vertex, Direction.BOTH, walkthroughRevisionInterval,
                EdgeLabel.DIRECT, EdgeLabel.FILTER, EdgeLabel.HAS_ATTRIBUTE, EdgeLabel.HAS_PARENT,
                EdgeLabel.HAS_RESOURCE, EdgeLabel.HAS_SOURCE, EdgeLabel.IN_LAYER, EdgeLabel.MAPS_TO);

        // check for each edge its tranEnd (tranEnd has to be 2.999999)
        for (Edge edge : adjacentEdges) {
            Double tranEnd = edge.getProperty(EdgeProperty.TRAN_END.t());
            Assert.assertEquals(expectedRevision, tranEnd);
        }

        // recursively repeat for all direct children vertices
        List<Vertex> directChildren = GraphOperation.getDirectChildren(vertex, walkthroughRevisionInterval);
        for (Vertex child : directChildren) {
            checkTranEndRecursively(child, walkthroughRevisionInterval, expectedRevision);
        }

    }
}
