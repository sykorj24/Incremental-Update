package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Základní implementace UniversalVisitoru pro účely testování.
 * Visitor do textového řetězce zaznamenává požadované informace 
 * o všech uzlech a hranách, které byly navštíveny (nic nevynechává).
 * Detail informace o uzlu/hraně lze nastavit, viz seznam {@link #properties}.
 * <br/>
 * Výslednou hodnotu (textový řetězec) z průchodu lze získat voláním {@link #toString()},
 * je to v této metodě, aby se to dobře rovnou ladilo, v debuggeru je hodnota hned vidět.
 * <b>Pozor</b>: v jUnit assert je potřeba toString() explicitně zavolat, tj. 
 * <code>Assert.assertEquals("[A][B][C]", stepTracer.toString());</code>
 * <br/>
 * Plný výstup pro vrchol je tvaru <code>[STEP:TYPE:DISTANCE:NAME*]</code>,
 * pro hranu je tvaru <code>[STEP:TYPE:DIRECTION:NAME]</code>. 
 * Pořadí informací je v obou případech volitelné, ale doporučení je, 
 * aby NAME a REPEAT_INDICATOR šly až na konci (třída je jen pro účely testování, takže není dokonale obecná).
 * 
 * @author Erik Kratochvíl
 */
public class StepTracer implements UniversalVisitor {

    /**
     * Vlastnosti vrcholů a hran, které mají být reportovány.
     * <table>
     * <tr><td>STEP</td><td>číslo kroku, ve kterém byl vrchol nebo hrana navštívena, počítáno od 0</td></tr>
     * <tr><td>TYPE</td><td>typ vrcholu/uzlu nebo typ (label) hrany</td></tr>
     * <tr><td>NAME</td><td>jméno vrcholu nebo vygenerované "jméno" hrany tvaru</td></tr>
     * <tr><td>REPEAT_INDICATOR</td><td>indikátor, že vrchol už dříve byl navštíven (tj. nevidíme ho poprvé)</td></tr>
     * <tr><td>DISTANCE</td><td>vzdálenost od nejbližšího výchozího vrcholu</td></tr>
     * <tr><td>DIRECTION</td><td>orientace hrany z pohledu vrcholu, pro který je hrana reportována</td></tr>
     * </table>
     * Vlastnosti vrcholů jsou přebírány ze třídy {@link AboutVertex}.
     * 
     * @author Erik Kratochvíl
     */
    public static enum Properties {
        STEP, TYPE, NAME, DISTANCE, REPEAT_INDICATOR, DIRECTION
    }

    /** Záznam (log) z průchodu ve tvaru textového řetězce. */
    private StringBuilder builder = new StringBuilder();
    /** Číslo aktuálního kroku. */
    private int step = 0;
    /** Pole vlastností, které se mají pro vrcholy a hrany reportovat. Pořadí je respektováno. */
    private Properties[] properties;

    /**
     * Nový tracer reportující dané vlastnosti vrcholů a hran.
     * @param properties vlastnosti vrcholů a hran, pořadí je respektováno
     */
    public StepTracer(Properties... properties) {
        this.properties = properties;
    }

    @Override
    public boolean visit(Vertex vertex, long distance, boolean visitedForTheFirstTime,
            RevisionInterval revisionInterval) {
        final AboutVertex v = new AboutVertex(vertex);
        builder.append("[");
        for (Properties element : properties) {
            switch (element) {
            case STEP:
                builder.append(step);
                builder.append(":");
                break;
            case DISTANCE:
                builder.append(distance);
                builder.append(":");
                break;
            case TYPE:
                builder.append(v.type);
                builder.append(":");
                break;
            case NAME:
                builder.append(v.name);
                break;
            case REPEAT_INDICATOR:
                if (!visitedForTheFirstTime) {
                    builder.append("*");
                }
                break;
            }
        }
        builder.append("]");
        ++step;
        return true;
    }

    @Override
    public boolean visit(Edge edge, EdgeLabel label, Direction direction, RevisionInterval revisionInterval) {
        builder.append("[");
        for (Properties element : properties) {
            switch (element) {
            case STEP:
                builder.append(step);
                builder.append(":");
                break;
            case TYPE:
                builder.append(edge.getLabel());
                builder.append(":");
                break;
            case DIRECTION:
                builder.append(direction);
                builder.append(":");
                break;
            case NAME:
                final AboutVertex a = new AboutVertex(edge.getVertex(Direction.OUT));
                final AboutVertex b = new AboutVertex(edge.getVertex(Direction.IN));
                builder.append(a.name);
                builder.append("->");
                builder.append(b.name);
                break;
            default:
                break;
            }
        }
        builder.append("]");
        ++step;
        return true;
    }

    @Override
    public String toString() {
        return builder.toString();
    }

}
