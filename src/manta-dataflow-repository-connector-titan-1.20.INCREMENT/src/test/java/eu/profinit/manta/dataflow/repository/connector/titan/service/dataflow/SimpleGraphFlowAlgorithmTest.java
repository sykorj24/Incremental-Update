package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.SimpleGraphFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmException;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

public class SimpleGraphFlowAlgorithmTest extends TestTitanDatabaseProvider {
    @Test
    public void testFullGraph() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                SimpleGraphFlowAlgorithm algorithm = new SimpleGraphFlowAlgorithm();
                Set<Object> outputNodes = new HashSet<Object>();
                try {
                    outputNodes.addAll(algorithm.findNodesByAlgorithm(Collections.singletonList(t1c2), null, transaction,
                            Direction.OUT, REVISION_INTERVAL_1_0));
                    outputNodes.addAll(algorithm.findNodesByAlgorithm(Collections.singletonList(t1c2), null, transaction,
                            Direction.IN, REVISION_INTERVAL_1_0));
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(4, outputNodes.size());

                return null;
            }
        });
    }

    @Test
    public void testAllowedNodes() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                Set<Object> allowedNodes = new HashSet<Object>();
                allowedNodes.add(t1c1.getId());
                allowedNodes.add(t1c2.getId());
                allowedNodes.add(t2c1.getId());

                SimpleGraphFlowAlgorithm algorithm = new SimpleGraphFlowAlgorithm();
                Set<Object> outputNodes = new HashSet<Object>();
                try {
                    outputNodes.addAll(algorithm.findNodesByAlgorithm(Collections.singletonList(t1c2), allowedNodes,
                            transaction, Direction.OUT, REVISION_INTERVAL_1_0));
                    outputNodes.addAll(algorithm.findNodesByAlgorithm(Collections.singletonList(t1c2), allowedNodes,
                            transaction, Direction.IN, REVISION_INTERVAL_1_0));
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(3, outputNodes.size());
                Assert.assertTrue(outputNodes.contains(t1c1.getId()));
                Assert.assertTrue(outputNodes.contains(t1c2.getId()));
                Assert.assertTrue(outputNodes.contains(t2c1.getId()));
                Assert.assertFalse(outputNodes.contains(t2c2.getId()));

                return null;
            }
        });
    }
}
