package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.routine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.AttributeNames;
import eu.profinit.manta.dataflow.repository.connector.titan.connection.SimpleDatabaseHolder;
import eu.profinit.manta.dataflow.repository.connector.titan.service.AbstractRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.routine.RoutineDataFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

/**
 * Test {@link RoutineDataFlowAlgorithm} na přítomnost chyby reportované v DEV-1731.
 *
 * @author ekratochvil
 */
public class Dev1731Test {

    /** SLF4J logger. Seeing is believing, but feeling is the truth. */
    private static final Logger LOGGER = LoggerFactory.getLogger(Dev1731Test.class);

    /** Cesta k uložené grafové databázi. Nastaveno relativně k adresáři /test/resources dle očekávání Maven. */
    private static final String DIR = "/DEV-1731/";

    /** Shortcut na speciální interval, který zahrnuje všechny možné revize. */
    private static final RevisionInterval ALL_REVISIONS = new RevisionInterval(0, Integer.MAX_VALUE);

    /**
     * Vlastní zjednodušená implementace callbacku, která se dá trochu rozumně parametrizovat.
     * Není thread-safe.
     */
    private abstract class Task implements TransactionCallback<String> {

        /** Zapamatované jméno vrcholu, na kterém se má task spustit. */
        private String vertexName;

        /**
         * Spuštění tohoto tasku (sebe sama) v grafové databázi v READ ONLY transakci.
         *
         * @param vertexName jméno vrcholu, na kterém se má task spustit; pokud je null, task se spouští se super rootem
         */
        public void execute(final String vertexName) {
            this.vertexName = vertexName;
            database.runInTransaction(TransactionLevel.READ, this);
        }

        @Override
        public String getModuleName() {
            return "TEST";
        }

        @Override
        public String callMe(final TitanTransaction tx) {
            LOGGER.debug("Dummy TransactionCallback wrapper.");
            final Vertex root = getSuperRoot(tx);
            this.go(this.vertexName != null ? findFirstVertexWithName(this.vertexName, root) : root, tx);
            return "OK";
        }

        /**
         * Vlastní aktivita.
         *
         * @param vertex první nalezený vrchol daného jména, na kterém má být task vykonán
         * @param tx transakce v TitanDB (kvůli případnému volání dalších funkcí, které potřebují o transakci také vědět)
         */
        abstract void go(final Vertex vertex, final TitanTransaction tx);

    }

    /** Načtená grafová databáze. Instance je jenom jedna a je celou dobu společná pro všechny testy. */
    private SimpleDatabaseHolder database;

    private AbstractRootHandler superRootHandler;

    /**
     * Načtení grafové databáze. Společná jedna jediná instance pro všechny testy!
     * 
     * @throws IOException
     */
    @Before
    public void loadDatabase() throws IOException {
        LOGGER.info("Loading database {}", DIR);
        final URL url = Dev1731Test.class.getResource(DIR);
        Validate.notNull(url);
        // do not use direct path string - whitespace will be escaped and different directory will be used
        String path = URLDecoder.decode(url.getFile(), "UTF-8");
        LOGGER.info("Database actually at {}", path);
        File dbFile = new File(path);
        LOGGER.info("File read: {}, write: {}, execute: {}, is directory: {}", dbFile.canRead(), dbFile.canWrite(),
                dbFile.canExecute(), dbFile.isDirectory());
        LOGGER.info("Files inside DB directory:");
        for (File file : dbFile.listFiles()) {
            LOGGER.info("File: {}, read: {}, write: {}, execute: {}, is directory: {}", file.getCanonicalPath(), file.canRead(), file.canWrite(),
                    file.canExecute(), file.isDirectory());
        }
        TestTitanDatabaseProvider databaseProvider = new TestTitanDatabaseProvider(path, false) {
        };
        database = databaseProvider.getDatabaseHolder();
        Validate.notNull(database);
        superRootHandler = databaseProvider.getSuperRootHandler();
        Validate.notNull(superRootHandler);
    }

    /**
     * Korektní uzavření grafové databáze.
     */
    @After
    public void closeDatabase() {
        Validate.notNull(database);
        database.closeDatabase();
        LOGGER.info("Database closed.");
    }

    /**
     * Test, že test je sepsán korektně a očekávané vstupy (hlavně tedy grafová databáze) jsou dostupné.
     */
    @Test
    public void testSmoke() {
        LOGGER.info("SMOKE TEST");
        new Task() {
            @Override
            public void go(final Vertex vertex, final TitanTransaction tx) {
                LOGGER.info("Here it is: {} ", getFullyQualifiedNameOf(vertex));
            }
        }.execute("sp1"); // DB1:SOURCEP1:SP1, v[220]
    }

    /**
     * Test naivního průchodu grafem z vrcholu DB1:SOURCEP1:SP1, v[220] bez ohledu na cykly či CALL ID na hranách.
     * Očekává se, že bude zobrazen detailní "strom" průchodu grafem z výchozího vrcholu,
     * přičemž bude ukazovat "nesprávný" výstup do DB1:TARGET2:T2C3, v[92], kam se dostane přes hranu s CALL ID 4244d79b-e895-46b5-a286-51c50167e554.
     *
     * Jinými slovy, ověřuje se, že podkladová databáze obsahuje správný problematický graf, na kterém chceme chování testovat.
     */
    @Test
    public void testWalkingGraphNaively() {
        LOGGER.debug("NAIVE DIRECT TRAVERSAL TEST");
        final StringBuilder result = new StringBuilder();
        new Task() { // GOSH, I'd give anything for Java 8
            @Override
            void go(final Vertex start, final TitanTransaction tx) {
                walk(start, null, "", true, result);
            }
        }.execute("sp1"); // DB1:SOURCEP1:SP1, v[220]
        final String text = result.toString();
        System.out.println(text);
        final String expected = "+- DB1:SOURCEP1:SP1, v[220]\n"
                + "   +- db1:procInner.sql:<9,1>SQL_create_procedure_stmt:<10,1>SQL_block_stmt:<16,5>SQL_select_stmt:<16,5>SelectInto:1 p1, v[212]\n"
                + "      +- db1:procInner.sql:<9,1>SQL_create_procedure_stmt:<10,1>SQL_block_stmt:<18,5>SQL_procedure_call_stmt:<18,5>Call procInner:in_param, v[196]\n"
                + "         +- DB1:procInner:in_param, v[256]   ~> 5d913e3e-5969-44a3-bc39-98efa2c6a28e\n"
                + "            +- db1:procInner.sql:<1,1>SQL_create_procedure_stmt:<2,1>SQL_block_stmt:<3,5>SQL_select_stmt:<3,5>SelectInto:1 rettt, v[240]\n"
                + "               +- DB1:procInner:rettt, v[260]\n"
                + "                  +- db1:procInner.sql:<9,1>SQL_create_procedure_stmt:<10,1>SQL_block_stmt:<24,5>SQL_procedure_call_stmt:<24,5>Call procInner:rettt, v[116]   ~> 4244d79b-e895-46b5-a286-51c50167e554\n"
                + "                  |  +- db1:procInner.sql:<9,1>SQL_create_procedure_stmt:<10,1>SQL_block_stmt:<25,5>SQL_insert_stmt:<25,5>Insert:3 T2C3, v[60]\n"
                + "                  |     +- DB1:TARGET2:T2C3, v[92]\n"
                + "                  +- db1:procInner.sql:<9,1>SQL_create_procedure_stmt:<10,1>SQL_block_stmt:<18,5>SQL_procedure_call_stmt:<18,5>Call procInner:rettt, v[200]   ~> 5d913e3e-5969-44a3-bc39-98efa2c6a28e\n"
                + "                     +- db1:procInner.sql:<9,1>SQL_create_procedure_stmt:<10,1>SQL_block_stmt:<19,5>SQL_insert_stmt:<19,5>Insert:3 T1C3, v[148]\n"
                + "                        +- DB1:TARGET1:T1C3, v[176]\n";
        assertEquals("Testovací databáze neobsahuje problematický případ, na který jsou testy zkonstruovány.", expected, text);
    }

    /**
     * Test {@link RoutineDataFlowAlgorithm} (RDFA).
     * Očekává se, že na rozdíl od naivního průchodu, bude RDFA průchod "inteligentní",
     * tj. nedojde k nechtěným průchodům přes hrany, které nemají správná CALL ID.
     *
     * Jinými slovy, očekává se, že navštíveny budou právě tyto vrcholy (ne nutně v tomto pořadí):
     *  - procInner.rettt, v[260]
     *  - SOURCEP1.SP1, v[220]
     *  - TARGET1.T1C3, v[176]
     *  - procInner.in_param, v[256]
     *  - <19,5>Insert.3 T1C3, v[148]
     *  - <18,5>Call procInner.in_param, v[196]
     *  - <16,5>SelectInto.1 p1, v[212]
     *  - <3,5>SelectInto.1 rettt, v[240]
     *  - <18,5>Call procInner.rettt, v[200]
     */
    @Test
    public void testWalkingGraphWithRDFA() {
        LOGGER.debug("ROUTINE DATA FLOW ALGORITHM TEST");
        final Set<Object> foundNodes = new HashSet<>();
        new Task() {
            @Override
            void go(final Vertex start, final TitanTransaction tx) {
                LOGGER.info("RoutineDataFlowAlgorithm initiated.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    // IDčka nalezených uzlů.
                    foundNodes.addAll(algorithm.findNodesByAlgorithm(startNodes, null, tx, Direction.OUT, ALL_REVISIONS));
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }
            }
        }.execute("sp1"); // DB1:SOURCEP1:SP1, v[220]
        final Set<Long> expectedNodes = new HashSet<>(Arrays.asList(260L, 220L, 176L, 256L, 148L, 196L, 212L, 240L, 200L));
        assertEquals("Algoritmus RDFA nenašel právě ty vrcholy, které najít měl.", expectedNodes, foundNodes);
    }

    /**
     *
     * @param name jméno hledaného vrcholu
     * @param root kořenový uzel stromu, ve kterém se má vrchol hledat
     * @return první vrchol daného jména v podstromě daném kořenovým uzlem (včetně tohoto uzlu), porovnání je case-insensitive, vyhledávání je do šířky (BFS)
     */
    private static Vertex findFirstVertexWithName(final String name, final Vertex root) {
        LOGGER.debug("Retrieving first vertex with name [{}].", name);
        final LinkedList<Vertex> candidates = new LinkedList<>(Arrays.asList(root));
        while (!candidates.isEmpty()) {
            final Vertex candidate = candidates.removeFirst(); // BFS
            final String candidateName = GraphOperation.getName(candidate);
            LOGGER.debug(" {}", candidateName);
            if (StringUtils.equalsIgnoreCase(name, candidateName)) {
                LOGGER.debug("Found: {}", getFullyQualifiedNameOf(candidate));
                return candidate;
            }
            for (final Vertex child : candidate.getVertices(Direction.IN, EdgeLabel.HAS_PARENT.t(), EdgeLabel.HAS_RESOURCE.t(), EdgeLabel.HAS_SOURCE.t())) {
                candidates.addLast(child); // BFS
            }
        }
        LOGGER.debug("Not found. Nothing for [{}].", name);
        return null;
    }

    /**
     *
     * @param vertex vrchol
     * @return plně kvalifikované jméno vrcholu (do všech nadřazených úrovní), obsahuje také ID vrcholu v grafové databázi pro jednoznačnou identifikaci
     */
    private static String getFullyQualifiedNameOf(final Vertex vertex) {
        if (vertex == null) {
            LOGGER.debug("Retrieving fully qualified name of NULL.");
            return "<n/a>";
        }
        LOGGER.debug("Retrieving fully qualified name of v[{}].", vertex.getId());
        final LinkedList<String> names = new LinkedList<>();
        for (Vertex node = vertex; node != null; node = GraphOperation.getParent(node)) {
            final String name = GraphOperation.getName(node);
            names.addFirst(name);
        }
        return String.format("%s, v[%s]", StringUtils.join(names, ":"), vertex.getId());
    }

    /**
     *
     * @param tx transakce TitanDB
     * @return super root, tj. hlavní kořenový vrchol, pod kterým žijí všechny další vrcholy
     */
    private Vertex getSuperRoot(final TitanTransaction tx) {
        LOGGER.debug("Retrieving SUPER ROOT.");
        return superRootHandler.getRoot(tx);
    }

    /**
     * Naivní rekurzivní průchod přes datové toky pro acyklické grafy.
     *
     * @param start počáteční vrchol
     * @param in vstupní hrana do vrcholu (kvůli tisku CALL_ID)
     * @param tab odsazení platné pro daný vrchol
     * @param result pro agregaci výsledku
     */
    private static void walk(final Vertex start, final Edge in, final String tab, final boolean isLast, final StringBuilder result) {
        final String name = getFullyQualifiedNameOf(start);
        String callId = null;
        if (in != null) {
            callId = in.getProperty(AttributeNames.EDGE_CALL_ID);
        }

        // Fancy print.
        result.append(tab);
        result.append("+- ");
        result.append(name);
        if (callId != null) {
            result.append("   ~> ");
            result.append(callId);
        }
        result.append("\n");

        for (final Iterator<Edge> edges = start.getEdges(Direction.OUT, EdgeLabel.DIRECT.t()).iterator(); edges.hasNext();) {
            final Edge out = edges.next();
            final String subTab = isLast ? "   " : "|  ";
            walk(out.getVertex(Direction.IN), out, tab + subTab, !edges.hasNext(), result);
        }
    }


}

