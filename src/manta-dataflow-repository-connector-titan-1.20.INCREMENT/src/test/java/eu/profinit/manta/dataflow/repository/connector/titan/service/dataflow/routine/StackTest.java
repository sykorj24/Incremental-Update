package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.routine;


import org.junit.Test;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.routine.Stack;

/**
 * Zjednodušený test pro třídu Stack.
 * Vytvořeno primárně kvůli lepšímu pochopení chování Stacku.
 * Ve skutečnosti nic netestuje.
 *
 * @author ekratochvil
 */
public class StackTest {

    private static final void print(final Stack stack) {
        System.out.println(stack.getCompleteStackAsString());
    }

    @Test
    public void testRoot() {
        final Stack stack = new Stack();
        stack.getCompleteStackAsString();
    }


    @Test
    public void testPush() {
        final Stack stack = new Stack();
        stack.push("a", null);
        stack.push("b", null);
        print(stack);
    }

    @Test
    public void testPush2() {
        final Stack stack = new Stack();
        final Stack a = stack.push("a", null);
        a.push("b", null);
        print(stack);
    }

    @Test
    public void testPush3() {
        final Stack stack = new Stack();
        stack.push("a", null);
        stack.push("b", null);
        stack.push("a", null); // nepridava znovu
        print(stack);
    }

    @Test
    public void testPush4() {
        final Stack stack = new Stack();
        final Stack a1 = stack.push("a", null);
        final Stack b = a1.push("b", null);
        b.push("a", null); // prida znovu
        print(stack);
    }

    @Test
    public void testPush5() {
        final Stack stack = new Stack();
        final Stack a1 = stack.push("a", null);
        final Stack b = a1.push("b", null);
        final Stack a2 = b.push("a", null); // prida znovu
        final Stack c = a2.push("c", null); // prida pod a1, nikoli pod a2!
        c.push("a", null);
        print(stack);
    }

    @Test
    public void testPush6() {
        final Stack stack = new Stack();
        stack.push("a", null).push("b", null).push("a", null).push("c", null).push("a", null);
        print(stack);
    }

    @Test
    public void testPush7() {
        final Stack stack = new Stack();
        stack.push("a", null).push("b", null).push("a", null).push("c", null).push("a", null).push("d", null).push("c", null).push("c", null).push("c", null);
        print(stack);
    }

}
