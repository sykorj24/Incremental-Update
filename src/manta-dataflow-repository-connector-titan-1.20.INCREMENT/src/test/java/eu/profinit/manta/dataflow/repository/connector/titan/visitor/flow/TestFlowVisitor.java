package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterResult;


/**
 * Visitor, co počítá a pouští všechny uzly.
 * 
 * @author tfechtner
 */
public class TestFlowVisitor implements FlowVisitor {

    private int nodeCount;
    
    @Override
    public FilterResult visitAndContinue(Vertex node, FlowItem flowItem) {
        nodeCount++;
        return FilterResult.OK_CONTINUE;
    }

    /**
     * @return počet uzlů
     */
    public int getNodeCount() {
        return nodeCount;
    }
    
}
