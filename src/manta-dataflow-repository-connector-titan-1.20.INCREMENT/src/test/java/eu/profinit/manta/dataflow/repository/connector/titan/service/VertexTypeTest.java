package eu.profinit.manta.dataflow.repository.connector.titan.service;

import org.junit.Assert;
import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

/**
 * Typu uzlů.
 * @author tfechtner
 *
 */
public class VertexTypeTest extends TestTitanDatabaseProvider {
    /**
     * Test zjištění typu.
     */
    @Test
    public void testGetType() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);
                Assert.assertEquals(VertexType.SUPER_ROOT, VertexType.getType(root));

                Vertex res = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator().next();
                Assert.assertEquals(VertexType.RESOURCE, VertexType.getType(res));

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Assert.assertEquals(VertexType.NODE, VertexType.getType(t1c1));

                Vertex attr = GraphCreation.createNodeAttribute(transaction, t1c1, "attr", "value", 1.000000);
                Assert.assertEquals(VertexType.ATTRIBUTE, VertexType.getType(attr));

                return null;
            }
        });
    }

    /**
     * Test, kdyz je parametr null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetTypeNull() {
        Assert.assertEquals(VertexType.ATTRIBUTE, VertexType.getType(null));
    }

    /**
     * Test, kdyz je parametr divny vrchol.
     */
    @Test(expected = IllegalStateException.class)
    public void testGetTypeIncorectVertex() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex v = transaction.addVertex();
                Assert.assertEquals(VertexType.ATTRIBUTE, VertexType.getType(v));
                return null;
            }
        });
    }
}
