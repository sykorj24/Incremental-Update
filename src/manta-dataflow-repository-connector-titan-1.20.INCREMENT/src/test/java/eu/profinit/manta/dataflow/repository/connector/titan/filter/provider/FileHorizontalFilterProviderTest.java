package eu.profinit.manta.dataflow.repository.connector.titan.filter.provider;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;

public class FileHorizontalFilterProviderTest extends TestTitanDatabaseProvider {
    @Test
    public void test() {
        cleanGraph();
        createBasicGraph();

        FileHorizontalFilterProvider provider = new FileHorizontalFilterProvider();
        provider.setDatabaseHolder(getDatabaseHolder());
        provider.setSuperRootHandler(getSuperRootHandler());

        provider.setConfigurationFile(new File("src/test/resources/horizontalFilters.json"));
        provider.refreshFilters();

        Assert.assertEquals(5, provider.getFilters().size());
        Assert.assertNotNull(provider.getFilter("Teradata"));
        Assert.assertNotNull(provider.getFilter("innerTrans"));
        Assert.assertNotNull(provider.getFilter("hideTeradata"));
        Assert.assertNotNull(provider.getFilter("hideDeduction"));
        Assert.assertNotNull(provider.getFilter("hideTempTables"));
        Assert.assertNull(provider.getFilter("none"));
    }
}
