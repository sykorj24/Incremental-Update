package eu.profinit.manta.dataflow.repository.connector.titan.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;

/**
 * Testování operací nad grafem s ohledem na změny v revizích.
 * @author pholecek
 * @author jsykora
 *
 */
public class GraphOperationRevisionTest extends TestTitanDatabaseProvider {
    
    /**
     * Test atributu uzlu.
     * 
     * t1
     * |
     * c1_____attr1 <1.000000, 1.999999>
     * ||\____attr2 <2.000000, 2.999999>
     * ||_____attr3 <2.000000, 2.999999>
     * |______attr4 <3.000000, 3.999999>
     */
    @Test
    public void testGetNodeAttribute() {
        cleanGraph();
        createExtendedGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex t1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Assert.assertEquals("TABLE", GraphOperation.getFirstNodeAttribute(t1, "TABLE_TYPE", REVISION_INTERVAL_1_000000_1_000000));

                Assert.assertNull(GraphOperation.getFirstNodeAttribute(t1, "nic", REVISION_INTERVAL_1_000000_1_000000));

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex attr1 = GraphCreation.createNodeAttribute(transaction, t1c1, "attr", "val1", REVISION_1_000000);
                Vertex attr2 = GraphCreation.createNodeAttribute(transaction, t1c1, "attr", "val2", REVISION_2_000000);
                Vertex attr3 = GraphCreation.createNodeAttribute(transaction, t1c1, "attr", "val3", REVISION_2_000000);
                Vertex attr4 = GraphCreation.createNodeAttribute(transaction, t1c1, "attr", "val4", REVISION_3_000000);

                List<Object> list = GraphOperation.getNodeAttribute(t1c1, "attr", REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(1, list.size());
                Assert.assertTrue(list.contains("val1"));
                
                list = GraphOperation.getNodeAttribute(t1c1, "attr", REVISION_INTERVAL_2_000000_2_000000);
                Assert.assertEquals(2, list.size());
                Assert.assertTrue(list.contains("val2"));
                Assert.assertTrue(list.contains("val3"));
                
                list = GraphOperation.getNodeAttribute(t1c1, "attr", REVISION_INTERVAL_3_000000_3_000000);
                Assert.assertEquals(1, list.size());
                Assert.assertTrue(list.contains("val4"));
                
                /*
                 * Set end revision of the attr2 attribute from revision 2.999999 to 3.000000
                 * 
                 * t1
                 * |
                 * c1_____attr1 <1.000000, 1.999999>
                 * ||\____attr2 <2.000000, 3.000000>
                 * ||_____attr3 <2.000000, 2.999999>
                 * |______attr4 <3.000000, 3.999999>
                 */
                Edge edge = attr2.getEdges(Direction.IN, EdgeLabel.HAS_ATTRIBUTE.t()).iterator().next();
                RevisionUtils.setEdgeTransactionEnd(edge, REVISION_3_000000);
                
                list = GraphOperation.getNodeAttribute(t1c1, "attr", REVISION_INTERVAL_2_000000_3_000000);
                Assert.assertEquals(3, list.size());
                Assert.assertTrue(list.contains("val2"));
                Assert.assertTrue(list.contains("val3"));
                Assert.assertTrue(list.contains("val4"));

                return null;
            }
        });
    }

    
    /**
     * Test atributu uzlu.
     * 
     * t1
     * |
     * +--------tableType <1.000000, 1.999999>
     * +--------attr1 <1.000000, 1.999999>
     * +--------attr2 <2.000000, 2.999999>
     * +--------attr3 <2.000000, 2.999999>
     * +--------attr4 <3.000000, 3.999999>
     */
    @Test
    public void testGetAllNodeAttributes() {
        cleanGraph();
        createExtendedGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex t1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex val1 = GraphCreation.createNodeAttribute(transaction, t1, "attr", "val1", REVISION_1_000000);
                Vertex val2 = GraphCreation.createNodeAttribute(transaction, t1, "attr", "val2", REVISION_2_000000);
                Vertex val3 = GraphCreation.createNodeAttribute(transaction, t1, "attr", "val3", REVISION_2_000000);
                Vertex val4 = GraphCreation.createNodeAttribute(transaction, t1, "attr", "val4", REVISION_3_000000);

                Map<String, List<Object>> attrs = GraphOperation.getAllNodeAttributes(t1, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(2, attrs.size());
                Assert.assertEquals(Collections.singletonList("TABLE"), attrs.get("TABLE_TYPE"));
                List<Object> list = attrs.get("attr");
                
                Assert.assertEquals(1, list.size());
                Assert.assertTrue(list.contains("val1"));
                
                attrs = GraphOperation.getAllNodeAttributes(t1, REVISION_INTERVAL_2_000000_2_000000);
                list = attrs.get("attr");
                Assert.assertEquals(2, list.size());
                Assert.assertTrue(list.contains("val2"));
                Assert.assertTrue(list.contains("val3"));
                
                attrs = GraphOperation.getAllNodeAttributes(t1, REVISION_INTERVAL_3_000000_3_000000);
                list = attrs.get("attr");
                Assert.assertEquals(1, list.size());
                Assert.assertTrue(list.contains("val4"));
                
                /*
                 * t1
                 * |
                 * +--------tableType <1.000000, 2.999999>
                 * +--------attr1 <1.000000, 1.999999>
                 * +--------attr2 <2.000000, 3.000000>
                 * +--------attr3 <2.000000, 2.999999>
                 * +--------attr4 <3.000000, 3.999999>
                 */
                Edge edge = val2.getEdges(Direction.IN, EdgeLabel.HAS_ATTRIBUTE.t()).iterator().next();
                RevisionUtils.setEdgeTransactionEnd(edge, REVISION_3_000000);
                
                attrs = GraphOperation.getAllNodeAttributes(t1, REVISION_INTERVAL_3_000000_3_000000);
                list = attrs.get("attr");
                Assert.assertEquals(2, list.size());
                Assert.assertTrue(list.contains("val2"));
                Assert.assertTrue(list.contains("val4"));

                return null;
            }
        });
    }

    
    /**
     * Data flow edges test
     */
    @Test
    public void testGetAdjacentVertices() {
        cleanGraph();
        createExtendedGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                // t2c1 has these edges: incoming filter flow from t1c1 <1.000000, 2.999999>
                //                       incoming direct flow from t1c2 <1.000000, 1.999999>
                //                       outcoming direct flow to t2c2  <1.000000, 1.999999>
                //                       outcoming direct flow to t3c1  <2.000000, 2.999999>
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();

                // t2c2
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();



                // none of the edges to/from t2c1 fulfills the conditions:
                //      incoming are only two edges from t1c1 and t1c2
                //      edge from t1c2 was removed in the revision 2.000000
                //      edge from t1c1 is filter flow, not direct flow
                List<Vertex> adjacents = GraphOperation.getAdjacentVertices(t2c1, Direction.IN, REVISION_INTERVAL_2_000000_2_000000, EdgeLabel.DIRECT);
                Assert.assertEquals(0, adjacents.size());

                // incoming filter flow from t1c1 <1.000000, 2.999999>
                adjacents = GraphOperation.getAdjacentVertices(t2c1, Direction.IN, REVISION_INTERVAL_2_000000_2_000000, EdgeLabel.FILTER);
                Assert.assertEquals(1, adjacents.size());
                Assert.assertEquals("t1c1", adjacents.get(0).getProperty(NodeProperty.NODE_NAME.t()));
                
                // outcoming direct flow to t3c1  <2.000000, 2.999999>
                adjacents = GraphOperation.getAdjacentVertices(t2c1, Direction.OUT, REVISION_INTERVAL_2_000000_2_000000, EdgeLabel.DIRECT);
                Assert.assertEquals(1, adjacents.size());
                Assert.assertEquals("t3c1", adjacents.get(0).getProperty(NodeProperty.NODE_NAME.t()));
                
                // t2c1 has no outcoming filter flow edges at all
                adjacents = GraphOperation.getAdjacentVertices(t2c1, Direction.OUT, REVISION_INTERVAL_2_000000_2_000000, EdgeLabel.FILTER);
                Assert.assertEquals(0, adjacents.size());
                
                // t2c2 has no edges in the revision 2.0000000 (t2c2 was removed in the revision 2.000000)
                adjacents = GraphOperation.getAdjacentVertices(t2c2, Direction.IN, REVISION_INTERVAL_2_000000_2_000000, EdgeLabel.DIRECT);
                Assert.assertEquals(0, adjacents.size());
                
                // outcoming direct flow to t3c1  <2.000000, 2.999999>
                adjacents = GraphOperation.getAdjacentVertices(t2c1, Direction.BOTH, REVISION_INTERVAL_2_000000_2_000000, EdgeLabel.DIRECT);
                Assert.assertEquals(1, adjacents.size());
                Assert.assertEquals("t3c1", adjacents.get(0).getProperty(NodeProperty.NODE_NAME.t()));
                
                // incoming filter flow from t1c1 <1.000000, 2.999999>
                adjacents = GraphOperation.getAdjacentVertices(t2c1, Direction.BOTH, REVISION_INTERVAL_2_000000_2_000000, EdgeLabel.FILTER);
                Assert.assertEquals(1, adjacents.size());
                Assert.assertEquals("t1c1", adjacents.get(0).getProperty(NodeProperty.NODE_NAME.t()));
                
                return null;
            }
        });
    }
    
    /**
     * Test listových potomků.
     */
    @Test
    public void testGetAllLists() {
        cleanGraph();
        createExtendedGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                // leaves of db in revision 1.000000: t1c1, t1c2, t2c1, t2c2
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                List<Vertex> vertices = GraphOperation.getAllLists(db, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(2 + 2, vertices.size());
                
                // leaves of db in revision 2.000000: t1c1, t1c2, t2c1, t3c1, t3c2
                vertices = GraphOperation.getAllLists(db, REVISION_INTERVAL_2_000000_2_000000);
                Assert.assertEquals(2 + 1 + 2, vertices.size());

                // leaves of table1 in revision 1.000000: t1c1, t1c2
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                vertices = GraphOperation.getAllLists(table1, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(2, vertices.size());
                
                // leaves of table2 in revision 1.000000: t2c1, t2c2
                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                vertices = GraphOperation.getAllLists(table2, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(2, vertices.size());
                
                // leaves of table2 in revision 2.000000: t2c1
                vertices = GraphOperation.getAllLists(table2, REVISION_INTERVAL_2_000000_2_000000);
                Assert.assertEquals(1, vertices.size());
                
                vertices = null;
                Vertex table3 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table3").iterator().next();
                try {
					vertices = GraphOperation.getAllLists(table3, REVISION_INTERVAL_1_000000_1_000000);
					Assert.fail();
				} catch (IllegalStateException e) {
				    // table 3 is not even present in the revision 1.000000
				    // (naturally cannot have any leaves in the same revision) 
				}
                Assert.assertNull(vertices);
                
                return null;
            }
        });
    }
    
    
    /**
     * Najít všechny uzly v podstromě.
     */
    @Test
    public void testGetWholeSubtree() {
        cleanGraph();
        createExtendedGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                // (table1, t1c1, t1c2), (table2, t2c1, t2c2)
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                List<Vertex> vertices = GraphOperation.getWholeSubtree(db, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(3 + 3, vertices.size());
                
                // (table1, t1c1, t1c2), (table2, t2c1), (table3, t3c1, t3c2) 
                vertices = GraphOperation.getWholeSubtree(db, REVISION_INTERVAL_2_000000_2_000000);
                Assert.assertEquals(3 + 2 + 3, vertices.size());

                // (t1c1, t1c2)
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                vertices = GraphOperation.getWholeSubtree(table1, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(2, vertices.size());
                
                // (t2c1, t2c2)
                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                vertices = GraphOperation.getWholeSubtree(table2, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(2, vertices.size());
                
                // (t2c1)
                vertices = GraphOperation.getWholeSubtree(table2, REVISION_INTERVAL_2_000000_2_000000);
                Assert.assertEquals(1, vertices.size());

                // leaf node => no children nodes
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                vertices = GraphOperation.getWholeSubtree(t1c1, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(0, vertices.size());
                
                Vertex table3 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table3").iterator().next();
                try {
					vertices = GraphOperation.getWholeSubtree(table3, REVISION_INTERVAL_1_000000_1_000000);
					Assert.fail();
				} catch (IllegalStateException e) {
				    // table 3 is not even present in the revision 1.000000
				    // (naturally cannot have any children nodes in the same revision) 
				}
                
                // (t3c1, t3c2)
                vertices = GraphOperation.getWholeSubtree(table3, REVISION_INTERVAL_2_000000_2_000000);
                Assert.assertEquals(2, vertices.size());
                
                
                return null;
            }
        });
    }
    
    /** Přímí potomci. */
    @Test
    public void testGetDirectChildren() {
        cleanGraph();
        createExtendedGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                
                // resource Teradata
                Vertex root = transaction.getVertices(NodeProperty.SUPER_ROOT.t(), true).iterator().next();
                List<Vertex> vertices = GraphOperation.getDirectChildren(root, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(1, vertices.size());

                // database
                Vertex teradata = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator().next();
                vertices = GraphOperation.getDirectChildren(teradata, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(1, vertices.size());

                // table1, table2
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                vertices = GraphOperation.getDirectChildren(db, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(2, vertices.size());
                
                // table1, table2, table3
                vertices = GraphOperation.getDirectChildren(db, REVISION_INTERVAL_2_000000_2_000000);
                Assert.assertEquals(3, vertices.size());

                // t1c1, t1c2
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                vertices = GraphOperation.getDirectChildren(table1, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(2, vertices.size());
                
                // t2c1, t2c2
                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                vertices = GraphOperation.getDirectChildren(table2, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(2, vertices.size());
                
                // t2c1
                vertices = GraphOperation.getDirectChildren(table2, REVISION_INTERVAL_2_000000_2_000000);
                Assert.assertEquals(1, vertices.size());
                
                // no children in revision 1.000000
                Vertex table3 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table3").iterator().next();
                vertices = GraphOperation.getDirectChildren(table3, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(0, vertices.size());
                
                // t3c1, t3c2
                vertices = GraphOperation.getDirectChildren(table3, REVISION_INTERVAL_2_000000_2_000000);
                Assert.assertEquals(2, vertices.size());
                
                return null;
            }
        });
    }
    
    
    /**
     * Test setting latest committed revision (2.000000) as end revision for the subtree with root node table2. 
     * 
     * BEFORE
     * 
     * revisions:    0.000000 (committed)
     *               1.000000 (committed)
     *               2.000000 (committed)
     * 
     * 
     * super root              
     *     |                     
     * <1.0, 2.9>                
     *  resource ---------------->layer
     *     |
     * <1.0, 2.9>
     *  database
     *     |
     *     +--------------------------------+----------------------------------------+
     *     |                                |                                        |
     * <1.0, 2.9>     <1.0, 2.9>        <1.0, 2.9>      <1.0, 2.9>               <2.0, 2.9>      <2.0, 2.9>
     *   table1---->tableType(TABLE)      table2----->tableType(VIEW)              table3----->tableType(TABLE)
     *     |                                |                                        |
     *     +----------+                     +--------------+               +---------+---------+
     *     |          |                     |              |               |                   |
     * <1.0, 2.9>  <1.0, 2.9>           <1.0, 2.9>     <1.0, 1.9>      <2.0, 2.9>          <2.0, 2.9>
     *    t1c1        t1c2                t2c1--+        t2c2            t3c1                t3c2
     *    | |          A |                A A   |          A              A |                  A
     *    | |<1.0, 2.9>| |                | |   |<1.0, 1.9>|              | |    <2.0, 2.9>    |
     *    | +--direct--+ |   <1.0, 1.9>   | |   +--direct--+              | +------filter------+
     *    |              +-----direct-----+ |   |                         |
     *    |        <1.0, 2.9>               |   |         <2.0, 2.9>      |
     *    +----------filter-----------------+   +-----------direct--------+
     *    
     *    
     *    
     * AFTER
     * 
     * revisions:    0.000000 (committed)
     *               1.000000 (committed)
     *               2.000000 (committed)
     * 
     * 
     * super root              
     *     |                     
     * <1.0, 2.9>                
     *  resource ---------------->layer
     *     |
     * <1.0, 2.9>
     *  database
     *     |
     *     +--------------------------------+----------------------------------------+
     *     |                                |                                        |
     * <1.0, 2.9>     <1.0, 2.9>        <1.0, 2.0>      <1.0, 2.0>               <2.0, 2.9>      <2.0, 2.9>
     *   table1---->tableType(TABLE)      table2----->tableType(VIEW)              table3----->tableType(TABLE)
     *     |                                |                                        |
     *     +----------+                     +--------------+               +---------+---------+
     *     |          |                     |              |               |                   |
     * <1.0, 2.9>  <1.0, 2.9>           <1.0, 2.0>     <1.0, 1.9>      <2.0, 2.9>          <2.0, 2.9>
     *    t1c1        t1c2                t2c1--+        t2c2            t3c1                t3c2
     *    | |          A |                A A   |          A              A |                  A
     *    | |<1.0, 2.9>| |                | |   |<1.0, 1.9>|              | |    <2.0, 2.9>    |
     *    | +--direct--+ |   <1.0, 1.9>   | |   +--direct--+              | +------filter------+
     *    |              +-----direct-----+ |   |                         |
     *    |        <1.0, 2.0>               |   |         <2.0, 2.0>      |
     *    +----------filter-----------------+   +-----------direct--------+
     */
    @Test
    public void testSetSubtreeTransactionEnd() {
        cleanGraph();
        createExtendedGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                GraphOperation.setSubtreeTransactionEnd(t2, REVISION_2_000000, REVISION_2_000000);
                
                Vertex vertex;
                Edge edge;
                
                // check table2 and its attribute
                vertex = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.HAS_PARENT.t()).iterator().next();
                Assert.assertEquals(REVISION_2_000000, edge.getProperty(EdgeProperty.TRAN_END.t()));
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE.t()).iterator().next();
                Assert.assertEquals(REVISION_2_000000, edge.getProperty(EdgeProperty.TRAN_END.t()));
                
                // check t2c1 and its flow edges
                vertex = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.HAS_PARENT.t()).iterator().next();
                Assert.assertEquals(REVISION_2_000000, edge.getProperty(EdgeProperty.TRAN_END.t()));
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.IN, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.FILTER.t()).iterator().next();
                Assert.assertEquals(REVISION_2_000000, edge.getProperty(EdgeProperty.TRAN_END.t()));
                
                Iterable<Edge> flowEdges = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t());
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();
                Vertex t3c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t3c1").iterator().next();
                for (Edge flowEdge : flowEdges) {
                    if (flowEdge.getVertex(Direction.OUT) == t2c2) {
                        Assert.assertEquals(REVISION_1_999999, flowEdge.getProperty(EdgeProperty.TRAN_END.t()));
                    } else if (flowEdge.getVertex(Direction.OUT) == t3c1) {
                        Assert.assertEquals(REVISION_2_000000, flowEdge.getProperty(EdgeProperty.TRAN_END.t()));
                    }
                }
                
                // check t2c2 and its flow edges
                vertex = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.HAS_PARENT.t()).iterator().next();
                Assert.assertEquals(REVISION_1_999999, edge.getProperty(EdgeProperty.TRAN_END.t()));
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.IN, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator().next();
                Assert.assertEquals(REVISION_1_999999, edge.getProperty(EdgeProperty.TRAN_END.t()));
                
                // check some nodes from another subtrees
                // check t3c1
                vertex = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t3c1").iterator().next();
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.HAS_PARENT.t()).iterator().next();
                Assert.assertEquals(REVISION_2_999999, edge.getProperty(EdgeProperty.TRAN_END.t()));
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.FILTER.t()).iterator().next();
                Assert.assertEquals(REVISION_2_999999, edge.getProperty(EdgeProperty.TRAN_END.t()));
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.IN, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator().next();
                Assert.assertEquals(REVISION_2_000000, edge.getProperty(EdgeProperty.TRAN_END.t()));
                
                // check t1c1
                vertex = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.HAS_PARENT.t()).iterator().next();
                Assert.assertEquals(REVISION_2_999999, edge.getProperty(EdgeProperty.TRAN_END.t()));
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator().next();
                Assert.assertEquals(REVISION_2_999999, edge.getProperty(EdgeProperty.TRAN_END.t()));
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.FILTER.t()).iterator().next();
                Assert.assertEquals(REVISION_2_000000, edge.getProperty(EdgeProperty.TRAN_END.t()));
                
                // check table1
                vertex = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.HAS_PARENT.t()).iterator().next();
                Assert.assertEquals(REVISION_2_999999, edge.getProperty(EdgeProperty.TRAN_END.t()));
                edge = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, getRevisionRootHandler().EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE.t()).iterator().next();
                Assert.assertEquals(REVISION_2_999999, edge.getProperty(EdgeProperty.TRAN_END.t()));
                return null;
            }
        });
    }
}
