package eu.profinit.manta.dataflow.repository.connector.titan.filter.group;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

public class HorizontalFilterGroupProviderImplTest {

    @Test
    public void test() {
        HorizontalFilterGroupProviderImpl provider = new HorizontalFilterGroupProviderImpl();
        provider.setConfigurationFile(new File("src/test/resources/horizontalFilterGroups.json"));
        provider.refreshGroups();
        Assert.assertEquals(2, provider.getGroups().size());
    }
}
