package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow;

import java.util.Collections;
import java.util.Set;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.SimpleGraphFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterEdgeGraphFlowFilter;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.GraphFlowFilter;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

public class FilterEdgeGraphFlowAlgorithmTest extends TestTitanDatabaseProvider {
    @Test
    public void testFullGraph() {
        cleanGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex resource = GraphCreation.createResource(transaction, root, "res", "res", "", layer , REVISION_1_000000);
                Vertex c1 = GraphCreation.createNode(transaction, resource, "c1", "Column", REVISION_1_000000);
                Vertex c2 = GraphCreation.createNode(transaction, resource, "c2", "Column", REVISION_1_000000);
                Vertex c3 = GraphCreation.createNode(transaction, resource, "c3", "Column", REVISION_1_000000);
                Vertex c4 = GraphCreation.createNode(transaction, resource, "c4", "Column", REVISION_1_000000);
                Vertex c5 = GraphCreation.createNode(transaction, resource, "c5", "Column", REVISION_1_000000);
                Vertex c6 = GraphCreation.createNode(transaction, resource, "c6", "Column", REVISION_1_000000);
                Vertex c7 = GraphCreation.createNode(transaction, resource, "c7", "Column", REVISION_1_000000);
                Vertex c8 = GraphCreation.createNode(transaction, resource, "c8", "Column", REVISION_1_000000);

                GraphCreation.createEdge(c1, c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c2, c3, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c1, c4, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(c4, c5, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(c4, c6, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c6, c7, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c1, c8, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c8, c7, EdgeLabel.DIRECT, REVISION_1_000000);

                SimpleGraphFlowAlgorithm algorithm = new SimpleGraphFlowAlgorithm();
                algorithm.setFilters(Collections.<GraphFlowFilter> singletonList(new FilterEdgeGraphFlowFilter()));
                Set<Object> outputNodes;
                try {
                    outputNodes = algorithm.findNodesByAlgorithm(Collections.singletonList(c1), null,
                            transaction, Direction.OUT, REVISION_INTERVAL_1_0);
                } catch (GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Assert.assertEquals(6, outputNodes.size());
                Assert.assertTrue(outputNodes.contains(c1.getId()));
                Assert.assertTrue(outputNodes.contains(c2.getId()));
                Assert.assertTrue(outputNodes.contains(c3.getId()));
                Assert.assertTrue(outputNodes.contains(c4.getId()));
                Assert.assertTrue(outputNodes.contains(c7.getId()));
                Assert.assertTrue(outputNodes.contains(c8.getId()));

                return null;
            }
        });
    }
}
