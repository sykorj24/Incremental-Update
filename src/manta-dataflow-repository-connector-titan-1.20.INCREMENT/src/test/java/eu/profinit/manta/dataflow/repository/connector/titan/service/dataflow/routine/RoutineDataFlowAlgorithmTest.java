package eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.routine;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.junit.AfterClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.AttributeNames;
import eu.profinit.manta.dataflow.model.NodeType;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.routine.RoutineDataFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

/**
 * Třída vznikla pro dohledání příčiny problému s exponenciálním nárůstem komplexity průchodu při zjišťování tzv. referenčního view
 * (do kterých všech vrcholů se lze dostat - https://mantatools.atlassian.net/browse/DEV-1713).
 * Postupně se přetavila v automatické testy algoritmu pro průchod grafem a sběr vrcholů {@link #RoutineDataFlowAlgorithm}.
 *
 * @author unknown
 * @author Erik Kratochvíl
 */
public class RoutineDataFlowAlgorithmTest extends TestTitanDatabaseProvider {

    /** SLF4J logger. */
    static final Logger LOGGER = LoggerFactory.getLogger(RoutineDataFlowAlgorithmTest.class);

    /** Prázdné pole parametrů jako indikace, že žádné parametry funkce/procedura nemá. */
    private static final String[] NO_PARAMETERS = new String[0];

    /** Rozlišení funkce, procedury a volání funkce/procedury. */
    enum Type {
        FUNCTION, PROCEDURE, CALL
    };

    /** Reprezentace databázové tabulky. */
    final static class Table {
        /** Vrchol reprezentující definici tabulky v grafu. */
        Vertex vertex;
        /** Seznam vrcholů reprezentujících sloupce tabulk. */
        Vertex[] columns;
        /** První sloupec. */
        Vertex c1;
        /** Druhý sloupec. */
        Vertex c2;
        /** Třetí sloupec. */
        Vertex c3;
        /** Čtvrtý sloupec. */
        Vertex c4;
        /** Pátý sloupec. */
        Vertex c5;

        /** Překlad seznamu vrcholů na zkratky c1, c2, c3, c4, c5. */
        void seal() {
            switch (this.columns.length) {
            case 5:
                this.c5 = this.columns[4];
                // fallthrough
            case 4:
                this.c4 = this.columns[3];
                // fallthrough
            case 3:
                this.c3 = this.columns[2];
                // fallthrough
            case 2:
                this.c2 = this.columns[1];
                // fallthrough
            case 1:
                this.c1 = this.columns[0];
                break;
            }
        }
    }

    /** Reprezentace databázové procedury se vstupními a výstupními parametry. */
    final static class Proc {
        /** Rozlišení definice procedury a volání procedury. */
        Type type;
        /** Pokud je type = volání procedury, pak tento vrchol reprezentuje proceduru, ve které je toto volání uskutečněno. */
        Vertex insideOf;
        /** Vrchol reprezentující definici procedury nebo volání procedury. */
        Vertex vertex;
        /** Seznam všech vstupních parametrů. */
        Vertex[] in;
        /** Seznam všech výstupních parametrů. */
        Vertex[] out;

        /** První vstupní parametr. */
        Vertex a;
        /** Druhý vstupní parametr. */
        Vertex b;
        /** Třetí vstupní parametr. */
        Vertex c;
        /** Čtvrtý vstupní parametr. */
        Vertex d;
        /** Pátý vstupní parametr. */
        Vertex e;

        /** První výstupní parametr. */
        Vertex x;
        /** Druhý výstupní parametr. */
        Vertex y;
        /** Třetí výstupní parametr. */
        Vertex z;
        /** Čtvrtý výstupní parametr. */
        Vertex u;
        /** Pátý výstupní parametr. */
        Vertex v;

        /** Překlad seznamu vstupních a výstupních parametrů na zkratky a, b, c, d, e, x, y, z, u, v. */
        void seal() {
            switch (this.in.length) {
            case 5:
                this.e = this.in[4];
                // fallthrough
            case 4:
                this.d = this.in[3];
                // fallthrough
            case 3:
                this.c = this.in[2];
                // fallthrough
            case 2:
                this.b = this.in[1];
                // fallthrough
            case 1:
                this.a = this.in[0];
                break;
            }
            switch (this.out.length) {
            case 5:
                this.v = this.out[4];
                // fallthrough
            case 4:
                this.u = this.out[3];
                // fallthrough
            case 3:
                this.z = this.out[2];
                // fallthrough
            case 2:
                this.y = this.out[1];
                // fallthrough
            case 1:
                this.x = this.out[0];
                break;
            }
        }
    }

    /**
     * Reprezentace databázové funkce se vstupními parametry a jedním výstupem.
     * Historicky třída Fn vznikla dlouho před potřebou reprezentovat i procedury,
     * takže přestože vlastně Fn je pouze speciální případ Proc, ponecháváme zde.
     */
    final static class Fn {
        /** Rozlišení definice funkce a volání funkce. */
        Type type;
        /** Pokud je type = volání funkce, pak tento vrchol reprezentuje funkci, ve které je toto volání uskutečněno. */
        Vertex insideOf;
        /** Vrchol reprezentující definici funkce nebo volání funkce. */
        Vertex vertex;
        /** Seznam všech vstupních parametrů. */
        Vertex[] parameters;
        /** Výsledek funkce. */
        Vertex result;

        /** První vstupní parametr. */
        Vertex a;
        /** Druhý vstupní parametr. */
        Vertex b;
        /** Třetí vstupní parametr. */
        Vertex c;
        /** Čtvrtý vstupní parametr. */
        Vertex d;
        /** Pátý vstupní parametr. */
        Vertex e;

        /** Překlad seznamu vstupních parametrů na zkratky a, b, c, d, e. */
        void seal() {
            switch (this.parameters.length) {
            case 5:
                this.e = this.parameters[4];
                // fallthrough
            case 4:
                this.d = this.parameters[3];
                // fallthrough
            case 3:
                this.c = this.parameters[2];
                // fallthrough
            case 2:
                this.b = this.parameters[1];
                // fallthrough
            case 1:
                this.a = this.parameters[0];
                break;
            }
        }
    }

    /** Zapouzdřuje výrobu různých typů vrcholů + automatizuje základní prolinkování (hierarchie i datových toků). */
    final class Make {
        /** Transakce TitanDB, která umožní vytvářet nové vrcholy. */
        private final TitanTransaction transaction;
        /** Vrchol reprezentující definice databáze, ve které budou vznikat všechny následující vrcholy. */
        private final Vertex db;

        /** Speciální interní čítač pro vytvářená volání funkcí a procedur. Každé volání dostává vlastní nové callNumber. */
        private int callNumber = 0;

        /** Cache pro překlad ID zpátky na vrcholy kvůli různým ladicím tiskům. */
        private final Map<Object, Vertex> vertexById = new HashMap<>();

        /**
         * Inicializace. Automaticky se vytvoří nový resource <b>Teradata</b> a v něm nová databáze <b>db</b>.
         * @param transaction transakce TitanDB pro práci s grafovou databází
         */
        public Make(final TitanTransaction transaction) {
            this.transaction = transaction;
            final Vertex root = RoutineDataFlowAlgorithmTest.this.getSuperRootHandler().ensureRootExistance(transaction);
            final Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
            final Vertex resource = GraphCreation.createResource(transaction, root, "Teradata", "Teradata", "Description", layer, 1.000000);
            this.db = GraphCreation.createNode(transaction, resource, "db", "Database", 1.000000);
        }

        /**
         * @return unikátní callNumber
         */
        private String generateCallNumber() {
            return Integer.toString(this.callNumber++);
        }

        /**
         * @param id ID vrcholu
         * @return zapamatovaný vertex dle jeho ID, používá se kvůli jednoduchému a rychlému dohledání vrcholu se všemi jeho vlastnostmi
         */
        public Vertex getVertexById(final Object id) {
            return this.vertexById.get(id);
        }

        /**
         * Překlad seznamu ID vrcholů na seznam vrcholů.
         *
         * @param ids seznam ID vrcholů
         * @return seznam vrcholů
         */
        public Collection<Vertex> unwind(final Collection<Object> ids) {
            final Collection<Vertex> list = new ArrayList<>();
            for (final Object id : ids) {
                list.add(this.getVertexById(id));
            }
            return list;
        }

        /**
         * Vytvoří nový vrchol daných vlastností.
         *
         * @param parent hierarchicky nadřazený vrchol
         * @param name jméno nového vrcholu
         * @param type typ nového vrcholu
         * @return nový vrchol
         */
        private Vertex v(final Vertex parent, final String name, final NodeType type) {
            final Vertex v = GraphCreation.createNode(this.transaction, parent, name, type.getId(), 1.000000);
            this.vertexById.put(v.getId(), v);
            return v;
        }

        /**
         * Vytvoří novou proměnnou.
         *
         * @param name jméno proměnné
         * @param parent vrchol reprezentující entitu, ve které je proměnná definována
         * @return vrchol reprezentující novou proměnnou
         */
        public Vertex variable(final String name, final Vertex parent) {
            return this.v(parent, name, NodeType.VARIABLE);
        }

        /**
         * Vytvoří novou globální proměnnou.
         * @param name jméno proměnné
         * @return vrchol reprezentující novou globální proměnnou
         */
        public Vertex variable(final String name) {
            return this.variable(name, this.db);
        }

        /**
         * Vytvoří novou lokální proměnnou uvnitř funkce.
         *
         * @param name jméno proměnné
         * @param function funkce
         * @return vrchol reprezentující novou lokální proměnnou
         */
        public Vertex variable(final String name, final Fn function) {
            return this.variable(name, function.vertex);
        }

        /**
         * Vytvoří novou lokální proměnnou uvnitř procedury.
         *
         * @param name jméno proměnné
         * @param procedure procedura
         * @return vrchol reprezentující novou lokální proměnnou
         */
        public Vertex variable(final String name, final Proc procedure) {
            return this.variable(name, procedure.vertex);
        }

        /**
         * Sada proměnných pro případ, že je potřeba jich vytvořit víc naráz a pracovat s nimi jako s celkem.
         *
         * @param name základ jména, například "a"
         * @param count počet proměnných
         * @param parent vrchol reprezentující objekt, ve kterém jsou proměnné definovány
         * @return sada proměnných pojmenovaných základem jména + suffixem (index proměnné v poli), například "a0", "a1", ...
         */
        public Vertex[] multivariable(final String name, final int count, final Vertex parent) {
            final Vertex[] result = new Vertex[count];
            for (int i = 0; i < count; i++) {
                result[i] = this.variable(String.format("%s%d", name, i), parent);
            }
            return result;
        }

        /**
         * Sada proměnných pro případ, že je potřeba jich vytvořit víc naráz a pracovat s nimi jako s celkem.
         *
         * @param name základ jména, například "a"
         * @param count počet proměnných
         * @param procedure definice procedury, ve které jsou proměnné definovány
         * @return sada proměnných pojmenovaných základem jména + suffixem (index proměnné v poli), například "a0", "a1", ...
         */
        public Vertex[] multivariable(final String name, final int count, final Proc procedure) {
            return this.multivariable(name, count, procedure.vertex);
        }

        /**
         * Vytvoří databázovou funkci.
         *
         * @param name jméno funkce
         * @return vrchol reprezentující databázovou funkci
         */
        private Vertex function(final String name) {
            return this.v(this.db, name, NodeType.FUNCTION);
        }

        /**
         * Vytvoří parametr funkce.
         *
         * @param f vrchol reprezentující databázovou funkci
         * @param name jméno parametru
         * @return vrchol reprezentující parametr funkce
         */
        private Vertex functionParameter(final Vertex f, final String name) {
            return this.v(f, name, NodeType.PARAMETER);
        }

        /**
         * Vytvoří návratovou hodnotu funkce.
         *
         * @param f vrchol reprezentující databázovou funkci
         * @return vrchol reprezentující návratovou hodnotu funkce
         */
        private Vertex functionResult(final Vertex f) {
            return this.v(f, "=", NodeType.ROUTINE_RETURN_VALUE);
        }

        /**
         * Vytvoří volání funkce.
         *
         * @param name jméno funkce
         * @param insideOf vrchol reprezentující funkci, uvnitř které dochází k volání funkce
         * @return vrchol reprezentující volání funkce
         */
        private Vertex functionCall(final String name, final Vertex insideOf) {
            final Vertex parent = insideOf == null ? this.db : insideOf;
            return this.v(parent, name, NodeType.ROUTINE_CALL);
        }

        /**
         * Vytvoří parametr volání funkce.
         *
         * @param call vrchol reprezentující volání funkce
         * @param name jméno parametru volání funkce
         * @return vrchol reprezentující parametr volání funkce
         */
        private Vertex functionCallParameter(final Vertex call, final String name) {
            return this.v(call, name, NodeType.CALL_ACTUAL_PARAMETER);
        }

        /**
         * Vytvoří návratovou hodnotu volání funkce.
         *
         * @param call vrchol reprezentující volání funkce
         * @return vrchol reprezentující návratovou hodnotu volání funkce
         */
        private Vertex functionCallResult(final Vertex call) {
            return this.v(call, "=", NodeType.CALL_RESULT);
        }

        /**
         * Orientovaná hrana reprezentující přímý datový tok.
         *
         * @param a zdrojový vrchol
         * @param b cílový vrchol
         * @param callNumber identifikátor volání na hraně (může být null)
         * @return orientovaná hrana
         */
        public Edge e(final Vertex a, final Vertex b, final String callNumber) {
            if (a == null || b == null) {
                return null;
            }
            final Edge e = GraphCreation.createEdge(a, b, EdgeLabel.DIRECT, 1.000000);
            if (callNumber != null) {
                e.setProperty(AttributeNames.EDGE_CALL_ID, callNumber);
            }
            return e;
        }

        /**
         * Orientovaná hrana reprezentující přímý datový tok.
         *
         * @param a zdrojový vrchol
         * @param b cílový vrchol
         * @return orientovaná hrana
         */
        public Edge e(final Vertex a, final Vertex b) {
            return this.e(a, b, null);
        }

        /**
         * Vytvoří sadu orientovaných hran ze zdrojové vrcholu do všech cílových.
         *
         * @param a zdrojový vrchol
         * @param bs seznam cílových vrcholů
         */
        public void edges(final Vertex a, final Vertex... bs) {
            for (final Vertex b : bs) {
                this.e(a, b);
            }
        }

        /**
         * Vytvoří sadu orientovaných hran mezi zdrojovými a cílovými vrcholy.
         * Vytváří hrany stylem i-tý s i-tým, nikoli každý s každým.
         *
         * @param src seznam zdrojových vrcholů
         * @param tgt seznam cílových vrcholů
         */
        public void edges(final Vertex[] src, final Vertex[] tgt) {
            for (int i = 0; i < Math.min(src.length, tgt.length); i++) {
                this.e(src[i], tgt[i]);
            }
        }

        /**
         * Vytvoří sadu orientovaných hran mezi sloupcemi zdrojové tabulky a cílovými vrcholy.
         * Vytváří hrany stylem i-tý s i-tým, nikoli každý s každým.
         *
         * @param src zdrojová tabulka
         * @param tgt seznam cílových vrcholů
         */
        public void edges(final Table src, final Vertex[] tgt) {
            this.edges(src.columns, tgt);
        }

        /**
         * Vytvoří sadu orientovaných hran mezi zdrojovými vrcholy a sloupcemi cílové tabulky.
         * Vytváří hrany stylem i-tý s i-tým, nikoli každý s každým.
         *
         * @param src seznam zdrojových vrcholů
         * @param tgt cílová tabulka
         */
        public void edges(final Vertex[] src, final Table tgt) {
            this.edges(src, tgt.columns);
        }

        /**
         * Vytvoří sadu orientovaných hran mezi sloupcemi zdrojové a cílové tabulky.
         * Vytváří hrany stylem i-tý s i-tým, nikoli každý s každým.
         *
         * @param src zdrojová tabulka
         * @param tgt cílová tabulka
         */
        public void edges(final Table src, final Table tgt) {
            this.edges(src.columns, tgt.columns);
        }

        /**
         * Vytvoří reprezentaci databázové tabulky a sloupců.
         *
         * @param name jméno tabulky
         * @param columnNames seznam jmen sloupců
         * @return reprezentace databázové tabulky
         */
        public Table table(final String name, final String... columnNames) {
            final Table t = new Table();
            t.vertex = this.v(this.db, name, NodeType.TABLE);
            t.columns = new Vertex[columnNames.length];
            for (int i = 0; i < columnNames.length; i++) {
                t.columns[i] = this.v(t.vertex, columnNames[i], NodeType.COLUMN);
            }
            t.seal();
            return t;
        }

        /**
         * Vytvoří reprezentaci definice funkce.
         *
         * @param name jméno funkce
         * @param parameterNames seznam jmen formálních parametrů funkce
         * @return reprezentace databázové funkce
         */
        public Fn function(final String name, final String... parameterNames) {
            final Fn f = new Fn();
            f.type = Type.FUNCTION;
            f.vertex = this.function(name);
            f.parameters = new Vertex[parameterNames.length];
            f.result = this.functionResult(f.vertex);
            for (int i = 0; i < parameterNames.length; i++) {
                f.parameters[i] = this.functionParameter(f.vertex, parameterNames[i]);
            }
            f.seal();
            return f;
        }

        /**
         * Vytvoří reprezentaci volání funkce.
         *
         * @param f reprezentace funkce, která je volána
         * @param insideOf reprezentace funkce, uvnitř které je volaná funkce volána
         * @param output vrchol reprezentující výstup volání funkce, do kterého bude napojen výstup definice funkce
         * @param inputParameters seznam vrcholů reprezentujících vstupní hodnoty parametrů, tyto vrcholy budou napojeny na vstupní parametry volání funkce
         * @return reprezentace volání funkce
         */
        public Fn call(final Fn f, final Fn insideOf, final Vertex output, final Vertex... inputParameters) {
            final Fn c = new Fn();
            c.type = Type.CALL;
            c.insideOf = insideOf == null ? null : insideOf.type == Type.CALL ? insideOf.insideOf : insideOf.vertex;
            final String newCallNumber = this.generateCallNumber();
            final String callName = nameOf(f.vertex) + newCallNumber + "()";
            c.vertex = this.functionCall(callName, c.insideOf);
            c.parameters = new Vertex[f.parameters.length];
            c.result = this.functionCallResult(c.vertex);
            this.e(c.result, output); // hrana do vystupniho vrcholu obsahujiciho vysledek volani

            this.e(f.result, c.result, newCallNumber);// vcetne hran s ID volani na funkci
            for (int i = 0; i < f.parameters.length; i++) {
                final String parameterName = nameOf(f.parameters[i])/* + newCallNumber */;
                c.parameters[i] = this.functionCallParameter(c.vertex, parameterName);
                this.e(c.parameters[i], f.parameters[i], newCallNumber); // napojeni s ID volani na funkci
                if (i < inputParameters.length) {
                    this.e(inputParameters[i], c.parameters[i]); // hrana ze vstupniho parametru definujiciho jeho hodnotu pri volani funkce
                }
            }
            c.seal();
            return c;
        }

        /**
         * Vytvoří reprezentaci volání funkce.
         *
         * @param function reprezentace funkce, která je volána
         * @param insideOf reprezentace funkce, uvnitř které je volaná funkce volána
         * @return reprezentace volání funkce
         */
        public Fn call(final Fn function, final Fn insideOf) {
            Validate.isTrue(function.type == Type.FUNCTION);
            switch (insideOf.type) {
            case FUNCTION:
                return this.call(function, insideOf, insideOf.result, insideOf.parameters);
            case CALL:
                return this.call(function, insideOf, function.result, insideOf.result);
            default:
                throw new IllegalArgumentException("Neni podporovano.");
            }
        }

        /**
         * Vytvoří reprezentaci definice procedury.
         *
         * @param name jméno procedury
         * @param inputParameterNames seznam jmen formálních vstupních parametrů funkce
         * @param outputParameterNames seznam jmen formálních výstupních parametrů funkce
         * @return reprezentace databázové funkce
         */
        public Proc procedure(final String name, final String[] inputParameterNames, final String[] outputParameterNames) {
            final Proc p = new Proc();
            p.type = Type.PROCEDURE;
            p.vertex = this.function(name);
            p.in = new Vertex[inputParameterNames.length];
            for (int i = 0; i < inputParameterNames.length; i++) {
                p.in[i] = this.functionParameter(p.vertex, inputParameterNames[i]);
            }
            p.out = new Vertex[outputParameterNames.length];
            for (int i = 0; i < outputParameterNames.length; i++) {
                p.out[i] = this.functionParameter(p.vertex, outputParameterNames[i]);
            }
            p.seal();
            return p;
        }

        /**
         * Vytvoří reprezentaci volání procedury.
         *
         * @param p reprezentace procedury, která je volána
         * @param insideOf reprezentace procedury, uvnitř které je volaná funkce volána
         * @param inputParameters seznam vrcholů reprezentujících vstupní hodnoty parametrů, tyto vrcholy budou napojeny na vstupní parametry volání procedury
         * @param outputParameters seznam vrcholů reprezentujících výstupní hodnoty parametrů, do těchto vrcholů budou napojeny na výstupní parametry volání procedury
         * @return reprezentace volání procedury
         */
        public Proc call(final Proc p, final Proc insideOf, final Vertex[] inputParameters, final Vertex[] outputParameters) {
            final Proc c = new Proc();
            c.type = Type.CALL;
            c.insideOf = insideOf == null ? null : insideOf.type == Type.CALL ? insideOf.insideOf : insideOf.vertex;
            final String newCallNumber = this.generateCallNumber();
            final String callName = nameOf(p.vertex) + newCallNumber + "()";
            c.vertex = this.functionCall(callName, c.insideOf);
            c.in = new Vertex[p.in.length];
            for (int i = 0; i < p.in.length; i++) {
                final String parameterName = nameOf(p.in[i]);
                c.in[i] = this.functionCallParameter(c.vertex, parameterName);
                this.e(c.in[i], p.in[i], newCallNumber);
                if (i < inputParameters.length) {
                    this.e(inputParameters[i], c.in[i]);
                }
            }
            c.out = new Vertex[p.out.length];
            for (int i = 0; i < p.out.length; i++) {
                final String parameterName = nameOf(p.out[i]);
                c.out[i] = this.functionCallParameter(c.vertex, parameterName);
                this.e(p.out[i], c.out[i], newCallNumber);
                if (i < outputParameters.length) {
                    this.e(c.out[i], outputParameters[i]);
                }
            }
            c.seal();
            return c;
        }
    }

    /**
     * Případ přímé rekurze s permutací vstupních i výstupních parametrů.
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * var s1, s2, o1, o2, o3;
     * p(s1, s2; o1, o2, o3);
     * procedure p(in a, in b; out x, out y, out z) {
     *   x := a;
     *   p(b, a; y, z, x);
     * }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy.
     */
    @Test
    public void testDirectRecursiveProcedureWithParameterShifting() {
        LOGGER.info("testDirectRecursiveProcedureWithParameterShifting()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex s1 = make.variable("s1"); // start
                final Vertex s2 = make.variable("s2");
                final Vertex o1 = make.variable("o1");
                final Vertex o2 = make.variable("o2");
                final Vertex o3 = make.variable("o3");
                final Proc p = make.procedure("p", array("a", "b"), array("x", "y", "z"));
                make.e(p.a, p.x); // x := a
                final Proc p0 = make.call(p, null, array(s1, s2), array(o1, o2, o3)); // p(s1, s2; o1, o2, o3);
                final Proc p1 = make.call(p, p, array(p.b, p.a), array(p.y, p.z, p.x)); // p(b, a; y, z, x);

                final Set<Object> expectedNodes = collectIds(s1, o1, o2, o3, p.a, p.b, p.x, p.y, p.z, p0.x, p0.y, p0.z, p0.a, p1.a, p1.b, p1.x, p1.y,
                        p1.z);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(s1);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");

                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }
    
//    @AfterClass
//    public void tearDown() {
//        getDatabaseHolder().closeDatabase();
//    }

    /**
     * Případ přímé rekurze s permutací výstupních parametrů.
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * var s1, s2, o1, o2, o3, o4;
     * p(s1, s2; o1, o2, o3, o4);
     * procedure p(in a; out x, out y, out z, out u) {
     *   u := a;
     *   p(a; u, x, y, z);
     * }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy.
     */
    @Test
    public void testDirectRecursiveProcedureWithOutputParameterShifting() {
        LOGGER.info("testDirectRecursiveProcedureWithOutputParameterShifting()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex s1 = make.variable("s1"); // start
                final Vertex o1 = make.variable("o1");
                final Vertex o2 = make.variable("o2");
                final Vertex o3 = make.variable("o3");
                final Vertex o4 = make.variable("o4");
                final Proc p = make.procedure("p", array("a"), array("x", "y", "z", "u"));
                make.e(p.a, p.x); // x := a
                final Proc p0 = make.call(p, null, array(s1), array(o1, o2, o3, o4)); // p(s1; o1, o2, o3, o4);
                final Proc p1 = make.call(p, p, array(p.a), array(p.u, p.x, p.y, p.z)); // p(a; u, x, y, z);

                final Set<Object> expectedNodes = collectIds(s1, o1, o2, o3, o4, p.a, p.x, p.y, p.z, p.u, p0.a, p0.x, p0.y, p0.z, p0.u, p1.a, p1.x, p1.y, p1.z,
                        p1.u);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(s1);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");

                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }



    /**
     * Případ přímé rekurze - základní varianta.
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * end = f(start);
     * function f(a) { return a; return f(a); }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy.
     */
    @Test
    public void testDirectRecursiveFunction() {
        LOGGER.info("testDirectRecursiveFunction()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end = make.variable("end");
                final Fn f = make.function("f", "a");
                make.e(f.a, f.result); // return a;
                final Fn f0 = make.call(f, null, end, start); // end = f(start);
                final Fn f1 = make.call(f, f); // return f(a);

                final Set<Object> expectedNodes = collectIds(start, f0.a, f.a, f.result, f0.result, f1.a, f1.result, end);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");

                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Případ nepřímé rekurze přes dvě funkce.
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * end = f(start, 0, 0);
     * function f(a,b,c) { return c; return g(a,b); }
     * function g(a,b) { return f(a, b, a); }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy dostupné ze <b>start</b>.
     */
    @Test
    public void testIndirectRecursiveFunctionCycleLength2() {
        LOGGER.info("testIndirectRecursiveFunctionCycleLength2()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end = make.variable("end");
                final Fn f = make.function("f", "a", "b", "c");
                make.e(f.c, f.result); // return c;
                final Fn g = make.function("g", "a", "b");
                final Fn f0 = make.call(f, null, end, start, null, null); // end = f(start, -, -);
                final Fn g1 = make.call(g, f); // return g(a,b);
                final Fn f2 = make.call(f, g, g.result, g.a, g.b, g.a); // return f(a, b, a);

                final Set<Object> expectedNodes = collectIds(start, f0.a, f0.result, end, f.a, g1.a, g.a, f2.a, f2.c, f.c, f.result, f2.result, g.result,
                        g1.result);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");
                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Případ nepřímé rekurze přes dvě funkce s cílovým vrcholem, který je nedostupný z výchozího.
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * end = f(start, 0, 0);
     * function f(a,b,c) { return c; return g(a,b); }
     * function g(a,b) { return f(a, b, b); }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy dostupné ze <b>start</b>,
     * v tomto případě se nelze dostat do <b>end</b>.
     */
    @Test
    public void testIndirectRecursiveFunctionCycleLength2WithUnreachableEnd() {
        LOGGER.info("testIndirectRecursiveFunctionCycleLength2WithUnreachableEnd()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end = make.variable("end");
                final Fn f = make.function("f", "a", "b", "c");
                make.e(f.c, f.result); // return c;
                final Fn g = make.function("g", "a", "b");
                final Fn f0 = make.call(f, null, end, start, null, null); // end = f(start, -, -);
                final Fn g1 = make.call(g, f); // return g(a,b);
                final Fn f2 = make.call(f, g, g.result, g.a, g.b, g.b); // return f(a, b, b);

                final Set<Object> expectedNodes = collectIds(start, f0.a, f.a, g1.a, g.a, f2.a);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");
                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Případ nepřímé rekurze přes tři funkce.
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * function f(a) { return a; return g(a); return h(a); }
     * function g(a) { return a; return f(a); return g(a); }
     * function h(a) { return a; return g(a); }
     * end = f(start);
     * </pre>
     *
     * Očekává se, že se najdou všechny uzly.
     */
    @Test
    public void testIndirectRecursiveFunctionCycleLength3() {
        LOGGER.info("testIndirectRecursiveFunctionCycleLength3()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end = make.variable("end"); // 結果
                final Fn f = make.function("f", "a");
                make.e(f.a, f.result);
                final Fn g = make.function("g", "a");
                make.e(g.a, g.result);
                final Fn h = make.function("h", "a");
                make.e(h.a, h.result);
                final Fn g0 = make.call(g, f);
                final Fn g1 = make.call(g, g);
                final Fn g2 = make.call(g, h);
                final Fn h3 = make.call(h, f);
                final Fn f4 = make.call(f, g);
                final Fn f5 = make.call(f, null, end, start); // end = a(start);

                final Set<Object> expectedNodes = collectIds(start, end, f.a, f.result, g.a, g.result, h.a, h.result, g0.a, g0.result, g1.a, g2.a, g2.result,
                        h3.a, h3.result, f4.a, f5.a, f5.result, g1.result, f4.result);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");
                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Případ přímé rekurze s vnitřním vnořením.
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * end = f(start);
     * function f(a) { return a; return f(f(a)); }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy.
     */
    @Test
    public void testDirectRecursiveFunctionWith1NestedCall() {
        LOGGER.info("testDirectRecursiveFunctionWith1NestedCall()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end = make.variable("end");
                final Fn f = make.function("f", "a");
                make.e(f.a, f.result); // return a;
                final Fn f0 = make.call(f, null, end, start); // end = f(start);

                final Fn f1 = make.call(f, f, null, f.a); // inner volani f(a)
                final Fn f2 = make.call(f, f1); // return f(f(a))

                final Set<Object> expectedNodes = collectIds(start, end, f.a, f.result, f0.a, f0.result, f1.a, f1.result, f2.a, f2.result);


                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");
                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Případ přímé rekurze s dvojitým vnitřním vnořením.
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * end = f(start);
     * function f(a) { return a; return f(f(f(a))); }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy.
     */
    @Test
    public void testDirectRecursiveFunctionWith2NestedCalls() {
        LOGGER.info("testDirectRecursiveFunctionWith2NestedCalls()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end = make.variable("end");
                final Fn f = make.function("f", "a");
                final Vertex checkpoint1 = make.variable("checkpoint1", f);
                final Vertex checkpoint2 = make.variable("checkpoint2", f);
                make.e(f.a, f.result); // return a;
                final Fn f0 = make.call(f, null, end, start); // end = f(start);
                final Fn f1 = make.call(f, f, checkpoint1, f.a); // checkpoint1 = f(a)
                final Fn f2 = make.call(f, f, checkpoint2, checkpoint1); // checkpoint2 = f(checkpoint1);
                final Fn f3 = make.call(f, f, f.result, checkpoint2); // return f(checkpoint3);

                final Set<Object> expectedNodes = collectIds(start, end, f.a, f.result, f0.a, f0.result, f1.a, f1.result, f2.a, f2.result, f3.a, f3.result,
                        checkpoint1, checkpoint2);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");
                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Případ přímé rekurze - dvě opakovaná volání.
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * end = f(start);
     * function f(a) { return a; return f(a); return f(a); }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy.Očekává se, že se algoritmus nezacyklí.
     */
    @Test
    public void testDirectRecursiveFunctionWith2RecursiveCalls() {
        LOGGER.info("testDirectRecursiveFunctionWith2RecursiveCalls()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end = make.variable("end");
                final Fn f = make.function("f", "a");
                final Fn f0 = make.call(f, null, end, start); // end = f(start);
                make.e(f.a, f.result); // return a;

                final Fn f1 = make.call(f, f); // return f(a);
                final Fn f2 = make.call(f, f); // return f(a);

                final Set<Object> expectedNodes = collectIds(start, end, f.a, f.result, f0.a, f0.result, f1.a, f1.result, f2.a, f2.result);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");
                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Případ přímé rekurze - dvě opakovaná volání, ale s jiným použitím parametrů.
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * end = f(start, -);
     * function f(a, b) { return b; return f(a,a); return f(b,b); }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy.
     */
    @Test
    public void testDirectRecursiveFunctionWith2RecursiveCallsWithDifferentParameters() {
        LOGGER.info("testRecursiveFlowDoubleMixedParameters()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end = make.variable("end");
                final Fn f = make.function("f", "a", "b");
                final Fn f0 = make.call(f, null, end, start); // end = f(start);

                make.e(f.b, f.result); // return b;
                final Fn f1 = make.call(f, f, f.result, f.a, f.a); // return f(a,a);
                final Fn f2 = make.call(f, f, f.result, f.b, f.b); // return f(b,b);

                final Set<Object> expectedNodes = collectIds(start, end, f.a, f.b, f.result, f0.a, f0.result, f1.a, f1.b, f2.a, f2.b, f1.result, f2.result);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");
                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Případ přímé rekurze - tři opakovaná volání, ale s jiným použitím parametrů.
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * end = f(start, -, -);
     * function f(a, b, c) { return c; return f(0,a,0); return f(0,0,b); return f(c,0,0); }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy.
     */
    @Test
    public void testDirectRecursiveFunctionWith3RecursiveCallsWithParameterShifting() {
        LOGGER.info("testDirectRecursiveFunctionWith3RecursiveCallsWithParameterShifting()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end = make.variable("end");
                final Fn f = make.function("f", "a", "b", "c");
                final Fn f0 = make.call(f, null, end, start); // end = f(start);

                make.e(f.c, f.result); // return c;
                final Fn f1 = make.call(f, f, f.result, null, f.a, null); // return f(0,a,0);
                final Fn f2 = make.call(f, f, f.result, null, null, f.b); // return f(0,0,b);
                final Fn f3 = make.call(f, f, f.result, f.c, null, null); // return f(c,0,0);

                final Set<Object> expectedNodes = collectIds(start, end, f0.a, f0.result, f.a, f.b, f.c, f.result, f1.b, f2.c, f3.a, f1.result, f2.result,
                        f3.result);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");
                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Případ přímé rekurze - čtyři opakovaná rekurzivní volání.
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * end = f(start);
     * function f(a) { return a; return f(a); return f(a); ...; return f(a); }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy.
     */
    @Test
    public void testDirectRecursiveFunctionWith4RecursiveCalls() {
        LOGGER.info("testRecursiveFlowMultipleCalls()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end = make.variable("end");
                final Fn f = make.function("f", "a");
                final Fn f0 = make.call(f, null, end, start); // end = f(start);
                make.e(f.a, f.result); // return a;

                final Fn f1 = make.call(f, f); // return f(a)
                final Fn f2 = make.call(f, f); // return f(a)
                final Fn f3 = make.call(f, f); // return f(a)
                final Fn f4 = make.call(f, f); // return f(a)

                final Set<Object> expectedNodes = collectIds(start, end, f.a, f.result, f0.a, f0.result, f1.a, f1.result, f2.a, f2.result, f3.a, f3.result,
                        f4.a, f4.result);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");
                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Případ přímé rekurze - tři opakovaná rekurzivní volání volaná zvnějšku dvakrát.
     * Prohledávání datových toků: z jediného zdroje (ale do dvou různých cílů).
     *
     * <pre>
     * end1 = f(start);
     * end2 = f(start);
     * function f(a) { return a; return f(a); return f(a); ...; return f(a); }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy.
     */
    @Test
    public void testDirectRecursiveFunctionWith3RecursiveCallsInvoked2x() {
        LOGGER.info("testRecursiveFlowMultipleCallsInvokedTwice()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end1 = make.variable("end1"), end2 = make.variable("end2");
                final Fn f = make.function("f", "a");
                make.e(f.a, f.result); // return a;

                final Fn f0 = make.call(f, null, end1, start); // end1 = f(start);
                final Fn f1 = make.call(f, null, end2, start); // end2 = f(start);

                final Fn f2 = make.call(f, f); // return f(a)
                final Fn f3 = make.call(f, f); // return f(a)
                final Fn f4 = make.call(f, f); // return f(a)

                final Set<Object> expectedNodes = collectIds(start, end1, end2, f.a, f.result, f0.a, f0.result, f1.a, f1.result, f2.a, f2.result, f3.a,
                        f3.result,
                        f4.a, f4.result);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");
                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Případ nepřímé rekurze přes dvě funkce, přičemž funkce se navzájem volají rekurzivně opakovaně (3x a 2x).
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * end = f(start);
     * function f(a) { return g(a); return g(a); return g(a); return a; }
     * function g(a) { return f(a); return f(a); return a; }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy.
     */
    @Test
    public void testIndirectRecursiveFunctionCycleLength2WithMultipleRecursiveCalls() {
        LOGGER.info("testIndirectRecursiveFunctionCycleLength2WithMultipleRecursiveCalls()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end = make.variable("end");
                final Fn f = make.function("f", "a");
                final Fn g = make.function("g", "a");
                final Fn f0 = make.call(f, null, end, start); // end = f(start);
                make.e(f.a, f.result); // return a;
                make.e(g.a, g.result); // return a;

                // Ve fci f 3x volam g.
                final Fn g1 = make.call(g, f);
                final Fn g2 = make.call(g, f);
                final Fn g3 = make.call(g, f);

                // Ve fci g 2x volam f.
                final Fn f4 = make.call(f, g);
                final Fn f5 = make.call(f, g);

                final Set<Object> expectedNodes = collectIds(start, end, f.a, f.result, g.a, g.result, f0.a, f0.result, g1.a, g2.a, g3.a, f4.a, f5.a, g1.result,
                        g2.result, g3.result, f5.result, f4.result);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");
                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Test vypsání chyby při překročení velikosti queue.
     *
     * <pre>
     * end = f(start);
     * function f(a) { return a; return f(a); return f(a); ...; return f(a); }
     * </pre>
     *
     * Očekává se, že se algoritmus skončí s výjimkou.
     */
    @Test
    public void testQueueOverflowError() {
        LOGGER.info("testQueueOverflowError()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end = make.variable("end");
                final Fn f = make.function("f", "a");
                make.call(f, null, end, start); // end = f(start);
                make.e(f.a, f.result); // return a;

                make.call(f, f); // return f(a)
                make.call(f, f);
                make.call(f, f);
                make.call(f, f);
                make.call(f, f);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);
                algorithm.setLimit(10);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    fail("Algorithm should've thrown an exception 'Too many vertices found. Exceeded queue capacity.'");
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });
    }

    /**
     * Případ přímé rekurze - čtyři opakovaná volání, ale s jiným použitím parametrů.
     * Prohledávání datových toků: z jediného zdroje.
     *
     * <pre>
     * end = f(start, -, -, -);
     * function f(a, b, c, d) { return d; return f(0,a,0,0); return f(0,0,b,0); return f(0,0,0,c0); return f(d,0,0,0); }
     * </pre>
     *
     * Očekává se, že se najdou všechny vrcholy.
     */
    @Test
    public void testDirectRecursiveFunctionWith4RecursiveCallsWithParameterShifting() {
        LOGGER.info("testDirectRecursiveFunctionWith4RecursiveCallsWithParameterShifting()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Vertex start = make.variable("start");
                final Vertex end = make.variable("end");
                final Fn f = make.function("f", "a", "b", "c", "d");
                final Fn f0 = make.call(f, null, end, start); // end = f(start);

                make.e(f.d, f.result); // return d;
                final Fn f1 = make.call(f, f, f.result, null, f.a, null, null); // return f(0,a,0,0);
                final Fn f2 = make.call(f, f, f.result, null, null, f.b, null); // return f(0,0,b,0);
                final Fn f3 = make.call(f, f, f.result, null, null, null, f.c); // return f(0,0,0,c);
                final Fn f4 = make.call(f, f, f.result, f.d, null, null, null); // return f(d,0,0,0);

                final Set<Object> expectedNodes = collectIds(start, end, f0.a, f0.result, f.a, f.b, f.c, f.d, f.result, f1.b, f2.c, f3.d, f4.a, f1.result,
                        f2.result, f3.result, f4.result);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNodes = Arrays.asList(start);
                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");
                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Testování průchodu grafem i s nepřímými toky.
     */
    @Test
    public void testIndirectFlow() {
        LOGGER.info("testIndirectFlow()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                final Vertex root = RoutineDataFlowAlgorithmTest.this.getSuperRootHandler().ensureRootExistance(transaction);
                final Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                final Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "Teradata",
                        "Description", layer, 1.000000);
                final Vertex db = GraphCreation.createNode(transaction, teradataRes, "db", "Database", 1.000000);
                final Vertex table1 = GraphCreation.createNode(transaction, db, "table1", "Table", 1.000000);
                final Vertex table2 = GraphCreation.createNode(transaction, db, "table2", "Table", 1.000000);

                final Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", 1.000000);
                final Vertex t1c2 = GraphCreation.createNode(transaction, table1, "t1c2", "Column", 1.000000);
                final Vertex t1c3 = GraphCreation.createNode(transaction, table1, "t1c3", "Column", 1.000000);
                final Vertex t1c4 = GraphCreation.createNode(transaction, table1, "t1c4", "Column", 1.000000);
                final Vertex t1c5 = GraphCreation.createNode(transaction, table1, "t1c5", "Column", 1.000000);
                final Vertex t1c6 = GraphCreation.createNode(transaction, table1, "t1c6", "Column", 1.000000);
                final Vertex t1c7 = GraphCreation.createNode(transaction, table1, "t1c7", "Column", 1.000000);
                final Vertex t1c8 = GraphCreation.createNode(transaction, table1, "t1c8", "Column", 1.000000);
                final Vertex t1c9 = GraphCreation.createNode(transaction, table1, "t1c9", "Column", 1.000000);

                final Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", 1.000000);
                final Vertex t2c2 = GraphCreation.createNode(transaction, table2, "t2c2", "Column", 1.000000);
                final Vertex t2c3 = GraphCreation.createNode(transaction, table2, "t2c3", "Column", 1.000000);

                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.FILTER, 1.000000);
                GraphCreation.createEdge(t1c2, t2c1, EdgeLabel.FILTER, 1.000000);
                GraphCreation.createEdge(t1c3, t2c1, EdgeLabel.FILTER, 1.000000);
                GraphCreation.createEdge(t1c4, t2c1, EdgeLabel.FILTER, 1.000000);
                GraphCreation.createEdge(t1c5, t2c1, EdgeLabel.FILTER, 1.000000);
                GraphCreation.createEdge(t1c6, t2c1, EdgeLabel.FILTER, 1.000000);
                GraphCreation.createEdge(t1c7, t2c1, EdgeLabel.FILTER, 1.000000);
                GraphCreation.createEdge(t1c8, t2c1, EdgeLabel.FILTER, 1.000000);
                GraphCreation.createEdge(t1c9, t2c1, EdgeLabel.DIRECT, 1.000000);
                GraphCreation.createEdge(t2c1, t2c2, EdgeLabel.DIRECT, 1.000000);
                GraphCreation.createEdge(t2c2, t2c3, EdgeLabel.FILTER, 1.000000);

                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(true);

                final Collection<Vertex> startNode = new HashSet<Vertex>();
                startNode.add(t1c1);
                startNode.add(t1c2);
                startNode.add(t1c3);
                startNode.add(t1c4);
                startNode.add(t1c5);
                startNode.add(t1c6);
                startNode.add(t1c7);
                startNode.add(t1c8);
                startNode.add(t1c9);

                Set<Object> outputNodes;
                try {
                    outputNodes = algorithm.findNodesByAlgorithm(startNode, null, transaction, Direction.OUT, new RevisionInterval(1));
                } catch (final GraphFlowAlgorithmException e) {
                    throw new RuntimeException(e);
                }
                Assert.assertEquals(12, outputNodes.size());

                return null;
            }
        });
    }

    /**
     * Test průchodu grafem i s nepřímými toky
     */
    @Test
    public void testAlgorithmMiddleFilterOracle() {
        LOGGER.info("testAlgorithmMiddleFilterOracle()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                final Vertex root = RoutineDataFlowAlgorithmTest.this.getSuperRootHandler().ensureRootExistance(transaction);

                RoutineDataFlowAlgorithmTest.this.getRevisionRootHandler().createMajorRevision(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                
                final Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "Teradata", "Description", layer, REVISION_1_000000);
                final Vertex db = GraphCreation.createNode(transaction, teradataRes, "db", "Database", REVISION_1_000000);

                final Vertex oracleRes = GraphCreation.createResource(transaction, root, "Oracle", "Oracle", "Description", layer, REVISION_1_000000);
                final Vertex schema = GraphCreation.createNode(transaction, oracleRes, "schema", "Database", REVISION_1_000000);

                final Vertex table1 = GraphCreation.createNode(transaction, db, "table1", "Table", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, table1, "TABLE_TYPE", "TABLE", REVISION_1_000000);
                final Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                final Vertex t1c2 = GraphCreation.createNode(transaction, table1, "t1c2", "Column", REVISION_1_000000);

                final Vertex table2 = GraphCreation.createNode(transaction, db, "table2", "Table", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, table2, "TABLE_TYPE", "VIEW", REVISION_1_000000);
                final Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
                final Vertex t2c2 = GraphCreation.createNode(transaction, table2, "t2c2", "Column", REVISION_1_000000);

                final Vertex table3 = GraphCreation.createNode(transaction, schema, "table3", "Table", REVISION_1_000000);
                final Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);
                final Vertex t3c2 = GraphCreation.createNode(transaction, table3, "t3c2", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t1c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c2, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);

                GraphCreation.createEdge(t3c1, t3c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c2, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, t2c2, EdgeLabel.FILTER, REVISION_1_000000);

                GraphCreation.createEdge(t2c1, t2c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c2, t1c1, EdgeLabel.DIRECT, REVISION_1_000000);

                RoutineDataFlowAlgorithmTest.this.getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);

                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(true);
                algorithm.setVerboseMode(true);

                final Collection<Vertex> startNode = new HashSet<Vertex>();
                startNode.add(t1c1);

                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    algorithm.findNodesByAlgorithm(startNode, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Test jednoduchého nerekurzivního volání procedur.
     * Prohledávání datových toků: ze dvou zdrojů současně.
     *
     * <pre>
     * table s1 (c1, c2, c3, c4);
     * table s2 (c1, c2, c3, c4);
     * table t1 (c1, c2, c3, c4);
     * table t2 (c1, c2, c3, c4);
     *
     * procedure p(in a; out x) { x := a }
     * procedure q(in a, in b; out x, out y) { p(a; x); p(b; y); }
     * procedure r(in a, in b, in c, in d; out x, out y, out z, out u) { q(a, b; x, y); q(c, d; z, u); }
     *
     * procedure go() {
     *   var a1, a2, a3, a4 := s1.c1, s1.c2, s1.c3, s1.c4;
     *   var b1, b2, b3, b4;
     *   r(a1, a2, a3, a4; b1, b2, b3, b4);
     *   t1.c1, t1.c2, t1.c3, t1.c4 := b1, b2, b3, b4;
     *
     *   var c1, c2, c3, c4 := s2.c1, s2.c2, s2.c3, s2.c4;
     *   var d1, d2, d3, d4;
     *   r(c1, c2, c3, c4; d1, d2, d3, d4);
     *   t2.c1, t2.c2, t2.c3, t2.c4 := d1, d2, d3, d4;
     * }
     *
     * </pre>
     *
     * Očekává se, že když se vyjde ze s1.c1 a s2.c1, najdou se jako koncové vrcholy t1.c1 a t2.c1.
     */
    @Test
    public void testOrdinaryProceduresWith2DifferentSources() {
        LOGGER.info("testOrdinaryProceduresWithTwoDifferentSources()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Table s1 = make.table("s1", "c1", "c2", "c3", "c4");
                final Table s2 = make.table("s2", "c1", "c2", "c3", "c4");
                final Table t1 = make.table("t1", "c1", "c2", "c3", "c4");
                final Table t2 = make.table("t2", "c1", "c2", "c3", "c4");

                final Proc p = make.procedure("p", array("a"), array("x"));
                make.e(p.a, p.x);

                final Proc q = make.procedure("q", array("a", "b"), array("x", "y"));
                final Proc p0 = make.call(p, q, array(q.a), array(q.x));
                /*final Proc p1 =*/ make.call(p, q, array(q.b), array(q.y));

                final Proc r = make.procedure("r", array("a", "b", "c", "d"), array("x", "y", "z", "u"));
                final Proc q2 = make.call(q, r, array(r.a, r.b), array(r.x, r.y));
                /*final Proc q3 =*/ make.call(q, r, array(r.c, r.d), array(r.z, r.u));

                final Proc go = make.procedure("go", NO_PARAMETERS, NO_PARAMETERS);

                final Vertex[] a = make.multivariable("a", 4, go);
                final Vertex[] b = make.multivariable("b", 4, go);
                final Vertex[] c = make.multivariable("c", 4, go);
                final Vertex[] d = make.multivariable("d", 4, go);

                // Datove toky uvnitr go().
                // var a1, a2, a3, a4 := s1.c1, s1.c2, s1.c3, s1.c4;
                make.edges(s1, a);
                // r(a1, a2, a3, a4; b1, b2, b3, b4);
                final Proc r4 = make.call(r, go, a, b);
                // t1.c1, t1.c2, t1.c3, t1.c4 := b1, b2, b3, b4;
                make.edges(b, t1);

                // var c1, c2, c3, c4 := s2.c1, s2.c2, s2.c3, s2.c4;
                make.edges(s2, c);
                // r(c1, c2, c3, c4; d1, d2, d3, d4);
                final Proc r5 = make.call(r, go, c, d);
                // t2.c1, t2.c2, t2.c3, t2.c4 := d1, d2, d3, d4;
                make.edges(d, t2);


                final Set<Object> expectedNodes = collectIds(s1.c1, s2.c1, t1.c1, t2.c1, a[0], b[0], c[0], d[0], p.a, p.x, q.a, q.x, r.a, r.x, p0.a, p0.x,
                        q2.a, q2.x,
                        r4.a, r4.x, r5.a, r5.x);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                // Dva výchozí vrcholy, které se sbíhají na stejných volání funkce, ale vystupují do různých vrcholů.
                final Collection<Vertex> startNodes = Arrays.asList(s1.c1, s2.c1);


                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");

                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Test jednoduchého volání procedur s rekurzí.
     * Prohledávání datových toků: ze čtyř zdrojů současně.
     *
     * <pre>
     * table s (c1, c2, c3, c4);
     * table t (c1, c2, c3, c4);
     *
     * procedure p(in a; out x) { x := a; p(a; x); }
     *
     * p(s.c1; t.c1);
     * p(s.c2; t.c2);
     * p(s.c3; t.c3);
     * p(s.c4; t.c4);
     *
     * </pre>
     *
     * Očekává se, že když se vyjde ze s.c1 + s.c2 + s.c3 + s.c4, najdou se jako koncové vrcholy t.c1 + t.c2 + t.c3 + t.c4.
     *
     */
    @Test
    public void testOrdinaryProceduresWith4DifferentSourcesWith1RecursiveProcedure() {
        LOGGER.info("testOrdinaryProceduresWith4DifferentSourcesWith1RecursiveProcedure()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Table s = make.table("s1", "c1", "c2", "c3", "c4");
                final Table t = make.table("t1", "c1", "c2", "c3", "c4");

                final Proc p = make.procedure("p", array("a"), array("x"));
                make.e(p.a, p.x);
                final Proc p0 = make.call(p, p, array(p.a), array(p.x)); // interni rekurzivni volani

                final Proc p1 = make.call(p, p, array(s.c1), array(t.c1)); // prvni vnejsi volani
                final Proc p2 = make.call(p, p, array(s.c2), array(t.c2)); // druhe vnejsi volani
                final Proc p3 = make.call(p, p, array(s.c3), array(t.c3)); // treti vnejsi volani
                final Proc p4 = make.call(p, p, array(s.c4), array(t.c4)); // ctvrte vnejsi volani

                // Ocekavane vystupy.
                final Set<Object> expectedNodes = collectIds(s.c1, s.c2, s.c3, s.c4, t.c1, t.c2, t.c3, t.c4, p.a, p.x, p0.a, p0.x, p1.a, p1.x, p2.a, p2.x, p3.a,
                        p3.x, p4.a, p4.x);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                // Dva výchozí vrcholy, které se sbíhají na stejných volání funkce, ale vystupují do různých vrcholů.
                final Collection<Vertex> startNodes = Arrays.asList(s.c1, s.c2, s.c3, s.c4);

                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");

                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }

    /**
     * Test jednoduchého volání procedur s rekurzí.
     * Prohledávání datových toků: ze dvou zdrojů současně.
     *
     * <pre>
     * table s1 (c1, c2, c3, c4);
     * table s2 (c1, c2, c3, c4);
     * table t1 (c1, c2, c3, c4);
     * table t2 (c1, c2, c3, c4);
     *
     * procedure p(in a; out x) { x := a; p(a; x); }
     * procedure q(in a, in b; out x, out y) { p(a; x); p(b; y); }
     * procedure r(in a, in b, in c, in d; out x, out y, out z, out u) { q(a, b; x, y); q(c, d; z, u); }
     *
     * procedure go() {
     *   var a1, a2, a3, a4 := s1.c1, s1.c2, s1.c3, s1.c4;
     *   var b1, b2, b3, b4;
     *   r(a1, a2, a3, a4; b1, b2, b3, b4);
     *   t1.c1, t1.c2, t1.c3, t1.c4 := b1, b2, b3, b4;
     *
     *   var c1, c2, c3, c4 := s2.c1, s2.c2, s2.c3, s2.c4;
     *   var d1, d2, d3, d4;
     *   r(c1, c2, c3, c4; d1, d2, d3, d4);
     *   t2.c1, t2.c2, t2.c3, t2.c4 := d1, d2, d3, d4;
     * }
     *
     * </pre>
     *
     * Očekává se, že když se vyjde ze s1.c1 a s2.c1, najdou se jako koncové vrcholy t1.c1 a t2.c1.
     */
    @Test
    public void testOrdinaryProceduresWith2DifferentSourcesWith1RecursiveProcedure() {
        LOGGER.info("testOrdinaryProceduresWith2DifferentSourcesWith1RecursiveProcedure()");
        this.cleanGraph();
        this.getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(final TitanTransaction transaction) {
                // Graf.
                LOGGER.info("Creating call graph.");
                final Make make = new Make(transaction);
                final Table s1 = make.table("s1", "c1", "c2", "c3", "c4");
                final Table s2 = make.table("s2", "c1", "c2", "c3", "c4");
                final Table t1 = make.table("t1", "c1", "c2", "c3", "c4");
                final Table t2 = make.table("t2", "c1", "c2", "c3", "c4");

                final Proc p = make.procedure("p", array("a"), array("x"));
                make.e(p.a, p.x);
                final Proc p0 = make.call(p, p, array(p.a), array(p.x)); // slivaci p.a/p.x

                final Proc q = make.procedure("q", array("a", "b"), array("x", "y"));
                final Proc p1 = make.call(p, q, array(q.a), array(q.x));
                final Proc p2 = make.call(p, q, array(q.b), array(q.y));

                final Proc r = make.procedure("r", array("a", "b", "c", "d"), array("x", "y", "z", "u"));
                final Proc q3 = make.call(q, r, array(r.a, r.b), array(r.x, r.y));
                /*final Proc q4 =*/ make.call(q, r, array(r.c, r.d), array(r.z, r.u));

                final Proc go = make.procedure("go", NO_PARAMETERS, NO_PARAMETERS);

                final Vertex[] a = make.multivariable("a", 4, go);
                final Vertex[] b = make.multivariable("b", 4, go);
                final Vertex[] c = make.multivariable("c", 4, go);
                final Vertex[] d = make.multivariable("d", 4, go);

                // Datove toky uvnitr go().
                // var a1, a2, a3, a4 := s1.c1, s1.c2, s1.c3, s1.c4;
                make.edges(s1, a);
                // r(a1, a2, a3, a4; b1, b2, b3, b4);
                final Proc r5 = make.call(r, go, a, b);
                // t1.c1, t1.c2, t1.c3, t1.c4 := b1, b2, b3, b4;
                make.edges(b, t1);

                // var c1, c2, c3, c4 := s2.c1, s2.c2, s2.c3, s2.c4;
                make.edges(s2, c);
                // r(c1, c2, c3, c4; d1, d2, d3, d4);
                final Proc r6 = make.call(r, go, c, d);
                // t2.c1, t2.c2, t2.c3, t2.c4 := d1, d2, d3, d4;
                make.edges(d, t2);

                final Set<Object> expectedNodes = collectIds(s1.c1, s1.c2, s2.c1, s2.c2, t1.c1, t1.c2, t2.c1, t2.c2, a[0], a[1], b[0], b[1], c[0], c[1], d[0],
                        d[1], p.a, p.x, p0.a, p0.x, p1.a, p1.x, p2.a, p2.x, q.a, q.b, q.x, q.y, q3.a, q3.b, q3.x, q3.y, r.a, r.b, r.x, r.y, r5.a, r5.b, r5.x,
                        r5.y, r6.a, r6.b, r6.x, r6.y);

                LOGGER.info("Creating THE ALGORITHM.");
                final RoutineDataFlowAlgorithm algorithm = new RoutineDataFlowAlgorithm();
                algorithm.setFilterEdgesEnabled(false);
                algorithm.setVerboseMode(true);

                // Dva výchozí vrcholy, které se sbíhají na stejných volání funkce, ale vystupují do různých vrcholů.
                final Collection<Vertex> startNodes = Arrays.asList(s1.c1, s1.c2, s2.c1, s2.c2);

                try {
                    LOGGER.info("THE ALGORITHM STARTS");
                    final Set<Object> foundNodes = algorithm.findNodesByAlgorithm(startNodes, null, transaction, Direction.OUT, new RevisionInterval(1));
                    LOGGER.info("THE ALGORITHM ENDS");

                    checkNodes(make, foundNodes, expectedNodes);
                } catch (final GraphFlowAlgorithmException e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }

                return null;
            }
        });
    }


    /**
     *
     * @param vertex vrchol
     * @return kvalifikované jméno vrcholu (pouze do nadřazené úrovně), obsahuje také ID vrcholu v grafové databázi pro jednoznačnou identifikaci
     */
    private static String qualifiedNameOf(final Vertex vertex) {
        final String myName = vertex.getProperty(NodeProperty.NODE_NAME.t());
        final String parentName = GraphOperation.getParent(vertex).getProperty(NodeProperty.NODE_NAME.t());
        return String.format("%s.%s, v[%s]", parentName, myName, vertex.getId());
    }

    /**
     *
     * @param vertex vrchol
     * @return jméno vrcholu
     */
    private static String nameOf(final Vertex vertex) {
        return vertex.getProperty(NodeProperty.NODE_NAME.t());
    }

    /**
     * Protože v Javě 7 nejde prostě elegantně nijak napsat <code>vertices.map(vertex=>vertex.getId())</code>,
     * musíme na to mít speciální funkci.
     *
     * @param vertices seznam vrcholů
     * @return množina ID všech vrcholů v seznamu
     */
    private static Set<Object> collectIds(final Vertex... vertices) {
        final Set<Object> set = new HashSet<>();
        for (final Vertex v : vertices) {
            set.add(v.getId());
        }
        return set;
    }

    /**
     * Porovnání nalezených a očekávaných vrcholů a tisk reportu.
     * Porovnává se jak to, jestli byl nalezen vrchol, který měl být nalezen,
     * tak to, jestli nebyl nalezen vrchol, který neměl být nalezen.
     * Jinými slovy, seznamy se musí úplně přesně shodovat.
     *
     * @param make třída, která vytvořila všechny vrcholy (pamatuje si totiž o nich detaily)
     * @param foundNodes seznam ID nalezených vrcholů
     * @param expectedNodes seznam ID očekávaných vrcholů
     */
    private static void checkNodes(final Make make, final Set<Object> foundNodes, final Set<Object> expectedNodes) {
        final Set<Object> mergedNodes = new HashSet<>();
        mergedNodes.addAll(foundNodes);
        mergedNodes.addAll(expectedNodes);

        LOGGER.info("Nodes found: {}, expected: {}", foundNodes.size(), expectedNodes.size());
        LOGGER.info("--------------------  F  E");
        int expectedNotFound = 0;
        int foundNotExpected = 0;

        final List<String> sortedLines = new ArrayList<>();

        for (final Vertex node : make.unwind(mergedNodes)) {
            final String name = qualifiedNameOf(node);
            final boolean isFound = foundNodes.contains(node.getId());
            final boolean isExpected = expectedNodes.contains(node.getId());
            final String line = String.format("%-20s %3s%3s", name, isFound ? " X " : "", isExpected ? " X " : "");
            sortedLines.add(line);
            if (isFound && !isExpected) {
                foundNotExpected++;
            }
            if (isExpected && !isFound) {
                expectedNotFound++;
            }
        }
        Collections.sort(sortedLines);
        for (final String line : sortedLines) {
            LOGGER.info(line);
        }

        if (expectedNotFound > 0 || foundNotExpected > 0) {
            fail("Missing vertices: " + expectedNotFound + ". Unexpected vertices: " + foundNotExpected + ".");
        }
    }

    /**
     * Protože v Java nejde prostě napsat <code>p(a, [b, c], [d, e])</code>, tak vznikla tato funkce,
     * která dovolí udělat alespoň <code>p(a, array(b, c), array(d, e));</code>
     * @param objs vstupní seznam objektů
     * @return vstupní seznam objektů jako pole
     */
    @SafeVarargs
    private static <T> T[] array(final T... objs) {
        return objs;
    }

    /*
    @Test
    public void reconstructStack() throws IOException {
        final List<String> lines = FileUtils.readLines(new File("c:/Users/admin/Documents/Manta/Quick_Analysis/R14/stack2.log"));
        final Stack stack = new Stack();
        final Multiset<String> functions = HashMultiset.create();
        final Multiset<String> calls = HashMultiset.create();
        final int check = reconstructStack(lines, 0, 1, stack, functions, calls);
        Assert.assertTrue(check == lines.size());
        System.out.println(stack.getCompleteStackAsString());
        printFrequency(functions, "%-10s %d");
        printFrequency(calls, "%-40s %d");
        System.out.println("-  -  -  -  -  -  -  -  -  -  -  -  -  -  -");
        findRecursiveCalls(stack);
        System.out.println(maxDepth(stack));
    }
    
    private static void findRecursiveCalls(final Stack stack) {
        if (stack != stack.findFirstCall()) {
            System.out.println(stack.getCalledNumberPath());
        }
        for (final Stack child : stack.getChildren()) {
            findRecursiveCalls(child);
        }
    }
    
    private static int maxDepth(final Stack stack) {
        if (stack.getChildren().isEmpty()) {
            return 0;
        }
        int max = 0;
        for (final Stack child : stack.getChildren()) {
            max = Math.max(max, maxDepth(child));
        }
        return max + 1;
    }
    
    private static <T> void printFrequency(final Multiset<T> elements, final String pattern) {
        final LinkedList<Multiset.Entry<T>> orderedElements = new LinkedList<>(elements.entrySet());
        final Comparator<Multiset.Entry<T>> byCountDescending = new Comparator<Multiset.Entry<T>>() {
            @Override
            public int compare(final Multiset.Entry<T> o1, final Multiset.Entry<T> o2) {
                return Integer.compare(o2.getCount(), o1.getCount());
            }
        };
        Collections.sort(orderedElements, byCountDescending);
        int total = 0, distinct = 0;
        for(final Multiset.Entry<T> element : orderedElements) {
            total += element.getCount();
            distinct++;
            System.out.println(String.format(pattern, element.getElement(), element.getCount()));
        }
        System.out.println("Total count: " + total + ". Distinct count: " + distinct + ".");
    }
    
    private static int reconstructStack(final List<String> lines, final int fromRow, final int level, final Stack parent, final Multiset<String> functions,
            final Multiset<String> calls) {
        final Pattern pattern = Pattern.compile("^(\\s+)([^ ]+) \\[(\\w+)\\].*$");
        int row = fromRow;
        Stack last = null;
        while (row < lines.size()) {
            final String line = lines.get(row).replace('?', ' ');
    
            final Matcher matcher = pattern.matcher(line);
            if (!matcher.matches()) {
                System.err.println(String.format("<KO> %s", line));
                continue;
            }
            final String whitespace = matcher.group(1);
            final String callNumber = matcher.group(2);
            final String calledFunction = matcher.group(3);
    
            functions.add(calledFunction);
            calls.add(callNumber);
    
            final int lineLevel = whitespace.length() / 2;
            if (lineLevel == level) {
                // stejna uroven
                last = parent.push(callNumber, calledFunction);
                row++;
            } else if (lineLevel > level) {
                // zanoreni
                row = reconstructStack(lines, row, lineLevel, last, functions, calls);
            } else {
                // vynoreni
                return row;
            }
        }
        return row;
    }
     */
}
