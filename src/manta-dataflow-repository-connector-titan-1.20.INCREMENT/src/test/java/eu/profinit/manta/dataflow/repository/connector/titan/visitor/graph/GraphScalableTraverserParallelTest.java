package eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.AccessLevel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorPrefix;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverserParallel;

public class GraphScalableTraverserParallelTest extends TestTitanDatabaseProvider {
    @Test
    public void testPrefixAll() {
        cleanGraph();
        createBasicGraph(false);

        TestGraphVisitor visitor = runTraverser(TraverserProcessorFactoryGeneric.DEFAULT_VISITED_PARTS, 5,
                TraverserProcessorPrefix.class);

        Assert.assertEquals(1, visitor.getLayerCount());
        Assert.assertEquals(1, visitor.getResourceCount());
        Assert.assertEquals(7, visitor.getNodeCount());
        Assert.assertEquals(2, visitor.getAttributeCount());
        Assert.assertEquals(4, visitor.getEdgeCount());
    }

    private <T extends TraverserProcessor<?>> TestGraphVisitor runTraverser(List<VisitedPart> visitedParts,
            int objectsPerTran, Class<T> processorClass) {
        TraverserProcessorFactory processorFactory = new TraverserProcessorFactoryGeneric<>(processorClass, "mr_basic",
                objectsPerTran, new SearchOrderDfs(), visitedParts);

        GraphScalableTraverserParallel traverser = new GraphScalableTraverserParallel(getDatabaseHolder(),
                processorFactory, 4);

        TestGraphVisitor visitor = new TestGraphVisitor();
        Long rootId = getSuperRootHandler().getRoot(getDatabaseHolder());
        traverser.traverse(visitor, rootId, AccessLevel.READ, REVISION_INTERVAL_1_0);
        return visitor;
    }
}
