package eu.profinit.manta.dataflow.repository.connector.titan.connection;

import java.util.Random;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

/**
 * Test holderu pro transakce.
 * @author tfechtner
 *
 */
public class SimpleDatabaseHolderTest extends TestTitanDatabaseProvider {
    private static final int THREAD_COUNT = 500;
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleDatabaseHolderTest.class);

    /**
     * Ověření transakce jen s čtecím přístupem. 
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testReadTransaction() {
        createBasicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction onlyRead) {
                Vertex table1 = onlyRead.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Assert.assertEquals("table1", table1.getProperty(NodeProperty.NODE_NAME.t()));

                GraphCreation.createNode(onlyRead, table1, "t1c3", "Column", 1.000000);
                return null;
            }
        });
    }

    /**
     * Ověření transakce s R/W přístupem. 
     */
    @Test
    public void testWriteTransaction() {
        createBasicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction readWrite) {
                Vertex table1 = readWrite.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Assert.assertEquals("table1", table1.getProperty(NodeProperty.NODE_NAME.t()));

                Assert.assertNotNull(GraphCreation.createNode(readWrite, table1, "t1c3", "Column", 1.000000));
                return null;
            }
        });
    }

    /**
     * Test multi-thread přístupu k db.
     * @throws InterruptedException přerušení vlákna
     */
    @Test
    @Ignore
    public void testMultipleWriteTransaction() throws InterruptedException {
        createBasicGraph();

        Long tableId = getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Long>() {
            @Override
            public Long callMe(TitanTransaction readTrans) {
                return (Long) readTrans.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next().getId();
            }
        });

        Thread[] threads = new Thread[THREAD_COUNT];
        for (int i = 0; i < THREAD_COUNT; i++) {
            threads[i] = new Thread(new TestThread(tableId, getDatabaseHolder()));
            threads[i].start();
        }

        for (int i = 0; i < THREAD_COUNT; i++) {
            threads[i].join();
        }

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Long>() {
            @Override
            public Long callMe(TitanTransaction readTrans) {
                Assert.assertEquals(getBasicGraphVertexCount() + 1 + THREAD_COUNT, getVertexCount(readTrans));
                return null;
            }
        });
    }

    /**
     * Rannable pro vytvoření jednoho uzlu per vlákno, plus jednoho jedinečnho uzlu.
     * @author tfechtner
     *
     */
    private static final class TestThread implements Runnable {

        private static final int BASIC_SLEEP = 10;

        private static final int SLEEP_TIME = 100;

        private final Long tableId;

        private final DatabaseHolder databaseHolder;

        private final Random rand = new Random();

        public TestThread(Long tableId, DatabaseHolder databaseHolder) {
            super();
            this.tableId = tableId;
            this.databaseHolder = databaseHolder;
        }

        @Override
        public void run() {
            LOGGER.info("Thread " + Thread.currentThread().getId() + " started.");
            try {
                Thread.sleep(BASIC_SLEEP + rand.nextInt(SLEEP_TIME));
            } catch (InterruptedException e) {
                LOGGER.info("Thread was interrupted.", e);
            }
            databaseHolder.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

                @Override
                public Object callMe(TitanTransaction trans) {
                    if (!trans.getVertices(NodeProperty.NODE_NAME.t(), "cc").iterator().hasNext()) {
                        try {
                            Thread.sleep(BASIC_SLEEP + rand.nextInt(SLEEP_TIME));
                            LOGGER.info("Thread " + Thread.currentThread().getId() + " created node.");
                            GraphCreation.createNode(trans, trans.getVertex(tableId), "cc", "Column", 1.000000);
                            Thread.sleep(BASIC_SLEEP + rand.nextInt(SLEEP_TIME));
                        } catch (InterruptedException e) {
                            LOGGER.info("Thread was interrupted.", e);
                        }
                    }
                    GraphCreation.createNode(trans, trans.getVertex(tableId), "cc" + Thread.currentThread().getId(),
                            "Column", 1.000000);
                    LOGGER.info("Thread " + Thread.currentThread().getId() + " commit.");
                    return null;
                }
            });
        }
    }
}
