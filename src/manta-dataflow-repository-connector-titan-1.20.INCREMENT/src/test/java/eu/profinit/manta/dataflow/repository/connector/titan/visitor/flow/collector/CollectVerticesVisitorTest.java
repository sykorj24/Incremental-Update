package eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.collector;

import java.util.Collections;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.EdgeTypeTraversing;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowTraverser;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowTraverserDfs;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

/**
 * Test sbírání id.
 * @author tfechtner
 *
 */
public class CollectVerticesVisitorTest extends TestTitanDatabaseProvider {
    /**
     * Test sbírání id. 
     */
    @Test
    public void testCollection() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();

                FlowTraverser traverser = new FlowTraverserDfs();

                CollectVerticesVisitor visitor = new CollectVerticesVisitor(CollectingAllFilter.INSTANCE);
                traverser.traverse(visitor, transaction, Collections.singleton(t2c1.getId()), EdgeTypeTraversing.BOTH,
                        Direction.IN, REVISION_INTERVAL_1_0);
                Assert.assertEquals(2 + 1, visitor.getCollectedVertexIds().size());
                Assert.assertTrue(visitor.getCollectedVertexIds().contains(t1c2.getId()));
                Assert.assertTrue(visitor.getCollectedVertexIds().contains(t2c1.getId()));
                Assert.assertTrue(visitor.getCollectedVertexIds().contains(t1c1.getId()));

                visitor = new CollectVerticesVisitor(new CollectingFilter() {
                    @Override
                    public boolean shouldCollect(Vertex node) {
                        return false;
                    }
                });
                traverser.traverse(visitor, transaction, Collections.singleton(t2c1.getId()), EdgeTypeTraversing.BOTH,
                        Direction.IN, REVISION_INTERVAL_1_0);
                Assert.assertEquals(0, visitor.getCollectedVertexIds().size());
                return null;
            }
        });
    }

}
