package eu.profinit.manta.dataflow.repository.dump.server.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.AccessLevel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorPrefix;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverser;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverserSerial;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.dump.server.helper.processor.DumpExportProcessorImpl;
import eu.profinit.manta.dataflow.repository.dump.server.helper.processor.DumpImportProcessorImpl;
import eu.profinit.manta.dataflow.repository.dump.server.helper.processor.SourceCodeDumpHelper;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;
import eu.profinit.manta.dataflow.repository.merger.server.helper.sourcecode.SourceCodeHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.sourcecode.TestHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.usage.ContentCollectorVisitor;
import eu.profinit.manta.dataflow.repository.merger.server.helper.usage.ContentCollectorVisitor.AttributeType;
import eu.profinit.manta.dataflow.repository.utils.NodeTypeDef;

/**
 * Test helperu pro export dumpu.
 * @author tfechtner
 *
 */
public class DumpHelperTest extends TestTitanDatabaseProvider {
    // Pocty pridanych vrcholu oproti zakladnimu grafu
    private static final int SOURCE_CODES_ADDED = 1;
    private static final int ATTRIBUTES_ADDED = 1;
    private static final int RESOURCES_ADDED = 1;
    private static final int LAYERS_ADDED = 1;

    /**
     * Otestuje export dumpu.
     * @throws IOException 
     */
    @Test
    public void testExportDump() throws IOException {
        createGraph();

        DumpHelper dumpHelper = new DumpHelper();

        final DumpExportProcessorImpl dumpExportProcessor = new DumpExportProcessorImpl();
        dumpExportProcessor.setDatabaseHolder(getDatabaseHolder());
        dumpExportProcessor.setRevisionRootHandler(getRevisionRootHandler());
        dumpExportProcessor.setSuperRootHandler(getSuperRootHandler());

        dumpHelper.setDumpExportProcessor(dumpExportProcessor);

        SourceCodeDumpHelper sourceCodeDumpHelper = new SourceCodeDumpHelper();
        sourceCodeDumpHelper.setSourceRootHandler(getSourceRootHandler());
        sourceCodeDumpHelper.setBackUpDir(new File("target/backup"));
        dumpHelper.setSourceCodeDumpHelper(sourceCodeDumpHelper);

        File file = new File("target/dump.zip");

        if (file.exists()) {
            FileUtils.forceDelete(file);
        }

        OutputStream outputStream = new FileOutputStream(file);
        dumpHelper.exportDump(outputStream);
        Assert.assertTrue(file.exists());
        IOUtils.closeQuietly(outputStream);
    }

    /**
     * Test import of a dump exported by the current version.
     * 
     * @throws IOException 
     */
    @Test
    public void testImportDump() throws IOException {
        String fileId = createGraph();
        // vytvorit atribut na hrane
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                GraphCreation.createNodeAttribute(transaction, t1c1, "ORDER", 8, REVISION_1_000000);

                Edge edge = t1c1.getEdges(Direction.OUT, EdgeLabel.FILTER.t()).iterator().next();
                edge.setProperty("customKey", "customValue");
                return null;
            }
        });

        // nejprve to vyexportujeme
        DumpHelper dumpHelper = new DumpHelper();

        DumpExportProcessorImpl dumpExportProcessor = new DumpExportProcessorImpl();
        dumpExportProcessor.setDatabaseHolder(getDatabaseHolder());
        dumpExportProcessor.setRevisionRootHandler(getRevisionRootHandler());
        dumpExportProcessor.setSuperRootHandler(getSuperRootHandler());

        dumpHelper.setDumpExportProcessor(dumpExportProcessor);

        SourceCodeDumpHelper sourceCodeDumpHelper = new SourceCodeDumpHelper();
        sourceCodeDumpHelper.setSourceRootHandler(getSourceRootHandler());
        sourceCodeDumpHelper.setBackUpDir(new File("target/backup"));
        dumpHelper.setSourceCodeDumpHelper(sourceCodeDumpHelper);

        File file = new File("target/dump.zip");
        OutputStream outputStream = new FileOutputStream(file);
        dumpHelper.exportDump(outputStream);
        Assert.assertTrue(file.exists());

        // vytvořime nový uzel, aby se grafy lišily
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                GraphCreation.createResource(transaction, root, "newRes", "newResType", "desc", layer,
                        REVISION_1_000000);
                return null;
            }
        });
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(getBasicGraphVertexCount() + SOURCE_CODES_ADDED + ATTRIBUTES_ADDED + RESOURCES_ADDED
                        + LAYERS_ADDED, getVertexCount(transaction));
                return null;
            }
        });

        // import        
        DumpImportProcessorImpl dumpImportProcessor = new DumpImportProcessorImpl();
        dumpImportProcessor.setDatabaseHolder(getDatabaseHolder());
        dumpImportProcessor.setRevisionRootHandler(getRevisionRootHandler());
        dumpImportProcessor.setSuperRootHandler(getSuperRootHandler());
        dumpImportProcessor.setSourceRootHandler(getSourceRootHandler());

        dumpHelper.setDumpImportProcessor(dumpImportProcessor);

        InputStream inputStream = new FileInputStream(file);
        dumpHelper.importDump(inputStream);

        checkImportedGraphRevisionNumbers();
        
        ContentCollectorVisitor collectedContent = collectContent(REVISION_INTERVAL_1_000000_1_000000);
        Assert.assertEquals(1, collectedContent.getResourceTypes().size());
        Assert.assertEquals(3, (long) collectedContent.getEdgeTypes().get(EdgeLabel.DIRECT));
        Assert.assertEquals(1, (long) collectedContent.getEdgeTypes().get(EdgeLabel.FILTER));

        Assert.assertEquals(3, collectedContent.getNodeTypes().size());
        Assert.assertEquals(1, (long) collectedContent.getNodeTypes().get(new NodeTypeDef("Database", "Teradata")));
        Assert.assertEquals(2, (long) collectedContent.getNodeTypes().get(new NodeTypeDef("Table", "Teradata")));
        Assert.assertEquals(4, (long) collectedContent.getNodeTypes().get(new NodeTypeDef("Column", "Teradata")));

        Assert.assertEquals(3, (long) collectedContent.getAttrTypes().get(AttributeType.VERTEX));
        Assert.assertEquals(1, (long) collectedContent.getAttrTypes().get(AttributeType.EDGE));

        Assert.assertTrue(new File(getSourceRootHandler().getSourceCodeDir(), fileId).exists());

    }

    /**
     * Test import of a dump from version 1.20.
     * 
     * Tested dump file was exported from the version 1.20 from the test method "testimportDump()" in this class.
     * That means, dump file should contain the same basic graph created by the method "createGraph()" and
     * enriched by node attribute on node t1c1. (see the original method "testimportDump()" from version 1.20).
     * 
     * @throws IOException
     */
    @Test
    public void testImportDump_1_20() throws IOException {
        // use dump file exported from version 1.20
        File file = new File("src/test/resources/dump_1_20.zip");
        DumpHelper dumpHelper = new DumpHelper();

        SourceCodeDumpHelper sourceCodeDumpHelper = new SourceCodeDumpHelper();
        sourceCodeDumpHelper.setSourceRootHandler(getSourceRootHandler());
        sourceCodeDumpHelper.setBackUpDir(new File("target/backup"));
        dumpHelper.setSourceCodeDumpHelper(sourceCodeDumpHelper);

        DumpImportProcessorImpl dumpImportProcessor = new DumpImportProcessorImpl();
        dumpImportProcessor.setDatabaseHolder(getDatabaseHolder());
        dumpImportProcessor.setRevisionRootHandler(getRevisionRootHandler());
        dumpImportProcessor.setSuperRootHandler(getSuperRootHandler());
        dumpImportProcessor.setSourceRootHandler(getSourceRootHandler());

        dumpHelper.setDumpImportProcessor(dumpImportProcessor);

        InputStream inputStream = new FileInputStream(file);
        dumpHelper.importDump(inputStream);
        
        checkImportedGraphRevisionNumbers();

        ContentCollectorVisitor collectedContent = collectContent(REVISION_INTERVAL_1_000000_1_000000);
        Assert.assertEquals(1, collectedContent.getResourceTypes().size());
        Assert.assertEquals(3, (long) collectedContent.getEdgeTypes().get(EdgeLabel.DIRECT));
        Assert.assertEquals(1, (long) collectedContent.getEdgeTypes().get(EdgeLabel.FILTER));

        Assert.assertEquals(3, collectedContent.getNodeTypes().size());
        Assert.assertEquals(1, (long) collectedContent.getNodeTypes().get(new NodeTypeDef("Database", "Teradata")));
        Assert.assertEquals(2, (long) collectedContent.getNodeTypes().get(new NodeTypeDef("Table", "Teradata")));
        Assert.assertEquals(4, (long) collectedContent.getNodeTypes().get(new NodeTypeDef("Column", "Teradata")));

        Assert.assertEquals(3, (long) collectedContent.getAttrTypes().get(AttributeType.VERTEX));
        Assert.assertEquals(1, (long) collectedContent.getAttrTypes().get(AttributeType.EDGE));

    }
    
    /**
     * Check revision numbers of selected vertices and edges in the main graph.
     * Check revision numbers in the whole revision tree (check revision numbers of root vertex and all revision nodes).
     * 
     */
    private void checkImportedGraphRevisionNumbers() {
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Assert.assertEquals(getBasicGraphVertexCount() + SOURCE_CODES_ADDED + ATTRIBUTES_ADDED,
                        getVertexCount(transaction));

                Iterator<Vertex> resIter = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata")
                        .iterator();
                Assert.assertTrue(resIter.hasNext());
                Vertex resourceTeradata = resIter.next();
                Assert.assertFalse(resIter.hasNext());

                Assert.assertEquals("Teradata", resourceTeradata.getProperty(NodeProperty.RESOURCE_TYPE.t()));
                Assert.assertEquals("Description", resourceTeradata.getProperty(NodeProperty.RESOURCE_DESCRIPTION.t()));

                // check resource revision validity (from <1, 1> to <1.0, 1.999999>
                Edge teradataControlEdge = GraphOperation.getControlEdge(resourceTeradata);
                Assert.assertEquals(REVISION_1_000000, teradataControlEdge.getProperty(EdgeProperty.TRAN_START.t()));
                Assert.assertEquals(REVISION_1_999999, teradataControlEdge.getProperty(EdgeProperty.TRAN_END.t()));

                Iterator<Vertex> tableIter = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator();
                Assert.assertTrue(tableIter.hasNext());
                Vertex nodeTable1 = tableIter.next();
                Assert.assertFalse(tableIter.hasNext());

                Assert.assertEquals("Table", nodeTable1.getProperty(NodeProperty.NODE_TYPE.t()));

                Edge table1ControlEdge = GraphOperation.getControlEdge(nodeTable1);
                Assert.assertEquals(REVISION_1_000000, table1ControlEdge.getProperty(EdgeProperty.TRAN_START.t()));
                Assert.assertEquals(REVISION_1_999999, table1ControlEdge.getProperty(EdgeProperty.TRAN_END.t()));

                Object attributeValue = GraphOperation.getFirstNodeAttribute(nodeTable1, "TABLE_TYPE",
                        REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertNotNull(attributeValue);
                Assert.assertEquals("TABLE", attributeValue);

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Edge t1c1ControlEdge = GraphOperation.getControlEdge(t1c1);
                Assert.assertEquals(REVISION_1_000000, t1c1ControlEdge.getProperty(EdgeProperty.TRAN_START.t()));
                Assert.assertEquals(REVISION_1_999999, t1c1ControlEdge.getProperty(EdgeProperty.TRAN_END.t()));

                Object attr = GraphOperation.getFirstNodeAttribute(t1c1, "ORDER", REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(8, attr);

                Edge filterFlowEdge = t1c1.getEdges(Direction.OUT, EdgeLabel.FILTER.t()).iterator().next();
                Assert.assertEquals("customValue", filterFlowEdge.getProperty("customKey"));
                Assert.assertEquals(REVISION_1_000000, filterFlowEdge.getProperty(EdgeProperty.TRAN_START.t()));
                Assert.assertEquals(REVISION_1_999999, filterFlowEdge.getProperty(EdgeProperty.TRAN_END.t()));

                // check revision root
                Vertex revisionRoot = getRevisionRootHandler().getRoot(transaction);
                Double latestCommittedRevision = revisionRoot.getProperty(NodeProperty.LATEST_COMMITTED_REVISION.t());
                Double latestUncommittedRevision = revisionRoot
                        .getProperty(NodeProperty.LATEST_UNCOMMITTED_REVISION.t());
                Assert.assertEquals(REVISION_1_000000, latestCommittedRevision);
                Assert.assertEquals((Double) (-1.0), latestUncommittedRevision);

                // check revision nodes
                List<Vertex> revisionNodes = GraphOperation.getAdjacentVertices(revisionRoot, Direction.OUT,
                        EVERY_REVISION_INTERVAL, EdgeLabel.HAS_REVISION);

                for (Vertex revisionNode : revisionNodes) {
                    Double revisionNumber = revisionNode.getProperty(NodeProperty.REVISION_NODE_REVISION.t());
                    Double previousRevision = revisionNode
                            .getProperty(NodeProperty.REVISION_NODE_PREVIOUS_REVISION.t());
                    Double nextRevision = revisionNode.getProperty(NodeProperty.REVISION_NODE_NEXT_REVISION.t());
                    Double technicalRevision = RevisionRootHandler.TECHNICAL_REVISION_NUMBER;
                    
                    // there has to be only two revision nodes: 0.0 and 1.0
                    Assert.assertThat(revisionNumber,
                            CoreMatchers.anyOf(CoreMatchers.is(technicalRevision), CoreMatchers.is(REVISION_1_000000)));

                    if (revisionNumber.equals(technicalRevision)) {
                        Assert.assertEquals((Double) (-1.0), previousRevision);
                        Assert.assertEquals(REVISION_1_000000, nextRevision);
                    } else if (revisionNumber.equals(REVISION_1_000000)) {
                        Assert.assertEquals(technicalRevision, previousRevision);
                        Assert.assertEquals((Double) (-1.0), nextRevision);
                    }
                }

                return null;
            }
        });
    }

    private String createGraph() {
        createBasicGraph(false);

        SourceCodeHelper sourceCodeHelper = new SourceCodeHelper();
        sourceCodeHelper.setSourceRootHandler(getSourceRootHandler());

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SOURCE_CODE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex sourceRoot = getSourceRootHandler().getRoot(transaction);
                GraphCreation.createSourceCode(transaction, sourceRoot, REVISION_1_000000,
                        "src/test/resources/sourceCode1.txt", "aaa");
                return null;
            }
        });

        String fileId = TestHelper.mergeFile(getDatabaseHolder(), getSourceRootHandler(), sourceCodeHelper,
                "src/test/resources/sourceCode1.txt", REVISION_1_000000);
        commitRevision(REVISION_1_000000);

        return fileId;
    }

    private ContentCollectorVisitor collectContent(RevisionInterval revision) {
        TraverserProcessorFactory processorFactory = new TraverserProcessorFactoryGeneric<TraverserProcessorPrefix>(
                TraverserProcessorPrefix.class, MergerController.MODULE_NAME, new SearchOrderDfs());

        GraphScalableTraverser traverser = new GraphScalableTraverserSerial(getDatabaseHolder(), processorFactory);
        ContentCollectorVisitor visitor = new ContentCollectorVisitor(getTerchnicalAttributes());
        Object startVertexId = getSuperRootHandler().getRoot(getDatabaseHolder());
        traverser.traverse(visitor, startVertexId, AccessLevel.READ, revision);

        return visitor;
    }
}
