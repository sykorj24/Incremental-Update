package eu.profinit.manta.dataflow.repository.dump.server.helper.processor;

import java.util.Set;

import static org.junit.Assert.*;
import org.junit.Test;

import eu.profinit.manta.dataflow.repository.dump.server.helper.processor.ProductVersion.Delta;

/**
 * Unit testy {@link ProductVersion}.
 * 
 * @author onouza
 */
public class ProductVersionTest {

    /**
     * Unit testy {@link ProductVersion#checkCompatibility(ProductVersion)}
     * a {@link ProductVersion#checkCompatibility(ProductVersion, ProductVersion))}.
     */
    @Test
    public void checkCompatibility () {
        assertTrue("Current version must be copatible with itself.", ProductVersion.checkCompatibility(ProductVersion.CURRENT));
        assertTrue("Current version must be copatible to any other.", ProductVersion.checkCompatibility(ProductVersion.V_1_10));
        assertTrue("Same versions must be copatible.", ProductVersion.checkCompatibility(ProductVersion.V_1_17, ProductVersion.V_1_17));
        assertTrue("Newer version must be copatible with older one.", ProductVersion.checkCompatibility(ProductVersion.V_1_10, ProductVersion.V_1_17));
        assertFalse("Older version must not be copatible with newer one.", ProductVersion.checkCompatibility(ProductVersion.V_1_17, ProductVersion.V_1_10));
    }

    /**
     * Unit testy {@link ProductVersion#diff(ProductVersion)}
     * a {@link ProductVersion#diff(ProductVersion, ProductVersion)}.
     */
    @Test
    public void diff () {
        Set<Delta> deltas;
        
        deltas = ProductVersion.diff(ProductVersion.CURRENT);
        assertTrue("Version hans no difference to itself.", deltas.isEmpty());
        
        deltas = ProductVersion.diff(ProductVersion.V_1_17, ProductVersion.V_1_17);
        assertTrue("Same versions hans no differences.", deltas.isEmpty());

        deltas = ProductVersion.diff(ProductVersion.V_1_10, ProductVersion.V_1_17);
        assertEquals("Version 1.17 is different to 1.10", 1, deltas.size());
        assertTrue("Version 1.17 has layer support copared to 1.10", deltas.contains(ProductVersion.Delta.LAYERS));
        
        deltas = ProductVersion.diff(ProductVersion.V_1_10, ProductVersion.V_1_22);
        assertEquals("Version 1.22 is different from version 1.10", 4, deltas.size());
        assertTrue("Version 1.22 has layer support compared to version 1.10", deltas.contains(ProductVersion.Delta.LAYERS));
        assertTrue("Version 1.22 has new revision root properties support compared to version 1.10", deltas.contains(ProductVersion.Delta.REVISION_ROOT_PROPERTIES));
        assertTrue("Version 1.22 has new revision node properties support compared to version 1.10", deltas.contains(ProductVersion.Delta.REVISION_NODE_PROPERTIES));
        assertTrue("Version 1.22 has new revision number data type support compared to version 1.10", deltas.contains(ProductVersion.Delta.DOUBLE_REVISION_NUMBER));
        
        deltas = ProductVersion.diff(ProductVersion.V_1_17, ProductVersion.V_1_22);
        assertEquals("Version 1.22 is different from version 1.17", 3, deltas.size());
        assertTrue("Version 1.22 has new revision root properties support compared to version 1.17", deltas.contains(ProductVersion.Delta.REVISION_ROOT_PROPERTIES));
        assertTrue("Version 1.22 has new revision node properties support compared to version 1.17", deltas.contains(ProductVersion.Delta.REVISION_NODE_PROPERTIES));
        assertTrue("Version 1.22 has new revision number data type support compared to version 1.17", deltas.contains(ProductVersion.Delta.DOUBLE_REVISION_NUMBER));

        
        try {
            ProductVersion.diff(ProductVersion.V_1_17, ProductVersion.V_1_10);
            fail("IllegalArgumentException is expected because older version is not compatible to newer one.");
        }
        catch (IllegalArgumentException e) {
            // ocekavame vyjimku
        }
    }
}
