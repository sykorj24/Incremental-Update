package eu.profinit.manta.dataflow.repository.dump.server.helper.processor;

import java.io.OutputStream;
import java.util.Set;

/**
 * Rozhraní pro provedení exportu dumpu.
 * @author tfechtner
 *
 */
public interface DumpExportProcessor {

    /**
     * Vyexportuje do výstupního streamu.
     * @param outputStream výstupní stream pro output
     * @return množina idéček source node souborů 
     */
    Set<String> processExport(OutputStream outputStream);

}