package eu.profinit.manta.dataflow.repository.dump.server.helper.processor;

import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper nad {@link DataInputStream} vracící při chybě null.
 * @author tfechtner
 *
 */
public class NoExceptionDataInputStream implements Closeable {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(NoExceptionDataInputStream.class);
    /** Vstupní podkladový stream, ze kterého se skutečně čte. */
    private final ObjectInputStream inputStream;

    /**
     * @param inputStream vstupní podkladový stream
     */
    public NoExceptionDataInputStream(ObjectInputStream inputStream) {
        super();
        this.inputStream = inputStream;
    }

    /**
     * Načte boolean ze streamu.
     * @return načtená hodnota nebo null při chybě 
     */
    public final Boolean readBoolean() {
        try {
            return inputStream.readBoolean();
        } catch (EOFException e) {
            throw new EndOfStreamException("Stream has no additional content.", e);
        } catch (IOException e) {
            LOGGER.error("IO exception during reading boolean.", e);
            return false;
        }
    }

    /**
     * Načte integer ze streamu.
     * @return načtená hodnota nebo null při chybě 
     */
    public final Integer readInt() {
        try {
            return inputStream.readInt();
        } catch (EOFException e) {
            throw new EndOfStreamException("Stream has no additional content.", e);
        } catch (IOException e) {
            LOGGER.error("IO exception during reading int.", e);
            return null;
        }
    }
    
    /**
     * Read double from the input stream
     *  
     * @return read double value or null in case of error
     */
    public final Double readDouble() {
        try {
            return inputStream.readDouble();
        } catch (EOFException e) {
            throw new EndOfStreamException("Stream has no additional content.", e);
        } catch (IOException e) {
            LOGGER.error("IO exception during reading double.", e);
            return null;
        }
    }

    /**
     * Načte long ze streamu.
     * @return načtená hodnota nebo null při chybě 
     */
    public final Long readLong() {
        try {
            return inputStream.readLong();
        } catch (EOFException e) {
            throw new EndOfStreamException("Stream has no additional content.", e);
        } catch (IOException e) {
            LOGGER.error("IO exception during reading long.", e);
            return null;
        }
    }

    /**
     * Načte utf string ze streamu.
     * @return načtená hodnota nebo null při chybě 
     */
    public final String readUTF() {
        try {
            return inputStream.readUTF();
        } catch (EOFException e) {
            throw new EndOfStreamException("Stream has no additional content.", e);
        } catch (IOException e) {
            LOGGER.error("IO exception during reading string.", e);
            return null;
        }
    }
    
    /**
     * Načte serializovaný objekt.
     * @return deserializovaný objekt
     */
    public final Object readObject() {
        try {
            return inputStream.readObject();
        } catch (EOFException e) {
            throw new EndOfStreamException("Stream has no additional content.", e);
        } catch (IOException e) {
            LOGGER.error("IO exception during reading serialized object.", e);
            return null;
        } catch (ClassNotFoundException e) {
            LOGGER.error("Class not found during reading serialized object.", e);
            return null;
        }
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }

}
