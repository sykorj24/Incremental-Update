package eu.profinit.manta.dataflow.repository.dump.server.helper.processor;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Wrapper nad {@link DataOutputStream} vyhazující při chybě {@link RuntimeException}. 
 * @author tfechtner
 *
 */
public class NoExceptionDataOutputStream implements Closeable {
    /** Podkladový stream, na který se deleguje zápis. */
    private final ObjectOutputStream objectOutputStream;

    /**
     * @param objectOutputStream podkladový stream, na který se deleguje zápis
     */
    public NoExceptionDataOutputStream(ObjectOutputStream objectOutputStream) {
        this.objectOutputStream = objectOutputStream;
    }

    /**
     * @param v zapisovaná hodnota
     */
    public final void writeLong(long v) {
        try {
            objectOutputStream.writeLong(v);
        } catch (IOException e) {
            throw new RuntimeException("It is not possible to write long to output.", e);
        }
    }

    /**
     * @param v zapisovaná hodnota
     */
    public final void writeInt(int v) {
        try {
            objectOutputStream.writeInt(v);
        } catch (IOException e) {
            throw new RuntimeException("It is not possible to write int to output.", e);
        }
    }
    
    /**
     * @param value  double value to be written
     */
    public final void writeDouble(double value) {
        try {
            objectOutputStream.writeDouble(value);
        } catch(IOException e) {
            throw new RuntimeException("It is not possible to write int to output.", e);
        }
    }

    /**
     * @param str zapisovaná hodnota
     */
    public final void writeUTF(String str) {
        try {
            if (str != null) {
                objectOutputStream.writeUTF(str);
            } else {
                objectOutputStream.writeUTF("");
            }
        } catch (IOException e) {
            throw new RuntimeException("It is not possible to write string to output.", e);
        }
    }

    /**
     * @param v zapisovaná hodnota
     */
    public final void writeBoolean(boolean v) {
        try {
            objectOutputStream.writeBoolean(v);
        } catch (IOException e) {
            throw new RuntimeException("It is not possible to write boolean to output.", e);
        }
    }

    /**
     * @param o objekt k zapsání
     */
    public void writeObject(Object o) {
        try {
            objectOutputStream.writeObject(o);
        } catch (IOException e) {
            throw new RuntimeException("It is not possible to serialize objet into the output.", e);
        }

    }

    /**
     * Deleguje flushnutí streamu.
     * @throws IOException chyba při zápisu
     */
    public void flush() throws IOException {
        objectOutputStream.flush();
    }

    /**
     * Deleguje uzavření streamu.
     * @throws IOException chyba při zavření
     */
    @Override
    public void close() throws IOException {
        objectOutputStream.close();
    }
}