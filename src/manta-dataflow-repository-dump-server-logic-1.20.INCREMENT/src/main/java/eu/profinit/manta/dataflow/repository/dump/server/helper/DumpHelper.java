package eu.profinit.manta.dataflow.repository.dump.server.helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.dataflow.repository.dump.server.helper.processor.DumpExportProcessor;
import eu.profinit.manta.dataflow.repository.dump.server.helper.processor.DumpImportProcessor;
import eu.profinit.manta.dataflow.repository.dump.server.helper.processor.SourceCodeDumpHelper;

/**
 * Helper pro provádění dumpu.
 * @author tfechtner
 *
 */
public class DumpHelper {
    /** Název souboru v zipu, který obsahuje dump. */
    private static final String DUMP_ENTRY_NAME = "dump";
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(DumpHelper.class);
    /** Kódování zip streamu. */
    private static final String ENCODING = "UTF-8";
    /** Procesor pro provedení exportu dumpu. */
    private DumpExportProcessor dumpExportProcessor;
    /** Procesor pro provedení importu dumpu. */
    private DumpImportProcessor dumpImportProcessor;
    /** Helper pro dump operace se source code soubory. */
    private SourceCodeDumpHelper sourceCodeDumpHelper;

    /**
     * Vytvoří a pošle dump.
     * @param outputStream výstupní stream
     * @return true, jestliže se export provedl v pořádku
     * @throws IOException chyba při odeslání error výsledku
     */
    public boolean exportDump(OutputStream outputStream) {
        ZipArchiveOutputStream zipStream = new ZipArchiveOutputStream(outputStream);
        zipStream.setEncoding(ENCODING);
        ZipArchiveEntry zipEntry = new ZipArchiveEntry(DUMP_ENTRY_NAME);
        try {
            zipStream.putArchiveEntry(zipEntry);
            Set<String> sourceFileIds = dumpExportProcessor.processExport(zipStream);
            zipStream.closeArchiveEntry();
            sourceCodeDumpHelper.exportSources(zipStream, sourceFileIds);
        } catch (IOException e) {
            LOGGER.error("Error during generating export.", e);
            return false;
        } finally {
            IOUtils.closeQuietly(zipStream);
        }
        return true;
    }

    /**
     * Importuje dump do databáze.
     * @param inputStream stream obsahující dump k importu
     * @return true, jestliže se import zdařil
     */
    public boolean importDump(InputStream inputStream) {
        ZipInputStream zipInputStream = new ZipInputStream(inputStream);

        boolean result;
        try {
            sourceCodeDumpHelper.backUp();
            result = processImport(zipInputStream);

            if (result) {
                sourceCodeDumpHelper.deleteBackUp();
            } else {
                sourceCodeDumpHelper.rollback();
            }
        } catch (Throwable t) {
            sourceCodeDumpHelper.rollback();
            throw t;
        } finally {
            IOUtils.closeQuietly(zipInputStream);
        }
        return result;
    }

    /**
     * Samotné provedení importu.
     * @param zipInputStream stream obsahující data
     * @return true, jestliže se import zdařil
     */
    private boolean processImport(ZipInputStream zipInputStream) {
        int importedSouceCodeFiles = 0;
        try {
            ZipEntry entry = null;
            boolean isLoadedMainData = false;
            while ((entry = zipInputStream.getNextEntry()) != null) {
                if (DUMP_ENTRY_NAME.equals(entry.getName())) {
                    dumpImportProcessor.processImport(zipInputStream);
                    isLoadedMainData = true;
                } else {
                    boolean isFileImported = sourceCodeDumpHelper.importSourceFile(zipInputStream, entry.getName());
                    if (isFileImported) {
                        importedSouceCodeFiles++;
                    }
                }
                zipInputStream.closeEntry();
            }
            if (!isLoadedMainData) {
                LOGGER.error("The zip archive does not contain main data file.");
                return false;
            }
        } catch (IOException e) {
            LOGGER.error("IO exception during reading zip archive.", e);
            return false;
        }

        LOGGER.info("Imported {} source code files.", importedSouceCodeFiles);
        return true;
    }

    /**
     * @return Procesor pro provedení exportu dumpu. 
     */
    public DumpExportProcessor getDumpExportProcessor() {
        return dumpExportProcessor;
    }

    /**
     * @param dumpExportProcessor Procesor pro provedení exportu dumpu. 
     */
    public void setDumpExportProcessor(DumpExportProcessor dumpExportProcessor) {
        this.dumpExportProcessor = dumpExportProcessor;
    }

    /**
     * @return Procesor pro provedení importu dumpu. 
     */
    public DumpImportProcessor getDumpImportProcessor() {
        return dumpImportProcessor;
    }

    /**
     * @param dumpImportProcessor Procesor pro provedení importu dumpu. 
     */
    public void setDumpImportProcessor(DumpImportProcessor dumpImportProcessor) {
        this.dumpImportProcessor = dumpImportProcessor;
    }

    /**
     * @return Helper pro dump operace se source code soubory. 
     */
    public SourceCodeDumpHelper getSourceCodeDumpHelper() {
        return sourceCodeDumpHelper;
    }

    /**
     * @param sourceCodeDumpHelper Helper pro dump operace se source code soubory. 
     */
    public void setSourceCodeDumpHelper(SourceCodeDumpHelper sourceCodeDumpHelper) {
        this.sourceCodeDumpHelper = sourceCodeDumpHelper;
    }

}
