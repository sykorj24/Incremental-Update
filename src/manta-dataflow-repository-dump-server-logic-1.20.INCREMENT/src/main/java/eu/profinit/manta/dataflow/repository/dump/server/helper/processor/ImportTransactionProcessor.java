package eu.profinit.manta.dataflow.repository.dump.server.helper.processor;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.dump.server.helper.ObjectType;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.ProcessorResult;

/**
 * Procesor pro provedení importu v rámci jedné transakce.
 * @author tfechtner
 *
 */
public class ImportTransactionProcessor implements TransactionCallback<Integer> {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(ImportTransactionProcessor.class);

    /** Podkladový zip vstupní stream. */
    private final ZipInputStream zipInputStream;
    /** Datový vstupní stream. */
    private final NoExceptionDataInputStream inputStream;
    /** Mapa ID vertexů z dumpu na Id v repository.*/
    private final Map<Long, Object> idsMap = new HashMap<Long, Object>();
    /** Držák na super root. */
    private final SuperRootHandler superRootHandler;
    /** Držák na revision root. */
    private final RevisionRootHandler revisionRootHandler;
    /** Držák na source root. */
    private final SourceRootHandler sourceRootHandler;
    /** Verze aplikace, ze ktere pochazi importovana data. */
    private final ProductVersion importedVersion;
    /** Struktura udržující přehled o počtech importovaných elementů. */
    private final ProcessorResult<ImportAction> importedObjects = new ProcessorResult<ImportAction>();

    /**
     * @param zipInputStream podkladový zip stream pro identifikaci konce
     * @param inputStream vstupní stream pro čtení dat
     * @param superRootHandler držák na super root
     * @param revisionRootHandler držák na revision root
     * @param sourceRootHandler držák na source root
     * @param importedVersion Verze importovaných dat
     */
    public ImportTransactionProcessor(ZipInputStream zipInputStream, NoExceptionDataInputStream inputStream,
            SuperRootHandler superRootHandler, RevisionRootHandler revisionRootHandler,
            SourceRootHandler sourceRootHandler, ProductVersion importedVersion) {
        super();
        this.zipInputStream = zipInputStream;
        this.inputStream = inputStream;
        this.superRootHandler = superRootHandler;
        this.revisionRootHandler = revisionRootHandler;
        this.sourceRootHandler = sourceRootHandler;
        this.importedVersion = importedVersion;
    }

    @Override
    public String getModuleName() {
        return MergerController.MODULE_NAME;
    }

    @Override
    public Integer callMe(TitanTransaction transaction) {
        int count = 0;
        try {
            // dokud není podkladový stream prázdný
            while (zipInputStream.available() != 0 && count < DumpImportProcessorImpl.MAXIMUM_OBJECTS_PER_TRAN) {
                String objectTypeString = null;
                try {
                    objectTypeString = inputStream.readUTF();
                } catch (EndOfStreamException e) {
                    LOGGER.info("Zip archive has ended.");
                    break;
                }

                if (objectTypeString == null) {
                    LOGGER.error("Object type must not be null.");
                    break;
                }

                ObjectType objectType = ObjectType.valueOf(objectTypeString);

                switch (objectType) {
                case VERTEX:
                    importVertex(transaction);
                    break;
                case EDGE:
                    importEdge(transaction);
                    break;
                default:
                    throw new IllegalStateException("Unknown object type " + objectType.toString() + ".");
                }
                count++;
            }
        } catch (IOException e) {
            DumpImportProcessorImpl.LOGGER.error("Error during finding end of the stream.", e);
            return 0;
        }

        return count;
    }

    /**
     * Vrátí strukturu udržující počty nahraných elementů.
     * @return struktura s počty nahraných elementů
     */
    public ProcessorResult<ImportAction> getImportedObjects() {
        return importedObjects;
    }

    /**
     * Naimportuje vertex objekt do repository.
     * @param transaction zapisovací transakce
     */
    private void importVertex(TitanTransaction transaction) {
        String vertexTypeString = inputStream.readUTF();
        VertexType vertexType = VertexType.valueOf(vertexTypeString);

        ImportAction importStatus;

        switch (vertexType) {
        case SUPER_ROOT:
            importStatus = importSuperRoot(transaction);
            break;
        case REVISION_ROOT:
            importStatus = importRevisionRoot(transaction);
            break;
        case LAYER:
            importStatus = importLayer(transaction);
            break;
        case RESOURCE:
            importStatus = importResource(transaction);
            break;
        case NODE:
            importStatus = importNode(transaction);
            break;
        case ATTRIBUTE:
            importStatus = importAttribute(transaction);
            break;
        case REVISION_NODE:
            importStatus = importRevision(transaction);
            break;
        case SOURCE_ROOT:
            importStatus = importSourceRoot(transaction);
            break;
        case SOURCE_NODE:
            importStatus = importSourceNode(transaction);
            break;
        default:
            throw new IllegalStateException("Unknown vertex type " + vertexType.toString() + ".");
        }

        importedObjects.saveVertexAction(importStatus, vertexType);

    }

    /**
     * Naimportuje source root do repository.
     * @param transaction zapisovací transakce
     * @return výsledek akce 
     */
    private ImportAction importSourceRoot(TitanTransaction transaction) {
        Vertex root = sourceRootHandler.getRoot(transaction);
        long id = inputStream.readLong();
        idsMap.put(id, root.getId());
        return ImportAction.ADD;
    }

    /**
     * Naimportuje super root do repository.
     * @param transaction zapisovací transakce
     * @return true, jestliže byl objekt v pořádku přidán 
     */
    private ImportAction importSuperRoot(TitanTransaction transaction) {
        Vertex root = superRootHandler.getRoot(transaction);
        long id = inputStream.readLong();
        idsMap.put(id, root.getId());
        return ImportAction.ADD;
    }

    /**
     * Naimportuje revision root do repository.
     * @param transaction zapisovací transakce
     * @return true, jestliže byl objekt v pořádku přidán 
     */
    private ImportAction importRevisionRoot(TitanTransaction transaction) {
        long id = inputStream.readLong();
        Vertex root = revisionRootHandler.getRoot(transaction);
        idsMap.put(id, root.getId());

        if (ProductVersion.diff(importedVersion).contains(ProductVersion.Delta.REVISION_ROOT_PROPERTIES)) {
            // Nothing to do - the imported version does NOT support revision root properties
            // "latestCommittedRevision" and "latestUncommittedRevision" 
        } else {
            if (ProductVersion.diff(importedVersion).contains(ProductVersion.Delta.DOUBLE_REVISION_NUMBER)) {
                // Revision root properties are NOT in Double -> Should never happen but still better be safe and check it
                throw new IllegalStateException(
                        "LatestCommittedRevision and LatestUncommittedRevision in revision root are expected to be in Double data type");
            } else {
                // Read properties "latestCommittedRevision" and "latestUncommittedRevision"
                Double latestCommittedRevision = inputStream.readDouble();
                Double latestUncommittedRevision = inputStream.readDouble();
                root.setProperty(NodeProperty.LATEST_COMMITTED_REVISION.t(), latestCommittedRevision);
                root.setProperty(NodeProperty.LATEST_COMMITTED_REVISION.t(), latestUncommittedRevision);
            }
        }
        
        return ImportAction.ADD;
    }

    /**
     * Naimportuje vrstvu do repository.
     * @param transaction Zapisovaci transakce.
     * @return {@code true} vysledek importu vrstvy.  
     */
    private ImportAction importLayer(TitanTransaction transaction) {
        if (ProductVersion.diff(importedVersion).contains(ProductVersion.Delta.LAYERS)) {
            // Zkousime importovat vrstvy z veze, kde jeste nebyly podporovany.
            // Nemelo by nastat, ale co kdyby...
            DumpImportProcessorImpl.LOGGER.error("Layers are not supported in version " + importedVersion + ".");
            return ImportAction.ERROR;
        }
        long id = inputStream.readLong();
        String name = inputStream.readUTF();
        String type = inputStream.readUTF();
        if (name == null) {
            DumpImportProcessorImpl.LOGGER.error("Layer name must not be null.");
            return ImportAction.ERROR;
        }
        if (type == null) {
            DumpImportProcessorImpl.LOGGER.error("Layer type must not be null.");
            return ImportAction.ERROR;
        }
        Vertex newVertex = GraphCreation.createLayer(transaction, name, type);
        idsMap.put(id, newVertex.getId());
        return ImportAction.ADD;
    }

    /**
     * Naimportuje resource do repository.
     * @param transaction zapisovací transakce
     * @return true, jestliže byl objekt v pořádku přidán 
     */
    private ImportAction importResource(TitanTransaction transaction) {
        long id = inputStream.readLong();
        String name = inputStream.readUTF();
        String type = inputStream.readUTF();
        String description = inputStream.readUTF();

        if (name == null) {
            DumpImportProcessorImpl.LOGGER.error("Resource name must not be null.");
            return ImportAction.ERROR;
        }
        if (type == null) {
            DumpImportProcessorImpl.LOGGER.error("Resource type must not be null.");
            return ImportAction.ERROR;
        }
        if (description == null) {
            DumpImportProcessorImpl.LOGGER.error("Resource description must not be null.");
            return ImportAction.ERROR;
        }

        Vertex newVertex = transaction.addVertex();
        newVertex.setProperty(NodeProperty.VERTEX_TYPE.t(), VertexType.RESOURCE);
        newVertex.setProperty(NodeProperty.RESOURCE_NAME.t(), name);
        newVertex.setProperty(NodeProperty.RESOURCE_TYPE.t(), type);
        newVertex.setProperty(NodeProperty.RESOURCE_DESCRIPTION.t(), description);
        idsMap.put(id, newVertex.getId());
        
        return ImportAction.ADD;
    }

    /**
     * Naimportuje uzel do repository.
     * @param transaction zapisovací transakce
     * @return true, jestliže byl objekt v pořádku přidán
     */
    private ImportAction importNode(TitanTransaction transaction) {
        long id = inputStream.readLong();
        String name = inputStream.readUTF();
        String type = inputStream.readUTF();

        if (name == null) {
            DumpImportProcessorImpl.LOGGER.error("Node name must not be null.");
            return ImportAction.ERROR;
        }
        if (type == null) {
            DumpImportProcessorImpl.LOGGER.error("Node type must not be null.");
            return ImportAction.ERROR;
        }
        Vertex newNode = transaction.addVertex(null);
        newNode.setProperty(NodeProperty.VERTEX_TYPE.t(), VertexType.NODE);
        newNode.setProperty(NodeProperty.NODE_NAME.t(), name);
        newNode.setProperty(NodeProperty.NODE_TYPE.t(), type);
        idsMap.put(id, newNode.getId());

        return ImportAction.ADD;
    }

    /**
     * Naimportuje atribut do repository.
     * @param transaction zapisovací transakce
     * @return true, jestliže byl objekt v pořádku přidán
     */
    private ImportAction importAttribute(TitanTransaction transaction) {
        long id = inputStream.readLong();
        String key = inputStream.readUTF();
        Object value = inputStream.readObject();

        if (key == null) {
            LOGGER.error("Attribute key must not be null.");
            return ImportAction.ERROR;
        }

        if (value == null) {
            LOGGER.error("Attribute value must not be null.");
            return ImportAction.ERROR;
        }

        Vertex newNode = transaction.addVertex();
        newNode.setProperty(NodeProperty.VERTEX_TYPE.t(), VertexType.ATTRIBUTE);
        newNode.setProperty(NodeProperty.ATTRIBUTE_NAME.t(), key);
        newNode.setProperty(NodeProperty.ATTRIBUTE_VALUE.t(), value);
        idsMap.put(id, newNode.getId());

        return ImportAction.ADD;
    }

    /**
     * Naimportuje revizní uzel do repository.
     * @param transaction zapisovací transakce
     * @return true, jestliže byl objekt v pořádku přidán
     */
    private ImportAction importRevision(TitanTransaction transaction) {
        Double revision;
        Boolean isCommitted;
        Date commitTime;
        Double previousRevision;
        Double nextRevision;
        
        ImportAction result = ImportAction.ADD;
        
        long id = inputStream.readLong();
        
        try {
            revision = readRevisionNumber();
            isCommitted = inputStream.readBoolean();
            try {
                commitTime = readCommitTime();
            } catch(ParseException e) {
                LOGGER.error("Error during parsing revision commit time.", e);
                commitTime = new Date();
                result = ImportAction.WARNING;
            }
            previousRevision = readPreviousRevisionNumber();
            nextRevision = readNextRevisionNumber();
        } catch(IllegalStateException e) {
            LOGGER.error(e.getMessage());
            return ImportAction.ERROR;
        }
        
        Vertex newRevisionVertex = transaction.addVertex(null);
        newRevisionVertex.setProperty(NodeProperty.VERTEX_TYPE.t(), VertexType.REVISION_NODE);
        newRevisionVertex.setProperty(NodeProperty.REVISION_NODE_REVISION.t(), revision);
        newRevisionVertex.setProperty(NodeProperty.REVISION_NODE_COMMITTED.t(), isCommitted);
        newRevisionVertex.setProperty(NodeProperty.REVISION_NODE_COMMIT_TIME.t(), commitTime);
        
        if(previousRevision != null) {
            newRevisionVertex.setProperty(NodeProperty.REVISION_NODE_PREVIOUS_REVISION.t(), previousRevision);
        }
        if(nextRevision != null) {
            newRevisionVertex.setProperty(NodeProperty.REVISION_NODE_NEXT_REVISION.t(), nextRevision);
        }
        
        idsMap.put(id, newRevisionVertex.getId());

        return result;
    }
    
    /**
     * Read revision number from the input stream
     * 
     * @return revision number
     */
    private Double readRevisionNumber() {
        Double revisionNumber;
        
        if((ProductVersion.diff(importedVersion).contains(ProductVersion.Delta.DOUBLE_REVISION_NUMBER))) {
            // The imported version does not support revision numbers in Double data type -> read Integer
            // and cast it to Double
            revisionNumber = new Double(inputStream.readInt());
        } else {
            // The imported version does support revision numbers in Double -> read Double
            revisionNumber = inputStream.readDouble();
        }
        
        if (revisionNumber < RevisionRootHandler.TECHNICAL_REVISION_NUMBER) {
            throw new IllegalStateException("Value of a revision node must be greater or equals than" + 
                    RevisionRootHandler.TECHNICAL_REVISION_NUMBER);
        }
        
        return revisionNumber;
    }
    
    /**
     * Read revision commit time from the input stream
     * 
     * @return revision commit time
     * @throws ParseException
     */
    private Date readCommitTime() throws ParseException {
        String commitTimeString = inputStream.readUTF();
        
        if (commitTimeString == null) {
            throw new IllegalStateException("Commit time must not be null.");
        }
        
        SimpleDateFormat format = new SimpleDateFormat(AbstractDumpProcessor.DATE_FORMAT);
        Date commitTime;
        
        try {
            commitTime = format.parse(commitTimeString);
        } catch (ParseException e) {
            throw e;
        }
        
        return commitTime;
    }
    
    /**
     * Read previous revision number of a revision node
     * 
     * @return previous revision number or null if property "previousRevision" does not exist
     */
    private Double readPreviousRevisionNumber() {
        if((ProductVersion.diff(importedVersion).contains(ProductVersion.Delta.REVISION_NODE_PROPERTIES))) {
            // The imported version does NOT support revision node property "previousRevision"
            return null;
        } else {
            if(ProductVersion.diff(importedVersion).contains(ProductVersion.Delta.DOUBLE_REVISION_NUMBER)) {
                // PreviousRevision is NOT in Double -> Should never happen but still better be safe and check it
                throw new IllegalStateException(
                        "PreviousRevision in revision node is expected to be in Double data type");
            } else {
                return inputStream.readDouble();
            }
        }
    }
    
    /**
     * Read next revision number of a revision node
     * 
     * @return next revision number or null if property "nextRevision" does not exist
     */
    private Double readNextRevisionNumber() {
        if((ProductVersion.diff(importedVersion).contains(ProductVersion.Delta.REVISION_NODE_PROPERTIES))) {
            // The imported version does NOT support revision node property "nextRevision"
            return null;
        } else {
            if(ProductVersion.diff(importedVersion).contains(ProductVersion.Delta.DOUBLE_REVISION_NUMBER)) {
                // NextRevision is NOT in Double -> Should never happen but still better be safe and check it
                throw new IllegalStateException(
                        "NextRevision in revision node is expected to be in Double data type");
            } else {
                return inputStream.readDouble();
            }
        }
    }

    /**
     * Naimportuje source node do repository.
     * @param transaction zapisovací transakce
     * @return výsledek operace
     */
    private ImportAction importSourceNode(TitanTransaction transaction) {
        long id = inputStream.readLong();
        String localName = inputStream.readUTF();
        String hash = inputStream.readUTF();
        String fileId = inputStream.readUTF();

        if (localName == null) {
            DumpImportProcessorImpl.LOGGER.error("Source Node local name must not be null.");
            return ImportAction.ERROR;
        }
        if (hash == null) {
            DumpImportProcessorImpl.LOGGER.error("Source Node hash must not be null.");
            return ImportAction.ERROR;
        }
        if (fileId == null) {
            DumpImportProcessorImpl.LOGGER.error("Source Node field id must not be null.");
            return ImportAction.ERROR;
        }
        Vertex newNode = transaction.addVertex(null);
        newNode.setProperty(NodeProperty.VERTEX_TYPE.t(), VertexType.SOURCE_NODE);
        newNode.setProperty(NodeProperty.SOURCE_NODE_LOCAL.t(), localName);
        newNode.setProperty(NodeProperty.SOURCE_NODE_HASH.t(), hash);
        newNode.setProperty(NodeProperty.SOURCE_NODE_ID.t(), fileId);
        idsMap.put(id, newNode.getId());

        return ImportAction.ADD;
    }

    /**
     * Naimportuje hranu do repository.
     * @param transaction zapisovací transakce
     */
    private void importEdge(TitanTransaction transaction) {
        long startId = inputStream.readLong();
        long endId = inputStream.readLong();
        String type = inputStream.readUTF();
        
        EdgeLabel edgeLabel = EdgeLabel.parseFromDbType(type);
        if (edgeLabel == null) {
            LOGGER.error("The type {} is not correct for the flow edge.", type);
            importedObjects.saveEdgeAction(ImportAction.ERROR, null);
            return;
        }
        
        Double tranStart;
        Double tranEnd;
        
        if(ProductVersion.diff(importedVersion).contains(ProductVersion.Delta.DOUBLE_REVISION_NUMBER)) {
            // The imported version does not support revision numbers in Double data type -> read Integer
            // and cast it to Double
            tranStart = new Double(inputStream.readInt());
            tranEnd = new Double(inputStream.readInt());
            
            // Set end revision (tranEnd property) to "end.999999" value, to keep consistent with the new repository logic
            // The only exception is hasRevision edge (for this edge always applies: tranStart == tranEnd == revisionNumber)
            if(!EdgeLabel.HAS_REVISION.equals(edgeLabel)) {
                tranEnd = RevisionUtils.getMaxMinorRevisionNumber(tranEnd);            
            }
        } else {
            // The imported version does support revision numbers in Double -> read Double
            tranStart = inputStream.readDouble();
            tranEnd = inputStream.readDouble();
        }
        
        // načíst atributy hrany
        Map<String, Object> properties = new HashMap<String, Object>();
        long attrCount = inputStream.readInt();
        for (int i = 0; i < attrCount; i++) {
            String key = inputStream.readUTF();
            Object value = inputStream.readObject();
            if (value != null) {
                properties.put(key, value);
            } else {
                LOGGER.warn("Deserialized object for property {} was null.", key);
            }
        }

        Double technicalRevision = RevisionRootHandler.TECHNICAL_REVISION_NUMBER;
        
        if (tranStart < technicalRevision) {
            LOGGER.error("Edge start revision must be greater or equal {}, it was {}.", technicalRevision, tranStart);
            importedObjects.saveEdgeAction(ImportAction.ERROR, edgeLabel);
            return;
        }
        if (tranEnd < technicalRevision) {
            LOGGER.error("Edge end revision must be greater or equal {}, it was {}.", technicalRevision, tranEnd);
            importedObjects.saveEdgeAction(ImportAction.ERROR, edgeLabel);
            return;
        }

        Vertex startNode = findVertex(transaction, startId);
        if (startNode == null) {
            LOGGER.error("Start of the edge {} is not in the repository.", type);
            importedObjects.saveEdgeAction(ImportAction.ERROR, edgeLabel);
            return;
        }

        Vertex endNode = findVertex(transaction, endId);
        if (endNode == null) {
            LOGGER.error("Target of the edge {} is not in the repository.", type);
            importedObjects.saveEdgeAction(ImportAction.ERROR, edgeLabel);
            return;
        }

        Edge newEdge = GraphCreation.createEdge(startNode, endNode, edgeLabel, new RevisionInterval(tranStart, tranEnd));
        // vytvořit atributy
        Set<Entry<String, Object>> propertyEntrySet = properties.entrySet();
        for (Entry<String, Object> propertyEntry : propertyEntrySet) {
            String key = propertyEntry.getKey();
            if (!EdgeProperty.TRAN_START.t().equals(key) && !EdgeProperty.TRAN_END.t().equals(key)) {
                newEdge.setProperty(key, propertyEntry.getValue());
            }
        }
        
        // Pokud pridavame hranu od vrcholu k resourcu a importujeme ze starsi verze,
        // ktera jeste nepodporovala vrstvy, vytvorime defaultni vrstvu a resource na ni pripojime.
        if (edgeLabel.equals(EdgeLabel.HAS_RESOURCE)
                && VertexType.getType(startNode).equals(VertexType.RESOURCE)
                && ProductVersion.diff(importedVersion).contains(ProductVersion.Delta.LAYERS)) {
            Vertex layerNode = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
            GraphCreation.createEdge(startNode, layerNode, EdgeLabel.IN_LAYER, new RevisionInterval(tranStart, tranEnd));
            importedObjects.saveVertexAction(ImportAction.NEW, VertexType.LAYER);
            importedObjects.saveEdgeAction(ImportAction.NEW, EdgeLabel.IN_LAYER);
        }

        importedObjects.saveEdgeAction(ImportAction.ADD, edgeLabel);
    }

    /**
     * Nalezne vertex podle id z dumpu.
     * @param transaction transakce pro přístup k repository
     * @param vertexId id hledaného vertexu z dumpu
     * @return nalezený vertex nebo null
     */
    private Vertex findVertex(TitanTransaction transaction, long vertexId) {
        Object dbId = idsMap.get(vertexId);
        if (dbId == null) {
            return null;
        }

        Vertex foundVertex = transaction.getVertex(dbId);
        if (foundVertex == null) {
            throw new IllegalStateException(
                    "Vertex with id " + dbId + " is not in the repository, even it is saved in IDs map.");
        }

        return foundVertex;
    }

    /**
     * Enum pro reprezentaci výsledku importu elementu.
     * @author tfechtner
     *
     */
    public enum ImportAction {
        /** Vygenerovani noveho objektu, ktery puvodne nebyl v importovanych datech, ale je nutny po prechodu na novou verzi */
        NEW,
        /** Úspěšné přidání objektu. */
        ADD,
        /** Chyba sice umožňující import, ale pochybný.*/
        WARNING,
        /** Chyba znemožňující import.*/
        ERROR;
    }
}