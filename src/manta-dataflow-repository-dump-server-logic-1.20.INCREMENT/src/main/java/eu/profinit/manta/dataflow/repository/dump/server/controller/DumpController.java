package eu.profinit.manta.dataflow.repository.dump.server.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.dump.server.helper.DumpHelper;
import eu.profinit.manta.dataflow.repository.dump.server.security.DumpRoles;
import eu.profinit.manta.platform.configuration.ZipFileDecompresser;

/**
 * Controller pro import a export dumpu databáze.
 * @author tfechtner
 *
 */
@Controller
public class DumpController {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(DumpController.class);
    
    
    /** Formát pro datum, přidávané jako prefix k výstupu. */
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    @Autowired
    private DumpHelper dumpHelper;
    @Autowired
    private HorizontalFilterProvider horizontalFilterProvider;

    /**
     * Provede export dumpu pro api.
     * @param response http response pro namapování streamu
     * @throws IOException chyba při nastovávní response chybového stavu 
     */
    @Secured(DumpRoles.DUMP)
    @RequestMapping(value = "/api/dump/export", method = RequestMethod.GET)
    public void exportDumpApi(HttpServletResponse response) throws IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        response.setHeader("Content-Disposition", "attachment;filename=dump_" + dateFormat.format(new Date()) + ".zip");
        dumpHelper.exportDump(response.getOutputStream());
    }
    
    /**
     * Provede export dumpu.
     * @param response http response pro namapování streamu
     * @throws IOException chyba při nastovávní response chybového stavu 
     */
    @Secured(DumpRoles.DUMP)
    @RequestMapping(value = "/dump/export", method = RequestMethod.GET)
    public void exportDump(HttpServletResponse response) throws IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        response.setHeader("Content-Disposition", "attachment;filename=dump_" + dateFormat.format(new Date()) + ".zip");
        dumpHelper.exportDump(response.getOutputStream());
    }

    /**
     * Zobrazí formulář pro import dumpu.
     * @return adresa šablony
     */
    @Secured(DumpRoles.DUMP)
    @RequestMapping(value = "/dump/import", method = RequestMethod.GET)
    public String showImportDumpForm() {
        return "manta-dataflow-repository-dump/dump";
    }

    /**
     * Provede import dumpu do repository.
     * @param file soubor obsahující dump
     * @param model spring model
     * @return adresa šablony
     * @throws IOException chyba při zpracovávání souboru
     */
    @Secured(DumpRoles.DUMP)
    @RequestMapping(value = "/dump/import", method = RequestMethod.POST)
    public String importDump(@RequestParam("file") MultipartFile file, Model model) throws IOException {
        ZipFileDecompresser decompresser = new ZipFileDecompresser();
        boolean result = false;
        if (decompresser.isZipValid(file.getInputStream())) {
            result = dumpHelper.importDump(file.getInputStream());
        } else {
            LOGGER.error("The zip for the dump import  is corrupted.");
        }
        horizontalFilterProvider.refreshFilters();
        model.addAttribute("isImport", true);
        model.addAttribute("isOk", result);
        return "manta-dataflow-repository-dump/dump";
    }

    /**
     * Perform import of dump in the new format to the repository also using the new format.
     * 
     * @param file soubor obsahující dump
     * @return vysledek importu
     */
    @Secured(DumpRoles.DUMP)
    @RequestMapping(value = "/api/dump/import", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> importDumpApi(@RequestParam("file") MultipartFile file) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        ZipFileDecompresser decompresser = new ZipFileDecompresser();
        boolean result = false;

        try {
            if (decompresser.isZipValid(file.getInputStream())) {
                result = dumpHelper.importDump(file.getInputStream());
            } else {
                resultMap.put("exception", "The zip file is corrupted.");
                LOGGER.error("The zip for the dump import  is corrupted.");
            }
        } catch (IOException e) {
            resultMap.put("exception", e.getMessage());
        }

        resultMap.put("isOk", result);
        return resultMap;
    }
        
}
