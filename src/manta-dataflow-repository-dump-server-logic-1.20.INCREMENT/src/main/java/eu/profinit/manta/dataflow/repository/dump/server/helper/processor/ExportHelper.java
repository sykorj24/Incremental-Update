package eu.profinit.manta.dataflow.repository.dump.server.helper.processor;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionModel;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.dump.server.helper.ObjectType;

/**
 * Helper pro zápis elementů do nastaveného streamu..
 * @author tfechtner
 *
 */
public final class ExportHelper {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(ExportHelper.class);

    /** Výstupní stream, kam se sype dump. */
    private final NoExceptionDataOutputStream outputStream;

    /**
     * @param outputStream výstupní stream, kam se sype dump
     */
    public ExportHelper(OutputStream outputStream) {
        super();
        try {
            this.outputStream = new NoExceptionDataOutputStream(new ObjectOutputStream(outputStream));
        } catch (IOException e) {
            throw new RuntimeException("Cannot create output stream.", e);
        }
    }

    /**
     * Zapíše verzi.
     * @param version verze, která se má zapsat.
     */
    public void writeVersion(ProductVersion version) {
        outputStream.writeUTF(version.number());
    }

    /**
     * Zapise vrstvu.
     * @param layer Vrstva, ktera se ma zapsat.
     */
    public void writeLayer(Vertex layer) {
        outputStream.writeUTF(ObjectType.VERTEX.toString());
        outputStream.writeUTF(VertexType.LAYER.toString());
        outputStream.writeLong((Long) layer.getId());
        outputStream.writeUTF((String) layer.getProperty(NodeProperty.LAYER_NAME.t()));
        outputStream.writeUTF((String) layer.getProperty(NodeProperty.LAYER_TYPE.t()));
    }

    /**
     * Zapíše resource.
     * @param resource element k zapsání
     */
    public void writeResource(Vertex resource) {
        outputStream.writeUTF(ObjectType.VERTEX.toString());
        outputStream.writeUTF(VertexType.RESOURCE.toString());
        outputStream.writeLong((Long) resource.getId());
        outputStream.writeUTF((String) resource.getProperty(NodeProperty.RESOURCE_NAME.t()));
        outputStream.writeUTF((String) resource.getProperty(NodeProperty.RESOURCE_TYPE.t()));
        outputStream.writeUTF((String) resource.getProperty(NodeProperty.RESOURCE_DESCRIPTION.t()));
    }

    /**
     * Zapíše uzel.
     * @param node element k zapsání
     */
    public void writeNode(Vertex node) {
        outputStream.writeUTF(ObjectType.VERTEX.toString());
        outputStream.writeUTF(VertexType.NODE.toString());
        outputStream.writeLong((Long) node.getId());
        outputStream.writeUTF((String) node.getProperty(NodeProperty.NODE_NAME.t()));
        outputStream.writeUTF((String) node.getProperty(NodeProperty.NODE_TYPE.t()));
    }

    /**
     * Zapíše atribut.
     * @param attribute element k zapsání
     */
    public void writeAttribute(Vertex attribute) {
        outputStream.writeUTF(ObjectType.VERTEX.toString());
        outputStream.writeUTF(VertexType.ATTRIBUTE.toString());
        outputStream.writeLong((Long) attribute.getId());
        outputStream.writeUTF((String) attribute.getProperty(NodeProperty.ATTRIBUTE_NAME.t()));
        outputStream.writeObject(attribute.getProperty(NodeProperty.ATTRIBUTE_VALUE.t()));
    }

    /**
     * Zapíše revizi.
     * @param revision element k zapsání
     */
    public void writeRevision(Vertex revision) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(AbstractDumpProcessor.DATE_FORMAT);

        outputStream.writeUTF(ObjectType.VERTEX.toString());
        outputStream.writeUTF(VertexType.REVISION_NODE.toString());
        outputStream.writeLong((Long) revision.getId());
        RevisionModel model = new RevisionModel(revision);
        outputStream.writeDouble(model.getRevision());
        outputStream.writeBoolean(model.isCommitted());
        outputStream.writeUTF(dateFormat.format(model.getCommitTime()));
        
        if(model.getPreviousRevision() == null) {
            outputStream.writeDouble(-1.0);
        } else {
            outputStream.writeDouble(model.getPreviousRevision());
        }
        
        if(model.getNextRevision() == null) {
            outputStream.writeDouble(-1.0);
        } else {
            outputStream.writeDouble(model.getNextRevision());
        }
    }

    /**
     * Zapíše super root.
     * @param superRoot element k zapsání
     */
    public void writeSuperRoot(Vertex superRoot) {
        outputStream.writeUTF(ObjectType.VERTEX.toString());
        outputStream.writeUTF(VertexType.SUPER_ROOT.toString());
        outputStream.writeLong((Long) superRoot.getId());
    }

    /**
     * Zapíše revision root.
     * @param revisionRoot element k zapsání
     */
    public void writeRevisionRoot(Vertex revisionRoot) {
        outputStream.writeUTF(ObjectType.VERTEX.toString());
        outputStream.writeUTF(VertexType.REVISION_ROOT.toString());
        outputStream.writeLong((Long) revisionRoot.getId());
        outputStream.writeDouble((Double) revisionRoot.getProperty(NodeProperty.LATEST_COMMITTED_REVISION.t()));
        outputStream.writeDouble((Double) revisionRoot.getProperty(NodeProperty.LATEST_UNCOMMITTED_REVISION.t()));
    }

    /**
     * Zapíše source root.
     * @param sourceRoot element k zapsání
     */
    public void writeSourceRoot(Vertex sourceRoot) {
        outputStream.writeUTF(ObjectType.VERTEX.toString());
        outputStream.writeUTF(VertexType.SOURCE_ROOT.toString());
        outputStream.writeLong((Long) sourceRoot.getId());        
    }

    /**
     * Zapíše source node.
     * @param sourceNode element k zapsání
     * @return id souboru
     */
    public String writeSourceNode(Vertex sourceNode) {
        outputStream.writeUTF(ObjectType.VERTEX.toString());
        outputStream.writeUTF(VertexType.SOURCE_NODE.toString());
        outputStream.writeLong((Long) sourceNode.getId());
        outputStream.writeUTF((String) sourceNode.getProperty(NodeProperty.SOURCE_NODE_LOCAL.t()));
        outputStream.writeUTF((String) sourceNode.getProperty(NodeProperty.SOURCE_NODE_HASH.t()));
        String fileId = (String) sourceNode.getProperty(NodeProperty.SOURCE_NODE_ID.t());
        outputStream.writeUTF(fileId);        
        return fileId;
    }

    /**
     * Zapíše hranu.
     * @param edge element k zapsání
     */
    public void writeEdge(Edge edge) {
        outputStream.writeUTF(ObjectType.EDGE.toString());
        outputStream.writeLong((Long) edge.getVertex(Direction.OUT).getId());
        outputStream.writeLong((Long) edge.getVertex(Direction.IN).getId());
        outputStream.writeUTF(edge.getLabel());

        Double revisionStart = edge.getProperty(EdgeProperty.TRAN_START.t());
        if (revisionStart != null) {
            outputStream.writeDouble(revisionStart);
        } else {
            LOGGER.error("Edge {} does not have defined start revision.", edge.getId().toString());
            outputStream.writeDouble(-1.0);
        }

        Double revisionEnd = edge.getProperty(EdgeProperty.TRAN_END.t());
        if (revisionEnd != null) {
            outputStream.writeDouble(revisionEnd);
        } else {
            LOGGER.error("Edge {} does not have defined end revision.", edge.getId().toString());
            outputStream.writeDouble(-1.0);
        }

        Set<String> keySet = edge.getPropertyKeys();
        int propertyCount = 0;
        for (String key : keySet) {
            // transakční property jsou zapsány již zvlášť
            if (EdgeProperty.TRAN_START.t().equals(key) || EdgeProperty.TRAN_END.t().equals(key)) {
                continue;
            }
            propertyCount++;
        }

        outputStream.writeInt(propertyCount);
        for (String key : keySet) {
            // transakční property jsou zapsány již zvlášť
            if (EdgeProperty.TRAN_START.t().equals(key) || EdgeProperty.TRAN_END.t().equals(key)) {
                continue;
            }

            outputStream.writeUTF(key);
            outputStream.writeObject(edge.getProperty(key));
        }
    }

    /**
     * Flushne podkaldový stream.
     */
    public void flush() {
        try {
            outputStream.flush();
        } catch (IOException e) {
            LOGGER.error("Failed to flush outputstream.", e);
        }
    }
}
