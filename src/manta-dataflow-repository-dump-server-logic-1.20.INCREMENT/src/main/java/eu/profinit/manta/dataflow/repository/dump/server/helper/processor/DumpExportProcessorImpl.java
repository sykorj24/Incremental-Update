package eu.profinit.manta.dataflow.repository.dump.server.helper.processor;

import java.io.OutputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionLockedOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;

/**
 * Implementace procesoru provádějící export dumpu.
 * @author tfechtner
 *
 */
public class DumpExportProcessorImpl extends AbstractDumpProcessor implements DumpExportProcessor {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(DumpExportProcessorImpl.class);

    @Override
    public Set<String> processExport(final OutputStream outputStream) {
        return getRevisionRootHandler().executeRevisionReadOperation(new RevisionLockedOperation<Set<String>>() {
            @Override
            public Set<String> call() {
                ExportHelper helper = new ExportHelper(outputStream);
                helper.writeVersion(ProductVersion.CURRENT);
                helper.flush();
           
                Set<String> sourceFileIds = new HashSet<>(); 
                
                LOGGER.info("Start exporting vertices.");
                int numberOfVertices = exportVertices(helper, sourceFileIds);
                LOGGER.info("Exported {} vertices.", numberOfVertices);

                LOGGER.info("Start exporting edges.");
                int numberOfEdges = exportEdges(helper);
                LOGGER.info("Exported {} edges.", numberOfEdges);
                helper.flush();
                return sourceFileIds;
            }
        });
    }

    /**
     * Vyexportuje všechny vrcholy v grafu. 
     * @param exportHelper helper používaný pro samotný zápis elementů
     * @param sourceFileIds množina id source node souborů
     * @return počet zapsaných vrcholů
     */
    private int exportVertices(final ExportHelper exportHelper, final Set<String> sourceFileIds) {
        return getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TransactionCallback<Integer>() {

            @Override
            public String getModuleName() {
                return MergerController.MODULE_NAME;
            }

            @Override
            public Integer callMe(TitanTransaction transaction) {
                Iterator<Vertex> vertices = transaction.getVertices().iterator();
                int numberOfVertices = 0;
                while (vertices.hasNext()) {
                    Vertex v = vertices.next();

                    switch (VertexType.getType(v)) {
                    case NODE:
                        exportHelper.writeNode(v);
                        break;
                    case ATTRIBUTE:
                        exportHelper.writeAttribute(v);
                        break;
                    case RESOURCE:
                        exportHelper.writeResource(v);
                        break;
                    case REVISION_NODE:
                        exportHelper.writeRevision(v);
                        break;
                    case REVISION_ROOT:
                        exportHelper.writeRevisionRoot(v);
                        break;
                    case SUPER_ROOT:
                        exportHelper.writeSuperRoot(v);
                        break;
                    case SOURCE_ROOT:
                        exportHelper.writeSourceRoot(v);
                        break;
                    case LAYER:
                        exportHelper.writeLayer(v);
                        break;
                    case SOURCE_NODE:
                        sourceFileIds.add(exportHelper.writeSourceNode(v));
                        break;
                    default:
                        throw new IllegalStateException("Unknown type (" + VertexType.getType(v) + ") of vertex.");
                    }

                    numberOfVertices++;
                }
                return numberOfVertices;
            }
        });
    }

    /**
     * Zapíše všechny hrany v grafu.
     * @param exportHelper helper používaný pro samotný zápis elementů
     * @return počet zapsaných hran
     */
    private int exportEdges(final ExportHelper exportHelper) {
        return getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TransactionCallback<Integer>() {

            @Override
            public String getModuleName() {
                return MergerController.MODULE_NAME;
            }

            @Override
            public Integer callMe(TitanTransaction transaction) {
                Iterator<Edge> edges = transaction.getEdges().iterator();
                int numberOfEdges = 0;
                while (edges.hasNext()) {
                    exportHelper.writeEdge(edges.next());
                    numberOfEdges++;
                }
                return numberOfEdges;
            }
        });
    }
}
