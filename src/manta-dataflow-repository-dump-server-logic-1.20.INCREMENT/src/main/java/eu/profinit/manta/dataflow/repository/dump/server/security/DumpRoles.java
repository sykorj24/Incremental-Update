package eu.profinit.manta.dataflow.repository.dump.server.security;

/**
 * Seznam rolí specifických pro modul dump.
 * @author tfechtner
 *
 */
public final class DumpRoles {

    private DumpRoles() {
    }

    /**
     * Role dumpu, který upravuje repository.
     */
    public static final String DUMP = "ROLE_MERGER";
}
