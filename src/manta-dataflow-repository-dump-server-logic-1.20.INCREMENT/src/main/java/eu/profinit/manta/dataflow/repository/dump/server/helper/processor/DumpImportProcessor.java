package eu.profinit.manta.dataflow.repository.dump.server.helper.processor;

import java.util.zip.ZipInputStream;

/**
 * Rozhraní pro import dumpu.
 * @author tfechtner
 *
 */
public interface DumpImportProcessor {

    /**
     * Naimportuje dump z daného streamu.
     * @param zipInputStream vstupní zip stream obsahující data z dumpu
     */
    void processImport(ZipInputStream zipInputStream);

}
