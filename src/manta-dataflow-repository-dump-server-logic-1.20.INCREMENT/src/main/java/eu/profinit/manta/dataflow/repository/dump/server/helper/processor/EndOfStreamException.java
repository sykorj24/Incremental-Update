package eu.profinit.manta.dataflow.repository.dump.server.helper.processor;


/**
 * Výjimka značící konec stream s dumpem.
 * @author tfechtner
 *
 */
public class EndOfStreamException extends RuntimeException {

    private static final long serialVersionUID = 1512731070727427357L;

    /**
     * @param message zpráva chyby
     * @param t způsobující výjimka
     */
    public EndOfStreamException(String message, Throwable t) {
        super(message, t);
    }

}
