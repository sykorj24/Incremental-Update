package eu.profinit.manta.dataflow.repository.dump.server.helper.processor;

import org.springframework.beans.factory.annotation.Autowired;

import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;

/**
 * zpolečná implementace pro procesory pracující s dumpy.
 * @author tfechtner
 *
 */
public abstract class AbstractDumpProcessor {
    /** Formát pro export data. */
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    
    @Autowired
    private DatabaseHolder databaseHolder;

    @Autowired
    private SuperRootHandler superRootHandler;

    @Autowired
    private RevisionRootHandler revisionRootHandler;
    
    @Autowired
    private SourceRootHandler sourceRootHandler;
    
    /**
     * @return držák na databázi
     */
    public DatabaseHolder getDatabaseHolder() {
        return databaseHolder;
    }

    /**
     * @param databaseHolder držák na databázi
     */
    public void setDatabaseHolder(DatabaseHolder databaseHolder) {
        this.databaseHolder = databaseHolder;
    }

    /**
     * @return držák na super root
     */
    public SuperRootHandler getSuperRootHandler() {
        return superRootHandler;
    }

    /**
     * @param superRootHandler držák na super root
     */
    public void setSuperRootHandler(SuperRootHandler superRootHandler) {
        this.superRootHandler = superRootHandler;
    }

    /**
     * @return držák na revision root
     */
    public RevisionRootHandler getRevisionRootHandler() {
        return revisionRootHandler;
    }

    /**
     * @param revisionRootHandler držák na revision root
     */
    public void setRevisionRootHandler(RevisionRootHandler revisionRootHandler) {
        this.revisionRootHandler = revisionRootHandler;
    }

    /**
     * @return držák na source root
     */
    public SourceRootHandler getSourceRootHandler() {
        return sourceRootHandler;
    }

    /**
     * @param sourceRootHandler držák na source root
     */
    public void setSourceRootHandler(SourceRootHandler sourceRootHandler) {
        this.sourceRootHandler = sourceRootHandler;
    }
}
