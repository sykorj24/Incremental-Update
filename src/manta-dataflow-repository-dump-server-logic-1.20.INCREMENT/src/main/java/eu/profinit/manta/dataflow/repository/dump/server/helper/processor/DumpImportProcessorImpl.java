package eu.profinit.manta.dataflow.repository.dump.server.helper.processor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionLockedOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.dump.server.helper.processor.ImportTransactionProcessor.ImportAction;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.ProcessorResult;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.ProcessorResult.ActionCounter;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.ProcessorResult.ActionType;

/**
 * Implementace pro import dumpu.
 * @author tfechtner
 *
 */
public class DumpImportProcessorImpl extends AbstractDumpProcessor implements DumpImportProcessor {
    /** SLF4J logger.*/
    static final Logger LOGGER = LoggerFactory.getLogger(DumpImportProcessorImpl.class);
    /** Počet objektů na jeden commit. */
    public static final int MAXIMUM_OBJECTS_PER_TRAN = 500;

    @Override
    public void processImport(final ZipInputStream zipInputStream) {
        getRevisionRootHandler().executeRevisionWriteOperation(new RevisionLockedOperation<Object>() {
            @Override
            public Object call() {
                LOGGER.info("Starting import of repository data.");
                importData(zipInputStream);
                setAdditionalData();
                LOGGER.info("Import of repository data finished.");
                return null;
            }
        });
    }

    /**
     * Naimportuje data ze vstupního zip streamu.
     * @param zipInputStream zip stream obsahující dump
     */
    private void importData(ZipInputStream zipInputStream) {
        NoExceptionDataInputStream inputStream = null;

        try {
            inputStream = new NoExceptionDataInputStream(new ObjectInputStream(zipInputStream));
        } catch (IOException e) {
            throw new RuntimeException("Cannot create outpu stream.", e);
        }

        // Overime kompatibilitu vezi
        ProductVersion importedVersion = ProductVersion.ofNumber(inputStream.readUTF());
        if (!ProductVersion.checkCompatibility(importedVersion)) {
            // v jiných případech není třeba zavírat, protože o zip stream se stará tvůrce zip streamu
            IOUtils.closeQuietly(inputStream);
            throw new IllegalArgumentException("Current version '" + ProductVersion.CURRENT + "' is not compatible with imported version '" + importedVersion + "'.");
        }

        LOGGER.info("Cleaning repository.");
        cleanDatabase();
        LOGGER.info("Importing repository data.");
        LOGGER.info("Curent version: " + ProductVersion.CURRENT);
        LOGGER.info("Imported version: " + importedVersion);

        ImportTransactionProcessor transactionProcessor = new ImportTransactionProcessor(zipInputStream, inputStream,
                getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler(), importedVersion);
        int processedObjects = 0;
        do {
            processedObjects = getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE,
                    transactionProcessor);
        } while (processedObjects > 0);

        ProcessorResult<ImportAction> importedObjects = transactionProcessor.getImportedObjects();
        checkConsistency(importedObjects);
        LOGGER.info("Imported vertices: {}.", importedObjects.getVertexActions());
        LOGGER.info("Imported edges: {}.", importedObjects.getEdgeActions());
    }

    /**
     * Ověří konzistenci na základě počtu nahraných elementů.
     * @param importedObjects struktura s počty importovaných elementů
     */
    private void checkConsistency(ProcessorResult<ImportAction> importedObjects) {
        if (importedObjects == null) {
            LOGGER.error("Object with informations about imported elements must not be null.");
            return;
        }

        Map<ActionType<ImportAction, VertexType>, ActionCounter> vertexActions = importedObjects.getVertexActions();
        Map<ActionType<ImportAction, EdgeLabel>, ActionCounter> edgeActions = importedObjects.getEdgeActions();

        int attributes = getElementNumber(vertexActions, VertexType.ATTRIBUTE);
        int attributeEdges = getElementNumber(edgeActions, EdgeLabel.HAS_ATTRIBUTE);
        if (attributes != attributeEdges) {
            LOGGER.error("The number of attributes ({}) and hasAttribute edges ({}) are not same.", attributes,
                    attributeEdges);
        }

        int resources = getElementNumber(vertexActions, VertexType.RESOURCE);
        int nodes = getElementNumber(vertexActions, VertexType.NODE);
        int resourceEdges = getElementNumber(edgeActions, EdgeLabel.HAS_RESOURCE);
        int parentEdges = getElementNumber(edgeActions, EdgeLabel.HAS_PARENT);

        if (resources > resourceEdges) {
            LOGGER.error("The number of resources ({}) is greater than the number of hasResource edges ({}).",
                    resources, resourceEdges);
        }
        int restOfHierarchyEdges = parentEdges + resourceEdges - resources;
        if (nodes > restOfHierarchyEdges) {
            LOGGER.error("The number of nodes ({}) is greater than the rest of hierarchy edges ({}).", resources,
                    restOfHierarchyEdges);
        }

        int revisions = getElementNumber(vertexActions, VertexType.REVISION_NODE);
        if (revisions == 0) {
            LOGGER.error("No revision has been created.");
            // vytvorit alespon technickou revizi
            GraphCreation.createTechnicalRevision(getDatabaseHolder(), getRevisionRootHandler());
        }

        int revisionEdges = getElementNumber(edgeActions, EdgeLabel.HAS_REVISION);
        if (revisions != revisionEdges) {
            LOGGER.error("The number of revisions ({}) and hasRevision edges ({}) are not same.", revisions,
                    revisionEdges);
        }

        int sourceNodes = getElementNumber(vertexActions, VertexType.SOURCE_NODE);
        int sourceNodesEdges = getElementNumber(edgeActions, EdgeLabel.HAS_SOURCE);
        if (sourceNodes != sourceNodesEdges) {
            LOGGER.error("The number of source nodes ({}) and hasSource edges ({}) are not same.", sourceNodes,
                    sourceNodesEdges);
        }
    }

    /**
     * Zjistí počet nahraných elementů z dané mapy.
     * @param actions mapa uskutečněných akcí
     * @param elementType typ elementu, který se má získat
     * @return počet nahraných elementů daného typu
     */
    private <T extends Enum<?>> int getElementNumber(Map<ActionType<ImportAction, T>, ActionCounter> actions,
            T elementType) {
        ActionCounter counter = actions.get(new ActionType<ImportAction, T>(ImportAction.ADD, elementType));
        if (counter != null) {
            return counter.getCount();
        }
        return 0;
    }

    /**
     * Vyčistí databázi od starých dat.
     */
    private void cleanDatabase() {
        getDatabaseHolder().truncateDatabase();
        GraphCreation.initDatabaseWithoutRevision(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());
    }
    
    /**
     * Set additional data that were missing in the dump file from the old model but have to be present in the new model.
     * 
     * In the revision tree are missing values of new properties of revision root (latestCommittedRevision and latestUncommittedRevision)
     * and values of new properties of revision node (previousRevision and nextRevision).
     */
    private void setAdditionalData() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TransactionCallback<Object>() {

            @Override
            public String getModuleName() {
                return "dump_controller";
            }

            @Override
            public Object callMe(TitanTransaction transaction) {
                // Get revision root
                Vertex revisionRoot = getRevisionRootHandler().getRoot(transaction);

                // Get all revision nodes
                List<Vertex> revisionNodes = GraphOperation.getAdjacentVertices(revisionRoot, Direction.OUT,
                        RevisionRootHandler.EVERY_REVISION_INTERVAL, EdgeLabel.HAS_REVISION);

                // Sort revision nodes in the ascending order by their revision number
                try {
                    Collections.sort(revisionNodes, new RevisionNodeComparator());
                } catch (IllegalArgumentException e) {
                    LOGGER.error("Error while sorting revision nodes. Detailed message: {}", e.getMessage());
                    return null;
                }

                Vertex latestRevisionNode = null;

                // Set revision node missing properties (previous revision and next revision)
                for (int i = 0; i < revisionNodes.size(); i++) {
                    Vertex revisionNode = revisionNodes.get(i);

                    if (i == 0) {
                        // First revision node has no previous revision
                        revisionNode.setProperty(NodeProperty.REVISION_NODE_PREVIOUS_REVISION.t(), -1.0);
                    } else {
                        Vertex previousRevisionNode = revisionNodes.get(i - 1);
                        Double previousRevisionNumber = previousRevisionNode
                                .getProperty(NodeProperty.REVISION_NODE_REVISION.t());
                        revisionNode.setProperty(NodeProperty.REVISION_NODE_PREVIOUS_REVISION.t(),
                                previousRevisionNumber);
                    }

                    if (i == revisionNodes.size() - 1) {
                        // Latest revision node has no next revision
                        revisionNode.setProperty(NodeProperty.REVISION_NODE_NEXT_REVISION.t(), -1.0);
                        latestRevisionNode = revisionNode;
                    } else {
                        Vertex nextRevisionNode = revisionNodes.get(i + 1);
                        Double nextRevisionNumber = nextRevisionNode
                                .getProperty(NodeProperty.REVISION_NODE_REVISION.t());
                        revisionNode.setProperty(NodeProperty.REVISION_NODE_NEXT_REVISION.t(), nextRevisionNumber);
                    }

                }
                
                // Prepare properties for revision root
                Double latestCommittedRevisionNumber;
                Double latestUncommittedRevisionNumber;
                Boolean isCommitted = latestRevisionNode.getProperty(NodeProperty.REVISION_NODE_COMMITTED.t()); 
                if(isCommitted) {
                    latestCommittedRevisionNumber = latestRevisionNode.getProperty(NodeProperty.REVISION_NODE_REVISION.t());
                    latestUncommittedRevisionNumber = -1.0;
                } else {
                    latestCommittedRevisionNumber = latestRevisionNode.getProperty(NodeProperty.REVISION_NODE_PREVIOUS_REVISION.t());
                    latestUncommittedRevisionNumber = latestRevisionNode.getProperty(NodeProperty.REVISION_NODE_REVISION.t());
                }
                
                // Set revision root missing properties (latest committed revision and latest uncommitted revision)
                revisionRoot.setProperty(NodeProperty.LATEST_COMMITTED_REVISION.t(), latestCommittedRevisionNumber);
                revisionRoot.setProperty(NodeProperty.LATEST_UNCOMMITTED_REVISION.t(), latestUncommittedRevisionNumber);

                return null;
            }
        });
    }

    /**
     * Comparator for sorting revision nodes by their revision numbers.
     * 
     * @author jsykora
     *
     */
    private class RevisionNodeComparator implements Comparator<Vertex> {

        @Override
        public int compare(Vertex revisionNode1, Vertex revisionNode2) {
            if (!VertexType.REVISION_NODE.equals(VertexType.getType(revisionNode1))) {
                throw new IllegalArgumentException(
                        "Wrong argument. Vertex has to be of type REVISION_NODE. (vertex ID = {})"
                                + revisionNode1.getId());
            } else if (!VertexType.REVISION_NODE.equals(VertexType.getType(revisionNode2))) {
                throw new IllegalArgumentException(
                        "Wrong argument. Vertex has to be of type REVISION_NODE. (vertex ID = {})"
                                + revisionNode2.getId());
            }

            Double revisionNumber1 = revisionNode1.getProperty(NodeProperty.REVISION_NODE_REVISION.t());
            Double revisionNumber2 = revisionNode2.getProperty(NodeProperty.REVISION_NODE_REVISION.t());

            if (revisionNumber1 == null) {
                throw new NullArgumentException(
                        "Revision number of vertex (vertex ID ={})  is null" + revisionNode1.getId());
            } else if (revisionNumber2 == null) {
                throw new NullArgumentException(
                        "Revision number of vertex (vertex ID ={})  is null" + revisionNode2.getId());
            }

            return revisionNumber1.compareTo(revisionNumber2);

        }

    }
}
