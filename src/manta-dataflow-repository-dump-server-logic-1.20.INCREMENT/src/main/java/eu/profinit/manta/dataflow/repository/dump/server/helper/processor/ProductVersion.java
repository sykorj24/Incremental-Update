package eu.profinit.manta.dataflow.repository.dump.server.helper.processor;

import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.lang3.Validate;

/**
 * Verze produktu.
 * Pozor - pri rozsirovani ciselniku je nutne dodrzet poradi verzi od nejstarsi po nejnovejsi!
 * 
 * @author onouza
 */
public enum ProductVersion {
 
    /** Verze 1.10 */
    V_1_10("1.10"),
    /** Verze 1.17 */
    V_1_17("1.17"),
    /** Version 1.22 */
    V_1_22("1.22");
    
    /**
     * Aktuali verze produktu
     */
    public static final ProductVersion CURRENT = V_1_22;
    /**
     * Textova reprezenace cisla verze
     */
    private String rawNumber;
    
    /**
     * Konstruktor.
     * @param number Textova reprezenace cisla verze
     */
    private ProductVersion(String number) {
        this.rawNumber = number;
    }
    
    /**
     * Vrati verzi produktu s danou textovou reprezenaci.
     * @param number Textova reprezenace hledane verze.
     * @return Verze produktu s danou textovou reprezenaci.
     * @throws IllegalArgumentException Pokud verze s danou textovou reprezentaci nefunguje
     */
    public static ProductVersion ofNumber(String number) {
        for (ProductVersion productVersion : ProductVersion.values()) {
            if (productVersion.number().equals(number)) {
                return productVersion;
            }
        }
        throw new IllegalArgumentException("Unknown version numner: " + number);
    }
    
    /**
     * @return Textova reprezenace cisla verze
     */
    public String number() {
        return rawNumber;
    }
    
    /**
     * Zjisti, zda je aktualni verze kompatibilni s danou verzi.
     * @param fromVersion Zkoumana verze.
     * @return {@code true}, pokud je aktualni verze kompatibilni s danou verzi, jinak {@code false}.
     */
    public static boolean checkCompatibility(ProductVersion fromVersion) {
        return checkCompatibility(fromVersion, ProductVersion.CURRENT);
    }

    /**
     * Zjisti, zda jsou dane verze kompatibilni.
     * @param fromVersion Verze, vuci ktere je kompatibilita zkoumana.
     * @param toVersion Verze, jejiz kompatibilita je zkoumana.
     * @return {@code true}, pokud jsou dane verze kompatibilni, jinak {@code false}.
     */
    public static boolean checkCompatibility(ProductVersion fromVersion, ProductVersion toVersion) {
        Validate.notNull(fromVersion, "'fromVersion' must not be null");
        Validate.notNull(toVersion, "'toVersion' must not be null");
        int from = fromVersion.ordinal();
        int to = toVersion.ordinal();
        return from <= to;
    }

    /**
     * Zjisti rozdily mezi danou a aktualni vezi.
     * @param fromVersion Verze, vuci kere se porovnava aktualni.
     * @return Seznam rozdilu mezi danou a aktualni vezi. 
     * @throws IllegalArgumentException pokud je dana veze vyssi nez aktualni.
     */
    public static Set<Delta> diff(ProductVersion fromVersion) {
        return diff(fromVersion, ProductVersion.CURRENT);
    }
    
    /**
     * Zjisti rozdily mezi dvema verzemi.
     * @param fromVersion Starsi verze
     * @param toVersion Novejsi veze
     * @return Seznam rozdilu mezi danymi verzemi.
     * @throws IllegalArgumentException pokud je starsi veze vyssi nez novejsi.
     */
    public static Set<Delta> diff(ProductVersion fromVersion, ProductVersion toVersion) {
        Validate.notNull(fromVersion, "'fromVersion' must not be null");
        Validate.notNull(toVersion, "'toVersion' must not be null");
        Set<Delta> result = new LinkedHashSet<>();
        int from = fromVersion.ordinal();
        int to = toVersion.ordinal();
        if (to < from) {
            throw new IllegalArgumentException("'fromVersion' must be less or equal 'toVersion'.");
        }
        // Projdeme postupne jednotlive veze  
        for (int i = from; i < to; i++) {
           //  a zjistime veskere rozdily oproti kazde novejsi
            for (int j = i + 1; j <= to; j++) {
                result.addAll(Delta.ofVersions(ProductVersion.values()[i], ProductVersion.values()[j]));
            } 
        }
        // Posbirane rozdily vratime
        return result;
    }
    
    @Override
    public String toString() {
        return number();
    }

    /**
     * Rozdily mezi verzemi.
     * Prvni prorvnavana verze je starsi, druha novejsi.
     * @author onouza
     */
    public enum Delta {
        /** Podpora vrstev */
        LAYERS(V_1_10, V_1_17),
        /** Support of revision root properties latestCommittedRevision and latestUncommittedRevision */
        REVISION_ROOT_PROPERTIES(V_1_17, V_1_22),
        /** Support of revision node properties previousRevision and nextRevision */
        REVISION_NODE_PROPERTIES(V_1_17, V_1_22),
        /** Support of revision numbers in Double data type (instead of Integer) */
        DOUBLE_REVISION_NUMBER(V_1_17, V_1_22);
        
        /** Starsi veze */
        private ProductVersion fromVersion;
        /** Novejsi veze */
        private ProductVersion toVersion;

        /**
         * Konstruktor.
         * @param fromVersion Starsi verze.
         * @param toVersion Novejsi verze.
         */
        private Delta(ProductVersion fromVersion, ProductVersion toVersion) {
            this.fromVersion = fromVersion;
            this.toVersion = toVersion;
        }
        
        /**
         * Vrati prime rozdily mezi danymi verzemi. Napr. pokud posloupnost verzi je A-B-C
         * a ptame se na rozdily mezi A a C, rozdily mezi A a B ani B a C ve vysledku NEbudou.
         * @param fromVersion Starsi verze
         * @param toVersion Novejsi verze
         * @return Prime rozdily mezi danymi verzemi. 
         */
        private static Set<Delta> ofVersions(ProductVersion fromVersion, ProductVersion toVersion) {
            Set<Delta> result = new LinkedHashSet<>();
            for (Delta delta : values()) {
                if (delta.fromVersion.equals(fromVersion)
                        && delta.toVersion.equals(toVersion)) {
                    result.add(delta);
                }
            }
            return result;
        } 
    }
}
