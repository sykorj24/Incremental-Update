package eu.profinit.manta.dataflow.repository.dump.server.helper;

/**
 * Typ objektu v dumpu.
 * @author tfechtner
 *
 */
public enum ObjectType {
    /** Databázový vrchol. */
    VERTEX, 
    /** Databázová hrana. */
    EDGE;
}
