package eu.profinit.manta.dataflow.repository.dump.server.helper.processor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.zip.ZipInputStream;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;

/**
 * Helper pro export a import dump source code souborů.
 * @author tfechtner
 *
 */
public class SourceCodeDumpHelper {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SourceCodeDumpHelper.class);

    /** Držák na source root. */
    @Autowired
    private SourceRootHandler sourceRootHandler;

    private File backUpDir = new File("backup");

    /**
     * Exportuje soubory do zipu.
     * @param zipStream zip stream, kam se soubory mají sypat
     * @param sourceFileIds množina idéček souborů, které se mají poslat do zipu
     */
    public void exportSources(ZipArchiveOutputStream zipStream, Set<String> sourceFileIds) {
        Validate.notNull(sourceRootHandler, "The source root handler must be defined.");
        if (sourceFileIds == null || sourceFileIds.isEmpty()) {
            LOGGER.info("There was not any source code file to export.");
            return;
        }

        LOGGER.info("Starting exporting source code files.");
        int exportedFiles = 0;
        for (String fileId : sourceFileIds) {
            File file = new File(sourceRootHandler.getSourceCodeDir(), fileId);
            if (!file.exists()) {
                LOGGER.debug("The source code file {} does not exist.", fileId);
                continue;
            }

            InputStream inputStream = null;
            try {
                inputStream = new FileInputStream(file);
                ZipArchiveEntry zipEntry = new ZipArchiveEntry(fileId);
                zipStream.putArchiveEntry(zipEntry);
                IOUtils.copy(inputStream, zipStream);
                zipStream.closeArchiveEntry();
                exportedFiles++;
            } catch (IOException e) {
                LOGGER.error("IO error during compressing source code file " + fileId + ".", e);
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
        }
        LOGGER.info("Exported {} source code files.", exportedFiles);
    }

    /**
     * Naimportuje jeden source code file ze streamu.
     * @param zipInputStream vstupní stream
     * @param fileId id souboru, který se importuje
     * @return true, jestliže se import zdařil
     */
    public boolean importSourceFile(ZipInputStream zipInputStream, String fileId) {
        Validate.notNull(sourceRootHandler, "The source root handler must be defined.");

        File outputFile = new File(sourceRootHandler.getSourceCodeDir(), fileId);
        if (outputFile.exists()) {
            LOGGER.error("Source code file with id {} already exists.", fileId);
            return false;
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(outputFile);
            IOUtils.copy(zipInputStream, fos);

        } catch (IOException e) {
            LOGGER.error("IO error during importing source code file " + fileId + ".", e);
            return false;
        } finally {
            IOUtils.closeQuietly(fos);
        }

        return true;
    }

    /**
     * Provede backup source node souborů.
     */
    public void backUp() {
        Validate.notNull(backUpDir, "Back up dir must not be null.");
        if (backUpDir.exists()) {
            try {
                FileUtils.forceDelete(backUpDir);
            } catch (IOException e) {
                LOGGER.error("Cannot delete old back up dir.", e);
            }
        }
        if (sourceRootHandler.getSourceCodeDir().exists()
                && !sourceRootHandler.getSourceCodeDir().renameTo(backUpDir)) {
            LOGGER.error("Failed to back up the source code directory.");
            return;
        }

        try {
            FileUtils.forceMkdir(sourceRootHandler.getSourceCodeDir());
        } catch (IOException e) {
            LOGGER.error("Cannot prepare source code dir for import.", e);
        }
    }

    /**
     * Smaže backup.
     */
    public void deleteBackUp() {
        Validate.notNull(backUpDir, "Back up dir must not be null.");
        try {
            FileUtils.forceDelete(backUpDir);
        } catch (IOException e) {
            LOGGER.warn("Cannot delete back up dir.", e);
        }
    }

    /**
     * Rollbackne původní source code před import.
     */
    public void rollback() {
        Validate.notNull(backUpDir, "Back up dir must not be null.");
        try {
            FileUtils.forceDelete(sourceRootHandler.getSourceCodeDir());
        } catch (IOException e) {
            LOGGER.error("Cannot delete source code dir, which should be rollbacked.", e);
        }
        if (backUpDir.exists() && !backUpDir.renameTo(sourceRootHandler.getSourceCodeDir())) {
            LOGGER.error("Failed to roll back the source code directory.");
        }

    }

    /**
     * @return Držák na source root. 
     */
    public SourceRootHandler getSourceRootHandler() {
        return sourceRootHandler;
    }

    /**
     * @param sourceRootHandler Držák na source root. 
     */
    public void setSourceRootHandler(SourceRootHandler sourceRootHandler) {
        this.sourceRootHandler = sourceRootHandler;
    }

    /**
     * @return back up adresáře pro source code soubory
     */
    public File getBackUpDir() {
        return backUpDir;
    }

    /**
     * @param backUpDir back up adresáře pro source code soubory
     */
    public void setBackUpDir(File backUpDir) {
        this.backUpDir = backUpDir;
    }

}
