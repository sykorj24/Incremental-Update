package eu.profinit.manta.dataflow.repository.exporter.server.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.exporter.server.helper.ExportHelper;
import eu.profinit.manta.dataflow.repository.exporter.server.security.ExporterRoles;
import eu.profinit.manta.dataflow.repository.exporter.server.visitor.ColumnTypeVisitor;
import eu.profinit.manta.dataflow.repository.exporter.server.visitor.ExporterVisitor;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;
import eu.profinit.manta.platform.web.core.security.SecurityHelper;

/**
 * Kontroler pro exportování.
 * @author tfechtner
 *
 */
@Controller
public class ExportController {
    /** Název akce pro usage stats pro export celého repository. */
    private static final String FLOW_REPOSITORY_EXPORT = "flow_repository_export";
    /** Název exporter modulu. */
    public static final String MODULE_NAME = "mr_exporter";
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(ExportController.class);

    @Autowired
    private ExportHelper exportHelper;

    @Autowired
    private DatabaseHolder databaseHolder;

    @Autowired
    private SuperRootHandler superRootHandler;

    @Autowired
    private RevisionRootHandler revisionRootHandler;

    @Autowired
    private TechnicalAttributesHolder technicalAttributesHolder;
    
    @Autowired
    private UsageStatsCollector usageStatsCollector;

    /**
     * Exportuje graf a pošle klientovi výsledek jako csv souboru.
     * @param response response objekt pro odeslání odpovědi
     * @throws IOException chyba při přesměrování
     */
    @Secured(ExporterRoles.EXPORTER)
    @RequestMapping(value = "/api/export", method = RequestMethod.GET)
    public void exportGraph(HttpServletResponse response) throws IOException {
        Double revision = revisionRootHandler.getHeadNumber(databaseHolder);
        if (revision != null) {
            ExporterVisitor visitor = new ExporterVisitor(technicalAttributesHolder);
            RevisionInterval revisionInterval = new RevisionInterval(revision, revision);
            
            File tempFile = exportHelper.generateExportFile(databaseHolder, superRootHandler, visitor, MODULE_NAME,
                    revisionInterval);
            exportHelper.sendExportFile(tempFile, response);
            
            if (!FileUtils.deleteQuietly(tempFile)) {
                LOGGER.warn("Cannot delete the temporary file.");
            }
            
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("revision", revision);
            params.put("vertices", visitor.getVertexTypeCounts());
            params.put("edges", visitor.getEdgeTypeCounts());            
            usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REPOSITORY_EXPORT, params);
        } else {
            response.sendError(HttpStatus.SC_NO_CONTENT, RevisionRootHandler.NO_REVISION_IN_REP);
        }
    }

    /**
     * Exportuje datové typy sloupců.
     * @param response response objekt pro odeslání odpovědi
     * @throws IOException chyba při přesměrování
     */
    @Secured(ExporterRoles.EXPORTER)
    @RequestMapping(value = "/api/export-types", method = RequestMethod.GET)
    public void exportColumnTypes(final HttpServletResponse response) throws IOException {
        final Double revision = revisionRootHandler.getHeadNumber(databaseHolder);
        if (revision != null) {
            databaseHolder.runInTransaction(TransactionLevel.READ, new TransactionCallback<Object>() {
                @Override
                public Object callMe(TitanTransaction transaction) {
                    RevisionInterval revisionInterval = new RevisionInterval(revision, revision);
                    File tempFile = exportHelper.generateExportFile(databaseHolder, superRootHandler,
                            new ColumnTypeVisitor(transaction, revisionInterval), MODULE_NAME, revisionInterval);

                    exportHelper.sendExportFile(tempFile, response);
                    if (!FileUtils.deleteQuietly(tempFile)) {
                        LOGGER.warn("Cannot delete the temporary file.");
                    }

                    return null;
                }

                @Override
                public String getModuleName() {
                    return MODULE_NAME;
                }
            });
        } else {
            response.sendError(HttpStatus.SC_NO_CONTENT, RevisionRootHandler.NO_REVISION_IN_REP);
        }
    }
}
