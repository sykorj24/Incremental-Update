package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Iterator;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.connector.http.client.HttpClientProvider;
import eu.profinit.manta.connector.http.client.SelfSignedHttpsProvider;
import eu.profinit.manta.connector.http.login.LoginHelper;
import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeMapping;
import eu.profinit.manta.platform.automation.MantaVersion;
import eu.profinit.manta.platform.automation.ScenarioFailedException;

/**
 * Abstraktni predek mergeru grafu.
 * Konkretni implementace urcuji, na jake urovni merge probehne.
 * 
 * @author onouza
 *
 */
public abstract class AbstractMergerWriter {

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractMergerWriter.class);
    /** Soubor obsahující verzi aplikace. */
    private static final String APPLICATION_VERSION_FILE = "/manta-dataflow-repository-merger-client/app-version.txt";
    /** Čítač, který se používá pro jména výstupních souborů. */
    private int outputFileIncrementer = 0;
    /** Adresa, kam se pošle POST request se zpracovanými daty. */
    private URI outputUrl;
    /** Objekt pro přihlášení do repository. */
    private LoginHelper loginHelper;
    /** Provider vytvářející http clienta.*/
    private HttpClientProvider httpClientProvider = new SelfSignedHttpsProvider();
    /** Http client používaný pro připojení do metadata repository. */
    private HttpClient httpClient;
    /** Jmeno souboru, do nejž se zapisují revize. */
    private String revisionFilename;
    /** Služba starající se o poslání source code na server. */
    private SourceCodeService sourceCodeService;
    /** Výstupní adresář, kam se zapisují zpracované csv.*/
    private String outputDir;
    /** Verze klienta. */
    private MantaVersion mantaVersion;

    public AbstractMergerWriter() {
        InputStream is = this.getClass().getResourceAsStream(APPLICATION_VERSION_FILE);
        if (is == null) {
            LOGGER.error("File with application version does not exist.");
            return;
        }

        StringWriter versionWriter = new StringWriter();
        try {
            IOUtils.copy(is, versionWriter, StandardCharsets.UTF_8);
        } catch (IOException e) {
            LOGGER.error("Error during reading file with application version.", e);
            return;
        }
        mantaVersion = MantaVersion.createVersionFromText(versionWriter.toString());
    }

    /**
     * Zapíše výstup výstupní byte array do souboru. 
     * Jméno výstupního souboru se postupně inkrementuje s každým dalším zpracovaným vstupem.
     * Výstupní adresář se bere z member field {@link #outputDir}, nesmí být null, pak se nic nevypíše.
     * @param byteArray pole bytů obsahující data k uložení
     */
    protected void saveToFile(byte[] byteArray) {
        if (outputDir != null) {
            FileOutputStream fos = null;
            String outputName = null;
            try {
                outputName = outputDir + "/output" + outputFileIncrementer + ".csv";
                outputFileIncrementer++;
                fos = new FileOutputStream(outputName);
                fos.write(byteArray);
            } catch (IOException e) {
                LOGGER.error("Cannot create output csv: " + outputName, e);
            } finally {
                IOUtils.closeQuietly(fos);
            }
        }
    }

    /**
     * Pošle data na server ve formě POST requestu.
     * Cílová adresa se beze z member fieldu {@link #outputUrl}, pouze pokud není null. 
     * Pokud je {@link #outputUrl} null, tak nedojde k žádné aktivitě.
     * @param byteArray posílaná data
     * @param revision číslo revize
     */
    protected void sendToServer(byte[] byteArray, Double revision) {
        if (outputUrl != null) {
            HttpClient client = getHttpClient();
            HttpPost post = null;

            try {
                post = new HttpPost(outputUrl);

                if (loginHelper == null) {
                    throw new ScenarioFailedException("Login helper must be set.");
                }
                if (!loginHelper.authenticate(client, post)) {
                    LOGGER.error("Authentication failed.");
                    return;
                }

                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create().addPart("file",
                        new ByteArrayBody(byteArray, "file"));

                if (revision != null) {
                    entityBuilder.addTextBody("revision", revision.toString());
                }
                if (mantaVersion != null) {
                    entityBuilder.addTextBody("mantaVersion", mantaVersion.getStringVersion());
                }

                HttpEntity reqEntity = entityBuilder.build();
                post.setEntity(reqEntity);
                try {
                    LOGGER.info("Sending data to server {}.", outputUrl);
                    HttpResponse response = client.execute(post);

                    int responseStatusCode = response.getStatusLine().getStatusCode();
                    String responseMessage = EntityUtils.toString(response.getEntity());

                    if (responseStatusCode == HttpStatus.SC_OK) {
                        JsonNode responseNode = parseResponse(responseMessage);
                        int requestedSources = resolveRequestedSourceCodes(responseNode);
                        LOGGER.info("Response from server: {}. ", simplifyResponse(responseMessage, requestedSources));
                    } else if (responseStatusCode == HttpStatus.SC_NOT_FOUND) {
                        throw new ScenarioFailedException("Server-side of merger does not exist on target address.");
                    } else if (responseStatusCode == HttpStatus.SC_BAD_REQUEST) {
                        throw new ScenarioFailedException(
                                "Bad format of request with response content " + responseMessage + ".");
                    } else if (responseStatusCode == HttpStatus.SC_METHOD_NOT_ALLOWED) {
                        throw new ScenarioFailedException("This type of http method is not supported.");
                    } else if (responseStatusCode == HttpStatus.SC_PAYMENT_REQUIRED) {
                        throw new ScenarioFailedException(MessageFormat.format("Error response from server ({0}): {1}", responseStatusCode, responseMessage));
                    } else {
                        LOGGER.error("Error response from server - the response code {} and contente {}.",
                                responseStatusCode, responseMessage);
                    }
                } catch (ClientProtocolException e) {
                    LOGGER.error("Error ClientProtocolException", e);
                } catch (IOException e) {
                    LOGGER.error("Error IOException", e);
                    throw new ScenarioFailedException("Error during sending file to the server.", e);
                }
            } finally {
                if (post != null) {
                    post.releaseConnection();
                }
            }
        }
    }
    
    // pomocne privatni metody
    
    /**
     * Naparsuje odpověď ze serveru.
     * @param responseMessage zpráva od serveru
     * @return json uzel nebo null při chybě
     */
    private JsonNode parseResponse(String responseMessage) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(responseMessage);
        } catch (JsonParseException e) {
            LOGGER.error("Error during parsing response: " + responseMessage + ". ", e);
            return null;
        } catch (JsonMappingException e) {
            LOGGER.error("Error during mapping response: " + responseMessage + ". ", e);
            return null;
        } catch (IOException e) {
            LOGGER.error("IO Error during resolving response: " + responseMessage + ". ", e);
            return null;
        }
    }

    /**
     * Vyřeší požadované source codes ze serveru.
     * @param responseNode json uzel reprezentující celou odpověď
     * @return počet vyžádaných source code
     */
    private int resolveRequestedSourceCodes(JsonNode responseNode) {
        JsonNode reportNode = responseNode.get("processingReport");
        if (reportNode == null) {
            LOGGER.warn("The response doesn't contain processing report.");
            return 0;
        }

        JsonNode sourcesNode = reportNode.get("requestedSourceCodes");
        if (sourcesNode == null) {
            LOGGER.warn("The processing report doesn't contain requested source codes.");
            return 0;
        }

        Iterator<JsonNode> elementIter = sourcesNode.getElements();
        int requestedSources = 0;
        while (elementIter.hasNext()) {
            JsonNode element = elementIter.next();

            JsonNode localId = element.get("localId");
            JsonNode dbId = element.get("dbId");

            if (localId != null && dbId != null) {
                sourceCodeService.planUpload(new SourceCodeMapping(localId.asText(), dbId.asText()));
                requestedSources++;
            }
        }

        return requestedSources;
    }

    /**
     * Nahradí množinu požadovaných source code za jejich počet.
     * @param responseMessage odpověď od serveru
     * @param requestedSources počet požadovaných source code
     * @return zjednodušená odpověď
     */
    private String simplifyResponse(String responseMessage, int requestedSources) {
        return responseMessage.replaceFirst("\"requestedSourceCodes\":\\[.*\\]",
                "\"requestedSourceCodes\":" + String.valueOf(requestedSources));
    }
    
    // gettery / settery
    
    /**
     * @return Jmeno souboru, do nejž se zapisují revize.
     */
    public String getRevisionFilename() {
        return revisionFilename;
    }

    /**
     * @param revisionFilename Jmeno souboru, do nejž se zapisují revize.
     */
    public void setRevisionFilename(String revisionFilename) {
        this.revisionFilename = revisionFilename;
    }

    /**
     * @return Provider vytvářející http clienta.
     */
    public HttpClientProvider getHttpClientProvider() {
        return httpClientProvider;
    }

    /**
     * @param httpClientProvider Provider vytvářející http clienta.
     */
    public void setHttpClientProvider(HttpClientProvider httpClientProvider) {
        this.httpClientProvider = httpClientProvider;
    }

    /**
     * Přečtě číslo revize ze souboru a vrátí jej.
     * @return číslo revize
     */
    protected Double readRevisionFromFile() {
        return RevisionFileUtils.readRevisionFromFile(revisionFilename);
    }

    /**
     * @param outputDir výstupní adresář pro csv
     */
    public void setOutputDir(String outputDir) {
        this.outputDir = outputDir;
    }

    /**
     * Lazy-load getter pro http client. <br>
     * Pokud je client ještě null, instaciuje jej a přihlásí.  
     * @return http client do repository
     */
    private synchronized HttpClient getHttpClient() {
        if (httpClient == null) {
            httpClient = httpClientProvider.createHttpClient();
        }
        return httpClient;
    }

    /**
     * @param outputUrl adresa, kam se má poslat výstup
     */
    public void setOutputUrl(URI outputUrl) {
        this.outputUrl = outputUrl;
    }

    /**
     * @param loginHelper Objekt pro přihlášení do repository.
     */
    public synchronized void setLoginHelper(LoginHelper loginHelper) {
        this.loginHelper = loginHelper;
    }

    /**
     * @return Služba starající se o poslání source code na server
     */
    public SourceCodeService getSourceCodeService() {
        return sourceCodeService;
    }

    /**
     * @param sourceCodeService Služba starající se o poslání source code na server
     */
    public void setSourceCodeService(SourceCodeService sourceCodeService) {
        this.sourceCodeService = sourceCodeService;
    }

}
