package eu.profinit.manta.dataflow.repository.merger.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.dataflow.model.Graph;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.dataflow.model.utils.GraphQueryUtils;

/**
 * Task sloužící k doplnění umístění zdrojového skriptu, ve kterých se nachází konkrétní uzly.
 * Varianta, kdy se lokace počítá z hierarchie uzlu.
 * @author tfechtner
 *
 * @param <I> typ vstupních dat
 */
public class SourceFileTask<I> extends AbstractSourceFileTask<I> {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SourceFileTask.class);

    /** Typ uzlu pro skript, ze kterého se získá název a umístění. */
    private String scriptNodeType;

    /** Typy uzlů v cestě ke skriptu, které se nebudou přidávat do cesty. */
    private Set<String> ignoreNodeTypes = Collections.emptySet();

    @Override
    protected String findInputName(I input, Graph graph) {

        // zjistit název souboru včetně cesty
        List<Node> scriptNodeList = GraphQueryUtils.findNodesByType(graph, scriptNodeType);
        if (scriptNodeList.size() != 1) {
            LOGGER.warn("Wrong number of script nodes found: " + scriptNodeList.size() + ".");
            return null;
        }
        Node node = scriptNodeList.get(0);

        List<String> parts = new ArrayList<String>();
        parts.add(node.getName());

        Node parent = node;
        while ((parent = parent.getParent()) != null) {
            if (!ignoreNodeTypes.contains(parent.getType())) {
                parts.add(parent.getName());
            }
        }

        StringBuilder sb = new StringBuilder();
        for (int i = parts.size() - 1; i >= 0; i--) {
            sb.append(parts.get(i));
            if (i != 0) {
                sb.append("/");
            }
        }

        return sb.toString().replace("\\", "/");
    }

    /**
     * @return Typ uzlu pro skript, ze kterého se získá název a umístění.
     */
    public String getScriptNodeType() {
        return scriptNodeType;
    }

    /**
     * @param scriptNodeType Typ uzlu pro skript, ze kterého se získá název a umístění.
     */
    public void setScriptNodeType(String scriptNodeType) {
        this.scriptNodeType = scriptNodeType;
    }

    /**
     * @return Typy uzlů v cestě ke skriptu, které se nebudou přidávat do cesty.
     */
    public Set<String> getIgnoreNodeTypes() {
        return ignoreNodeTypes;
    }

    /**
     * @param ignoreNodeTypes Typy uzlů v cestě ke skriptu, které se nebudou přidávat do cesty.
     */
    public void setIgnoreNodeTypes(Set<String> ignoreNodeTypes) {
        this.ignoreNodeTypes = ignoreNodeTypes;
    }
}
