package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.connector.http.caller.CallerResponse;
import eu.profinit.manta.connector.http.task.UrlTask;

/**
 * Url task, který po úspěšném dokončení smaže soubor s revizí.
 * @author tfechtner
 *
 */
public class RevisionFinishTask extends UrlTask {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(RevisionFinishTask.class);

    /** Jméno souboru, kde je uložená revize. */
    private String revisionFilename;

    @Override
    protected void postProcessOk(CallerResponse response) {
        super.postProcessOk(response);

        if (revisionFilename == null) {
            LOGGER.warn("Revision file name is null.");
            return;
        }

        File revisionFile = new File(revisionFilename);
        if (revisionFile.exists() && !FileUtils.deleteQuietly(revisionFile)) {
            LOGGER.warn("Cannot delete revision file name {}.", revisionFile.getAbsolutePath());
        }
    }

    /**
     * @return Jméno souboru, kde je uložená revize. 
     */
    public String getRevisionFilename() {
        return revisionFilename;
    }

    /**
     * @param revisionFilename Jméno souboru, kde je uložená revize. 
     */
    public void setRevisionFilename(String revisionFilename) {
        this.revisionFilename = revisionFilename;
    }
}
