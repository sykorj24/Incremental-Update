package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.connector.http.caller.PayLoadCaller;
import eu.profinit.manta.connector.http.caller.callback.BasicErrorCallback;
import eu.profinit.manta.connector.http.caller.callback.BasicOkCallback;
import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeMapping;
import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeUploadRequest;

/**
 * Služba zajišťující překlad lokální názvů souborů na server id.
 * V případě potřeb soubory posílá na server. Posílání je asynchronní. <br />
 * <b>Při ukončování kontextu je třeba volat metodu destroy,
 * aby se počkalo na dokončení práce všech workerů.</b>
 * @author tfechtner
 *
 */
public class StandardSourceCodeService implements SourceCodeFileService {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(StandardSourceCodeService.class);
    /** Počet vláken, které posílají soubory na server. */
    private int threadCount = 2;
    /** Adresa url pro upload souboru. */
    private String uploadUrl;
    /** Thread pool pro pracovní vlákna spouštějící jednotlivé instance tasků. */
    private ExecutorService threadPool;
    /** Caller pro komunikaci přes http. */
    private PayLoadCaller httpCaller;
    /** Vnitřní číslo pro id source code. */
    private int sourceCodeIdCounter;
    /** Mapování lokálního názvu source code na jejich lokální id. */
    private final Map<String, String> sourceCodeByLocalName = new HashMap<>();
    /** Mapování lokálního id source code na soubor. */
    private final Map<String, File> sourceCodeById = new HashMap<>();

    @Override
    public synchronized String resolveFile(File file) {
        Validate.notNull(file, "File must not be null.");
        Validate.isTrue(file.canRead(), "Cannot read file: " + file.getAbsolutePath());

        String localName = getLocalName(file);
        String sourceCodeId = sourceCodeByLocalName.get(localName);
        if (sourceCodeId == null) {
            sourceCodeId = String.valueOf(sourceCodeIdCounter);
            sourceCodeIdCounter++;
            sourceCodeByLocalName.put(localName, sourceCodeId);
            sourceCodeById.put(sourceCodeId, file);
        }

        return sourceCodeId;
    }

    @Override
    public SourceCodeRecord getSourceCode(String id) {
        File file = sourceCodeById.get(id);
        if (file == null) {
            return null;
        }

        String localName = getLocalName(file);
        String hash = SourceHash.computeHash(file);
        return new SourceCodeRecord(id, localName, hash);
    }

    @Override
    public void planUpload(SourceCodeMapping mapping) {
        if (StringUtils.isEmpty(uploadUrl)) {
            LOGGER.info("Upload URL is not set => upload will not be executed", mapping.getLocalId());
            return;
        }
        File file = sourceCodeById.get(mapping.getLocalId());
        if (file == null) {
            LOGGER.error("The source code with id {} doesn't exist.", mapping.getLocalId());
            return;
        }

        getThreadPool().execute(new UploadWorker(mapping.getDbId(), file, httpCaller, uploadUrl));
    }

    /**
     * Lokální jméno souboru použité v rámci serveru.
     * @param file soubor k určení
     * @return lokální jméno pro daný soubor, nikdy null
     */
    private String getLocalName(File file) {
        Validate.notNull(file);
        return FilenameUtils.normalize(file.getPath());
    }

    /**
     * Finalizční metoda čekající až doběhnou všechny workery.
     */
    public void destroy() {
        LOGGER.info("Waiting for finishing upload.");
        getThreadPool().shutdown();
        try {
            getThreadPool().awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.warn("Service has been interrupted.", e);
            List<Runnable> threads = getThreadPool().shutdownNow();
            for (Runnable thread : threads) {
                LOGGER.warn("Worker has not been started: " + thread.toString());
            }
            Thread.currentThread().interrupt();
        }
        LOGGER.info("Everything done.");
    }

    /**
     * Lazy-load getter pro thread pool.
     * @return thread pool pro tuto instanci
     */
    private synchronized ExecutorService getThreadPool() {
        if (threadPool == null) {
            threadPool = Executors.newFixedThreadPool(threadCount);
        }
        return threadPool;
    }

    /**
     * @return Počet vláken, které posílají soubory na server.
     */
    public int getThreadCount() {
        return threadCount;
    }

    /**
     * @param threadCount Počet vláken, které posílají soubory na server.
     */
    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    /**
     * @return Adresa url pro upload souboru.
     */
    public String getUploadUrl() {
        return uploadUrl;
    }

    /**
     * @param uploadUrl Adresa url pro upload souboru.
     */
    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    /**
     * @return Caller pro komunikaci přes http.
     */
    public PayLoadCaller getHttpCaller() {
        return httpCaller;
    }

    /**
     * @param httpCaller Caller pro komunikaci přes http.
     */
    public void setHttpCaller(PayLoadCaller httpCaller) {
        this.httpCaller = httpCaller;
    }

    /**
     * Worker, posílající soubor na server.
     * @author tfechtner
     *
     */
    private static class UploadWorker implements Runnable {
        /** Server id souboru. */
        private final String fileId;
        /** Soubor k odeslání. */
        private final File file;
        /** Caller pro komunikaci přes HTTP. */
        private final PayLoadCaller payLoadCaller;

        private final String uploadUrl;

        /**
         * @param fileId Server id souboru
         * @param file Soubor k odeslání
         * @param payLoadCaller Caller pro komunikaci přes HTTP.
         * @param uploadUrl adresa, kam se má poslat url
         */
        UploadWorker(String fileId, File file, PayLoadCaller payLoadCaller, String uploadUrl) {
            super();
            this.fileId = fileId;
            this.file = file;
            this.payLoadCaller = payLoadCaller;
            this.uploadUrl = uploadUrl;
        }

        @Override
        public void run() {
            byte[] fileData;
            try {
                fileData = FileUtils.readFileToByteArray(file);
            } catch (IOException e) {
                LOGGER.error("IO error during uploading file.", e);
                return;
            }

            SourceCodeUploadRequest request = new SourceCodeUploadRequest();
            request.setFileId(fileId);
            request.setFileData(Base64.encodeBase64String(fileData));

            LOGGER.debug("Uploading file {} to the server.", FilenameUtils.normalize(file.getPath()));
            payLoadCaller.sendData(request, uploadUrl, BasicOkCallback.INSTANCE, BasicErrorCallback.INSTANCE);
        }
    }
}
