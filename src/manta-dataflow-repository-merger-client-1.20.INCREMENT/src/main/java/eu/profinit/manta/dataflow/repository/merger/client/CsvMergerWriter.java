package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.IOException;

import eu.profinit.manta.platform.automation.OutputWriter;

/**
 * Merger grafu na urovni binarnich dat ve formatu CSV.
 * Vezme data ze vstupu tak, jak jsou, ulozi je do souboru a odesle na server,
 * kde probehne vlastni merge.
 * 
 * @author onouza
 *
 */
public class CsvMergerWriter extends AbstractMergerWriter implements OutputWriter<byte[]> {

    @Override
    public void write(byte[] input) throws IOException {
        Double revision = readRevisionFromFile();
        saveToFile(input);
        sendToServer(input, revision);
    }

    @Override
    public void close() {
        // NOOP
    }

}
