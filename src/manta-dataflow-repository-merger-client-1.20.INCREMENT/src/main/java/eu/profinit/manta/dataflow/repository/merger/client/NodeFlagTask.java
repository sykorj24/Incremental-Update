package eu.profinit.manta.dataflow.repository.merger.client;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.dataflow.model.Graph;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.dataflow.model.NodeType;
import eu.profinit.manta.platform.automation.AbstractTask;

/**
 * Add REMOVE flag as a node attribute to all specified nodes.
 * Node attribute is created only temporarily for the purposes of input creation for incremental update.
 * This temporarily created node attribute is not included in the output sent to the server.
 * 
 * @author jsykora
 *
 * @param <I> input type
 */
public class NodeFlagTask<I> extends AbstractTask<I, Graph> {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeFlagTask.class);

    /** Attribute key of the flag. 
     *  Attribute key must be unique throughout all user's attribute keys (no collision with user's attributes may occur).
     */
    public static final String FLAG_ATTRIBUTE_KEY = "_flag_";
    /** Attribute value of the flag REMOVE */
    public static final String FLAG_ATTRIBUTE_VALUE = "remove";

    /** List of nodes types that will have added an additional flag */
    private List<String> nodeFlagTypeList = Collections.emptyList();

    @Override
    protected void doExecute(I input, Graph graph) {
        if(nodeFlagTypeList.isEmpty()) {
            return;
        }
        
        long start = System.currentTimeMillis();
        LOGGER.debug("Input graph for the incremental update:");
        // check all nodes in the graph
        for (Node node : graph.nodeSet()) {
            // check whether node is in the list of to-be-flagged nodes 
            if(node.getParent() != null) {
                LOGGER.info("Node: {} [{}] res={} (parent: {} [{}] res={})", 
                        node.getName(), node.getType(), node.getResource().getName(),
                        node.getParent().getName(), node.getParent().getType(), node.getParent().getResource().getName());
            } else {
                LOGGER.info("Node: {} [{}] res={}", 
                        node.getName(), node.getType(), node.getResource().getName());
            }
            
            if(nodeFlagTypeList.contains(node.getType())) {
                if(NodeType.PROCEDURE.getId().equals(node.getType())) {
                    // We cannot mark directly node Procedure (we cannot remove this node due to potential inconsistency issues).
                    // Instead, we find its direct child with different resource and append flag to this child node.
                    String procedureResource = node.getResource().getName();
                    boolean found = false;
                    for (Node child : node.getChildren()) {
                        if(!procedureResource.equals(child.getResource().getName())) {
                            // child node has different resource -> append flag to this child node
                            LOGGER.info("Appending flag {} to the Procedure child node {}.", FLAG_ATTRIBUTE_VALUE, child.getName());
                            child.addAttribute(FLAG_ATTRIBUTE_KEY, FLAG_ATTRIBUTE_VALUE);
                            found = true;
                        }
                    }
                    if(!found) {
                        LOGGER.warn("Could not append flag {} into Procedure structure (Node {} [Procedure] does not have any child with different resource).",
                                FLAG_ATTRIBUTE_VALUE, node.getName());
                    }
                    continue;
                }      
                LOGGER.info("Appending flag (as attribute) {} to the node {}.", FLAG_ATTRIBUTE_VALUE, node.getName());
                node.addAttribute(FLAG_ATTRIBUTE_KEY, FLAG_ATTRIBUTE_VALUE);
            }
        }
        
        LOGGER.debug("Finnished appending flags to chosen nodes in {} ms", System.currentTimeMillis() - start);
    }

    public List<String> getNodeFlagTypeList() {
        return nodeFlagTypeList;
    }

    public void setNodeFlagTypeList(List<String> nodeFlagTypeList) {
        this.nodeFlagTypeList = nodeFlagTypeList;
    }

}
