package eu.profinit.manta.dataflow.repository.merger.client;

import java.util.Collections;
import java.util.List;

import eu.profinit.manta.dataflow.model.Graph;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.platform.automation.AbstractTask;

/**
 * Task sloužící k doplnění umístění zdrojového skriptu, ve kterých se nachází konkrétní uzly.
 * Varianta, kdy se lokace a hash počítá z hierarchie uzlu.
 * @author LHERMANN
 *
 * @param <I> typ vstupních dat
 */
public class SourceNodeTask<I> extends AbstractTask<I, Graph> {

    /** Výchozí název atributu, kam se ukládá umístění. */
    public static final String DEFAULT_SOURCE_LOCATION = "sourceLocation";

    /** Název atributu, kam se ukládá umístění. */
    private String sourceLocationProperty = DEFAULT_SOURCE_LOCATION;

    /** Seznam typů uzlů, do kterých se přidává informace o umístění. */
    private List<String> editedNodeTypeList = Collections.emptyList();

    /** Služba zajišťující mergování hashů na server. */
    private SourceCodeNodeService sourceCodeService;

    /**
     * Vrací název atributu, kam se ukládá umístění.
     * @return název atributu, kam se ukládá umístění
     */
    public String getSourceLocationProperty() {
        return sourceLocationProperty;
    }

    /**
     * Nastavuje název atributu, kam se ukládá umístění.
     * @param sourceLocationProperty název atributu, kam se ukládá umístění
     */
    public void setSourceLocationProperty(String sourceLocationProperty) {
        this.sourceLocationProperty = sourceLocationProperty;
    }

    /**
     * Vrací seznam typů uzlů, do kterých se přidává informace o umístění.
     * @return seznam typů uzlů, do kterých se přidává informace o umístění
     */
    public List<String> getEditedNodeTypeList() {
        return editedNodeTypeList;
    }

    /**
     * Nastavuje seznam typů uzlů, do kterých se přidává informace o umístění.
     * @param editedNodeTypeList seznam typů uzlů, do kterých se přidává informace o umístění
     */
    public void setEditedNodeTypeList(List<String> editedNodeTypeList) {
        this.editedNodeTypeList = editedNodeTypeList;
    }

    /**
     * Vrací službu zajišťující mergování hashů na server.
     * @return služba zajišťující mergování hashů na server
     */
    public SourceCodeNodeService getSourceCodeService() {
        return sourceCodeService;
    }

    /**
     * Nastavuje zajišťující mergování hashů na server.
     * @param sourceCodeService zajišťující mergování hashů na server
     */
    public void setSourceCodeService(SourceCodeNodeService sourceCodeService) {
        this.sourceCodeService = sourceCodeService;
    }

    @Override
    protected void doExecute(I input, Graph graph) {

        for (Node node : graph.nodeSet()) {
            if (editedNodeTypeList.contains(node.getType())) {
                String sourceCodeId = sourceCodeService.resolveNode(node, graph);

                node.addAttribute(sourceLocationProperty, sourceCodeId);
            }
        }

    }

}
