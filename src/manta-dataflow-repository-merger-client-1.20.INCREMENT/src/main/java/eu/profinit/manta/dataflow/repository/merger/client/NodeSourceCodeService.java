package eu.profinit.manta.dataflow.repository.merger.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.profinit.manta.dataflow.model.Graph;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeMapping;

/**
 * Služba získávající source code pomocí id a přiřazující id na základě uzlu v grafu.
 * @author LHERMANN
 *
 */
public class NodeSourceCodeService implements SourceCodeNodeService {

    /** Vnitřní číslo pro id source code. */
    private int sourceCodeIdCounter;
    /** Mapování lokálního názvu source code na jejich lokální id. */
    private final Map<String, String> sourceCodeByLocalName = new HashMap<>();
    /** Mapování lokálního id source code na záznam source code. */
    private final Map<String, SourceCodeRecord> sourceCodeById = new HashMap<>();

    @Override
    public String resolveNode(Node node, Graph graph) {
        String localName = computeLocalName(node, graph);
        String sourceCodeId = sourceCodeByLocalName.get(localName);
        if (sourceCodeId == null) {
            sourceCodeId = String.valueOf(sourceCodeIdCounter);
            sourceCodeIdCounter++;
            sourceCodeByLocalName.put(localName, sourceCodeId);
            sourceCodeById.put(sourceCodeId, new SourceCodeRecord(sourceCodeId, localName, computeHash(node, graph)));
        }

        return sourceCodeId;
    }

    @Override
    public SourceCodeRecord getSourceCode(String id) {
        return sourceCodeById.get(id);
    }

    @Override
    public void planUpload(SourceCodeMapping mapping) {
        // NOOP
    }

    /**
     * Spočítá plně kvalifikované jméno uzlu.
     * @param node uzel
     * @param graph graf obsahující uzel
     * @return plně kvalifikované jméno uzlu
     */
    protected String computeLocalName(Node node, Graph graph) {
        StringBuilder sb = new StringBuilder(node.getName());
        while (node.getParent() != null) {
            node = node.getParent();
            sb.insert(0, node.getName() + "/");
        }
        return sb.toString();
    }

    /**
     * Spočítá hash uzlu na základě jeho jména, typu, počtu příchozích a odchozích hran a rekurzivně jeho dětí.
     * @param node uzel
     * @param graph graf obsahující uzel
     * @return hash uzlu
     */
    protected String computeHash(Node node, Graph graph) {
        return Integer.toString(computeIntHash(node, graph));
    }

    private int computeIntHash(Node node, Graph graph) {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((node.getName() == null) ? 0 : node.getName().hashCode());
        result = prime * result + ((node.getType() == null) ? 0 : node.getType().hashCode());

        List<Node> children = new ArrayList<Node>(graph.getChildren(node));
        Collections.sort(children, new Comparator<Node>() {

            @Override
            public int compare(Node node1, Node node2) {
                int result = node1.getName().compareTo(node2.getName());
                if (result == 0) {
                    return node1.getType().compareTo(node2.getType());
                } else {
                    return result;
                }

            }
        });

        if (!children.isEmpty()) {
            for (Node child : children) {
                result = prime * result + computeIntHash(child, graph);
            }
        } else {
            result = prime * result + graph.getIncomingEdges(node).size();
            result = prime * result + graph.getOutgoingEdges(node).size();
        }

        return result;
    }

}
