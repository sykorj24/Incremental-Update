package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.Iterator;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.connector.http.client.HttpClientProvider;
import eu.profinit.manta.connector.http.client.SelfSignedHttpsProvider;
import eu.profinit.manta.connector.http.login.LoginHelper;
import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeMapping;
import eu.profinit.manta.platform.automation.AbstractTask;
import eu.profinit.manta.platform.automation.MantaVersion;
import eu.profinit.manta.platform.automation.Null;
import eu.profinit.manta.platform.automation.ScenarioFailedException;

/**
 * DeleteVerticesTask sends to server a file containing vertices to be deleted and server processes this file. 
 * 
 * @author jsykora
 *
 */
public class DeleteVerticesTask extends AbstractTask<Null, Null> {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteVerticesTask.class);
    /** Server url to which is sent file with vertices to be deleted */
    private String serverUrl;
    /** Version of Manta client */
    private MantaVersion mantaVersion;
    /** File containing version of Manta client */
    private static final String APPLICATION_VERSION_FILE = "/manta-dataflow-repository-merger-client/app-version.txt";
    /** Object used to login to the repository */
    private LoginHelper loginHelper;
    /** Provider creating HTTP client */
    private HttpClientProvider httpClientProvider = new SelfSignedHttpsProvider();
    /** HTTP client used for connection to the metadata repository */
    private HttpClient httpClient;
    /** Name of the file containing vertices that are to be deleted */
    private String deleteVerticesFilename;
    /** Name of the file containing revision number */
    private String revisionFilename;
    
    public DeleteVerticesTask() {
        InputStream is = this.getClass().getResourceAsStream(APPLICATION_VERSION_FILE);
        if (is == null) {
            LOGGER.error("File with application version does not exist.");
            return;
        }

        StringWriter versionWriter = new StringWriter();
        try {
            IOUtils.copy(is, versionWriter, StandardCharsets.UTF_8);
        } catch (IOException e) {
            LOGGER.error("Error during reading file with application version.", e);
            return;
        }
        mantaVersion = MantaVersion.createVersionFromText(versionWriter.toString());
    }
    
    /**
     * Read file with vertices, read revision number from revision file and start sending it both to server
     */
    @Override
    protected void doExecute(Null arg0, Null arg1) {
        byte[] byteArray;
        try {
            byteArray = Files.readAllBytes(Paths.get(deleteVerticesFilename));
        } catch (IOException e) {
            LOGGER.error("Error reading file", e);
            return;
        }
        Double revision = RevisionFileUtils.readRevisionFromFile(revisionFilename);
        sendToServer(byteArray, revision);
    }
    
    /**
     * Send to server content of file, revision number and Manta client version.
     * After the request is finished by server, a server response is processed.
     * 
     * @param byteArray  content of file with vertices to be deleted
     * @param revision  revision number within which the vertices are ought to be deleted
     */
    private void sendToServer(byte[] byteArray, Double revision) {
        if (serverUrl != null) {
            HttpClient client = getHttpClient();
            HttpPost post = null;

            try {
                post = new HttpPost(serverUrl);

                if (loginHelper == null) {
                    throw new ScenarioFailedException("Login helper must be set.");
                }
                if (!loginHelper.authenticate(client, post)) {
                    LOGGER.error("Authentication failed.");
                    return;
                }

                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create().addPart("file",
                        new ByteArrayBody(byteArray, "file"));

                if (revision != null) {
                    entityBuilder.addTextBody("revision", revision.toString());
                }
                if (mantaVersion != null) {
                    entityBuilder.addTextBody("mantaVersion", mantaVersion.getStringVersion());
                }

                HttpEntity reqEntity = entityBuilder.build();
                post.setEntity(reqEntity);
                try {
                    LOGGER.info("Sending data to server {}.", serverUrl);
                    HttpResponse response = client.execute(post);

                    int responseStatusCode = response.getStatusLine().getStatusCode();
                    String responseMessage = EntityUtils.toString(response.getEntity());

                    if (responseStatusCode == HttpStatus.SC_OK) {
                        JsonNode responseNode = parseResponse(responseMessage);
                        LOGGER.info("Response from server: {}. ", simplifyResponse(responseNode));
                    } else if (responseStatusCode == HttpStatus.SC_NOT_FOUND) {
                        throw new ScenarioFailedException("Server-side of merger does not exist on target address.");
                    } else if (responseStatusCode == HttpStatus.SC_BAD_REQUEST) {
                        throw new ScenarioFailedException(
                                "Bad format of request with response content " + responseMessage + ".");
                    } else if (responseStatusCode == HttpStatus.SC_METHOD_NOT_ALLOWED) {
                        throw new ScenarioFailedException("This type of http method is not supported.");
                    } else if (responseStatusCode == HttpStatus.SC_PAYMENT_REQUIRED) {
                        throw new ScenarioFailedException(MessageFormat.format("Error response from server ({0}): {1}", responseStatusCode, responseMessage));
                    } else {
                        LOGGER.error("Error response from server - the response code {} and content {}.",
                                responseStatusCode, responseMessage);
                    }
                } catch (ClientProtocolException e) {
                    LOGGER.error("Error ClientProtocolException", e);
                } catch (IOException e) {
                    LOGGER.error("Error IOException", e);
                    throw new ScenarioFailedException("Error during sending file to the server.", e);
                }
            } finally {
                if (post != null) {
                    post.releaseConnection();
                }
            }
        }
    }
    
    private synchronized HttpClient getHttpClient() {
        if (httpClient == null) {
            httpClient = httpClientProvider.createHttpClient();
        }
        return httpClient;
    }
    
    /**
     * Naparsuje odpověď ze serveru.
     * @param responseMessage zpráva od serveru
     * @return json uzel nebo null při chybě
     */
    private JsonNode parseResponse(String responseMessage) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(responseMessage);
        } catch (JsonParseException e) {
            LOGGER.error("Error during parsing response: " + responseMessage + ". ", e);
            return null;
        } catch (JsonMappingException e) {
            LOGGER.error("Error during mapping response: " + responseMessage + ". ", e);
            return null;
        } catch (IOException e) {
            LOGGER.error("IO Error during resolving response: " + responseMessage + ". ", e);
            return null;
        }
    }
    
    /**
     * Retrieve report summarizing deletion of vertices on server
     * 
     * @param responseNode  JsonNode representing the whole response
     * @return
     */
    private String simplifyResponse(JsonNode responseNode) {
        JsonNode reportNode = responseNode.get("processingReport");
        if (reportNode == null) {
            LOGGER.warn("Response does not contain processing report.");
            return "Response does not contain processing report.";
        }
        JsonNode proccessingTimeNode = responseNode.get("proccessingTime");
        if (proccessingTimeNode == null) {
            LOGGER.warn("Response does not contain processing time.");
        }
        
        return reportNode.toString() + ", {\"proccessingTime\":" + proccessingTimeNode.toString() + "}";
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public MantaVersion getMantaVersion() {
        return mantaVersion;
    }

    public void setMantaVersion(MantaVersion mantaVersion) {
        this.mantaVersion = mantaVersion;
    }

    public LoginHelper getLoginHelper() {
        return loginHelper;
    }

    public void setLoginHelper(LoginHelper loginHelper) {
        this.loginHelper = loginHelper;
    }

    public HttpClientProvider getHttpClientProvider() {
        return httpClientProvider;
    }

    public void setHttpClientProvider(HttpClientProvider httpClientProvider) {
        this.httpClientProvider = httpClientProvider;
    }

    public String getDeleteVerticesFilename() {
        return deleteVerticesFilename;
    }

    public void setDeleteVerticesFilename(String deleteVerticesFilename) {
        this.deleteVerticesFilename = deleteVerticesFilename;
    }

    public String getRevisionFilename() {
        return revisionFilename;
    }

    public void setRevisionFilename(String revisionFilename) {
        this.revisionFilename = revisionFilename;
    }

    public void setHttpClient(HttpClient httpClient) {
        this.httpClient = httpClient;
    }
    
    
}
