package eu.profinit.manta.dataflow.repository.merger.client;

import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeMapping;

/**
 * Rozhraní služby získávající a nahrávající source code pomocí id.
 * @author tfechtner
 *
 */
public interface SourceCodeService {

    /**
     * Získá detail o source code s daným id.
     * @param id id hledaného source code
     * @return informace o daném source code nebo null při neexistenci
     */
    SourceCodeRecord getSourceCode(String id);

    /**
     * Naplánuje upload daného source code.
     * @param mapping mapping source code, které se má poslat
     */
    void planUpload(SourceCodeMapping mapping);
}
