package eu.profinit.manta.dataflow.repository.merger.client;

/**
 * Záznam reprezentující source code.
 * @author tfechtner
 *
 */
public class SourceCodeRecord {
    /** Lokální id. */
    private final String id;
    /** Lokální jméno. */
    private final String localName;
    /** Hash souboru. */
    private final String hash;

    /**
     * @param id Lokální id
     * @param localName Lokální jméno
     * @param hash Hash souboru
     */
    public SourceCodeRecord(String id, String localName, String hash) {
        super();
        this.id = id;
        this.localName = localName;
        this.hash = hash;
    }

    /**
     * @return Lokální id
     */
    public String getId() {
        return id;
    }

    /**
     * @return Lokální jméno
     */
    public String getLocalName() {
        return localName;
    }

    /**
     * @return Hash souboru
     */
    public String getHash() {
        return hash;
    }
}
