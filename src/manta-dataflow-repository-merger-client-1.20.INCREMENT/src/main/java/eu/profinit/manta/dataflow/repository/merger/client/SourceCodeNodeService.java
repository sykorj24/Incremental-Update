package eu.profinit.manta.dataflow.repository.merger.client;

import eu.profinit.manta.dataflow.model.Graph;
import eu.profinit.manta.dataflow.model.Node;

/**
 * Rozhraní služby získávající a nahrávající source code pomocí id a přiřazující id na základě uzlu v grafu.
 * @author LHERMANN
 *
 */
public interface SourceCodeNodeService extends SourceCodeService {
    
    /**
     * Přiřadí danému uzlu lokální id.
     * @param node uzel
     * @param graph graf obsahující uzel
     * @return lokální id souboru, nebo null při chybě
     */
    String resolveNode(Node node, Graph graph);
}