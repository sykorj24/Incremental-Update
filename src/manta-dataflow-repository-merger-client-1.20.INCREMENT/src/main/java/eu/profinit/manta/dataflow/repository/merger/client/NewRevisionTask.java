package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.connector.http.client.HttpClientProvider;
import eu.profinit.manta.connector.http.client.SelfSignedHttpsProvider;
import eu.profinit.manta.connector.http.login.LoginHelper;
import eu.profinit.manta.platform.automation.AbstractTask;
import eu.profinit.manta.platform.automation.MantaVersion;
import eu.profinit.manta.platform.automation.Null;
import eu.profinit.manta.platform.automation.ScenarioFailedException;

/**
 * Task pro zavolání get requestu na server, kde se vytvoří nový revizní uzel a vrátí
 * se klientovi jeho číslo. Klient toto číslo revize uloží do souboru
 * 
 * @author pholecek
 */
public class NewRevisionTask extends AbstractTask<Null, Null> {

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(NewRevisionTask.class);

    /** Kódování pro příjem zprávy. */
    private static final String ENCODING = "utf-8";

    /** Url adresa serveru, kam se klient doptá. */
    private String serverUrl;

    /** Jméno souboru, do nějž se revize zapíše. */
    private String revisionFilename;

    /** Objekt pro přihlášení do repository. */
    private LoginHelper loginHelper;

    /** Provider vytvářející http clienta.*/
    private HttpClientProvider httpClientProvider = new SelfSignedHttpsProvider();

    /** Parametr, pod kterým se posílá v jsonu číslo revize. */
    public static final String REVISION_JSON = "revision";
    /** Soubor obsahující verzi aplikace. */
    private static final String APPLICATION_VERSION_FILE = "/manta-dataflow-repository-merger-client/app-version.txt";

    /** Verze klienta. */
    private MantaVersion mantaVersion;

    public NewRevisionTask() {
        InputStream is = this.getClass().getResourceAsStream(APPLICATION_VERSION_FILE);
        if (is == null) {
            LOGGER.error("File with application version does not exist.");
            return;
        }

        StringWriter versionWriter = new StringWriter();
        try {
            IOUtils.copy(is, versionWriter, StandardCharsets.UTF_8);
        } catch (IOException e) {
            LOGGER.error("Error during reading file with application version.", e);
            return;
        }
        mantaVersion = MantaVersion.createVersionFromText(versionWriter.toString());
    }

    @Override
    protected void doExecute(Null arg0, Null arg1) {
        Double revision = getRevisionOnServer();
        writeRevisionToFile(revision);
    }

    /**
     * Vytvoří nový soubor, do nějž se zapíše revize. V případě již existujícího souboru
     * tento soubor přepíše
     * 
     * @param revision číslo revize, které bude zapsaáno do souboru
     */
    private void writeRevisionToFile(Double revision) {
        if (revision == null || revision < 0) {
            LOGGER.error("Incorrect revision value - {}", revision);
            throw new ScenarioFailedException("Incorrect revision number.");
        }
        if (revisionFilename == null || revisionFilename.trim().equals("")) {
            throw new ScenarioFailedException("Filename for revision not set.");
        }

        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(revisionFilename), ENCODING));

            writer.write(String.valueOf(revision));

        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Unsupported encoding.", e);
        } catch (IOException e) {
            LOGGER.error("Unexpected IO exception when trying to write to revision file.", e);
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

    /**
     * Zavola na serveru prikaz pro vytvoreni noveho revizniho uzlu, server vrati cislo nove revize.
     * a metoda tuto revizi vrati
     * @return číslo vytvořené revize
     */
    private Double getRevisionOnServer() {
        LOGGER.info("ServerUrl=" + serverUrl);
        if (serverUrl == null || serverUrl.trim().equals("")) {
            System.out.println(serverUrl);
            throw new IllegalStateException("ServerUrl not set");
        }
        if (loginHelper == null) {
            throw new IllegalStateException("Login helper not set");
        }

        HttpClient httpClient = httpClientProvider.createHttpClient();

        Double revision = null;
        
        HttpPost request = new HttpPost(serverUrl);
        if (!loginHelper.authenticate(httpClient, request)) {
            LOGGER.error("Authentication failed.");
            return null;
        }

        try {
            if (mantaVersion != null) {
                HttpEntity reqEntity = EntityBuilder.create()
                        .setParameters(new BasicNameValuePair("mantaVersion", mantaVersion.getStringVersion())).build();
                request.setEntity(reqEntity);
            }

            long start = System.currentTimeMillis();
            //LOGGER.info("Sending new revision request to {}", serverUrl);
            LOGGER.info("Sending new revision request.");
            HttpResponse response = httpClient.execute(request);
            long end = System.currentTimeMillis();
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {

                String retSrc = EntityUtils.toString(response.getEntity());
                revision = new JSONObject(retSrc).getDouble(REVISION_JSON);

                LOGGER.info("OK response: revision {}, time: {}ms", revision, (end - start));

            } else {
                LOGGER.error("Error response from server. Message was {} with code {}.",
                        EntityUtils.toString(response.getEntity()), response.getStatusLine().getStatusCode());
            }

        } catch (ClientProtocolException e) {
            LOGGER.error("Error during http comunication. ", e);
        } catch (IOException e) {
            LOGGER.error("Error during saving response. ", e);
        } catch (JSONException e) {
            LOGGER.error("Error during getting json response. ", e);
        } finally {
            request.releaseConnection();
        }
        return revision;
    }

    /**
     * @return Url adresa serveru, kam se klient doptá. 
     */
    public String getServerUrl() {
        return serverUrl;
    }

    /**
     * @param serverUrl Url adresa serveru, kam se klient doptá. 
     */
    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    /**
     * @return Jméno souboru, do nějž se revize zapíše. 
     */
    public String getRevisionFilename() {
        return revisionFilename;
    }

    /**
     * @param revisionFilename Jméno souboru, do nějž se revize zapíše. 
     */
    public void setRevisionFilename(String revisionFilename) {
        this.revisionFilename = revisionFilename;
    }

    /**
     * @return Objekt pro přihlášení do repository
     */
    public LoginHelper getLoginHelper() {
        return loginHelper;
    }

    /**
     * @param loginHelper Objekt pro přihlášení do repository
     */
    public void setLoginHelper(LoginHelper loginHelper) {
        this.loginHelper = loginHelper;
    }

    /**
     * @return Provider vytvářející http clienta.
     */
    public HttpClientProvider getHttpClientProvider() {
        return httpClientProvider;
    }

    /**
     * @param httpClientProvider Provider vytvářející http clienta.
     */
    public void setHttpClientProvider(HttpClientProvider httpClientProvider) {
        this.httpClientProvider = httpClientProvider;
    }
}
