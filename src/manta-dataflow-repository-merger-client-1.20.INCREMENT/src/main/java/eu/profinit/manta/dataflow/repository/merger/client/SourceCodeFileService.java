package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.File;

/**
 * Rozhraní služby získávající a nahrávající source code pomocí id a přiřazující id na základě souboru na disku.
 * @author LHERMANN
 *
 */
public interface SourceCodeFileService extends SourceCodeService {

    /**
     * Přiřadí danému souboru lokální id.
     * @param file soubor
     * @return lokální id souboru, nebo null při chybě
     */
    String resolveFile(File file);
}