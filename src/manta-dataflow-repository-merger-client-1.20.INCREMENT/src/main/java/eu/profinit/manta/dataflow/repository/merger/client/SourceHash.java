package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utilita pro výpočet hashe zdrojového skriptu.
 * 
 * @author jtousek
 */
public class SourceHash {
    
    private static final Logger log = LoggerFactory.getLogger(SourceHash.class);
    
    /** Velikost buffer při počítání hashe souboru. */
    private static final int BUFFER_SIZE = 512;
    
    
    private SourceHash() { /* no thanks */ }
    
    
    /**
     * Spočte md5 hash obsahu souboru.
     * 
     * @param file soubor, pro který se hash počítá
     * 
     * @return md5 hash nebo null při chybě
     */
    public static String computeHash(File file) {
        try (FileInputStream is = new FileInputStream(file)) {
            return computeHash(is);
        } catch (FileNotFoundException e) {
            log.error("File from which the hash should be computed, does not exist: " + file.getAbsolutePath(), e);
            return null;
        } catch (IOException e) {
            log.error("I/O error while computing hash from: " + file.getAbsolutePath(), e);
            return null;
        }
    }


    /**
     * Spočte md5 hash vstupu.
     * 
     * @param inputStream vstup
     * 
     * @return md5 hash nebo null při chybě
     */
    public static String computeHash(InputStream inputStream) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            log.error("MD5 hash is not known.", e);
            return null;
        }
        
        try {
            byte[] buffer = new byte[BUFFER_SIZE];
            while (inputStream.read(buffer) != -1) {
                md.update(buffer);
            }
        } catch (IOException e) {
            log.error("IO error while computing hash.", e);
            return null;
        }
        
        return new String(Hex.encodeHex(md.digest()));
    }
    
    
}
