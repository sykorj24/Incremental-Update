package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Pomocná třída pro práci s revizním souborem.
 * @author pholecek
 *
 */
public final class RevisionFileUtils {

    private RevisionFileUtils() {
    }

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(RevisionFileUtils.class);
    /** Encoding souboru s revizí. */
    private static final String ENCODING = "utf-8";

    /**
     * Přečtě číslo revize ze souboru a vrátí jej.
     * @param revisionFilename nazev souboru s revizi
     * @return číslo revize
     */
    public static Double readRevisionFromFile(String revisionFilename) {
        Double revision = null;

        if (StringUtils.isNotEmpty(revisionFilename)) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(revisionFilename)),
                        ENCODING));

                String line = reader.readLine();
                if (line == null || line.trim().equals("")) {
                    throw new IllegalStateException("Cannot read revision from empty file.");
                }
                revision = Double.parseDouble(line);

            } catch (FileNotFoundException e) {
                LOGGER.error("Revision file not found.", e);
            } catch (IOException e) {
                LOGGER.error("Unexpected IO exception when trying to read from revision file.", e);
            } catch (NumberFormatException e) {
                LOGGER.error("Incorrect revision file number format.", e);
            } finally {
                IOUtils.closeQuietly(reader);
            }
        }

        return revision;
    }
}
