package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVWriter;
import eu.profinit.manta.dataflow.model.Edge;
import eu.profinit.manta.dataflow.model.Graph;
import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.dataflow.model.Resource;
import eu.profinit.manta.dataflow.model.impl.ResourceImpl;
import eu.profinit.manta.dataflow.repository.utils.Base64AttributeCodingHelper;
import eu.profinit.manta.dataflow.repository.utils.CsvHelper;
import eu.profinit.manta.platform.automation.OutputWriter;

/**
 * Klientská část mergeru mergující na úrovni modelu grafu přes lokální id.
 * Vstupní graf je seřazen podle závislostí a poslán na server jako POST request či uložen na disk. 
 * 
 * @author tfechtner
 * @author onouza
 *
 */
public class MergerWriter extends AbstractMergerWriter implements OutputWriter<Graph> {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(MergerWriter.class);
    /** Výchozí resource, používaný pro uzly s null resource. */
    private static final Resource DEFAULT_RESOURCE = new ResourceImpl("Default", "Default", "Default resource");
    /** Používané kódování. */
    private static final String ENCODING = "utf-8";
    /** Výchozá počet uzlů grafu k zapsání. */
    private static final int DEFAULT_SENDING_GRAPH_SIZE = 4096;
    /** Počet uzlů grafu, aby se graf zapsal. */
    private int sendingGraphSize = DEFAULT_SENDING_GRAPH_SIZE;
    /** Vnitřní graf, kam se merguje, dokud není graf dostatečně velký k zapsání. */
    private Graph innerGraph;

    @Override
    public synchronized void write(Graph input) throws IOException {
        mergeGraph(input);
        if (innerGraph.nodeSet().size() > sendingGraphSize) {
            writeInnerGraph();
        }
    }

    /**
     * Zamerguje vstupní graf do vnitřní struktury.
     * @param inputGraph vstupní data k přidání
     */
    private void mergeGraph(Graph inputGraph) {
        if (innerGraph == null) {
            innerGraph = inputGraph;
            return;
        }

        int innerSize = innerGraph.nodeSet().size();
        int inputSize = inputGraph.nodeSet().size();

        if (innerSize > inputSize) {
            innerGraph.merge(inputGraph);
        } else {
            inputGraph.merge(innerGraph);
            innerGraph = inputGraph;
        }
    }

    /**
     * Zapíše vnitřní nashromážděný graf.
     */
    private void writeInnerGraph() {
        if (innerGraph != null) {
            byte[] byteArray = createData(innerGraph);
            Double revision = readRevisionFromFile();
            saveToFile(byteArray);
            sendToServer(byteArray, revision);
            innerGraph = null;
        }
    }

    @Override
    public synchronized void close() {
        writeInnerGraph();
    }

    /**
     * Výtvoří linearizovanou podobu grafu.
     * Nejprve jsou vrstvy, poté uzly seřazené podle parent vztahu, a nakonec hrany. 
     * @param input vstupní graf k zpracování
     * @return byte array reprezentující zpracovaný graf
     * @throws IOException chyba zápisu do pole
     */
    protected byte[] createData(Graph input) {
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        CSVWriter writer = null;
        try {
            writer = CsvHelper.getCSVWriter(new PrintWriter(new OutputStreamWriter(byteArray, ENCODING)));

            printSourceCodes(writer, input.nodeSet());
            Map<Layer, Integer> mapLayerToId = printLayers(writer, input.nodeSet());
            Map<Resource, Integer> mapResourceToId = printResources(writer, input.nodeSet(), mapLayerToId.size(), mapLayerToId);
            Map<Node, Integer> sortedNodeIds = sortNodes(input.nodeSet(), mapLayerToId.size() + mapResourceToId.size());
            printNodes(writer, sortedNodeIds, mapResourceToId);
            printNodeAtributes(writer, sortedNodeIds);
            printEdges(writer, input.edgeSet(), sortedNodeIds, mapResourceToId);
            printEdgeAtributes(writer, input.edgeSet());
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Uknown encoding.", e);
        } finally {
            IOUtils.closeQuietly(writer);
        }

        return byteArray.toByteArray();
    }

    /**
     * Setřídí uzly podle parent vztahu.
     * @param nodeSet množina uzlů k setřízení
     * @param index index od kterého se začínají číslovat uzly
     * @return seřazená mapa indexovaná uzly, kde hodnota je přiřazené id
     */
    private Map<Node, Integer> sortNodes(Set<Node> nodeSet, int index) {
        Deque<Node> sortedNodes = new LinkedList<Node>();

        for (Node node : nodeSet) {
            for (Node tmpNode = node; tmpNode != null; tmpNode = tmpNode.getParent()) {
                sortedNodes.push(tmpNode);
            }
        }

        LinkedHashMap<Node, Integer> sortedNodeIds = new LinkedHashMap<Node, Integer>();
        int i = index;
        for (Node node : sortedNodes) {
            if (!sortedNodeIds.containsKey(node)) {
                sortedNodeIds.put(node, i++);
            }
        }

        return sortedNodeIds;
    }

    /**
     * Zapíše uzly do output writeru.
     * @param outputWriter výstupní writer
     * @param sortedNodeIds seřazené uzly jako indexy do mapy s jejich id
     * @param mapResourceToId 
     */
    private void printNodes(CSVWriter outputWriter, Map<Node, Integer> sortedNodeIds,
            Map<Resource, Integer> mapResourceToId) {
        Set<Node> nodeSet = sortedNodeIds.keySet();
        for (Node node : nodeSet) {
            outputWriter.writeNext(getNodeTextForm(node, sortedNodeIds, mapResourceToId));
        }
    }

    /**
     * Zapíše source codes do output writeru.
     * @param outputWriter výstupní writer
     * @param nodeSet všechny uzly, pro které se mají poslat source codes
     */
    private void printSourceCodes(CSVWriter outputWriter, Set<Node> nodeSet) {
        if (getSourceCodeService() == null) {
            return;
        }

        Set<String> sourceCodeIds = new HashSet<>();

        for (Node node : nodeSet) {
            Set<Object> sourceCodeIdSet = node.getAttributeSet(AbstractSourceFileTask.DEFAULT_SOURCE_LOCATION);
            if (!sourceCodeIdSet.isEmpty()) {
                if (sourceCodeIdSet.size() > 1) {
                    LOGGER.error("Node {} has {} source code locations.", node, sourceCodeIdSet.size());
                }
                sourceCodeIds.add(sourceCodeIdSet.iterator().next().toString());
            }
        }

        for (String id : sourceCodeIds) {
            SourceCodeRecord record = getSourceCodeService().getSourceCode(id);
            if (record == null) {
                LOGGER.error("Unknown source code id {}.", id);
                continue;
            }

            outputWriter.writeNext(getSourceCodeTextForm(record));
        }
    }

    /**
     * Zapíše hrany do output writeru.
     * @param writer  výstupní writer
     * @param edgeSet seznam hran pro vypsání
     * @param sortedNodeIds seřazené uzly jako indexy do mapy s jejich id
     * @param mapResourceToId 
     */
    private void printEdges(CSVWriter writer, Set<Edge> edgeSet, Map<Node, Integer> sortedNodeIds,
            Map<Resource, Integer> mapResourceToId) {
        int index = 0;
        for (Edge edge : edgeSet) {
            String[] edgeTextForm = getEdgeTextForm(index, edge, sortedNodeIds, mapResourceToId);
            if (edgeTextForm != null) {
                writer.writeNext(edgeTextForm);
            }
            index++;
        }
    }

    /**
     * Zapise vrstvy do vystupniho writeru.
     * 
     * @param writer Vystupni writer.
     * @param nodeSet Seznam vsech uzlu v grafu, ve kterych se hledaji vrstvy.
     * @return Mapa vrstvev na jejich ID.
     */
    private Map<Layer, Integer> printLayers(CSVWriter writer, Set<Node> nodeSet) {
        int index = 0;
        Map<Layer, Integer> mapLayerToId = new HashMap<Layer, Integer>();

        for (Node node : nodeSet) {
            Layer layer = node.getResource() != null ? node.getResource().getLayer() : null;
            if (!(mapLayerToId.containsKey(layer))) {
                mapLayerToId.put(layer, index);

                writer.writeNext(getLayerTextForm(index, layer));
                index++;
            }
        }

        return mapLayerToId;
    }

    /**
     * Zapíše resource do výstupního writeru.
     * @param writer vástupní writer
     * @param nodeSet seznam všech uzlů v grafu, ve kterých se hledají resources
     * @param mapLayerToId Mapa vrstvev na jejich ID. 
     * @return mapa resource na jejich id
     */
    private Map<Resource, Integer> printResources(CSVWriter writer, Set<Node> nodeSet, int index, Map<Layer, Integer> mapLayerToId) {
        Map<Resource, Integer> mapResourceToId = new HashMap<Resource, Integer>();

        for (Node node : nodeSet) {
            Resource res = node.getResource();
            if (!(mapResourceToId.containsKey(res))) {
                mapResourceToId.put(res, index);

                writer.writeNext(getResourceTextForm(index, res, mapLayerToId));
                index++;
            }
        }

        return mapResourceToId;
    }

    /**
     * Vypíše atributy uzlů do výstupního writeru.
     * @param writer výstupní writer
     * @param sortedNodeIds setřízená mapa uzlů na jejich id
     */
    private void printNodeAtributes(CSVWriter writer, Map<Node, Integer> sortedNodeIds) {
        Set<Entry<Node, Integer>> nodeEntrySet = sortedNodeIds.entrySet();
        //pro všechny uzly
        for (Entry<Node, Integer> nodeEntry : nodeEntrySet) {
            // pro všechny jejich atributy
            Set<Entry<String, Set<Object>>> atrributesEntrySet = nodeEntry.getKey().getAttributes().entrySet();
            for (Entry<String, Set<Object>> attributeEntry : atrributesEntrySet) {
                if(attributeEntry.getKey().equals(NodeFlagTask.FLAG_ATTRIBUTE_KEY) ||
                        attributeEntry.getKey().equals(IncrementalDeleteTask.FLAG_ATTRIBUTE_KEY)) {
                    // do not include flag node attribute in the output
                    continue;
                }
                // pro všechny hodnoty daného atributu
                for (Object attributeValue : attributeEntry.getValue()) {
                    try {
                        writer.writeNext(getNodeAttributeTextForm(nodeEntry.getValue(), attributeEntry.getKey(),
                                Base64AttributeCodingHelper.encodeAttribute(attributeValue)));
                    } catch (IOException e) {
                        LOGGER.error("Could not serialize object " + attributeValue, e);
                    }
                }
            }
        }
    }

    /**
     * Zapíše seznam atributů hran do výstupního writeru.
     * @param writer výstupní writer
     * @param edgeSet seznam hran, v němž se hledají atributy
     */
    private void printEdgeAtributes(CSVWriter writer, Set<Edge> edgeSet) {
        //pro všechny hrany
        int index = 0;
        for (Edge edge : edgeSet) {
            // pro všechny jejich atributy
            Set<Entry<String, Set<Object>>> atrributesEntrySet = edge.getAttributes().entrySet();
            for (Entry<String, Set<Object>> attributeEntry : atrributesEntrySet) {
                // pro všechny hodnoty daného atributu
                for (Object attributeValue : attributeEntry.getValue()) {
                    try {
                        writer.writeNext(getEdgeAttributeTextForm(index, attributeEntry.getKey(),
                                Base64AttributeCodingHelper.encodeAttribute(attributeValue)));
                    } catch (IOException e) {
                        LOGGER.error("Could not serialize object " + attributeValue, e);
                    }
                }
            }
            index++;
        }

    }

    /**
     * Vrátí textovou reprezentaci jednoho source code.
     * @param record záznam reprezentující sourc code
     * @return textová podoba source code
     */
    private String[] getSourceCodeTextForm(SourceCodeRecord record) {
        List<String> result = new ArrayList<String>();
        result.add("source_code");
        result.add(record.getId());
        result.add(record.getLocalName());
        result.add(record.getHash());
        return result.toArray(new String[result.size()]);
    }

    /**
     * Vrátí textovou reprezentaci jednoho uzlu.
     * Additionally searches for temporarily added flag node attribute and appends the flag to the node text form.
     * 
     * @param nodeToGet uzel pro zapsání 
     * @param sortedNodeIds mapa uzlů na jejich id
     * @param mapResourceToId 
     * @return textová podoba uzlu
     */
    private String[] getNodeTextForm(Node nodeToGet, Map<Node, Integer> sortedNodeIds,
            Map<Resource, Integer> mapResourceToId) {
        List<String> result = new ArrayList<String>();
        result.add("node");
        result.add(String.valueOf(sortedNodeIds.get(nodeToGet)));

        Integer parentId = sortedNodeIds.get(nodeToGet.getParent());
        if (parentId != null) {
            result.add(String.valueOf(parentId));
        } else {
            result.add("");
        }

        result.add(nodeToGet.getName());
        result.add(nodeToGet.getType());

        Integer resourceId = mapResourceToId.get(nodeToGet.getResource());
        result.add(String.valueOf(resourceId));
        // append flag REMOVE to the node text form if node has flag REMOVE attribute node
        Set<Entry<String, Set<Object>>> attributesEntrySet = nodeToGet.getAttributes().entrySet();
        for (Entry<String, Set<Object>> attributeEntry : attributesEntrySet) {
            if (NodeFlagTask.FLAG_ATTRIBUTE_KEY.equals(attributeEntry.getKey())) {
                result.add(NodeFlagTask.FLAG_ATTRIBUTE_VALUE);
                break;
            } else if (IncrementalDeleteTask.FLAG_ATTRIBUTE_KEY.equals(attributeEntry.getKey())) {
                result.add(IncrementalDeleteTask.FLAG_ATTRIBUTE_VALUE);
                break;
            }
        }
        return result.toArray(new String[result.size()]);
    }

    /**
     * Vrátí textovou reprezentaci jedné hrany.
     * @param index 
     * @param edge zkoumaná hrana
     * @param sortedNodeIds seřazené uzly jako indexy do mapy s jejich id
     * @param mapResourceToId 
     * @return textová reprezentace hrany
     */
    private String[] getEdgeTextForm(int index, Edge edge, Map<Node, Integer> sortedNodeIds,
            Map<Resource, Integer> mapResourceToId) {

        Integer sourceId = sortedNodeIds.get(edge.getSource());
        Integer targetId = sortedNodeIds.get(edge.getTarget());
        if (sourceId != null && targetId != null) {
            List<String> result = new ArrayList<String>();
            result.add("edge");
            result.add(String.valueOf(index));
            result.add(String.valueOf(sourceId));
            result.add(String.valueOf(targetId));
            result.add(edge.getType().toString());
            Integer resourceId = mapResourceToId.get(edge.getResource());
            result.add(String.valueOf(resourceId));
            return result.toArray(new String[result.size()]);
        } else {
            return null;
        }
    }

    /**
     * Vrati textovou reprezentaci vrstvy.
     * @param layerID ID vrstvy.
     * @param layer Vrstva, pro niz se stavi textova reprezentace
     * @return Textova reprezentace vrstvy.
     */
    private String[] getLayerTextForm(int layerID, Layer layer) {
        if (layer == null) {
            layer = Layer.DEFAULT;
        }
        List<String> result = new ArrayList<String>();
        result.add("layer");
        result.add(String.valueOf(layerID));
        result.add(layer.getName());
        result.add(layer.getType());
        return result.toArray(new String[result.size()]);
    }

    /**
     * Vrátí textovou reprezentaci resource.
     * @param resourceID id resource
     * @param resource resource, pro nějž se staví textová reprezentace
     * @param mapLayerToId 
     * @return textová reprezentace resource
     */
    private String[] getResourceTextForm(int resourceID, Resource resource, Map<Layer, Integer> mapLayerToId) {
        if (resource == null) {
            resource = DEFAULT_RESOURCE;
        }
        List<String> result = new ArrayList<String>();
        result.add("resource");
        result.add(String.valueOf(resourceID));
        result.add(resource.getName());
        result.add(resource.getType());
        result.add(resource.getDescription());
        result.add(String.valueOf(mapLayerToId.get(resource.getLayer())));
        return result.toArray(new String[result.size()]);
    }

    /**
     * Vrátí textovou reprezentaci atributu uzlu.
     * @param nodeId id uzlu
     * @param key klíč atributu
     * @param value hodnota atributu
     * @return textová reprezentace atributu uzlu
     */
    private String[] getNodeAttributeTextForm(Integer nodeId, String key, String value) {
        List<String> result = new ArrayList<String>();
        result.add("node_attribute");
        result.add(String.valueOf(nodeId));
        result.add(key);
        result.add(value);
        return result.toArray(new String[result.size()]);
    }

    /**
     * Vrátí textovou reprezentaci atributu hrany.
     * @param edgeId id hrany
     * @param key klíš atributu
     * @param value hodnota atributu
     * @return textová reprezentace atributu
     */
    private String[] getEdgeAttributeTextForm(int edgeId, String key, String value) {
        List<String> result = new ArrayList<String>();
        result.add("edge_attribute");
        result.add(String.valueOf(edgeId));
        result.add(key);
        result.add(value);
        return result.toArray(new String[result.size()]);
    }

    /**
     * @return minimální počet uzlů grafu, aby se graf zapsal. 
     */
    public int getSendingGraphSize() {
        return sendingGraphSize;
    }

    /**
     * @param sendingGraphSize minimální počet uzlů grafu, aby se graf zapsal.
     */
    public void setSendingGraphSize(int sendingGraphSize) {
        this.sendingGraphSize = sendingGraphSize;
    }

}
