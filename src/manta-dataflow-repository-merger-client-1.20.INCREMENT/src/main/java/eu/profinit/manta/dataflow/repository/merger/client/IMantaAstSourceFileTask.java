package eu.profinit.manta.dataflow.repository.merger.client;

import eu.profinit.manta.ast.IMantaAstNode;
import eu.profinit.manta.dataflow.model.Graph;

/**
 * Task sloužící k doplnění umístění zdrojového skriptu, ve kterých se nachází konkrétní uzly.
 * Varianta, kdy se umístění souboru bere z AST pomocí XPATHu.
 * @author tfechtner
 *
 */
public class IMantaAstSourceFileTask extends AbstractSourceFileTask<IMantaAstNode> {
    /** XPath pro nalezení souboru v AST. */
    private String fileNameXpath;

    @Override
    protected String findInputName(IMantaAstNode input, Graph graph) {
        Object value = input.selectSingleNode(fileNameXpath);
        if (value != null) {
            return value.toString();
        } else {
            return null;
        }
    }

    /**
     * @return XPath pro nalezení souboru v AST. 
     */
    public String getFileNameXpath() {
        return fileNameXpath;
    }

    /**
     * @param fileNameXpath XPath pro nalezení souboru v AST. 
     */
    public void setFileNameXpath(String fileNameXpath) {
        this.fileNameXpath = fileNameXpath;
    }
}
