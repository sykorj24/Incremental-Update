package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeMapping;

/**
 * Služba, která nenahrává skripty na server a pouze nastavuje jméno podle cesty k souboru.
 * @author tfechtner
 *
 */
public class DirectSourceCodeService implements SourceCodeFileService {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DirectSourceCodeService.class);
    /** Základní požadovaná lokace souboru, která se prefixne výsledku. */
    private String baseLocation = "";
    /** Adresář, od kterého je cesta k souboru zajímavá. */
    private String baseDir = "";
    /** Mapování lokálního id source code na soubor. */
    private final Map<String, File> sourceCodeById = new HashMap<>();

    @Override
    public String resolveFile(File file) {
        String fullPath = FilenameUtils.normalize(file.getAbsolutePath());
        String basePath = FilenameUtils.normalize(baseDir);
        
        boolean contains = false;
        try {
            contains = FilenameUtils.directoryContains(basePath, fullPath);
        } catch (IOException e) {
            LOGGER.error("Error during comparing base dir and current file.", e);
        }
        
        String filePath;
        if (contains) {
            filePath = baseLocation + FilenameUtils.separatorsToUnix(fullPath.substring(basePath.length()));
        } else {
            filePath = baseLocation + FilenameUtils.separatorsToUnix(fullPath);
        }
        
        sourceCodeById.put(filePath, file);
        return filePath;
    }

    @Override
    public SourceCodeRecord getSourceCode(String id) {
        File file = sourceCodeById.get(id);
        
        String fileHash;
        if (file != null) {
            fileHash = SourceHash.computeHash(file);
        } else {
            LOGGER.error("No source file found on path: " + id);
            fileHash = null;
        }
        
        return new SourceCodeRecord(id, id, fileHash);
    }

    @Override
    public void planUpload(SourceCodeMapping mapping) {
        // NOOP
    }

    /**
     * @return Základní požadovaná lokace souboru, která se prefixne výsledku
     */
    public String getBaseLocation() {
        return baseLocation;
    }

    /**
     * @param baseLocation Základní požadovaná lokace souboru, která se prefixne výsledku
     */
    public void setBaseLocation(String baseLocation) {
        if (baseLocation != null) {
            this.baseLocation = baseLocation;
        } else {
            this.baseLocation = "";
        }
    }

    /**
     * @return adresář, od kterého je cesta k souboru zajímavá
     */
    public String getBaseDir() {
        return baseDir;
    }

    /**
     * @param baseDir adresář, od kterého je cesta k souboru zajímavá
     */
    public void setBaseDir(String baseDir) {
        if (baseDir != null) {
            this.baseDir = baseDir;
        } else {
            this.baseDir = "";
        }
    }

}
