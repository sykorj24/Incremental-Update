package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.dataflow.model.Graph;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.platform.automation.AbstractTask;

/**
 * Spolený předek pro úlohy naplňující source code.
 * @author tfechtner
 *
 * @param <I> typ vstupu
 */
public abstract class AbstractSourceFileTask<I> extends AbstractTask<I, Graph> {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(SourceFileTask.class);
    /** Výchozí název atributu, kam se ukládá umístění. */
    public static final String DEFAULT_SOURCE_LOCATION = "sourceLocation";
    /** Název atributu, kam se ukládá encoding. */
    public static final String SOURCE_ENCODING = "sourceEncoding";
    /** Název atributu, kam se ukládá umístění. */
    private String sourceLocationProperty = DEFAULT_SOURCE_LOCATION;
    /** Seznam typů uzlů, do kterých se přidává informace o umístění. */
    private List<String> editedNodeTypeList = Collections.emptyList();
    /** Encoding vstupních souborů . */
    private String encoding = "utf-8";
    /** Služba zajišťující mergování souborů na server. */
    private SourceCodeFileService sourceCodeService;
    /** Adresář, kde jsou umístěny soubory, které se mají mapovat. */
    private File baseDir;
    /** Množina typů resourců pro skriptové uzly. */
    private Set<String> scriptResourceTypes = Collections.emptySet();

    @Override
    protected void doExecute(I input, Graph graph) {
        if (editedNodeTypeList.isEmpty() || scriptResourceTypes.isEmpty()) {
            return;
        }

        long start = System.currentTimeMillis();
        Validate.notNull(sourceCodeService, "Source code service must not be null.");
        Validate.notNull(baseDir, "Base dir must not be null.");

        String inputName = findInputName(input, graph);
        if (inputName == null) {
            LOGGER.error("The input name is null.");
            return;
        }
        String sourceCodeId = null;

        // nastavit na všechny příslušné uzly
        for (Node node : graph.nodeSet()) {
            if (editedNodeTypeList.contains(node.getType()) && hasScriptChild(node)) {
                if (sourceCodeId == null) {
                    sourceCodeId = resolveFile(inputName);
                    if (sourceCodeId == null) {
                        return;
                    }
                }
                node.addAttribute(sourceLocationProperty, sourceCodeId);
                node.addAttribute(SOURCE_ENCODING, encoding);
            }
        }
        LOGGER.debug("{}", System.currentTimeMillis() - start);
    }

    /**
     * Pomocí služby se dotáže na server a zjistí jeho id.
     * @param inputName název souboru
     * @return id zdrojáku
     */
    private String resolveFile(String inputName) {
        File file = new File(baseDir, inputName);
        String sourceCodeId = sourceCodeService.resolveFile(file);
        if (StringUtils.isEmpty(sourceCodeId)) {
            LOGGER.warn("Source code id for the {} was empty.", inputName);
            return null;
        }
        return sourceCodeId;
    }

    /**
     * Jestliže uzel má alespoň jednoho potomka ze skriptvým resourcem.
     * @param node ověřovaný uzel
     * @return true, jestliže má alespoň jednoho skriptového potomka
     */
    private boolean hasScriptChild(Node node) {
        for (Node child : node.getChildren()) {
            if (child.getResource() != null && scriptResourceTypes.contains(child.getResource().getType())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Metoda získá název souboru ze vstupu nebo grafu.
     * @param input vstup ke zpracování
     * @param graph výsledný graf
     * @return název vstupu nebo null
     */
    protected abstract String findInputName(I input, Graph graph);

    /**
     * @return Seznam typů uzlů, do kterých se přidává informace o umístění.
     */
    public List<String> getEditedNodeTypeList() {
        return editedNodeTypeList;
    }

    /**
     * @param editedNodeTypeList Seznam typů uzlů, do kterých se přidává informace o umístění.
     */
    public void setEditedNodeTypeList(List<String> editedNodeTypeList) {
        this.editedNodeTypeList = editedNodeTypeList;
    }

    /**
     * @return Encoding vstupních souborů.
     */
    public String getEncoding() {
        return encoding;
    }

    /**
     * @param encoding Encoding vstupních souborů. 
     */
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    /**
     * @return Služba zajišťující mergování souborů na server. 
     */
    public SourceCodeFileService getSourceCodeService() {
        return sourceCodeService;
    }

    /**
     * @param sourceCodeService Služba zajišťující mergování souborů na server. 
     */
    public void setSourceCodeService(SourceCodeFileService sourceCodeService) {
        this.sourceCodeService = sourceCodeService;
    }

    /**
     * @return Adresář, kde jsou umístěny soubory, které se mají mapovat. 
     */
    public File getBaseDir() {
        return baseDir;
    }

    /**
     * @param baseDir Adresář, kde jsou umístěny soubory, které se mají mapovat. 
     */
    public void setBaseDir(File baseDir) {
        this.baseDir = baseDir;
    }

    /**
     * @return Množina typů resourců pro skriptové uzly. 
     */
    public Set<String> getScriptResourceTypes() {
        return scriptResourceTypes;
    }

    /**
     * @param scriptResourceTypes Množina typů resourců pro skriptové uzly. 
     */
    public void setScriptResourceTypes(Set<String> scriptResourceTypes) {
        this.scriptResourceTypes = scriptResourceTypes;
    }

    /**
     * @return Název atributu, kam se ukládá umístění
     */
    public String getSourceLocationProperty() {
        return sourceLocationProperty;
    }

    /**
     * @param sourceLocationProperty Název atributu, kam se ukládá umístění
     */
    public void setSourceLocationProperty(String sourceLocationProperty) {
        this.sourceLocationProperty = sourceLocationProperty;
    }

}
