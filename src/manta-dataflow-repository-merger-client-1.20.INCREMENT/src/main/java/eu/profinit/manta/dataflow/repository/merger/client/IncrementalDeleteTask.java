package eu.profinit.manta.dataflow.repository.merger.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.dataflow.model.Graph;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.platform.automation.AbstractTask;

/**
 * Create a new graph containing only paths to the specified nodes. Additionally, append a special flag
 * to these specified nodes, so these nodes are removed in the new revision (including their whole subtree).
 * 
 * @author jsykora
 *
 * @param <I> input type
 */
public class IncrementalDeleteTask<I> extends AbstractTask<I, Graph> {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(IncrementalDeleteTask.class);

    /** Attribute key of the flag. 
     *  Attribute key must be unique throughout all user's attribute keys (no collision with user's attributes may occur).
     */
    public static final String FLAG_ATTRIBUTE_KEY = "_flagDelete_";
    /** Attribute value of the flag REMOVE */
    public static final String FLAG_ATTRIBUTE_VALUE = "removeMyself";

    /** List of nodes types that will have created single path and will have added an additional flag */
    private List<String> nodeTypeList = Collections.emptyList();

    @Override
    protected void doExecute(I input, Graph graph) {
        if (nodeTypeList.isEmpty()) {
            LOGGER.warn("No node type specified. Perform nothing.");
            return;
        }

        LOGGER.info("Starting execution");

        // List of all nodes' predecessors.
        // Single Linked list of nodes represents a list of one node's predecessors
        List<LinkedList<Node>> allNodesPredecessors = new ArrayList<>();

        // First step: Find all specified nodes and save their path (their list of predecessors)
        for (Node node : graph.nodeSet()) {
            if (nodeTypeList.contains(node.getType())) {
                // get node's predecessors
                LinkedList<Node> oneNodePredecessors = getNodePredecessors(node);

                // add node itself to the end of the list of its predecessors
                oneNodePredecessors.addLast(node);

                // add node's predecessors to the list of all nodes' predecessors
                allNodesPredecessors.add(oneNodePredecessors);
            }
        }

        // Second step: Clear the whole graph (remove all nodes and edges)
        graph.clear();

        // Third step: Create graph containing only nodes with their direct predecessors
        for (LinkedList<Node> oneNodePredecessors : allNodesPredecessors) {
            int i = 0;
            for (Node node : oneNodePredecessors) {
                if (i == oneNodePredecessors.size() - 1) {
                    // Add last node and append flag to this last node
                    Node leafNode = graph.addNode(node.getName(), node.getType(), node.getParent(), node.getResource());
                    leafNode.addAttribute(FLAG_ATTRIBUTE_KEY, FLAG_ATTRIBUTE_VALUE);
                } else {
                    // Only add predecessor
                    graph.addNode(node.getName(), node.getType(), node.getParent(), node.getResource());
                }
                i++;
            }
        }
    }

    /**
     * Get predecessors of a node. At first position is root of predecessors, at last position is direct parent of the starting node.
     * 
     * @param node  node for which to get its all predecessors
     * @return node's predecessors
     */
    private LinkedList<Node> getNodePredecessors(Node node) {
        LinkedList<Node> nodePredecessors = new LinkedList<>();
        Node parent = node.getParent();
        // iterate through all node's direct parents (i.e. its predecessors starting from the closes one)
        while (parent != null) {
            nodePredecessors.addFirst(parent);
            parent = parent.getParent();
        }

        return nodePredecessors;
    }

    public List<String> getNodeTypeList() {
        return nodeTypeList;
    }

    public void setNodeTypeList(List<String> nodeTypeList) {
        this.nodeTypeList = nodeTypeList;
    }
}
