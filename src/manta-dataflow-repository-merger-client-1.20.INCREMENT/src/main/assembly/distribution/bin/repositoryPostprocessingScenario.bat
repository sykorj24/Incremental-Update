@echo off
echo ---------------------------------------------------------------------
echo Manta Repository Postprocessing Scenario (version ${version})
echo ---------------------------------------------------------------------

echo repositoryPostprocessingScenario start: %DATE%, %TIME%
cd "%~dp0"
set MANTA_LICENSE_LOADER=eu.profinit.manta.platform.licensing.LicenseLoaderImpl
call "%~dp0..\..\..\platform\bin\mantar.bat" ${manta.module.name} repositoryPostprocessingScenario
echo repositoryPostprocessingScenario end: %DATE%, %TIME%
echo.