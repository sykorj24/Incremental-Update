@echo off
echo ---------------------------------------------------------------------
echo Manta New Revision Scenario (version ${version})
echo ---------------------------------------------------------------------

echo newRevisionScenario start: %DATE%, %TIME%
cd "%~dp0"
set MANTA_LICENSE_LOADER=eu.profinit.manta.platform.licensing.LicenseLoaderImpl
call "%~dp0..\..\..\platform\bin\mantar.bat" ${manta.module.name} newRevisionScenario
echo newRevisionScenario end: %DATE%, %TIME%
echo.