#!/bin/bash
echo ---------------------------------------------------------------------
echo "Manta Prune Revisions Scenario (version ${version})"
echo ---------------------------------------------------------------------

cd `dirname $0`
SCRIPT_DIR=`pwd`
export MANTA_LICENSE_LOADER=eu.profinit.manta.platform.licensing.LicenseLoaderImpl
bash "$SCRIPT_DIR/../../../platform/bin/mantar.sh" ${manta.module.name} pruneRevisionScenario
cd -
echo
