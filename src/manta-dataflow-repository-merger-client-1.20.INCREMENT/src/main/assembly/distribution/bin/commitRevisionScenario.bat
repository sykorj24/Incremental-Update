@echo off
echo ---------------------------------------------------------------------
echo Manta Commit Revision Scenario (version ${version})
echo ---------------------------------------------------------------------

echo commitRevisionScenario start: %DATE%, %TIME%
cd "%~dp0"
set MANTA_LICENSE_LOADER=eu.profinit.manta.platform.licensing.LicenseLoaderImpl
call "%~dp0..\..\..\platform\bin\mantar.bat" ${manta.module.name} commitRevisionScenario
echo commitRevisionScenario end: %DATE%, %TIME%
echo.