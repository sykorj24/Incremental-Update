@echo off
echo ---------------------------------------------------------------------
echo Manta Prune Revisions Scenario (version ${version})
echo ---------------------------------------------------------------------

echo pruneRevisionScenario start: %DATE%, %TIME%
cd "%~dp0"
set MANTA_LICENSE_LOADER=eu.profinit.manta.platform.licensing.LicenseLoaderImpl
call "%~dp0..\..\..\platform\bin\mantar.bat" ${manta.module.name} pruneRevisionScenario
echo pruneRevisionScenario end: %DATE%, %TIME%
echo.