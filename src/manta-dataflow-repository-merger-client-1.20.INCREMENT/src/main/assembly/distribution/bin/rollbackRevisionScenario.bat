@echo off
echo ---------------------------------------------------------------------
echo Manta Rollback Revision Scenario (version ${version})
echo ---------------------------------------------------------------------

echo rollbackRevisionScenario start: %DATE%, %TIME%
cd "%~dp0"
set MANTA_LICENSE_LOADER=eu.profinit.manta.platform.licensing.LicenseLoaderImpl
call "%~dp0..\..\..\platform\bin\mantar.bat" ${manta.module.name} rollbackRevisionScenario
echo rollbackRevisionScenario end: %DATE%, %TIME%
echo.