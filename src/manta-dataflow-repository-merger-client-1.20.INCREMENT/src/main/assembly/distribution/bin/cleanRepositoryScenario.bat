@echo off
echo ---------------------------------------------------------------------
echo Manta Clean Repository Scenario (version ${version})
echo ---------------------------------------------------------------------

echo cleanRepositoryScenario start: %DATE%, %TIME%
cd "%~dp0"
set MANTA_LICENSE_LOADER=eu.profinit.manta.platform.licensing.LicenseLoaderImpl
call "%~dp0..\..\..\platform\bin\mantar.bat" ${manta.module.name} cleanRepositoryScenario
echo cleanRepositoryScenario end: %DATE%, %TIME%
echo.