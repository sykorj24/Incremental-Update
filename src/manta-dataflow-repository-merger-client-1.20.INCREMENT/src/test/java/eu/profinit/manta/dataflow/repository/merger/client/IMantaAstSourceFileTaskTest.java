package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.File;
import java.util.Map;

import org.junit.Test;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import eu.profinit.manta.dataflow.generator.oracle.scenario.OracleScenario;
import junit.framework.Assert;

public class IMantaAstSourceFileTaskTest {

    @Test
    public void testFulDataFlow() {
        FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext(
                "file:src/test/resources/plsqlDataflowScenario.xml");
        OracleScenario scenario = context.getBean("plsqlDataflowScenario", OracleScenario.class);

        LocationWriter writer = new LocationWriter();
        scenario.setOutputWriter(writer);
        scenario.execute();

        Map<String, String> locations = writer.getLocations();
        Assert.assertEquals(1, locations.size());
        Assert.assertEquals(new File("src/test/resources/inputScript.sql").getPath(), locations.get("Procedure_Name"));

        context.close();
    }
}
