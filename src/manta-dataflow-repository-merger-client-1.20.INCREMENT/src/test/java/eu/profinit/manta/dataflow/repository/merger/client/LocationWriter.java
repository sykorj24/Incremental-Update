package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import eu.profinit.manta.dataflow.model.Graph;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.platform.automation.OutputWriter;

/**
 * Pomocná třída pro testování, která sbírá umístění souborů.
 * @author tfechtner
 *
 */
public class LocationWriter implements OutputWriter<Graph> {

    private Map<String, String> locations = new HashMap<String, String>();
    
    @Override
    public void write(Graph input) throws IOException {
        for (Node node: input.nodeSet()) {
            Set<Object> atts = node.getAttributeSet(SourceFileTask.DEFAULT_SOURCE_LOCATION);
            if (!atts.isEmpty()) {
                locations.put(node.getName(), (String)atts.iterator().next());
            }
        }
        
    }

    /**
     * @return sesbírané umístění
     */
    public Map<String, String> getLocations() {
        return Collections.unmodifiableMap(locations);
    }
    
    @Override
    public void close() {
        // NOOP        
    }
    
}
