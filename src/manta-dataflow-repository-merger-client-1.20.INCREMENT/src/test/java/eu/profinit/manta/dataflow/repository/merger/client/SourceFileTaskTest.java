package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.File;
import java.util.Collections;

import org.junit.Test;

import eu.profinit.manta.dataflow.model.Graph;
import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.dataflow.model.Resource;
import eu.profinit.manta.dataflow.model.impl.GraphImpl;
import junit.framework.Assert;

public class SourceFileTaskTest {
    @Test
    public void testExecute() {
        SourceFileTask<Integer> task = new SourceFileTask<Integer>();
        task.setSourceCodeService(new DummySourceCodeService());
        task.setBaseDir(new File(""));
        Resource resource = new Resource() {
            @Override
            public String getType() {
                return "Oracle scripts";
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public String getDescription() {
                return null;
            }

            @Override
            public Layer getLayer() {
                return null;
            }
        };
        Graph graph = new GraphImpl(resource);
        Node parent = graph.addNode("a", "b", null, resource);
        Node script = graph.addNode("script.sql", "PLSQL Script", parent, resource);
        graph.addNode("Block", "PLSQL block", script, resource);

        task.setScriptNodeType("PLSQL Script");
        task.setEditedNodeTypeList(Collections.singletonList("PLSQL Script"));
        task.setScriptResourceTypes(Collections.singleton("Oracle scripts"));
        task.doExecute(null, graph);

        Assert.assertEquals("/a/script.sql",
                ((String) script.getAttributeSet(SourceFileTask.DEFAULT_SOURCE_LOCATION).iterator().next()).replace("\\", "/"));
    }

    public static class DummySourceCodeService extends StandardSourceCodeService {
        @Override
        public synchronized String resolveFile(File file) {
            return file.getPath();
        }
    }
}
