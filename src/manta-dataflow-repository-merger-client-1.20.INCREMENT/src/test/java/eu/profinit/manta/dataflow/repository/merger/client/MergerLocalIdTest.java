package eu.profinit.manta.dataflow.repository.merger.client;

import java.io.CharArrayWriter;
import java.io.IOException;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.Test;

import eu.profinit.manta.dataflow.model.Edge;
import eu.profinit.manta.dataflow.model.Graph;
import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.dataflow.model.Resource;
import eu.profinit.manta.dataflow.model.impl.GraphImpl;
import eu.profinit.manta.dataflow.model.impl.LayerImpl;
import eu.profinit.manta.dataflow.model.impl.ResourceImpl;
import junit.framework.Assert;

/**
 * Test lokálního mergeru.
 * @author tfechtner
 *
 */
public class MergerLocalIdTest {
    /**
     * Otestuje tvorbu dat.
     * @throws IOException chyba při čtení
     */
    @Test
    public void testCreateData() throws IOException {
        MergerWriter merger = new MergerWriter();

        Layer layerPhysical = new LayerImpl("Physical", "Physical");
        Resource resourcePhysical = new ResourceImpl("resource", "resource", "resource", layerPhysical);

        Layer layerBusiness = new LayerImpl("Business", "Business");
        Resource resourceBusiness = new ResourceImpl("resource2", "resource2", "resource2", layerBusiness);

        Graph g1 = new GraphImpl(resourcePhysical);
        Node g1n1 = g1.addNode("n1", "type", null, resourcePhysical);
        Node g1n2 = g1.addNode("n2", "type", g1n1, resourcePhysical);
        Node g1nb1 = g1.addNode("nb1", "attribute", null, resourceBusiness);

        g1.addEdge(g1n1, g1n2, Edge.Type.DIRECT);
        g1.addEdge(g1n1, g1nb1, Edge.Type.MAPS_TO);

        byte[] bytes1 = merger.createData(g1);
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        byteStream.write(bytes1);
        
        CharArrayWriter expected = new CharArrayWriter();
        expected.write("\"layer\",\"0\",\"Physical\",\"Physical\"\n");
        expected.write("\"layer\",\"1\",\"Business\",\"Business\"\n");
        expected.write("\"resource\",\"2\",\"resource\",\"resource\",\"resource\",\"0\"\n");
        expected.write("\"resource\",\"3\",\"resource2\",\"resource2\",\"resource2\",\"1\"\n");
        expected.write("\"node\",\"4\",\"\",\"nb1\",\"attribute\",\"3\"\n");
        expected.write("\"node\",\"5\",\"\",\"n1\",\"type\",\"2\"\n");
        expected.write("\"node\",\"6\",\"5\",\"n2\",\"type\",\"2\"\n");
        expected.write("\"edge\",\"0\",\"5\",\"6\",\"DIRECT\",\"2\"\n");
        expected.write("\"edge\",\"1\",\"5\",\"4\",\"MAPS_TO\",\"2\"\n");
        
        Assert.assertEquals(expected.toString(), byteStream.toString());
        byteStream.close();
        merger.close();
    }
    
    /**
     * Otestuje tvorbu dat z divných jmen.
     * @throws IOException chyba při čtení
     */
    @Test
    public void testWeirdNames() throws IOException {
        MergerWriter merger = new MergerWriter();

        Resource resource = new ResourceImpl("reso,urce", " resource", "resource");

        Graph g1 = new GraphImpl(resource);
        Node g1n1 = g1.addNode("n1\na", "type", null, resource);
        Node g1n2 = g1.addNode("n2\"", "type", g1n1, resource);

        g1.addEdge(g1n1, g1n2, Edge.Type.DIRECT);

        byte[] bytes1 = merger.createData(g1);
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        byteStream.write(bytes1);
        
        CharArrayWriter expected = new CharArrayWriter();
        expected.write("\"layer\",\"0\",\"Physical\",\"Physical\"\n");
        expected.write("\"resource\",\"1\",\"reso,urce\",\" resource\",\"resource\",\"0\"\n");
        expected.write("\"node\",\"2\",\"\",\"n1\na\",\"type\",\"1\"\n");
        expected.write("\"node\",\"3\",\"2\",\"n2\\\"\",\"type\",\"1\"\n");
        expected.write("\"edge\",\"0\",\"2\",\"3\",\"DIRECT\",\"1\"\n");
        
        Assert.assertEquals(expected.toString(), byteStream.toString());
        byteStream.close();
        merger.close();
    }
}
