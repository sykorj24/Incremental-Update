package eu.profinit.manta.dataflow.repository.core.model;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;

/**
 * Rozhraní pro rozhodování o hranách.
 *
 * @author Erik Kratochvíl
 */
public interface EdgePredicate {

    /**
     * Vyhodnotí, zda hrana splňuje podmínku.
     *
     * @param edge hrana grafu
     * @param direction směr, ve kterém je hrana procházena (OUT = po směru hrany, IN = proti směru hrany)
     * @return true, pokud hrana splňuje podmínku
     */
    boolean evaluate(Edge edge, Direction direction);

}
