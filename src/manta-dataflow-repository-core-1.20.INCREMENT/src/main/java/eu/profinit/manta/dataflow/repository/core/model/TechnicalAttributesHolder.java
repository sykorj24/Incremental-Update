package eu.profinit.manta.dataflow.repository.core.model;


/**
 * Rozhraní pro uchovávání informace o tom, které atributy jsou technické a 
 * tudíš se nemají exportovat, zobrazovat apod. 
 * @author tfechtner
 *
 */
public interface TechnicalAttributesHolder {
    
    /**
     * Ověří jestli je daný atribut pro uzel technický.
     * @param attr jméno atributu k ověření
     * @return true, jestliže je atribut technický
     */
    boolean isNodeAttributeTechnical(String attr);

    /**
     * Ověří jestli je daný atribut pro hranu technický.
     * @param attr jméno atributu k ověření
     * @return true, jestliže je atribut technický
     */
    boolean isEdgeAttributeTechnical(String attr);
}
