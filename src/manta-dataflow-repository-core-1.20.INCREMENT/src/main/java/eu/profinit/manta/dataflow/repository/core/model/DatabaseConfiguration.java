package eu.profinit.manta.dataflow.repository.core.model;

/**
 * Datová třída udržující konfiguraci pro danou instanci databáze.
 * @author tfechtner
 *
 */
public final class DatabaseConfiguration {
    /** Přepínač, jestli má být vytvořen index pro node name.*/
    private boolean hasNodeNameIndex;

    /**
     * @return True, jestli má být vytvořen index pro node name.
     */
    public boolean hasNodeNameIndex() {
        return hasNodeNameIndex;
    }

    /**
     * @param hasNodeNameIndex True, jestli má být vytvořen index pro node name.
     */
    public void setHasNodeNameIndex(boolean hasNodeNameIndex) {
        this.hasNodeNameIndex = hasNodeNameIndex;
    }
}
