package eu.profinit.manta.dataflow.repository.core.model;

/**
 * Úroveň exkluzivity transakce ovlivňující povolené operace a zámky.
 * @author tfechtner
 *
 */
public enum TransactionLevel {
    /** Čtecí transakce, zakazující zápis ve stejnou chvíli. */
    READ,
    /** Čtecí transakce, umozňuje ve stejnou chvíli i zapisovat. */
    READ_NOT_BLOCKING,
    /** Exkluzivní zapisovací transakce. */
    WRITE_EXCLUSIVE,
    /** Zapisovací transakce umožňující zápis pro několik transakcí s WRITE_SHARE. */
    WRITE_SHARE,
    /** Zapisovací transakce nad source_code podstromem. */
    WRITE_SOURCE_CODE;

}
