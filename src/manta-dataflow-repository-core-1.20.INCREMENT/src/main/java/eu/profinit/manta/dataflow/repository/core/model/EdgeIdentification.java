package eu.profinit.manta.dataflow.repository.core.model;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.thinkaurelius.titan.graphdb.relations.RelationIdentifier;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;

import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;

/**
 * Unikátní identifikace hrany v rámci dané revizi.
 * @author tfechtner
 *
 */
public class EdgeIdentification {
    /** ID startovního vrcholu. */
    private final long startId;
    /** ID koncového vrcholu. */
    private final long endId;
    /** Label hrany. */
    private final EdgeLabel label;
    /** Id relace v hrany v titanu, nepoužívá se v equals. */
    private final Long relationId;

    /**
     * @param edge titan hrana, pr kterou se vytváří identifikace
     */
    public EdgeIdentification(Edge edge) {
        Validate.notNull(edge, "Edge object must not be null.");
        this.startId = (long) edge.getVertex(Direction.OUT).getId();
        this.endId = (long) edge.getVertex(Direction.IN).getId();
        this.label = EdgeLabel.parseFromDbType(edge.getLabel());
        this.relationId = ((RelationIdentifier) edge.getId()).getLongRepresentation()[2];
    }

    /**
     * @param startId ID startovního vrcholu
     * @param endId ID koncového vrcholu
     * @param label Label hrany
     */
    public EdgeIdentification(long startId, long endId, EdgeLabel label) {
        super();
        this.startId = startId;
        this.endId = endId;
        this.label = label;
        this.relationId = null;
    }

    /**
     * @return ID startovního vrcholu
     */
    public long getStartId() {
        return startId;
    }

    /**
     * @return ID koncového vrcholu
     */
    public long getEndId() {
        return endId;
    }

    /**
     * @return Label hrany
     */
    public EdgeLabel getLabel() {
        return label;
    }

    /**
     * @return Id relace v hrany v titanu, nepoužívá se v equals. 
     */
    public Long getRelationId() {
        return relationId;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(startId);
        builder.append(endId);
        builder.append(label);
        return builder.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        EdgeIdentification other = (EdgeIdentification) obj;
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(startId, other.startId);
        builder.append(endId, other.endId);
        builder.append(label, other.label);
        return builder.isEquals();
    }

    @Override
    public String toString() {
        return startId + "-" + label + "->" + endId + "[" + relationId + "]";
    }
}