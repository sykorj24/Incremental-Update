package eu.profinit.manta.dataflow.repository.core.model;



/**
 * Rozhraní pro holdery databází.
 * @author tfechtner
 *
 */
public interface DatabaseHolder {
    /**
     * Inicializace holderu.
     */
    void init();
    
    /**
     * Spustit callback kód s transakcí.
     * @param level vyžadovaná úroveň přístupu pro práci s transakcí  
     * @param callback kód ke spuštění
     * @return výsledek spuštěného kódu
     * @param <T> návratový typ spouštěného kódu 
     */
    <T> T runInTransaction(TransactionLevel level, TransactionCallback<T> callback);

    /**
     * Zavře databázi, uvolní resourcy a zneplatní všechny otevřené transakce.
     */
    void closeDatabase();
    
    /**
     * Smaže kompletně obsah databáze, bez návratu.
     */
    void truncateDatabase();
    
    /**
     * @return konfigurace databáze jako např indexy
     */
    DatabaseConfiguration getConfiguration();
}
