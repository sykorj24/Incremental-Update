package eu.profinit.manta.dataflow.repository.core.model;

import com.thinkaurelius.titan.core.TitanTransaction;

/**
 * Rozhraní pro obalení kódu prováděného v transakcích.
 * @author tfechtner
 *
 * @param <T> návratový typ spouštěného kódu
 */
public interface TransactionCallback<T> {

    /**
     * Vrací název modulu daného callbacku.
     * @return název modulu
     */
    String getModuleName();
    
    /**
     * Provede daný kód s danou transakcí.
     * @param transaction poskytnutá transakce pro provedení kódu
     * @return výsledek prováděného kódu
     */
    T callMe(TitanTransaction transaction);
}
