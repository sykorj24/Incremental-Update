package eu.profinit.manta.dataflow.repository.core.model;

/**
 * @author Matyas Krutsky <matyas.krutsky@profinit.eu>
 */
public enum FlowLevel {
    /** Vrcholové uzly - např. databáze.*/
    TOP(0),
    /** Střední úroveň uzlů - např. tabulka.*/
    MIDDLE(1),
    /** Listové uzly - např. sloupce.*/
    BOTTOM(2);

    /** Číselná úroveň instance pro porovnání.  */
    private int level;

    /**
     * @param level číselná úroveň instance
     */
    FlowLevel(int level) {
        this.level = level;
    }

    /**
     * Vrátí enum odpovídající danému jménu.
     * @param name jméno enumu, který se hledá
     * @return enum odpovídající názvu
     * @throw IllegalArgumentException jestliže neexistuje enum s odpovídajícím jménem
     */
    public static FlowLevel parseFromName(String name) {
        for (FlowLevel l : values()) {
            if (l.name().equalsIgnoreCase(name)) {
                return l;
            }
        }

        throw new IllegalArgumentException("Unknown name of enum " + name + ".");
    }

    /**
     * Porovná dva enumy a vrátí který je výš v hierarchii.
     * @param other druhý enum, se kterým se porovnává
     * @return true, jestliže je aktuální výš nebo rovno předanému, včetně null
     */
    public boolean isHigherThan(FlowLevel other) {
        if (other != null) {
            return this.level <= other.level;
        } else {
            return true;
        }
    }

    /**
     * @return číselná hodnota úrovně
     */
    public int getLevel() {
        return level;
    }
}
