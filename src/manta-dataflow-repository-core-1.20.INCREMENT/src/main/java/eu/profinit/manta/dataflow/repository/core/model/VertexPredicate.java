package eu.profinit.manta.dataflow.repository.core.model;

import com.tinkerpop.blueprints.Vertex;

/**
 * Rozhraní v dialektu pro definici jestli daný vrchol splňuje konkrétní podmínky.
 * @author tfechtner
 *
 */
public interface VertexPredicate {
    
    /**
     * Ověří jestli daný vrchol splňuje podmínku.
     * @param vertex vrchol k ověření
     * @param revisionInterval interval revizí, pro které se predikát ověřuje
     * @return true, jestliže splňuje podmínku
     */
    boolean evaluate(Vertex vertex, RevisionInterval revisionInterval);
}
