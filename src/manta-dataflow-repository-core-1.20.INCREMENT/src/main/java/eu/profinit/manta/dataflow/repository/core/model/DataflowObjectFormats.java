package eu.profinit.manta.dataflow.repository.core.model;

/**
 * Třída obsahující konstanty definující formát zpráv pro local id merger.
 * @author tfechtner
 *
 */
public final class DataflowObjectFormats {

    private DataflowObjectFormats() {
    }

    /**
     * Typy objektů ve zprávách.
     * @author tfechtner
     *
     */
    public enum ItemTypes {
        /** Vrstva datoveho toku */
        LAYER("layer", 4),
        /** Resource - teradata bteq, teradata ddl, ifpc, ... */
        RESOURCE("resource", 6),
        /** Uzel dataflow grafu. */
        NODE("node", 7),
        /** Hrana v dataflow grafu - direct/flow. */
        EDGE("edge", 6),
        /** Atribut uzlu dataflow grafu. */
        NODE_ATTRIBUTE("node_attribute", 4),
        /** Atribut hrany v dataflow uzlu. */
        EDGE_ATTRIBUTE("edge_attribute", 4),
        /** Source code. */
        SOURCE_CODE("source_code", 4);

        /** Textová reprezentace typu ve zprávě. */
        private final String text;
        /** Počet sloupců záznamu. */
        private final int size;

        /**
         * 
         * @param text Textová reprezentace typu ve zprávě.
         * @return odpovídající instance enumu nebo null, pokud taková neexistuje
         */
        public static ItemTypes parseString(String text) {
            for (ItemTypes item : values()) {
                if (item.getText().equals(text)) {
                    return item;
                }
            }

            return null;
        }

        /**
         * @param text Textová reprezentace typu ve zprávě.
         * @param size Počet sloupců záznamu.
         */
        ItemTypes(String text, int size) {
            this.text = text;
            this.size = size;
        }

        /**
         * @return Textová reprezentace typu ve zprávě.
         */
        public String getText() {
            return text;
        }

        /**
         * @return Počet sloupců záznamu.
         */
        public int getSize() {
            return size;
        }
    }
    
    /**
     * Format zprav pro vrstvy.
     * 
     * @author onouza
     *
     */
    public enum LayerFormat {
        /** ID vrstvy. */
        ID(1),
        /** Nazev vrstvy. */
        NAME(2),
        /** Typ vrstvy. */
        TYPE(3);

        private int index;

        /**
         * @param index index ve zprávě
         */
        LayerFormat(int index) {
            this.index = index;
        }

        /**
         * @return index ve zprávě
         */
        public int getIndex() {
            return index;
        }
    }

    /**
     * Formát zpráv pro uzly.
     * @author tfechtner
     *
     */
    public enum NodeFormat {
        /** ID uzlu. */
        ID(1),
        /** Id předka uzlu, prázdný string pokud předka nemá. */
        PARENT_ID(2),
        /** Jméno uzlu.*/
        NAME(3),
        /** Typ uzlu (tabulka, view, sloupec). */
        TYPE(4),
        /** Id resource, do kterého uzlu patří. */
        RESOURCE_ID(5),
        /** Special mark of the node indicating a special operation to be performed on this node. */
        FLAG(6);

        private int index;

        /**
         * @param index index ve zprávě
         */
        NodeFormat(int index) {
            this.index = index;
        }

        /**
         * @return index ve zprávě
         */
        public int getIndex() {
            return index;
        }
    }
    
    /**
     * Type of flag sent in the FLAG NodeFormat.
     * Used for additional operation during incremental update.
     * 
     * @author jsykora
     */
    public enum Flag {
        /** Remove node and its subtree during incremental update (but recover subtree root node after)*/
        REMOVE("remove"),
        /** Remove node and its subtree during incremental update (do not recover subtree root node after*/
        REMOVE_MYSELF("removeMyself");
        
        /** Flag in the form of text sent in the CSV */
        private final String text;
        
        Flag(String text) {
            this.text = text;
        }
        
        public String t() {
            return text;
        }
    }

    /**
     * Formát zpráv resource.
     * @author tfechtner
     *
     */
    public enum ResourceFormat {
        /** Id resource. */
        ID(1),
        /** Jméno resource. */
        NAME(2),
        /** Typ resource. */
        TYPE(3),
        /** Popis resource. */
        DESCRIPTION(4),
        /** ID vrstvy. */
        LAYER(5);

        private int index;

        /**
         * @param index index ve zprávě
         */
        ResourceFormat(int index) {
            this.index = index;
        }

        /**
         * @return index ve zprávě
         */
        public int getIndex() {
            return index;
        }
    }

    /**
     * Formát zpráv hran. 
     * @author tfechtner
     *
     */
    public enum EdgeFormat {
        /** Id hrany.*/
        ID(1),
        /** Id zdrojového uzlu hrany. */
        SOURCE(2),
        /** Id koncového uzlu hrany.*/
        TARGET(3),
        /** Typ hrany - direct/flow {@link eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel}.*/
        TYPE(4);

        private int index;

        /**
         * @param index index ve zprávě
         */
        EdgeFormat(int index) {
            this.index = index;
        }

        /**
         * @return index ve zprávě
         */
        public int getIndex() {
            return index;
        }
    }

    /**
     * Formát zpráv atributů.
     * @author tfechtner
     *
     */
    public enum AttributeFormat {
        /** Id objektu, ke kterému se atribut vztahuje. */
        OBJECT_ID(1),
        /** Klíč/název atributu. */
        KEY(2),
        /** hodnota atributu. */
        VALUE(3);

        private int index;

        /**
         * @param index index ve zprávě
         */
        AttributeFormat(int index) {
            this.index = index;
        }

        /**
         * @return index ve zprávě
         */
        public int getIndex() {
            return index;
        }
    }

    /**
     * Formát zpráv pro source code.
     * @author tfechtner
     *
     */
    public enum SourceCodeFormat {
        /** ID source code. */
        ID(1),
        /** Jméno sourcecode. */
        LOCAL_NAME(2),
        /** Hash pro identifikaci změn.*/
        HASH(3);

        private int index;

        /**
         * @param index index ve zprávě
         */
        SourceCodeFormat(int index) {
            this.index = index;
        }

        /**
         * @return index ve zprávě
         */
        public int getIndex() {
            return index;
        }
    }
}
