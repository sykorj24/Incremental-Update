<#-- @ftlvariable name="rc" type="org.springframework.web.servlet.support.RequestContext" -->
<#macro welcome_import>
	    <script src="${rc.contextPath}/js/ext/jquery-2.1.0.min.js"></script>
	    <script src="${rc.contextPath}/js/ext/bootstrap.min.js"></script>
	    <script src="${rc.contextPath}/js/ext/bootstrap-select.js"></script>
	    <script src="${rc.contextPath}/js/ext/handlebars-v1.3.0.js"></script>
	    <script src="${rc.contextPath}/js/ext/jquery-ui-1.10.4.custom.min.js"></script>
	    <script src="${rc.contextPath}/js/ext/jstree.min.js"></script>
	    <script src="${rc.contextPath}/js/ext/jquery.cookie.js"></script>
	    <script src="${rc.contextPath}/js/ext/bootstrap-dialog.min.js"></script>

	    <script src="${rc.contextPath}/js/utils/handlebars-helpers.js" type="text/javascript"></script>
	    
	    <script src="${rc.contextPath}/js/flow/presentationManager.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/refresher.js" type="text/javascript"></script>
	    
	    <script src="${rc.contextPath}/js/pages/page-welcome.js"></script>  
</#macro>    