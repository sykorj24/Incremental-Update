<#-- @ftlvariable name="rc" type="org.springframework.web.servlet.support.RequestContext" -->
<#macro dataflow_import>
	    <script src="${rc.contextPath}/js/ext/jquery-2.1.0.min.js"></script>
	    <script src="${rc.contextPath}/js/ext/bootstrap.min.js"></script>
	    <script src="${rc.contextPath}/js/ext/bootstrap-dialog.js"></script>
	    <script src="${rc.contextPath}/js/ext/handlebars-v1.3.0.js"></script>
	    <script src="${rc.contextPath}/js/ext/jquery-ui-1.10.4.custom.min.js"></script>
	    <script src="${rc.contextPath}/js/ext/jquery.mousewheel.min.js"></script>
	    <script src="${rc.contextPath}/js/ext/jstree.min.js"></script>
	    <script src="${rc.contextPath}/js/ext/stats.min.js"></script>
	    <script src="${rc.contextPath}/js/ext/kinetic-v5.1.0.min.js"></script>
	    <script src="${rc.contextPath}/js/ext/kineticjs.viewport-v1.x.0.js"></script>
	    <script src="${rc.contextPath}/js/ext/aop.min.js"></script>
	    <script src="${rc.contextPath}/js/ext/codemirror.js"></script>
	    <script src="${rc.contextPath}/js/ext/codemirror-sql.js"></script>
	    <script src="${rc.contextPath}/js/ext/codemirror-active-line.js"></script>
	    <script src="${rc.contextPath}/js/ext/codemirror-search.js"></script>
	    <script src="${rc.contextPath}/js/ext/codemirror-searchcursor.js"></script>
	    <script src="${rc.contextPath}/js/ext/codemirror-dialog.js"></script>
	    <script src="${rc.contextPath}/js/ext/codemirror-simplescrollbars.js"></script>
	    <script src="${rc.contextPath}/js/ext/codemirror-annotatescrollbar.js"></script>
	    <script src="${rc.contextPath}/js/ext/codemirror-matchesonscrollbar.js"></script>
	    
	    <script src="${rc.contextPath}/js/utils/handlebars-helpers.js" type="text/javascript"></script>
	    
	    <script src="${rc.contextPath}/js/flow/utilities.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/cache.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/geometry.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/exceptions.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/filterManager.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/semantics.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/connection.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/graphics.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/tools.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/controller.js" type="text/javascript"></script>
        <script src="${rc.contextPath}/js/flow/undoManager.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/layout.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/hierarchicalLayout.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/presentationManager.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/operations.js" type="text/javascript"></script>
        <script src="${rc.contextPath}/js/flow/hiddenByFilters.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/follow.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/collapseManager.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/expandCollapse.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/filter.js" type="text/javascript"></script>
	    <script src="${rc.contextPath}/js/flow/undoRedo.js" type="text/javascript"></script>
        <script src="${rc.contextPath}/js/flow/refresher.js" type="text/javascript"></script>

	    <script src="${rc.contextPath}/js/pages/page-dataflow.js" type="text/javascript"></script>   
</#macro>