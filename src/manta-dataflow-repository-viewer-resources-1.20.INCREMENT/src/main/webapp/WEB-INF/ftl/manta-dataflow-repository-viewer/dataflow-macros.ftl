<#import "../macros/layout.ftl" as macros />

<#macro layout header="" title="Manta Flow">
    <@macros.layout title=title header=header>
        <div id="loading-overlay" class="ui-widget-overlay ui-helper-hidden"></div>        
        <#nested />        
    </@>    
</#macro>

<#macro handlebars>
    <!-- Handlebars -->
    <script id="autocomplete-item-template" type="text/x-handlebars-template">
    <li data-id="{{id}}">
       <a style="block" class="clearfix">
           <div class="item-name"><strong>{{text}}</strong></span></div>
           <div>
               <div class="item-path text-muted">{{pathString}}</div>
               <div class="item-type text-muted">{{type}}</div>
           </div>        
        </a>
    </li>
    </script>
    
    <script id="color-layer-checkbox-template" type="text/x-handlebars-template">
        {{#each this}}
            <div class="checkbox">
                <input type="checkbox" id="colorLayer_{{id}}" value="{{id}}" {{#ifIsTrue checked}}checked="checked"{{/ifIsTrue}}>
                <label for="colorLayer_{{id}}">{{name}}</label>
            </div>
        {{/each}}
    </script>
    
    <script id="node-detail-header-template" type="text/x-handlebars-template">
       {{#ifIsFormatted label 27}}
            <div class="show-untruncated-btn">
                 <button type="button" class="close remove-item btn btn-default btn-xs" aria-hidden="true" title="Show node name" onclick="Manta.DataflowUI.showAtribute('Node full name', '{{escape label}}');">
                     <span class="glyphicon glyphicon-new-window"></span>
                </button>
            </div>
       {{/ifIsFormatted}}
       <div class="truncate" title="{{label}}">{{label}}</div>
    </script>

    <script id="node-detail-template" type="text/x-handlebars-template">
        <div class="box-content">
            <div class="node-detail-item clearfix">
                <div class="node-detail-item-label"><strong>Tech</strong></div>
                <div class="node-detail-item-value">{{model.tech}}</div>
            </div>
            <div class="node-detail-item clearfix border-top">
                <div class="node-detail-item-label"><strong>Type</strong></div>
                <div class="node-detail-item-value">{{model.type}}</div>
            </div>
            <div class="node-detail-item clearfix border-top">
                <div class="node-detail-item-label"><strong>Path</strong></div>
                <div class="node-detail-item-value">{{pathString}}</div>
            </div>
            <!-- DEBUG
            <div class="node-detail-item clearfix border-top">
                <div class="node-detail-item-label"><strong>Atomic</strong></div>
                <div class="node-detail-item-value">{{model.atomic}}</div>
            </div>
            <div class="node-detail-item clearfix border-top">
                <div class="node-detail-item-label"><strong>Proxy</strong></div>
                <div class="node-detail-item-value">{{model.proxy}}</div>
            </div>
            <div class="node-detail-item clearfix border-top">
                <div class="node-detail-item-label"><strong>Status</strong></div>
                <div class="node-detail-item-value">{{model.status}}</div>
            </div>
            -->
            <div>
            {{#each attributes}}
                {{#ifNonBlank value}}
                    <div class="node-detail-item clearfix border-top">
                        <div class="node-detail-item-label truncate" title="{{this.label}}">
                            <strong>{{this.label}}</strong>
                        </div>
                        <div class="node-detail-item-value truncate" title="{{nl2space (printAttrs this.value)}}">      
                            <span>
                                {{#each value}}
                                    {{#ifIsFormatted value 20}}
                                        <div class="show-untruncated-btn">
                                            <button type="button" class="close remove-item btn btn-default btn-xs" aria-hidden="true" 
                                                title="Show attribute" 
                                                onclick="Manta.DataflowUI.showAtribute('Attribute value', '{{nl2br (escape (printAttrs this.value))}}');
                                            ">
                                                <span class="glyphicon glyphicon-new-window"></span>
                                            </button>
                                        </div>
                                    {{/ifIsFormatted}}
                                    <span
                                        {{#ifCond revState '==' 'NEW'}} class="attribute-value-new"{{/ifCond}}
                                        {{#ifCond revState '==' 'DELETED'}} class="attribute-value-deleted"{{/ifCond}}
                                    >
                                        {{linkProcessor (nl2space value)}}
                                    </span>
                                {{/each}}
                            </span>
                        </div>
                    </div>
                {{/ifNonBlank}}
            {{/each}}

            {{#each model.nodeMapping}}
                <div class="node-detail-item clearfix border-top">
	                <div class="node-detail-item-label truncate" title="{{@key}} Layer"><strong>{{@key}} Layer</strong></div>
            		<div class="node-detail-item-value truncate" title="{{nl2space (printMappedNodes this)}}">
                        <span>
			            {{#each this}}
	                        {{#ifIsFormatted this.pathString 20}}
	                            <div class="show-untruncated-btn">
	                                <button type="button" class="close remove-item btn btn-default btn-xs" aria-hidden="true" 
	                                    title="Show {{@key}} layer mapping" 
	                                    onclick="Manta.DataflowUI.showAtribute('{{@key}} layer mapping', '{{nl2br (escape (nl2space this.pathString))}}');
	                                ">
	                                    <span class="glyphicon glyphicon-new-window"></span>
	                                </button>
	                            </div>
	                        {{/ifIsFormatted}}
				            <span>
	                            {{linkProcessor (nl2space this.pathString)}}
	            			</span>
			            {{/each}}
                        </span>
            		</div>
                </div>
            {{/each}}
        </div>
    </script>
    
    <script id="edge-detail-header-template" type="text/x-handlebars-template">
       {{#ifIsFormatted (concat source.label ' -> ' target.label) 27}}
            <div class="show-untruncated-btn">
                 <button type="button" class="close remove-item btn btn-default btn-xs" aria-hidden="true" title="Show node name" onclick="Manta.DataflowUI.showAtribute('Edge full name', '{{escape (concat source.label ' -> ' target.label)}}');">
                     <span class="glyphicon glyphicon-new-window"></span>
                </button>
            </div>
       {{/ifIsFormatted}}
       <div class="truncate" title="{{source.label}} -> {{target.label}}">{{source.label}} -> {{target.label}}</div>
    </script>

    <script id="edge-detail-template" type="text/x-handlebars-template">
        <div class="box-content">
            <div class="node-detail-item clearfix">
                <div class="node-detail-item-label"><strong>Source</strong></div>
                <div class="node-detail-item-value">{{model.source.label}}</div>
            </div>
            <div class="node-detail-item clearfix border-top">
                <div class="node-detail-item-label"><strong>Target</strong></div>
                <div class="node-detail-item-value">{{model.target.label}}</div>
            </div>
            <div class="node-detail-item clearfix border-top">
                <div class="node-detail-item-label"><strong>Type</strong></div>
                <div class="node-detail-item-value">{{model.type}}</div>
            </div>
            {{#ifCond model.revState '!=' 'STABLE'}}
                <div class="node-detail-item clearfix border-top">
                    <div class="node-detail-item-label"><strong>Revision State</strong></div>
                    <div class="node-detail-item-value">{{model.revState}}</div>
                </div>  
            {{/ifCond}}              
            <div>
            {{#ifCond filtredNumber '>' 0}}
                <div class="node-detail-item clearfix border-top">
                    <div class="node-detail-item-label"><strong>Filtred</strong></div>
                    <div class="node-detail-item-value">{{filtredNumber}} {{#ifCond filtredNumber '==' 1}}node{{else}}nodes{{/ifCond}}</div>
                </div>            
            {{else}}    
                {{#ifCond childrenNumber '>' 1}}
                    <div class="node-detail-item clearfix border-top">
                        <div class="node-detail-item-label"><strong>Selected</strong></div>
                        <div class="node-detail-item-value">{{childrenNumber}} edges</div>
                    </div> 
                {{/ifCond}}    
            {{/ifCond}}            
            {{#each attrs}}
                {{#ifNonBlank value}}
                    <div class="node-detail-item clearfix border-top">
                        <div class="node-detail-item-label truncate" title="{{this.label}}">
                            <strong>{{this.label}}</strong>
                        </div>
                        <div class="node-detail-item-value truncate" title="{{nl2space value}}">                            
                            {{#ifIsFormatted value 20}}
                                <div class="show-untruncated-btn">
                                    <button type="button" class="close remove-item btn btn-default btn-xs" aria-hidden="true" title="Show attribute" onclick="Manta.DataflowUI.showAtribute('Attribute value', '{{nl2br (escape value)}}');">
                                        <span class="glyphicon glyphicon-new-window"></span>
                                    </button>
                                </div>
                            {{/ifIsFormatted}}
                            <span>{{linkProcessor (nl2space value)}}</span>
                        </div>
                    </div>
                {{/ifNonBlank}}
            {{/each}}
        </div>
    </script>
    
    <script id="context-menu-template" type="text/x-handlebars-template">
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;position:static;margin-bottom:5px;padding:5px;">
            {{#each links}}
                {{#if sublinks}}
                    <li class="dropdown-submenu">
                        <a tabindex="-1" href="#">
                            <span class="glyphicon glyphicon-chevron-right pull-right" style=""></span>
					        {{{title}}}
                        </a>
			            <ul class="dropdown-menu">
                            {{#each sublinks}}
                                <li><a tabindex="-1" href="#" onClick="{{action}}">{{{title}}}</a></li>
                            {{/each}}
			            </ul>
			        </li>                
                {{else}}
                    <li><a tabindex="-1" href="#" onClick="{{action}}">{{{title}}}</a></li>
                {{/if}}            
            {{/each}}
        </ul>
    </script>
    
    <script id="messages-template" type="text/x-handlebars-template">
        {{#each this}}
            <div class="alert {{cssClass}} alert-dismissible" role="alert">
                <span class="alert-icon pull-left"></span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{text}}
            </div>
        {{/each}}
    </script>
</#macro>

<#macro sourceCodeDialog>
    <div id="editorDialog" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Source for the selected statement.</h4>
          </div>
          <div id="editorDialogBody" class="modal-body">
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->  
</#macro>

<#macro hintDialog>
    <div id="hintDialog" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">
              <div class="glyphicon glyphicon-info-sign modal-dialog-icon"></div>
              <div>Demo Gallery Tips &amp; Tricks</div>
            </h4>
          </div>
          <div id="hintDialogBody" class="modal-body">
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->  
</#macro>

<#macro searchBox>
    <div id="search">
        <div class="left-inner-addon">
            <span class="icon-search">&nbsp;</span>
            <input class="form-control autocomplete" type="text" placeholder="Find element..." />
        </div>             
    </div>
</#macro>    

<#macro nodeDetail>
   <div id="detail" class="box-over-graph ui-helper-hidden">
        <div class="clearfix" id="collapseDetailContentBtn">
            <div class="pull-left text-muted header"><span></span></div>
            <div class="pull-right"><span class="glyphicon glyphicon-chevron-down"></span></div>
        </div>
        <div class="content ui-helper-hidden"></div>        
    </div>
</#macro>  

<#macro toolbar>
    <div id="toolbar">
        <div class="btn-group">
            <button id="zoomIn" class="btn btn-default">
                <span class="glyphicon glyphicon-zoom-in"></span>                
            </button>
            <button id="zoomOut" class="btn btn-default">
                <span class="glyphicon glyphicon-zoom-out"></span>
            </button>
        </div>

        <div class="btn-group">
            <button id="undo" class="btn btn-default">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </button>
            <button id="redo" class="btn btn-default">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </button>
        </div>
        
        <div class="btn-group">
            <button id="center" class="btn btn-default" title="Center On Start Node">
                <span class="glyphicon glyphicon-screenshot"></span>
            </button>
        </div>
        
        <div id="tips-and-tricks-group" class="btn-group" style="display:none">
            <button id="tips-and-tricks" class="btn btn-default" title="Show Tips &amp; Tricks">
                <span class="glyphicon glyphicon-info-sign"></span>
            </button>
        </div>
        
        <#nested />
    </div>
</#macro>  

<#macro messages messageList=[] >
    <div id="messages" class="ui-helper-hidden">
        <#if messageList??>
            <#list messageList as m>
                <div class="alert alert-<#if m.severity=='ERROR'>danger<#else>${m.severity?lower_case}</#if> alert-dismissible" role="alert">
                    <span class="alert-icon pull-left"></span>
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    ${m.text}
                </div>
            </#list>
        </#if>
    </div>
</#macro>

<#macro options resourceFilterList customFilterList resourceList>
    <div id="options" class="box-over-graph">
        <div class="clearfix" id="collapseOptionsBtn">
            <div class="pull-left text-muted">Options</div>
            <div class="pull-right"><span class="glyphicon glyphicon-plus"></span></div>
        </div>
        <div id="optionsPane" class="ui-helper-hidden">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#resourceTab" data-toggle="tab">Resources</a></li>
                <li><a href="#filtersTab" data-toggle="tab">Filters</a></li>
                <li><a href="#collapseTab" data-toggle="tab">Detail</a></li>  
                <li><a href="#colorTab" data-toggle="tab">Colors</a></li>                    
            </ul>
        
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="resourceTab">
                    <#if resourceFilterList?has_content>
                         <div id="resource-list" class="options-pane-content">
                             <#list resourceFilterList?sort_by("name") as resource>
                                 <div class="checkbox resource-item" <#if resource.layer??>data-layer="${resource.layer}"</#if>>
                                    <input type="checkbox" id="resource_${resource.normalizedId}" value="${resource.id}" ${resource.isFilterActive?string('', 'checked="checked"')}>
                                    <label for="resource_${resource.normalizedId}">${resource.name}</label>
                                 </div>
                             </#list>
                         </div>
                        <div class="divider">
                        </div>
                        <div class="clearfix">
                            <button id="resource-apply" class="btn btn-default pull-right" type="button">
                                <span class="glyphicon glyphicon-ok"></span>
                                Apply
                            </button>
                        </div>
                    </#if>
                </div>
                <div class="tab-pane" id="filtersTab">
                    <#if customFilterList?has_content>
                        <div id="filter-list" class="options-pane-content">
                            <#list customFilterList?sort_by("name") as resource>
                                <div class="checkbox filter-item" <#if resource.layer??>data-layer="${resource.layer}"</#if>>
                                    <input type="checkbox" id="filter_${resource.normalizedId}" value="${resource.id}" ${resource.isFilterActive?string('checked="checked"', '')}>
                                    <label for="filter_${resource.normalizedId}">${resource.name}</label>
                                </div>
                            </#list>
                        </div>
                        <div class="divider">
                        </div>
                        <div class="clearfix">
                            <button id="clean-manual" class="btn btn-default pull-right" type="button">
                                <span class="glyphicon glyphicon-refresh"></span>
                                Clean Manual Visibility
                            </button>
                            <button id="filter-apply" class="btn btn-default pull-right" type="button">
                                <span class="glyphicon glyphicon-ok"></span>
                                Apply
                            </button>
                        </div>
                    </#if>
                </div>
                <div class="tab-pane" id="collapseTab">
                    <div id="collapse-settings-list" class="options-pane-content">
                        <div class="collapse-settings-row collapse-settings-caption">
                            <div class="collapse-settings-name">Resource</div>
                            <div class="collapse-settings-check" title="Low Detail">L</div>
                            <div class="collapse-settings-check" title="Medium Detail">M</div>
                            <div class="collapse-settings-check" title="High Detail">H</div>
                            <div class="collapse-settings-check" title="Custom Manually Settings">C</div>
                        </div>                                
                        <#list resourceList?sort_by("name") as resource>
                            <div class="collapse-settings-row resource-item" <#if resource.layer??>data-layer="${resource.layer}"</#if>>
                                <div class="collapse-settings-name">${resource.name}</div>
                                <div class="collapse-settings-check">
                                    <input type="radio" name="collapse_${resource.id?string('0')}" value="0" <#if formData.level=='TOP'>checked="checked"</#if>>
                                </div>
                                <div class="collapse-settings-check">
                                    <input type="radio" name="collapse_${resource.id?string('0')}" value="1" <#if formData.level=='MIDDLE'>checked="checked"</#if>>
                                </div>
                                <div class="collapse-settings-check">
                                    <input type="radio" name="collapse_${resource.id?string('0')}" value="2" <#if formData.level=='BOTTOM'>checked="checked"</#if>>
                                </div>
                                <div class="collapse-settings-check">
                                    <input type="radio" id="collapse_${resource.id?string('0')}_custom" name="collapse_${resource.id?string('0')}" value="-1" disabled >
                                </div>
                            </div>
                        </#list>
                    </div>
                    <div class="divider">
                    </div>
                    <div class="clearfix">
                        <button id="collapse-settings-apply" class="btn btn-default pull-right" type="button" disabled="disabled">
                            <span class="glyphicon glyphicon-ok"></span>
                            Apply
                        </button>
                    </div>
                </div>
                <div class="tab-pane" id="colorTab">
                    <div id="color-layers-list" class="options-pane-content">
                    </div>
                    <div class="divider">
                    </div>
                    <div class="clearfix">
                        <button id="color-layers-apply" class="btn btn-default pull-right" type="button" disabled="disabled">
                            <span class="glyphicon glyphicon-ok"></span>
                            Apply
                        </button>
                    </div>
                </div>
                <#nested />
            </div>
        </div>          
    </div>
</#macro>  