/**
 * Definice vlastnich pomocnych funnkci Handlebars
 * 
 * @author onouza
 */

window.Manta = window.Manta || {};

(function(ns, $, undefined) {

    // document ready:
    $(function() {

        // Test neprazdnosti retezce
        Handlebars.registerHelper('ifNonBlank', function(variable, options) {
            if (variable instanceof Array) {
                return options.fn(this);
            } else {
                if (variable && variable.trim()) {
                    return options.fn(this);
                } else {
                    return options.inverse(this);
                }
            }
        });

        // Test boolean promenne
        Handlebars.registerHelper('ifIsTrue', function(variable, options) {
            if (variable) {
                return options.fn(this);
            } else {
                return options.inverse(this);
            }
        });

        // test, ze je promenna pole
        Handlebars.registerHelper('ifIsArray', function(variable, options) {
            if (variable instanceof Array) {
                return options.fn(this);
            } else {
                return options.inverse(this);
            }
        });

        // test, ze je promenna root, tedy rovna /
        Handlebars.registerHelper('ifNormalPath', function(variable, options) {
            if (variable.trim() === "/") {
                return options.inverse(this);
            } else {
                return options.fn(this);
            }
        });

    });

    /**
     * Zkrati text na pozdaovanou delku a odstrani zalomeni radku
     */
    Handlebars.registerHelper('truncate', function(str, len) {
        if ($.isArray(str)) {
            str = str.join();
        }
        if (str.length > len && str.length > 0) {
            var new_str;
            new_str = str.substr(0, len);
            new_str = str.substr(0, new_str.lastIndexOf(" "));
            new_str = (new_str.length > 0) ? new_str : str.substr(0, len);
            new_str.replace(/(\r\n|\n|\r)/gm, " ");

            return new Handlebars.SafeString(new_str + '...');
        }
        return str;
    });

    /**
     * Oescapuje string.
     */
    Handlebars.registerHelper('escape', function(str) {
        if ($.isArray(str)) {
            str = str.join();
        }
        return str.replace(/'/g, "&#39;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    });

    /**
     * Prevede \n na br tag.
     */
    Handlebars.registerHelper('nl2br', function(str) {
        if ($.isArray(str)) {
            str = str.join();
        }
        return str.replace(/(?:\r\n|\r|\n)/g, '<br />');
    });

    /**
     * Prevede \n na mezeru.
     */
    Handlebars.registerHelper('nl2space', function(str) {
        if ($.isArray(str)) {
            str = str.join();
        }
        if (str == null) {
            return "";
        }
        return str.replace(/\\r\\n/g, " ").replace(/\\n/g, " ");
    });
    
    /**
     * Spojí parametry.
     */
    Handlebars.registerHelper('concat', function() {
        var str = '';
        for (var i = 0; i < arguments.length; i++) {
            str += arguments[i];
        }
        return str;
    });
    
    /**
     * Podmínka.
     */
    Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {

        switch (operator) {
            case '==':
                return (v1 == v2) ? options.fn(this) : options.inverse(this);
            case '===':
                return (v1 === v2) ? options.fn(this) : options.inverse(this);
            case '<':
                return (v1 < v2) ? options.fn(this) : options.inverse(this);
            case '<=':
                return (v1 <= v2) ? options.fn(this) : options.inverse(this);
            case '>':
                return (v1 > v2) ? options.fn(this) : options.inverse(this);
            case '>=':
                return (v1 >= v2) ? options.fn(this) : options.inverse(this);
            case '&&':
                return (v1 && v2) ? options.fn(this) : options.inverse(this);
            case '||':
                return (v1 || v2) ? options.fn(this) : options.inverse(this);
            case '!=':
                return (v1 != v2) ? options.fn(this) : options.inverse(this);    
            default:
                return options.inverse(this);
        }
    });

    /**
     * Prevede odkazy na a href.
     */
    Handlebars.registerHelper('linkProcessor', function(str) {
        if ($.isArray(str)) {
            str = str.join();
        }

        return new Handlebars.SafeString(str.replace(/(https?:\/\/\S+)/g, "<a target='_blank' href='$1'>$1</a>"));
    });
    
    /**
     * Vytiskne atributy, ktere jsou struktura.
     */
    Handlebars.registerHelper('printAttrs', function(attrs) {
        if (attrs === undefined) {
            return "";
        }
        
        if (!($.isArray(attrs))) {
            return attrs;            
        }
        
        var str = "";
        for (var i = 0; i < attrs.length; i++) {
            if (i != 0) {
                str += ", ";
            }
            str += attrs[i].value;
        }

        return str;
    });

    /**
     * Vytiskne uzly mapovani.
     * @param Uzly mapovani, ktere se maji vytisknout.
     */
    Handlebars.registerHelper('printMappedNodes', function(mappedNodes) {
        if (mappedNodes === undefined) {
            return "";
        }
        
        if (!($.isArray(mappedNodes))) {
            return mappedNodes;            
        }
        
        var str = "";
        for (var i = 0; i < mappedNodes.length; i++) {
            if (i != 0) {
                str += ", ";
            }
            str += mappedNodes[i].pathString;
        }

        return str;
    });

    /**
     * Podminka pro vykresleni textu, pokud je text delsi nex len, nebo obsahuje
     * zalomeni radku vyhodnoti se jako true
     */
    Handlebars.registerHelper('ifIsFormatted', function(str, len, options) {
        if ($.isArray(str)) {
            str = str.join();
        }
        if (str !== null && (str.length > len || (str.match(/\n/g) || []).length > 0)) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

}(Manta.Handlebars = Manta.Handlebars || {}, jQuery));