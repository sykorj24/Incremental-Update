/*
  Rewritten for usage with KineticJS 5.1.0
  2014-07-02
  Martin Slapak
*/

function Viewport(stage, scaleLimits, layerDraggable) {
	this.stage = stage;
    // begin scale at 100%
    this.scale = {x:1.0, y:1.0};
    this.minimumScale = 0.5;
    this.maximumScale = 1.5;
    if (typeof scaleLimits !== 'undefined' && typeof scaleLimits.max !== 'undefined' && typeof scaleLimits.min !== 'undefined') {
        this.minimumScale = scaleLimits.min;
        this.maximumScale = scaleLimits.max;
    }
    this.scaleRate = 0.03;
    this.viewPadding = 100; // for testing only

    this.offset = {x:0, y:0};
    
	this.canvasWidth = stage.getWidth();
	this.canvasHeight = stage.getHeight();
	
	// we zero out the view -- position it at 0,0
	this.viewLeft = 0;
	this.viewTop = 0;
	this.viewRight = 0;
	this.viewBottom = 0;
	
	// compute the view right and bottom positions. the top, left will still be 0,0.
	this.updateViewBounds();
		
	this.panRate = 10.0;
	
	// nodes can be anything which inherits Node, such as Groups, Shapes, except Layers.
	this.nodes = {};
	
	// create layer
	this.layer = new Kinetic.Layer({draggable: false});
		
	// add layer to stage
	stage.add(this.layer);
	
	// set initial zoom
	this.setScale(this.scale);
}

Viewport.prototype.updateViewBounds = function() {
	this.viewRight = Math.round(this.viewLeft + this.canvasWidth * (1 / this.scale.x));
	this.viewBottom = Math.round(this.viewTop + this.canvasHeight * (1 / this.scale.y));
};

/// <summary>
/// Returns the ID to be used when adding the next node.
/// </summary>
Viewport.prototype.getNewNodeID = function() {
	if( typeof(this.nextNodeID) == "undefined" ) {
		// init to 1
		this.nextNodeID = 1;
	}
	else
	{
		// increment by 1
		this.nextNodeID++;
	}
	// return next ID value
	return this.nextNodeID;
};

Viewport.prototype.setNodeX = function( nodeID, x ) {
	var node = this.getNode( nodeID );
	node.setX(this.viewLeft + x);
};

Viewport.prototype.setNodeY = function( nodeID, y ) {
	var node = this.getNode( nodeID );
	node.setY(this.viewTop + y);
};

// returns the game-space position, not the view-space position
Viewport.prototype.getNodeX = function( nodeID ) {
	var node = this.getNode( nodeID );
	return node.getX() + this.viewLeft;
};

Viewport.prototype.getNodeY = function( nodeID ) {
	var node = this.getNode( nodeID );
	return node.getY() + this.viewTop;
};

Viewport.prototype.addNodeX = function( nodeID, x ) {
	var node = this.getNode( nodeID );
	node.setX(node.getX() + x);
};

Viewport.prototype.addNodeY = function( nodeID, y ) {
	var node = this.getNode( nodeID );
	node.setY(node.getY() + y);
};

Viewport.prototype.getNode = function( nodeID ) {
	var node = this.nodes[nodeID];
	return node;
};

/// <summary>
/// Adds the node to the viewport and returns the ID.
/// </summary>
Viewport.prototype.add = function(node, /* only required if node doesn't have it */ radius) {

	if( typeof( node.radius ) === 'undefined' || node.radius() < 1 ) {
		// node didn't have a valid radius property
		if( typeof( radius ) === 'undefined' || radius < 1 ) {
			// we didn't get a radius, so throw an exception
			throw new "Error: Viewport.add() requires a positive integer value for the 'radius' parameter if the provided node does not have a valid radius property.";
		} else {
			// store the provided radius
			node.WPradius = radius;
		}
	} else {
	    node.WPradius = node.radius();
	}

	// get the new node ID and store in node
	node.ID = this.getNewNodeID();

	// store node in hashmap by node ID
	this.nodes[node.ID] = node;
	
	// apply view-space position adjustment
	node.x -= this.viewLeft;
	node.y -= this.viewTop;
	

	// record that the node is not visible, not added to layer
	node.WPvisible = false;
		
	// log event
	//console.log( "added a new node with ID = " + node.ID );
	
	// reurn node ID
	return node.ID;
};

Viewport.prototype.draw = function() {
    console.log("viewport.draw() invoked");
    // call a method in here which calcs the viewport dimensions, position. then loops thru all nodes and calcs whether they are visible. if they are visible and not shown, then shown, and visa-versa. flag accordingly.
	this.updateViewBounds();

	// set which nodes are visible (added to layer) based on the view bounds
	this.updateVisibleNodes();

	// re-render the layer
	this.layer.draw();
  //this.layer.batchDraw();
  //this.stage.batchDraw();
};

Viewport.prototype.updateVisibleNodes = function() {
	// for each node, determine if node is in bounds. if it is, and isn't visible, add it. if it isn't and is visible, remove it.
	
	// store values in local variables for convenience and performance
	var viewPadding = this.viewPadding;
	var viewLeft = this.viewLeft + viewPadding * this.scale.x;
	var viewTop = this.viewTop + viewPadding * this.scale.y;
	var viewRight = this.viewRight - viewPadding * this.scale.x;
	var viewBottom = this.viewBottom - viewPadding * this.scale.y;
	
	console.log( "view rect: sc="+this.scale+" l=" + viewLeft + ", t=" + viewTop + ", r=" + viewRight +  ", b=" + viewBottom );	
	
	// start ClipRect - for testing only
	if( typeof this.clipRect === 'undefined' ) {
		// add test polygon for the clipping rect
		this.clipRect = new Kinetic.Rect( {
			x: viewLeft,
	    	y: viewTop,
	    	fill: 'yellow',
	    	stroke: "grey",
	    	strokeWidth: 1,
	    	visible: true,
	    	height: viewBottom - viewTop,
	    	width: viewRight - viewLeft
    	});
		// add rect to layer
		this.layer.add(this.clipRect);
		this.clipRect.moveToBottom();
	} else {
		this.clipRect.setX(viewLeft/this.scale.x);
		this.clipRect.setY(viewTop/this.scale.y);
		this.clipRect.width((viewRight - viewLeft) + (viewPadding-viewPadding/this.scale.x));
		this.clipRect.height((viewBottom - viewTop) - (viewPadding-viewPadding/this.scale.y));		
		this.clipRect.moveToBottom();
	}
	//console.log( "layer l=" +this.layer.offsetX()+ " t="+this.layer.offsetY() + " stage l="+this.stage.offsetX()+" t="+this.stage.offsetY());
	var cr = this.clipRect;
	console.log( "clip rect: sc="+this.scale+" l=" + cr.x() + ", t=" + cr.y() + ", w=" + cr.getWidth() + ", h=" + cr.getHeight() );	
	// end ClipRect
	
	
	for( var nodeID in this.nodes ) {
		// get current node
		var node = this.getNode(nodeID);
		
		// TODO: set node radius to actual value
		var nodeRadius = node.WPradius;
		var nodeDiameter = nodeRadius * 2;
		
		// calculate node positions (taky boundingbox)
		var nodeLeft = this.getNodeX( nodeID );
		var nodeRight = nodeLeft + node.getWidth();
		var nodeTop = this.getNodeY( nodeID );
		var nodeBottom = nodeTop + node.getHeight();
		
        // special case for circles
        if (node.className === 'Circle') {
            nodeLeft = this.getNodeX( nodeID ) - nodeRadius;
            nodeRight = nodeLeft + nodeDiameter;
            nodeTop = this.getNodeY( nodeID ) - nodeRadius;
            nodeBottom = nodeTop + nodeDiameter;
        }
        // only manta edges and nodes has boundingbox
		if (typeof node.boundingBox !== 'undefined') {
		    nodeLeft    = node.boundingBox.x;
            nodeRight   = nodeLeft + node.boundingBox.width;
            nodeTop     = node.boundingBox.y;
            nodeBottom  = nodeTop + node.boundingBox.height;
            // hrany posunem po Z ose nahoru
            
            var nname = node.getName();
            if (typeof nname === 'undefined') return false;
            nname = nname.split('_');
            if (typeof node.getParent() !== 'undefined' && nname[0] === Manta.Semantics.SemanticType.EDGE) node.moveToTop();
            //console.log(node.getName()," ma bbox = ",node.boundingBox);
		}
		/*
        var t = ''+node.getX()+','+node.getY();
        var text = new Kinetic.Text({
            x: 0,
            y: -15,
            text: t,
            fontSize: 15,
            fontFamily: 'Calibri',
            fill: 'red',
            name: 'coords'
        });
        var coords = node.find('.coords');
        if (coords.length === 0) {
            node.add(text);
        } else {
            coords[0].text(t);
        }
        */
		
		// determine whether node is within the view bounds
		var isInView = 
			( viewLeft <= nodeRight // node's right is to the right of the view's left edge
			&& nodeLeft <= viewRight // node's left is to the left of the view's right edge
			&& viewTop <= nodeBottom // node's bottom below the view's top edge
			&& nodeTop <= viewBottom ); // node's top is above the view's bottom edge
		/*
		if( node.ID == 33) {
			console.log("node " + nodeID + ", r="+nodeDiameter+" isin="+isInView+" bounds: left=" + nodeLeft + ", right=" + nodeRight + ", top=" + nodeTop + ", bottom=" + nodeBottom );
		}
		*/
		
		//console.log("is in ", node.ID, isInView)
		switch( isInView ) {
			case true:
			    //console.log("vis=", node.isVisible());
				if( !node.WPvisible ) {
    				// node isn't visible, and it needs to be
    				// add node to layer
    				this.layer.add(node);
    				
    				if (nname[0] !== Manta.Semantics.SemanticType.EDGE) {
    				    //node.cacheText();
    				}
    				// flag node as visible
    				node.WPvisible = true;
    
    				//if( nodeID == 34 ) {
    					console.log("showing item " + nodeID + " item.name=" + node.getName()+ ": left=" + nodeLeft + ", right=" + nodeRight + ", top=" + nodeTop + ", bottom=" + nodeBottom );
    				//}
				}			
				break;
			case false:
				if( node.WPvisible) {
					// node is visible, and it shouldn't be					
					// remove node from layer
				    node.remove();

					// flag node as not visible
				    node.WPvisible = false;
					
					//if( nodeID == 34 || nodeID == 1 ) {
						console.log("hiding item " + nodeID + " item.name=" + node.getName()+": left=" + nodeLeft + ", right=" + nodeRight + ", top=" + nodeTop + ", bottom=" + nodeBottom );
					//}
				}
				break;
		}
		
	}
	
};

Viewport.prototype.panLeft = function(amount) {
	this.panX( -amount );
};

Viewport.prototype.panRight = function(amount) {
	this.panX( amount );
};

Viewport.prototype.panUp = function(amount) {
	this.panY( -amount );
};

Viewport.prototype.panDown = function(amount) {
	this.panY( amount );
};

Viewport.prototype.panX = function(amount) {
	var panChange = amount * ( 1 / this.scale.x);
	for( var nodeID in this.nodes ) {
		// move node position by given amount
		this.nodes[nodeID].setX(this.nodes[nodeID].getX() + -panChange);
		this.nodes[nodeID].boundingBox.x += -panChange;
	}
	// record new view left
	//this.viewLeft += panChange;
};

Viewport.prototype.panY = function(amount) {
	var panChange = amount * ( 1 / this.scale.y);
	for( var nodeID in this.nodes ) {
		// move node position by given amount
	    this.nodes[nodeID].setY(this.nodes[nodeID].getY() + -panChange);
	    this.nodes[nodeID].boundingBox.y += -panChange;
	}
	// record new view top
	//this.viewTop += panChange;
};

Viewport.prototype.pan = function(amount) {
    var panChangeX = amount.x * ( 1 / this.scale.x);
    var panChangeY = amount.y * ( 1 / this.scale.y);
    var n;
    for( var nodeID in this.nodes ) {
        // move node position by given amount
        n = this.nodes[nodeID];
        n.setX(n.getX() + -panChangeX);
        n.setY(n.getY() + -panChangeY);
        n.boundingBox.x += -panChangeX;
        n.boundingBox.y += -panChangeY;
    }
    // record new view top and left
    this.viewLeft += panChangeX;
    this.viewTop += panChangeY;
};

Viewport.prototype.panWithoutView = function(amount) {
    var panChangeX = amount.x * ( 1 / this.scale.x);
    var panChangeY = amount.y * ( 1 / this.scale.y);
    var n;
    for( var nodeID in this.nodes ) {
        // move node position by given amount
        n = this.nodes[nodeID];
        n.setX(n.getX() + -panChangeX);
        n.setY(n.getY() + -panChangeY);
        n.boundingBox.x += -panChangeX;
        n.boundingBox.y += -panChangeY;
    }
};

Viewport.prototype.zoomIn = function() {
    var current = this.scale();
    current.x += this.scaleRate;
    current.y += this.scaleRate;
	this.setScale(current);
};

Viewport.prototype.zoomOut = function() {
	var current = this.scale();
    current.x -= this.scaleRate;
    current.y -= this.scaleRate;
    this.setScale(current);
};

Viewport.prototype.getScale = function() {
    var scale = this.scale;
    return scale;
};

Viewport.prototype.setScale = function(scale) {
	// store new scale
	this.scale = scale;
	if( this.scale < this.minimumScale ) {
		// the scale is below the minimum - set to minimum
		this.scale = this.minimumScale;
	}
	else if( this.scale > this.maximumScale ) {
		// the scale is above the maximum - set to maximum
		this.scale = this.maximumScale;
	}
	
	// apply new scale
	this.layer.setScale(this.scale);
	
	//this.layer.draw();
};

Viewport.prototype.addOffset = function(offset) {
    var x = this.layer.offsetX();
    var y = this.layer.offsetY();
    offset.x += x;
    offset.y += y;
    this.offset = offset;
    this.layer.setOffset(offset);
};

Viewport.prototype.setOffset = function(offset) {
    this.offset = offset;
    this.layer.setOffset(offset);
    //this.layer.draw();
};
