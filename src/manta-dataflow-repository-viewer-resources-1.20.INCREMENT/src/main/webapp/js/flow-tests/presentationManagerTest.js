function validateHMTLColor(color) {
    if (typeof $('#colorTester')[0] === 'undefined') {
        var test = document.createElement('div');
        document.body.appendChild(test);
        test.id = 'colorTester';    
        $('#colorTester').css('color', '#000000');
    }
    var valid = $('#colorTester').css('color');
    $('#colorTester').css('color', color);
    var x;
    if(valid == $('#colorTester').css('color') && color!=='#000000') {
        x = false;
    } else {
        x = true;
        $('#colorTester').css('color', valid);
    }
    return x;
}

// zkompaktneni JSONu: http://jsonformatter.curiousconcept.com/
var jsonGFXConfData = '{"universalImages":{"btnExpandExpandedPositive":"/img/icon_expanded_dark_grey.png","btnExpandExpandedNegative":"/img/icon_expanded_white.png","btnExpandCollapsedPositive":"/img/icon_collapsed_dark_grey.png","btnExpandCollapsedNegative":"/img/icon_collapsed_white.png","btnFollowPositive":"/img/icon_related_connection_grey.png","btnFollowNegative":"/img/icon_related_connection_white.png"},"defEdge":{"width":2,"color":"#666666","borderWidth":0,"borderColor":"#000000","headFill":"#FFFFFF","headWidth":1,"hoverColor":"#FF0000","highlightColor":"#000000","fadeoutColor":"#EEEEEE","opacity":1.0},"defNode":{"iconPositive":null,"iconNegative":null,"bgcolor":"#4d98ea","bgimage":null,"borderWidth":1,"borderColor":"#666666","color":"#FFFFFF","cornerRadius":5,"opacity":1,"selection":{"bgcolor":"#FFFA00","positive":true,"color":"#000000"},"focus":{"bgcolor":"#FFFCC0","positive":true,"color":"#000000"},"highlight":{"bgcolor":"#FF0000","positive":false,"color":"#FFFFFF"},"attributes":{}},"edges":{"DIRECT":{},"FILTER":{"color":"#AAAAFF"},"AGGREGATED":{"width":4},"INDIRECT":{"color":"#AAAAFF","width":1}},"techs":{"Default":{"Directory":{"bgcolor":"#dddfe1","color":"#393b3d","borderColor":"#9fa0a1","iconPositive":"/img/icons/folder_icon_v2_positive.png","iconNegative":"/img/icons/folder_icon_v2_negative.png","positive":true},"File":{"bgcolor":"#4d98ea","color":"#ffffff","iconPositive":"/img/icons/file_icon_positive.png","iconNegative":"/img/icons/file_icon_negative.png","positive":false},"Table":{"bgcolor":"#0b9211","color":"#ffffff","iconPositive":"/img/icons/table_icon_positive.png","iconNegative":"/img/icons/table_icon_negative.png","positive":false,"attributes":{"TABLE_TYPE":"Table type"}},"View":{"bgcolor":"#0b9211","color":"#ffffff","iconPositive":"/img/icons/view_icon_positive.png","iconNegative":"/img/icons/view_icon_negative.png","positive":false,"attributes":{"TABLE_TYPE":"Table type"}},"Column":{"bgcolor":"#ffffff","color":"#616365","iconPositive":"/img/icons/column_icon_positive.png","iconNegative":"/img/icons/column_icon_negative.png","positive":true,"attributes":{"COLUMN_CHARSET":"Column charset","COLUMN_LENGTH":"Column length","COLUMN_TYPE":"Column type","ORDER":"Order","tpt column description":"TPT description","ifpc column description":"IFPC description"}},"Database":{"bgcolor":"#d7ebeb","color":"#393b3d","borderColor":"#6bbd6e","iconPositive":"/img/icons/database_icon_positive.png","iconNegative":"/img/icons/database_icon_negative.png","positive":true},"Schema":{"bgcolor":"#d7ebeb","color":"#393b3d","borderColor":"#6bbd6e","iconPositive":"/img/icons/database_schema_icon_positive.png","iconNegative":"/img/icons/database_schema_icon_negative.png","positive":true},"Parameter":{"bgcolor":"#ffffff","color":"#616365","borderColor":"#6bbd6e","iconPositive":"/img/icons/parameter_icon_positive.png","iconNegative":"/img/icons/parameter_icon_negative.png","positive":true},"Function":{"bgcolor":"#0b9211","color":"#ffffff","iconPositive":"/img/icons/function_icon_positive.png","iconNegative":"/img/icons/function_icon_negative.png","positive":false},"Procedure":{"bgcolor":"#0b9211","color":"#ffffff","iconPositive":"/img/icons/procedure_icon_v1_positive.png","iconNegative":"/img/icons/procedure_icon_v1_negative.png","positive":false},"Macro":{"bgcolor":"#0b9211","color":"#ffffff","iconPositive":"/img/icons/macro_icon_positive.png","iconNegative":"/img/icons/macro_icon_negative.png","positive":false},"Trigger":{"bgcolor":"#0b9211","color":"#ffffff","iconPositive":"/img/icons/trigger_icon_positive.png","iconNegative":"/img/icons/trigger_icon_negative.png","positive":false},"Package":{"bgcolor":"#d7ebeb","color":"#393b3d","iconPositive":"/img/icons/package_icon_positive.png","iconNegative":"/img/icons/package_icon_negative.png","positive":true}},"ODBC":{"Database":{"bgcolor":"#aaaaff","color":"#393b3d","iconPositive":"/img/icon_database_off.png"},"Schema":{"bgcolor":"#ffaaaa","color":"#393b3d","iconPositive":"/img/icon_database_off.png"},"Table":{"bgcolor":"#aaffaa","color":"#393b3d","bgimage":"/img/bgd_frame_expanded.png","iconPositive":"/img/icon_table_positive.png","iconNegative":"/img/icon_table_negative.png"},"Column":{"bgcolor":"#bd3632","color":"#ffffff","iconPositive":"/img/icon_table_column.png"},"Script":{"bgcolor":"#4d98ea","color":"#ffffff","iconPositive":"/img/icon_script.png"}},"Teradata":{"Database":{"bgcolor":"#ffbbbb","color":"#393b3d","iconPositive":"/img/icon_database_off.png"},"Table":{"bgcolor":"#bd3632","color":"#ffffff","iconPositive":"/img/icon_table.png"},"View":{"bgcolor":"#7b2927","color":"#ffffff","iconPositive":"/img/icon_table.png"},"Column":{"bgcolor":"#ffffff","color":"#616365","iconPositive":"/img/icon_table_column.png"}},"Teradata DDL":{"Directory":{"bgcolor":"#0d57a0","color":"#000000","iconPositive":"/img/icon_folder.png","bgimage":"/img/bgd_frame_expanded.png"},"BTEQ Script":{"bgcolor":"#4d98ea","color":"#ffffff","iconPositive":"/img/icon_script.png"},"BTEQ Statement":{"bgcolor":"#2d77c0","color":"#ffffff"},"BTEQ DataFlow":{"bgcolor":"#0d57a0","color":"#ffffff"},"BTEQ ColumnFlow":{"bgcolor":"#ffffff","color":"#616365","iconPositive":"/img/icon_table_column.png"}}}}';


test( "PresentaionManager - BasicNodeBound object", function() {
    var bnb = new Manta.PresentationManager.BasicNodeBounds();

    equal(isFinite(bnb.elementMargin),true);
    equal(isFinite(bnb.minHeight),true);
    equal(isFinite(bnb.minWidth),true);
    equal(isFinite(bnb.headHeight),true);
});

test( "PresentaionManager - Parse JSON", function() {
    Manta.PresentationManager.parseJSON($.parseJSON(jsonGFXConfData));
    var keys = Object.keys(Manta.PresentationManager.gfxConf.conf); 
    equal(keys.contains('defEdge'), true);
    equal(keys.contains('defNode'), true);
    equal(keys.contains('universalImages'), true);
    
    keys = Object.keys(Manta.PresentationManager.gfxConf.conf.universalImages);
    equal(keys.contains('btnExpandCollapsedPositive'), true);
    equal(keys.contains('btnExpandCollapsedNegative'), true);
    equal(keys.contains('btnExpandExpandedPositive'), true);
    equal(keys.contains('btnExpandExpandedNegative'), true);
    equal(keys.contains('btnFollowPositive'), true);
    equal(keys.contains('btnFollowNegative'), true);
});

test( "PresentaionManager - default EDGE configuration", function() { 
    Manta.PresentationManager.parseJSON($.parseJSON(jsonGFXConfData));
    var de = Manta.PresentationManager.gfxConf.conf.defEdge;
    // TODO: border a borderColor se zatim neresi
    equal(isFinite(de.width), true, "width must be a number.");
    ok(de.width >= 1, "width must be greater or equal then 1");
    equal(isFinite(de.headWidth), true, "headWidth must be a number.");
    ok(de.headWidth >= 0, "headWidth must be greater or equal then 0");
    equal(validateHMTLColor(de.color), true, "valid color");
    equal(validateHMTLColor(de.fadeoutColor), true, "valid color");
    equal(validateHMTLColor(de.headFill), true, "valid color");
    equal(validateHMTLColor(de.highlightColor), true, "valid color");
    equal(validateHMTLColor(de.hoverColor), true, "valid color");
});

test( "PresentaionManager - default NODE configuration", function() {
    Manta.PresentationManager.parseJSON($.parseJSON(jsonGFXConfData));
    var dn = Manta.PresentationManager.gfxConf.conf.defNode;
    equal(isFinite(dn.cornerRadius), true, "Corner radius must be a number.");
    ok(dn.cornerRadius >= 0, "Corner radius must be a positive!");
    equal(validateHMTLColor(dn.bgcolor), true, "valid color");
    equal(validateHMTLColor(dn.color), true, "valid color");
    equal(validateHMTLColor(dn.selection.color), true, "valid color");
    equal(validateHMTLColor(dn.selection.bgcolor), true, "valid color");
    equal(dn.selection.positive, true);
    equal(validateHMTLColor(dn.focus.color), true, "valid color");
    equal(validateHMTLColor(dn.focus.bgcolor), true, "valid color");
    equal(dn.focus.positive, true);
    equal(validateHMTLColor(dn.highlight.color), true, "valid color");
    equal(validateHMTLColor(dn.highlight.bgcolor), true, "valid color");
    equal(dn.highlight.positive, false);
});

test( "PresentaionManager - GFXConf request handling", function() {

    Manta.PresentationManager.parseJSON($.parseJSON(jsonGFXConfData));
    var dn = $.extend(true, {}, Manta.PresentationManager.gfxConf.conf.defNode);
    var model = new Manta.Semantics.Node(-1, false);

    model.tech = undefined;
    model.type = '';    
    conf = Manta.PresentationManager.getGFXConf(model);
    deepEqual(conf, dn);   

    model.tech = null;
    model.type = '';    
    conf = Manta.PresentationManager.getGFXConf(model);
    deepEqual(conf, dn);
    
    model.tech = '';
    model.type = null;    
    conf = Manta.PresentationManager.getGFXConf(model);
    deepEqual(conf, dn);
    
    model.tech = '';
    model.type = {};
    conf = Manta.PresentationManager.getGFXConf(model);
    deepEqual(conf, dn);
    
    model.tech = '';
    model.type = '';    
    conf = Manta.PresentationManager.getGFXConf(model);
    deepEqual(conf, dn);
    
    model.tech = '';
    model.type = 'Table';    
    conf = Manta.PresentationManager.getGFXConf(model);
    model.tech = 'Default';
    model.type = 'Table';    
    confDefault = Manta.PresentationManager.getGFXConf(model);
    deepEqual(conf, confDefault);
    
    model.tech = 'ODBC';
    model.type = '';    
    conf = Manta.PresentationManager.getGFXConf(model);
    deepEqual(conf, dn);
    
    model.techType = 'ODBC';
    model.type = 'Table';    
    conf = Manta.PresentationManager.getGFXConf(model);
    ok(conf);
    ok(conf.bgimage);
    ok(conf.bgimage instanceof HTMLImageElement);
    equal(conf.bgcolor, '#aaffaa');
    equal(typeof conf.iconPositive, 'object');
    equal(conf.iconPositive instanceof HTMLImageElement, true);
    equal(typeof conf.iconNegative, 'object');
    equal(conf.iconNegative instanceof HTMLImageElement, true);
});

test( "PresentaionManager - Icon of by technology and type", function() {
    var parsedGFXConfData = $.parseJSON(jsonGFXConfData);
    Manta.PresentationManager.parseJSON(parsedGFXConfData);

    // Nalezeni ikony dle technologie a typu
    var icon = Manta.PresentationManager.getIconByTechnologyAndType('ODBC', 'Table');
    equal(icon instanceof HTMLImageElement, true);
    
    // Defaultni ikona - neurcena technologie
    icon = Manta.PresentationManager.getIconByTechnologyAndType(undefined, 'Table');
    equal(icon instanceof HTMLImageElement, true);
    
    // Defaultni ikona - neurceny typ
    icon = Manta.PresentationManager.getIconByTechnologyAndType('ODBC', undefined);
    equal(icon instanceof HTMLImageElement, true);
    
    // Defaultni ikona - neexistujici technologie
    icon = Manta.PresentationManager.getIconByTechnologyAndType('@blbost!', 'Table');
    equal(icon instanceof HTMLImageElement, true);
    
    // Defaultni ikona - neexistujici typ
    icon = Manta.PresentationManager.getIconByTechnologyAndType('ODBC', '@blbost!');
    equal(icon instanceof HTMLImageElement, true);
});