/**
 * Created by mkrutsky on 29.4.2014.
 */
window.Manta = window.Manta || {};

(function(ns, $) {

    ns.ResponseFactory = function(startNodeId) {
        this.response = {
            "type": "follow",
            "componentID": startNodeId,
            "components": [],
            "flows": []
        };
    };

    ns.ResponseFactory.prototype.getResponse = function() {
        return this.response;
    };

    ns.ResponseFactory.prototype.createFollowResponse = function(components, flows) {
        var i,
            j,
            flow;

        if(components) {
            for (i = 0; i < components.length; i++) {
                this.pushComponentRecursively(components[i], -1);
            }
        }

        if(flows) {
            for (i = 0; i < flows.length; i++) {
                flow = {
                    proxies: [],
                    aggregated: flows[i].length > 2,
                    childEdges : []
                };
                for (j = 0; j < flows[i].length; j++) {
                    flow.proxies.push({
                        path: [ flows[i][j] ],
                        tech: "ODBC"
                    });
                }
                this.response.flows.push(flow);
            }
        }
    };

    ns.ResponseFactory.prototype.pushComponentRecursively = function(comp, parentId) {
        var i;

        this.response.components.push(this.createComponent(
            comp.id,
            false,
            parentId,
            comp.children && comp.children.length > 0,
            comp.type ? comp.type : "Table"));

        if(comp.children) {
            for (i = 0; i < comp.children.length; i++) {
                this.pushComponentRecursively(comp.children[i], comp.id);
            }
        }
    };

    ns.ResponseFactory.prototype.createComponent = function(id, proxy, parentID, atomic, border, type) {
        return {
            "id": id.toString(),
            "proxy": proxy,
            "parentID": parentID,
            "label": "Node " + id,
            "atomic": atomic,
            "border": border,
            "tech": "ODBC",
            "type": type,
            "path": []
        };
    }

}(Manta.Test = Manta.Test || {}, jQuery));

