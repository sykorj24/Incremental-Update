window.Manta = window.Manta || {};

(function(ns, $) {
    
    ns.GraphFactory = function(graph) {
        this.graph = graph;        
    };

    ns.GraphFactory.prototype.createAndAddNodes = function(ids) {
        var self = this,
            node;
        $.each(ids, function(i, id) {
            node = self.createNode(id);
            node.graph = self.graph;
            self.graph.children.push(node);
            self.graph.nodeMap[id] = node;
        });
    };
    
    
    ns.GraphFactory.prototype.createNode = function(id) {
        var node = new Manta.Semantics.Node(id);
        node.atomic = false;
        node.filtered = false;
        return node;
    };

    ns.GraphFactory.prototype.createFilteredProxyNode = function(id) {
        var node = new Manta.Semantics.Node(id);
        node.atomic = false;
        node.filtered = true;
        node.proxy = true;
        return node;
    };

    ns.GraphFactory.prototype.createAndAddEdges = function(edges) {
        var self = this;
        $.each(edges, function(i, edgeIds) {
            if(edgeIds instanceof Array && edgeIds.length ==2) {
                self.addEdge(edgeIds[0], edgeIds[1]);
            } else {
                throw new Manta.Exceptions.IllegalArgumentException("Invalid edge format: " + edgeIds);
            }
        });
    };

    /**
     * Prida vsechny predane hrany ve formatu [[0,1, [1,2], ...]
     * @param edges
     */
    ns.GraphFactory.prototype.addEdges = function(edges) {
        var i;
        for(i=0; i < edges.length; i++) {
            this.addEdge(edges[i][0], edges[i][1]);
        }
    };

    /**
     * Prida hranu mezi zadanymi uzly
     * @param source
     * @param target
     */
    ns.GraphFactory.prototype.addEdge = function(source, target) {
        var edge = this.createEdge();
        this.graph.addEdgeBetween(edge, source, target);
    };

    /**
     * Prida filter hranu mezi zadanymi uzly
     * @param source
     * @param target
     */
    ns.GraphFactory.prototype.addFilterEdge = function(source, target) {
        var edge = this.createEdge();
        edge.type = Manta.Semantics.Edge.EdgeType.FILTER;
        this.graph.addEdgeBetween(edge, source, target);
    };

    ns.GraphFactory.prototype.createEdge = function() {
        return new Manta.Semantics.Edge();
    };

}(Manta.Test = Manta.Test || {}, jQuery));