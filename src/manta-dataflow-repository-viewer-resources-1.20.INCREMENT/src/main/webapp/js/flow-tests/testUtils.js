Manta.TestUtils = {
    computeCrossings : function(layers) {
        var i,j, k, l, m, sum= 0, v1, v2, s1, s2, rest;
        for(i=1; i < layers.length; i++){
            rest = [];
            for(j=0; j < layers[i].vertices.length; j++){
                rest.push(layers[i].vertices[j]);
            }
            while(!(rest.length === 0)) {
                v1 = rest.pop();
                for(k=0; k < v1.segmentsIn.length; k++) {
                    s1 = v1.segmentsIn[k];
                    for(l=0; l < rest.length; l++) {
                        v2 = rest[l];
                        if(v1 !== v2) {
                            for(m=0; m < v2.segmentsIn.length; m++) {
                                s2 = v2.segmentsIn[m];
                                if(v1.position < v2.position) {
                                    if(s1.source.position > s2.source.position) {
                                        sum++;
                                    }
                                } else {
                                    if(s1.source.position < s2.source.position) {
                                        sum++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return sum;
    }
};

