test( "Iterator - basic", function() {

    var arr = [0,1,2];
    var iterator = new Manta.Utils.Iterator(arr, true);

    ok(iterator.hasNext());
    equal(iterator.next(), 0);
    ok(iterator.hasNext());
    equal(iterator.next(), 1);
    ok(iterator.hasNext());
    equal(iterator.next(), 2);
    ok(!iterator.hasNext());

    iterator = new Manta.Utils.Iterator(arr, false);

    ok(iterator.hasNext());
    equal(iterator.next(), 2);
    ok(iterator.hasNext());
    equal(iterator.next(), 1);
    ok(iterator.hasNext());
    equal(iterator.next(), 0);
    ok(!iterator.hasNext());
});

test( "Iterator - String", function() {

    var arr = ["a","b","c"];
    var iterator = new Manta.Utils.Iterator(arr, true);

    ok(iterator.hasNext());
    equal(iterator.next(), "a");
    ok(iterator.hasNext());
    equal(iterator.next(), "b");
    ok(iterator.hasNext());
    equal(iterator.next(), "c");
    ok(!iterator.hasNext());

    iterator = new Manta.Utils.Iterator(arr, false);

    ok(iterator.hasNext());
    equal(iterator.next(), "c");
    ok(iterator.hasNext());
    equal(iterator.next(), "b");
    ok(iterator.hasNext());
    equal(iterator.next(), "a");
    ok(!iterator.hasNext());
});