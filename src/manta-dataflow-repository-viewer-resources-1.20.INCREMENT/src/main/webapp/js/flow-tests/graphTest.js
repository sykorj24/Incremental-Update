//noinspection JSUnresolvedFunction
test( "Graph - Add Nodes", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode('4'),2);
    graph.addNode(graphFactory.createNode('8'),-1);
    graph.addNode(graphFactory.createNode('7'),3);
    graph.addNode(graphFactory.createNode('5'),2);
    graph.addNode(graphFactory.createNode('6'),3);
    graph.addNode(graphFactory.createNode('3'),1);
    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'),1);

    graph.validateStructure();

    equal(Object.keys(graph.nodeMap).length, 8);
    equal(graph.children.length, 2);
    equal(graph.nodeMap['1'].children.length, 2);
    equal(graph.nodeMap['2'].children.length, 2);
    equal(graph.nodeMap['3'].children.length, 2);
    equal(graph.nodeMap['4'].children.length, 0);
    equal(graph.nodeMap['5'].children.length, 0);
    equal(graph.nodeMap['6'].children.length, 0);
    equal(graph.nodeMap['7'].children.length, 0);
    equal(graph.nodeMap['8'].children.length, 0);
});

//noinspection JSUnresolvedFunction
test( "Graph - Add Edges - Simple", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode('4'),2);
    graph.addNode(graphFactory.createNode('8'),-1);
    graph.addNode(graphFactory.createNode('7'),3);
    graph.addNode(graphFactory.createNode('5'),2);
    graph.addNode(graphFactory.createNode('6'),3);
    graph.addNode(graphFactory.createNode('3'),1);
    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'),1);

    graph.addEdgeBetween(graphFactory.createEdge(), '4', '6');
    graph.addEdgeBetween(graphFactory.createEdge(), '4', '7');
    graph.addEdgeBetween(graphFactory.createEdge(), '8', '4');
    graph.addEdgeBetween(graphFactory.createEdge(), '5', '8');
    graph.addEdgeBetween(graphFactory.createEdge(), '5', '8');
    graph.addEdgeBetween(graphFactory.createEdge(), '7', '8');

    graph.validateStructure();

    equal(graph.nodeMap['1'].edgesIn.length, 0);
    equal(graph.nodeMap['1'].edgesOut.length, 0);
    equal(graph.nodeMap['4'].edgesOut.length, 2);
    equal(graph.nodeMap['4'].edgesIn.length, 1);
    equal(graph.nodeMap['5'].edgesOut.length, 1);
    equal(graph.nodeMap['5'].edgesIn.length, 0);
    equal(graph.nodeMap['7'].edgesOut.length, 1);
    equal(graph.nodeMap['8'].edgesIn.length, 2);
    equal(graph.nodeMap['8'].edgesOut.length, 1);
});

//noinspection JSUnresolvedFunction
test( "Graph - Add Edges - Aggregated", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createFilteredProxyNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createFilteredProxyNode(4));
    graph.addNode(graphFactory.createNode(5));

    graphFactory.addEdges([[1,2], [2,3], [3,4], [4,5]]);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].edgesOut.length, 2);
    ok(graph.nodeMap[1].edgesOut[1].isVisible());
    equal(graph.nodeMap[1].edgesOut[1].target.id, 5);
    equal(graph.nodeMap[1].edgesOut[1].nodesOnPath.length, 3);
    ok(graph.nodeMap[1].edgesOut[1].type === Manta.Semantics.Edge.EdgeType.DIRECT);
    ok(!graph.nodeMap[1].edgesOut[0].isVisible());
    equal(graph.nodeMap[2].edgesOut.length, 1);
    equal(graph.nodeMap[3].edgesOut.length, 1);
    equal(graph.nodeMap[4].edgesOut.length, 1);
    equal(graph.nodeMap[5].edgesIn.length, 2);
});

//noinspection JSUnresolvedFunction
test( "Graph - Add Edges - Aggregated Filtered", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createFilteredProxyNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createFilteredProxyNode(4));
    graph.addNode(graphFactory.createNode(5));

    graphFactory.addEdges([[1,2], [3,4], [4,5]]);
    graphFactory.addFilterEdge(2,3);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].edgesOut.length, 2);
    ok(graph.nodeMap[1].edgesOut[1].isVisible());
    equal(graph.nodeMap[1].edgesOut[1].target.id, 5);
    equal(graph.nodeMap[1].edgesOut[1].nodesOnPath.length, 3);
    ok(graph.nodeMap[1].edgesOut[1].type === Manta.Semantics.Edge.EdgeType.FILTER);
    ok(!graph.nodeMap[1].edgesOut[0].isVisible());
    equal(graph.nodeMap[2].edgesOut.length, 1);
    equal(graph.nodeMap[3].edgesOut.length, 1);
    equal(graph.nodeMap[4].edgesOut.length, 1);
    equal(graph.nodeMap[5].edgesIn.length, 2);
});

//noinspection JSUnresolvedFunction
test( "Graph - Add Edges - Aggregated Two way", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createFilteredProxyNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createFilteredProxyNode(4));
    graph.addNode(graphFactory.createNode(5));

    graphFactory.addEdges([[1,2], [1,3], [2,3], [3,4], [4,5]]);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].edgesOut.length, 3);
    ok(graph.nodeMap[1].edgesOut[2].isVisible());
    equal(graph.nodeMap[1].edgesOut[2].target.id, 5);
    ok(graph.nodeMap[1].edgesOut[2].type === Manta.Semantics.Edge.EdgeType.DIRECT);
    ok(!graph.nodeMap[1].edgesOut[0].isVisible());
    ok(!graph.nodeMap[1].edgesOut[1].isVisible());
});

//noinspection JSUnresolvedFunction
test( "Graph - Add Edges - Mixed FILTER and DIRECT", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createFilteredProxyNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createFilteredProxyNode(4));
    graph.addNode(graphFactory.createFilteredProxyNode(5));
    graph.addNode(graphFactory.createNode(6));

    graphFactory.addEdges([[1,2], [3,6], [1,4], [4,5], [5,6]]);
    graphFactory.addFilterEdge(2,3);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].edgesOut.length, 3);
    ok(graph.nodeMap[1].edgesOut[2].isVisible());
    equal(graph.nodeMap[1].edgesOut[2].target.id, 6);
    equal(graph.nodeMap[1].edgesOut[2].nodesOnPath.length, 4);
    ok(graph.nodeMap[1].edgesOut[2].type === Manta.Semantics.Edge.EdgeType.DIRECT);
});

//noinspection JSUnresolvedFunction
test( "Graph - Unfilter middle node on aggregated edge", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var component = {
        proxy: false,
        id: 3,
        path: []
    };

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createFilteredProxyNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createFilteredProxyNode(4));
    graph.addNode(graphFactory.createNode(5));

    graphFactory.addEdges([[1,2], [2,3], [3,4], [4,5]]);
    graph.updateAggregatedEdges();

    graph.nodeMap[3].fillFromComponent(component);
    graph.unfilterNode(graph.nodeMap[3], component);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].edgesOut.length, 2);
    equal(graph.nodeMap[1].edgesOut[0].target.id, 2);
    equal(graph.nodeMap[1].edgesOut[1].target.id, 3);
    equal(graph.nodeMap[3].edgesOut.length, 2);
    equal(graph.nodeMap[3].edgesOut[0].target.id, 4);
    equal(graph.nodeMap[3].edgesOut[1].target.id, 5);
    ok(!graph.nodeMap[3].proxy);
    equal(graph.nodeMap[5].edgesIn.length, 2);
});

//noinspection JSUnresolvedFunction
test( "Graph - Unfilter first node on aggregated edge)", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var component = {
        proxy: false,
        id: 3,
        path: []
    };

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createFilteredProxyNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createFilteredProxyNode(4));
    graph.addNode(graphFactory.createNode(5));

    graphFactory.addEdges([[1,2], [2,3], [3,4], [4,5]]);
    graph.updateAggregatedEdges();

    graph.unfilterNode(graph.nodeMap[2], component);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].edgesOut.length, 1);
    equal(graph.nodeMap[1].edgesOut[0].target.id, 2);
    equal(graph.nodeMap[2].edgesOut.length, 2);
    equal(graph.nodeMap[2].edgesOut[0].target.id, 3);
    equal(graph.nodeMap[2].edgesOut[1].target.id, 5);
    equal(graph.nodeMap[3].edgesOut.length, 1);
    equal(graph.nodeMap[4].edgesOut.length, 1);
    equal(graph.nodeMap[5].edgesIn.length, 2);
});

//noinspection JSUnresolvedFunction
test( "Graph - Unfilter last node on aggregated edge", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var component = {
        proxy: false,
        id: 3,
        path: []
    };

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createFilteredProxyNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createFilteredProxyNode(4));
    graph.addNode(graphFactory.createNode(5));

    graphFactory.addEdges([[1,2], [2,3], [3,4], [4,5]]);
    graph.updateAggregatedEdges();

    graph.unfilterNode(graph.nodeMap[4], component);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].edgesOut.length, 2);
    equal(graph.nodeMap[1].edgesOut[0].target.id, 2);
    equal(graph.nodeMap[1].edgesOut[1].target.id, 4);
    equal(graph.nodeMap[2].edgesOut.length, 1);
    equal(graph.nodeMap[3].edgesOut.length, 1);
    equal(graph.nodeMap[4].edgesIn.length, 2);
});

//noinspection JSUnresolvedFunction
test( "Graph - Add two Aggregated Edges", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createFilteredProxyNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createNode(4));

    graphFactory.addEdges([[1, 2],[2, 4]]);
    graphFactory.addEdges([[1, 3],[3, 4]]);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].edgesOut.length, 3);
    ok(graph.nodeMap[1].edgesOut[2].isVisible());
    ok(!graph.nodeMap[1].edgesOut[0].isVisible());
    ok(!graph.nodeMap[1].edgesOut[1].isVisible());
    equal(graph.nodeMap[1].edgesOut[2].target.id, 4);
    equal(graph.nodeMap[1].edgesOut[2].nodesOnPath.length, 2);
    ok(graph.nodeMap[1].edgesOut[2].nodesOnPath.contains(graph.nodeMap[2]));
    ok(graph.nodeMap[1].edgesOut[2].nodesOnPath.contains(graph.nodeMap[3]));
});

//noinspection JSUnresolvedFunction
test( "Graph - Add three Aggregated Edges", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createFilteredProxyNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createFilteredProxyNode(4));
    graph.addNode(graphFactory.createNode(5));

    graphFactory.addEdges([[1, 2],[2, 5], [1, 3],[3, 5],[1, 4],[4, 5]]);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].edgesOut.length, 4);
    equal(graph.nodeMap[1].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap[1].edgesOut[3].nodesOnPath.length, 3);
    ok(graph.nodeMap[1].edgesOut[3].nodesOnPath.contains(graph.nodeMap[2]));
    ok(graph.nodeMap[1].edgesOut[3].nodesOnPath.contains(graph.nodeMap[3]));
    ok(graph.nodeMap[1].edgesOut[3].nodesOnPath.contains(graph.nodeMap[4]));
});

//noinspection JSUnresolvedFunction
test( "Graph - Filter node - simple", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createNode(2));
    graph.addNode(graphFactory.createNode(3));
    graph.addNode(graphFactory.createNode(4));
    graph.addNode(graphFactory.createNode(5));

    graphFactory.addEdges([[1, 3], [2, 3], [3, 4], [3, 5]]);
    graph.updateAggregatedEdges();

    graph.filterNode(graph.nodeMap[3]);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].getVisibleEdgesOut().length, 2);
    equal(graph.nodeMap[2].getVisibleEdgesOut().length, 2);
    equal(graph.nodeMap[4].getVisibleEdgesIn().length, 2);
    equal(graph.nodeMap[5].getVisibleEdgesIn().length, 2);
    equal(graph.nodeMap[3].getVisibleEdgesOut().length, 0);
    equal(graph.nodeMap[3].getVisibleEdgesIn().length, 0);
    ok(graph.nodeMap[3].filtered);
});

//noinspection JSUnresolvedFunction
test( "Graph - Filter nodes ", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createNode(2));
    graph.addNode(graphFactory.createNode(3));
    graph.addNode(graphFactory.createNode(4));

    graphFactory.addEdge(1, 2);
    graphFactory.addEdge(1, 3);
    graphFactory.addEdge(2, 4);
    graphFactory.addEdge(3, 4);

    graph.updateAggregatedEdges();

    graph.filterNode(graph.nodeMap[2]);
    graph.filterNode(graph.nodeMap[3]);

    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap[1].getVisibleEdgesOut()[0].nodesOnPath.length, 2);
    ok(graph.nodeMap[1].getVisibleEdgesOut()[0].nodesOnPath.contains(graph.nodeMap[2]));
    ok(graph.nodeMap[1].getVisibleEdgesOut()[0].nodesOnPath.contains(graph.nodeMap[3]));
    equal(graph.nodeMap[1].getVisibleEdgesOut()[0].nodesOnPath.length, 2);
    equal(graph.nodeMap[4].getVisibleEdgesIn().length, 1);
    ok(graph.nodeMap[2].filtered);
    ok(graph.nodeMap[3].filtered);
});

//noinspection JSUnresolvedFunction
test( "Graph - Filter node - filter leaves", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createNode(2));
    graph.addNode(graphFactory.createNode(3));

    graphFactory.addEdge(1, 3);
    graphFactory.addEdge(2, 3);

    graph.filterNode(graph.nodeMap[1]);
    graph.filterNode(graph.nodeMap[2]);

    graph.updateAggregatedEdges();

    equal(graph.nodeMap[3].getVisibleEdgesIn().length, 0);
    ok(graph.nodeMap[1].filtered);
    ok(graph.nodeMap[1].filtered);
});

//noinspection JSUnresolvedFunction
test( "Graph - Filter Node - merge source aggregated edges", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createFilteredProxyNode(2));
    graph.addNode(graphFactory.createNode(3));
    graph.addNode(graphFactory.createNode(4));

    graphFactory.addEdges([[1,2], [2,3], [3,4]]);
    graph.updateAggregatedEdges();
    graph.filterNode(graph.nodeMap[3]);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap[1].getVisibleEdgesOut()[0].target.id, 4);
});

//noinspection JSUnresolvedFunction
test( "Graph - Filter Node - merge target aggregated edges", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createNode(4));

    graphFactory.addEdge(1,2);
    graphFactory.addEdge(2,4, [[2,3], [3,4]]);

    graph.filterNode(graph.nodeMap[2]);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap[1].getVisibleEdgesOut()[0].target.id, 4);
});

//noinspection JSUnresolvedFunction
test( "Graph - Filter Node - merge two aggregated edges", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createNode(4));

    graphFactory.addEdges([[2,3], [3,4], [1,2]]);
    graph.filterNode(graph.nodeMap[2]);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap[1].getVisibleEdgesOut()[0].target.id, 4);
});

//noinspection JSUnresolvedFunction
test( "Graph - Filter Node - merge aggregated parent edges", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createFilteredProxyNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createNode(4));
    graph.addNode(graphFactory.createNode(5));

    graphFactory.addEdges([[1,2], [2,4], [1,3], [3,4]]);
    graphFactory.addEdge(4,5);

    graph.filterNode(graph.nodeMap[4]);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap[1].getVisibleEdgesOut()[0].target.id, 5);
});

//noinspection JSUnresolvedFunction
test( "Graph - Filter Node - merge multipleEdges", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createNode(4));
    graph.addNode(graphFactory.createNode(5));
    graph.addNode(graphFactory.createNode(6));

    graphFactory.addEdges([[1,3], [2,3], [3,4]]);
    graphFactory.addEdge(4,5);
    graphFactory.addEdge(4,6);

    graph.filterNode(graph.nodeMap[4]);
    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].getVisibleEdgesOut().length, 2);
    equal(graph.nodeMap[1].getVisibleEdgesOut()[0].target.id, 5);
    equal(graph.nodeMap[1].getVisibleEdgesOut()[1].target.id, 6);
    equal(graph.nodeMap[2].getVisibleEdgesOut().length, 2);
    equal(graph.nodeMap[2].getVisibleEdgesOut()[0].target.id, 5);
    equal(graph.nodeMap[2].getVisibleEdgesOut()[1].target.id, 6);
    equal(graph.nodeMap[5].getVisibleEdgesIn().length, 2);
    equal(graph.nodeMap[6].getVisibleEdgesIn().length, 2);
});

//noinspection JSUnresolvedFunction
test( "Graph - Update aggregated edges - cycle", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode(1));
    graph.addNode(graphFactory.createFilteredProxyNode(2));
    graph.addNode(graphFactory.createFilteredProxyNode(3));
    graph.addNode(graphFactory.createFilteredProxyNode(4));
    graph.addNode(graphFactory.createFilteredProxyNode(5));
    graph.addNode(graphFactory.createFilteredProxyNode(6));
    graph.addNode(graphFactory.createNode(7));

    graphFactory.addEdges([[1,2],[2,3],[3,4],[4,5],[5,6],[6,3],[3,7]]);

    graph.updateAggregatedEdges();

    equal(graph.nodeMap[1].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap[1].edgesOut.length, 2);
    equal(graph.nodeMap[1].edgesOut[1].target.id, 7);
    equal(graph.nodeMap[1].edgesOut[1].nodesOnPath.length, 2);
    ok(graph.nodeMap[1].edgesOut[1].nodesOnPath.contains(graph.nodeMap[2]));
    ok(graph.nodeMap[1].edgesOut[1].nodesOnPath.contains(graph.nodeMap[3]));
});

//noinspection JSUnresolvedFunction
test( "Graph - Collect leaves", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'), 1);
    graph.addNode(graphFactory.createNode('3'), 1);
    graph.addNode(graphFactory.createNode('4'), 2);
    graph.addNode(graphFactory.createNode('5'), 2);
    graph.addNode(graphFactory.createNode('6'), 3);
    graph.addNode(graphFactory.createNode('7'), 1);

    equal(graph.nodeMap['1'].getLeavesId().length, 4);
    ok(graph.nodeMap['1'].getLeavesId().contains('4'));
    ok(graph.nodeMap['1'].getLeavesId().contains('5'));
    ok(graph.nodeMap['1'].getLeavesId().contains('6'));
    ok(graph.nodeMap['1'].getLeavesId().contains('7'));
    equal(graph.nodeMap['2'].getLeavesId().length, 2);
    ok(graph.nodeMap['2'].getLeavesId().contains('4'));
    ok(graph.nodeMap['2'].getLeavesId().contains('5'));
    equal(graph.nodeMap['3'].getLeavesId().length, 1);
    ok(graph.nodeMap['3'].getLeavesId().contains('6'));
});


//<editor-fold desc="EXPAND-COLLAPSE">

//noinspection JSUnresolvedFunction
test( "Graph - Contract Node - simple", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var node;

    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'), '1');
    graph.addNode(graphFactory.createNode('3'), '1');
    graph.addNode(graphFactory.createNode('4'), '2');
    graph.addNode(graphFactory.createNode('5'), '2');
    graph.addNode(graphFactory.createNode('6'));

    graph.addEdgeBetween(graphFactory.createEdge(), '3', '6');
    graph.addEdgeBetween(graphFactory.createEdge(), '4', '6');
    graph.addEdgeBetween(graphFactory.createEdge(), '5', '6');

    node = graph.nodeMap['1'];
    node.collapse();

    equal(graph.nodeMap['1'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['1'].getVisibleEdgesIn().length, 0);
    equal(graph.nodeMap['6'].getVisibleEdgesOut().length, 0);
    equal(graph.nodeMap['6'].getVisibleEdgesIn().length, 1);
    equal(graph.nodeMap['6'].edgesIn.length, 4);
    equal(graph.nodeMap['1'].children.length, 2);
    equal(graph.nodeMap['1'].getVisibleChildren().length, 0);
});

//noinspection JSUnresolvedFunction
test( "Graph - Contract Node - self-loop", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var node;

    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'), '1');
    graph.addNode(graphFactory.createNode('3'), '1');

    graph.addEdgeBetween(graphFactory.createEdge(), '2', '3');
    node = graph.nodeMap['1'];
    node.collapse();

    equal(graph.nodeMap['1'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['1'].getVisibleEdgesIn().length, 1);
    equal(graph.nodeMap['1'].getVisibleEdgesIn()[0].source.id, '1');
    equal(graph.nodeMap['1'].getVisibleEdgesIn()[0].target.id, '1');
    equal(graph.nodeMap['1'].getVisibleEdgesOut()[0].source.id, '1');
    equal(graph.nodeMap['1'].getVisibleEdgesOut()[0].target.id, '1');
});

//noinspection JSUnresolvedFunction
test( "Graph - Contract Node - Multiple Nodes", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'), '1');
    graph.addNode(graphFactory.createNode('3'), '1');
    graph.addNode(graphFactory.createNode('4'), '2');
    graph.addNode(graphFactory.createNode('5'), '2');
    graph.addNode(graphFactory.createNode('6'), '3');
    graph.addNode(graphFactory.createNode('7'), '3');
    graph.addNode(graphFactory.createNode('8'));
    graph.addNode(graphFactory.createNode('9'), '8');
    graph.addNode(graphFactory.createNode('10'), '8');
    graph.addNode(graphFactory.createNode('11'), '9');
    graph.addNode(graphFactory.createNode('12'), '10');
    graph.addNode(graphFactory.createNode('13'), '10');

    graph.addEdgeBetween(graphFactory.createEdge(), '4', '13');
    graph.addEdgeBetween(graphFactory.createEdge(), '5', '12');
    graph.addEdgeBetween(graphFactory.createEdge(), '5', '13');
    graph.addEdgeBetween(graphFactory.createEdge(), '6', '11');
    graph.addEdgeBetween(graphFactory.createEdge(), '7', '11');
    graph.addEdgeBetween(graphFactory.createEdge(), '7', '12');
    graph.addEdgeBetween(graphFactory.createEdge(), '7', '13');

    graph.nodeMap['2'].collapse();
    equal(graph.nodeMap['2'].getVisibleEdgesOut().length, 2);
    equal(graph.nodeMap['2'].getVisibleEdgesOut()[0].target.id, '12');
    equal(graph.nodeMap['12'].getVisibleEdgesIn().length, 2);
    equal(graph.nodeMap['13'].getVisibleEdgesIn().length, 2);

    graph.nodeMap['10'].collapse();
    equal(graph.nodeMap['2'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['2'].getVisibleEdgesOut()[0].target.id, '10');
    equal(graph.nodeMap['10'].getVisibleEdgesIn().length, 2);

    graph.nodeMap['1'].collapse();
    equal(graph.nodeMap['1'].getVisibleEdgesOut().length, 2);
    equal(graph.nodeMap['10'].getVisibleEdgesIn().length, 1);
    equal(graph.nodeMap['11'].getVisibleEdgesIn().length, 1);
    equal(graph.nodeMap['8'].getVisibleEdgesIn().length, 0);
    equal(graph.nodeMap['9'].getVisibleEdgesIn().length, 0);
    equal(graph.nodeMap['10'].getVisibleEdgesIn()[0].source.id, '1');
    equal(graph.nodeMap['11'].getVisibleEdgesIn()[0].source.id, '1');

    graph.nodeMap['8'].collapse();
    equal(graph.nodeMap['1'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['1'].getVisibleEdgesOut()[0].target.id, '8');
    equal(graph.nodeMap['8'].getVisibleEdgesIn().length, 1);
    equal(graph.nodeMap['8'].getVisibleEdgesIn()[0].source.id, '1');
});

//noinspection JSUnresolvedFunction
test( "Graph - Expand Node - simple", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'), '1');
    graph.addNode(graphFactory.createNode('3'), '1');
    graph.addNode(graphFactory.createNode('4'), '2');
    graph.addNode(graphFactory.createNode('5'), '2');
    graph.addNode(graphFactory.createNode('6'));

    graph.addEdgeBetween(graphFactory.createEdge(), '3', '6');
    graph.addEdgeBetween(graphFactory.createEdge(), '4', '6');
    graph.addEdgeBetween(graphFactory.createEdge(), '5', '6');

    graph.nodeMap['1'].collapse();
    equal(graph.nodeMap['1'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['6'].getVisibleEdgesIn().length, 1);

    graph.nodeMap['1'].expand();
    equal(graph.nodeMap['1'].getVisibleEdgesOut().length, 0);
    equal(graph.nodeMap['3'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['4'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['5'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['6'].getVisibleEdgesIn().length, 3);
});


//noinspection JSUnresolvedFunction
test( "Graph - Expand Node - self-loop", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var node;

    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'), '1');
    graph.addNode(graphFactory.createNode('3'), '1');

    graph.addEdgeBetween(graphFactory.createEdge(), '2', '3');
    node = graph.nodeMap['1'];
    node.collapse();
    node.expand();

    equal(graph.nodeMap['1'].getVisibleEdgesOut().length, 0);
    equal(graph.nodeMap['1'].getVisibleEdgesIn().length, 0);
    equal(graph.nodeMap['2'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['3'].getVisibleEdgesIn().length, 1);
});

//noinspection JSUnresolvedFunction
test( "Graph - Expand Node - Multiple Nodes", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'), '1');
    graph.addNode(graphFactory.createNode('3'), '1');
    graph.addNode(graphFactory.createNode('4'), '2');
    graph.addNode(graphFactory.createNode('5'), '2');
    graph.addNode(graphFactory.createNode('6'), '3');
    graph.addNode(graphFactory.createNode('7'), '3');
    graph.addNode(graphFactory.createNode('8'));
    graph.addNode(graphFactory.createNode('9'), '8');
    graph.addNode(graphFactory.createNode('10'), '8');
    graph.addNode(graphFactory.createNode('11'), '9');
    graph.addNode(graphFactory.createNode('12'), '10');
    graph.addNode(graphFactory.createNode('13'), '10');

    graph.addEdgeBetween(graphFactory.createEdge(), '4', '13');
    graph.addEdgeBetween(graphFactory.createEdge(), '5', '12');
    graph.addEdgeBetween(graphFactory.createEdge(), '5', '13');
    graph.addEdgeBetween(graphFactory.createEdge(), '6', '11');
    graph.addEdgeBetween(graphFactory.createEdge(), '7', '11');
    graph.addEdgeBetween(graphFactory.createEdge(), '7', '12');
    graph.addEdgeBetween(graphFactory.createEdge(), '7', '13');

    graph.nodeMap['2'].collapse();
    graph.nodeMap['10'].collapse();
    graph.nodeMap['1'].collapse();
    graph.nodeMap['8'].collapse();
    equal(graph.nodeMap['1'].getVisibleEdgesOut().length, 1);

    graph.nodeMap['1'].expand();
    equal(graph.nodeMap['1'].getVisibleEdgesOut().length, 0);
    equal(graph.nodeMap['2'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['6'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['7'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['8'].getVisibleEdgesIn().length, 3);

    graph.nodeMap['2'].expand();
    equal(graph.nodeMap['2'].getVisibleEdgesOut().length, 0);
    equal(graph.nodeMap['4'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['5'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['6'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['7'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['8'].getVisibleEdgesIn().length, 4);

    graph.nodeMap['8'].expand();
    equal(graph.nodeMap['2'].getVisibleEdgesOut().length, 0);
    equal(graph.nodeMap['3'].getVisibleEdgesOut().length, 0);
    equal(graph.nodeMap['4'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['5'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['6'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['7'].getVisibleEdgesOut().length, 2);
    equal(graph.nodeMap['8'].getVisibleEdgesIn().length, 0);
    equal(graph.nodeMap['10'].getVisibleEdgesIn().length, 3);

    graph.nodeMap['9'].expand(); // nemel by udelat nic...
    graph.nodeMap['10'].expand();
    equal(graph.nodeMap['1'].getVisibleEdgesOut().length, 0);
    equal(graph.nodeMap['2'].getVisibleEdgesOut().length, 0);
    equal(graph.nodeMap['3'].getVisibleEdgesOut().length, 0);
    equal(graph.nodeMap['4'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['5'].getVisibleEdgesOut().length, 2);
    equal(graph.nodeMap['6'].getVisibleEdgesOut().length, 1);
    equal(graph.nodeMap['7'].getVisibleEdgesOut().length, 3);
    equal(graph.nodeMap['8'].getVisibleEdgesIn().length, 0);
    equal(graph.nodeMap['9'].getVisibleEdgesIn().length, 0);
    equal(graph.nodeMap['10'].getVisibleEdgesIn().length, 0);
    equal(graph.nodeMap['11'].getVisibleEdgesIn().length, 2);
    equal(graph.nodeMap['12'].getVisibleEdgesIn().length, 2);
    equal(graph.nodeMap['13'].getVisibleEdgesIn().length, 3);
});

//</editor-fold>

//<editor-fold desc="SHADOWS">

//noinspection JSUnresolvedFunction
test( "Graph - Create Shadow Node - simple", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var node;
    var child;
    var shadow;

    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'), '1');
    graph.addNode(graphFactory.createNode('3'), '1');

    node = graph.nodeMap['1'];
    node.atomic = false;
    child = graph.nodeMap['2'];
    shadow = node.createShadowNode([child]);
    equal(node.shadows.length, 1);
    equal(shadow.owner, node);
    ok(shadow.shadow);
    equal(child.parent, shadow);
    equal(graph.nodeMap['3'].parent, node);
    equal(node.children.length, 1);
    equal(node.children[0].id, '3');
    equal(shadow.children.length, 1);
    equal(shadow.children[0].id, '2');
});

//noinspection JSUnresolvedFunction
test( "Graph - Create Shadow Node - Collapsed Nodes with edges", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var node;
    var child;
    var shadow;

    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'), '1');
    graph.addNode(graphFactory.createNode('3'), '1');

    graph.addNode(graphFactory.createNode('4'));
    graph.addNode(graphFactory.createNode('5'), '4');
    graph.addNode(graphFactory.createNode('6'), '4');

    graph.addEdgeBetween(graphFactory.createEdge(), '2', '5');
    graph.addEdgeBetween(graphFactory.createEdge(), '3', '6');
    graph.addEdgeBetween(graphFactory.createEdge(), '2', '6');

    node = graph.nodeMap['1'];
    node.atomic = false;
    node.collapse();

    child = graph.nodeMap['2'];
    shadow = node.createShadowNode([child]);

    ok(node.hasShadowNodes());
    equal(node.shadows.length, 1);
    equal(node.children.length, 1);
    equal(shadow.children.length, 1);
    equal(node.edgesOut.length, 1);
    equal(node.edgesOut[0].target.id, '6');
    equal(shadow.edgesOut.length, 2);
    equal(shadow.edgesOut[0].target.id, '5');
    equal(shadow.edgesOut[1].target.id, '6');

    graph.nodeMap['4'].collapse();

    equal(node.edgesOut.length, 1);
    equal(node.edgesOut[0].target.id, '4');
    equal(shadow.edgesOut.length, 1);
    equal(shadow.edgesOut[0].target.id, '4');
});

//noinspection JSUnresolvedFunction
test( "Graph - Clear Shadow Nodes - simple", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var node;
    var child;
    var shadow;

    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'), '1');
    graph.addNode(graphFactory.createNode('3'), '1');

    node = graph.nodeMap['1'];
    node.atomic = false;
    child = graph.nodeMap['2'];
    shadow = node.createShadowNode([child]);
    node.clearShadowNodes();

    equal(node.shadows.length, 0);
    equal(node.children.length, 2);
    equal(child.parent, node);
    equal(graph.nodeMap['3'].parent, node);
    ok(!(shadow.id in graph.nodeMap));
    ok(!node.hasShadowNodes());
});


//noinspection JSUnresolvedFunction
test( "Graph - Clear Shadow Node - Collapsed Nodes with edges", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var node;
    var child;
    var shadow;

    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'), '1');
    graph.addNode(graphFactory.createNode('3'), '1');

    graph.addNode(graphFactory.createNode('4'));
    graph.addNode(graphFactory.createNode('5'), '4');
    graph.addNode(graphFactory.createNode('6'), '4');

    graph.addEdgeBetween(graphFactory.createEdge(), '2', '5');
    graph.addEdgeBetween(graphFactory.createEdge(), '3', '6');
    graph.addEdgeBetween(graphFactory.createEdge(), '2', '6');

    node = graph.nodeMap['1'];
    node.atomic = false;
    node.collapse();

    child = graph.nodeMap['2'];
    shadow = node.createShadowNode([child]);
    node.clearShadowNodes();

    equal(node.shadows.length, 0);
    ok(!(shadow.id in graph.nodeMap));
    equal(node.children.length, 2);
    equal(node.edgesOut.length, 2);
    equal(node.edgesOut[0].target.id, '5');
    equal(node.edgesOut[1].target.id, '6');

    graph.nodeMap['4'].collapse();

    equal(node.edgesOut.length, 1);
    equal(node.edgesOut[0].target.id, '4');
});
//</editor-fold>











