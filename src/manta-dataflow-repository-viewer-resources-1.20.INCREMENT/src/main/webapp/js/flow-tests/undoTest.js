test( "Undo - Serialize node state", function() {
    // otestuje, zda jsem nezapomnel zachytit nejakou vlastnost uzlu
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var nodeState, node, prop;
    // vlastnosti, ktere se z uzlu neprenaseji:
    var ignoredProperties = ["children", "edgesIn", "edgesOut", "graph"];
    // vlastnosti, jejichz hodnoty se lisi u uzlu a jeho stavu:
    var notEqualProperties = ["bounds", "shadows", "path", "attributes", "owner"];
    var json;

    // stav grafu pred mergem:
    graphFactory.createAndAddNodes([0,1,2]);
    graphFactory.createAndAddEdges([[0,1],[0,2],[1,2]]);

    node = graph.nodeMap[0];
    nodeState = node.serialize();

    json = JSON.stringify(nodeState);
    ok(json);


    for(prop in node) {
        if(node.hasOwnProperty(prop) && !ignoredProperties.contains(prop)) {
            ok(nodeState.hasOwnProperty(prop), "Node property: " + prop);
            if(!notEqualProperties.contains(prop)) {
                equal(nodeState[prop], node[prop], "Node property equality: " + prop);
            }
        }
    }
});

test( "Undo - Deserialize node state", function() {
    // otestuje, zda jsem nezapomnel zachytit nejakou vlastnost uzlu
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var nodeState, node, prop, newNode;
    // vlastnosti, ktere se z uzlu neprenaseji:
    var ignoredProperties = ["children", "edgesIn", "edgesOut", "graph"];
    // vlastnosti, jejichz hodnoty se lisi u uzlu a jeho stavu:
    var notEqualProperties = ["bounds", "shadows", "path", "attributes", "owner"];

    // stav grafu pred mergem:
    graphFactory.createAndAddNodes([0,1,2]);
    graphFactory.createAndAddEdges([[0,1],[0,2],[1,2]]);

    node = graph.nodeMap[0];
    nodeState = node.serialize();

    newNode = new Manta.Semantics.Node();
    newNode.deserialize(nodeState);

    graph.addNode(newNode, nodeState.parent);

    for(prop in newNode) {
        if(newNode.hasOwnProperty(prop) && !ignoredProperties.contains(prop)) {
            ok(newNode.hasOwnProperty(prop), "Node property: " + prop);
            if(!notEqualProperties.contains(prop)) {
                equal(newNode[prop], node[prop], "Node property equality: " + prop);
            }
        }
    }
});

test( "Undo - Serialize edge state", function() {
    // otestuje, zda jsem nezapomnel zachytit nejakou vlastnost hrany
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var edgeState, edge, prop;
    // vlastnosti, ktere se z uzlu neprenaseji:
    var ignoredProperties = ["children", "semanticType"];
    // vlastnosti, jejichz hodnoty se lisi u hrany a jejiho stavu:
    var notEqualProperties = ["source", "target", "bendpoints", "nodesOnPath"];
    var json;

    // stav grafu pred mergem:
    graphFactory.createAndAddNodes([0,1,2]);
    graphFactory.createAndAddEdges([[0,1],[0,2],[1,2]]);

    edge = graph.nodeMap[0].edgesOut[0];
    edgeState = edge.serialize();

    json = JSON.stringify(edgeState);
    ok(json);

    for(prop in edge) {
        if(edge.hasOwnProperty(prop) && !ignoredProperties.contains(prop)) {
            ok(edgeState.hasOwnProperty(prop), "Edge property: " + prop);
            if(!notEqualProperties.contains(prop)) {
                equal(edgeState[prop], edge[prop], "Edge property equality: " + prop);
            }
        }
    }
});

test( "Undo - Deserialize edge state", function() {
    // otestuje, zda jsem nezapomnel zachytit nejakou vlastnost hrany
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var edgeState, edge, prop, newEdge;
    // vlastnosti, ktere se z uzlu neprenaseji:
    var ignoredProperties = ["children", "semanticType"];
    // vlastnosti, jejichz hodnoty se lisi u hrany a jejiho stavu:
    var notEqualProperties = ["source", "target", "bendpoints", "nodesOnPath"];
    var json;

    // stav grafu pred mergem:
    graphFactory.createAndAddNodes([0,1,2]);
    graphFactory.createAndAddEdges([[0,1],[0,2],[1,2]]);

    edge = graph.nodeMap[0].edgesOut[0];
    edgeState = edge.serialize();

    newEdge = new Manta.Semantics.Edge();
    newEdge.deserialize(edgeState);

    for(prop in newEdge) {
        if(newEdge.hasOwnProperty(prop) && !ignoredProperties.contains(prop)) {
            ok(newEdge.hasOwnProperty(prop), "Edge property: " + prop);
            if(!notEqualProperties.contains(prop)) {
                equal(newEdge[prop], edge[prop], "Edge property equality: " + prop);
            }
        }
    }
});