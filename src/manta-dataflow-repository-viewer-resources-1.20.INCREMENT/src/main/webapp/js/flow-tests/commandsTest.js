/**
 * Created by mkrutsky on 29.4.2014.
 */
test( "Follow Operation - Simple Node Merge", function() {
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var responseFactory = new Manta.Test.ResponseFactory();
    var context;
    var follow;

    // stav grafu pred mergem:
    graphFactory.createAndAddNodes([0,1,2]);
    graphFactory.createAndAddEdges([[0,1],[0,2],[1,2]]);

    responseFactory.createFollowResponse([
        { id: 2, children: [{id: 3},{id: 4}] },
        { id: 5, children: [
            { id: 6, children: [{id: 9}, {id: 10}] },
            { id: 7}]
        },
        { id: 1, children: [{id: 8}] }
    ]);

    context = new Manta.Operations.OperationContext(graph, {}, {}, {}, {});
    follow = new Manta.Operations.Follow(context);
    follow._mergeComponentsIntoGraph(responseFactory.getResponse().components);

    equal(graph.getAllNodes().length, 11);
    equal(Object.keys(graph.nodeMap).length, 11);
    equal(graph.children.length, 4);
    equal(graph.nodeMap['1'].children.length, 1);
    equal(graph.nodeMap['2'].children.length, 2);
    equal(graph.nodeMap['3'].children.length, 0);
    equal(graph.nodeMap['4'].children.length, 0);
    equal(graph.nodeMap['5'].children.length, 2);
    equal(graph.nodeMap['6'].children.length, 2);
    equal(graph.nodeMap['7'].children.length, 0);
    equal(graph.nodeMap['8'].children.length, 0);
    equal(graph.nodeMap['9'].children.length, 0);
    equal(graph.nodeMap['10'].children.length, 0);
});

test( "Filter Operation - Remove starting nodes", function() {
    // odebere z filtrovane mnoziny startovni uzly a vsechny jejich predky a potomky
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var context, filter, nodesToFilter;
    Manta.Globals.formData.selectedItems = [3];

    graph.addNode(graphFactory.createNode('1'));
    graph.addNode(graphFactory.createNode('2'), '1');
    graph.addNode(graphFactory.createNode('3'), '2');
    graph.addNode(graphFactory.createNode('4'), '2');
    graph.addNode(graphFactory.createNode('5'), '3');
    graph.addNode(graphFactory.createNode('6'), '3');
    graph.addNode(graphFactory.createNode('7'));
    graph.addNode(graphFactory.createNode('8'), '7');
    graph.addNode(graphFactory.createNode('9'), '7');

    context = new Manta.Operations.OperationContext(graph, {}, {}, {}, {});
    filter = new Manta.Operations.Filter(context);
    nodesToFilter = graph.getAllNodes().slice();

    filter._removeStartingNodes(nodesToFilter);

    ok(!nodesToFilter.contains(graph.nodeMap['1']));
    ok(!nodesToFilter.contains(graph.nodeMap['2']));
    ok(!nodesToFilter.contains(graph.nodeMap['3']));
    ok(nodesToFilter.contains(graph.nodeMap['4']));
    ok(!nodesToFilter.contains(graph.nodeMap['5']));
    ok(!nodesToFilter.contains(graph.nodeMap['6']));
    ok(nodesToFilter.contains(graph.nodeMap['7']));
    ok(nodesToFilter.contains(graph.nodeMap['8']));
    ok(nodesToFilter.contains(graph.nodeMap['9']));

    // cleanup
    Manta.Globals.formData.selectedItems = [];
});
