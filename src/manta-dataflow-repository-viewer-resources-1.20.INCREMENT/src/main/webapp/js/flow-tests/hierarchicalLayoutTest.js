/**
 * Created by mkrutsky on 14.3.14.
 */

test( "Hierarchical Layout: Cycle breaking - SCC - simple", function() {
    var layout;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    
    // binarni strom s jednim cyklem
    graphFactory.createAndAddNodes([1,2,3,4,5,6,7]);
    graphFactory.createAndAddEdges([[1,2], [1,3], [2,4], [2,5], [3,6], [3,7], [5,1]]);

    layout = new Manta.Layout.Hierarchical(graph);    
    
    // schvalne spoustim dvakrat - test jestli se cycleBreaking 
    // vrati po exekuci do konzistentniho stavu
    layout.cycleBreaking.getStronglyConnectedComponents();
    layout.cycleBreaking.getStronglyConnectedComponents();    
    
    equal(layout.cycleBreaking.components.length, 1);
    equal(Object.keys(layout.cycleBreaking.components[0]).length, 3);
    equal(layout.cycleBreaking.componentId['1'], 1);
    equal(layout.cycleBreaking.componentId['2'], 1);
    equal(layout.cycleBreaking.componentId['3'], 3);
    equal(layout.cycleBreaking.componentId['4'], 4);
    equal(layout.cycleBreaking.componentId['5'], 1);
    equal(layout.cycleBreaking.componentId['6'], 6);
    equal(layout.cycleBreaking.componentId['7'], 7);    
});

test( "Hierarchical Layout: Cycle breaking - SCC - medium", function() {
    var layout;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    
    // Graf - viz http://en.wikipedia.org/wiki/File:Scc.png 
    graphFactory.createAndAddNodes([1, 2, 3, 4, 5, 6, 7, 8]);
    graphFactory.createAndAddEdges([[1,2],[2,3],[2,5],[2,6],[3,4],[3,7],[4,3],[4,8],[5,1],[5,6],[6,7],[7,6],[8,4],[8,7]]);
    
    layout = new Manta.Layout.Hierarchical(graph);
    layout.cycleBreaking.getStronglyConnectedComponents();
    
    equal(layout.cycleBreaking.components.length, 3);
    equal(Object.keys(layout.cycleBreaking.components[0]).length, 2);
    equal(Object.keys(layout.cycleBreaking.components[1]).length, 3);
    equal(Object.keys(layout.cycleBreaking.components[2]).length, 3);
    equal(layout.cycleBreaking.componentId['1'], 1);    
    equal(layout.cycleBreaking.componentId['2'], 1);    
    equal(layout.cycleBreaking.componentId['3'], 3);    
    equal(layout.cycleBreaking.componentId['4'], 3);    
    equal(layout.cycleBreaking.componentId['5'], 1);    
    equal(layout.cycleBreaking.componentId['6'], 7);    
    equal(layout.cycleBreaking.componentId['7'], 7);    
    equal(layout.cycleBreaking.componentId['8'], 3);    
});

test( "Hierarchical Layout: Cycle breaking - execute - simple", function() {
    var layout;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    // binarni strom s jednim cyklem a propojenymi listy
    graphFactory.createAndAddNodes([1,2,3,4,5,6,7]);
    graphFactory.createAndAddEdges([[1,2], [1,3], [2,4], [2,5], [3,6], [3,7], [5,1], [5,4], [2,6]]);

    layout = new Manta.Layout.Hierarchical(graph);
    layout.cycleBreaking.execute();
    
    equal(graph.nodeMap['1'].edgesIn.length, 0);
    equal(graph.nodeMap['1'].edgesOut.length, 3);
    equal(graph.nodeMap['5'].edgesIn.length, 2);
    equal(graph.nodeMap['5'].edgesOut.length, 1);
    equal(graph.nodeMap['4'].edgesIn.length, 2);
    equal(graph.nodeMap['4'].edgesOut.length, 0);
    equal(graph.nodeMap['2'].edgesIn.length, 1);
    equal(graph.nodeMap['2'].edgesOut.length, 3);
    equal(graph.nodeMap['6'].edgesIn.length, 2);
    equal(graph.nodeMap['6'].edgesOut.length, 0);
});
      
test( "Hierarchical Layout: Cycle breaking - execute - medium", function() {
    var layout;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    // Graf - viz http://en.wikipedia.org/wiki/File:Scc.png 
    graphFactory.createAndAddNodes([1, 2, 3, 4, 5, 6, 7, 8]);
    graphFactory.createAndAddEdges([[1,2],[2,3],[2,5],[2,6],[3,4],[3,7],[4,3],[4,8],[5,1],[5,6],[6,7],[7,6],[8,4],[8,7]]);

    layout = new Manta.Layout.Hierarchical(graph);
    layout.cycleBreaking.execute();

    // test dobehne, ktere hrany se obrati uz neni nutne napevno dane
    // musi jich byt ale minimum = 4
    equal(layout.cycleBreaking.reversed.length, 4);
});

test( "Hierarchical Layout: Cycle breaking - reverse right edge", function() {
    var layout;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var component;

    graphFactory.createAndAddNodes([1, 2, 3, 4, 5, 6]);
    graphFactory.createAndAddEdges([[1,2],[1,3],[2,4],[2,5],[3,6],[4,6],[5,6],[6,1]]);

    layout = new Manta.Layout.Hierarchical(graph);
    component = [];
    component.push(graph.nodeMap[5]);
    component.push(graph.nodeMap[6]);
    component.push(graph.nodeMap[1]);
    component.push(graph.nodeMap[2]);
    component.push(graph.nodeMap[3]);
    component.push(graph.nodeMap[4]);

    layout.cycleBreaking.reverseMostConnectedEdge(component);

    equal(layout.cycleBreaking.reversed.length, 1);
    equal(layout.cycleBreaking.reversed[0].source.id, 1);
    equal(layout.cycleBreaking.reversed[0].target.id, 6);
});

test( "Hierarchical Layout: Cycle breaking - reverse right edge 2", function() {
    var layout;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var component;

    graphFactory.createAndAddNodes([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    graphFactory.createAndAddEdges([[1,2],[1,3],[1,4],[2,5],[2,6],[3,7],[4,7],[5,7],[6,7],[6,9],[7,8],[7,9],[8,9],[8,1],[9,1]]);

    layout = new Manta.Layout.Hierarchical(graph);
    component = [];
    component.push(graph.nodeMap[4]);
    component.push(graph.nodeMap[6]);
    component.push(graph.nodeMap[1]);
    component.push(graph.nodeMap[2]);
    component.push(graph.nodeMap[3]);
    component.push(graph.nodeMap[4]);
    component.push(graph.nodeMap[5]);
    component.push(graph.nodeMap[7]);
    component.push(graph.nodeMap[8]);
    component.push(graph.nodeMap[9]);

    layout.cycleBreaking.reverseMostConnectedEdge(component);

    equal(layout.cycleBreaking.reversed.length, 1);
    equal(layout.cycleBreaking.reversed[0].source.id, 1);
    equal(layout.cycleBreaking.reversed[0].target.id, 9);
});

test( "Hierarchical Layout: Cycle breaking - find most frequented edge", function() {
    var layout;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);
    var score;

    graphFactory.createAndAddNodes([1, 2, 3, 4, 5, 6]);
    graphFactory.createAndAddEdges([[1,2],[2,3],[2,4],[4,5],[4,6],[3,1],[4,1],[6,1]]);
    layout = new Manta.Layout.Hierarchical(graph);

    score = layout.cycleBreaking.evaluateEdgeConnectionsDFS(graph.getAllNodes());

    equal(score[graph.nodeMap[2].findEdgeFrom(graph.nodeMap[1]).id], 3);
    equal(score[graph.nodeMap[3].findEdgeFrom(graph.nodeMap[2]).id], 1);
    equal(score[graph.nodeMap[4].findEdgeFrom(graph.nodeMap[2]).id], 2);
    equal(score[graph.nodeMap[6].findEdgeFrom(graph.nodeMap[4]).id], 1);
});

test( "Hierarchical Layout: Longest Path Layer Assignment - execute - trivial", function() {
    var layout;
    var layers;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graphFactory.createAndAddNodes([1,2,3]);
    graphFactory.createAndAddEdges([[1,2], [1,3]]);

    layout = new Manta.Layout.Hierarchical(graph);
    layout.cycleBreaking.execute();
    layout.layerAssignment.execute();

    layers = layout.layerAssignment.layers;

    equal(layers.length, 2);
    equal(layers[0].size(), 1);
    ok(layers[0].containsId(1));
    equal(layers[1].size(), 2);
    ok(layers[1].containsId(2));
    ok(layers[1].containsId(3));    
});

test( "Hierarchical Layout: Longest Path Layer Assignment - execute - simple", function() {
    var layout;
    var layers;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    // binarni strom s jednim cyklem a propojenymi listy
    graphFactory.createAndAddNodes([1,2,3,4,5,6,7]);
    graphFactory.createAndAddEdges([[1,2], [1,3], [2,4], [2,5], [3,6], [3,7], [5,1], [5,4], [2,6]]);

    layout = new Manta.Layout.Hierarchical(graph);
    layout.cycleBreaking.execute();
    layout.layerAssignment.execute();

    layers = layout.layerAssignment.layers;
    
    equal(layers.length, 4);
    equal(layers[0].size(), 1);
    ok(layers[0].containsId(1));
    equal(layers[1].size(), 3);
    ok(layers[1].containsId(2));
    ok(layers[1].containsId(3));
    equal(layers[2].size(), 4);
    ok(layers[2].containsId(5));
    ok(layers[2].containsId(6));
    ok(layers[2].containsId(7));
    equal(layers[3].size(), 1);
    ok(layers[3].containsId(4));
});

test( "Hierarchical Layout: Longest Path Layer Assignment - execute - medium", function() {
    var layout, layers;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    // Graf - viz http://en.wikipedia.org/wiki/File:Scc.png 
    graphFactory.createAndAddNodes([1, 2, 3, 4, 5, 6, 7, 8]);
    graphFactory.createAndAddEdges([[1,2],[2,3],[2,5],[2,6],[3,4],[3,7],[4,3],[4,8],[5,1],[5,6],[6,7],[7,6],[8,4],[8,7]]);

    layout = new Manta.Layout.Hierarchical(graph);
    layout.cycleBreaking.execute();
    layout.layerAssignment.execute();
    
    layers = layout.layerAssignment.layers;
    
    equal(layers.length, 7);
    equal(layers[0].size(), 1);
    ok(layers[0].containsId(1));
    equal(layers[1].size(), 2);
    ok(layers[1].containsId(2));
    equal(layers[2].size(), 4);
    ok(layers[2].containsId(3));
    equal(layers[3].size(), 5);
    ok(layers[3].containsId(4));
    equal(layers[4].size(), 5);
    ok(layers[4].containsId(8));
    equal(layers[5].size(), 3);
    ok(layers[5].containsId(5));
    ok(layers[5].containsId(7));
    equal(layers[6].size(), 1);
    ok(layers[6].containsId(6));
});


test( "Hierarchical Layout: Cross reduction - execute - simple", function() {
    var layout, layers;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graphFactory.createAndAddNodes([1, 2, 3, 4, 5, 6, 7]);
    graphFactory.createAndAddEdges([[1,4],[2,3],[3,5],[3,7],[4,6]]);

    layout = new Manta.Layout.Hierarchical(graph);
    layout.cycleBreaking.execute();

    layers = layout.layerAssignment.execute();
    layout.crossReduction.execute(layers);

    equal(Manta.TestUtils.computeCrossings(layers), 0);
    equal(layers[0].size(), 2);
    equal(layers[0].getVertexByNodeId(1).position, 0);
    equal(layers[0].getVertexByNodeId(2).position, 1);
    equal(layers[1].size(), 2);
    equal(layers[1].getVertexByNodeId(4).position, 0);
    equal(layers[1].getVertexByNodeId(3).position, 1);
    equal(layers[2].size(), 3);
    equal(layers[2].getVertexByNodeId(6).position, 0);
    equal(layers[2].getVertexByNodeId(5).position, 1);
    equal(layers[2].getVertexByNodeId(7).position, 2);
});

test( "Hierarchical Layout: Cross reduction - execute - medium", function() {
    var layout, layers;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graphFactory.createAndAddNodes([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]);
    graphFactory.createAndAddEdges([[1,4],[1,5],[1,6],[2,5],[2,7],[3,4],[3,5],[4,9],[4,11],
        [5,12],[6,8],[6,10],[7,9],[7,11],[8,14],[8,15],[9,13],[9,15],[10,14],[11,14],[11,15],
        [12,13],[13,16],[14,17]]);

    layout = new Manta.Layout.Hierarchical(graph);
    layout.cycleBreaking.execute();
    layers = layout.layerAssignment.execute();
    layout.crossReduction.execute(layers);

    equal(Manta.TestUtils.computeCrossings(layers), 5);
    equal(layers[0].size(), 3);
    equal(layers[0].getVertexByNodeId(1).position, 0);
    equal(layers[0].getVertexByNodeId(3).position, 1);
    equal(layers[0].getVertexByNodeId(2).position, 2);
    equal(layers[1].size(), 4);
    equal(layers[1].getVertexByNodeId(6).position, 0);
    equal(layers[1].getVertexByNodeId(4).position, 1);
    /*equal(layers[1].getVertexByNodeId(7).position, 2);
    equal(layers[1].getVertexByNodeId(5).position, 3);*/
    // zde jsou prohozeny pozice, kvuli prohozeni poradi vypoctu medianu z duvodu estetiky
    equal(layers[1].getVertexByNodeId(7).position, 3);
    equal(layers[1].getVertexByNodeId(5).position, 2);
    equal(layers[2].size(), 5);
    equal(layers[2].getVertexByNodeId(10).position, 0);
    equal(layers[2].getVertexByNodeId(8).position, 1);
    equal(layers[2].getVertexByNodeId(11).position, 2);
    equal(layers[2].getVertexByNodeId(9).position, 3);
    equal(layers[2].getVertexByNodeId(12).position, 4);
    equal(layers[3].size(), 3);
    equal(layers[3].getVertexByNodeId(14).position, 0);
    equal(layers[3].getVertexByNodeId(15).position, 1);
    equal(layers[3].getVertexByNodeId(13).position, 2);
    equal(layers[4].size(), 2);
    equal(layers[4].getVertexByNodeId(17).position, 0);
    equal(layers[4].getVertexByNodeId(16).position, 1);
});

test( "Hierarchical Layout: Cross reduction - execute - medium 2", function() {
    var layout, layers;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    // TODO: Jak udelat cross reduction nezavisle na poradi vstupu? (viz prohozeni 1-4 a 1-21)
    graphFactory.createAndAddNodes([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]);
    graphFactory.createAndAddEdges([[1,3],[1,13],[1,4],[1,21],[2,3],[2,20],[3,23],[3,4],[3,5],[4,6],[5,7],[6,8],
        [6,16],[6,23], [7,9],[8,10],[8,11],[9,12],[10,13],[10,14],[10,15],[11,15],[11,16],
        [12,20],[13,17],[14,17],[14,18],[16,18],[16,19],[16,20],[18,21],[19,22],[21,23],[22,23]]);

    layout = new Manta.Layout.Hierarchical(graph);
    layout.cycleBreaking.execute();
    layers = layout.layerAssignment.execute();
    layout.crossReduction.execute(layers);

    equal(layers.length, 10);
    equal(Manta.TestUtils.computeCrossings(layers), 6);
    equal(layers[0].size(), 2);
    equal(layers[0].getVertexByNodeId(1).position, 0);
    equal(layers[0].getVertexByNodeId(2).position, 1);
    equal(layers[1].size(), 5);
    equal(layers[1].getVertexByNodeId(3).position, 3);
    equal(layers[2].size(), 6);
    equal(layers[2].getVertexByNodeId(4).position, 1);
    //equal(layers[2].getVertexByNodeId(5).position, 4);
    equal(layers[3].size(), 6);
    equal(layers[3].getVertexByNodeId(6).position, 1);
    equal(layers[3].getVertexByNodeId(5).position, 4);
    equal(layers[4].size(), 8);
    equal(layers[4].getVertexByNodeId(8).position, 1);
    equal(layers[4].getVertexByNodeId(7).position, 6);
    equal(layers[5].size(), 9);
    equal(layers[5].getVertexByNodeId(10).position, 1);
    equal(layers[5].getVertexByNodeId(11).position, 2);
    equal(layers[5].getVertexByNodeId(9).position, 7);
    equal(layers[6].size(), 9);
    equal(layers[6].getVertexByNodeId(12).position, 7);
    equal(layers[6].getVertexByNodeId(13).position, 0);
    equal(layers[6].getVertexByNodeId(14).position, 1);
    equal(layers[6].getVertexByNodeId(15).position, 2);
    equal(layers[6].getVertexByNodeId(16).position, 3);
    equal(layers[7].size(), 7);
    equal(layers[7].getVertexByNodeId(17).position, 0);
    // zde jsou prohozeny pozice, kvuli prohozeni poradi vypoctu medianu z duvodu estetiky
    /*equal(layers[7].getVertexByNodeId(18).position, 2);
    equal(layers[7].getVertexByNodeId(19).position, 1);*/
    equal(layers[7].getVertexByNodeId(20).position, 6);
    equal(layers[8].size(), 4);
    equal(layers[8].getVertexByNodeId(21).position, 1);
    equal(layers[8].getVertexByNodeId(22).position, 0);
    equal(layers[9].size(), 1);
    equal(layers[9].getVertexByNodeId(23).position, 0);
});

test( "Hierarchical Layout: Vertex promotion heuristic", function() {
    var layout, layers;
    var graph = new Manta.Semantics.Graph();
    var graphFactory = new Manta.Test.GraphFactory(graph);

    graphFactory.createAndAddNodes([1,2,3,4,5,6,7,8]);
    graphFactory.createAndAddEdges([[1,2], [2,3], [3,4], [5,6], [6,4], [7,6], [7,4], [8,4]]);

    layout = new Manta.Layout.Hierarchical(graph);
    layout.cycleBreaking.execute();
    layers = layout.layerAssignment.execute();

    equal(layers[0].size(), 1);
    equal(layers[1].size(), 3);
    equal(layers[2].size(), 4);
    equal(layers[3].size(), 1);

    ok(layers[0].containsId(1));
    ok(layers[1].containsId(2));
    ok(layers[1].containsId(5));
    ok(layers[1].containsId(7));
    ok(layers[2].containsId(3));
    ok(layers[2].containsId(6));
    ok(layers[2].containsId(8));
    ok(layers[3].containsId(4));
});