/**************************************************************************
 * @namespace    Manta.Operations
 * @author       Matyas Krutsky
 *
 * Operace undo a redo.
 *
 **************************************************************************/

(function(ns, $) {

    ns.UndoRedo = function(context) {
        ns.BaseOperation.call(this, ns.UndoRedo.NAME, context);
    };

    ns.UndoRedo.prototype = Object.create(ns.BaseOperation.prototype);
    ns.UndoRedo.NAME = 'undoRedo';

    /**
     * Vyvola undo/redo a potrebne refreshe controlleru. Nevola se layout, pozice
     * se pouziji ze zachyceneho stavu.
     * @param direction
     */
    ns.UndoRedo.prototype.invoke = function(direction) {
        var undoManager = this.context.undoManager,
            graph = this.context.graph,
            newState;

        if(direction === Manta.Undo.Direction.UNDO) {
            newState = undoManager.previousState();
        } else if(direction === Manta.Undo.Direction.REDO) {
            newState = undoManager.nextState();
        }

        if(JSON) {
            // pokud je mozna serializovat do JSON, musim opet deserializovat stav
            newState = JSON.parse(newState);
        }

        graph.wipeOut(); // vycisteni grafu
        this._updateNodes(newState);
        this._updateEdges(newState);
        this._updateFilters(newState);
        this._updateColapseSettings(newState);
        this._updateColorLayers(newState);
        this._updateStagePosition(newState);
        graph.validateStructure();
        this.context.undoManager.setServerState(newState.serverState);

        // zneplatnim vsechny predchozi hodnoty pro layout, jelikoz uz nemusi platit
        this.context.layoutManager.setForgetState(true);

        this.context.graphController.clearSelection();
        this.context.graphController.refresh();
        Manta.DataflowUI.redrawStage();
        this.finish();
    };

    ns.UndoRedo.prototype.receiveResponse = function(response, callback) {
        // neprijimam zadne zpravy...
    };

    /**
     * Vlozi do grafu vsechny hrany, ktere byly ulozeny v predanem stavu
     * @param newState stav, ze ktereho se berou hrany
     * @private
     */
    ns.UndoRedo.prototype._updateEdges = function(newState) {
        var i, edgeLength = newState.edgeStates.length,
            edge,
            edgeState,
            edgeIndex = {},
            graph = this.context.graph,
            parent;

        // nejdriv znovuvytvorim a vlozim vsechny hrany do grafu
        for(i=0; i < edgeLength; i++) {
            edgeState = newState.edgeStates[i];
            edge = new Manta.Semantics.Edge();
            edge.deserialize(edgeState, graph);

            graph.addEdgeBetween(edge, edgeState.source, edgeState.target);
            edgeIndex[edge.id] = edge;
        }

        // v druhem kole pak zapojim hierarchii hran
        for(i=0; i < edgeLength; i++) {
            edgeState = newState.edgeStates[i];
            edge = edgeIndex[edgeState.id];
            if(edgeState.parent) {
                parent = edgeIndex[edgeState.parent];
                edge.parent = parent;
                parent.children.push(edge);
            }
        }
    };

    /**
     * Vlozi do grafu vsechny uzly, ktere byly ulozeny v predanem stavu
     * @param newState stav, ze ktereho se berou uzly
     * @private
     */
    ns.UndoRedo.prototype._updateNodes = function(newState) {
        var i, nodeLength = newState.nodeStates.length,
            j,
            nodeState,
            node,
            graph = this.context.graph;

        // vlozim nove uzly do grafu
        for(i=0; i < nodeLength; i++) {
            nodeState = newState.nodeStates[i];
            node = new Manta.Semantics.Node();
            node.deserialize(newState.nodeStates[i]);
            graph.addNode(node, nodeState.parent);
        }

        // doplnim shadow uzly a ownery
        for(i=0; i < nodeLength; i++) {
            nodeState = newState.nodeStates[i];
            node = graph.nodeMap[nodeState.id];
            node.owner = graph.nodeMap[nodeState.owner];

            for(j=0; j < nodeState.shadows.length; j++) {
                node.shadows.push(graph.nodeMap[nodeState.shadows[j]]);
            }
        }
    };

    /**
     * Obnovi nastaveni filteru z predaneho stavu
     * @param newState stav, ze ktereho se cte nastaveni filteru
     * @private
     */
    ns.UndoRedo.prototype._updateFilters = function(newState) {
        this.context.graph.filterManager.applyState(newState.filterState);
    };
    
    /**
     * Obnovi nastaveni hromadneho sbaleni/rozbaleni z predaneho stavu
     * @param newState stav, ze ktereho se cte nastaveni filteru
     * @private
     */
    ns.UndoRedo.prototype._updateColapseSettings = function(newState) {
        this.context.collapseManager.setSettings(newState.collapseState);
    };
    
    /**
     * Obnovi nastaveni barevnych vrstev z predaneho stavu
     * @param newState stav, ze ktereho se cte nastaveni filteru
     * @private
     */
    ns.UndoRedo.prototype._updateColorLayers = function(newState) {
        if (Manta.PresentationManager.isCurrentStateDifferent(newState.colorState)) {
            Window.operationContext.graphController.revalidateWidgets();
        }        
        
        Manta.PresentationManager.setLayersState(newState.colorState);
    };

    /**
     * Obnovi pozici stage z predaneho stavu
     * @param newState stav, ze ktereho se cte poloha stage
     * @private
     */
    ns.UndoRedo.prototype._updateStagePosition = function(newState) {
        this.context.stage.setPosition($.extend(true, {}, newState.stagePosition));
    };

}(Manta.Operations = Manta.Operations || {}, jQuery));
