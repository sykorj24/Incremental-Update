/**************************************************************************
 * @namespace    Manta.Undo
 * @author       Matyas Krutsky
 *
 * Manager, ktery spravuje undo a redo stavy. V klientske aplikaci by mela
 * byt prave jedna instance teto tridy.
 *
 **************************************************************************/

(function(ns, $) {
    ns.Direction = {
        UNDO: "UNDO",
        REDO: "REDO"
    };

    ns.UndoManager = function(graph, filterManager, stage, collapseManager) {
        this.states = [];
        this.cursor = 0;
        this.graph = graph;
        this.filterManager = filterManager;
        this.collapseManager = collapseManager;
        this.serverState = null;
        this.listeners = {};
        this.stage = stage;
    };

    /**
     * Sejme a vlozi do fronty novy undo stav. Pokud nyni existuji nejake redo stavy, jsou vsechny
     * smazany. Pokud velikost fronty presahne maximalni velikost, nejstarsi stavy jsou
     * odmazany.
     */
    ns.UndoManager.prototype.pushUndoState = function() {
        // sejmu aktualni stav vseho sledovaneho
        var newState = this._captureState();

        // smazu vsechny redo stavy dopredu
        this.states.splice(this.cursor + 1, this.states.length);

        // vlozim novy stav
        this.states.push(newState);

        // oriznu stavy pokud uz jich je moc
        if(this.states.length > Manta.Globals.undoCapacity) {
            this.states.splice(0, this.states.length - Manta.Globals.undoCapacity);
        }

        // posunu kurzor na novy stav
        this.cursor = this.states.length-1;
        this._notify('change');
    };

    /**
     * Presune aktualni stav o jedna dozadu a vrati tetno predchozi stav.
     * @returns {*} Predchozi stav
     */
    ns.UndoManager.prototype.previousState = function() {
        if(!this.canUndo()) {
            throw new Manta.Exceptions.IllegalStateException("Cannot perform Undo!");
        }
        var state = this.states[--this.cursor];
        this._notify('change');
        return state;
    };

    /**
     * Presune aktualni stav o jedna dopredu a vrati aktualni novy stav.
     * @returns {*} Predchozi stav
     */
    ns.UndoManager.prototype.nextState = function() {
        if(!this.canRedo()) {
            throw new Manta.Exceptions.IllegalStateException("Cannot perform Redo!");
        }
        var state = this.states[++this.cursor];
        this._notify('change');
        return state;
    };

    /**
     * Existuje undo stav, ke kteremu se lze vratit
     * @returns {boolean} true, pokud je mozne provest Undo
     */
    ns.UndoManager.prototype.canUndo = function() {
        return this.cursor > 0;
    };

    /**
     * Existuje redo stav, ke kteremu se lze vratit
     * @returns {boolean} true, pokud je mozne provest Redo
     */
    ns.UndoManager.prototype.canRedo = function() {
        return this.cursor < this.states.length-1;
    };

    /**
     * @returns {null|*} Stav serveru, ke kteremu se vztahuje aktualni stav klienta
     */
    ns.UndoManager.prototype.getServerState = function(){
        return this.serverState;
    };

    /**
     * @param serverState Nastavi stav serveru, ke kteremu se vztahuje aktualni stav klienta
     */
    ns.UndoManager.prototype.setServerState = function(serverState){
        this.serverState = serverState;
    };

    /**
     * Zaregostruje posluchace na predanou udalost.
     *
     * Momentalne je podporvana pouze udalost "change"
     *
     * @param event udalost pro kterou registruji posluchace
     * @param func funkce ktera je volana pokud udalost nastane
     */
    ns.UndoManager.prototype.on = function(event, func){
        if(!(event in this.listeners)) {
            this.listeners[event] = [];
        }
        this.listeners[event].push(func);
    };

    /**
     * Zavola posluchace na predane udalosti
     * @param event udalost, pro kterou se volaji posluchaci
     * @private
     */
    ns.UndoManager.prototype._notify = function(event) {
        var i;
        var listeners = this.listeners[event];
        if(listeners) {
            for(i=0; i < listeners.length; i++) {
                listeners[i]();
            }
        }
    };

    /**
     * Zachyti kompletni stav klienta, tzn.
     *
     * - semanticky model
     * - nastaveni filteru
     * - polohu kamery (pozice stage)
     * - stav serveru
     *
     * @returns {State} JSON string objekt reprezentujici kompletni stav klienta v danou chvili
     * @private
     */
    ns.UndoManager.prototype._captureState = function() {
        var state = new ns.State();
        state.serverState = this.serverState;
        this._captureNodeStates(state);
        state.filterState = $.extend(true, {}, this.filterManager.getState());
        state.collapseState = $.extend(true, {}, this.collapseManager.getSettings());
        state.colorState = $.extend(true, {}, Manta.PresentationManager.getLayersState());
        state.stagePosition = $.extend(true, {}, this.stage.getPosition());

        if(JSON) {
            // pokud muzu, serializuji cely stav do JSON
            state = JSON.stringify(state);
        }
        return state;
    };

    /**
     * Zachyti stav vsech uzlu = stav celeho semantickeho modelu
     * @param state stav do ktereho se nahraje stav uzlu
     * @private
     */
    ns.UndoManager.prototype._captureNodeStates = function(state) {
        var i, iLen,
            allNodes = this.graph.getAllNodes();

        for(i=0, iLen = allNodes.length; i < iLen; i++) {
            state.nodeStates.push(allNodes[i].serialize());
            this._captureEdgeStates(state, allNodes[i]);
        }
    };

    /**
     * Zachyti kompletni stav odchozich hran pro dany uzel (kdyz se provede pro vsechny uzly,
     * zachyti se kompletne vsechny hrany)
     * @param state stav do ktereho se nahrava stav hran
     * @param node uzel pro ktery zachycuji odchozi hrany
     * @private
     */
    ns.UndoManager.prototype._captureEdgeStates = function(state, node) {
        var i, iLen,
            edges = node.edgesOut;

        for(i=0, iLen = edges.length; i < iLen; i++) {
            state.edgeStates.push(edges[i].serialize());
        }
    };

    /**
     * Objekt reprezentujici kompletni stav klienta v jeden dany okamzik
     * @constructor
     */
    ns.State = function() {
        this.serverState = null;    // stav serveru (timestamp, ktery urcuje flowState na serveru)
        this.stagePosition = null;  // poloha kamery (= poloha stage)
        this.nodeStates = [];       // stavy vsech uzlu v grafu
        this.edgeStates = [];       // stavy vsech hran v grafu
        this.filterState = {};      // nastaveni filteru
        this.collapseState = {};    // nastaveni hromadne sbaleni/rozbaleni
        this.colorState = {};       // nastaveni barevnych vrstev
    };


}(Manta.Undo = Manta.Undo || {}, jQuery));
