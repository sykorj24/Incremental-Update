/**************************************************************************
* @namespace    Manta.Geometry
* @author       Martin Podloucky
* 
* Geometricka primitiva
* 
**************************************************************************/

(function(ns, $, undefined) {
    
    /**************************************************************************
     * Bod v rovine.
     */
    ns.Point = function(x, y) {
        this.x = x;
        this.y = y;        
    };
    
    /**************************************************************************
     * Ramecek v rovine s konstantnim okrajem
     * padding, width, height 
     */
    ns.Frame = function (p, w, h) {
      this.padding  = p;
      this.width    = w;
      this.height   = h;
      // left, top, right, bottom
      this.l = this.padding;
      this.t = this.padding;
      this.r = this.width - this.padding;
      this.b = this.height - this.padding;
    };
    
    ns.Frame.prototype.setWidth = function(w) {
        this.width = w;  
        this.r = this.width - this.padding;
    };
    
    ns.Frame.prototype.setHeight = function(h) {
        this.height = h;  
        this.b = this.height - this.padding;
    };
      
    ns.Frame.prototype.setPadding = function(p) {
        this.padding  = p;
        this.l = this.padding;
        this.t = this.padding;
        this.r = this.width - this.padding;
        this.b = this.height - this.padding;
    };
    
    /**************************************************************************
     * Obdelnik v rovine. 
     */
    ns.Rectangle = function(x, y, width, height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    };
    
    /**
     * Vypocte stred obdelnika. 
     */
    ns.Rectangle.prototype.center = function() {
        return new ns.Point(this.x + this.width/2,
            this.y + this.height/2);
    };

    /**
     * Vytvori presnou kopii obdelnika. 
     */
    ns.Rectangle.prototype.copy = function() {
        return new ns.Rectangle(this.x, this.y, this.width, this.height);
    };
    
    /**
     * Vypocte bod, ze ktereho ma vychazet hrana, pokud tato hrana spojuje tento obdelnik
     * s nejakym zadanym bodem. 
     * 
     * @param target	Dany bod.
     */
    ns.Rectangle.prototype.pivot = function(target) {
        var center = this.center();

        var dx = target.x - center.x;
        var dy = target.y - center.y;

        var w, h, s;

        if(dy == 0) {
            h = 0;
            w = Infinity;
        }
        else if(dx == 0) {
            h = Infinity;
            w = 0;
        }
        else {
            s = dy/dx;
            h = s * this.width/2;
            w = this.height/2/s;
        }

        if((-this.height/2 <= h) && (h <= this.height/2)) {
            if(target.x > center.x) {
            	// pivot na pravo
                return new ns.Point(center.x + this.width/2, center.y + h);
            } else {
            	// pivot vlevo
                return new ns.Point(center.x - this.width/2, center.y - h);
            }
        } else if((-this.width/2 <= w) && (w <= this.width/2)) {
            if(target.y > center.y) {
            	// pivot dole
                //return new ns.Point(center.x + w, center.y + this.height/2);
            	// TODO: promyslet na jakou stranu pivot dat
            	// fake vzdy vlevo
            	return new ns.Point(center.x - this.width/2, center.y);
            } else {
            	// pivot nahore
                //return new ns.Point(center.x - w, center.y - this.height/2);
            	// TODO: promyslet na jakou stranu pivot dat
            	// fake vzdy vlevo
            	return new ns.Point(center.x - this.width/2, center.y);
            }
        }        
        //throw new Manta.Exceptions.IllegalStateException("Failed to compute the pivot.");
    };
}(Manta.Geometry = Manta.Geometry || {}));


