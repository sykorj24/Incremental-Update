/**************************************************************************
* @namespace    Manta.Graphics
* @author       Martin Podloucky, Martin Slapak
* 
* Realizace vykreslovani grafu za pomoci knihovny KineticJS.
* 
**************************************************************************/

(function (ns, $, undefined) {

    ns.NODE_ID_SEPARATOR = "-";
    
    ns.HEAD_FILL_DEFAULT = "#ffffff";

    /**************************************************************************
     *  Graficka reprezentace celeho grafu. 
     */ 
    ns.GraphWidget = function () {
        this.nodeLayer  = new Kinetic.Layer({draggable: false, listening: true});
        this.edgeLayer  = new Kinetic.Layer({draggable: false, listening: true});
    };

    /**
     * Pridani graficke reprezentace potomka
     * @param widget KineticJS node, ktery se ma pridat.
     */
    ns.GraphWidget.prototype.addChild = function (widget) {
        if (widget.semanticType === Manta.Semantics.SemanticType.EDGE) {
            this.edgeLayer.add(widget.visual);
        } else {
            this.nodeLayer.add(widget.visual);
        }
    };
    
    /**
     * Funkce se postara o obnoveni kese vsech objektu nevyssi urovne.
     * Coz jsou primi potomci nodeLayer.
     */
    ns.GraphWidget.prototype.recacheAllHighestLevelNodes = function () {
        var i, node;
        for (i = 0; i < this.nodeLayer.children.length; i++) {
            node = this.nodeLayer.children[i];
            node.clearCache();
            node.cache();
        }
    };

    /**
     * Pocita globalni boundningbox (ohraniceni) vsech objektu na scene.
     * @returns {Object} spocitany boundingbox
     */
    ns.GraphWidget.prototype.globalBoundingBox = function() {
        var i, e, n, maxX=-Infinity, minX=Infinity, minY=Infinity, maxY=-Infinity;
        var boundingBox = {};
        
        for(i=0;i<this.edgeLayer.children.length; i++) {
            e = this.edgeLayer.children[i].getAbsolutePosition();
            minX = Math.min(e.x, minX);
            minY = Math.min(e.y, minY);
            maxX = Math.max(e.x + this.edgeLayer.children[i].boundingBox.width, maxX);
            maxY = Math.max(e.y + this.edgeLayer.children[i].boundingBox.height, maxY);
        }
        for(i=0;i<this.nodeLayer.children.length; i++) {
            n = this.nodeLayer.children[i].getAbsolutePosition();
            minX = Math.min(n.x, minX);
            minY = Math.min(n.y, minY);
            maxX = Math.max(n.x + this.nodeLayer.children[i].boundingBox.width, maxX);
            maxY = Math.max(n.y + this.nodeLayer.children[i].boundingBox.height, maxY);
        }
        boundingBox.x = minX;
        boundingBox.y = minY;
        boundingBox.width = maxX - minX;
        boundingBox.height = maxY - minY;
        return boundingBox;
    };

    /**************************************************************************
     *  Graficka reprezentace jednoho uzlu. 
     *  @param model semanticky model uzlu
     *  @param gfxcnf graficka konfigurace pro tento uzel
     */
    ns.NodeWidget = function (model, gfxcnf) {
        var self = this;
    	var modelid = model.semanticType + ns.NODE_ID_SEPARATOR + model.id;
    	// objekt s obrazky
    	var imgs = Manta.PresentationManager.getImages();
    	var basicNB = new Manta.PresentationManager.BasicNodeBounds();
        this.PADDING_TOP = model.atomic || model.isHighestUnsplitableLevel() ? 5 : 10;
    	this.PADDING_LEFT = 10;
    	this.ICON_SIZE = 16;
    	this.ICON_PADDING = 4;

    	this.gfxcnf = gfxcnf;
    	this.headHeight = basicNB.headHeight;    
    	this.highlighted = false;
    	this.highlightedDir = null;
    	this.selected = false;
    	this.semanticType = model.semanticType;
        this.expanded = model.expanded;
        this.imgs = imgs;
        this.positive = gfxcnf.positive;
        
        // Nastavime popisky a hodnoty atributu.
        // Popisky jsou ulozeny v JSON konfiguraci, hodnoty primo ve vlastnostech uzlu.
        // Pokud atribut nema popisek definovan, bude popiskem jeho nazev.
        this.attributes = {};
        if(model.attributes) {
            $.each(model.attributes, function(name, value){
                self.attributes[name] = {label: gfxcnf.attributes[name] ? gfxcnf.attributes[name] : name, value: value};
            });
        }
        // zastresujici Kineticti obejkt pro cely uzel
        this.visual = new Kinetic.Group({
            x: 0,
            y: 0,
            width: 200,
            height: 250,
            name: modelid
        });
        // field drzici rozmery prvku kvuli skryvani pro viewport
        this.visual.boundingBox = new Manta.Geometry.Rectangle(0,0,100,100);
        // obdelnik na pozadi
        this.background = new Kinetic.Rect({
            x: 0,
            y: 0,
            cornerRadius: gfxcnf.cornerRadius,
            opacity: gfxcnf.opacity,
            name: modelid
        });
        if (gfxcnf.bgimage === undefined) {
        	this.background.fillPriority('color');
        	this.background.fill(gfxcnf['bgcolor']);
        } else {
            this.background.fillPatternImage(gfxcnf.bgimage);
        	this.background.fillPriority('pattern');
        }
        // ramecek uzlu
        this.border = new Kinetic.Rect({
            x: 0,
            y: 0,
            stroke: gfxcnf.borderColor,
            strokeWidth: gfxcnf.borderWidth,
            opacity: gfxcnf.opacity,
            visible: true,
            cornerRadius: gfxcnf.cornerRadius,
            name: modelid
        });
        // strokeWidth = 0 nestaci, nutno dat stroke === null
        if (gfxcnf.borderWidth === 0) this.border.setStroke(null);
        // rozbalovaci tlacitko
        this.btnExpand = new Kinetic.Image({
            x: this.PADDING_LEFT,
            y: this.PADDING_TOP,
            image: self.getExpandCollapseIcon(self.positive, self.expanded),
            opacity: gfxcnf.opacity,
            width: this.ICON_SIZE,
            height: this.ICON_SIZE,
            name: modelid
        });
        // hover pro expand tlacitko
        this.btnExpand.on('mouseover', function () {            
            self.btnExpand.image(self.getExpandCollapseIconOnHover(self.positive, self.expanded));
            self._recacheRoot();
        });
        this.btnExpand.on('mouseout', function () {
            self.btnExpand.image(self.getExpandCollapseIcon(self.positive, self.expanded));
            self._recacheRoot();
        });        
        // ikonka uzlu
        this.icon = new Kinetic.Image({
            x: this.PADDING_LEFT + this.ICON_SIZE + this.ICON_PADDING,
            y: this.PADDING_TOP,
            image: gfxcnf.positive ? gfxcnf.iconPositive : gfxcnf.iconNegative,
            opacity: gfxcnf.opacity,
            width: this.ICON_SIZE,
            height: this.ICON_SIZE,
            name: modelid
        });
        // textovy popisek uzlu 
        this.label = new Kinetic.Text({
            text: '#node-title#',
            fontFamily: 'Arial',
            fontSize: 13,
            fontStyle: 'bold',
            padding: this.PADDING_TOP,
            fill: gfxcnf.color,
            opacity: gfxcnf.opacity,
            x: 46   ,
            y: 0,
            name: modelid
        });
        // follow tlacitka
        this.btnFollow = new Kinetic.Image({
            x: 160,
            y: this.PADDING_TOP,
            image: self.positive ? self.imgs['btnFollowPositive'] : self.imgs['btnFollowNegative'],
            opacity: gfxcnf.opacity,
            width: 16,
            height: 16,
            visible: false
        });
        // hover pro follow tlacitko
        this.btnFollow.on('mouseover', function () {            
            self.btnFollow.image(self.positive ? self.imgs['btnFollowPositiveOn'] : self.imgs['btnFollowNegativeOn']);
            self._recacheRoot();
        });
        this.btnFollow.on('mouseout', function () {
            self.btnFollow.image(self.positive ? self.imgs['btnFollowPositive'] : self.imgs['btnFollowNegative']);
            self._recacheRoot();
        });  
        
        // hover pro cely uzel
        this.visual.on('mouseover', function () {
        	$(stage.container()).css('cursor', 'pointer');
        });
        this.visual.on('mouseout', function () {
        	$(stage.container()).css('cursor', 'auto');
        });

        // vse naskladame do grupy
        this.visual.add(this.background);
        this.visual.add(this.border);
        this.visual.add(this.btnExpand);
        this.visual.add(this.icon);
        this.visual.add(this.label);
        this.visual.add(this.btnFollow);
    };

    /**
     * Zlikviduje vizualni prvek reprezentovany widget a vsechny jeho vizualni prvky (ne potomky, protoze tim bych
     * znicil i uzly v hierarchii niz) a take odhlasi poslouchani vsech udalosti.
     */
    ns.NodeWidget.prototype.destroy = function () {
        this.visual.off('mouseover mouseout click');
        this.btnExpand.off('mouseover mouseout click');
        this.btnFollow.off('mouseover mouseout click');

        this.background.destroy();
        this.border.destroy();
        this.btnExpand.destroy();
        this.icon.destroy();
        this.label.destroy();
        this.btnFollow.destroy();
        this.visual.remove();
    };

    /**
     * Pro ucely kesovani musime ziskavat nejvyssiho rodice, ktery je uz primo na nodeLayer.
     * @returns uzel nejvyssi urovne, ktery je nejvyssim rodicem aktualniho uzlu
     */
    ns.NodeWidget.prototype.getCachedParent = function() {
        var tmp = this.visual;
        while (tmp.parent && tmp.parent.nodeType !== 'Layer') tmp = tmp.parent;
        return tmp;
    };
    
    /**
     * Pridavani graficke reprezentace potomka uzlu.
     * @param widget KineticJS node pro pridani
     */
    ns.NodeWidget.prototype.addChild = function (widget) {
        this.visual.add(widget.visual);
    };

    /**
     * Handler pro kliknuti na uzel
     * @param func callback funkce
     */
    ns.NodeWidget.prototype.onClick = function (func) {
        this.visual.on('click', func);
    };
    
    /**
     * Handler pro obsluhu kliku na tlacitko follow.
     * @param func callback funkce
     */
    ns.NodeWidget.prototype.onFollowClick = function (func) {
    	this.btnFollow.on('click', func);
    	this.btnFollow.visible(true);
    };
    
    /**
     * Handler pro obsluhu kliku na tlacitko expand.
     * @param func callback funkce.
     */
    ns.NodeWidget.prototype.onExpandClick = function (func) {
    	this.btnExpand.on('click', func);
    	this.btnExpand.visible(true);
    };
    
    /**
     * Vrati ikonu rozbaleni / sbaleni uzlu pro dany stav rozbalenosti uzlu (rozbalen / sbalen) a druh podbarveni (pozitivni / negativni).
     * @param positive <code>true</code>, pokud chceme ikonu pro podbarveni pozitivni barvou, nebo <code>false</code>, pokud negativni.
     * @param expanded <code>true</code>, pokud chceme ikonu pro rozbaleny uzel, nebo <code>false</code>, pokud pro sbaleny.
     * @returns Ikona rozbaleni / sbaleni uzlu pro dany stav rozbalenosti uzlu a druh podbarveni.
     */
    ns.NodeWidget.prototype.getExpandCollapseIcon = function (positive, expanded) {
        return positive
                ? (expanded ? this.imgs['btnExpandExpandedPositive'] : this.imgs['btnExpandCollapsedPositive'])
                : (expanded ? this.imgs['btnExpandExpandedNegative'] : this.imgs['btnExpandCollapsedNegative']);
    };
    
    /**
     * Vrati ikonu rozbaleni / sbaleni uzlu pro dany stav rozbalenosti uzlu (rozbalen / sbalen) a druh podbarveni (pozitivni / negativni).
     * @param positive <code>true</code>, pokud chceme ikonu pro podbarveni pozitivni barvou, nebo <code>false</code>, pokud negativni.
     * @param expanded <code>true</code>, pokud chceme ikonu pro rozbaleny uzel, nebo <code>false</code>, pokud pro sbaleny.
     * @returns Ikona rozbaleni / sbaleni uzlu pro dany stav rozbalenosti uzlu a druh podbarveni.
     */
    ns.NodeWidget.prototype.getExpandCollapseIconOnHover = function (positive, expanded) {
        return positive
                ? (expanded ? this.imgs['btnExpandExpandedPositiveOn'] : this.imgs['btnExpandCollapsedPositiveOn'])
                : (expanded ? this.imgs['btnExpandExpandedNegativeOn'] : this.imgs['btnExpandCollapsedNegativeOn']);
    };
    
    /**
     * Aktualizuje ikonu rozbaleni / sbaleni uzlu na zaklade stavu uzlu.
     */
    ns.NodeWidget.prototype.refreshExpandCollapseIcon = function () {
        this.btnExpand.image(this.getExpandCollapseIcon(this.positive, this.expanded));
    };
    
    /**
     * Nastavi ikonu expand tlacitka do pozadovaneho stavu.
     * @param state novy stav
     */
    ns.NodeWidget.prototype.setExpandButtonState = function (state) {
        this.expanded = state;
        this.refreshExpandCollapseIcon();
    };
    
    /**
     * Zobrazi/skryje expand tlacitko.
     * @param display true/false, zda se ma tlacitko zobrazovat.
     */
    ns.NodeWidget.prototype.displayExpandButton = function (display) {
    	this.btnExpand.visible(display);
    	if (display === false) {
    		// nutno posunout icon a label
    		this.icon.x(10);
    		this.label.x(26);
    	}
    };
    
    /**
     * Zobrazi/skryje follow tlacitko.
     * @param display true/false, zda se ma tlacitko zobrazovat.
     */
    ns.NodeWidget.prototype.displayFollowButton = function (display) {
    	this.btnFollow.visible(display);
    };
    
    /**
     * Funkce zjisti aktualni rozmery graficke reprezentace uzlu.
     * @returns {Object} s rozmery uzlu.
     */
    ns.NodeWidget.prototype.getBounds = function () {
    	return new Manta.Geometry.Rectangle(
    		this.visual.getAbsolutePosition().x, 
    		this.visual.getAbsolutePosition().y, 
    		this.visual.width(), 
    		this.visual.height()
    	);
    };
    
    /**
     * Nastavi rozmery uzlu.
     * @param bounds cilove rozmery, ktery ma uzel mit.
     */
    ns.NodeWidget.prototype.setBounds = function (bounds) {
    	var h = bounds.height;
    	var w = bounds.width;
        this.visual.setX(bounds.x);
        this.visual.setY(bounds.y);
        this.visual.height(h);
        this.visual.width(w);

        this.background.width(w);
        this.background.height(h);
        this.border.width(w);
        this.border.height(h);
        
        this.btnFollow.setX(w - 26);
        
        this.visual.boundingBox.x = bounds.x;
        this.visual.boundingBox.y = bounds.y;
        this.visual.boundingBox.width = w;
        this.visual.boundingBox.height = h;
    };
    
    /**
     * (Od)vybere aktualni uzel.
     * @param state true/false zda se ma vybrat (true) ci odvybrat (false)
     */
    ns.NodeWidget.prototype.setSelected = function (state) {
        if (this.selected === state) return;
        var cachedParent = this.getCachedParent();

        this.selected = state;
        this.border.cornerRadius(this.gfxcnf.cornerRadius);
        this.refreshColors();
        if(cachedParent) {
            cachedParent.clearCache();
            cachedParent.cache();
        }
    };

    /**
     * Aktualizuje priznak focusu na uzel a barevne schema uzlu.
     * @param state Novy priznak focusu na uzel
     */
    ns.NodeWidget.prototype.setFocused = function (state) {
        if (this.focused === state) {
            return;
        }
        var cachedParent = this.getCachedParent();

        this.focused = state;
        this.border.cornerRadius(this.gfxcnf.cornerRadius);
        this.refreshColors();
        if(cachedParent) {
            cachedParent.clearCache();
            cachedParent.cache();
        }
    };
    
    /**
     * Nastavi odsazeni uzlu.
     * @param model semanticky model uzlu
     */
    // TODO: grafika by si asi model mohla sama drzet komplet od sveho vytvoreni cely
    ns.NodeWidget.prototype.updatePadding = function (model) {
        var padding = model.atomic || model.isHighestUnsplitableLevel() ? 5 : 10;
        this.btnExpand.y(padding);
        this.btnFollow.y(padding);
        this.icon.y(padding);
        this.label.padding(padding);
    };

    /**
     * Nastavi, zda je uzel viditelny
     * @param visible true/false zda ma byt uzel videt.
     */
    ns.NodeWidget.prototype.setVisible = function (visible) {
        this.visual.visible(visible);
    };
    
    /**
     * Nastavi popisek, pokud popisek presahuje vymezenou velikost, prislusne ho zkrati a doplni tri tecky.
     * @param str text k zobrazeni na popisku.
     */
    ns.NodeWidget.prototype.setLabel = function (str) {
        var label,
            // defaultni prostor pro popisek - pocitam s ikonou pro uzel a paddingem po obou stranach
            availableWidth = this.visual.width() - this.ICON_SIZE - 2*this.ICON_PADDING - 2*this.PADDING_LEFT;
        if(this.visual.width() === 0) return;

        if(this.btnFollow.visible()) {
            availableWidth -= this.ICON_SIZE - this.ICON_PADDING;
        }
        if(this.btnExpand.visible()) {
            availableWidth -= this.ICON_SIZE - this.ICON_PADDING;
        }

        this.label.text(str);
        label = str;
        while(this.label.width() > availableWidth && label.length > 0) {
            // dokud je sirka vetsi nez vymezena sirka, zmensuju o jeden znak...
            label = label.substr(0, label.length - 1);
            this.label.text(label + '...');
        }
    };
    
    /**
     * Nastavi barvu pozadi.
     * @param color HTML reprezentace barvy (#00FF00, namedcolor, ...)
     */
    ns.NodeWidget.prototype.setBackground = function (color) {
        this.background.fill(color);
    };
    
    /**
     * Nastavi obrazek vyplne a prepne prioritu na vypln obrazkem.
     * @param imgObj DOM Image objekt s obrazkem.
     */
    ns.NodeWidget.prototype.setFillPatternImage = function (imgObj) {
        this.background.fillPatternImage(imgObj);
        this.background.setFillPriority('pattern');
    };
    
    /**
     * Nastavi barvu pisma popisku.
     * @param color HTML reprezentace barvy (#00FF00, namedcolor, ...)
     */
    ns.NodeWidget.prototype.setFontColor = function (color) {
        this.label.fill(color);
    };
    
    /**
     * Nastavi novou ikonku pro dany uzel.
     * @param imgObj DOM Image objekt s obrazkem.
     */
    ns.NodeWidget.prototype.changeIcon = function (imgObj) {
    	this.icon.image(imgObj);
    };
    
    /**
     * Zvyrazni uzel. 
     * O prekesovani se starame centralne az na nejvyssi urovni.
     */
    ns.NodeWidget.prototype.highlight = function (direction) {
        this.highlighted = true;
        this.highlightedDir = direction;
    	this.refreshColors();
    };
    
    /**
     * Odzvyrazni uzel.
     * O prekesovani se starame centralne az na nejvyssi urovni.
     */
    ns.NodeWidget.prototype.unhighlight = function () {
    	this.highlighted = false;
        this.highlightedDir = null;
        this.refreshColors();
    };

    /**
     * Nastavi barevne schema uzlu na zaklade jeho aktualniho stavu. 
     * Mozne stavy uzlu jsou nasledujici, serazene podle priotity od nejvyssi po nejnizsi:
     * <ol>
     *     <li>Na uzlu je focus</li>
     *     <li>Uzel je oznacen</li>
     *     <li>Uzel je zvyraznen</li>
     *     <li>Ostatni</li>
     * </ol>
     * Pokud ma uzel stavu vice, uplatni se stav s nejvyssi prioritou
     * (napr. je-li na zvyraznenem uzlu focus, vybere se schema pro uzel s focusem). 
     */ 
    ns.NodeWidget.prototype.refreshColors = function () {
        var configToUse;
        if (this.focused) {
            configToUse = this.gfxcnf['focus'];
        }
        else if (this.selected) {
            configToUse = this.gfxcnf['selection'];
        }
        else if (this.highlighted) {
            configToUse = this.gfxcnf['highlight' + this.highlightedDir];
        }
        else {
            configToUse = this.gfxcnf;
        }
        this.colorByConfig(configToUse);
    };

    /**
     * Obarvi uzel na zaklade konfigurace. Nastavi uzlu barvu pozadi a textu
     * a aktualizuje ikony podle typu podbarveni (pozitivni / negativni).
     * @param config Konfigurace pro obarveni. Musi obsahovat atributy:
     *        <ol>
     *          <li>'bgimage' nebo 'bgcolor' - Obrazek na pozadi (prioritnejsi) nebo barva pozadi uzlu</li>
     *          <li>'color' - Barva textu v uzlu</li>
     *          <li>'positive' - <code>true</code>, pokud chceme ikony uzlu pro pozitivni podbarveni,
     *                           nebo <code>false</code>, pokud pro negativni
     *          </li>
     *        </ol>
     */
    ns.NodeWidget.prototype.colorByConfig = function (config) {
        if (config.bgimage === undefined) {
            this.background.fillPriority('color');
            this.background.fill(config['bgcolor']);
        } else {
            this.background.fillPatternImage(config.bgimage);
            this.background.fillPriority('pattern');
        }
        this.setFontColor(config.color);
        this.positive = config.positive;
        this.changeIcon(this.positive ? this.gfxcnf.iconPositive : this.gfxcnf.iconNegative);
        this.refreshExpandCollapseIcon();
        this.btnFollow.image(this.positive ? this.imgs['btnFollowPositive'] : this.imgs['btnFollowNegative']);
    };

    ns.NodeWidget.prototype._recacheRoot = function () {
        var root;
        root = this.getCachedParent();
        if(root) {
            root.clearCache();
            root.cache();
            Manta.DataflowUI.redrawStage();
        }
    };

    /**************************************************************************
     *  Graficka reprezentace jedne hrany. 
     *  @param model semanticky model hrany
     *  @param gfxcnf konfigurace grafiky pro danou hranu
     */
    ns.EdgeWidget = function (model, gfxcnf) {
    	var modelid = model.semanticType + ns.NODE_ID_SEPARATOR + model.id;
    	var line;
    	var arrow;
    	var self = this;
    	
    	this.gfxcnf = gfxcnf;
    	this.semanticType = model.semanticType;
    	this.unhighlightColor = this.gfxcnf.color;
    	this.unhighlightHeadFill = this.gfxcnf['headFill'] != undefined ? this.gfxcnf['headFill'] : ns.HEAD_FILL_DEFAULT;
        this.highlightedDir = null;
        this.highlighted = false;
    	
    	// zastresujici Kineticti obejkt pro cely uzel
    	this.visual = new Kinetic.Group({
    	    x: 0, y: 0, 
    	    width: 100, height:100,
    	    name: modelid
    	});
    	this.visual.boundingBox = new Manta.Geometry.Rectangle(0,0,100,100);
    	// cara dane hrany
        this.line = new Kinetic.Line({
            points: [73, 70, 340, 23, 450, 60, 500, 20],
            stroke: gfxcnf.color,
            strokeWidth: gfxcnf.width,
            opacity: gfxcnf.opacity,
            lineJoin: 'round',
            name: modelid
        });
        if(model.type === Manta.Semantics.Edge.EdgeType.FILTER) {
            this.line.dash([5,5]);
        }
        // Nastavime popisky a hodnoty atributu.
        // Popisky jsou ulozeny v JSON konfiguraci, hodnoty primo ve vlastnostech hrany.
        // Pokud atribut nema popisek definovan, bude popiskem jeho nazev.
        this.attributes = {};
        if(model.attrs) {
            $.each(model.attrs, function(name, value){
                self.attributes[name] = {label: gfxcnf.attributes[name] ? gfxcnf.attributes[name] : name, value: String(value)};
            });
        }
        // sipka na konci hrany
        this.arrow = new Kinetic.Line({
            points: [0, 0, 7, 1, 1, 7],
            fill: gfxcnf['headFill'],
            stroke: gfxcnf.color,
            strokeWidth:  gfxcnf['headWidth'],
            opacity: gfxcnf.opacity,
            closed: true,
            rotation: 0,
            name: modelid
        });
        
        // poskladame to dohromady
        this.visual.add(this.line);
        this.visual.add(this.arrow);
        
        this.sourceAnchor = null;
        this.targetAnchor = null;
        this.bendpoints = [];

        arrow = this.arrow;
        line = this.line;
        
        // hover pro hranu
        this.visual.on('mouseenter', function () {
        	line.stroke(gfxcnf['hoverColor']);
        	arrow.stroke(gfxcnf['hoverColor']);
        	arrow.fill(gfxcnf['hoverColor']);
            self.visual.moveToTop();
        	Manta.DataflowUI.redrawStage();
            $(stage.container()).css('cursor', 'pointer');
        });
        this.visual.on('mouseout', function () {
            // korektni vybrani barvy
            var color = (self.highlighted === true) ? gfxcnf['highlightColor' + self.highlightedDir] : self.unhighlightColor;
            var headFill = (self.highlighted === true) ? gfxcnf['highlightColor' + self.highlightedDir] : self.unhighlightHeadFill;
        	line.stroke(color);
        	arrow.stroke(color);
        	arrow.fill(headFill);
            if(!self.highlighted) {
                self.visual.moveToBottom();
            }
        	// INFO: kvuli antialiasingu, prekreslujem vse
        	Manta.DataflowUI.redrawStage();
        	$(stage.container()).css('cursor', 'auto');
        });    
        
        this.setVisible(false);
    };

    /**
     * Graficke zvyrazneni hrany ve vybranem toku
     */
    ns.EdgeWidget.prototype.highlight = function (direction) {
    	this.line.stroke(this.gfxcnf['highlightColor' + direction]);
    	this.arrow.stroke(this.gfxcnf['highlightColor' + direction]);
    	this.arrow.fill(this.gfxcnf['highlightColor' + direction]);
    	this.visual.moveToTop();
        this.highlighted = true;
        this.highlightedDir = direction;
    };
    
    /**
     * Graficke odzvyrazneni
     */
    ns.EdgeWidget.prototype.unhighlight = function () {
        this.unhighlightColor = this.gfxcnf.color;
        this.line.stroke(this.gfxcnf.color);
        this.arrow.stroke(this.gfxcnf.color);
        this.arrow.fill(this.unhighlightHeadFill);
        this.highlighted = false;
        this.highlightedDir = null;
    };
    
    /**
     * Pocita uhel pro rotaci sipky.
     * @param leftToRight true/false zda jde sipka zleva doprava
     * @param x1 souradnice pocatku cary
     * @param y1 souradnice pocatku cary
     * @param x2 souradnice konce cary
     * @param y2 souradnice konce cary
     * @returns {Number} vysledny uhel pro otoceni sipky
     */
    ns.EdgeWidget.prototype.angle = function (leftToRight, x1, y1, x2, y2) {
        var r = Math.atan((y2 - y1) / (x2 - x1));

        var angle = r * 180 / Math.PI;
        if(leftToRight) {
            angle += 180;
        }
        return angle;
    };

    /**
     * Nastavi viditelnost vsech grafickych prvku hranu
     * @param visible false - hrana se nema vykreslovat
     */
    ns.EdgeWidget.prototype.setVisible = function (visible) {
        if (visible) {
            this.visual.show();
        } else {
            this.visual.hide();
        }
    };

    /**
     * Handler pro kliknuti na hranu.
     * @param func callback funkce
     */
    ns.EdgeWidget.prototype.onClick = function (func) {
    	this.visual.on('click', func);
    };
    
    /**
     * Nastavi barvu hrany.
     * @param color HTML reprezentace barvy (#00FF00, namedcolor, ...)
     */
    ns.EdgeWidget.prototype.setColor = function (color) {
        this.unhighlightColor = color;
    	this.line.stroke(color);
    	this.arrow.stroke(color);
    	this.arrow.fill(color);
    };

    /**
     * Resi aktualizaci hrany, pote co z layoutu prijdou jeji definicni body.
     */
    ns.EdgeWidget.prototype.update = function () {
        var backEdge = this.sourceAnchor.x > this.targetAnchor.x;
        var loopEdge = this.sourceAnchor.x === this.targetAnchor.x;
        var bendpointsLength = this.bendpoints.length;
        var sp, tp, i, points, angle;

        if (backEdge) {
            // hrana vede zleva doprava
            sp = new Manta.Geometry.Point(this.sourceAnchor.x, this.sourceAnchor.center().y);
            tp = new Manta.Geometry.Point(this.targetAnchor.x + this.targetAnchor.width, this.targetAnchor.center().y);
            this.bendpoints.reverse();
        } else if (loopEdge) {
            sp = new Manta.Geometry.Point(this.sourceAnchor.x + this.sourceAnchor.width, this.sourceAnchor.center().y);
            tp = new Manta.Geometry.Point(this.targetAnchor.x + this.targetAnchor.width, this.targetAnchor.center().y);
        } else {
            // hrana vede zprava doleva
            sp = new Manta.Geometry.Point(this.sourceAnchor.x + this.sourceAnchor.width, this.sourceAnchor.center().y);
            tp = new Manta.Geometry.Point(this.targetAnchor.x, this.targetAnchor.center().y);
        }

        if(bendpointsLength !== 0) {
            // pokud existuji nejake kontrolni body, segment k prvnimu
            // a segmentu k poslednimu z nich je vodorovny
            sp.y = this.bendpoints[0].y;
            tp.y = this.bendpoints.last().y;
        }

        points = [sp.x, sp.y];
        for (i = 0; i < bendpointsLength; i++) {
            points.push(this.bendpoints[i].x);
            points.push(this.bendpoints[i].y);
        }
        points.push(tp.x);
        points.push(tp.y);
        
        this.line.points(points);
        this.arrow.x(tp.x);
        this.arrow.y(tp.y);

        // uhel pod kterym se ma vykreslit sipka
        angle = this.angle(backEdge || loopEdge,
            points[points.length - 4],
            points[points.length - 3],
            points[points.length - 2],
            points[points.length - 1]);

        this.arrow.rotation(angle + 135);
        
        // aktualizujeme rozmer kvuli viewportu, ulozi se to do this.visual
        // this.calculateBoundingBox();
        
        /**
         * Workaround pro odchytavani udalosti mysi nad tenkou carou. Umele rozsirime
         * aktivni zonu o range bodu nahoru i dolu.
         * @param context
         */
        // TODO: pro velmi a zcela svisle hrany je to "nerozsiri", chtelo by to podle sklonu
        // resit rozsireni bud po Y pro lezate, nebo po X pro svisle hrany
        this.line.hitFunc(function(context) {
        	var i;
        	var range = 5;
			context.beginPath();
			context.moveTo(points[0], points[1] + range);
			context.lineTo(points[0], points[1] - range);
			for (i = 2; i < points.length; i+=2) {
				context.lineTo(points[i], points[i+1] - range);	
			}
			context.lineTo(points[i], points[i+1] + range);
			for (; i > 0; i-=2) {
				context.lineTo(points[i], points[i+1] + range);	
			}
	        context.closePath();
	        context.fillStrokeShape(this);
		});
        
        // NEMAZAT: kvuli vizualni kontrole shape pro detekci kolizi/over/in/out
        /*
        this.line.sceneFunc(function(context) {
        	var i;
			context.beginPath();
			context.moveTo(points[0], points[1]+5);
			context.lineTo(points[0], points[1]-5);
			for (i = 2; i < points.length; i+=2) {
				context.lineTo(points[i], points[i+1]-5);	
			}
			context.lineTo(points[i], points[i+1]+5);
			for (; i > 0; i-=2) {
				context.lineTo(points[i], points[i+1]+5);	
			}
	        context.closePath();
	        context.fillStrokeShape(this);
		});
		*/

    };
    
    
}(Manta.Graphics = Manta.Graphics || {}, jQuery));