/**************************************************************************
* @namespace    Manta.Operations
* @author       Martin Podloucky
* 
* Operace follow. 
* 
**************************************************************************/

(function (ns) {
    
    ns.Follow = function (context) {
        ns.BaseOperation.call(this, ns.Follow.NAME, context);
    };

    ns.Follow.prototype = Object.create(ns.BaseOperation.prototype);
    ns.Follow.NAME = 'follow';

    /**
     * Provede operaci follow. Odesle dotaz na server. Na odpoved pak reaguje funkce
     * receiveResponse.
     *
     * @param componentIDs      ID komponenty, na ktere se dela follow (long).
     * @param level             Cislo urovne, na ktere se ma follow provest (int).
     * @param radius            Polomer okoli (int).
     * @param activeFilters     Množina id aktivních filtrů
     * @param callback          Funkce ktera se zavola po zpracovani odpovedi ze serveru. Funkce sama musi zavolat
     *                          update a zaroven finish() na registrovane operaci!
     */
    ns.Follow.prototype.invoke = function (componentIDs, level, radius, activeFilters, callback) {
        var request;
        if(!componentIDs || componentIDs.length === 0) {
            return;
        }

        this.resetGraph();
    	request = {
    		componentIds        : componentIDs,
    		radius              : radius,
    		level               : level,
    		activeFilters       : activeFilters,
            sessionIndex        : this.context.sessionIndex,
            serverState         : this.context.undoManager.getServerState()
    	};
    	this.context.connection.sendRequest(request, this.name, null, callback);
    };

    /**
     * Reaguje na odpoved ze serveru. Provede zmergovani okoli obdrzeneho od serveru
     * s jiz zobrazenym grafem a updatuje vsechno potrebne jako layout, controllery apod.
     *
     * @param response    Odpoved ze serveru.
     * @param callback    Funkce ktera se zavola po zpracovani odpovedi
     */
    ns.Follow.prototype.receiveResponse = function (response, callback) {
        var graph = this.context.graph;

        // update server stavu pro undo
        this.updateServerState(response);

        if(response.components.length === 0 && response.flows.length === 0) {
            console.log("Follow command yielded no results.");
            return;
        } else {
            //noinspection JSUnresolvedVariable
            console.log("Follow response recieved, data info: " +
                " proxy components: " + response.dataInfo.proxyComponents +
                ", full components: " + response.dataInfo.fullComponents +
                ", edges: " + response.dataInfo.edges);
        }

        this._mergeComponentsIntoGraph(response.components);
        this._mergeFlowsIntoGraph(response.flows);
        this._updateCollapseSettings(response.components);
        
        graph.validateStructure();
        graph.updateAggregatedEdges();
        // sbalime predtim sbalene, pokud takove uzly jsou
        this.collapseNodes();

        if(callback) {
            // deleguju zavolani vykresleni a finish na callback!
            callback();
        } else {
            this.update(true);
            this.finish();
        }
        
        Manta.Tools.reHighlightFlow();
	};

    /**
     * Zmerguje nove uzly s jiz existujicimi. Vezme prisle uzly a na zaklade jejich id 
     * porovna se stavajicim grafem. Pokud uzel s danym id v grafu jeste neni tak ho 
     * vytvori a zaradi ho spravne do hierarchicke struktury
     * 
     * @param components	Seznam uzlu doslych ze serveru.
     */
    ns.Follow.prototype._mergeComponentsIntoGraph = function (components) {
		var i,
            node,
            graph = this.context.graph,
            component,
            componentsLength = components.length;

        // zapojeni vsech uzlu
		for (i = 0; i < componentsLength; i++) {
            component = components[i];
            if(graph.containsNode(component.id)) {
                // uzel uz existuje
                node = graph.getNode(component.id);
                if (component.proxy) {
                    node.fillFromProxyComponent(component);
                } else {
                    if(node.proxy) {
                        node.setForceVisible(true);
                    }
                    node.setForceCollapse(!component.expanded);
                    node.fillFromComponent(component);
                }
                if(!component.filtered) {
                    this.context.graph.unfilterNode(node);
                }
            } else {
                // uzel v grafu jeste neni, musim vytvorit novy
                node = Manta.Semantics.Node.createFromComponent(components[i]);
                graph.addNode(node, components[i].parentID);
                node.setForceVisible(!components[i].proxy);
                if(!components[i].proxy) {
                    node.setForceCollapse(!component.expanded);
                }
            }
		}
	};

	/**
     * Zmerguje nove hrany s jiz existujicim grafem. Vezme prisle hrany a pokud hrana jeste v 
     * grafu neni, napoji ji na spravne uzly. 
     * 
     * @param newFlows Seznam hran doslych ze serveru.
     */	
    ns.Follow.prototype._mergeFlowsIntoGraph = function(newFlows) {
		var i,
            graph = this.context.graph,
            flow,
            edge,
            subEdges;

		for (i = 0; i < newFlows.length; i++) {
            subEdges = [];
            // vzdy vytvarim novou hranu o duplicitu se stara az addEdge
			flow = newFlows[i];
            edge = new Manta.Semantics.Edge();
            edge.type = flow.label;
            edge.attrs = flow.attrs;
            edge.revState = flow.revState;
            graph.addEdgeBetween(edge, flow.source, flow.target);
		}
    };
    
    /**
     * Aktualizuje nastaveni hromadneho rozbalovani.
     * @param components Seznam uzlu doslych ze serveru.
     */
    ns.Follow.prototype._updateCollapseSettings = function (components) {
        // převést components na nodes
        var nodes = []
        var graph = this.context.graph;
        for (var i = 0; i < components.length; i++) {
            var component = components[i];
            var node = graph.nodeMap[component.id];
            nodes.push(node);
        }
        
        // poznačit si uzly s rozbalenými potomky
        var notMostExpanded = [];
        for (var i = 0; i < nodes.length; i++) {
            var node = nodes[i];
            if (!node.proxy && node.expanded && node.parent != null) {
                if (notMostExpanded.indexOf(node.parent.id) === -1) {
                    notMostExpanded.push(node.parent.id);
                }
            }
        }
        
        // zkontrolovat správnost nastavení
        var resState = this.context.collapseManager.getSettings();
        for (var i = 0; i < nodes.length; i++) {
            var node = nodes[i];
            if (!node.proxy && node.expanded && node.status != "UNVISITED" && notMostExpanded.indexOf(node.id) === -1) {
                var toBeLevel = resState[node.owner.tech];
                if (toBeLevel != node.level) {
                    var resourceId = node.topParentTech;                       
                    $("#collapse_" + resourceId + "_custom").prop("checked", true);
                }
            }
        }
    };

}(Manta.Operations = Manta.Operations || {}));
