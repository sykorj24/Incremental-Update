/*******************************************************************************
 * @namespace Manta.Operations
 * @author Martin Podloucky, Matyas Krutsky
 * 
 * Operace expand/collapse.
 * 
 ******************************************************************************/

(function(ns, undefined) {

    ns.ExpandCollapse = function(context) {
        ns.BaseOperation.call(this, ns.ExpandCollapse.NAME, context);
    };

    ns.ExpandCollapse.prototype = Object.create(ns.BaseOperation.prototype);
    ns.ExpandCollapse.NAME = 'expandCollapse';

    /**
     * Zavola operaci na dany uzel. Podle stavu uzlu udela bud expand nebo
     * collapse.
     * 
     * @param node
     *            dany uzel
     */
    ns.ExpandCollapse.prototype.invoke = function(node) {
        if (node.expanded) {
            this.invokeCollapse([node]);
        } else {
            this.invokeExpand([node]);
        }
    };

    ns.ExpandCollapse.prototype.invokeCollapse = function(nodeList) {
        this.setNodeToCenterOnId(undefined);
        for (var nodeCursor = 0; nodeCursor < nodeList.length; nodeCursor++) {
            var node = nodeList[nodeCursor];     
            // uzel je rozbaleny, tudiz bude zabalen
            node.collapse();
        }
        if (nodeList.length > 1) {
            this.resetGraph();
            this.context.graph.updateAggregatedEdges();
            this.collapseNodes();
            this.update(true);
        } else {
            this.update(false);
        }
        this.finish();

        Manta.Tools.reHighlightFlow();
    };

    ns.ExpandCollapse.prototype.invokeExpand = function(nodeList) {
        var proxies = [];
        this.setNodeToCenterOnId(undefined);
        for (var nodeCursor = 0; nodeCursor < nodeList.length; nodeCursor++) {
            var node = nodeList[nodeCursor];
            proxies = proxies.concat(this._getAllProxyChildrenIds(node));
            node.expand();
        }
        
        if (proxies.length > 0) {
            // odesilam follow request na proxy potomky
            this._sendRequest(proxies);
        } else {
            // zadne proxy uzly - zpracuju offline
            this._processOffline();
        }
    };

    /**
     * 
     * @param response
     *            Odpoved ze serveru.
     * @param callback
     *            Funkce, ktera se zaovla po zpracovani odpovedi
     */
    ns.ExpandCollapse.prototype.receiveResponse = function(response, callback) {
        var i, component, node;

        // update server stavu pro undo
        this.updateServerState(response);

        for (i = 0; i < response.components.length; i++) {
            component = response.components[i];
            node = this.context.graph.getNode(component.id);
            if (node === undefined) {
                throw new Manta.Exceptions.IllegalStateException(
                        "Components Command: Loaded component proxy not yet in the graph!");
            }
            if (node.proxy) {
                // pokud uzel byl pred expandem v grafu jako proxy, musim ho
                // sbalit
                // (vsichni potomci jsou proxy) a zaroven ho urcite chci ukazat
                //node.setForceCollapse(true);
                //node.setForceVisible(true);
            } else {
                // nedelam nic, hodnoty se uz berou z drivejsiho stavu uzlu
            }
            node.fillFromComponent(component);
        }
        
        this.resetGraph();
        this.context.graph.updateAggregatedEdges();
        this.collapseNodes();
                
        if (callback) {
            // vykresleni a zavolani finish deleguju na callback
            callback();
        } else {
            this.update(true);
            this.finish();
        }       
        Manta.Tools.reHighlightFlow();
    };

    /**
     * Dokonci pozadavek na rozbaleni uzlu bez nutnosti nacitat data ze serveru
     * 
     * @private
     */
    ns.ExpandCollapse.prototype._processOffline = function() {
        // Je nutne provest reset i v pripade offline zpracovani, jelikoz muzu
        // na urovni MIDDLE mit jeste
        // MIDDLE potomky, kteri uz jsou full v grafu, vysledkem follow na
        // urovni MIDDLE, ale jeste nebyly
        // zobrazeny a rozpadnou se do vice vrstev...
        this.resetGraph();
        this.context.graph.updateAggregatedEdges();
        this.collapseNodes();
        
        this.update(true);
        this.finish();
        
        Manta.Tools.reHighlightFlow();
    };

    /**
     * Odesle dotaz na proxy uzly na server
     * 
     * @param componentIds
     * @private
     */
    ns.ExpandCollapse.prototype._sendRequest = function(componentIds) {
        var request = {
            componentIDs : componentIds,
            sessionIndex : this.context.sessionIndex,
            serverState : this.context.undoManager.getServerState()
        };
        this.context.connection.sendRequest(request, "components", ns.ExpandCollapse.NAME, undefined);
    };

    /**
     * Vrati vsechny potomky uzlu, ktere jsou proxy uzly
     * 
     * @param node
     * @returns {Array}
     * @private
     */
    ns.ExpandCollapse.prototype._getAllProxyChildrenIds = function(node) {
        var i, proxies = [];

        for (i = 0; i < node.children.length; i++) {
            if (node.children[i].proxy && node.children[i].status !== Manta.Semantics.Node.NodeType.UNVISITED) {
                proxies.push(node.children[i].id);
            }
        }
        return proxies;
    };

}(Manta.Operations = Manta.Operations || {}));
