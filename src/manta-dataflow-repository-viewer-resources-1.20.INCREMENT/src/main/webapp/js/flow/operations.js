/**************************************************************************
* @namespace    Manta.Operations
* @author       Martin Podloucky
* 
* Zakladni prvky pro implementaci ruznych operaci nad grafem.  
* 
**************************************************************************/

(function (ns, $, undefined) {

	/*********************************************************************
	 * Umoznuje operacim pristup ke vsemu, k cemu potrebuji mit pristup, 
	 * tedy k semantice, spojeni se serverem, layotovanim, controllery 
	 * a vykreslovani. Slouzi take okolnimu svetu k pristupu ke vsem 
	 * opracim. 
	 */
	ns.OperationContext = function(graph, connection, layoutManager, graphController, stage,
	        sessionIndex, handleMessages, undoManager, collapseManager) {
    	this.graph                  = graph;
    	this.connection             = connection;
        this.layoutManager          = layoutManager;
        this.graphController        = graphController;
        this.stage                  = stage;
        this.sessionIndex           = sessionIndex;
        this.handleMessages         = handleMessages;
        this.undoManager            = undoManager;
        this.collapseManager        = collapseManager;
        this.operations             = {};			// Zaregistrovane operace.
    };

    /**
     * Zaregistruje operaci, kterou pak lze kdykoliv vyvolat podle jmena.
     *
     * @param operation     operace k zaregistrovani.
     * @param undoSensitive je-li operace citliva na undo, tzn. ze pri jejim zavolani se vyvola ulozeni undo stavu
     */
    ns.OperationContext.prototype.registerOperation = function (operation, undoSensitive) {
        var self = this;
    	this.operations[operation.name] = operation;
    	this.connection.registerReceiver(operation.name,
    			function (response, callback) {
                    // Zpracovani chyby - pokud v hlaskach odpovedi je nejaka, ktera ma zavaznost chyby,
                    // nepoustim operaci, ale volam jenom finish pro odstraneni kolecka, do budoucna by
                    // bylo lepsi vratit rovnou spravny status ze serveru!
                    var i, error = false;
                    for(i=0; i < response.messages.length; i++) {
                        if(response.messages[i].severity === 'ERROR') {
                            error = true;
                            break;
                        }
                    }
                    if(error) {
                        operation.finish();
                    } else {
                        operation.receiveResponse(response, callback);
                    }
    			});
    	// Zaregistrujeme posluchace zprav v odpovedi
    	this.connection.registerResponseListener(
    	        function(request, response) {
    	            if (response.messages) {
    	                // Pokud existuji zpravy, odchytneme je
    	                self.handleMessages(response.messages);
    	            }
    	        });

        if(undoSensitive) {
            jQuery.aop.before( {target: operation, method: 'finish'},
                function() {
                    if(self.undoManager) {
                        self.undoManager.pushUndoState();
                    }
                });
        }

    	// Zmena kurzoru na presypaci hodiny pred spustenim operace 
        jQuery.aop.before( {target: operation, method: 'invoke'},
                function() {
                    Manta.Tools.unHighlightAll(operation.context.graphController, false);   // false = resetovat do zakladni barvy
                    $("#loading-overlay").fadeIn(1);
                });
        // Zmena kurzoru na defaultni, skonci-li spusteni operace vyjimkou.
        // Skonci-li spusteni operace OK, kurzor menit nesmime, protoze operace muze jeste probihat
        jQuery.aop.afterThrow( {target: operation, method: 'invoke'},
                function(exception) {
                    $("#loading-overlay").fadeOut(1);
                    console.log("Uncaught exception: " + exception);
                    throw exception;
                });
        // Zmena kurzoru na defaultni po skonceni operace (lhostejno zda OK ci s chybou)
        // (matyas: upravil jsem afterFinally na after a afterThrow, jelikoz afterFinally mi
        // nejak pozira vyjimky)
        jQuery.aop.afterFinally( {target: operation, method: 'finish'},
                function() {
                    $("#loading-overlay").fadeOut(1);
                });
    };
    
    /**
     * Vrati drive zaregistrovanou operaci podle jmena. 
     */
    ns.OperationContext.prototype.getOperation = function (name) {
    	return this.operations[name];
    };
    
    //--------------------------------------------------------------------
    
    
	/********************************************************************* 
	 * Zakladni trida pro operace nad grafem.
	 * 
	 * @param name 		jmeno operace typu string
	 * @param context 	kontext typu OperationContext
	 */
    ns.BaseOperation = function (name, context) {
    	this.name = name;
    	this.context = context;
        this.nodeToCenterOnId = null; // ID uzlu, na ktery zamerime stred obrazovky obdrzeni odpovedi
    };

    /**
     * Nastavi uzel, na ktery se ma pri updatu centrovat
     * @param node
     */
    ns.BaseOperation.prototype.setNodeToCenterOnId = function(node) {
        this.nodeToCenterOnId = node;

    };

    /**
     * Sbali vsechny uzly, ktere by mely byt sbalene pred zapocetim layoutu
     */
    ns.BaseOperation.prototype.collapseNodes = function() {
        var i,
            allNodes = this.context.graph.getAllNodes(),
            node;

        for(i=0; i < allNodes.length; i++) {
            node = allNodes[i];
            if((node.forceCollapse || this._canCollapse(node)) && !node.isDescendantVisibilityForced()) {
            // I24010 - HOTFIX - pokud bude zlobit, vratit - forceVisible napotomcich ma prednost pred
            // forceCollapse na predkovi, drive obracene...
            //if(node.forceCollapse || this._canCollapse(node)) {
                node.collapse();
                node.setForceCollapse(false);
            }
        }

    };

    /**
     * Uchova stav sbalenosti grafu pro dalsi prelayoutovani tak, aby sbalene uzly
     * zustaly sbalene a rozbalene uzly zustaly rozbalene. Stavy se uchovavaji kvuli
     * moznosti rozdelovat uzly ve stavu forceVisible (znaci, ze uzel je viditelny a
     * proto i v dalsim layoutu nesmi byt jeho predek sbaleny) a highestCollapsedParent
     * (identifikuje nejvyse polozeneho predka, ktery byl sbaleny, diky tomu se zamezi
     * rozbalovani uzlu, ktere vznikly z rozbalenych owneru po resetu grafu napr. behem
     * remote expandu.)
     */
    ns.BaseOperation.prototype.storeCollapsedState = function () {
        var i,
            allNodes = this.context.graph.getAllNodes(),
            node, parent;

        // nastavim flagy pro stav expand/collapse v dalsim kroku
        for(i=0; i < allNodes.length; i++) {
            node = allNodes[i];
            // najdu nejvyse sbaleny uzel
            if(node.hidden) {
                parent = node.parent;
                while(parent !== null && (parent.hidden || !parent.expanded)) {
                    node.highestCollapsedParentId = parent.owner.id;
                    parent = parent.parent;
                }
                // uzel nebude v pristim kroku viditelny (pokud neprijde ze serveru)
                node.setForceVisible(false);
            } else {
                node.highestCollapsedParentId = null;
                node.setForceVisible(!node.shadowOwner && !node.proxy);
            }
        }
    };

    /**
     * Prevede semanticky graf do kanonickeho tvaru:
     * <ul>
     *     <li>Vsechny shadow uzly jsou slouceny zpet</li>
     *     <li>Vsechny uzly jsou expandovany a jejich puvodni stav je uchovan</li>
     * </ul>
     * nebo maji jiny stav
     */
    ns.BaseOperation.prototype.resetGraph = function () {
        var i,
            allNodes = this.context.graph.getAllNodes(),
            node;

        if(!this.context.graph.dirty) return;
        this.storeCollapsedState();

        // Priprava grafu pro zapojeni - vymazu vsechny shadow uzly a vsechny uzly rozbalim
        for(i=0; i < allNodes.length; i++) {
            node = allNodes[i];
            node.expand();
        }
        this.context.graph.clearAllShadowNodes();
        this.context.graph.setDirty(false);
    };

    /**
     * Zavola vsechny potrebne updaty po provedeni prikazu tzn.
     * <ul>
     *     <li>Layout</li>
     *     <li>Refresh controlleru</li>
     *     <li>Zruseni vyberu objektu a zvyrazneni</li>
     *     <li>Centrovani grafu (pokud to lze)</li>
     *     <li>Prekresleni sceny</li>
     * </ul>
     * @param divideNodes maji se rozdelovat uzly behem layoutu?
     */
    ns.BaseOperation.prototype.update = function(divideNodes) {
        var divide = true;

        if(divideNodes !== undefined) {
            divide = divideNodes;
        }

        this.context.layoutManager.layout(divide);
        if(divide) {
            this._postProcessCollapsibleShadows();
        }

        this.context.graphController.refresh();
        this.context.graphController.clearSelection();
        if(this.nodeToCenterOnId) {
            this.context.graphController.centerToNode(this.nodeToCenterOnId);
        }
        Manta.DataflowUI.redrawStage();
    };

    /**
     * Slouzi predevsim jako hook pro aop. Defaultni implementace nedela nic,
     * v potomcich je mozne pretizit.
     */
    ns.BaseOperation.prototype.finish = function () {
        // nedelam nic
    };

    /**
     * Nastavi z odpovedi aktualni id stavu serveru pro synchronizaci flowState.
     */
    ns.BaseOperation.prototype.updateServerState = function (response) {
        if (response.serverState === undefined) {
            throw new Manta.Exceptions.IllegalArgumentException("Unable to obtain server state from response!");
        }
        this.context.undoManager.setServerState(response.serverState);
    };


    /**
     * Projde cely graf a collapsuje vsechny uzly, jejichz viditelnost neni vynucena,
     * predevsim se to deje pro shadow uzly, ktere byly drive sbalene, a ovlivnilo je
     * rozbaleni jejich owner uzlu. Diky tomuto postprocessingu, budou po layoutu opet
     * sbalene.
     * @private
     */
    ns.BaseOperation.prototype._postProcessCollapsibleShadows = function () {
        var i,
            allNodes,
            node,
            parent,
            relayout = false;

        allNodes = this.context.graph.getAllNodes();
        for(i=0; i < allNodes.length; i++) {
            node = allNodes[i];
            if(node.highestCollapsedParentId !== null) {
                parent = node;
                while(parent !== null) {
                    if(parent.owner.id === node.highestCollapsedParentId) {
                        if(this._canCollapse(parent)) {
                            parent.collapse();
                            relayout = true;
                        }
                        break;
                    }
                    parent = parent.parent;
                }
            }
        }
        if(relayout) {
            this.context.layoutManager.layout(false);
        }
    };

    /**
     * Rozhodne zda se uzel ma (muze) collapsovat. Pravidlo zni, ze uzel, ktery se muze collapsovat,
     * by se mel collapsovat.
     * @param node
     * @returns {boolean}
     * @private
     */
    ns.BaseOperation.prototype._canCollapse = function(node) {
        return  !node.proxy &&
                !node.isFiltered() &&
                !node.shadowOwner &&
                // I24010 - HOTFIX - pokud bude zlobit, vratit
                //!node.isDescendantVisibilityForced() &&
                node.expanded;
    }

}(Manta.Operations = Manta.Operations || {}, jQuery));
