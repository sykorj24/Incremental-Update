/**************************************************************************
 * @namespace    Manta.Semantics
 * @author       Martin Podloucky
 * 
 * Semanticky model grafu datovych toku.   
 * 
 **************************************************************************/
var Manta = Manta || {};
(function(ns, $, undefined) {

    var visibleFilter = function(element) { return element.isVisible(); };
    var contractFilter = function(edge) {
        return !edge.source.hidden && !edge.target.hidden;
    };
    var filterManager;

    /**
     * Filter, ktery vraci jen uzly, ktere nemaji priznak o vyfiltrovani
     * @param node
     * @returns {boolean}
     */
    var filteredFilter = function(node) {
        return !node.isFiltered();
    };

    /**
     * Vycet vsech moznych typu uzlu v grafu
     * @type {{FINISHED: string, VISITED: string, UNVISITED: string, UNKNOWN: string}}
     */
    ns.SemanticType = {
        NODE : "NODE",
        EDGE : "EDGE"
    };
	/**************************************************************************
	 * Reprezentace celeho grafu. 
	 */
	ns.Graph = function(filterManager) {
		this.nodeMap = {};
		this.children = [];
        this.orphans = {};          // uzly, ktere jsou vlozeny do grafu, ale zatim nemaji prirazeneho rodice
                                    // po dokonceni kazde tranakce vkladani uzlu, by mel byt objekt prazdny
        this.dirty = false;         // flag, ktery signalizuje "cistotu" grafu. Graf je cisty, pouze pokud je v
                                    // "kanonickem" tvaru (neobsahuje zadne shadow ani collapsed uzly). Pouziti
                                    // viz napr. BaseOperation.resetGraph (vola se jen pokud graf neni cisty)
        this.filterManager = filterManager;
        this.startNodeIds = [];     // id startovnich uzlu
        this.startHierarchyNodeIds = null; // id uzlu spadajicich do hierachie startovniho uzlu, predci i potomci
	};

    /**
     * Vymaze vsechna data z grafu. Po zavolani metody bude graf uplne prazdny stejne jako na zacatku,
     * pred vlozenim prvniho uzlu
     */
    ns.Graph.prototype.wipeOut = function() {
        this.nodeMap = {};
        this.children = [];
        this.orphans = {};
        this.dirty = true;
    };
    
    /**
     * Nastaví startovní uzly.
     * @param nodeIds množina id startovních uzlů
     */
    ns.Graph.prototype.initStartNodeIds = function(nodeIds) {
        for (var i = 0; i < nodeIds.length; i++) {
            this.startNodeIds.push(nodeIds[i]);
        }
    }
    
    /**
     * Nastaví hierarchii startovních uzlů také jako startovní.
     */
    ns.Graph.prototype.initHierarchyStartNodeIds = function() {
        this.startHierarchyNodeIds = [];
        for (var i = 0; i < this.startNodeIds.length; i++) {
            var id = this.startNodeIds[i];
            var node = this.nodeMap[id];
            if (node != null) {
                // predci
                var parent = node.owner.parent;
                while (parent != null) {
                    this.startHierarchyNodeIds.push(parent.owner.id);
                    parent = parent.owner.parent;
                }
                
                // potomci
                var descendants = [ node.owner ];
                while(descendants.length !== 0) {
                    descendant = descendants.pop();
                    this.startHierarchyNodeIds.push(descendant.owner.id);
                    descendants.append(descendant.owner.children);
                }
                
            } else {
                console.log("Start node with id " + id +" doesn't exist in node map.");
            }
        }
    }

    /**
     * Nastavi falg, ktery urcuje zda se graf nachazi v kanonickem tvaru
     * @param dirty
     */
    ns.Graph.prototype.setDirty = function(dirty) {
        this.dirty = dirty;
    };

	/**
	 * Vraci vsechny uzly v grafu. 
	 * @returns	Array Seznam vsech uzlu v grafu.
	 */
	ns.Graph.prototype.getAllNodes = function() {
		return Object.keys(this.nodeMap).map(function(id) {
			return this.nodeMap[id];
		}, this);
	};

	/**
	 * Vraci vsechny viditelne uzly v grafu.
	 * @returns	Array Seznam vsech uzlu v grafu.
	 */
	ns.Graph.prototype.getVisibleNodes = function() {
		return this.getAllNodes().filter(visibleFilter);
	};

    /**
     * Rozhodne zda uzel s danym id existuje v grafu
     * @param nodeId id uzlu
     * @returns {boolean}
     */
    ns.Graph.prototype.containsNode = function(nodeId) {
        return (nodeId in this.nodeMap);
    };

    /**
     * Vrati uzel s danym id, nebo undefined
     * @param nodeId id uzlu
     * @returns {Node}
     */
    ns.Graph.prototype.getNode = function(nodeId) {
        return this.nodeMap[nodeId];
    };

    /**
     * Vlozi uzel do grau. Metoda resi spravne zapojeni uzlu do hierarchie.
     * @param node
     * @param parentId nepovinny parametr, pokud neni zadan, uzel se bere jako korenovy.
     * Stejny efekt ma zadani -1
     */
    ns.Graph.prototype.addNode = function(node, parentId) {
        var i,
            parent,
            orphans;
        
        if(!(node.id in this.nodeMap)) {
            // uzel jeste neni v grafu, vlozim ho:
            this.nodeMap[node.id] = node;
            node.graph = this;

            // vlozim ho do hierarchie:
            if(parentId in this.nodeMap) {
                // Uzel uz ma v grafu rodice
                parent = this.nodeMap[parentId];
                parent.children.push(node);
                node.setParent(parent);
            } else if(parentId === undefined || parentId === null || parentId === -1) {
                // Uzel nema rodice, je na nejvyssi urovni
                this.children.push(node);
            } else {
                // Uzel v grafu jeste nema sveho rodice, pridam ho do sirotku
                if(!(parentId in this.orphans)) {
                    this.orphans[parentId] = [];
                }
                this.orphans[parentId].push(node);
            }

            // kontrola sirotku
            if(node.id in this.orphans) {
                // uzel ma v sirotcinci potomky a nyni je cas se jich ujmout
                orphans = this.orphans[node.id];
                for (i = 0; i < orphans.length; i++) {
                    node.children.push(orphans[i]);
                    orphans[i].setParent(node);
                }
                delete this.orphans[node.id];
            }
        }
    };

    /**
     * Zvaliduje, ze v grafu nejsou zadne uzly, ktere nemaji predka, i kdyz by ho meli mit.
     * Pokud takove uzly existuji, vyhodi vyjimku.
     */
    ns.Graph.prototype.validateStructure = function() {
        if(Object.keys(this.orphans).length > 0) {
            throw new Manta.Exceptions.IllegalStateException("Graph hierarchy is invalid!");
        }
    };

    /**
     * Rozhodne zda stejna hrana uz v grafu neni
     * @param edge hrana, kterou hledam v grafu pomoci equals metody
     * @returns {Edge}
     */
    ns.Graph.prototype.findEdge = function(edge) {
        var i;
        for(i=0; i < edge.source.edgesOut.length; i++) {
            if(edge.equals(edge.source.edgesOut[i])) {
                return edge.source.edgesOut[i];
            }
        }
        return null;
    };

    /**
     * Prida novou hranu a vlozi ji mezi uzly se zadanymi id. Pokud uzly s danymi id
     * neexistuji, vyhodi vyjimku.
     * @param edge
     * @param sourceId
     * @param targetId
     * @returns {Edge} Hrana, ktera ve skutecnosti je v grafu - bud pridavana hrana, nebo jina
     * instance teze hrany, ktera uz byla do grafu pridana driv.
     */
    ns.Graph.prototype.addEdgeBetween = function(edge, sourceId, targetId) {
        var sourceNode = this.nodeMap[sourceId],
            targetNode = this.nodeMap[targetId];

        if(sourceNode === undefined || targetNode === undefined) {
            throw new Manta.Exceptions.IllegalArgumentException("Cannot add edge - end nodes are not in the graph yet!");
        }
        edge.source = sourceNode;
        edge.target = targetNode;
        return this.addEdge(edge);
    };

    /**
     * Odpoji hranu z grafu
     * @param edge
     */
    ns.Graph.prototype.removeEdge = function(edge) {
        var i;
        if(edge.hasChildren()) {
            for(i=0; i < edge.children.length; i++) {
                edge.children[i].setParent(null);
            }
        }
        edge.disconnectFromSource();
        edge.disconnectFromTarget();
    };

    /**
     * Prida hranu do grafu a spravne ji zapoji do svych koncu.
     *
     * Metoda neresi stav hierarchie uzlu (visible, expanded, collapsed) do ktere je zapojovana.
     * @param edge
     */
    ns.Graph.prototype.addEdge = function(edge) {
        var existing = this.findEdge(edge);
        if(existing === null) {
            edge.source.addEdgeOut(edge);
            edge.target.addEdgeIn(edge);
        }
    };

    /**
     * Smaze vsechny agregovane hrany v grafu a znovu je vytvori podle aktualniho stavu.
     * Agregovana hrana se vytvari pokud je nalezena libovolna cesta mezi dvema
     * nevyfiltrovanymi, navstivenymi uzly o delce vetsi nez 1.
     */
    ns.Graph.prototype.updateAggregatedEdges = function() {
        var i, j,
            allNodes=this.getAllNodes(),
            edges,
            node;

        for(i=0; i < allNodes.length; i++) {
            node = allNodes[i];
            if(!node.isFilteredOrUnvisited() && !node.hasChildren()) {
                // nejdriv odeberu vsechny agregovane hrany, ktere vedou z uzlu
                edges = node.edgesOut.slice();
                for(j=0; j < edges.length; j++) {
                    if(edges[j].aggregated) {
                        this.removeEdge(edges[j]);
                    }
                }
                // a potom je znovu vytvorim
                this._createAggregatedEdgesFrom(node);
            }
        }
    };

    /**
     * Provede DFS pruchod ze startovniho uzlu a vytvori agregovanou hranu pokazde kdyz
     * v pruchodu dojdu k jinemu nevyfiltrovanemu, navstivenemu uzlu. Do hrany se nasledne
     * ulozi vsechny uzly, pres ktere jsem dosel k cilovemu uzlu. Pripadne cykly na ceste
     * jsou ignorovany (neukladaji se na agregovanou hranu)
     * @param start
     * @private
     */
    ns.Graph.prototype._createAggregatedEdgesFrom = function(start) {
        var i,
            path = {},
            // ke kazdemu uzlu ukladam aktualni cestu jak jsem se k nemu dostal
            actualPath,
            open = [],
            visited = [],
            // objekt slouzi k ulozeni agregovanych hran ve formatu
            // aggregatedEdges[id] = [seznam uzlu na ceste]
            aggregatedEdges = {},
            // objekt slouzi k evidenci typu vytvarene agregovane hrany (FILTER/DIRECT),
            // pokud nevede mezi uzly cesta slozene pouze z DIRECT hran, agg. hrana bude FILTER
            direct = {},
            node, edge,
            id;

        open.push(start);
        path[start.id] = [];
        // cestu ze startovniho uzlu zacnu jako DIRECT
        direct[start.id] = true;
        
        var revStates = {};

        while(open.length > 0) {
            node = open.pop();
            actualPath = path[node.id].slice();
            if(node !== start) {
                // startovni uzel nebudu pridavat do cesty...
                actualPath.push(node);
            }
            visited.pushOnce(node);

            for(i=0; i < node.edgesOut.length; i++) {
                edge = node.edgesOut[i];
                if(!edge.isSelfLoop()) {
                    if(!edge.target.isFilteredOrUnvisited()) {
                        // dosel jsem nekam kam chci napojit agregovanou hranu
                        if(!(edge.target.id in aggregatedEdges)) {
                            aggregatedEdges[edge.target.id] = [];
                        }
                        aggregatedEdges[edge.target.id].appendUniqueElements(actualPath);
                    } else if(!visited.contains(edge.target) || !direct[edge.target.id] && direct[node.id]) {
                        // v uzlu na konci hrany jsem jeste nebyl, nebo jsem v nem byl po INDIRECT hrane a
                        // ted prichazim po DIRECT hrane
                        path[edge.target.id] = actualPath;
                        open.push(edge.target);
                    } else {
                        // kdyz dojdu do uz navstiveneho uzlu, ktery byl navstiven direct cestou, nebo indirect
                        // cestou a ja jsem na indirect ceste, nedelam nic
                    }
                    // cesta do ciloveho uzlu je prima pokud predchozi cesta je prima a hrana je prima, nebo
                    // pokud do nej uz vede jina prima cesta
                    if(!direct[edge.target.id]) {
                        // undefined je falsy exp. proto je ted hodnota pro konec hrany neurcena, nebo je false...
                        direct[edge.target.id] = (direct[node.id] && edge.type === ns.Edge.EdgeType.DIRECT);
                    }
                    revStates[edge.target.id] = ns._mergeRevState(revStates[edge.target.id], ns._mergeRevState(revStates[node.id], edge.revState));
                }
            }
        }
        for(id in aggregatedEdges) {
            if(aggregatedEdges.hasOwnProperty(id) && aggregatedEdges[id].length > 0) {
                node = this.nodeMap[id];
                edge = new Manta.Semantics.Edge();
                edge.aggregated = true;
                edge.nodesOnPath = aggregatedEdges[id];
                edge.source = start;
                edge.target = node;
                edge.type = direct[edge.target.id] ? ns.Edge.EdgeType.DIRECT : ns.Edge.EdgeType.FILTER;
                edge.revState = revStates[edge.target.id];
                this.addEdge(edge);
                this.setDirty(true);
            }
        }
    };


    /**
     * Vraci true, pokud graf neobsahuje zadne uzly
     * @returns {boolean}
     */
    ns.Graph.prototype.isEmpty = function() {
        return Object.keys(this.nodeMap).length === 0;
    };

    /**
     * Vycisti z grafu vsechny shadow uzly a vrati ho do puvodniho semantickeho stavu
     */
    ns.Graph.prototype.clearAllShadowNodes = function() {
        var i,
            nodes = this.getAllNodes();

        for(i=0; i < nodes.length; i++) {
            nodes[i].clearShadowNodes();
        }
    };

    /**
     * Povysi uzel, ktery je skryty v agregovane hrane tak aby byl v grafu plnohodnotne zobrazen a zaroven
     * rozbije vsechny agregovane hrany, ktere tento uzel obsahuji jako proxy. Tato metoda provadi
     * inverni operaci k metode filterNode
     * @param node uzel, ktery se ma zapojit
     */
    ns.Graph.prototype.unfilterNode = function(node)  {
        // NOOP provede se samo pri spravnem nastaveni filter ze serveru a zmenou resource filtru
    };

    /**
     * Odfiltruje uzel z grafu a zavola prepojeni vsech hran. Napr. pokud vedla hrana:
     * <pre>
     * A - B - C
     * </pre>
     * a na uzlu B je zavolana tato operace, uzel je odfiltrovan a vytvori se agregovana hrana
     * <pre>
     * A - C
     * </pre>
     * s proxy uzlem B. Operace je inverzni k operaci unfilterNode.
     *
     * @param node
     */
    ns.Graph.prototype.filterNode = function(node)  {
        // NOOP provede se samo pri spravnem nastaveni filter ze serveru a zmenou resource filtru
    };
    
    /**
     * Manuálně zkontrahuje uzel. Podobné jako filter, ale nastavení přetrvává bez ohledu na změnu filtru.
     * @param node uzel, který se má upravit 
     */
    ns.Graph.prototype.manuallyContractNode = function(node)  {
        node.setManualVisibility(ns.Node.ManualVisibility.CONTRACTED);
        
        for (var i = 0; i < node.children.length; i++) {
            this.manuallyContractNode(node.children[i]);
        }
    };
    
    /**
     * Manuálně ofiltruje uzel. Nastavení přetrvává bez ohledu na změnu filtru
     * @param node uzel, který se má upravit
     */
    ns.Graph.prototype.manuallyUnfilterNode = function(node)  {
        this.unfilterNode(node);
        node.setManualVisibility(ns.Node.ManualVisibility.UNFILTERED);
        
        for (var i = 0; i < node.children.length; i++) {
            this.manuallyUnfilterNode(node.children[i]);
        }
    };
    
    /**
     * Odstraní manuální nastavení viditelnosti uzlu.
     * @param node uzel, který se má upravit
     */
    ns.Graph.prototype.cleanNodeManualVisibility = function(node) {
        node.setManualVisibility(ns.Node.ManualVisibility.UNCHANGED);
    }

    /**
     * Vyhleda uzly podle popisku. Vrati uzly, jejichz popisek obsahuje dany podretezec.
     * Porovnavani NENI citlive na velikost pismen.
     * 
     * @param substring Podretezec, podle ktereho se bude vyhledavat.
     * @returns Nalezene uzly podle popisku.
     */
    ns.Graph.prototype.findNodesByLabel = function(substring) {
        // Nezalezi na velikosti pismen
        var phrase = substring ? substring.toLowerCase() : '';
        var result = $.grep(this.getAllNodes(), function(item) {
            return item.isVisible() && (item.label.toLowerCase().indexOf(phrase) >= 0);
        });
        // Nalezene uzly seradime podle pravidel:
        // 1) Uzel, jehoz nazev na vyhledavaci podretezec zacina, bude zarazen vyse,
        //    nez ten, ktery jej ma uprostred nazvu.
        // 2) Pokud nelze poradi uzlu urcit pravidlem 1), seradi se podle abecedy. 
        result.sort(function(a, b) {
            var aLabel = a.label.toLowerCase();
            var bLabel = b.label.toLowerCase();
            // Porovname abecedne nazvy uzlu
            var result = aLabel > bLabel ? 1 : -1;
            if (aLabel.indexOf(substring) === 0) {
                // Uzel 'a' zacina na vyhledavaci podretezec
                // => na vysledek to ma vliv vetsi nez abecendni poradi,
                // ale stejny, jako kdyz na vyhledavaci podretezec zacina uzel 'b'
                result -= 10;
            }
            if (bLabel.indexOf(substring) === 0) {
                // Uzel 'b' zacina na vyhledavaci podretezec
                // => na vysledek to ma vliv vetsi nez abecendni poradi,
                // ale stejny, jako kdyz na vyhledavaci podretezec zacina uzel 'a'
                result += 10;
            }
            return result;
        });
        return result;
    };

    /**
     * Spoji pole hran do jedne hrany a nastavi ho jako potomky nove vytvorene metahrany.
     * Novou metahranu zapoji mezi predane uzly. Metahrana obsahuje vzdy jen skutecne hrany,
     * nikdy by nemela obsahovat jine metahrany.
     * @param edges
     * @param source
     * @param target
     * @private
     */
    ns.Graph.prototype._mergeEdges = function(edges, source, target) {
        var i,
            newEdge,
            filterEdge = true,
            children = [],
            revState = undefined;

        if(edges.length === 1 && edges[0].source === source && edges[0].target === target) {
            // hrana je jen jedna a zapojuje se mezi sve skutecne konce - neni potreba
            // budovat hierarchii
            edges[0].parent = null;
            return;
        }

        newEdge = new Manta.Semantics.Edge();
        newEdge.setMeta(true);

        for(i=0; i < edges.length; i++) {
            if(edges[i].hasChildren()) {
                // hrana je metahrana, preberu jeji potomky
                children.append(edges[i].children);
            } else {
                // hrana je skutecna hrana, pridam ji mezi potomky
                children.push(edges[i]);
            }
            filterEdge = filterEdge && edges[i].type === Manta.Semantics.Edge.EdgeType.FILTER;
            revState = ns._mergeRevState(revState, edges[i].revState);
            newEdge.aggregated = newEdge.aggregated || edges[i].aggregated;
        }
        newEdge.revState = revState;
        
        // prevezmu atributy, pokud metahrana reprezentuje jen jednu hranu
        // to je za ucelem user-frienldy zobrazovani atributu.
        if (edges.length == 1) {
            newEdge.attrs = edges[0].attrs;
        } else {
            newEdge.attrs = {};
        }

        if(filterEdge) {
            // vsechny shlukovane hrany jsou typu filter, proto i metahrana musi byt filter
            newEdge.type = Manta.Semantics.Edge.EdgeType.FILTER;
        }
        
        // nastavime potomkum noveho rodice
        for (i=0; i < children.length; i++) {
            children[i].setParent(newEdge);
        }

        // zapojeni metahrany
        source.addEdgeOut(newEdge);
        target.addEdgeIn(newEdge);

        for(i=0; i < edges.length; i++) {
            if(edges[i].meta) {
                // vycistim graf od metahran, ktere nyni uz postradaji smysl
                // (jejich potomky prebrala nova metahrana)
                this.removeEdge(edges[i]);
            }
        }
    };
    
    /**
     * Merge dvou revision states.
     */
    ns._mergeRevState = function(revState1, revState2) {
        if (revState1 === undefined) {
            return revState2;
        } else if (revState2 === undefined) {
            return revState1;
        } else if (revState1 == ns.Edge.RevisionState.STABLE) {
            return revState2;
        } else if (revState2 == ns.Edge.RevisionState.STABLE) {
            return revState1;
        } else if (revState1 == ns.Edge.RevisionState.DELETED && revState2 == ns.Edge.RevisionState.DELETED) {
            return ns.Edge.RevisionState.DELETED;
        } else if (revState1 == ns.Edge.RevisionState.NEW && revState2 == ns.Edge.RevisionState.NEW) {
            return ns.Edge.RevisionState.NEW;
        } else if (
                (revState1 == ns.Edge.RevisionState.NEW && revState2 == ns.Edge.RevisionState.DELETED) ||
                (revState1 == ns.Edge.RevisionState.DELETED && revState2 == ns.Edge.RevisionState.NEW)
                ) {
            return ns.Edge.RevisionState.MIXED;
        } else if (revState1 == ns.Edge.RevisionState.MIXED || revState2 == ns.Edge.RevisionState.MIXED) {
            return ns.Edge.RevisionState.MIXED;
        } else {
            console.log("Undefined merge of rev states " + revState1 + " and " + revState2 + ".");
            return ns.Edge.RevisionState.STABLE;
        }
        
            
    }

	/**************************************************************************
	 *  Uzel grafu. 
	 */
	ns.Node = function(id, proxy) {
		this.semanticType = Manta.Semantics.SemanticType.NODE;		// semanticky typ, aby slo v presentationManageru korektne vybirat konfigurace
		this.id = id;			// Databazove ID (long)
		this.label = null;		// Popisek (string)
		this.tech = null;		// Technologie (string)
		this.techType = null;   // Typ technologie (string)
        this.topParentTech = -1;// Flag zda je uzel vyfiltrovan aktualnim nastavenim filteru
		this.type = null;		// Typ (column, databaze adpo.) (string)
		this.bounds = new Manta.Geometry.Rectangle(0, 0, 0, 0);	// Hranice uzlu v rovine
        this.parent = null;		    // Rodic (Node)
        this.children = [];		    // Synovske uzly (Node)
		this.atomic = false;	    // Je to nejnizsi uroven = atomicky uzel?
		this.status = ns.Node.NodeType.UNKNOWN; //
		this.graph = null;		    // Odkaz na graf (Graph)
		this.edgesIn = [];		    // Hrany dovnitr (Edge)
		this.edgesOut = [];		    // Hrany ven (Edge)
        this.owner = this;          // Uzel ktereho je tento uzel shadow, pokud shadow = true, nebo odkaz na sebe, pokud shadow = false
        this.shadows = [];          // Seznam vlastnich shadows (Node[])
        this.level = 2;             // Uroven uzlu v hierarchii: 0=TOP, 1=MIDDLE, 2=BOTTOM
        this.attributes = {};       // Atributy uzlu
        this.path = [];             // Absolutni cesta k uzlu - obsahuje nazvy uzlu nadrazenych v hierarchii
        this.hasSource = false;     // flag, jestli ma uzel source code
        this.nodeMapping = {};      // Mapovani uzlu v jinych vrstvach
        this.layer = undefined;     // Vrstva, do ktere uzel spada

        // Mozne stavy uzlu, ktere se nastavuji pomoci techto flagu:
        this.shadow = false;        // Je uzel shadow jineho uzlu (boolean)
        this.filtered = false;      // Flag zda je uzel vyfiltrovan aktualnim nastavenim filteru
        this.proxy = proxy || false;// Je proxy? (boolean)
        this.expanded = true;       // Je rozbalen? (boolean)
        this.hidden = false;        // Je soucasti nejakeho sbaleneho predka?
        this.shadowOwner= false;    // Je uzel vlastnikem shadow uzlu? Pokud ano je reprezentovan svymi shadow a samneni videt
        this.instance = ns.Node.baseID++; // debug flag
        this.forceVisible = false;  // Flag zda uzel musi byt viditelny, tj. zda je vynuceno, ze hierarchie nad nim bude expanded
        this.forceCollapse = false; // Flag , ktery muze vynutit, ze uzel musi byt sbaleny
        this.highestCollapsedParentId = null; // Nejvyse sbaleny uzel
        this.manualVisibility = ns.Node.ManualVisibility.UNCHANGED; // Flag, jestli uživatel provedl nějakou manuální změnu týkajííc se viditelnosti
	};

    ns.Node.baseID = 0;

	/**
     * Vycet vsech moznych typu uzlu v grafu
     * @type {{FINISHED: string, VISITED: string, UNVISITED: string, UNKNOWN: string}}
     */
    ns.Node.NodeType = {
        FINISHED : "FINISHED",
        VISITED : "VISITED",
        UNVISITED : "UNVISITED",
        UNKNOWN : "UNKNOWN"
    };
    
    /**
     * Stav manuálního nastavení viditelnosti.
     */
    ns.Node.ManualVisibility = {
        /** Uzel je ručne zkontrahován. */
        CONTRACTED : {
            name: "CONTRACTED", 
            filtered: true,
            add: function(other) {
                if (other !== ns.Node.ManualVisibility.UNFILTERED) {
                    return this;
                } else {
                    return other;
                }
            }
        },
        /** Uzel nebyl ručně upraven. */
        UNCHANGED: {
            name: "UNCHANGED",
            filtered: false,
            add: function(other) {
                return other;
            }
        },
        /** Uzel je ručne odfiltrován. */
        UNFILTERED : {
            name: "UNFILTERED",
            filtered: false,
            add: function(other) {
                return this;
            }
        }
    };
    
	/**
	 * Stringova reprezentace. Asi kvuli debugovani. 
	 */
	ns.Node.prototype.toString = function() {
		return "[" + this.id + "] ";
	};
	
	ns.Node.prototype.getHasSourceCode = function() {
	    var currentNode = this;
	    while (currentNode != null) {
	        if (currentNode.owner.hasSource) {
	            return true;
	        }
	        currentNode = currentNode.parent;
	    }
	    
	    return false;
	}

	/**
	 * Vytvori novy uzel podle komponenty, ktera prisla ze serveru.
	 *
	 * @param component	Dosla komponenta
	 */
	ns.Node.createFromComponent = function(component)  {
		var node = new Manta.Semantics.Node(component.id, component.proxy);
		if (component.proxy) {
			node.fillFromProxyComponent(component);
		} else {
            node.fillFromComponent(component);
        }
		return node;
	};

    /**
     * Naplni atributy uz existujiciho uzlu podle komponenty, ktera prisla ze serveru.
     * Hodi se k resolvovani proxies.
     *
     * @param component	Dosla komponenta
     */
    ns.Node.prototype.fillFromProxyComponent = function(component)  {
        this.proxy      = component.proxy;
        this.tech       = component.tech;
        this.status	    = component.status;
        this.level      = component.level;
        this.setFiltered(component.filtered);
        this.topParentTech = component.topParentTech;
    };

	/**
	 * Naplni atributy uz existujiciho uzlu podle komponenty, ktera prisla ze serveru.
	 * Hodi se k resolvovani proxies. 
	 * 
	 * @param component	Dosla komponenta
	 */
	ns.Node.prototype.fillFromComponent = function(component)  {
		this.proxy      = component.proxy;
		this.label      = component.label;
		this.tech       = component.tech;
		this.techType   = component.techType;
		this.type       = component.type;
		this.atomic	    = component.atomic;
        this.level      = component.level;
        this.attributes = $.extend({}, component.attributes);
        this.path       = component.path.slice();
        this.status     = component.status;
        this.layer      = component.layer;
        this.hasSource  = component.hasSource;
        this.setFiltered(component.filtered);     
        this.topParentTech = component.topParentTech;
        this._initNodeMapping(component.nodeMapping);
    };
	
	/** 
	 * Vrati hranice uzlu v absolutnich souradnicich. 
	 * 
	 * @returns Rectangle
	 */
	ns.Node.prototype.getAbsoluteBounds = function() {
        var parentAbsolute;

		if(this.parent === null)
			return this.bounds;
		
		parentAbsolute = this.parent.getAbsoluteBounds();
		
		return new Manta.Geometry.Rectangle(
				parentAbsolute.x + this.bounds.x, 
				parentAbsolute.y + this.bounds.y, 
				this.bounds.width, 
				this.bounds.height);               
	};
	
	/** 
	 * Prida odchozi hranu.
	 * 
	 * @param edge Nova hrana.
	 */ 
	ns.Node.prototype.addEdgeOut = function(edge) {
		this.edgesOut.pushOnce(edge);
		edge.source = this;
	};

	/** 
	 * Prida prichozi hranu.
	 * 
	 * @param edge Nova hrana.
	 */
	ns.Node.prototype.addEdgeIn = function(edge) {
		this.edgesIn.pushOnce(edge);
		edge.target = this;
	};

    /**
     * Vrati podmnozinu odchozich hran, ktere jsou viditelne
     * @returns {Array}
     */
    ns.Node.prototype.getVisibleEdgesOut = function() {
        return this.edgesOut.filter(visibleFilter);
    };

    /**
     * Vrati podmnozinu prichozich hran, ktere jsou viditelne
     * @returns {Array}
     */
    ns.Node.prototype.getVisibleEdgesIn = function() {
        return this.edgesIn.filter(visibleFilter);
    };

    //noinspection JSUnusedGlobalSymbols
    ns.Node.prototype.getAllEdges = function() {
        return this.edgesIn.concat(this.edgesOut);
    };

    //noinspection JSUnusedGlobalSymbols
    ns.Node.prototype.getAllVisibleEdges = function() {
        return this.getVisibleEdgesIn().concat(this.getVisibleEdgesOut());
    };

    /**
     * Vrati vsechny prime potomky, kteri jsou viditelni
     * @returns {Array}
     */
    ns.Node.prototype.getVisibleChildren = function() {
        return this.children.filter(visibleFilter);
    };

    /**
     * Vrati vsechny prime potomky, kteri jsou viditelni
     * @returns {Array}
     */
    ns.Node.prototype.getUnfilteredChildren = function() {
        return this.children.filter(filteredFilter);
    };

    /**
     * Rozhodne zda uzel ma nejake potomky
     * @returns {boolean}
     */
    ns.Node.prototype.hasChildren = function() {
        return this.children.length > 0;
    };

    /**
     * Vrati hloubku zanoreni uzlu v hierarchii
     * @returns {*}
     */
    ns.Node.prototype.getDepth = function() {
        if (this.parent === null) {
            return 0;
        } else {
            return this.parent.getDepth() + 1;
        }
    };

    /**
     * Urci zda ascendant je libovolnym predkem node
     *
     * @param node Uzel ktery testujeme zda je predkem tohoto uzlu
     * @returns {boolean} true pokud ascendant je predkem node
     */
    ns.Node.prototype.isDescendantOf = function(node) {
        var actual = this;

        while(actual) {
            if(actual === node) {
                return true;
            }
            actual = actual.parent;
        }
        return false;
    };

    /**
     * Nastavi uzlu parenta, ten nesmi byt ten samy uzel a nesmi byt undefined (validni hodnota je null),
     * jinak vyhodi IllegalArgumentException
     * @param parent
     */
    ns.Node.prototype.setParent = function(parent) {
        if(this === parent) {
            throw new Manta.Exceptions.IllegalArgumentException("Attempt to set node parent to node itself.");
        }
        if(parent === undefined) {
            throw new Manta.Exceptions.IllegalArgumentException("Attempt to set node parent to undefined.");
        }
        this.parent = parent;
    };

    /**
     * Najde rodice nejvyse v hierarchii. Pokud uzel nema rodice, vrati sebe sama.
     *
     * @returns {Object} korenovy uzel
     */
    ns.Node.prototype.getRoot = function() {
        var actual = this;
        while(actual.parent !== null) {
            actual = actual.parent;
        }
        return actual;
    };

    /**
     * Rozhodne zda se uzel nachazi na "nejnizsi nedelitelne urovni", to znamena,
     * ze ho nelze rozdelit. Aby bylo vraceno true, musi byt uzel bud sbaleny, nebo
     * mit vsechny potomky atomic.
     * @returns {boolean}
     */
    ns.Node.prototype.isHighestUnsplitableLevel = function()  {
        var i,
            isSatisfied = true;
        if(this.expanded) {
            for (i = 0; i < this.children.length; i++) {
                isSatisfied = isSatisfied && this.children[i].atomic;
            }
            return isSatisfied;
        } else {
            return true;
        }
    };

    /**
     * Rozbali uzel a zajisti spravne prepojeni svych hran niz do hierarchie
     */
    ns.Node.prototype.expand = function() {
        if(this.expanded || !this.hasChildren()) return;

        this.expanded = true;
        this._setDescendantsHidden(this, false);

        this._splitEdges(this.edgesIn.slice(), this.graph, false);
        this._splitEdges(this.edgesOut.slice(), this.graph, true);
        this.graph.setDirty(true);
    };

    /**
     * Sbali uzel a zajisti spravne prepojeni hran nize v hierarchii
     */
    ns.Node.prototype.collapse = function() {
        var edgesIn = {},
            edgesOut = {},
            nodeID,
            node;
        // ma vubec smysl delat collapse?
        if(!this.expanded || !this.hasChildren()) return;

        // posbiram vsechny prichozi a odchozi hrany v cele hierarchii pod uzlem
        this._collectEdgesToContract(edgesIn, 'edgesIn', 'source', this);
        this._collectEdgesToContract(edgesOut, 'edgesOut', 'target', this);

        // schovam vechny potomky v hierarchii
        this._setDescendantsHidden(this, true);

        for(nodeID in edgesIn) {
            if(edgesIn.hasOwnProperty(nodeID)) {
                // spojim prichozi hrany se stejnym koncem a pripojim je k sobe
                node = this.graph.nodeMap[nodeID];
                this.graph._mergeEdges(edgesIn[nodeID], node, this);
            }
        }

        for(nodeID in edgesOut) {
            if(edgesOut.hasOwnProperty(nodeID)) {
                // spojim odchozi hrany se stejnym koncem a pripojim je k sobe
                node = this.graph.nodeMap[nodeID];
                this.graph._mergeEdges(edgesOut[nodeID], this, node);
            }
        }
        this.expanded = false;
        this.graph.setDirty(true);
    };

    /**
     * Vytvori shadow uzel tak, ze vytrhne zadane potomky z tohoto uzlu a prida je nove vytvorenemu shadow
     * @param children seznam uzlu, ktere budou potomky nove vytvoreneho shadow uzlu
     * @returns {*} shadow uzel
     */
    ns.Node.prototype.createShadowNode = function(children) {
        var wasExpanded = this.expanded;

        // Pro atomicke uzly bez potmoku zakazu vytvaret shadows
        if (this.atomic && !this.hasChildren()) {
            throw new Manta.Exceptions.IllegalStateException("Attempt to create shadow of atomic node");
        }
        // Shadow uzly lze vytvaret pouze z puvodniho uzlu - owner
        if (this.shadow) {
            throw new Manta.Exceptions.IllegalStateException("Attempt to create shadow of shadow node");
        }

        var shadowNode = this._clone();
        this.shadows.push(shadowNode);
        shadowNode.shadow = true;
        shadowNode.owner = this;
        shadowNode.id = this.id + "_" + this.shadows.length;
        shadowNode.setParent(this.parent);
        if (this.parent !== null) {
            this.parent.children.push(shadowNode);
        }
        this.graph.nodeMap[shadowNode.id] = shadowNode;
        
        // Zbavim se vsech hran na tomto uzlu
        this.expand();

        // prehazim vsechny pozadovane potomky do shadow uzlu
        for (var i = 0; i < children.length; i++) {
            var child = children[i];
            // Odstranim potomka ze sebe a zaroven ze vsech shadow uzlu tak,
            // aby mel urcite jenom jednoho parenta
            this.children.remove(child, Manta.Utils.equalsById);
            for (var j = 0; j < this.shadows.length; j++) {
                this.shadows[j].children.remove(child, Manta.Utils.equalsById);
            }
            shadowNode.children.push(child);
            child.setParent(shadowNode);
        }
        
        if (!wasExpanded) {
            // uzel byl collapsed, zavolam teda znovu collapse na obou uzlech,
            // timto spravne prepojim vsechny hrany na uzlu
            this.collapse();
            shadowNode.collapse();
        }
        
        //console.log("creating shadow for", this.id, "->", shadowNode.id, children);
        // tento uzel uz slouzi jenom jako owner, nezobrazuje se...
        this.setShadowOwner(true);

        this.graph.setDirty(true);
        return shadowNode;
    };

    /**
     * Odstrani z grafu vsechny shadow uzly a spravne vrati vsechny potomky pod jejich
     * puvodni rodice
     */
    ns.Node.prototype.clearShadowNodes = function() {
        var i, j,
            shadow,
            wasExpanded = this.expanded;

        if(this.atomic || this.shadow || this.shadows.length === 0) return;

        // zbavim se vsech hran
        this.expand();

        for(i=0; i < this.shadows.length; i++) {
            //preberu vsechny potomky shadow zpet pod sebe a smazu ho
            shadow = this.shadows[i];
            shadow.expand();
            for(j=0; j < shadow.children.length; j++) {
                this.children.push(shadow.children[j]);
                shadow.children[j].setParent(this);
            }
            if(this.parent !== null) {
                this.parent.children.remove(shadow);
            }
            delete this.graph.nodeMap[shadow.id];
        }
        if(!wasExpanded) {
            // pokud uzel byl collapsed, znovu ho smrsknu (nyni uz se vsemi potomky)
            this.collapse();
        }
        this.setShadowOwner(false);
        this.shadows = [];
    };

    /**
     * Nastavi flag, ktery vynucuje viditelnost uzlu
     * @param forceVisible
     */
    ns.Node.prototype.setForceVisible = function(forceVisible) {
        this.forceVisible = forceVisible;
    };

    /**
     * Nastavi flag, ktery vynucuje,ze uzel ma byt sbaleny
     * @param forceCollapse
     */
    ns.Node.prototype.setForceCollapse = function(forceCollapse) {
        this.forceCollapse = forceCollapse;
    };

    /**
     * rozhodne zda uzel ma nejake shadow uzly
     * @returns {Array|boolean}
     */
    ns.Node.prototype.hasShadowNodes = function() {
        return this.shadows && this.shadows.length > 0;
    };

    /**
     * Rekurzivne projde vsechny potomky a hleda zda nejaky z nich v libovolne urovni
     * ma nastaveny priznak forceVisibility
     */
    ns.Node.prototype.isDescendantVisibilityForced = function() {
        var i;
        for(i=0; i < this.children.length; i++) {
            if(this.children[i].forceVisible || this.children[i].isDescendantVisibilityForced()) {
                return true;
            }
        }
        return false;
    };

    /**
     * Rozhodne zda uzel ma byt viditelny. Uzel neni viditelny v pripade, ze plati
     * alespon jedna podminka:
     * <ul>
     *     <li>je vyfiltrovan,</li>
     *     <li>jedna se o proxy uzel,</li>
     *     <li>uzel je sbaleny uvnitr nejakeho sveho predka (hidden),</li>
     *     <li>uzel je reprezentovany jenom svymi shadow uzly a sam by nemel byt videt</li>
     * </ul>
     * @returns {boolean}
     */
    ns.Node.prototype.isVisible = function() {
        return  !this.isFiltered() &&
                !this.proxy &&
                !this.hidden &&
                !this.shadowOwner;
    };
    
    /**
     * Ověří, jestli je uzel vyfiltrován libovolným aktivním filtrem.
     * U uzlů s potomky platí, že pokud je alespoň jeden potomek nevyfiltrován, tak je 
     * tento složený uzel taky nevyfiltrován.
     * @returns true, jestliže je uzel vyfiltrován
     */
    ns.Node.prototype.isFiltered = function () {
        var self = this;
        // Priznak vyfiltrovani / nevyfiltrovani uzlu muze byt cacheovan
        return Manta.Cache.NODE_FILTERING_CACHE.get(self, function() {
            if (self.isStartNode() || self.isStartHierarchyNode()) {
                // startovni uzel a jeho hirearchie neni nikdy filtrovana
                return false;
            } else {
                var treeManual = self._getTreeManualVisibility();                        
                if (treeManual !== ns.Node.ManualVisibility.UNCHANGED) {
                    // manualni zmena -> ovlivnuje jen ona
                    return treeManual.filtered;
                } else {
                    // zadna manualni zmena -> zalezi na aktualnich filtrech
                    return self._getTreeSimpleFiltering(); 
                }
            }
        });  
    };

    /**
     * Inicializuje mapovani uzlu.
     * Nastavi uzlu jako atribut kopii daneho mapovani a obohati ji o textove podoby cest k uzlum, ktere se mapovani ucastni. 
     * @param nodeMapping Mapovani uzlu. Jedna se o mapu, kde klicem je nazev vrstvy
     *                    a hodnota je pole odkazu na uzly, ktere tento uzel mapuje nebo jsou timto uzlem mapovany.
     */
    ns.Node.prototype._initNodeMapping = function(nodeMapping) {
        var self = this;
        self.nodeMapping = {};
        $.each(nodeMapping, function(layer, mappedNodes) {
            self.nodeMapping[layer] = [];
            $.each(mappedNodes, function(index, mappedNode) {
                self.nodeMapping[layer].push({
                    id: mappedNode.id,
                    path: $.extend({}, mappedNode.path),
                    // Pridame tetxovou podobu cesty k uzlu
                    pathString: '/' + mappedNode.path.join('/')
                });
            });
        });
    };
    
    /**
     * Zjistí jestli má být podstrom vyfiltrován, když vezmeme v potaz jen aktivní filtry.
     * Platí zde stejná logika pro složené uzly, tedy alespoň jeden nevyfiltrovaný potomek
     * znamená nevyfiltrovaní předka.
     * @returns true, jestliže je kořen vyfiltrován pouze aktivními filtry
     */
    ns.Node.prototype._getTreeSimpleFiltering = function () {
        if (this.children.length > 0) {
            for(var i = 0; i < this.children.length; i++) {
                if(!this.children[i]._getTreeSimpleFiltering()) {
                    return false;
                }
            }
            return true;
        } else {
            return this.owner.filtered || this.graph.filterManager.hasDisabledResource(this);
        }
    }
    
    /**
     * Zjistí jestli jaké má daný podstrom manuální viditelnost.
     * Sčítání manuálního nastavení se řídí prioritu UNFILTERED > CONTRACTED > UNCHANGED.
     * @returns manuální viditelnost podstromu, jehož kořen je aktuální instance
     */
    ns.Node.prototype._getTreeManualVisibility = function() {
        if(this.children.length === 0) {
            return this.manualVisibility;
        } else {
            var manualSettings = this.manualVisibility;
            for(var i = 0; i < this.children.length; i++) {
                manualSettings = manualSettings.add(this.children[i]._getTreeManualVisibility());
            }
            return manualSettings;
        } 
    }

    /**
     * Rohodne zda uzel je vyfiltrovany nebo jeste neni navstiveny a tim padem musi byt soucasti
     * nejake agregovane hrany. Zaroven zadny uzel nemuze byt soucasti agregovane hrany aniz by
     * na nem tato metoda nevracela true.
     * @returns {boolean}
     */
    ns.Node.prototype.isFilteredOrUnvisited = function() {
        return this.isFiltered() || this.status === ns.Node.NodeType.UNVISITED;
    };

    /**
     * Nastavi priznak zda je uzel vyfiltrovany
     */
    ns.Node.prototype.setFiltered = function(filtered) {
        this.filtered = filtered;
    };
    
    ns.Node.prototype.setManualVisibility = function(state) {
        this.manualVisibility = state;
    }
    
    /**
     * Nastavi priznak zda je uzel schovany = nejaky jeho predek je sbaleny
     */
    ns.Node.prototype.setHidden = function(hidden) {
        this.hidden = hidden;
    };

    ns.Node.prototype.setShadowOwner = function(shadowOwner) {
        this.shadowOwner = shadowOwner;
    };

    /**
     * Vyhleda id vsech listu nize v hierarchii
     * @returns {Array} pole id vsech listu
     */
    ns.Node.prototype.getLeavesId = function() {
        var i, ids = [];
        if(this.children.length > 0) {
            for(i=0; i < this.children.length; i++) {
                ids.append(this.children[i].getLeavesId());
            }
        } else {
            ids.push(this.id);
        }
        return ids;
    };

    /**
     * Vyhleda nejnizsi rozbalenou uroven v hierarchii od tohoto uzlu nize
     * @returns {number} pole id vsech listu
     */
    ns.Node.prototype.getLowestExpandedLevel = function() {
        var i, level = this.level;
        if(this.children.length > 0 && this.expanded) {
            for(i=0; i < this.children.length; i++) {
                level = Math.max(level, this.children[i].getLowestExpandedLevel());
            }
        }
        return level;
    };

    /**
     * Vytvori z uzlu jednoduchy objekt, ktery je mozne serializovat nar. do JSON
     * @returns {{}} nodeState objekt reprezentujici kompletni stav uzlu
     */
    ns.Node.prototype.serialize = function() {
        var i, iLen,
            nodeState = {},
            prop;

        for(prop in this) {
            // zkopiruju vsechny "jednoduche" vlastnosti uzlu, tzn. ty co nejsou pole ani funkce
            if(this.hasOwnProperty(prop) && !$.isFunction(this[prop]) && !$.isArray(this[prop])) {
                nodeState[prop] = this[prop];
            }
        }
        delete nodeState.graph; // referenci na graf musim ze stavu vyhodit...

        nodeState.owner = this.owner.id;
        nodeState.parent = this.parent ? this.parent.id : null;
        nodeState.bounds = $.extend(true, {}, this.bounds);
        nodeState.shadows = [];
        nodeState.attributes = $.extend(true, {}, this.attributes);
        nodeState.path = $.extend(true, [], this.path);
        nodeState.manualVisibility = this.manualVisibility.name;

        for(i=0, iLen=this.shadows.length; i < iLen; i++) {
            nodeState.shadows.push(this.shadows[i].id);
        }
        return nodeState;
    };

    /**
     * Nastavi uzlu stav, ktery odpovida predanemu objektu, ktery vzniknul jako zachyceni stavu jineho uzlu
     * pomoci meody serialize(). Manualne je potreba doplnit:
     *
     * - shadow uzly
     * - owner uzel
     * - parent (doplni se vlozenim do grafu)
     *
     * @param nodeState kompletni stav uzlu, ze ktereho se plni tento uzel
     */
    ns.Node.prototype.deserialize = function(nodeState) {
        var prop;

        for(prop in nodeState) {
            if(nodeState.hasOwnProperty(prop) && prop in this) {
                this[prop] = nodeState[prop];
            }
        }
        this.parent = null; // parent bude doplnen pri vlozeni do grafu
        this.bounds = new Manta.Geometry.Rectangle(
            nodeState.bounds.x,
            nodeState.bounds.y,
            nodeState.bounds.width,
            nodeState.bounds.height);
        this.shadows = []; // shadow uzly musi byt doplneny manualne az po vytvoreni vsech uzlu
        this.attributes = $.extend(true, {}, nodeState.attributes);
        this.path = $.extend(true, [], nodeState.path);
        
        for (var stateName in ns.Node.ManualVisibility) {
            if (!ns.Node.ManualVisibility.hasOwnProperty(stateName)) {
                continue;
            }
            if (ns.Node.ManualVisibility[stateName].name == nodeState.manualVisibility) {
                this.manualVisibility = ns.Node.ManualVisibility[stateName];
            }
        }
    };

    /**
     * Nalezne prvni hranu jejiz zdrojovy uzel odpovida predanemu a vede do tohoto uzlu,
     * nebo vrati null pokud hrana neexistuje
     * @param source zdrojovy uzel hrany
     */
    ns.Node.prototype.findEdgeFrom = function(source) {
        var i, edge;

        for(i=0; i < this.edgesIn.length; i++) {
            edge = this.edgesIn[i];
            if(edge.source.id === source.id) {
                return edge;
            }
        }
        return null;
    };
    
    /**
     * Ověří je uzel startovní.
     * @returns true, jestliže je uzel startovní
     */
    ns.Node.prototype.isStartNode = function() {
        return this.graph.startNodeIds.contains(this.owner.id);
    }
    
    /**
     * Ověří je uzel v startovní hierarchii, tam se počítají jak předci tak potomci startovních uzlů.
     * @returns true, jestliže je uzel startovní
     */
    ns.Node.prototype.isStartHierarchyNode = function() {
        if (this.graph.startHierarchyNodeIds == null) {
            this.graph.initHierarchyStartNodeIds();
        }
        
        return this.graph.startHierarchyNodeIds.contains(this.owner.id);
    }

    // NODE PRIVATE METHODS ///////////////////////////////////////////////////
    ns.Node.prototype._clone = function() {
        var clone = new ns.Node();
        clone.proxy             = this.proxy;
        clone.label             = this.label;
        clone.tech              = this.tech;
        clone.techType          = this.techType;
        clone.topParentTech     = this.topParentTech;
        clone.type              = this.type;
        clone.atomic            = this.atomic;
        clone.status            = this.status;
        clone.layer             = this.layer;
        clone.graph             = this.graph;
        clone.attributes        = $.extend({}, this.attributes);
        clone.path              = this.path.slice();
        clone.level             = this.level;
        clone.filtered          = this.filtered;
        clone.hasSource         = this.hasSource;
        clone.manualVisibility  = this.manualVisibility;
        return clone;
    };

    /**
     * Projde vsechny potomky uzlu (bez predaneho uzlu) v hierarchii a nastavi
     * jejich vlastnost visible na pozadovanou hodnotu.
     * @param node
     * @param value
     */
    ns.Node.prototype._setDescendantsHidden = function(node, value) {
        var i,
            child,
            length = node.children.length;

        for(i=0; i < length; i++) {
            child = node.children[i];
            child.setHidden(value);
            if(child.hasChildren() && child.expanded)
                this._setDescendantsHidden(child, value);
        }
    };

    /**
     * Rozbije predane hrany a zavola sdruzeni hran podle jejich novych zakonceni
     * @param edges
     * @param graph
     * @param out smer hrany z exandovaneho uzlu true = uzel je source,
     * false = uzel target
     */
    ns.Node.prototype._splitEdges = function(edges, graph, out) {
        var i, groups, nodeId, n1, n2,
            end = out ? 'source' : 'target';

        for(i=0; i < edges.length; i++) {
            n1 = out ? edges[i].target : edges[i].source;
            groups = this._splitEdge(graph, edges[i], end);
            for(nodeId in groups) {
                if(groups.hasOwnProperty(nodeId)) {
                    n2 = graph.nodeMap[nodeId];
                    if(out)
                        graph._mergeEdges(groups[nodeId], n2, n1);
                    else
                        graph._mergeEdges(groups[nodeId], n1, n2);
                }
            }
        }
    };

    /**
     * Rozbije hranu na jeji potomky, a najde pro kazdou novy dostupny konec, ktery
     * je v hierarchii niz nez zapojeni rozbijene hrany. Vrati shluky hran podle
     * jejich dostupnych koncu.
     *
     * @param graph
     * @param edge
     * @param end ktery konec hrany se prepojuje - "source" / "target"
     * @returns {{}} mapa [id uzlu do ktereho se ma hrana prepojit] => pole hran
     */
    ns.Node.prototype._splitEdge = function(graph, edge, end) {
        var i, child, node,
            groups = {},
            children;

        if(!edge.hasChildren()) return {};

        children = edge.children.slice();
        for(i=0; i < children.length; i++) {
            child = children[i];
            node = this._getConnectableNode(child, end);
            if (!(node.id in groups)) {
                groups[node.id] = [];
            }
            child.setParent(null);
            groups[node.id].push(child);
        }
        graph.removeEdge(edge);
        return groups;
    };

    /**
     * Vrati prvni uzel v hierarchii uzlu smerem nahoru, ktery se zobrazuje v grafu
     * a je tudiz mozne na nej napojovat viditelnou hranu
     * @param edge hrana, pro kterou hledame uzel ke kteremu se ma pripojit
     * @param endField konec hrany, ktery pripojujeme = "source" nebo "target"
     * @returns {*} nejvyse polozeny pripojitelny uzel nebo puvodni konec hrany,
     * pokud lepsi neexistuje
     */
    ns.Node.prototype._getConnectableNode = function(edge, endField) {
        var end = edge[endField],
            candidate = end;

        while(end) {
            if(!end.expanded && end.isVisible()) {
                candidate = end;
            }
            end = end.parent;
        }
        return candidate;
    };

    /**
     * Rekurzivne projde vsechny potomky a posbira jejich hrany ve smeru urcenem pomoci parametru
     * field a end
     * @param values objekt do ktereho se ukladaji posbirane hodnoty
     * @param field policko objektu, ktere slouzi jako zdroj hran - mozne hodnoty: "edgesIn" nebo "edgesOut"
     * @param end policko hrany, ktere urcuje jeji druhy konec (sousedni od prave prohledavaneho),
     * mozne hodnoty: "source", "target"
     * @param collapsedNode uzel, ktery je sbalovan (kvuli moznemu rekurzivnimu volani)
     * @returns {*} pole vsech hran v potomcich zadaneho uzlu (bez nej) v zadanem smeru
     */
    ns.Node.prototype._collectEdgesToContract = function(values, field, end, collapsedNode) {
        var i, j,
            child,
            childEdges,
            endId;

        for(i=0; i < this.children.length; i++) {
            child = this.children[i];
            if(child.isFiltered()) {
                continue;
            }

            if(child.expanded && child.hasChildren()) {
                // uzel ma jeste potomky, tudiz muze obsahovat pouze slozene hrany,
                // ktere nas nezajimaji
                child._collectEdgesToContract(values, field, end, collapsedNode);
            } else {
                // uzel je sbaleny, posbirame jeho viditelne hrany a umisitme je do chlivecku
                // podle jejich druheho konce...
                childEdges = child[field].filter(contractFilter);
                for(j=0; j < childEdges.length; j++) {
                    if(childEdges[j][end].isDescendantOf(collapsedNode)) {
                        // hrana, ktera zacina i konci v uzlech uvnitr hierarchie, vytvorim smycku
                        if(end === 'source') {
                            // kontrola na end == source je proto aby se hrana nepridala dvakrat (in + out)
                            endId = collapsedNode.id;
                        } else {
                            continue;
                        }
                    } else {
                        endId = childEdges[j][end].id;
                    }
                    if(!(endId in values)) {
                        // zadna hrana jeste do druheho konce nevede, vytvorim kolekci
                        values[endId] = [];
                    }
                    values[endId].pushOnce(childEdges[j]);
                }
            }
        }
        return values;
    };

	/**************************************************************************
	 *  Hrana grafu.
     *
     *  Mozne stavy hrany:
     *  <ol>
     *      <li>Hrana je agregovana a ma subEdges = reprezentuje jednu cestu mezi uzly</li>
     *      <li>Hrana je metahrana a ma children = Hrana sdruzuje hrany mezi potomky incidentniho sbaleneho uzlu</li>
     *      <li>Hrana neni metahrana a ma children, ktere jsou agregovane hrany reprezentujici cesty mezi stejnymi uzly</li>
     *  </ol>
	 */
	ns.Edge = function() {
        this.aggregated = false;                                // priznak, ze se jedna o agregovanou hranu
        this.semanticType = Manta.Semantics.SemanticType.EDGE;  // semanticky typ, aby slo v presentationManageru korektne
                                                                // vybirat konfigurace
		this.source = null;			                            // Realny source uzel (Node)
		this.target = null;			                            // Realny target uzel (Node)
		this.attrs = {};                                        // Atributy hrany.  
		this.bendpoints = [];		                            // Body, pre ktere hrana prochazi (Point)
		this.id = ns.Edge.baseID++;	                            // Unikatni ID hrany v klientovi - pocitani instanci... jde to jinak?
        this.children = [];
        this.parent = null;                                     // rodicovska hrana, pokud existuje
        this.type = ns.Edge.EdgeType.DIRECT;                    // typ neboli label hrany
        this.meta = false;                                      // příznak, že se jedná o parent hranu (snad)
        this.revState = ns.Edge.RevisionState.STABLE;           // typ hrany v rámci revizniho intervalu
        this.nodesOnPath = [];                                  // uzly ktere jsou na agregovane ceste reprezentoane touto hranou
    };

	/**
	 * Staticka promenna pro pocitani instanci. Pouziva se ke generovani unikatnich ID hran.  
	 */
	ns.Edge.baseID = 0;

    ns.Edge.prototype.toString = function() {
        return "[" + this.source.id + " -> " + this.target.id +
            ", aggregated: " + this.aggregated +
            ", meta: " + this.children.length > 0 + "]";
    } ;

	/**
	 * Odpoji hranu od zdrojoveho uzlu, pokud zdrojovy uzel existuje. (Vicenasobne odebrani
     * hrany je validni stav napr. u meta hran pri follow na top urovni)
	 */
	ns.Edge.prototype.disconnectFromSource = function() {
        if(this.source) {
            this.source.edgesOut.remove(this);
            this.source = null;
        }
	};

    /**
	 * Odpoji hranu od ciloveho uzlu, pokud cilovy uzel existuje. (Vicenasobne odebrani
     * hrany je validni stav napr. u meta hran pri follow na top urovni)
	 */
	ns.Edge.prototype.disconnectFromTarget = function() {
        if(this.target) {
            this.target.edgesIn.remove(this);
            this.target = null;
        }
	};

    /**
     * Nastavi hrane prislusneho rodice a zaroven rovnou vlozi hranu do potomku rodice. Pokud
     * hrana uz je potomkem jine hrany, odebere ji z jejich potomku.
     * @param parent novy parent hrany nebo null, pokud hrana nema mit parenta
     */
    ns.Edge.prototype.setParent = function(parent) {
        if(this.parent) {
            this.parent.children.remove(this);
        }
        if(parent) {
            parent.children.pushOnce(this);
        }
        this.parent = parent;
    };

    /**
	 * Nastavi priznak zda se jedna o metahranu
	 */
	ns.Edge.prototype.setMeta = function(meta) {
		this.meta = meta;
	};

    /**
	 * Vrati zda je hrana viditelna, tzn, ze splnuje vsechny podminky:
     * <ul>
     *      <li>Neni potomkem meta hrany</li>
     *      <li>Neni podhrana nejake agregovane hrany</li>
     *      <li>Neni sdruzenou agregovanou hranou (svazek agregovanych hran,
     *      ktere vedou mezi stejnymi uzly)</li>
     *      <li>Oba jeji konce jsou viditelne</li>
     * </li>
	 */
	ns.Edge.prototype.isVisible = function() {
		return  this.parent === null &&
                this.source.isVisible() &&
                this.target.isVisible();
	};

    /**
     * Vycet vsech moznych typu hran v grafu
     * @type {{DIRECT: string, FILTER: string}}
     */
    ns.Edge.EdgeType = {
        DIRECT : "DIRECT",
        FILTER : "FILTER"
    };
    
    /**
     * Stavy hran v revizi. 
     */
    ns.Edge.RevisionState = {
        DELETED: "DELETED",
        NEW: "NEW",
        STABLE: "STABLE",
        MIXED: "MIXED"
    }

    /**
     * Rozhodne zda je hrana smycka (ma stejny pocatek i konec)
     *
     * @returns {boolean} true pokud je hrana smycka
     */
    ns.Edge.prototype.isSelfLoop = function() {
        return this.target === this.source;
    };

    /**
     * Rozhodne zda jsou si hrany rovny. Hrany jsou si rovny pokud:
     * <ul>
     *  <li>maji stejny pocatek a konec a zaroven</li>
     *  <li>jsou stejneho typu (viz Manta.Semantics.Edge.EdgeType) a zaroven</li>
     *  <li>obe jsou/nejsou agregovane</li>
     * </ul>
     * @param edge
     * @returns {boolean}
     */
    ns.Edge.prototype.equals = function(edge) {
        var equals = true;
        if(edge === null || edge === undefined) {
            return false;
        }
        if(edge === this) {
            return true;
        }
        equals = equals && this.target.id === edge.target.id;
        equals = equals && this.source.id === edge.source.id;
        equals = equals && this.type === edge.type;
        equals = equals && this.aggregated === edge.aggregated;
        return equals;
    };

    /**
     * Rozhodne zda hrana ma nejake potomky
     *
     * @returns {boolean}
     */
    ns.Edge.prototype.hasChildren = function() {
        return this.children.length > 0;
    };

    /**
     * Otoci hranu
     */
    ns.Edge.prototype.reverse = function() {
        var temp = this.source,
            wasAttachedToSource,
            wasAttachedToTarget;
        
        // Musime si zapamatovat napojenost hrany
        // - ta muze byt zaslepena, a tim padem byt napojena nemusi
        wasAttachedToSource = this.source.edgesOut.remove(this);
        wasAttachedToTarget = this.target.edgesIn.remove(this);

        if (wasAttachedToSource) {
            this.source.edgesIn.push(this);
        }
        if (wasAttachedToTarget) {
            this.target.edgesOut.push(this);
        }

        this.source = this.target;
        this.target = temp;
        this.bendpoints.reverse();
    };

    /**
     * Zaslepi hranu. Dulezite pro vypocet layoutu, kdy se nepovede hrano otocit.
     */
    ns.Edge.prototype.blind = function() {
        this.source.edgesOut.remove(this);
        this.target.edgesIn.remove(this);
    };

    /**
     * Zrusi zaslepeni hrany.
     */
    ns.Edge.prototype.unblind = function() {
        this.source.edgesOut.push(this);
        this.target.edgesIn.push(this);
    };

    /**
     * Vytvori objekt, ktery je serializovatelny do JSON a zaroven reprezentuje kompletni stav hrany tak,
     * aby bylo mozne ji pomoci metody deserialize plne obnovit do puvodniho stavu.
     *
     * @returns {{}} kompletni stav hrany
     */
    ns.Edge.prototype.serialize = function() {
        var i, iLen,
            edgeState = {};

        edgeState.id = this.id;
        edgeState.source = this.source.id;
        edgeState.target = this.target.id;
        edgeState.attrs =  this.attrs;
        edgeState.aggregated = this.aggregated;
        edgeState.bendpoints = $.extend(true, [], this.bendpoints);
        edgeState.parent = this.parent ? this.parent.id : null;
        edgeState.type = this.type;
        edgeState.meta = this.meta;
        edgeState.revState = this.revState;
        edgeState.nodesOnPath = [];
        for(i=0, iLen=this.nodesOnPath.length; i < iLen; i++) {
            edgeState.nodesOnPath.push(this.nodesOnPath[i].id);
        }
        return edgeState;
    };

    /**
     * Vyplni vsechny hodnoty podle predaneho stavu, ktery byl ziskan pomoci metody serialie(). Nasledne
     * je potreba manualne doplnit:
     *
     * - parent hrany
     * - source target (doplni se vloenim hrany do grafu)
     *
     * @param edgeState
     * @param graph
     * @returns {*}
     */
    ns.Edge.prototype.deserialize = function(edgeState, graph) {
        var i, iLen;

        this.id = edgeState.id;
        this.aggregated = edgeState.aggregated;
        this.bendpoints = $.extend(true, [], edgeState.bendpoints);
        this.attrs = edgeState.attrs;
        this.type = edgeState.type;
        this.meta = edgeState.meta;
        this.revState = edgeState.revState;
        this.nodesOnPath = [];
        // vlozim vsechny uzly po ceste podle jejich idcek
        for(i=0, iLen=edgeState.nodesOnPath.length; i < iLen; i++) {
            this.nodesOnPath.push(graph.nodeMap[edgeState.nodesOnPath[i]]);
        }
    };

}(Manta.Semantics = Manta.Semantics || {}, jQuery));