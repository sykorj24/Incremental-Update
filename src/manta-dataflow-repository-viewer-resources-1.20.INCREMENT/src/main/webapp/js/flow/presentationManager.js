/*******************************************************************************
 * @namespace Manta.PresentationManager
 * @author Martin Slapak, Tomas Fechtner
 * 
 * Trida starajici se o konfiguraci graficke stranky objektu.
 * 
 ******************************************************************************/

(function(ns, $, undefined) {
    var _DEFAULT_TECHNOLOGY = 'Default';
    
    var _DEFAULT_TYPE = 'Table';
    
    var gfxConf = {};
    ns.gfxConf = gfxConf; // public visibility

    /**
     * Z predane URL vytvori DOM objekt Image.
     * 
     * @param url Adresa obrazku z nejz se ma vytvorit DOM objekt.
     * @returns Image DOM objekt.
     */
    ns.makeImgFromURL = function(url) {
        if (typeof url !== 'string')
            return undefined;
        var image = new Image();
        image.src = Manta.Globals.contextPath + url;
        // image.onload = function() {ns.loadedImages++;};
        // ns.usedImages++;
        return image;
    };

    /**
     * Vraci konfiguraci grafiky na zaklade nastaveni v JSONu a predane entity.
     * 
     * @param model semanticka entita pro niz se ma vybrat korektni nastaveni
     */
    ns.getGFXConf = function(model) {
        var key = null;
        var cnf = {};
        var specific = {};
        if (model.semanticType === Manta.Semantics.SemanticType.EDGE) {
            // udelame deepcopy defaultni hodnoty, at si ji
            // pak neprepisujeme pri jinych konkretizacich
            cnf = $.extend(true, {}, gfxConf.commonConf['defEdge']);
            key = (model.aggregated ? "AGGREGATED_" : "") + model.type;

            $.extend(true, cnf, gfxConf.commonConf['edges'][key] ? gfxConf.commonConf['edges'][key] : {});
            ns._mergeEdgeLayers(cnf, model);
        }
        if (model.semanticType === Manta.Semantics.SemanticType.NODE) {
            // udelame deepcopy defaultni hodnoty, at si ji
            // pak neprepisujeme pri jinych konkretizacich
            cnf = $.extend(true, {}, gfxConf.commonConf['defNode']);
            ns._mergeTechnologyAndTypeToNode(cnf, gfxConf.nodesConf["Default"], model.type);
            ns._mergeTechnologyAndTypeToNode(cnf, gfxConf.nodesConf[model.techType], model.type);
            ns._mergeLayers(cnf, model);
            if (Manta.Utils.isStartingNode(model.owner)) {
                // Pokud se jedna o startovni uzel, ostylujeme jej specialne
                $.extend(true, cnf, gfxConf.commonConf['startNode']);
            }
        }
        return cnf;
    };

    /**
     * Vrati ikonu na zaklade technologie a typu uzlu. Pokud ji nenajde, varti
     * defaultni ikonu.
     * 
     * @param technology Technologie
     * @param type Typ uzlu
     * @returns Ikona na zaklade technologie a typu uzlu.
     */
    ns.getIconByTechnologyAndType = function(technology, type) {
        // tato technologie neexistuje - prepnout na default
        if (gfxConf.nodesConf[technology] === undefined) {
            technology = _DEFAULT_TECHNOLOGY;
        } else {
            // pro danou technologii neni definovan tento typ -> prepnout na Default technologii
            if (!type || gfxConf.nodesConf[technology][type] === undefined) {
                technology = _DEFAULT_TECHNOLOGY;
            }
        }
        
        // ani pro takto osetrenou technologii neni typ definovan -> prepnout na default typ         
        if (!type || gfxConf.nodesConf[technology][type] === undefined) {
            type = _DEFAULT_TYPE;
        }
        return gfxConf.nodesConf[technology][type].iconPositive;
    };

    /**
     * Vrati atributy daneho typu uzlu pro danou technologii. Pokud neni
     * specifikovan typ, bude vracen prazdny objekt. Pokud neni specifikovana
     * technologie nebo je neznama, budou vraceny atributy daneho typu v
     * defultni technologii. V ostatnich pripadech budou vraceny atributy daneho
     * typu v defaulti technologii rozsirene ci prepsane atributy daneho typu ve
     * specifikovane technologii.
     * 
     * @param technology Technologie
     * @param type Typ uzlu
     * @returns {Object} Atributy na zaklade technologie a typu uzlu.
     */
    ns.getAttributesByTechnologyAndType = function(technology, type) {
        var result = {};
        $.extend(result, gfxConf.commonConf.defNode.attributes);   
        if (!type) {
            return result;
        }
        
        if (!technology || gfxConf.nodesConf[technology] === undefined) {
            technology = "Default";
        }
        if (gfxConf.nodesConf["Default"][type] !== undefined) {
            $.extend(result, gfxConf.nodesConf["Default"][type].attributes);
        }
        if (gfxConf.nodesConf[technology][type] !== undefined) {
            $.extend(result, gfxConf.nodesConf[technology][type].attributes);
        }
        return result;
    };

    /**
     * Objekt poskytujici univerzalni obrazky (expand, follow, ...)
     */
    ns.getImages = function() {
        return gfxConf.commonConf['universalImages'];
    };

    /**
     * Objekt se zakladnim nastavenim rozmeru nodu. Pouziva se pro layout.
     */
    ns.BasicNodeBounds = function() {
        var em = 4;
        this.elementMargin = em;
        this.minHeight = em * 2 + 16;
        this.minWidth = em * 2 + 12 + em + 16 + em + 150 + em + 16;
        this.headHeight = em * 2 + 16;
    };

    /**
     * Vrátí model barevných vrstev.
     * 
     * @return pole vrstev, pro každou vrstu obsahuje id, jméno a flag jestli je zaškrtnutá
     */
    ns.getColorLayers = function() {
        var result = {};

        for (var i = 0; i < gfxConf.layers.length; i++) {
            var layerModel = {};
            layerModel.id = i;
            layerModel.checked = gfxConf.layers[i].checked;
            layerModel.name = gfxConf.layers[i].name;
            result[i] = layerModel;
        }
        return result;
    };

    /**
     * Nastaví jestli je vrstva aktivní.
     * 
     * @param layerId id vrstvy, která se má nastavit
     * @param isChecked flag, jestli je aktivní
     */
    ns.setLayerChecked = function(layerId, isChecked) {
        if (gfxConf.layers[layerId] != undefined) {
            gfxConf.layers[layerId].checked = isChecked;
            $("#colorLayer_" + layerId).prop('checked', isChecked);
        }
    };
    
    /**
     * Vrátí stav nastavení barevných vrstev.
     * @return mapa stavu, kde klíč je id vrstvy a hodnota true, jestli je zaškrtnutá 
     */
    ns.getLayersState = function() {
        var currentState = {};
        for (var id in gfxConf.layers) {
            currentState[id] = gfxConf.layers[id].checked;
        }
        
        return currentState;
    } 
    
    /**
     * Porovná, jestli se aktuální stav nastavení vrstev liší od obdrženého. 
     * @param otherState stav k porovnání
     * @return true, jestli se stavy liší
     */
    ns.isCurrentStateDifferent = function(otherState) {
        if (otherState === undefined) {
            return true;
        }
        
        for (var id in gfxConf.layers) {
            if (gfxConf.layers[id].checked !== undefined && otherState[id] === undefined || otherState[id] != gfxConf.layers[id].checked ) {
                return true;
            }            
        }
        
        return false;
    }
    
    /**
     * Nastaví zapnutí barevných vrstev na daný stav. 
     * @param newState nový stav zapnutí vrstev
     */
    ns.setLayersState = function(newState) {
        for (var id in newState) {
            this.setLayerChecked(id, newState[id]);
        }
    }

    /**
     * Načte konfiguraci.
     */
    ns.loadConfig = function() {
        $.ajax({
            dataType : "json",
            cache: false,
            url : Manta.Globals.contextPath + "/js/gfx/common-conf.json",
            async : false,
            success : Manta.PresentationManager._parseCommonConfig,
            error :  Manta.PresentationManager._corruptedCommonConfig
        });
        $.ajax({
            dataType : "json",
            cache: false,
            url : Manta.Globals.contextPath + "/js/gfx/nodes-conf.json",
            async : false,
            success : Manta.PresentationManager._parseNodesConfig,
            error :  Manta.PresentationManager._corruptedNodesConfig
        });
        $.ajax({
            dataType : "json",
            cache: false,
            url : Manta.Globals.contextPath + "/js/gfx/layers/_layers.json",
            async : false,
            success : Manta.PresentationManager._parseLayers,
            error :  Manta.PresentationManager._corruptedLayers
        });
    };
    
    ns._corruptedCommonConfig = function(jqXHR, textStatus, errorThrown) {
        ns._showErrorMessage("Common configuration is corrupted.");
    };
    
    ns._corruptedNodesConfig = function(jqXHR, textStatus, errorThrown) {
        ns._showErrorMessage("Nodes configuration is corrupted.");
    };
    
    ns._corruptedLayers = function(jqXHR, textStatus, errorThrown) {
        ns._showErrorMessage("Configuration with list of layers is corrupted.");
    };
   
    ns._corruptedLayersConfig = function(jqXHR, textStatus, errorThrown) {
        ns._showErrorMessage("Layer configuration is corrupted.");
    };
    
    var messagesTemplate = Handlebars.compile($("#messages-template").html());
    ns._showErrorMessage = function(message) {
        var handlebarsMessages = [];
        handlebarsMessages.push({'text': message, 'cssClass': 'alert-danger'});
        var $messageConfigDiv = $('#messagesConfig');
        if ($messageConfigDiv != undefined) {            
            $('#messages').after('<div id="messagesConfig"></div>');
            $messageConfigDiv = $('#messagesConfig');
        }        
        $messageConfigDiv.append(messagesTemplate(handlebarsMessages));
    };
    

    /**
     * Zmerguje konfiguraci z barevných vrstev.
     * 
     * @param nodeCnf výsledná konfigurace, výstupní atribut
     * @param model model uzlu, pro který se hledá konfigurace
     */
    ns._mergeLayers = function(nodeCnf, model) {
        var resultConf = null;
        model.cnfPropagation = null;

        // prohledat vrstvy
        for ( var layerIndex in gfxConf.layers) {
            var layer = gfxConf.layers[layerIndex];
            if (layer.definitions != undefined && layer.checked) {
                resultConf = ns._selectAttrConfigWithHigherPriority(resultConf, layer, model);
                resultConf = ns._selectConfigWithHigherPriority(resultConf, layer.definitions.types[model.type]);
                resultConf = ns._selectConfigWithHigherPriority(resultConf, layer.definitions.resources[model.techType]);
                resultConf = ns._selectConfigWithHigherPriority(resultConf, layer.definitions.any);

                if (resultConf != null) {
                    break;
                }
            }
        }

        // zkontrolovat jestli se něco nepropaguje z předka
        if (model.parent != null && model.parent.owner.cnfPropagation != null) {
            resultConf = ns._selectConfigWithHigherPriority(resultConf, model.parent.owner.cnfPropagation);
        }

        // aplikovat podle color schématu uzlu
        var colorSchemaName = nodeCnf.colorSchemaName;
        if (resultConf != null && resultConf != undefined && colorSchemaName != undefined) {
            if (resultConf.colorSchema[colorSchemaName] != null) {
                $.extend(true, nodeCnf, resultConf.colorSchema[colorSchemaName]);

                if (resultConf.propagation == "Down") {
                    model.owner.cnfPropagation = resultConf;
                } else {
                    model.owner.cnfPropagation = null;
                }
            } else {
                console.log("Undefined color schema name ", colorSchemaName, " in ", resultConf);
            }
        }
    };
    
    /**
     * Zmerguje konfiguraci z barevných vrstev hran.
     * 
     * @param edgeCnf výsledná konfigurace, výstupní atribut
     * @param model model hrany, pro který se hledá konfigurace
     */
    ns._mergeEdgeLayers = function(edgeCnf, model) {
        var resultConf = null;
        model.cnfPropagation = null;

        // prohledat vrstvy
        for ( var layerIndex in gfxConf.layers) {
            var layer = gfxConf.layers[layerIndex];
            if (layer.edgeDefinitions != undefined && layer.checked) {
                resultConf = ns._selectEdgeAttrConfigWithHigherPriority(resultConf, layer, model);
                if (resultConf != null) {
                    break;
                }
            }
        }

        if (resultConf != null) {
            $.extend(true, edgeCnf, resultConf.css);
        }
    };

    /**
     * Vybere konfiguraci s vyšší prioritou, pričemž prohledává konfigurace pro
     * atributy.
     * 
     * @param actual aktuální konfiguraci, má přednost při stejných prioritách
     * @param layer vrstva s konfiguracemi, který se prohledává
     * @param model model uzlu, pro který se konfigurace hledá
     * @return konfigurace s vyšší prioritou
     */
    ns._selectAttrConfigWithHigherPriority = function(actualConf, layer, model) {
        var resultConf = actualConf;
        for (var attrKey = 0; attrKey < layer.interestingAttributes.length; attrKey++) {
            var attrName = layer.interestingAttributes[attrKey];
            var attrValueArray = model.attributes[attrName];
            var attrDefinitions = layer.definitions.attrs[attrName];
            if (attrValueArray != undefined && attrDefinitions != undefined) {
                // nejprve STABLE a NEW, mají prioritu, protože platí stále
                for (var attrArrayIndex = 0; attrArrayIndex < attrValueArray.length; attrArrayIndex++) {
                    if (attrValueArray[attrArrayIndex].revState == Manta.Semantics.Edge.RevisionState.STABLE 
                            || attrValueArray[attrArrayIndex].revState == Manta.Semantics.Edge.RevisionState.NEW) {
                        resultConf = ns._selectConfigWithHigherPriority(resultConf,
                                attrDefinitions[attrValueArray[attrArrayIndex].value]);
                    }
                }
                
                // teprve potom zbytek
                for (var attrArrayIndex = 0; attrArrayIndex < attrValueArray.length; attrArrayIndex++) {
                    if (attrValueArray[attrArrayIndex].revState != Manta.Semantics.Edge.RevisionState.STABLE 
                            && attrValueArray[attrArrayIndex].revState != Manta.Semantics.Edge.RevisionState.NEW) {
                        resultConf = ns._selectConfigWithHigherPriority(resultConf,
                                attrDefinitions[attrValueArray[attrArrayIndex].value]);
                    }
                }
            }
            
            var attrExistanceDefinitions = layer.definitions.attrsExistance[attrName];
            if (attrValueArray != undefined && attrExistanceDefinitions !== undefined) {
                resultConf = ns._selectConfigWithHigherPriority(resultConf, attrExistanceDefinitions);
            }
        }
        return resultConf;
    };
    
    ns._selectEdgeAttrConfigWithHigherPriority = function(actualConf, layer, model) {
        var resultConf = actualConf;
        for (var attrKey = 0; attrKey < layer.interestingAttributes.length; attrKey++) {
            var attrName = layer.interestingAttributes[attrKey];
            var attrValue;
            if (attrName === "revState") {
                attrValue = model.revState;
            } else {
                if (model.attrs[attrName] != null) {
                  attrValue = model.attrs[attrName].value;
                } else {
                  attrValue = "";
                }                
            }
            var attrDefinitions = layer.edgeDefinitions.attrs[attrName];
            if (attrValue != undefined && attrDefinitions != undefined) {
                resultConf = ns._selectConfigWithHigherPriority(resultConf, attrDefinitions[attrValue]);
            }
            
            var attrExistanceDefinitions = layer.edgeDefinitions.attrsExistance[attrName];
            if (attrValue != undefined && attrExistanceDefinitions !== undefined) {
                resultConf = ns._selectConfigWithHigherPriority(resultConf, attrExistanceDefinitions);
            }
        }
        return resultConf;
    };

    /**
     * Vybere konfiguraci s vyšší prioritou.
     * 
     * @param actual aktuální konfiguraci, má přednost při stejných prioritách
     * @oaram potentional potencionální konfigurace
     * @return konfigurace s vyšší prioritou
     */
    ns._selectConfigWithHigherPriority = function(actual, potentional) {
        if (potentional == undefined || potentional == null || potentional.priority == undefined) {
            return actual;
        } else if (actual == undefined || actual == null || actual.priority == undefined) {
            return potentional;
        } else if (potentional.priority > actual.priority) {
            return potentional;
        } else {
            return actual;
        }
    };

    /**
     * Nalije atributy specificke pro technologii a typ do konfigurace uzlu.
     * Pokud technologie nebo uzel neni definovan(a), nic se nestance.
     * 
     * @param nodeCnf konfigurace uzlu
     * @param tech Technologie
     * @param type typ uzlu
     */
    ns._mergeTechnologyAndTypeToNode = function(nodeCnf, tech, type) {
        // HOTFIX - rychla oprava, jen aby to nepadalo...
        var specific = (tech && tech[type]) ? tech[type] : {};
        $.extend(true, nodeCnf, specific);
    };

    /**
     * Načte obecnou konfiguraci.
     */
    ns._parseCommonConfig = function(json) {
        var key = null;
        // URL obrazku musime nahradit za DOM objekty Image
        // obrazky defaultnich entit
        json.defNode.iconPositive = ns.makeImgFromURL(json.defNode.iconPositive);
        json.defNode.iconNegative = ns.makeImgFromURL(json.defNode.iconNegative);
        json.defNode.bgimage = ns.makeImgFromURL(json.defNode.bgimage);
        // univerzalni obrazky
        for (key in json.universalImages) {
            if (json.universalImages.hasOwnProperty(key)) {
                json.universalImages[key] = ns.makeImgFromURL(json.universalImages[key]);
            }
        }
        gfxConf.commonConf = json;
        console.log("loaded JSON GFX common conf: ", json);
    };

    /**
     * Načte konfiguraci jednotlivých uzlů v technologiích.
     */
    ns._parseNodesConfig = function(json) {
        var techKey = null;
        var nodeKey = null;
        // jednotlivé resource
        for (techKey in json) {
            if (json.hasOwnProperty(techKey)) {
                tech = json[techKey];
                // jednotlivé uzly
                for (nodeKey in tech) {
                    if (tech.hasOwnProperty(nodeKey)) {
                        entity = tech[nodeKey];
                        entity.iconPositive = ns.makeImgFromURL(entity.iconPositive);
                        entity.iconNegative = ns.makeImgFromURL(entity.iconNegative);
                        entity.bgimage = ns.makeImgFromURL(entity.bgimage);
                    }
                }
            }
        }
        gfxConf.nodesConf = json;
        console.log("loaded JSON GFX nodes conf: ", json);
    };

    /**
     * Načte všechny barevné vrstvy.
     */
    ns._parseLayers = function(json) {
        for (var i = 0; i < json.layers.length; i++) {
            var name = json.layers[i];
            $.ajax({
                dataType : "json",
                cache: false,
                url : Manta.Globals.contextPath + "/js/gfx/layers/" + name + ".json",
                async : false,
                success : Manta.PresentationManager._parseLayerConfig,
                error: Manta.PresentationManager._corruptedLayersConfig
            });
        }

        // setridit podle priority sestupne
        gfxConf.layers.sort(function(a, b) {
            return a.priority < b.priority;
        });
    };

    /**
     * Načte a zpracuje jednu konkrétní vrstvu.
     */
    ns._parseLayerConfig = function(json) {
        if (gfxConf.layers == undefined) {
            gfxConf.layers = [];
        }

        var isChecked;
        if (Manta.Globals.isComparison) {
            isChecked = json.checkedComparison !== undefined ? json.checkedComparison : false;
        } else {            
            isChecked = json.checked;
        }
        
        var newLayer = {
            "name" : json.name,
            "checked" : isChecked,
            "propagation" : json.propagation,
            "priority" : json.priority,
            "interestingAttributes" : [],
            "definitions" : {
                "any": {},
                "resources" : {},
                "types" : {},
                "attrs" : {},
                "attrsExistance" : {}
            },
            "edgeDefinitions" : {
                "any": {},
                "attrs" : {},
                "attrsExistance" : {}
            }
        };

        var interestingAttributes = [];

        for (var i = 0; i < json.definitions.length; i++) {
            var def = json.definitions[i];
            var key = def.identification.value;
            def.priority = newLayer.priority;

            switch (def.identification.type) {
            case "any":
                newLayer.definitions.any = def;
            case "resource":
                newLayer.definitions.resources[key] = def;
                break;
            case "type":
                newLayer.definitions.types[key] = def;
                break;
            case "attribute":
                var attrValue = def.identification.attrValue;
                if (attrValue !== undefined) {
                    if (newLayer.definitions.attrs[key] == null) {
                        newLayer.definitions.attrs[key] = {};                        
                    }                
                    newLayer.definitions.attrs[key][attrValue] = def;
                } else {
                    newLayer.definitions.attrsExistance[key] = def;
                }
                interestingAttributes.push(key);
                break;
            default:
                console.log("Unknown identification: ", def);
            }
        }
        
        if (json.edgeDefinitions !== undefined) {
            for (var i = 0; i < json.edgeDefinitions.length; i++) {
                var def = json.edgeDefinitions[i];
                var key = def.identification.value;
                def.priority = newLayer.priority;
    
                switch (def.identification.type) {
                case "any":
                    newLayer.edgeDefinitions.any = def;
                case "attribute":
                    var attrValue = def.identification.attrValue;
                    if (attrValue !== undefined) {
                        if (newLayer.edgeDefinitions.attrs[key] == null) {
                            newLayer.edgeDefinitions.attrs[key] = {};                        
                        }                
                        newLayer.edgeDefinitions.attrs[key][attrValue] = def;
                    } else {
                        newLayer.edgeDefinitions.attrsExistance[key] = def;
                    }
                    interestingAttributes.push(key);
                    break;
                default:
                    console.log("Unknown identification: ", def);
                }
            }
        }
        
        newLayer.interestingAttributes = interestingAttributes;

        gfxConf.layers.push(newLayer);
        console.log("loaded GFX layer", json.name, newLayer);
    };

    /**
     * Vrati nastavenou hodnotu z globalnich nastaveni (klic "globalSettings" v
     * JSON konfiguraci)
     * 
     * @param key nazev policka objektu globalnich nastaveni
     * @param defaultValue defaultni hodnota, ktera se ma vratit, pokud neni hodnota nastavena v konfiguraci
     * @returns {*}
     */
    ns.getGlobalSettings = function(key, defaultValue) {
        if (gfxConf.commonConf['globalSettings'][key] === undefined) {
            return defaultValue;
        } else {
            return gfxConf.commonConf['globalSettings'][key];
        }
    };

}(Manta.PresentationManager = Manta.PresentationManager || {}, jQuery));
