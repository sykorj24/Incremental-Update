/**
 * Refresher, který při aktivním okně udržuje session a zároveň kontroluje,
 * že je uživatel přihlášen, jinak obnoví okno a tím zobrazí login obrazovku. 
 * 
 */
(function(ns, $) {
    
    ns.REFRESH_LIMIT = 60000;
    
    ns.General = function() {
        ns.attachEvent(this, "/viewer/check");
        
    };
    
    ns.Flow = function(sessionIndex) {
        ns.attachEvent(this, "/viewer/dataflow/check/" + sessionIndex);
        
    };
    
    ns.attachEvent = function (obj, url) {
        obj.lastRefresh = 0;
        $(document).mousemove(function() {
            var now = new Date().getTime();
            
            if (now > obj.lastRefresh + ns.REFRESH_LIMIT) {
                $.ajax({
                    type: "GET",
                    url : Manta.Globals.contextPath + url,
                    async : true,
                    success : function(data, textStatus, jqXHR) {
                        if (data !== "OK") {
                            dialog = new BootstrapDialog({
                                title: 'Sesion',
                                message: 'The login session has expired',
                                buttons: [{
                                    label: 'Close',
                                    action: function(dialogItself){
                                        dialogItself.close();
                                        location.reload();
                                    }
                                }],
                                animate: false,
                                closable : false,
                                onshown: function(dialog) {
                                    obj.lastRefresh = Number.MAX_VALUE;
                                }
                            });
                            dialog.realize();
                            dialog.open();
                        }
                    },
                    error: function(jqXHR, textStatus, error) {
                        location.reload();                
                    }
                });            
                
                obj.lastRefresh = now;
            }
        });
    }

}(Manta.Refresher = Manta.Refresher || {}, jQuery));
