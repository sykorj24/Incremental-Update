/**************************************************************************
 * @namespace    Manta.CollapseSettings
 * @author       Tomas Fechtner
 * 
 * 
 * 
 **************************************************************************/

(function(ns, $) {
    
    ns.COLLAPSE_NAME2LVL = {"LOW": 0, "MIDDLE":1, "BOTTOM": 2};
    ns.COLLAPSE_LVL2NAME = {0: "LOW", 1: "MIDDLE", 2: "BOTTOM"};
    
    ns.CollapseManager = function(graph) {
        this.graph = graph;
    };
    
    ns.CollapseManager.prototype.getSettings = function() {
        var resState = {};
        for (var resName in Manta.Globals.resourceMap) {
            var resId = Manta.Globals.resourceMap[resName];
            resState[resName] = $('input[name="collapse_' + resId + '"]:checked').val() * 1; 
        }
        
        return resState;
    }
    
    ns.CollapseManager.prototype.setSettings = function(newSettings) {
        if (newSettings === undefined ) {
            return;
        }
        
        for (var resName in newSettings) {
            var resId = Manta.Globals.resourceMap[resName];
            $('input[name="collapse_' + resId + '"][value=' + newSettings[resName] + ']').prop('checked', true);
        }
        
    }

    ns.CollapseManager.prototype.changeNode = function(node) {
        var resourceId = Manta.Globals.resourceMap[node.owner.tech];
        var customBtn = $("#collapse_" + resourceId + "_custom");
        customBtn.prop("checked", true);
    };
    
    ns.CollapseManager.prototype.apply = function() {
        var resState = this.getSettings();        
        var nodesToCollapse = [];
        var nodesToExpand = [];
        var alreadyAdded = {};
        // projit uzly
        for (var nodeId in this.graph.nodeMap) {
            var node = this.graph.nodeMap[nodeId];
            var toBeLevel = resState[node.owner.tech];
            if (toBeLevel == -1) {
                continue;
            }
            if (node.status == "UNVISITED") {
                continue;
            }
            
            if (!node.proxy && node.level > toBeLevel && node.parent != undefined && node.parent.expanded) {
                if (alreadyAdded[node.parent.id] != true) {
                    nodesToCollapse.push(node.parent);
                    alreadyAdded[node.parent.id] = true;
                }
            } else if (node.level <= toBeLevel && node.parent != undefined && (!node.parent.expanded || node.parent.proxy)) {
                if (alreadyAdded[node.parent.id] != true) {
                    nodesToExpand.push(node.parent);
                    alreadyAdded[node.parent.id] = true;
                }
            }
        }
        
        // aplikovat zmeny
        var op = Window.operationContext.getOperation(Manta.Operations.ExpandCollapse.NAME);
        if (nodesToCollapse.length > 0) {
            op.invokeCollapse(nodesToCollapse);
        }
        if (nodesToExpand.length > 0) {
            op.invokeExpand(nodesToExpand);
        }
    };

}(Manta.CollapseSettings = Manta.CollapseSettings || {}, jQuery));
