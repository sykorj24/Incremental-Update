/**************************************************************************
 * @namespace    Manta.FilterSettings
 * @author       Tomas Fechtner
 * 
 * 
 * 
 **************************************************************************/

(function(ns, $) {
        
    ns.FilterManager = function() {
        this.resourcesEnableList = {};
        this.customFiltersEnableList = {};
        this.graph = {};
    };
    
    /**
     * Nacte nastaveni formuláře. 
     */
    ns.FilterManager.prototype.loadFromForm = function() {
        var self = this;
        $('#resource-list').find('input').each(function() {
            var key = $(this).val();
            self.resourcesEnableList[key] = $(this).prop('checked');
        });
        
        $('#filter-list').find('input').each(function() {
            var key = $(this).val();
            self.customFiltersEnableList[key] = $(this).prop('checked');
        });
    };
    
    /**
     * Aplikuje aktuální nastavení ve formuláři.
     */
    ns.FilterManager.prototype.applyNewFilters = function() {
        var oldResourcesEnableList = jQuery.extend(true, {}, this.resourcesEnableList);
        var oldCustomFiltersEnableList = jQuery.extend(true, {}, this.customFiltersEnableList);
        this.loadFromForm();
                
        if (!this._isCustomFiltersSame(oldCustomFiltersEnableList)) {
            // prenacist informace o odfiltrovanosti pres custom filtry
            
            var nodes = [];
            for (var i in this.graph.nodeMap) {
                nodes.pushOnce(this.graph.nodeMap[i].owner.id);
            }
            
            var filters = [];
            for (var i in this.customFiltersEnableList) {
                if (this.customFiltersEnableList[i] === true) {
                    filters.push(i);
                }
            }
            
            var hiddenByFilters = Window.operationContext.getOperation(Manta.Operations.HiddenByFilters.NAME);
            hiddenByFilters.invoke(nodes, filters, function() {
                var filterOp = Window.operationContext.getOperation(Manta.Operations.Filter.NAME);
                filterOp.invoke();
            });
        } else {
            var filterOp = Window.operationContext.getOperation(Manta.Operations.Filter.NAME);
            filterOp.invoke();
        }
    }
    
    /**
     * Resetuje manuální nastavení uzlů a překreslí graf.
     */
    ns.FilterManager.prototype.cleanManualVisibility = function() {
        for (var i in this.graph.nodeMap) {
            this.graph.cleanNodeManualVisibility(this.graph.nodeMap[i]);
        }
        
        var filterOp = Window.operationContext.getOperation(Manta.Operations.Filter.NAME);
        filterOp.invoke();
    }
    
    /**
     * Vrátí množinu id aktivních filtrů.
     * @return pole id aktivních filtrů, nikdy null  
     */
    ns.FilterManager.prototype.getActiveFilters = function() {
        var filters = [];
        
        for (var i in this.resourcesEnableList) {
            if (this.resourcesEnableList[i] === false) {
                filters.push(i);
            }
        }
        
        for (var i in this.customFiltersEnableList) {
            if (this.customFiltersEnableList[i] === true) {
                filters.push(i);
            }
        }
        return filters;
    }
    
    /**
     * Ověří, jestli má uzel resource, který se nemá zobrazovat.
     * @param node zkoumaný uzel
     * @return true, jestliže je uzlův resource vyfiltrován 
     */
    ns.FilterManager.prototype.hasDisabledResource = function(node) {
        var resourceId = node.owner.tech;
        var resourceEnabled = this.resourcesEnableList[resourceId];
        return resourceEnabled !== undefined && resourceEnabled !== true;
    }
    
    /**
     * Vrátí aktuální stav provideru.
     * @returns objekt obsahující aktuální stav
     */
    ns.FilterManager.prototype.getState = function() {
        var state = {};

        state.resources = jQuery.extend(true, {}, this.resourcesEnableList);
        state.custom = jQuery.extend(true, {}, this.customFiltersEnableList);
        return state;
    }
    
    /**
     * Aplikuje daný stav filtrů.
     * @param newState stav k nastavení
     */
    ns.FilterManager.prototype.applyState = function(newState) {
        if (!this._isSameState(newState.resources, this.resourcesEnableList) || 
            !this._isSameState(newState.custom, this.customFiltersEnableList)) {
            this.setFormFromState(newState);
            this.loadFromForm();
        }
    }
    
    /**
     * Nastaví formulář filtrů ze stavu.
     * @param newState stav k nastavení
     */
    ns.FilterManager.prototype.setFormFromState = function(newState) {
        for (var i in newState.resources) {
            if (newState.resources.hasOwnProperty(i)) {
                $('#resource_' + this._normalizeHtmlId(i)).prop('checked', newState.resources[i]);
            }
        }
        
        for (var i in newState.custom) {
            if (newState.custom.hasOwnProperty(i)) {
                $('#filter_' + this._normalizeHtmlId(i)).prop('checked', newState.custom[i]);
            }
        }
    }
    
    ns.FilterManager.prototype._normalizeHtmlId = function(id) {
        return id.replace(/[^A-Za-z0-9]/g, "_");
    }
    
    /**
     * @param graph hlavní instance graphu
     */
    ns.FilterManager.prototype.setGraph = function(graph) {
        this.graph = graph;
    }
    
    /**
     * Porovná dvě nastavení.
     * @param state1 nastavení k porovnání číslo 1
     * @param state2 nastavení k porovnání číslo 2
     * @return true, jestliže jsou nastavení totožná
     */
    ns.FilterManager.prototype._isSameState = function(state1, state2) {
        if (state1.length === state2.length) {
            for (var i in state1) {
                if (!state1.hasOwnProperty(i)) {
                    continue;
                }
                if (state1[i] !== state2[i]) {
                    return false;
                }
            }
            
            return true;
        } else {
            return false;
        }        
    }
    
    /**
     * Zjistí jestli je nastavení custom filtrů stejne jako aktuální.
     * @param otherSettings porovnávaný seznam filtrů
     * @returns True, jestliže jsou custom filtry stejné
     */
    ns.FilterManager.prototype._isCustomFiltersSame = function(otherSettings) {
        if (this.customFiltersEnableList.length === otherSettings.length) {
            for (var i in this.customFiltersEnableList) {
                if (this.customFiltersEnableList[i] !== otherSettings[i]) {
                    return false;
                }
            }
            
            return true;
        } else {
            return false;
        }
    }
    
    
}(Manta.FilterSettings = Manta.FilterSettings || {}, jQuery));
