/**************************************************************************
* @namespace    Manta.Server
* @author       Martin Podloucky
* 
* Rozhrani pro komunikaci se serverem.
* 
**************************************************************************/

(function (ns) {

	/*********************************************************************
	 * Objekt pro posilani dotazu na serveru.
	 * 
	 * @param url	adresa serveru (String)
	 */
	ns.Connection = function (url) {
		this.url = url;					// adresa serveru
		this.lastRequest = null;		// naposledy odeslany request
		this.receivers = {};			// mapa reakci na odpovedi ze 
										// serveru
		this.responseListener = null;	// reakce na vsechny odpovedi
	};

	/**
	 * Zaregistruje funkci, ktera se zavola ve chvili, kdy prijde odpoved daneho
	 * typu ze serveru.
	 * 
	 * @param name		typ odpovedi, na kterou se ma reagovat (String)
	 * @param receiver	funkce s jednim parametrem, do ktereho se vlozi response
	 * 					ze serveru.
	 * 
	 * @see Connection.receiveResponse
	 */
	ns.Connection.prototype.registerReceiver = function(name, receiver) {
		this.receivers[name] = receiver;
	};

	/**
	 * Zaregistruje funkci, ktera se zavola vzdy, kdyz prijde odpoved ze 
	 * serveru bez ohledu na typ odpovedi.
	 *
	 * @param listener	funkce se dvema parametry, do kterech se vlozi po
	 * 					rade pouvodni dotaz a odpoved ze serveru.
	 * 
	 * @see Connection.receiveResponse
	 */
	ns.Connection.prototype.registerResponseListener = function(listener) {
		this.responseListener = listener;
	};

    /**
     * Odesle request na server.
     *
     * @param request    request (Object)
     * @param type        typ requestu (String)
     * @param receiver nazev operace, ktera se stara o prevzeti odpovedi, pokud neni specifikovan,
     * pouzije se defaultni = shodny s nazvem requestu
     * @param callback funkce, ktera se zavola po zpracovani odpovedi prikazem
     */
	ns.Connection.prototype.sendRequest = function(request, type, receiver, callback) {
		var req = new XMLHttpRequest();
		var self = this;
		var content = JSON.stringify(request);
        var response;

		req.onreadystatechange = function() {
			if (req.readyState == 4) {
				if (req.responseText.indexOf("<!DOCTYPE") != -1) {
					location.reload();
				} else if (req.status == 200 || window.location.href.indexOf("http") == -1) {
					response = JSON.parse(req.responseText);
                    self.receiveResponse(response, receiver, callback);
				}
				else {
			        if (self.responseListener) {
			            // Chyba
			            self.responseListener(request, {});
			        }
				}
			}
		};

		req.open("POST", this.url + type, true);
		req.setRequestHeader("Content-type", "application/json");
		req.setRequestHeader("Accept", "application/json");
		req.send(content);

		this.lastRequest = request;
	};

    /**
     * Reaguje na odpoved ze serveru.
     *
     * @param response    prichozi odpoved
     * @param receiver nazev operace, ktera se stara o prevzeti odpovedi, pokud neni specifikovan,
     * pouzije se defaultni = shodny s nazvem requestu
     * @param callback funkce ktera se zavola po zpracovani odpovedi
     */
	ns.Connection.prototype.receiveResponse = function(response, receiver, callback) {
		var func;
		
		if (this.responseListener)
			this.responseListener(this.lastRequest, response);

		func = this.receivers[receiver ? receiver : response.type];
		if (func) {
            func(response, callback);
        }
	};

}(Manta.Server = Manta.Server || {}));
