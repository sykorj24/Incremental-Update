/**************************************************************************
* @namespace Manta.Operations
* @author    onouza
*
* Operace filter.
*
**************************************************************************/

(function (ns) {

    //<editor-fold desc="FILTER">
    ns.Filter = function (context) {
        ns.BaseOperation.call(this, ns.Filter.NAME, context);
    };

    ns.Filter.prototype = Object.create(ns.BaseOperation.prototype);
    ns.Filter.NAME = 'filter';

    /**
     * Provede operaci filter. Odesle dotaz na server. Na odpoved pak reaguje funkce
     * receiveResponse.
     *
     * @param filterManager správce filtrů
     */
    ns.Filter.prototype.invoke = function () {
        this.resetGraph();
        var allNodes = this.context.graph.getAllNodes();
        var nodesToFilter=[];
        // nalez neviditelny uzly
        for(var i = 0; i < allNodes.length; i++) {
            var node = allNodes[i];
            if(!node.shadow && node.isFiltered()) {
                nodesToFilter.push(node);
            }
        }

        // Najdu uzly ktere filtruju a jeste z nich provedu folow
        var nodeIdsToFollow=[];
        for(var i = 0; i < nodesToFilter.length; i++) {
            var node = nodesToFilter[i];
            if(node.children.length === 0 && node.status === Manta.Semantics.Node.NodeType.VISITED) {
                // uzel je list a je ve spravnem stavu pro follow
                nodeIdsToFollow.pushOnce(node.id);
            }
        }

        // pokud je nutne z nejakeho uzlu jeste delat follow, provedu ho (jede se asynchronne pres server)
        var unfilter = Window.operationContext.getOperation(Manta.Operations.Unfilter.NAME)
        if(nodeIdsToFollow.length > 0) {
            var follow = Window.operationContext.getOperation(Manta.Operations.Follow.NAME);
            var self = this;
            var filterManager = this.context.graph.filterManager;
            follow.invoke(nodeIdsToFollow, Manta.Utils.getNumericLevelValue(Manta.Globals.formData.level) || 2, 1, filterManager.getActiveFilters(), function() {
                // callback pro follow funkci
                self._filterNodes(nodesToFilter);
                unfilter.invoke(false);
            });
        } else {
            this._filterNodes(nodesToFilter);
            unfilter.invoke(false);
        }
    };
    
    /**
     * Provede operaci filter na jeden uzel.
     *
     * @param nodeId ID node k odfiltorvání
     * @param activeFilters ID aktivovanych filtru
     */
    ns.Filter.prototype.invokeOnNode = function (nodeId) {
        var self = this;
        var follow = Window.operationContext.getOperation(Manta.Operations.Follow.NAME);

        var node = this.context.graph.getNode(nodeId);
        var nodesToFilterIds = node.getLeavesId(); 
        var nodesToFilter = [];
        for (var i = 0; i < nodesToFilterIds.length; i++) {
            nodesToFilter.push(this.context.graph.getNode(nodesToFilterIds[i]));
        }
        
        // z mnoziny uzlu, ktere budu filtrovat odeberu startvni uzly, tak, aby mi vzdycky zustaly viditelne
        if(!(Manta.PresentationManager.getGlobalSettings('showFilters', true))) {
            this._removeStartingNodes(nodesToFilter);
        }
        if (nodesToFilter.length == 0) {
            return;
        }
        
        // Najdu uzly ktere filtruju a jeste z nich provedu folow
        var nodeIdsToFollow = [];
        for(i=0; i < nodesToFilter.length; i++) {
            node = nodesToFilter[i];
            if(node.children.length === 0 && node.status === Manta.Semantics.Node.NodeType.VISITED) {
                // uzel je list a je ve spravnem stavu pro follow
                nodeIdsToFollow.pushOnce(node.id);
            }
        }
        
        // pokud je nutne z nekterych uzlu jeste delat follow, provedu ho (jede se asynchronne pres server)
        if(nodeIdsToFollow.length > 0) {
            var filterManager = this.context.graph.filterManager;
            follow.invoke(nodeIdsToFollow, Manta.Utils.getNumericLevelValue(Manta.Globals.formData.level) || 2, 1, filterManager.getActiveFilters(), function() {
                // callback pro follow funkci
                self._applicateFilter(nodesToFilter);
            });
        } else {
            this._applicateFilter(nodesToFilter);
        }
    };
    
    ns.Filter.prototype._applicateFilter = function (nodesToFilter) {
        this._contractNodes(nodesToFilter);
        // prekreslit   
        this.resetGraph();
        this.context.graph.updateAggregatedEdges();
        this.collapseNodes();
        this.update(true);
        this.finish();
    }

    /**
     * Zkontrahuje predane uzly
     * @param nodesToFilter
     * @private
     */
    ns.Filter.prototype._contractNodes = function (nodesToFilter) {
        if(nodesToFilter.length > 0) {
            // je co filtrovat...
            for(var i = 0; i < nodesToFilter.length; i++) {
                this.context.graph.manuallyContractNode(nodesToFilter[i]);
            }
        }
    };
    
    /**
     * Vyfiltruje predane uzly
     * @param nodesToFilter
     * @private
     */
    ns.Filter.prototype._filterNodes = function (nodesToFilter) {
        var i;
        if(nodesToFilter.length > 0) {
            // je co filtrovat...
            for(i=0; i < nodesToFilter.length; i++) {
                this.context.graph.filterNode(nodesToFilter[i]);
            }
        }
    };

    //</editor-fold>

    //<editor-fold desc="UNFILTER">

    /**
     * Trida se stara o operaci unfilter na seznamu hran - bud se
     * vola unfilter na jedne hrane pomoci kontextoveho menu, nebo obecne
     * unfilter nad celym grafem.
     * @param context
     * @constructor
     */
    ns.Unfilter = function (context) {
        ns.BaseOperation.call(this, ns.Unfilter.NAME, context);
    };

    ns.Unfilter.prototype = Object.create(ns.BaseOperation.prototype);
    ns.Unfilter.NAME = 'unfilter';

    /**
     * Zavola prikaz unfilter.
     * @param edges seznam hran, ktere se maji unfiltrovat = agregovane hrany na nichz
     * jsou nejake proxy uzly
     * @param force
     * @param edges
     */
    ns.Unfilter.prototype.invoke = function (manual, edges) {
        var self = this,
            i,
            allNodes = [],
            componentIds,
            follow = Window.operationContext.getOperation(Manta.Operations.Follow.NAME);

        var centerTargetId;
        
        if(edges != null) {
            for(i=0; i < edges.length; i++) {
                allNodes.append(this._getNodesToUnfilter(edges[i]));
            }
            // centruju na startovni uzel odfiltrovavane hrany
            if(edges[0]) {
                centerTargetId = edges[0].source.id;                
            }
        } else {
            allNodes = this.context.graph.getAllNodes();
            // centruju na prvni startovni uzel
            centerTargetId = Manta.Globals.formData.selectedItems[0];
        }

        this.resetGraph();
        componentIds = this._getNodesToUnfilterIds(allNodes, manual);    
                
        if(componentIds.length > 0) {
            var filterManager = this.context.graph.filterManager;
            var unfilterNode = function(node) {
                manual ? self.context.graph.manuallyUnfilterNode(node) : self.context.graph.unfilterNode(node);
            }
            // v prvni fazi unfiltrujeme uzly na hranach
            for (var i = 0; i < componentIds.length; i++) {
                var nodeToUnfilter = self.context.graph.nodeMap[componentIds[i]];
                unfilterNode(nodeToUnfilter);
            }
            // follow na unfiltrovanych uzlech musime vzdy provest do maximalni hloubky,
            // abychom byli schopni zjistit sourozence atomickych uzlu
            follow.setNodeToCenterOnId(centerTargetId);
            follow.invoke(componentIds, Manta.Utils.getNumericLevelValue("BOTTOM"), 0, [], function() {
                // ve druhe fazi unfiltrujeme sourozence atomickych uzlu unfiltrovanych ve fazi prvni
                var siblingsLeaves = [];
                for (var i = 0; i < componentIds.length; i++) {
                    var nodeToExamine = self.context.graph.nodeMap[componentIds[i]];
                    if (nodeToExamine.atomic && nodeToExamine.parent) {
                        siblingsLeaves = Manta.Tools.getLeafNodes(nodeToExamine.parent);
                        for (var j = 0; j < siblingsLeaves.length; j++) {
                            unfilterNode(siblingsLeaves[j]);
                        }
                    }
                }
                // ve treti fazi sbalime uzel na uroven zadanou ve vstupnim formulari 
                var level = Manta.Utils.getNumericLevelValue(Manta.Globals.formData.level);
                if (Manta.Utils.getNumericLevelValue("BOTTOM") > level) {
                    for (var i = 0; i < componentIds.length; i++) {
                        Manta.Tools.collapseNodeToLevel(self.context.graph.nodeMap[componentIds[i]], level);
                    }
                }
                // v zaverecne fazi provedeme potrebna zacisteni
                self.resetGraph();
                self.context.graph.updateAggregatedEdges();
                self.collapseNodes();
                self.update(true);
                self.finish();
            });
        } else {
            this.setNodeToCenterOnId(centerTargetId);
            // vzdycky volam update a finish, i kdyz nic neunfiltruju - minimalne musim uklidit po filteru
            this.context.graph.updateAggregatedEdges();
            this.collapseNodes();
            this.update(true);
            this.finish();
        }
    };

    /**
     * Posbira rekurzivne vsechny id uzlu, ktere maji byt doplneny do hran.
     * @param edge hrana na ktere sbiram id uzlu
     * @private
     */
    ns.Unfilter.prototype._getNodesToUnfilter = function (edge) {
        var i,
            nodesToUnfilter = [];

        if(edge.children.length > 0) {
            // hrana ma potomky, teprve ti mohou byt agregovane hrany
            for(i=0; i < edge.children.length; i++) {
                nodesToUnfilter.append(this._getNodesToUnfilter(edge.children[i]));
            }
        } else if(edge.aggregated){
            // hrana je konecne agregovana hrana, zajimaji me jeji proxy
            for(i=0; i < edge.nodesOnPath.length; i++) {
                // Kazdy uzel na ceste krome prvniho a posledniho (proto length - 1)
                nodesToUnfilter.push(edge.nodesOnPath[i]);
            }
        }
        return nodesToUnfilter;
    };

    /**
     * Projde vsechny predane uzly, a nalezne ty, ktere maji byt unfiltrovany, tzn. ze splnuji vsechny
     * podminky definovane metodou _canUnfilter
     * @param nodes mnozina uzlu, ze ktere se maji vybrat uzly k unfiltrovani
     * @param manual
     * @returns {Array}
     * @private
     */
    ns.Unfilter.prototype._getNodesToUnfilterIds = function (nodes, manual) {
        var i,
            node,
            ids = [];

        for(i=0; i < nodes.length; i++) {
            node = nodes[i];
            
            // najdu nevyfiltrovane technologie a listy, ktere tomu odpovidaji
            if(this._canUnfilter(node, manual)) {
                // i kdyz uz uzel mam nacten, vzdycky si reknu na server znova
                // aby data byly vzdy plne konzistentni - jinak bych castecne musel
                // tady duplikovat serverovvou logiku se sourozencema a tak.
                ids.pushOnce(node.id);
            }
        }
        return ids;
    };

    /**
     * Rozhodne zda se ma uzel unfiltrovat: technologie uzlu musi byt povolena, uzel musi byt listem a
     * pokud to je pozadovano parametrem unfilterUnvisited(=false), tak musi byt v navstivenem stavu
     * (VISITED nebo FINISHED)
     *
     * @param node uzel, o kterem se rozhoduje
     * @param manual
     * @returns {boolean}
     * @private
     */
    ns.Unfilter.prototype._canUnfilter = function (node, manual) {
        return  node.children.length === 0 &&
                (
                    manual || 
                    (
                        !node.isFiltered() &&
                        (
                            node.status === Manta.Semantics.Node.NodeType.VISITED ||
                            node.status === Manta.Semantics.Node.NodeType.FINISHED
                        )
                    )
                );
    };

    //</editor-fold>
}(Manta.Operations = Manta.Operations || {}));