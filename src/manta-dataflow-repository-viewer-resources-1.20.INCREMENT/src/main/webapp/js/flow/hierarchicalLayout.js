/***********************************************************************************************************************
 * @namespace Manta.Layout
 * @author Matyas Krutsky
 * 
 * Hierarchicky layout.Drzi se Sugiyama frameworku, ktery rozdeluje algoritmus layoutu na 4 faze, ktere jsou na sobe
 * nezavisle a kazdou je mozne implementovat rozdilnym zpusobem.
 * 
 * 1. faze - cycle removal Implementace vychazi z kapitoly 13.2.4 - Heuristics Based on Cycle Breaking, konkretne pak z
 * clanku "A Technique for Drawing Directed Graphs" (Gansner et al) (http://www.graphviz.org/Documentation/TSE93.pdf)
 * 
 * 2. faze - layer assignment Implementace vychazi z longest path layering heuristiky + je doplnena o posouvani uzlu do
 * co nejvyssi (nejvic vpravo) vrstvy to jde (popis napr. http://cs.brown.edu/~rt/gdhandbook/chapters/hierarchical.pdf)
 * 
 * 3. faze - crossing minimization Zde se vyuziva heuristiky zalozene na fixnim poctu layer-by-layer sweep s medianem
 * pozic ze sousedni vrstvy (popis napr. http://cs.brown.edu/~rt/gdhandbook/chapters/hierarchical.pdf)
 * 
 * 4. faze - coordinates assignment Je implemntovana podle clanku "Fast and Simple Horizontal Coordinate Assignment"
 * (Brands, Kopf), pouze s otocenym smerem - nikoliv shora dolu, ale zleva doprava a zaroven je potreba zohlednit
 * rozdilne velikosti jednotlivych uzlu a jejich hierarchii
 * (http://www.inf.uni-konstanz.de/algo/publications/bk-fshca-01.pdf)
 * 
 * Obecne o hierarchickem layoutu: http://cs.brown.edu/~rt/gdhandbook/chapters/hierarchical.pdf
 * 
 **********************************************************************************************************************/
Manta = Manta || {};
(function(ns, $, undefined) {

    var CycleBreaking;
    var LayerAssignment;
    var Layer;
    var Vertex;
    var Segment;
    var CrossReduction;
    var CoordinateAssignment;
    var NodePlacement;
    var EdgeRouting;
    // parametry vykresleni
    var NODE_WIDTH = 300;
    var NODE_HEIGHT = 27;
    var LAYER_GAP = 170;
    var VSPACE = 5;
    var TOP = 200;
    var START_LEFT = 100;
    var START_TOP = 200;
    var EDGE_PADDING = 40;
    var PADDING = 2;
    var EDGE_DELTA = 2;
    var PARENT_PADDING = 10;
    var EDGE_IN_OUT_SEPARATION = 6;
    var LOOP_SEPARATION = 3;
    var ORDER_ATTRIBUTE = "ORDER";
    var MAXIMUM_VERTICAL_SPACE = 50;

    var AtomicCrossReduction;

    var verticesSort = function(v1, v2) {
        return v1.position - v2.position;
    };

    ns.Hierarchical = function(graph) {
        ns.Base.call(this, graph);
        this.contracted = {};
        this.nodes = graph.getAllNodes().filter(ns.Hierarchical._nodeFilter, this);
        this.cycleBreaking = new CycleBreaking(this.nodes);
        this.layerAssignment = new LayerAssignment(this.nodes);
        this.crossReduction = new CrossReduction();
        this.coordinatesAssignment = new CoordinateAssignment(this);
        this.nodePlacement = new NodePlacement(this);
        this.edgeRouting = new EdgeRouting();
        this.nodeSplitter = new NodeSplitter(this);
        this.layers = [];
        this.lastOrder = {};
        this.nodeMap = {}; // mapa uzlu, ktere se layoutuji pro usnadneni
        // vyhledavani
        this.forgetState = false; // pokud flag je nastaven na true, povazuji
        // se vsechny predchozi hodnoty
        // za neplatne a layout se musi vytvaret cely odznovu
    };

    ns.Hierarchical.prototype = Object.create(ns.Base.prototype);

    /**
     * Zavola vsechny faze layoutu nad zadanym grafem.
     * 
     * @param divideNodes zda se ma prepocitavat rozdeleni uzlu, nebo se pouzije uz vypocitane rozdeleni do vrstev
     */
    ns.Hierarchical.prototype.layout = function(divideNodes) {
        var start = new Date().getTime();
        this.contracted = {};
        this.nodeMap = {};
        this.nodes = this.getLayoutedNodes();

        if (this.nodes.length === 0) {
            console.log("Hierarchical layout: No visible nodes found!");
            return;
        }

        // PREPROCESSING
        this._preProcess();
        this._assignBounds();

        // LAYOUT EXECUTION
        // Vycistime a zapneme cache vyfiltrovanosti uzlu kvuli optimalizaci
        Manta.Cache.NODE_FILTERING_CACHE.clean();
        Manta.Cache.NODE_FILTERING_CACHE.turnOn();
        this.cycleBreaking = new CycleBreaking(this.nodes);
        this.cycleBreaking.execute();
        this.layerAssignment.nodes = this.nodes;
        var shouldRedraw = divideNodes || this.forgetState;
        if (shouldRedraw) {
            // prepocitam rozdeleni do vrstev
            this.layers = this.layerAssignment.execute();
            this.crossReduction.execute(this.layers, false, CrossReduction.ITERATIONS);
        } else {
            // pouzivam posledni vypocitane deleni do vrstev
            this.layers = this.layerAssignment.applyLastLayers();
            // pokud pouzivam posledni vrstvy, chci zaroven i zachovat poradi
            // uzlu ve vrstve
            this.crossReduction.applyLastOrder(this.layers, this.lastOrder);
        }

        // pokud se obratily nebo zaslepily nejake hrany pro odstraneni cyklu, nyni je vratim do puvodniho stavu
        this.cycleBreaking.unblind();
        this.cycleBreaking.reverseBack();
        // Vypneme cache vyfiltrovanosti uzlu
        Manta.Cache.NODE_FILTERING_CACHE.turnOff();
        
        this.coordinatesAssignment.execute(this.layers);
                
        // Spocteni umisteni uzlu a hran a vertikalni rozdeleni
        this.nodePlacement.doPhase1(this.nodes, this.layers, shouldRedraw);
        
        // horizontalní rozseknuti uzlu s mezerami
        if (shouldRedraw) {
            this.nodeSplitter.execute(this.layers);
            this.coordinatesAssignment.execute(this.layers);
        }

        // prepocitani souradnic a vytvoreni zalamovacich bodu v hranach
        this.nodePlacement.doPhase2(this.nodes, this.layers, shouldRedraw);
        
        this.edgeRouting._routeEdges(this.layers);

        this._saveLastOrder();
        this.forgetState = false;
        console.log('Layout execution time: ' + (new Date().getTime() - start) / 1000 + ' s');
    };
    
    /**
     * Priznak, ze layout ma zapomenout vsechny vypocitane hodnoty a nikdy je tak znovu neaplikovat, jelikoz uz z
     * nejakeho duvodu nejsou validni
     * 
     * @param value
     */
    ns.Hierarchical.prototype.setForgetState = function(value) {
        this.forgetState = value;
    };

    /**
     * @returns {Array.<Node>} Vrati uzly, ktere se maji layoutovat, to znamena jsou viditelne, posledni neatomicke,
     *          nebo sbalene
     */
    ns.Hierarchical.prototype.getLayoutedNodes = function() {
        return this.graph.getAllNodes().filter(ns.Hierarchical._nodeFilter, this);
    };

    /**
     * Filtr, ktery vybere uzly, ktere se maji layoutovat (hierarchie uzlu se pak sklada okolo nich). Uzel musi byt:
     * <ul>
     * <li>viditelny</li>
     * <li>nejvyse v hierarchii, nebo jeho rodic neni posledni nedelitelny uzel (napr. tabulka)</li>
     * <li>sbaleny, nebo posledni nedelitelny stupen</li>
     * </ul>
     * 
     * @param node
     * @returns {boolean}
     * @private
     */
    ns.Hierarchical._nodeFilter = function(node) {
        return node.isVisible()
                && (node.parent === null || !node.parent.isHighestUnsplitableLevel() || node.parent.shadow)
                && (!node.expanded || (node.isHighestUnsplitableLevel() && !node.shadow));
    };

    /**
     * Inicialni setrideni hran pro konstantni vysledky v ruznych prohlizecich a ruznem poradi vstupu
     * 
     * @param e1
     * @param e2
     * @param end
     * @returns {number}
     * @private
     */
    ns.Hierarchical.prototype._edgeSort = function(e1, e2, end) {
        var deg1, deg2;
        deg1 = e1[end].edgesIn.length + e1[end].edgesOut.length;
        deg2 = e2[end].edgesIn.length + e2[end].edgesOut.length;
        if (deg1 === deg2) {
            return e1[end].id - e2[end].id;
        } else {
            return deg1 - deg2;
        }
    };

    // <editor-fold desc="PREPROCESS">

    /**
     * Pred spustenim layoutu pretridim hrany podle stupne jejich koncu a pripadne id tak, aby poradi jejich vstupu bylo
     * vzdy stejne - tim bude vzdy stejny layout (pri jinem poradi se heuristiky mohou dobrat jineho vysledku a stejny
     * graf by se tak mohl vykreslit ruzne, coz nechceme).
     * 
     * Zaroven vycistim bendpointy hran pokud uz nejake jsou.
     * 
     * @private
     */
    ns.Hierarchical.prototype._preProcess = function() {
        var i, j, node, self = this, length, nodeSort = function(n1, n2) {
            return n1.id - n2.id;
        };

        for (i = 0; i < this.nodes.length; i++) {
            node = this.nodes[i];
            this.nodeMap[node.id] = node;
            if (node.hasChildren() && node.expanded) {
                node.collapse();
                this.contracted[node.id] = node;
            }
            node.edgesIn.sort(function(e1, e2) {
                return self._edgeSort(e1, e2, 'source');
            });
            node.edgesOut.sort(function(e1, e2) {
                return self._edgeSort(e1, e2, 'target');
            });
            length = node.edgesOut.length;
            for (j = 0; j < length; j++) {
                this._clearBendpoints(node.edgesOut[j]);
            }
        }
        this.nodes.sort(nodeSort);
    };

    ns.Hierarchical.prototype._assignBounds = function() {
        var i, node, height, nodes = this.nodes;

        for (i = 0; i < nodes.length; i++) {
            node = nodes[i];
            height = NODE_HEIGHT;
            if (node.id in this.contracted) {
                // velikost je urcena potomky
                height += node.getUnfilteredChildren().length * (NODE_HEIGHT + PADDING);
            }
            nodes[i].bounds = new Manta.Geometry.Rectangle(0, 0, NODE_WIDTH, height);
        }
    };

    ns.Hierarchical.prototype._clearBendpoints = function(edge) {
        var i;
        edge.bendpoints = [];
        for (i = 0; i < edge.children.length; i++) {
            edge.children[i].bendpoints = [];
        }
    };

    ns.Hierarchical.prototype._saveLastOrder = function() {
        var i, j, iLen = this.layers.length, jLen, vertex, parent;

        for (i = 0; i < iLen; i++) {
            for (j = 0, jLen = this.layers[i].size(); j < jLen; j++) {
                vertex = this.layers[i].vertices[j];
                this.lastOrder[vertex.id] = vertex.position;
                if (!vertex.isDummy) {
                    parent = vertex.node.parent;
                    while (parent) {
                        this.lastOrder[parent.id] = vertex.position;
                        parent = parent.parent;
                    }
                }
            }
        }
    };

    // </editor-fold>

    // <editor-fold desc="NODE SPLITTER">

    /**
     * Třída pro rozdělování předků s příliš velkou mezerou.
     */
    NodeSplitter = function(layout) {
        this.layout = layout;
    };

    /**
     * Provede vertikální rozdělení předků s mezerou.
     * @param layers vrstvy s vypočtenými souřadnicemi
     */
    NodeSplitter.prototype.execute = function(layers) {
        for (var layerIndex = 0; layerIndex < layers.length; layerIndex++) {
            var layer = layers[layerIndex];

            var previousBottom = undefined;
            var previousVertex;

            var shadowsBuilder = new NodeSplitterShadowBuilder();
            var nodesInLayer = layer.vertices.length;
            for (var vertexIndex = 0; vertexIndex < layer.vertices.length; vertexIndex++) {
                var vertex = layer.vertices[vertexIndex];
                if (vertex.isDummy) {
                    continue;
                }

                var currentTop = this._totalNodeY(vertex);

                var space = 0;
                var topPoint = (currentTop - vertex.node.getDepth() * (NODE_HEIGHT + PARENT_PADDING)) - vertex.node.bounds.height / 2;
                if (previousBottom != undefined) {                    
                    space = topPoint - previousBottom;
                }

                if (previousVertex == null) {
                    shadowsBuilder.startBuilding([ vertex.node ], vertex.node.getRoot());
                } else if (space > MAXIMUM_VERTICAL_SPACE) {
                    if (shadowsBuilder.isBuilding()) {
                        shadowsBuilder.buildShadows();
                    }
                    if (this._sameOwnerRoot(vertex.node, previousVertex.node)) {
                        shadowsBuilder.startBuilding([ vertex.node ], vertex.node.getRoot());
                    }
                } else if (shadowsBuilder.isBuilding()) {
                    if (shadowsBuilder.belongsToCurrentBuild(vertex.node)) {
                        shadowsBuilder.add(vertex.node);
                    } else {
                        shadowsBuilder.buildShadows();
                    }
                } else if (previousVertex != null && this._sameRoot(vertex.node, previousVertex.node)) {
                    shadowsBuilder.startBuilding([ previousVertex.node, vertex.node ], vertex.node.getRoot());
                } 

                previousVertex = vertex;
                previousBottom = currentTop + vertex.node.bounds.height / 2 + vertex.node.getDepth() * PARENT_PADDING;
            }

            if (shadowsBuilder.isBuilding()) {
                shadowsBuilder.buildShadows(nodesInLayer);
            }
        }
    };

    /**
     * Zjistí absolutní y souřadnici uzlu.
     * @param node uzel ke sjištění
     * @returns absolutní y souřadnice
     */
    NodeSplitter.prototype._totalNodeY = function(vertex) {
        return vertex.node.getAbsoluteBounds().y;
    };

    /**
     * True, jestliže mají uzly stejný root.
     */
    NodeSplitter.prototype._sameRoot = function(node1, node2) {
        return node1.getRoot() == node2.getRoot();
    };

    /**
     * True, jestliže je stejný owner rootů uzlů.
     */
    NodeSplitter.prototype._sameOwnerRoot = function(node1, node2) {
        return node1.getRoot().owner == node2.getRoot().owner;
    }

    // </editor-fold>

    // <editor-fold desc="NODE PLACEMENT">

    /**
     * Builder shadow uzlů v rámci jednoho společného předka.
     */
    NodeSplitterShadowBuilder = function() {
        this.group = [];
        this.root;
        this.isBuildingShadowGroup = false;
    };

    /**
     * Vrací, jestli se momentálně sbírají uzly pro nový shadow.
     * @returns true, jestliže se sbírají uzly pro nový shadow
     */
    NodeSplitterShadowBuilder.prototype.isBuilding = function() {
        return this.isBuildingShadowGroup;
    };

    /**
     * Začne sběr uzlů pro nový shadow.
     * @param nodes počáteční uzly pro nový shadow
     * @param commonAncestor společný předek sbíraných uzlů, z nějž se vytvoří shadow
     */
    NodeSplitterShadowBuilder.prototype.startBuilding = function(nodes, root) {
        this.isBuildingShadowGroup = true;
        this.root = root;
        this.group = nodes.slice();
    };

    /**
     * Zapamatuje si daný uzel, že se má patřit do nového shadow.
     * @param node přidávaný uzel
     */
    NodeSplitterShadowBuilder.prototype.add = function(node) {
        this.group.push(node);
    };

    /**
     * Ověří jestli daný uzel má společného předka jako je současná stavba shadows.
     * @param node ověřovaný uzel
     * @returns true, jestliže patří do současné stavby shadows
     */
    NodeSplitterShadowBuilder.prototype.belongsToCurrentBuild = function(node) {
        return node.isDescendantOf(this.root);
    };

    /**
     * Vytvoří shadow uzly pro současné nastavení.
     * @param pocet uzlů ve vrstvě
     */
    NodeSplitterShadowBuilder.prototype.buildShadows = function(nodesInLayer) {
        if (this.group.length != nodesInLayer) {
            this.isBuildingShadowGroup = false;
            this._buildShadowsInner(this.group);
        }
    };

    /**
     * Vytvoří shadow uzly pro dané uzly
     * @param nodesForNewShadow množina uzlů pro nový shadow
     */
    NodeSplitterShadowBuilder.prototype._buildShadowsInner = function(nodesForNewShadow) {
        if (nodesForNewShadow == null || nodesForNewShadow.length == 0) {
            throw new Error("Set must not be empty.");
        } else if (nodesForNewShadow.length == 1) {
            this._buildShadowsOne(nodesForNewShadow[0], this.root);
        } else {
            var splitRoot = this._buildSplitTree(nodesForNewShadow);
            var newNode = this._processSplitTree(splitRoot);
        }
    };

    /**
     * Vytvoří tzv split strom, který reprezentuje podstrom reality, pro kterou se staví shadow.
     * @param nodesForNewShadow množina nedělitelných uzlů, pro který se staví nový shadow podstrom
     * @returns kořen postaveného split stromu
     */
    NodeSplitterShadowBuilder.prototype._buildSplitTree = function(nodesForNewShadow) {
        var splitNodeSet = [];
        for (var i = 0; i < nodesForNewShadow.length; i++) {
            var node = nodesForNewShadow[i];
            this._buildSplitNode(node, null, splitNodeSet);
        }
        var rootNode = nodesForNewShadow[0].getRoot();
        return splitNodeSet[rootNode.id];
    };

    /**
     * Vytvoří split uzel reprezentující daný uzel.
     * Předpokládá se stavba zdola.
     * @param node uzel k reprezentaci
     * @param child případný potomek, který vyvolal stavby
     * @param splitNodeSet množina již vytvořených split uzlů indexovaných idečkama uzlů
     */
    NodeSplitterShadowBuilder.prototype._buildSplitNode = function(node, child, splitNodeSet) {
        if (node == null) {
            return;
        }
        if (splitNodeSet[node.id] != undefined) {
            if (child != null) {
                splitNodeSet[node.id].children.push(splitNodeSet[child.id]);
            }
            return;
        }

        var splitNode = {};
        splitNode.node = node;
        splitNode.id = node.id; // jen pro lepší debug
        if (child != null) {
            splitNode.children = [ splitNodeSet[child.id] ];
        } else {
            splitNode.children = [];
        }
        splitNodeSet[node.id] = splitNode;

        this._buildSplitNode(node.parent, node, splitNodeSet);
        if (node.parent != null) {
            splitNode.splitParent = splitNodeSet[node.parent.id];
        }
    };

    /**
     * Zpracuje split strom.
     * @param splitNode zpracovávaný split uzel
     * @returns uzel představující podstrom reprezentovaný split uzlem
     */
    NodeSplitterShadowBuilder.prototype._processSplitTree = function(splitNode) {
        var allChildrenNumber = splitNode.node.children.length;
        var childrenNumber = splitNode.children.length;
        if (childrenNumber == 0) {
            return splitNode.node;
        } else {
            var newNodes = [];
            for (var i = 0; i < childrenNumber; i++) {
                var splitChild = splitNode.children[i];
                var newNode = this._processSplitTree(splitChild);
                newNodes.push(newNode);
            }
            
            if (this._shouldCreateShadow(splitNode, newNodes)) {
                var shadow = splitNode.node.owner.createShadowNode(newNodes);
                if (splitNode.splitParent != undefined) {
                    shadow.parent.children.remove(shadow);
                    shadow.setParent(splitNode.node.parent);
                    splitNode.node.parent.children.push(shadow);
                }
                return shadow;
            } else {
                return splitNode.node;
            }
        }
    };
    
    NodeSplitterShadowBuilder.prototype._shouldCreateShadow = function(splitNode, newNodes) {
        if (splitNode.length != newNodes.length) {
            return true;
        }
        
        var currentIds = [];
        for (var i = 0; i < splitNode.node.children.length; i++) {
            currentIds.push(splitNode.node.children[i]);
        }
        
        for (var i = 0; i < newNodes.length; i++) {
            if (currentIds.indexOf(newNodes[i]) == -1) {
                return true;
            }
        }
        
        return false;
    };

    /**
     * Vytvoří šňůru shadow uzlů pro daný uzel až po ancestor. Nevytváří shadow, pokud má předek jen jednoho potomka.
     * @param currentNode uzel, pro který s vytváří šňůra shadow uzlů
     * @param ancestor předek až pod který se vytváří shadow uzly
     * @returns poslední uzel pod předkem, původní nebo shadow
     */
    NodeSplitterShadowBuilder.prototype._buildShadowsOne = function(currentNode, ancestor) {
        while (currentNode.owner != ancestor.owner) {
            currentNode = this._buildShadowsOneStep(currentNode);
        }
        return currentNode;
    };

    /**
     * Pokud předek daného uzlu má více potomků, tak pro vytvoří shadow s daným uzelm jako jediným potomkem.
     * V opačném případě vrátí jen předka.
     * @param currentNode uzel, který se řeší
     * @returns předek, nebo shadow předek
     */
    NodeSplitterShadowBuilder.prototype._buildShadowsOneStep = function(currentNode) {
        if (currentNode.parent.children.length > 1) {
            return currentNode.parent.owner.createShadowNode([ currentNode ]);
        } else {
            return currentNode.parent;
        }
    };

    /**
     * Nalezne první uzel pod ancestor uzlem.
     * @param node uzel, z něhož se podpředek hledá
     * @param ancestor uzel, pro který se první "pod-předek" hledá
     * @returns "pod-předek" pod ancestor uzelem
     */
    NodeSplitterShadowBuilder.prototype._findBelowNode = function(node, ancestor) {
        var currentNode = node;
        while (currentNode != null && currentNode.parent != ancestor) {
            currentNode = currentNode.parent;
        }
        if (currentNode == null) {
            console.log("Node", node, " doesn't have ancestor", ancestor, ".");
        }
        return currentNode;
    };

    /**
     * Nalezne prvního společného předka dvou uzlů.
     * @param node1 první zkoumaný uzel
     * @param node2 druhý zkoumaný uzel
     * @returns první společný předek nebo null
     */
    NodeSplitterShadowBuilder.prototype._getCommonAncestor = function(node1, node2) {
        var parents = [];
        var currentParent = node2.parent;
        while (currentParent != undefined) {
            parents.push(currentParent);
            currentParent = currentParent.parent;
        }
        currentParent = node1.parent;
        while (currentParent != undefined) {
            if (parents.indexOf(currentParent) !== -1) {
                return currentParent;
            }
            currentParent = currentParent.parent;
        }

        return null;
    };

    // </editor-fold>

    // <editor-fold desc="NODE PLACEMENT">

    NodePlacement = function(layout) {
        this.layout = layout;
    };

    /**
     * Spočte pozice uzlu a hran a provede horizontalni rozesknuti společných předků.
     * @param nodes vykreslované uzly
     * @param layers horizontální vrstvy
     * @param divideNodes true, jestliže dochází k přerozdělování uzlů do vrstev 
     */
    NodePlacement.prototype.doPhase1 = function(nodes, layers, divideNodes) {
        this._placeLayers(this.layout, true);
        this._placeChildren(this.layout, true);
        if (divideNodes) {
            this._divideNodesMain(layers);
        }
        this._setAllParentBounds(nodes);
    }

    /**
     * Přepočte pozice uzlů po případném veritkálním rozsekávání a vytvoří bendpointy hranám.
     * @param nodes vykreslované uzly
     * @param layers horizontální vrstvy
     * @param divideNodes true, jestliže dochází k přerozdělování uzlů do vrstev
     */
    NodePlacement.prototype.doPhase2 = function(nodes, layers, divideNodes) {
        this._placeLayers(this.layout, false);
        this._placeChildren(this.layout, false);

        if (divideNodes) {
            this._fixShadowOwnersParents();
            this._removeAllEmptyShadows();
            this._fixShadowOwnersChildren(layers);
        }

        this._setAllParentBounds(nodes);
    }

    NodePlacement.prototype._placeNode = function(node, x, y) {
        node.bounds = new Manta.Geometry.Rectangle(x, y, NODE_WIDTH, node.bounds.height);
    };

    /**
     * Provede horizontální rozdělení předků, kteří jdou přes více vrstev.
     * @param layers horizontální vrstvy
     */
    NodePlacement.prototype._divideNodesMain = function(layers) {
        for (var i = 0; i < layers.length; i++) {
            var roots = [];
            for (var j = 0; j < layers[i].vertices.length; j++) {
                var vertex = layers[i].vertices[j];
                if (!vertex.isDummy) {
                    var root = vertex.node.getRoot();
                    roots.pushOnce(root);
                }
            }
            this._divideNodesInOneLayer(roots, layers[i]);
        }
    }

    /**
     * Prohledá všechny uzly a odstraní jejich shadow varianty, které nemají žádné potomky. V případě, když už nebude
     * mít shadow owner žádné shadow, je i změní na normální uzly.
     */
    NodePlacement.prototype._removeAllEmptyShadows = function() {
        var nodes = this.layout.graph.getAllNodes();
        var deleted = 0;
        for (var nodeIndex = 0; nodeIndex < nodes.length; nodeIndex++) {
            var node = nodes[nodeIndex];
            if (node != undefined && node.shadow && node.children.length == 0) {
                deleted += this._removeEmptyShadow(node);
            }
        }
    };

    /**
     * Provede smazání konkrétního shadow uzlu. Rekurzivně smaže i případné nyní již prázdné shadow potomky
     * @param node shadow uzel ke smazání
     * @returns {Number} počet smazaných shadow uzlů
     */
    NodePlacement.prototype._removeEmptyShadow = function(node) {
        var owner = node.owner;
        owner.shadows.remove(node, Manta.Utils.equalsById);
        if (owner.shadows.length == 0) {
            owner.shadowOwner = false;
            if (owner.parent != null) {
                owner.parent.children.pushOnce(owner);
            }
        }

        var deletedShadows = 1;

        var parent = node.parent;
        if (parent != null) {
            parent.children.remove(node, Manta.Utils.equalsById);
            if (parent.shadow && parent.children.length == 0) {
                deletedShadows += this._removeEmptyShadow(parent);
            }
        }

        delete this.layout.graph.nodeMap[node.id];
        return deletedShadows;
    };

    /**
     * Zkontroluje a případně opraví předky shadow owner uzlů. Takové uzly nesmějí mít jako předka shadow uzel.
     */
    NodePlacement.prototype._fixShadowOwnersParents = function() {
        var nodes = this.layout.graph.getAllNodes();
        for (var nodeIndex = 0; nodeIndex < nodes.length; nodeIndex++) {
            var node = nodes[nodeIndex];
            var currentParent = node.parent;
            if (node.shadowOwner && currentParent != null && currentParent.shadow) {
                node.setParent(currentParent.owner);
                currentParent.owner.children.pushOnce(node);
                currentParent.children.remove(node, Manta.Utils.equalsById);
                // console.log("oprava predka pro shadow owner", node);
            }
        }
    }

    /**
     * Zkontroluje a případně opraví potomky shadow owner uzlů. Shadow owner totiž nesmí mít zobrazitlené potomky,
     * protože se shadow owner uzly během vizualizace ignorují. Takové nalezení předci se vloží do nových shadow uzlů.
     * @param layers horizontální vrstvy
     */
    NodePlacement.prototype._fixShadowOwnersChildren = function(layers) {
        var nodes = this.layout.graph.getAllNodes();
        for (var nodeIndex = 0; nodeIndex < nodes.length; nodeIndex++) {
            var node = nodes[nodeIndex];
            if (node.shadowOwner && node.children.length > 0) {
                var children = node.children.slice();
                var visibleChildren = [];
                for (var childIndex = 0; childIndex < children.length; childIndex++) {
                    var child = children[childIndex];
                    if (!child.isVisible()) {
                        continue;
                    }
                    visibleChildren.push(child);
                }

                if (visibleChildren.length > 0) {
                    for (var layerIndex = 0; layerIndex < layers.length; layerIndex++) {
                        var childrenInLayer = [];
                        for (var vertexIndex = 0; vertexIndex < layers[layerIndex].vertices.length; vertexIndex++) {
                            var vertex = layers[layerIndex].vertices[vertexIndex];
                            if (vertex.isDummy) {
                                continue;
                            }
                            var currentNode = vertex.node;
                            while (currentNode != null) {
                                if (visibleChildren.contains(currentNode)) {
                                    childrenInLayer.push(currentNode);
                                    break;
                                }
                                currentNode = currentNode.parent;
                            }
                        }
                        if (childrenInLayer.length > 0) {
                            var shadow = node.createShadowNode(childrenInLayer);
                        }
                    }
                }
                if (node.children.length > 0) {
                    var visibleChildren = [];
                    for (var i = 0; i < node.children.length; i++) {
                        if (node.children[i].isVisible()) {
                            visibleChildren.push(node.children[i]);
                        }
                    }
                    if (visibleChildren.length > 0) {
                        var shadow = node.createShadowNode(visibleChildren);
                    }
                }
            }
        }
        nodes = this.layout.graph.getAllNodes();
        for (var nodeIndex = 0; nodeIndex < nodes.length; nodeIndex++) {
            var node = nodes[nodeIndex];
            if (node.isVisible() && node.parent != null && node.parent.shadowOwner) {
                node.parent.createShadowNode([ node ]);
                // console.log("presun", node, "do noveho shadow");
            }
        }
    };

    /**
     * Provede horizontální rozdělení uzlů v jedné vrstvě.
     * @param nodes množina root uzlů v dané vrstvě
     * @param layer prohledávaná vrstva
     */
    NodePlacement.prototype._divideNodesInOneLayer = function(nodes, layer) {
        for (var i = 0; i < nodes.length; i++) {
            var parent = nodes[i];
            if (parent.shadowOwner) {
                var shadows = parent.shadows.slice();
                for (var s = 0; s < shadows.length; s++) {
                    this._divideNode(shadows[s], layer);
                }
            }
            this._divideNode(parent, layer);
        }
    };

    /**
     * Provede rozdělení jednoho stromu v rámci dané vrstvy
     * @param parent rozdělovaný předek
     * @param layer zkoumaná vrstva
     */
    NodePlacement.prototype._divideNode = function(parent, layer) {
        if (parent.children.length > 0 && !(parent.id in this.layout.nodeMap)) {
            this._divideNodesInOneLayer(parent.children.slice(), layer);
            var group = [];
            for (var j = 0; j < layer.size(); j++) {
                var vertex = layer.vertices[j];
                if (vertex.isDummy) {
                    continue;
                }

                if (vertex.node.isDescendantOf(parent) && vertex.node !== parent) {
                    var node = vertex.node;
                    while (node !== parent && node.parent !== parent) {
                        node = node.parent;
                    }
                    group.pushOnce(node);
                } else {
                    if (group.length > 0) {
                        parent.owner.createShadowNode(group);
                        group = [];
                    }
                }
            }
            if (group.length > 0) {
                parent.owner.createShadowNode(group);
            }
        }
    }

    NodePlacement.prototype._setAllParentBounds = function(nodes) {
        var i, boundsSet = {};

        for (i = 0; i < nodes.length; i++) {
            boundsSet[nodes[i].id] = true;
        }
        for (i = 0; i < nodes.length; i++) {
            this._setBoundsRecursively(boundsSet, nodes[i].getRoot());
        }
    };

    NodePlacement.prototype._setBoundsRecursively = function(boundsSet, node) {
        if (node.id in boundsSet) {
            return;
        }

        for (var i = 0; i < node.children.length; i++) {
            var child = node.children[i];
            if (child.isVisible() && !(child.id in boundsSet)) {
                this._setBoundsRecursively(boundsSet, child);
            }
        }
        node.bounds = this._getParentBounds(node);
        boundsSet[node.id] = true;
    };

    NodePlacement.prototype._getParentBounds = function(node) {
        var x = Infinity;
        var y = Infinity;
        var height = 0;
        var width = 0;
        var pad = PARENT_PADDING;

        for (var i = 0; i < node.children.length; i++) {
            if (node.children[i].isVisible()) {
                var childBounds = node.children[i].bounds;
                x = Math.min(childBounds.x, x);
                y = Math.min(childBounds.y, y);
            }
        }

        x -= pad;
        y = y - NODE_HEIGHT - pad;

        for (var i = 0; i < node.children.length; i++) {
            node.children[i].bounds.x -= x;
            node.children[i].bounds.y -= y;
            childBounds = node.children[i].bounds;
            width = Math.max(childBounds.x + childBounds.width, width);
            height = Math.max(childBounds.y + childBounds.height, height);
        }
        return new Manta.Geometry.Rectangle(x, y, width + pad, height + pad);
    };

    NodePlacement.prototype._placeChildren = function(layout, onlyCalc) {
        var i, iLen, j, jLen, id, y, node, child, childPos, children, atomicCrossReduction = new AtomicCrossReduction();

        if (!onlyCalc) {
            for (id in layout.contracted) {
                if (layout.contracted.hasOwnProperty(id)) {
                    node = layout.contracted[id];
                    node.expand();
                }
            }

            atomicCrossReduction.execute(layout.layers, layout.nodes);
        }

        for (i = 0, iLen = layout.nodes.length; i < iLen; i++) {
            node = layout.nodes[i];
            y = NODE_HEIGHT;
            children = node.children.slice();
            for (j = 0, jLen = children.length; j < jLen; j++) {
                child = children[j];
                if (child.isVisible()) {
                    child.bounds = new Manta.Geometry.Rectangle(PADDING, y, node.bounds.width - PADDING * 2,
                            NODE_HEIGHT);
                    childPos = child.getAbsoluteBounds().y + child.bounds.height / 2;
                    y += NODE_HEIGHT + PADDING;
                }
            }
        }
    };

    NodePlacement.prototype._placeLayers = function(layout, onlyCalc) {
        var i, iLen, j, jLen, x = START_LEFT, y, vertex, layers = layout.layers;
        for (i = 0, iLen = layers.length; i < iLen; i++) {
            y = START_TOP;

            layers[i].vertices.sort(verticesSort);
            for (j = 0, jLen = layers[i].size(); j < jLen; j++) {
                vertex = layers[i].vertices[j];
                if (vertex.isDummy) {
                    y = vertex.x - vertex.spaceUp + TOP;
                    if (!onlyCalc) {
                        layout.edgeRouting._appendBendpoint(vertex.edge, x - EDGE_PADDING, y);
                        layout.edgeRouting._appendBendpoint(vertex.edge, x + EDGE_PADDING + NODE_WIDTH, y);
                    }
                } else {
                    y = vertex.x - vertex.node.bounds.height / 2 + TOP;
                    this._placeNode(vertex.node, x, y);
                }
            }
            x += NODE_WIDTH + LAYER_GAP;
        }
    };

    // </editor-fold>

    // <editor-fold desc="EDGE ROUTING">

    EdgeRouting = function() {
        this.nodesToCheck = [];
        this.loopGap = {};
    };

    EdgeRouting.prototype._prependBendpoint = function(edge, x, y) {
        var i;
        for (i = 0; i < edge.children.length; i++) {
            edge.children[i].bendpoints.unshift(new Manta.Geometry.Point(x, y));
        }
        edge.bendpoints.unshift(new Manta.Geometry.Point(x, y));
    };

    EdgeRouting.prototype._appendBendpoint = function(edge, x, y) {
        var i;
        for (i = 0; i < edge.children.length; i++) {
            edge.children[i].bendpoints.push(new Manta.Geometry.Point(x, y));
        }
        edge.bendpoints.push(new Manta.Geometry.Point(x, y));
    };

    EdgeRouting.prototype._routeEdges = function(layers) {
        var i, j, k, iLen, jLen, kLen, vertex, node, shift;

        this.nodesToCheck = [];
        this.loopGap = {};

        for (i = 0, iLen = layers.length; i < iLen; i++) {
            for (j = 0, jLen = layers[i].size(); j < jLen; j++) {
                vertex = layers[i].vertices[j];
                if (!vertex.isDummy) {
                    node = vertex.node;
                    if (node.hasChildren() && node.expanded) {
                        shift = 0;
                        for (k = 0, kLen = node.children.length; k < kLen; k++) {
                            this._routeNodeEdgesIn(node.children[k], shift);
                            shift += EDGE_DELTA;
                        }
                    } else {
                        this._routeNodeEdgesIn(node, 0);
                    }
                }
            }
        }
        if (this.nodesToCheck.length > 0) {
            for (i = 0, iLen = this.nodesToCheck.length; i < iLen; i++) {
                this._separateInAndOutEdges(this.nodesToCheck[i]);
            }
        }
    };

    EdgeRouting.prototype._routeNodeEdgesIn = function(node, shift) {
        var j, jLen, edge;
        for (j = 0, jLen = node.getVisibleEdgesIn().length; j < jLen; j++) {
            edge = node.getVisibleEdgesIn()[j];
            this._routeEdge(edge, shift);
        }
    };

    EdgeRouting.prototype._routeEdge = function(edge, shift) {
        var i, iLen, reversed = this._isReversed(edge), loop = this._isLoop(edge);

        if (edge.hasChildren() && edge.bendpoints.length === 0) {
            // musim pripadne prebrat bendpointy potomka, protoze nevim jak lip
            // to vyresit
            edge.bendpoints = edge.children[0].bendpoints.slice();
        }

        if (shift) {
            for (i = 0, iLen = edge.bendpoints.length; i < iLen; i++) {
                edge.bendpoints[i].y += shift;
            }
        }
        if (reversed || loop) {
            // oba konce hrany jsou ted podezrele z toho, ze je potreba
            // separovat
            // odch. a prich. hrany na jedne strane uzlu
            this.nodesToCheck.pushOnce(edge.source);
            this.nodesToCheck.pushOnce(edge.target);
            if (loop) {
                this._placeLoopEndPivots(edge);
            } else {
                this._placeEndPivot(edge.source, edge, false);
                this._placeEndPivot(edge.target, edge, true);
            }
        } else {
            this._placeEndPivot(edge.source, edge, true);
            this._placeEndPivot(edge.target, edge, false);
        }
    };

    EdgeRouting.prototype._isReversed = function(edge) {
        return edge.target.getAbsoluteBounds().x < edge.source.getAbsoluteBounds().x;
    };

    EdgeRouting.prototype._isLoop = function(edge) {
        return edge.target.getAbsoluteBounds().x === edge.source.getAbsoluteBounds().x;
    };

    EdgeRouting.prototype._separateInAndOutEdges = function(node) {
        var i, edges, edge,
        // posbiram hrany podle toho na jake strane uzlu se nachazeji
        leftIn = [], leftOut = [], rightIn = [], rightOut = [],
        // jestli budou nahore hrany dovnitr nebo ven urcim podle prumeru jejich
        // pozic
        leftInSumY = 0, leftOutSumY = 0, rightInSumY = 0, rightOutSumY = 0, inShift, outShift;

        // hrany do uzlu
        edges = node.getVisibleEdgesIn();
        for (i = 0; i < edges.length; i++) {
            edge = edges[i];
            if (this._isReversed(edge) || this._isLoop(edge)) {
                rightIn.push(edge);
                rightInSumY += edge.bendpoints[1].y;
            } else {
                leftIn.push(edge);
                leftInSumY += edge.bendpoints[edge.bendpoints.length - 2].y;
            }
        }

        // hrany z uzlu
        edges = node.getVisibleEdgesOut();
        for (i = 0; i < edges.length; i++) {
            edge = edges[i];
            if (this._isReversed(edge)) {
                leftOut.push(edge);
                leftOutSumY += edge.bendpoints[edge.bendpoints.length - 2].y;
            } else {
                rightOut.push(edge);
                rightOutSumY += edge.bendpoints[1].y;
            }
        }

        if (leftIn.length > 0 && leftOut.length > 0) {
            // na leve strane se sesly hrany dovnitr i ven
            if (leftInSumY / leftIn.length < leftOutSumY / leftOut.length) {
                inShift = -EDGE_IN_OUT_SEPARATION;
                outShift = EDGE_IN_OUT_SEPARATION;
            } else {
                inShift = EDGE_IN_OUT_SEPARATION;
                outShift = -EDGE_IN_OUT_SEPARATION;
            }
            for (i = 0; i < leftIn.length; i++) {
                leftIn[i].bendpoints.last().y += inShift;
            }
            for (i = 0; i < leftOut.length; i++) {
                leftOut[i].bendpoints.last().y += outShift;
            }
        }

        if (rightIn.length > 0 && rightOut.length > 0) {
            // na prave strane se sesly hrany dovnitr i ven
            if (rightInSumY / rightIn.length < rightOutSumY / rightOut.length) {
                inShift = -EDGE_IN_OUT_SEPARATION;
                outShift = EDGE_IN_OUT_SEPARATION;
            } else {
                inShift = EDGE_IN_OUT_SEPARATION;
                outShift = -EDGE_IN_OUT_SEPARATION;
            }
            for (i = 0; i < rightIn.length; i++) {
                if (this._isLoop(rightIn[i])) {
                    rightIn[i].bendpoints.last().y += inShift;
                } else {
                    rightIn[i].bendpoints.first().y += inShift;
                }
            }
            for (i = 0; i < rightOut.length; i++) {
                rightOut[i].bendpoints.first().y += outShift;
            }
        }
    };

    /**
     * Rozmisti hrany na boku uzlu do stredu layoutovaneho uzlu
     */
    EdgeRouting.prototype._placeLoopEndPivots = function(edge) {
        var middleOfSource = edge.source.getAbsoluteBounds().y + edge.source.bounds.height / 2, middleOfTarget = edge.target
                .getAbsoluteBounds().y
                + edge.target.bounds.height / 2, x, parent = edge.source.parent,
        // pokud uzel nema parenta, musi se jednat o self-loop
        id = parent ? parent.id : edge.source.id;

        if (!(id in this.loopGap)) {
            this.loopGap[id] = 0;
        } else {
            this.loopGap[id] = Math.min(this.loopGap[id] + LOOP_SEPARATION, LAYER_GAP / 2 - EDGE_PADDING);
        }
        x = edge.source.getAbsoluteBounds().x + edge.source.bounds.width + this.loopGap[id] + EDGE_PADDING;

        this._prependBendpoint(edge, x, middleOfSource);
        this._appendBendpoint(edge, x, middleOfTarget);
    };

    /**
     * Rozmisti hrany na boku uzlu do stredu layoutovaneho uzlu
     * 
     * @param node
     * @param edge
     * @param right smer kterym rozmistuju hrany - true = zleva doprava, false = zprava doleva
     * @private
     */
    EdgeRouting.prototype._placeEndPivot = function(node, edge, right) {
        var middleOfNode = node.getAbsoluteBounds().y + node.bounds.height / 2,
        // x souradnice spolecna pro vsechny hrany
        x = right ? node.getAbsoluteBounds().x + node.bounds.width + EDGE_PADDING : node.getAbsoluteBounds().x
                - EDGE_PADDING;

        if (right) {
            this._prependBendpoint(edge, x, middleOfNode);
        } else {
            this._appendBendpoint(edge, x, middleOfNode);
        }
    };

    // </editor-fold>

    // <editor-fold desc="MODEL">
    Layer = function() {
        this.vertices = [];
    };

    /**
     * Prida uzel do vrstvy
     * 
     * @param vertex
     */
    Layer.prototype.add = function(vertex) {
        vertex.position = this.vertices.length;
        this.vertices.push(vertex);
    };

    Layer.prototype.size = function() {
        return Object.keys(this.vertices).length;
    };

    /**
     * @returns {Array} vsechny hrany, ktere smeruji do nejakeho uzlu v teto vrstve
     */
    Layer.prototype.getAllSegmentsIn = function() {
        var i, edges = [];
        for (i = 0; i < this.vertices.length; i++) {
            edges.push.apply(edges, this.vertices[i].segmentsIn);
        }
        return edges;
    };

    // noinspection JSUnusedGlobalSymbols
    /**
     * @returns {Array} vsechny hrany, ktere smeruji od nejakeho uzlu v teto vrstve
     */
    Layer.prototype.getAllSegmentsOut = function() {
        var i, edges = [];
        for (i = 0; i < this.vertices.length; i++) {
            edges.push.apply(edges, this.vertices[i].segmentsOut);
        }
        return edges;
    };

    /**
     * Metoda pouze pro testovani - overuje, zda vrstva obsahuje uzel se zadanym id
     * 
     * @param id
     * @returns {boolean}
     */
    Layer.prototype.containsId = function(id) {
        return this.getVertexByNodeId(id) !== null;
    };

    /**
     * Metoda pouze pro testovani - overuje, zda vrstva obsahuje uzel se zadanym id
     * 
     * @param id
     * @returns {Vertex}
     */
    Layer.prototype.getVertexByNodeId = function(id) {
        var i;
        for (i = 0; i < this.vertices.length; i++) {
            if (!this.vertices[i].isDummy) {
                if (this.vertices[i].node.id === id) {
                    return this.vertices[i];
                }
            }
        }
        return null;
    };

    /**
     * Vrati predchozi uzel v poradi ve vrstve, pokud predchozi uzel neni, vrati null
     * 
     * @param vertex uzel, pro nejz hledame souseda ve vrstve
     * @returns {Vertex} predchozi vrchol
     */
    Layer.prototype.getPrevVertex = function(vertex) {
        return this.getVertexAt(vertex.position - 1);
    };

    /**
     * Vrati dalsi uzel v poradi ve vrstve, pokud dalsi uzel neni, vrati null
     * 
     * @param vertex uzel, pro nejz hledame souseda ve vrstve
     * @returns {Vertex} dalsi vrchol
     */
    Layer.prototype.getNextVertex = function(vertex) {
        return this.getVertexAt(vertex.position + 1);
    };

    /**
     * @param pos
     * @returns {Vertex} Vrchol na zadane pozici
     */
    Layer.prototype.getVertexAt = function(pos) {
        if (!(pos in this.vertices)) {
            return null;
        }
        return this.vertices[pos];
    };

    Vertex = function() {
        this.id = 'not assigned';
        this.segmentsIn = [];
        this.segmentsOut = [];
        this.isDummy = false;
        this.node = null;
        this.edge = null;
        this.layer = 0;
        this.position = 0;
        this.medianUp = -1;
        this.medianDown = -1;

        // Coordinate assignment
        this.root = this;
        this.align = this;
        this.sink = this;
        this.shift = Infinity;
        this.x = 0;
        this.spaceUp = 0; // Prostor vrcholu, ktery potrebuje nad sebou pro
        // hlavicky parentu
        this.spaceDown = 0; // Prostor vrcholu, ktery potrebuje pod sebou proh
        // paticky parentu
        this.coordinates = [];

        // Atomic cross reduction
        this.handicap = 0;
    };

    Vertex.prototype.toString = function() {
        if (this.isDummy) {
            return 'Dummy, edge:' + this.edge.toString();
        } else {
            return 'Node ' + this.node.id;
        }
    };

    Vertex.prototype.setNode = function(node) {
        this.node = node;
        this.id = node.id;
    };

    Vertex.prototype.setEdge = function(edge) {
        this.edge = edge;
        this.id = 'dummy: ' + edge.source.id + '-' + edge.target.id;
    };

    Vertex.prototype.getSegment = function(vertex, from) {
        var i, segments = from ? this.segmentsIn : this.segmentsOut, end = from ? 'source' : 'target';

        for (i = 0; i < segments.length; i++) {
            if (segments[i][end] === vertex) {
                return segments[i];
            }
        }
        return null;
    };

    Segment = function(source, target, edge) {
        this.source = source;
        this.target = target;
        this.edge = edge;

        // Coordinate assignment
        this.type1Conflict = false;
    };

    // </editor-fold>

    // <editor-fold desc="COORDINATE ASSIGNMENT">
    /**
     * Objekt se stara o prirazeni souradnic uzlum dle "Fast and Simple Horizontal Coordinate Assignment" (Brandes,
     * Kopf). Zkladnim principem je, ze zachovava poradi uzlu ve vrstvach a grantuje, ze zadna "dlouha" hrana nebude mit
     * zalomeni jinde, nez v prvnim a/nebo poslednim segmentu.
     * 
     * @constructor
     */
    CoordinateAssignment = function(layout) {
        /**
         * Stav objektu - jakym horizontalnim smerem se layout zarovnava
         * 
         * @type {boolean}
         */
        this.leftToRight = true;

        /**
         * Stav objektu - jakym vertikalnim smerem se layout zarovnava
         * 
         * @type {boolean}
         */
        this.topBottom = true;

        this.layout = layout;
        this.TOP_RIGHT = 1;
        this.BOTTOM_RIGHT = 2;
    };

    CoordinateAssignment.prototype.execute = function(layers) {
        var width = [], vertex, i, j;
        // PRO DEBUG - balance X not balance
        var balance = true;

        this._cleanCoordinates(layers);
        this._setVerticesSpace(layers);
        this.preprocess(layers);

        this.leftToRight = true;
        this.topBottom = true;

        // zarovnani vlevo nahoru
        width.push(this.assignCoordinates(layers));
        this.leftToRight = false;
        // zarovnani vpravo nahoru
        width.push(this.assignCoordinates(layers));
        this.topBottom = false;
        // zarovnani vpravo dolu
        width.push(this.assignCoordinates(layers));
        this.leftToRight = true;
        // zarovnani vlevo dolu
        width.push(this.assignCoordinates(layers));
        
        this.normalizeCoordinates(layers, width);
        // vyvazeni vsech zarovnani
        for (i = 0; i < layers.length; i++) {
            for (j = 0; j < layers[i].size(); j++) {
                vertex = layers[i].vertices[j];
                
                // zarovnani jsou 4 - vezmu dve prostredni pro prumerny median
                if (balance) {
                    vertex.coordinates.sort(function(a, b) {
                        return a - b
                    });
                    vertex.x = (vertex.coordinates[1] + vertex.coordinates[2]) / 2;
                } else {
                    if (!vertex.isDummy) {
                        vertex.node.label = vertex.id + ' - root:' + vertex.root.id + ', sink:' + vertex.root.sink.id;
                    }
                    vertex.x = vertex.coordinates[0];
                }
            }
        }
    };
    
    CoordinateAssignment.prototype._cleanCoordinates = function(layers) {
        for (i = 0; i < layers.length; i++) {
            for (j = 0; j < layers[i].size(); j++) {
                layers[i].vertices[j].coordinates = [];
            }
        }
    };

    /**
     * Provede posun vsech souradnic tak aby byly spravne polozeny pres sebe pro vsechny zarovnani. Provadi se tak, ze
     * zarovnani vlevo/vpravo zarovnavam od leve/prave strany nejuzsiho zarovnani
     * 
     * @param layers
     * @param width pole vyslednych sirek vsech zarovnani
     */
    CoordinateAssignment.prototype.normalizeCoordinates = function(layers, width) {
        var i, j, minWidth = Infinity, vertex;

        for (i = 0; i < width.length; i++) {
            if (width[i] < minWidth) {
                minWidth = width[i];
            }
        }

        for (i = 0; i < layers.length; i++) {
            for (j = 0; j < layers[i].size(); j++) {
                vertex = layers[i].vertices[j];
                if (this.TOP_RIGHT in vertex.coordinates)
                    vertex.coordinates[this.TOP_RIGHT] += minWidth;
                if (this.BOTTOM_RIGHT in vertex.coordinates)
                    vertex.coordinates[this.BOTTOM_RIGHT] += minWidth;
            }
        }
    };

    /**
     * Zkratka pro volani vsech fazi prirazeni souradnic.
     * 
     * @param layers
     * @returns {*} sirka vysledneho prirazeni
     */
    CoordinateAssignment.prototype.assignCoordinates = function(layers) {
        this.reset(layers);
        this.verticalAlignment(layers);
        return this.horizontalAlignment(layers);
    };

    /**
     * Vyresetuje vsechny pomocne pole vsech layoutovanych vrcholu a pripravi je tak na nove kolo prirazeni souradnic.
     * 
     * @param layers
     */
    CoordinateAssignment.prototype.reset = function(layers) {
        var i, j, vertex;

        for (i = 0; i < layers.length; i++) {
            layers[i].vertices.sort(verticesSort);
            for (j = 0; j < layers[i].size(); j++) {
                vertex = layers[i].vertices[j];
                vertex.root = vertex;
                vertex.align = vertex;
                vertex.sink = vertex;
                vertex.shift = this.leftToRight ? Infinity : -Infinity;
                vertex.x = undefined;
            }
        }
    };

    /**
     * Nastavi vsem vrcholum jaky maji prostor nad a pod sebou, vzhledem k hierarchickemu zarazeni
     * 
     * @param layers
     * @private
     */
    CoordinateAssignment.prototype._setVerticesSpace = function(layers) {
        var i, j, vertex;

        for (i = 0; i < layers.length; i++) {
            layers[i].vertices.sort(verticesSort);
            // posledni uzel ve vrstve uz pocitat nemusim...
            for (j = 0; j < layers[i].size(); j++) {
                vertex = layers[i].vertices[j];
                this._setVertexSpace(layers[i], vertex);
            }
        }
    };

    /**
     * Nastavi jednomu vrcholu jaky ma mit prostor nad a pod sebou vzhledem k hierarchii vrcholu, ktere jsou vedle nej
     * ve vrstve - podle jejich prvniho spolecneho parenta.
     * 
     * @param layer
     * @param vertex
     * @private
     */
    CoordinateAssignment.prototype._setVertexSpace = function(layer, vertex) {
        var commonDepth, prev, next;

        if (vertex.isDummy) {
            this._setDummyVertexSpace(vertex);
            return;
        }

        prev = layer.getPrevVertex(vertex);
        while (prev !== null) {
            if (!prev.isDummy) {
                break;
            }
            prev = layer.getPrevVertex(prev);
        }
        commonDepth = prev ? this._getCommonDepth(prev.node, vertex.node) : 0;
        vertex.spaceUp = (vertex.node.getDepth() - commonDepth) * (NODE_HEIGHT + PARENT_PADDING)
                + vertex.node.bounds.height / 2;
        
        next = layer.getNextVertex(vertex);
        while (next !== null) {
            if (!next.isDummy) {
                break;
            }
            next = layer.getNextVertex(next);
        }
        commonDepth = next ? this._getCommonDepth(next.node, vertex.node) : 0;
        vertex.spaceDown = vertex.node.bounds.height / 2 + (vertex.node.getDepth() - commonDepth) * PARENT_PADDING;
    };

    /**
     * Nastavi prostor ro dummy vrchol
     * 
     * @param vertex
     * @private
     */
    CoordinateAssignment.prototype._setDummyVertexSpace = function(vertex) {
        var space;
        // uzel je dumy, jeho velikost tak urcuji hrany
        if (vertex.edge.source.id in this.layout.contracted || vertex.edge.target.id in this.layout.contracted) {
            // hrany uzlu se budou rozpadat, musim pro ne proto udelat misto
            space = ((vertex.edge.children.length) * EDGE_DELTA);
        } else {
            // hrana se nebude rozpadat, staci default
            space = EDGE_DELTA;
        }
        vertex.spaceUp = space;
        vertex.spaceDown = space;
    };

    /**
     * Najde hloubku prvniho spolecneho parenta obou uzlu.
     * 
     * @param n1
     * @param n2
     * @returns {*}
     * @private
     */
    CoordinateAssignment.prototype._getCommonDepth = function(n1, n2) {
        var common;
        common = n2.parent;
        while (common !== null) {
            if (n1.isDescendantOf(common))
                break;
            common = common.parent;
        }
        return common ? common.getDepth() + 1 : 0;
    };

    // Horizontal alignment
    // **************************************************************************
    /**
     * Finalni faze prirazeni souradnic - priradi jednotlivym blokum uzlu takove souradnice, aby vysledne prirazeni bylo
     * co nejkompaktnejsi
     * 
     * Pozn. drzim se terminologie z clanku (top bottom smer), v nasem pripade (left right)jde vlastne o vertikalni
     * zarovnani.
     * 
     * @param layers
     * @returns {number} sirka vysledneho prirazeni
     */
    CoordinateAssignment.prototype.horizontalAlignment = function(layers) {
        var minX = Infinity, maxX = -Infinity, layerIterator = new Manta.Utils.Iterator(layers, this.topBottom), layer, vertexIterator, vertex, shiftInLayer, shifted;

        while (layerIterator.hasNext()) {
            layer = layerIterator.next();
            vertexIterator = new Manta.Utils.Iterator(layer.vertices, !this.leftToRight);
            while (vertexIterator.hasNext()) {
                vertex = vertexIterator.next();
                if (vertex.root === vertex) {
                    this.placeBlock(vertex, layers);
                }
            }
        }

        // doplnim souradnice vsech uzlu v jednotlivych blocich
        layerIterator = new Manta.Utils.Iterator(layers, !this.topBottom);
        while (layerIterator.hasNext()) {
            layer = layerIterator.next();
            vertexIterator = new Manta.Utils.Iterator(layer.vertices, !this.leftToRight);

            // fix kvuli moznemu shiftu o vice trid v jedne vrstve
            shiftInLayer = 0;
            shifted = {};

            while (vertexIterator.hasNext()) {
                vertex = vertexIterator.next();
                vertex.x = vertex.root.x;
                if (Math.abs(vertex.root.sink.shift) < Infinity) {
                    if (!(vertex.root.sink.id in shifted)) {
                        shiftInLayer += vertex.root.sink.shift;
                        shifted[vertex.root.sink.id] = true;
                    }
                    // pridam posun vsech trid v aktualni vrstve
                    vertex.x += shiftInLayer;
                }
                                
                vertex.coordinates.push(vertex.x);
                minX = Math.min(minX, vertex.x);
                maxX = Math.max(maxX, vertex.x);
            }
        }
        return maxX - minX;
    };

    /**
     * Rekurzivne rozdeli bloky do trid a urci jejich posun tak, aby sirka prirazeni byla co nejmensi.
     * 
     * @param v
     * @param layers
     */
    CoordinateAssignment.prototype.placeBlock = function(v, layers) {
        var w, u, layer, neighbor;

        if (v.x === undefined) {
            v.x = 0;
            w = v;
            do {
                layer = layers[w.layer];
                // uzel neni prvni ani posledni ve vrstve
                // noinspection OverlyComplexBooleanExpressionJS
                if ((this.leftToRight && w.position > 0) || (!this.leftToRight && w.position < layer.size() - 1)) {
                    neighbor = this.leftToRight ? layer.getPrevVertex(w) : layer.getNextVertex(w);
                    u = neighbor.root;
                    this.placeBlock(u, layers);
                    if (v.sink === v) {
                        v.sink = u.sink;
                    }

                    if (v.sink === u.sink) {
                        if (this.leftToRight) {
                            v.x = Math.max(v.x, neighbor.root.x + neighbor.spaceDown + w.spaceUp + VSPACE);
                        } else {
                            v.x = Math.min(v.x, neighbor.root.x - neighbor.spaceUp - w.spaceDown - VSPACE);
                        }
                    } else {
                        if (this.leftToRight) {
                            u.sink.shift = Math.min(u.sink.shift, v.x - u.x - neighbor.spaceDown - w.spaceUp - VSPACE);
                        } else {
                            u.sink.shift = Math.max(u.sink.shift, v.x - u.x + neighbor.spaceUp + w.spaceDown + VSPACE);
                        }
                    }
                }
                w = w.align;
            } while (w !== v);
        }
    };

    // Preprocessing
    // *********************************************************************************
    /**
     * Oznaci konflikty typu 1 = krizeni vnitrnich (vedoucich mezi dvema dummy vrcholy) segmentu. To se pozdeji pouzije
     * pri prirazovani bloku
     * 
     * @param layers
     */
    CoordinateAssignment.prototype.preprocess = function(layers) {
        var i, j, k, segmentsIn, length = layers.length;

        for (i = 1; i < length; i++) {
            segmentsIn = layers[i].getAllSegmentsIn();
            for (j = 0; j < segmentsIn.length; j++) {
                if (this.isInnerSegment(segmentsIn[j])) {
                    for (k = 0; k < segmentsIn.length; k++) {
                        if (j !== k && this.isInnerSegment(segmentsIn[j])
                                && this.segmentsCross(segmentsIn[j], segmentsIn[k])) {
                            segmentsIn[k].type1Conflict = true;
                        }
                    }
                }
            }
        }
    };

    /**
     * Helper metoda, ktera stanovi zda jde o vnitrni segment = vedoucich mezi dvema dummy vrcholy
     * 
     * @param segment
     * @returns {boolean|*}
     */
    CoordinateAssignment.prototype.isInnerSegment = function(segment) {
        return segment.source.isDummy && segment.target.isDummy;
    };

    /**
     * Helper metoda rozhodne, zda se segmenty krizi.
     * 
     * @param s1
     * @param s2
     * @returns {boolean}
     */
    CoordinateAssignment.prototype.segmentsCross = function(s1, s2) {
        if (s1.target.position > s2.target.position) {
            return s1.source.position < s2.source.position;
        } else {
            return s1.source.position > s2.source.position;
        }
    };

    // Vertical alignment
    // ****************************************************************************
    /**
     * Zarovna uzly do bloku, ktere budou mit stejnou souradnici.
     * 
     * Pozn. drzim se terminologie z clanku (top bottom smer), v nasem pripade (left right)jde vlastne o horizontalni
     * zarovnani.
     * 
     * @param layers
     */
    CoordinateAssignment.prototype.verticalAlignment = function(layers) {
        var i, j, k, r, vertices, vertex, candidates;

        for (i = this.topBottom ? 1 : layers.length - 2; this.topBottom ? i < layers.length : i >= 0; this.topBottom ? i++
                : i--) {
            r = this.leftToRight ? -1 : Infinity;
            vertices = layers[i].vertices;
            for (j = this.leftToRight ? 0 : vertices.length - 1; this.leftToRight ? j < vertices.length : j >= 0; this.leftToRight ? j++
                    : j--) {
                vertex = vertices[j];
                candidates = this.getCandidates(vertex);
                for (k = 0; k < candidates.length; k++) {
                    if (vertex.align === vertex) {
                        // uzel jeste neni prirazen do bloku
                        r = this.assignToBlock(vertex, candidates[k], r);
                    }
                }
            }
        }
    };

    /**
     * Vlozi uzel do bloku a vrati aktualni pozici ve vrstve.
     * 
     * @param vertex uzel, ktery se zarovnava
     * @param candidate uzel, ktery je kandidatem na zarovnani do stejneho bloku jako zrovnanavany uzel
     * @param r pozice v predchozi vrstve
     * @returns {*} nova pozice v predchozi vrstve, pokud kandidat byl zarovnan do bloku s vrcholem, nebo puvodni
     *          pozice, pokud nebyl
     */
    CoordinateAssignment.prototype.assignToBlock = function(vertex, candidate, r) {
        var segment;

        segment = vertex.getSegment(candidate, this.topBottom);
        if (!segment.type1Conflict) {
            // noinspection OverlyComplexBooleanExpressionJS
            if ((this.leftToRight && r < candidate.position) || (!this.leftToRight && r > candidate.position)) {
                candidate.align = vertex;
                vertex.root = candidate.root;
                vertex.align = candidate.root;
                return candidate.position;
            }
        }
        return r;
    };

    /**
     * Vraci prostredni sousedni vrcholy predaneho vrcholu v predchozi vrstve. Pokud je pocet sousednich vrcholu lichy
     * vrati prostredni uzel, pokud je sudy vrati dva prostredni vrcholy.
     * 
     * @param vertex
     * @returns {Array}
     */
    CoordinateAssignment.prototype.getCandidates = function(vertex) {
        var neighbours = this.getSortedAdjacentVertices(vertex, this.topBottom), candidates = [];
        if (neighbours.length === 0) {
            return candidates;
        }

        if (neighbours.length % 2 === 0) {
            // zalezi na poradi podle stavu:
            if (this.leftToRight) {
                candidates.push(neighbours[neighbours.length / 2 - 1]);
                candidates.push(neighbours[neighbours.length / 2]);
            } else {
                candidates.push(neighbours[neighbours.length / 2]);
                candidates.push(neighbours[neighbours.length / 2 - 1]);
            }
        } else {
            candidates.push(neighbours[Math.floor(neighbours.length / 2)]);
        }
        return candidates;
    };

    /**
     * @param vertex
     * @param up
     * @returns {Array} sousedni vrcholy setridene podle jejich poradi ve vrstve
     */
    CoordinateAssignment.prototype.getSortedAdjacentVertices = function(vertex, up) {
        var i, segments = up ? vertex.segmentsIn : vertex.segmentsOut, end = up ? 'source' : 'target', neighbours = [];

        for (i = 0; i < segments.length; i++) {
            neighbours.push(segments[i][end]);
        }
        neighbours.sort(verticesSort);
        return neighbours;
    };

    // </editor-fold>

    // <editor-fold desc="ATOMIC CROSS REDUCTION">
    /**
     * Objekt se stara o trideni atomickkych uzlu, tak aby byl minimalizovan pocet krizeni hran, ktere mezi nimi vedou.
     * Vyuziva algoritmus v CrossReduction (jedna iterace odshora dolu, s pouzitim "handicap" hodnoty podle nadrazenych
     * uzlu).
     * 
     * @constructor
     */
    AtomicCrossReduction = function() {
        this.nodeVertex = {};
        this.crossReduction = new CrossReduction();
    };

    /**
     * Provede setrideni atomickych uzlu na predanych vrstvach
     * 
     * @param layers vrstvy layoutu
     * @param nodes vsechny layoutovane uzly (jejich atomicti potomci se budou tridit)
     */
    AtomicCrossReduction.prototype.execute = function(layers, nodes) {
        var i, node,
        // vytvorim vrstvy obsahujici uzly
        childLayers = this._createVertices(layers), self = this;

        // Vytvorim segmenty mezi vrstvami
        this._createSegments(childLayers);
        this.applyOrderAttribute(childLayers);
        // pouziju algoritmus na minimalizaci krizeni hran
        this.crossReduction.execute(childLayers, true, 1);

        for (i = 0; i < nodes.length; i++) {
            // setridim vsechny potomky layoutovanych uzlu (pokud jsou atomicti)
            node = nodes[i];
            node.children.sort(function(n1, n2) {
                if (!(n2.id in self.nodeVertex) || !(n1.id in self.nodeVertex))
                    return 0;
                return self.nodeVertex[n1.id].position - self.nodeVertex[n2.id].position;
            });
        }
    };

    /**
     * Aplikuje na atomicke uzly trideni podle jejich ORDER atributu.
     * 
     * @param layers
     */
    AtomicCrossReduction.prototype.applyOrderAttribute = function(layers) {
        var i, j, vertex, layer, self = this;

        var sortFunc = function(v1, v2) {
            if (v1.position === v2.position) {
                // pokud vznikne konflikt uprednostnim vrchol, ktery ma
                // atribut...
                if (self.crossReduction.hasOrderAttribute(v2)) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                return v1.position - v2.position;
            }
        };

        for (i = 0; i < layers.length; i++) {
            layer = layers[i];
            for (j = 0; j < layer.size(); j++) {
                vertex = layer.vertices[j];
                layer.vertices[j].position = this.crossReduction.hasOrderAttribute(vertex) ? this.crossReduction
                        .getOrderAttribute(vertex.node) : vertex.position;
            }
            // setridim uzly
            layer.vertices.sort(sortFunc);
            // normalizuju pozice, aby nemohly vzniknout "diry", duplicity apod.
            for (j = 0; j < layer.size(); j++) {
                layer.vertices[j].position = j;
            }
        }
    };

    /**
     * Vytvori nove vrstvy, ktere obsahuji pouze atomicke uzly (nebo sbalene layoutovane uzly). Kazdy uzel ma nastaveny
     * handicap podle toho do ktere tabulky patri
     * 
     * @param layers vrstvy grafu, dle kterych se detske vrstvy vytvari
     * @returns {Array} seznam detskych vrstev
     * @private
     */
    AtomicCrossReduction.prototype._createVertices = function(layers) {
        var i, j, k, vertex, node, newVertex, layer, childrenLayers = [], handicap = 0, handicapStep = 100;

        for (i = 0; i < layers.length; i++) {
            layer = new Layer();
            for (j = 0; j < layers[i].vertices.length; j++) {
                vertex = layers[i].vertices[j];
                if (!vertex.isDummy) {
                    node = vertex.node;
                    if (node.expanded) {
                        for (k = 0; k < node.children.length; k++) {
                            if (node.children[k].atomic) {
                                newVertex = this._createVertex(node.children[k], handicap);
                                layer.add(newVertex);
                            }
                        }
                    } else {
                        newVertex = this._createVertex(node, handicap);
                        layer.add(newVertex);
                    }
                    handicap += handicapStep;
                }
            }
            childrenLayers.push(layer);
        }
        return childrenLayers;
    };

    /**
     * Vytvori vertex porlde uzlu pro atomickou vrstvu
     * 
     * @param node
     * @param handicap
     * @returns {Vertex}
     * @private
     */
    AtomicCrossReduction.prototype._createVertex = function(node, handicap) {
        var newVertex = new Vertex();
        newVertex.node = node;
        newVertex.handicap = handicap;
        newVertex.id = node.id;
        this.nodeVertex[newVertex.node.id] = newVertex;
        return newVertex;
    };

    /**
     * Vytvori spojeni mezi vertexy v detskych vrstvach
     * 
     * @param layers
     * @private
     */
    AtomicCrossReduction.prototype._createSegments = function(layers) {
        var i, j, k, node, edges, edge, source, target, segment;

        for (i = 0; i < layers.length; i++) {
            for (j = 0; j < layers[i].vertices.length; j++) {
                node = layers[i].vertices[j].node;
                edges = node.getVisibleEdgesIn();
                for (k = 0; k < edges.length; k++) {
                    edge = edges[k];
                    if (edge.source.id in this.nodeVertex && edge.target.id in this.nodeVertex) {
                        source = this.nodeVertex[edge.source.id];
                        target = this.nodeVertex[edge.target.id];
                        segment = new Segment(source, target, edge);
                        source.segmentsOut.push(segment);
                        target.segmentsIn.push(segment);
                    }
                }
            }
        }
    };
    // </editor-fold>

    // <editor-fold desc="CROSS REDUCTION">
    /**
     * @constructor
     */
    CrossReduction = function() {
        this.ITERATIONS = 24;
        this.layerOrderIndex = {};
    };

    /**
     * Provede setrideni vrcholu (uzlu a dummy vrcholu) ve vrstvach tak, aby pocet krizeni hran byl co nejmensi.
     * 
     * @param layers
     * @param onlyForward - true - pruchod se provadi pouze dopredu, false - pruchazi se obema smery
     * @param iterations - pocet iteraci algoritmu, pokud neni zadan, bere se defaultni
     */
    CrossReduction.prototype.execute = function(layers, onlyForward, iterations) {
        var i, j;

        if (!onlyForward) {
            this.initialOrder(layers);
        }

        for (i = 0; i < (iterations + 1 || this.ITERATIONS); i++) {
            // Vzdycky jako prvni provedu medianUp a teprve potom se pustim do
            // iteraci medianDown -> medianUp
            // proto +1 u iteraci
            if (!onlyForward && i > 0) {
                for (j = layers.length - 2; j >= 0; j--) {
                    this.computeMedians(layers[j], false);
                    this.applyOrder(layers[j], 'medianDown');
                }
            }
            for (j = 1; j < layers.length; j++) {
                this.computeMedians(layers[j], true);
                this.applyOrder(layers[j], 'medianUp');
            }
        }
    };

    /**
     * Aplikuje poradi uzlu z poseldniho behu, misto aby algoritmus provedl nove pretrideni uzlu ve vrstvach
     * 
     * @param layers
     * @param lastOrder
     */
    CrossReduction.prototype.applyLastOrder = function(layers, lastOrder) {
        var i, j, layer, sort = function(v1, v2) {
            if (!(v2.id in lastOrder)) {
                return -1;
            }
            if (!(v1.id in lastOrder)) {
                return 1;
            }
            return lastOrder[v1.id] - lastOrder[v2.id];
        };

        for (i = 0; i < layers.length; i++) {
            layer = layers[i];
            layer.vertices.sort(sort);
            for (j = 0; j < layer.vertices.length; j++) {
                layer.vertices[j].position = j;
            }
        }
    };

    /**
     * Vypocte median sousednich uzlu pro vsechny uzly v zadane vrstve podle zadanych parametru
     * 
     * @param layer
     * @param topBottom smer, ze ktereho chceme pocitat mediany (seshora dolu / zespoda nahoru), pokud true pocita se
     *            pro vrstvu "nad", jinak pro vrstvu "pod"
     */
    CrossReduction.prototype.computeMedians = function(layer, topBottom) {
        var i, j, segments = topBottom ? 'segmentsIn' : 'segmentsOut', median = topBottom ? 'medianUp' : 'medianDown', end = topBottom ? 'source'
                : 'target', vertex, medians, left, right, middle;
        for (i = 0; i < layer.size(); i++) {
            vertex = layer.vertices[i];
            medians = [];
            for (j = 0; j < vertex[segments].length; j++) {
                medians.push(vertex[segments][j][end].position + vertex[segments][j][end].handicap);
            }
            medians.sort(function(a, b) {
                return b - a;
            });
            middle = medians.length / 2;
            if (medians.length === 0) {
                // zadny soused, necham pozici
                vertex[median] = -1;
            } else if (medians.length === 2) {
                // prave dva sousedi - udelam prumer
                vertex[median] = (medians[0] + medians[1]) / 2;
            } else if (medians.length % 2 === 0) {
                // sudy pocet sousedu - pouziju "vazeny median" dle graphviz (A
                // Technique for Drawing Directed Graphs)
                left = medians[middle - 1] - medians[0];
                right = medians[medians.length - 1] - medians[middle];
                vertex[median] = (medians[middle - 1] * right + medians[middle] * left) / (left + right);
            } else {
                // lichy pocet sousedu, beru proste median
                vertex[median] = medians[Math.floor(medians.length / 2)];
            }
        }
    };

    /**
     * Nastavi uzly ve vrstve podle zadaneho pole vrcholu
     * 
     * @param layer vrstvu, jejiz poradi menime
     * @param median nazev pole, ktere je pouzito pro setrideni (medianUp / medianDown)
     */
    CrossReduction.prototype.applyOrder = function(layer, median) {
        var i;
        var otherMedian = median === 'medianUp' ? 'medianDown' : 'medianUp';
        var self = this;
        layer.vertices.sort(function(v1, v2) {
            var val1, val2;

            if (v1[median] === v2[median] || v1[median] === -1 || v2[median] === -1) {
                val1 = v1[otherMedian];
                val2 = v2[otherMedian];
            } else {
                val1 = v1[median];
                val2 = v2[median];
            }

            if (self.hasOrderAttribute(v1)) {
                val1 = self.getOrderAttribute(v1.node);
            }
            if (self.hasOrderAttribute(v2)) {
                val2 = self.getOrderAttribute(v2.node);
            }

            if (val1 === val2) {
                // logika zde je takova, ze predpokladame, ze objekty, ktere
                // patri vicemene k sobe
                // (napr. maji spolecne predky) budou mit podobne id
                if (v1.isDummy)
                    return 1;
                if (v2.isDummy)
                    return -1;
                val1 = v1.id;
                val2 = v2.id;
            }
            // nikdy nechceme vratit 0, protoze ruzne browsery se k tomu potom
            // chovaji ruzne...
            return val1 - val2;
        });
        for (i = 0; i < layer.vertices.length; i++) {
            layer.vertices[i].position = i;
        }
    };

    /**
     * Rozhodne zda uzel ma ORDER atribut, podle ktereho se vynucuje jeho poradi
     * 
     * @param vertex
     * @returns {boolean}
     */
    CrossReduction.prototype.hasOrderAttribute = function(vertex) {
        return !vertex.isDummy && this.getOrderAttribute(vertex.node) !== null;
    };

    /**
     * Ziska z uzlu ORDER atribut a vrati ho, nebo vrati null, pokud uzel ORDER atribut nema
     * 
     * @param node
     * @returns {*}
     */
    CrossReduction.prototype.getOrderAttribute = function(node) {
        if (node.attributes && node.attributes[ORDER_ATTRIBUTE]) {
            return node.attributes[ORDER_ATTRIBUTE];
        }
        return null;
    };

    /**
     * Vypocita inicialni rozdeleni do vrstev pomoci DFS pruchodu. Pro stromy uz toto poradi zaruci, ze hrany se nebudou
     * krizit ani jednou.
     * 
     * @param layers
     */
    CrossReduction.prototype.initialOrder = function(layers) {
        var i;
        for (i = 0; i < layers.length; i++) {
            layers[i].vertices.sort(function(v1, v2) {
                if (v1.isDummy)
                    return 1;
                if (v2.isDummy)
                    return -1;
                return v1.id - v2.id;
            });
        }
        for (i = 0; i < layers[0].vertices.length; i++) {
            this.initialOrderDFS(layers[0].vertices[i]);
        }
    };

    /**
     * 
     * Vola se rekurzivne pri DFS pruchodu pro incialni setrideni uzlu ve vrstvach.
     * 
     * @param vertex Vychozi bod DFS
     */
    CrossReduction.prototype.initialOrderDFS = function(vertex) {
        var i, target;
        for (i = 0; i < vertex.segmentsOut.length; i++) {
            target = vertex.segmentsOut[i].target;
            // segment je self-loop - musim ho ignorovat
            if (target === vertex)
                continue;

            if (!(target.layer in this.layerOrderIndex)) {
                this.layerOrderIndex[target.layer] = 0;
            }
            target.position = this.layerOrderIndex[target.layer]++;
            this.initialOrderDFS(target);
        }
    };

    // </editor-fold>

    // <editor-fold desc="LAYER ASSIGNMENT">
    /**
     * Algoritmus pro rozdeleni uzlu do vrstev
     * 
     * @param nodes
     * @constructor
     */
    LayerAssignment = function(nodes) {
        this.nodes = nodes;
        this.layers = [];
        this.nodeInLayer = {};
        this.nodeVertex = {};
    };

    /**
     * Vytvori vrstvy layoutu, ktere obsahuji vrcholy odpovidajici uzlum a dummy vrcholy pro layoutovani a kratke (jen
     * do sousedni vrstvy) segmenty reprezentujici hrany grafu
     * 
     * @returns {Array} vrstvy s naplnenimy daty
     */
    LayerAssignment.prototype.execute = function() {
        var nodes = this.nodes, i;

        this.layers = [];
        this.nodeInLayer = {};

        for (i = 0; i < nodes.length; i++) {
            this.assignLayer(nodes[i]);
        }
        this.promoteVertices(nodes);

        return this.createLayers();
    };

    /**
     * Aplikuje posledni pripsanou vrstvu pro vsechny uzly. Pokud uzel nema pripsanou vrstvu (napr. byl sbalen), pokusi
     * se nalezt posledni pripsanou vrstvu mezi jeho potomky. Pokud se nepodari ani tak nalezt vrstvu pro uzel, vyhodi
     * se vyjimka.
     * 
     * @returns {Array} vrstvy s naplnenymi daty
     */
    LayerAssignment.prototype.applyLastLayers = function() {
        var i, node, layer;

        for (i = 0; i < this.nodes.length; i++) {
            node = this.nodes[i];
            if (!(node.id in this.nodeInLayer)) {
                // uzel nema pripsanou vrstvu z minula, hledam mezi potomky
                layer = this.findChildrenLastLayer(node);
                if (layer !== null) {
                    this.nodeInLayer[node.id] = layer;
                } else {
                    // ani mezi potomky se nenalezla vrstva - nevim kam uzel
                    // zaradit
                    throw new Manta.Exceptions.IllegalStateException("Unable to obain last layer for node!");
                }
            }
        }
        return this.createLayers();
    };

    /**
     * Vyhleda prisouzenou vrstvu mezi vsemi potmky uzlu. Pokud najde mezi potomky dve ruzne vrstvy, vyhodi vyjimku.
     * Pokud nenajde mezi potomky zadnou vrstvu, vrati null.
     * 
     * @param node
     * @returns {*}
     */
    LayerAssignment.prototype.findChildrenLastLayer = function(node) {
        var i, child, layer = null, tmp; // kandidat na prisanou vrstvu
        for (i = 0; i < node.children.length; i++) {
            child = node.children[i];
            if (!(child.id in this.nodeInLayer)) {
                // vrstvu hledame rekurzivne v hierarchii
                tmp = this.findChildrenLastLayer(child);
            } else {
                tmp = this.nodeInLayer[child.id];
            }
            if (layer !== null && layer !== tmp) {
                // nasli jsme dve odlisne vrstvy, nevim jakou vybrat...
                throw new Manta.Exceptions.IllegalStateException("Inconsistent last layer for node!");
            } else {
                layer = tmp;
            }
        }
        return layer;
    };

    /**
     * Vytvori vlastni vrstvy na zaklade dat obsazenych v mape nodeInLayer.
     * 
     * @returns {Array} poe s vrstvami, ktere obsahuji vsechny vrcholy
     */
    LayerAssignment.prototype.createLayers = function() {
        var i, j, layerNum, layer, nodes = this.nodes, reindexVertices = false;

        this.layers = [];

        for (i = 0; i < nodes.length; i++) {
            layerNum = this.nodeInLayer[nodes[i].id];
            if (!(layerNum in this.layers)) {
                this.layers[layerNum] = new Layer();
            }
            this.layers[layerNum].add(this.createVertexFromNode(nodes[i], layerNum));
        }

        for (i = 0; i < this.layers.length; i++) {
            if (this.layers[i] === undefined) {
                this.layers.splice(i, 1);
                reindexVertices = true;
            }
        }

        if (reindexVertices) {
            for (i = 0; i < this.layers.length; i++) {
                for (j = 0; j < this.layers[i].size(); j++) {
                    this.layers[i].vertices[j].layer = i;
                }
            }
        }

        for (i = 0; i < this.layers.length; i++) {
            layer = this.layers[i];
            this.connectVerticesProperly(layer);
        }
        return this.layers;
    };

    /**
     * Posune vsechny uzly, ktere lze posunout, do nejvyssi mozne vrstvy.
     * 
     * @param nodes
     */
    LayerAssignment.prototype.promoteVertices = function(nodes) {
        var i, j, jLen, node, edges, edge, layer, self = this, nodesToPromote = nodes.slice();

        nodesToPromote.sort(function(a, b) {
            // setridim uzly podle vrstev od nejvyssi po nejnizsi
            return self.nodeInLayer[b.id] - self.nodeInLayer[a.id];
        });

        for (i = 0; i < nodesToPromote.length; i++) {
            node = nodesToPromote[i];
            layer = Infinity;
            edges = node.getVisibleEdgesOut();
            for (j = 0, jLen = edges.length; j < jLen; j++) {
                // najdu nejnizsi z vrstev kam je mozne uzel posunout
                edge = edges[j];
                if (!edge.isSelfLoop() && edge.target.id in this.nodeInLayer) {
                    // hrana neni smycka a konec hrany ma nastavenu vrstvu
                    // (pokud ne, je to chyba a
                    // algoritmus spadne pozdeji, tohle je ochrana pred
                    // nastavenim NaN vrstvy)
                    layer = Math.min(layer, this.nodeInLayer[edge.target.id] - 1);
                }
            }
            if (layer !== Infinity && layer !== this.nodeInLayer[node.id]) {
                // pokud je vrstva vyssi nez aktualni, posunu uzel
                this.nodeInLayer[node.id] = layer;
            }
        }
    };

    /**
     * Vezme vrcholy z vrstvy a propoji je dle hran s ostatnimi vrcholy. Pokud je hrana dlouha vlozi do ni dummy uzly,
     * tak aby kazdy segment sel prave z jedne vrstvy do dalsi.
     * 
     * @param layer vrstva jejiz vrcholy chceme zpracovat
     */
    LayerAssignment.prototype.connectVerticesProperly = function(layer) {
        var i, j, edges, sourceVertex, targetVertex, targetId, dummy, segment, tmp;

        for (i = 0; i < layer.vertices.length; i++) {
            if (layer.vertices[i].isDummy)
                continue;

            edges = layer.vertices[i].node.getVisibleEdgesOut();
            for (j = 0; j < edges.length; j++) {
                if (edges[j].isSelfLoop())
                    continue;
                sourceVertex = layer.vertices[i];
                targetId = edges[j].target.id;
                if (!(targetId in this.nodeVertex)) {
                    throw new Manta.Exceptions.IllegalStateException('Target node is not assigned to Vertex:'
                            + this.analyzeWhyNodeNotAssigned(edges[j].target));
                }
                targetVertex = this.nodeVertex[targetId];
                if (sourceVertex.layer >= targetVertex.layer) { // pokud se
                    // jedna o
                    // zpetnou
                    // hranu,
                    // otocime ji
                    // tohle muze nastat v pripade uplatneni poslednich vrstev
                    tmp = sourceVertex;
                    sourceVertex = targetVertex;
                    targetVertex = tmp;
                }
                while (targetVertex.layer - sourceVertex.layer > 1) { // dokud
                    // hrana
                    // vede
                    // pres
                    // cvice
                    // vrstev
                    // vloz
                    // dummy
                    // vytvoreni dummy uzlu
                    dummy = this.createDummyVertex(edges[j], sourceVertex.layer + 1);
                    this.layers[dummy.layer].add(dummy);
                    // vytvoreni dummy segmentu
                    segment = new Segment(sourceVertex, dummy, edges[j]);
                    sourceVertex.segmentsOut.push(segment);
                    dummy.segmentsIn.push(segment);
                    sourceVertex = dummy;
                }
                // vytvoreni posledniho segmentu
                segment = new Segment(sourceVertex, targetVertex, edges[j]);
                sourceVertex.segmentsOut.push(segment);
                targetVertex.segmentsIn.push(segment);
            }
        }
    };

    LayerAssignment.prototype.analyzeWhyNodeNotAssigned = function(node) {
        var msg = "Node with id " + node.id + " not assigned because: \n";
        if (!node.isVisible()) {
            msg += "Node is not visible: ";
            if (node.proxy)
                msg += "Node is proxy. ";
            if (node.selfOrDescendantsFiltered())
                msg += "Node is filtered. ";
            if (node.hidden)
                msg += "Node is hidden. ";
            if (node.shadowOwner)
                msg += "Node is shadow owner. ";
            msg += "\n";
        }
        if (node.parent === null || !node.parent.isHighestUnsplitableLevel()) {
            msg += "Node is not root of hierarchy and node parent is not unsplittable. \n";
        }
        if (!node.expanded || node.isHighestUnsplitableLevel()) {
            msg += "Node is not contracted and node is not unsplittable. \n";
        }
        return msg;
    };

    /**
     * Dle algoritmu Longest Path Layering rekurzivne rozdeli uzly do vrstev
     * 
     * @param node uzel, pro ktery urcujeme vrstvu, vrstva bude rekurzivne urcena i pro jeho predky v DAG
     */
    LayerAssignment.prototype.assignLayer = function(node) {
        var layer = 0, i, predecessor, edgesIn = node.getVisibleEdgesIn();

        for (i = 0; i < edgesIn.length; i++) {
            if (!edgesIn[i].isSelfLoop()) {
                predecessor = edgesIn[i].source;
                if (!(predecessor.id in this.nodeInLayer)) {
                    this.assignLayer(predecessor);
                }
                layer = Math.max(layer, this.nodeInLayer[predecessor.id] + 1);
            }
        }
        this.nodeInLayer[node.id] = layer;
    };

    /**
     * Vytvori dummy vrchol - vrchol, ktery se cvklada do dlouhych hran
     * 
     * @param edge
     * @param layer
     * @returns {Vertex}
     */
    LayerAssignment.prototype.createDummyVertex = function(edge, layer) {
        var vertex = new Vertex();
        vertex.setEdge(edge);
        vertex.isDummy = true;
        vertex.layer = layer;
        return vertex;
    };
    /**
     * Vytvori vrchol pro layoutovani z uzlu grafu
     * 
     * @param node
     * @param layer
     * @returns {Vertex}
     */
    LayerAssignment.prototype.createVertexFromNode = function(node, layer) {
        var vertex = new Vertex();
        vertex.setNode(node);
        vertex.isDummy = false;
        vertex.layer = layer;
        this.nodeVertex[node.id] = vertex;
        return vertex;
    };

    // </editor-fold>

    // <editor-fold desc="CYCLE BREAKING">
    // Cycle breaking algorithm
    // --------------------------------------------------------------------
    /**
     * Algoritmus pro eliminaci cyklu v grafu zalozeny na heuristice rozbijeni cyklu. Snahou teto implementace je
     * nalazeni co nejmensi mnoziny hran, ktere je nutne obratit pro eliminaci vsech cyklu v grafu. viz "A Technique for
     * Drawing Directed Graphs" (E. R. Gansner, E. Koutsos, S. C. North, and K.-P Vo)
     * 
     * @param nodes
     * @constructor
     */
    CycleBreaking = function(nodes) {
        this.nodes = nodes;
        this.notInComponent = [];
        this.notAssigned = [];
        this.order = {};
        this.actualOrder = 0;
        this.components = [];
        this.componentId = {};

        this.reversed = [];
        this.blind = [];
        this.path = [];
    };

    CycleBreaking.prototype.init = function() {
        this.notInComponent = [];
        this.notAssigned = [];
        this.order = {};
        this.actualOrder = 0;
        this.components = [];
        this.componentId = {};

        this.reversed = [];
        this.blind = [];
        this.path = [];
    };

    /**
     * Nalezne vsechny silne propojene komponenty a v kazde iteraci odebira jednu hranu z kazde komponenty do te doby,
     * dokud v grafu existuji silne propojene komponenty.
     */
    CycleBreaking.prototype.execute = function() {
        // muzu odebrat maximalne 10 000 hran z kazde komponenty
        var ITER_LIMIT = 10000, self = this, iteration = 0, i;
        this.init();
        this.getStronglyConnectedComponents();
        while (this.components.length !== 0) {
            for (i = 0; i < this.components.length; i++) {
                self.reverseMostConnectedEdge(this.components[i]);
            }
            if (iteration++ > ITER_LIMIT) {
                throw new Manta.Exceptions.IllegalStateException("Too many cycle breaking iterations"
                        + " - possible endless loop?");
            }
            this.getStronglyConnectedComponents();
        }
    };

    /**
     * Otoci vsechny hrany, ktere byly otocene v cycle breaking fazi zpet
     */
    CycleBreaking.prototype.reverseBack = function() {
        var i, reversedLength = this.reversed.length;

        for (i = 0; i < reversedLength; i++) {
            this.reversed[i].reverse();
        }
    };

    /**
     * Zrusi zaslepeni vsech hran zaslepenych v cycle breaking fazi.
     */
    CycleBreaking.prototype.unblind = function() {
        var i, blindLength = this.blind.length;

        for (i = 0; i < blindLength; i++) {
            this.blind[i].unblind();
        }
    };

    /**
     * Otoci tu hranu uvnitr zadane komponenty, ktera se ucastni nejvice cyklu. Pokud je takovych hran vice, vybere se
     * ta s nejnizsim souctem stupnu v obou koncovych bodu.
     */
    CycleBreaking.prototype.reverseMostConnectedEdge = function(component) {
        var heighestScore = 0, edgeToReverse = undefined, i, j, node, edge, visibleEdges, score, alreadyReversed = undefined;

        score = this.evaluateEdgeConnectionsDFS(component);

        for (i = 0; i < component.length; i++) {
            node = component[i];
            for (j = 0, visibleEdges = node.getVisibleEdgesOut(); j < visibleEdges.length; j++) {
                edge = visibleEdges[j];

                if (this.isEdgeAlreadyReveresed(edge)) {
                    // Zapamatujeme si kteroukoliv jednu hranu, ktera jiz byla otocena.  
                    alreadyReversed = edge;
                    continue;
                }

                if (score[edge.id] !== undefined && score[edge.id] > heighestScore) {
                    heighestScore = score[edge.id];
                    edgeToReverse = edge;
                } else if (score[edge.id] !== undefined && score[edge.id] > heighestScore
                        && this.getDegree(edgeToReverse) < this.getDegree(edge)) {
                    edgeToReverse = edge;
                }
            }
        }

        if (edgeToReverse !== undefined) {
            edgeToReverse.reverse();
            this.reversed.push(edgeToReverse);
        } else if (alreadyReversed !== undefined) {
            // Pokud jsme nenasli hranu k otoceni a nasli jsme nejakou,
            // ktera jiz otocena byla, zaslepime ji.
            alreadyReversed.blind();
            this.blind.push(alreadyReversed);
        } else {
            console.log('There is no edge to reverse');
        }
    };

    /**
     * Kontrola, jestli už daná hrana nebyla otočena.
     * 
     * @param edge kontrolovaná hrana
     */
    CycleBreaking.prototype.isEdgeAlreadyReveresed = function(edge) {
        for (var revEdgeIndex = 0; revEdgeIndex < this.reversed.length; revEdgeIndex++) {
            if (this.reversed[revEdgeIndex].id === edge.id) {
                return true;
            }
        }
        return false;
    };

    /**
     * Pomoci DFS projde komponentu a ohodnoti vsechny hrany podle poctu cyklu kterych se ucastni (edge.score)
     * 
     * @param nodes vsechny uzly, ktere nalezi do dilne propojene komponenty
     */
    CycleBreaking.prototype.evaluateEdgeConnectionsDFS = function(nodes) {
        var i, j, node, visibleEdges, score = {}, edge;

        for (i = 0; i < nodes.length; i++) {
            node = nodes[i];
            for (j = 0, visibleEdges = node.getVisibleEdgesOut(); j < visibleEdges.length; j++) {
                edge = visibleEdges[j];
                if (!edge.isSelfLoop()) {
                    score[edge.id] = this.evaluateEdge(visibleEdges[j]);
                }
            }
        }
        return score;
    };

    /**
     * Projde pomoci DFS graf od ciloveho uzlu hrany a spocita kolikrat se dostanu do zdrojoveho uzlu - spocita kolika
     * cyklu se hrana ucastni
     * 
     * @param edge hrana pro kterou pocitam cykly
     * @returns {number} pocet cyklu, kterych se hrana ucastni
     */
    CycleBreaking.prototype.evaluateEdge = function(edge) {
        var i, j, cyc = 0, visibleEdges, start = edge.source, pred = {}, // predek
        // uzlu
        // -
        // kudy
        // jsem
        // se
        // do
        // nej
        // dostal,
        // kvuli
        // sledovani
        // aktualni
        // cesty
        path = [], // aktualni cesta, kterou jsem urazil
        open = [], // uzly ke zpracovani
        visited = [], // jiz navstivene uzly
        formsCycle = {}, // eviduje, ktere uzly tvori cyklus, pokud se
        // dostanu nejakou cestou do uzlu, ktery tvori
        // cyklus, nemusim uz pokracovat dal - urcite dojdu do startovniho
        // cyklu, format objektu
        // je node.id -> pocet cest, kterymi se dostanu do zdrojoveho uzlu hrany
        actualNode, cycles = 0, next, MAX_CYCLES = 20000;

        open.push(edge.target);

        while (open.length > 0) {
            actualNode = open.pop();
            while (path.length > 0 && path.last() !== pred[actualNode.id]) {
                // vratim se po ceste k aktualnimu uzlu
                path.pop();
            }
            path.push(actualNode);
            visited.push(actualNode);

            for (i = 0, visibleEdges = actualNode.getVisibleEdgesOut(); i < visibleEdges.length; i++) {
                next = visibleEdges[i].target;
                if (next === start) {
                    // dosel jsem do zdrojovehu uzlu hrany - nalezen cyklus
                    for (j = 1; j < path.length; j++) {
                        // vsechny uzly na aktualni dojdou do zdrojoveho uzlu
                        if (!(path[j].id in formsCycle)) {
                            formsCycle[path[j].id] = 0;
                        }
                        // pokud jsem tudy dosel pres uzel do zdrojoveho
                        // vickrat, tvori vic cyklu...
                        formsCycle[path[j].id]++;
                    }
                    cycles++;
                } else if (!visited.contains(next)) {
                    open.push(next);
                    pred[next.id] = actualNode;
                } else if (next.id in formsCycle && !path.contains(next)) {
                    // dosel jsem do uzlu, ktery uz urcite tvori cyklus
                    cycles += formsCycle[next.id];
                }
            }
            // rezort posledni zachrany - kdyby nahodou v algoritmu byla chyba,
            // ochranim browser pred nekonecnou smyckou...
            if (cyc++ > MAX_CYCLES)
                throw new Manta.Exceptions.IllegalStateException("Too many cycles in cycle breaking");
        }
        return cycles;
    };

    /**
     * Nalezne vsechny silne propojene komponenty v celem grafu a ulozi je do CycleBreaking.components
     */
    CycleBreaking.prototype.getStronglyConnectedComponents = function() {
        var i, nodes = this.nodes;
        this.notInComponent = [];
        this.notAssigned = [];
        this.order = {};
        this.actualOrder = 0;
        this.components = [];
        this.componentId = {};

        for (i = 0; i < nodes.length; i++) {
            if (!this.hasOrder(nodes[i])) {
                // uzel je prvni nebo je nedostupny z predchozich uzlu -
                // vezmem to jeste od nej...
                this.componentSearchDFS(nodes[i]);
            }
        }
    };

    /**
     * Spocita soucet prichozich i odchozich stupnu obou koncovych bodu hrany
     */
    CycleBreaking.prototype.getDegree = function(edge) {
        return edge.source.getVisibleEdgesIn().length + edge.source.getVisibleEdgesOut().length
                + edge.target.getVisibleEdgesIn().length + edge.target.getVisibleEdgesOut().length;
    };

    /**
     * Rozhodne zda uzel uz byl navstiven (ma nastavene poradi)
     * 
     * @param node
     * @returns {boolean} true pokud uzel uz by navstiven
     */
    CycleBreaking.prototype.hasOrder = function(node) {
        return node.id in this.order;
    };

    /**
     * Rozhodne zda uzel uz je v nejake komponente
     * 
     * @param node
     * @returns {boolean} true pokud uzel je v komponente
     */
    CycleBreaking.prototype.hasComponent = function(node) {
        return node.id in this.componentId;
    };

    /**
     * Prida uzel do komponenty
     */
    CycleBreaking.prototype.assignTo = function(node, componentId, component) {
        this.componentId[node.id] = componentId;
        if (component !== undefined) {
            component.push(node);
        }
    };

    /**
     * Viz Path-based strongly connected component algorithm.
     * 
     * Najde vsechny silne propojene komponenty a ulozi je do CycleBreaking.components.
     * 
     * @param node Uzel u ktereho zacinam s prohledavanim - v jednom volani najdu pouze ty komponenty, ktere jsou
     *            dostupne z tohoto uzlu.
     */
    CycleBreaking.prototype.componentSearchDFS = function(node) {
        var component, componentId, part, i, target, visibleEdges;

        this.order[node.id] = this.actualOrder++;
        this.notAssigned.push(node);
        this.notInComponent.push(node);

        for (i = 0, visibleEdges = node.getVisibleEdgesOut(); i < visibleEdges.length; i++) {
            target = visibleEdges[i].target;
            if (!this.hasOrder(target)) {
                this.componentSearchDFS(target);
            } else if (!this.hasComponent(target)) {
                while (this.order[this.notAssigned.last().id] > this.order[target.id]) {
                    this.notAssigned.pop();
                }
            }
        }

        if (this.notAssigned.last() === node) {
            this.notAssigned.pop();
            part = this.notInComponent.pop();
            componentId = node.id;
            if (part === node) {
                this.assignTo(node, componentId);
            } else {
                component = [];
                while (part != node) {
                    this.assignTo(part, componentId, component);
                    part = this.notInComponent.pop();
                }
                this.assignTo(part, componentId, component);
                this.components.push(component);
            }
        }
    };

    // </editor-fold>

}(Manta.Layout = Manta.Layout || {}, jQuery));
