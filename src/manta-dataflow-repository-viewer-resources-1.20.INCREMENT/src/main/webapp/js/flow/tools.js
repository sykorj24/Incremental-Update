/**************************************************************************
* @namespace    Manta.Tools
* @author       Martin Slapak
*
* Pomocne rozsahle funkce (zvyraznovani toku).
*
**************************************************************************/

(function(ns, $, undefined){
    /**
     * Priznak zda je vysvicen nejaky tok, aby se korektne nastavovaly barvy na mouse hover
     */
    ns.flowHighligted = false;
    /**
     * Pocatecni cas pro mereni vykonu.
     */
    ns.startTime = null;
    
    ns.lastStartNodeController = null;

    ns.lastStartEdgeController = null;
    
    ns.Direction = {
        FWD: 'Fwd',
        BWD: 'Bwd',
        CYCLE: 'Cycle'
    };

    ns.reHighlightFlow = function() {
        if (ns.lastStartNodeController != undefined) {
            if (ns.lastStartNodeController.model.hidden) {
                ns.cleanLastHighlightFlow();
                return;
            }
            
            if (ns.lastStartEdgeController != undefined && 
                    (ns.lastStartEdgeController.model.source == undefined || ns.lastStartEdgeController.model.target == undefined)) {
                ns.cleanLastHighlightFlow();
                return;
            }
            
            ns.highlightFlow(ns.lastStartNodeController, ns.lastStartEdgeController);
            if (ns.lastStartEdgeController == undefined) {
                ns.lastStartNodeController.select(true);
            }
        }
    };
    
    ns.cleanLastHighlightFlow = function() {
        ns.lastStartNodeController = null;
        ns.lastStartEdgeController = null;
    };
    
    /**
     * Funkce se stara o zvyrazneni toku od zadaneho uzlu ci hrany.
     *
     * Algoritmus pracuje ve dvou fazich - dopredne a zpetne. V kazde fazi se pro
     * kliknuty objekt najdou hrany na atomicke urovni. V pripade startovni hrany
     * to jsou jeji potomci na nejnizsi urovni. Neni-li startovni hrana definovana
     * najdou se atomicke uzly ke startovnimu uzlu. Podle smeru (dopredny/zpetny) se
     * pak berou odchozi/prichozi hrany techto atomickych uzlu. Ty ale jeste nemusi
     * byt nutne samy atomicke, takze se dohledaji atomicke hrany pro tyto vstupni/
     * vystupni hrany. Mezivysledkem je mnozina atomickych hran.
     * Tuto mnozinu prochazime s kontrolou IDcek jiz zpracovanych hran, abychom se
     * nezacyklili. Pro kazdou takovouhle hranu zvyraznime prvni viditelny rodicovsky
     * uzel na SRC i DST strane a hranu samotnou. Podle smeru pak opet pro cil nebo
     * zdroj projdem vystupni/vstupni hrany a jejich atomicke potomky si pridame do
     * mnoziny ke zpracovani.
     * Na zaver znovu zakesujeme vsechny uzly nejvyssi urovne (prime potomky
     * nodeLayer) a prekreslime scenu.
     *
     * @param startNodeController controller pro vychozi uzel pro zvyrazneni
     * @param startEdgeController controller pro vychozi hranu pro zvyrazneni
     */
    ns.highlightFlow = function(startNodeController, startEdgeController) {
        ns.lastStartNodeController = startNodeController;
        ns.lastStartEdgeController = startEdgeController;
        
        var i, iLen, j, jLen;
        var rep = startNodeController.graphController;
        var open;
        var edges;
        var visitedIDs;
        var one = [];
        var leafNodes = [];
        var leafEdges = [];
        var highlightedFwdNodes = {};
        var highlightedFwdEdges = {};

        ns.startTime = (new Date).getTime();

        // graphController, true = fadeout out other
        ns.unHighlightAll(startNodeController.graphController, true);

        //startNodeController.getWidget().highlight();
        if (startEdgeController !== undefined) startEdgeController.getWidget().highlight(ns.Direction.FWD);

        // FORWARD HIGHLIGHT
        visitedIDs = {};
        open = [];
        if (startEdgeController === undefined) {
            // vezmem vsechny atomicke uzly od daneho
            leafNodes = ns.getLeafNodes(startNodeController.model);
            // a z kazdeho vsechny vystupni hrany
            for(i=0, iLen = leafNodes.length; i < iLen; i++) {
                edges = leafNodes[i].edgesOut;
                for(j=0, jLen = edges.length; j < jLen; j++) {
                    open.append(ns._getLeafEdges(edges[j]));
                }
            }
        } else {
            open.append(ns._getLeafEdges(startEdgeController.model));
        }

        while (open.length > 0) {
            one = open.shift();
            visitedIDs[one.id] = true;
            
            if(one.target != undefined && !one.target.isFilteredOrUnvisited() && one.target !== startNodeController.model) {
                this._highlightFwd(this._getFirstVisibleParentNodeController(one.target, rep), highlightedFwdNodes);
            }
            this._highlightFwd(this._getFirstVisibleParentEdgeController(one, rep), highlightedFwdEdges);
       
            // u FILTER jen jeden krok, u DIRECT jedem dál
            if(one.type === Manta.Semantics.Edge.EdgeType.DIRECT) {
                // projdem vystupni hrany
                edges = one.target.edgesOut;
                for(i=0, iLen=edges.length; i < iLen; i++) {
                    leafEdges = ns._getLeafEdges(edges[i]);
                    for(j=0, jLen=leafEdges.length; j < jLen; j++) {
                        if (visitedIDs[leafEdges[j].id] === undefined) {
                            open.pushOnce(leafEdges[j]);
                        }
                    }
                }
            }
        }

        //BACKWARD HIGHLIGHT
        visitedIDs = {};
        open = [];
        if (startEdgeController === undefined) {
            // vezmem vsechny atomicke uzly od daneho
            leafNodes = ns.getLeafNodes(startNodeController.model);
            // a z kazdeho vsechny vystupni hrany
            for(i=0, iLen = leafNodes.length; i < iLen; i++) {
                edges = leafNodes[i].edgesIn;
                for(j=0, jLen = edges.length; j < jLen; j++) {
                    open.append(ns._getLeafEdges(edges[j]));
                }
            }
        } else {
            leafEdges = ns._getLeafEdges(startEdgeController.model);
            this._highlightBwd(startNodeController, highlightedFwdEdges);
            open.append(leafEdges);
        }
        while (open.length > 0) {
            one = open.shift();
            visitedIDs[one.id] = true;
            
            if(one.source != undefined && !one.source.isFilteredOrUnvisited() && one.source !== startNodeController.model) {
                this._highlightBwd(this._getFirstVisibleParentNodeController(one.source, rep), highlightedFwdNodes);
            }
            this._highlightBwd(this._getFirstVisibleParentEdgeController(one, rep), highlightedFwdEdges);
            // u FILTER jen jeden krok, u DIRECT jedem dál
            if(one.type === Manta.Semantics.Edge.EdgeType.DIRECT) {
                // projdem vstupni hrany
                edges = one.source.edgesIn;
                for(i=0, iLen=edges.length; i < iLen; i++) {
                    leafEdges = ns._getLeafEdges(edges[i]);
                    for(j=0, jLen=leafEdges.length; j < jLen; j++) {
                        if (visitedIDs[leafEdges[j].id] === undefined) {
                            open.pushOnce(leafEdges[j]);
                        }
                    }
                }
            }
        }
        //console.log('Flow highlighting execution time: ' + ((new Date).getTime() - ns.startTime)/1000 + ' s');
        ns.startTime = (new Date).getTime();
        // teoreticky by stacilo, jen ty zmenene, ale tohle je uz docela rychle
        rep.getWidget().recacheAllHighestLevelNodes();
        Manta.DataflowUI.redrawStage();
        //console.log('Flow highlighting redraw stage time: ' + ((new Date).getTime() - ns.startTime)/1000 + ' s');
    };

    /**
     * Zvyrazni dopredny tok a zapise zvyraznene elementy do kolekce
     * @param controller controller elementu ktery se zvyraznuje (EdgeController/NodeController)
     * @param highlighted seznam jiz zvyraznenych uzlu, do ktereho se bude pridavat
     * @private
     */
    ns._highlightFwd = function(controller, highlighted) {
        if(controller && controller.getWidget() !== null) {
            controller.getWidget().highlight(ns.Direction.FWD);
            if(highlighted) {
                highlighted[controller.model.id] = true;
            }
        }
    };

    /**
     * Zvyrazni zpetny tok, pokud uz nejaky uzel je zvyraznen (doprednym tokem), obarvi se jak soucast cyklu
     * @param controller controller elementu ktery se zvyraznuje (EdgeController/NodeController)
     * @param highlighted seznam jiz zvyraznenych uzlu/hran
     * @private
     */
    ns._highlightBwd = function(controller, highlighted) {
        var dir;
        if(controller && controller.getWidget() !== null) {
            dir = highlighted[controller.model.id] ? ns.Direction.CYCLE : ns.Direction.BWD;
            controller.getWidget().highlight(dir);
        }
    };

    /**
     * Stoupa po rodicich UZLU az narazi na prvni viditelny objekt a ten zvyrazni.
     * @param _tmp EP uzlu od nejz se hleda po rodicich smerem vzhuru
     * @param graphController graphController pro pristup k mape EP uzlu
     */
    ns._getFirstVisibleParentNodeController = function(_tmp, graphController) {
        var npm = graphController.nodeControllersMap;
        var tmpNode = _tmp;
        while (tmpNode !== null && !tmpNode.isVisible()){
            tmpNode = tmpNode.parent;
        }
        if (tmpNode !== null && npm[tmpNode.id].getWidget() !== null) {
            return npm[tmpNode.id];
        }
        return null;
    };

    /**
     * Stoupa po rodicich HRANY az narazi na prvni viditelny objekt a ten zvyrazni.
     * @param _tmp EP hrany od niz se hleda po rodicich smerem vzhuru
     * @param rep rootEditParta pro pristup k mape EP hran
     */
    ns._getFirstVisibleParentEdgeController = function(_tmp, rep) {
        var epm = rep.edgeControllersMap;
        var tmpEdge = _tmp;
        while (tmpEdge !== null && !tmpEdge.isVisible()){
            tmpEdge = tmpEdge.parent;
        }
        if (tmpEdge !== null && epm[tmpEdge.id].getWidget() !== null) {
            return epm[tmpEdge.id];
        }
        return null;
    };

    /**
     * Vraci vsechny hrany (sematics.Edge) nejnizsi urovne pro zvolenou metahranu.
     * @param fromEdge EP zvolene hrany
     * @returns {Array} pole semantickych reprezentaci hran (nikoli jejich editparty)
     */
    ns._getLeafEdges = function(fromEdge) {
        var edges = [];
        var open = [];
        var one;
        if (fromEdge.children.length === 0 ) return [fromEdge];
        open.append(fromEdge.children);
        while(open.length > 0) {
            one = open.pop();
            if (one.children.length === 0 && one.type === Manta.Semantics.Edge.EdgeType.DIRECT) {
                edges.push(one);
            }
            open.append(one.children);
        }
        return edges;
    };

    /**
     * Funkce dohleda vsechny atomicke potomky daneho uzlu
     * @param fromNode EP zvoleny uzel
     * @returns {Array} pole semantickych reprezentaci uzlu (nikoli jejich editparty)
     */
    ns.getLeafNodes = function(fromNode) {
        var nodes = [];
        var open = [];
        var one;
        if (fromNode.children.length === 0 ) return [fromNode];
        open.append(fromNode.children);
        while(open.length > 0) {
            one = open.pop();
            if (one.children.length === 0) nodes.push(one);
            open.append(one.children);
        }
        return nodes;
    };

    /**
     * Funkce se stara o odzvyraznovani toku.
     * @param rep rootEditParta, kvuli mapam EP hran i uzlu
     * @param fadeoutOther true/false urcuje, zda hrany maji vybarvit nevyrazne, 
     * protoze jina bude vybrana (true). Nebo zda do zakladni dle graf. konfigurace,
     * protoze jinou hranu nevybirame (false).
     */
    ns.unHighlightAll = function (rep, fadeoutOther) {
        var topLevelNodesToRecache = [], root;
        // projdem uzly, odzvyraznime
        $.each(rep.nodeControllersMap, function(key, nodeEditPart) {
            if (nodeEditPart.widget!==null) {
                // odzvyraznime jen zvyraznene
                if (nodeEditPart.widget.highlighted) {
                    nodeEditPart.widget.unhighlight();
                    root = nodeEditPart.widget.getCachedParent();
                    if(root) {
                        topLevelNodesToRecache.push(root);
                    }
                }
            }
        });
        // projdem hrany, odzvyraznime
        $.each(rep.edgeControllersMap, function(key, edgeEditPart) {
            if (edgeEditPart.widget !== null) {
                edgeEditPart.widget.unhighlight();
                if (fadeoutOther) edgeEditPart.widget.setColor(edgeEditPart.widget.gfxcnf['fadeoutColor']);
            }
        });
        // prekesujem uzly nevyssi urovne, kde se v potomcich neco menilo
        $.each(topLevelNodesToRecache.unique(), function(key, nodeWidgetVisual) {
            nodeWidgetVisual.cache();
        });
    };
    
    /**
     * Export kompletniho grafu do PNG.
     * 
     * Zavisi na: bootstrap-dialog.min.js
     * Rozsahlymi grafy byva vykonostni problem v nekterych prohlizecich. Obraz 3700*1600px 
     * se jeste dari povetsinou exportovat.
     * 
     * Pro volitelny export s nepruhlednym pozadim je treba nakreslit umele pozadi jako obdelnik. 
     * Ten se pak zase musí smazat. Taktez je potreba docasne vynulovat zoom a posunou grafiku do
     * uvodni pozice. Nasledne spocitame globalni boundingbox sceny a provedeme toImage() a obrazek
     * nabidneme v dialogu ke stazeni. Na zaver je treba opravit pozici i zoom sceny.
     */
    ns.export2PNG = function (sessionIndex) {
        ns.startTime = (new Date).getTime();
        
        /** Kolik bytů by měl zvládnout stáhnout chrome. */
        var MAXIMUM_SIZE = 1572825;
        
        /** pokud bude true, nevlozime pozadi bileho obdelniku a bude pruhledne */
        var printWatermark = Manta.PresentationManager.getGlobalSettings('showWatermark', false);
        
        var rep = Manta.DataflowUI.rep;
        var globBBox = rep.getWidget().globalBoundingBox();
        var oldScale = stage.scale();
        var oldWidth = stage.width();
        var oldHeight = stage.height();
        var oldOffX = stage.offsetX();
        var oldOffY = stage.offsetY();
        var tmpLayer, tmpRect, tmpImg;
        
        var movedBy = {x:0, y:0};
        // drobny posun, protoze jinak existuje prilis velky horni levy okraj (vpravo dole je to presne na hranu)
        var correction = {
            x: 0,
            y: 0
        };
        
        var dialog = new BootstrapDialog({
            title: 'Exporting PNG image',
            message: 'Exporting PNG image. Please wait...',
            closable: false,
            closeByBackdrop: false,
            closeByKeyboard: false,
            animate: false
        });
        dialog.open();
        
        // informovat server jen kvuli statistikam
        $.get(Manta.Globals.contextPath + "/viewer/dataflow/export-png/" + sessionIndex);
        
        // Overime, zda prohlizec podporuje data URI v <a href="...
        // Vysledek bude ulozen v parametru dataHrefSupported, ktery ovlivni rozhodnuti,
        // zda zobrazit odkaz na stazeni obrazku ci nikoliv.
        Manta.Utils.browserSupportsDataHref(function(dataHrefSupported) {
            
            // umisteno v timeoutu, aby byl cas na prekresleni a zobrazeni dialogu 
            // (pri velkych obrazcich se nestihlo zobrazit nacitaci okno)
            setTimeout(function() {
                
                //netreba prekreslovat :-), ale BBox je nutne prepocitavat pri zmenach        
                stage.offsetX(0);
                stage.offsetY(0);
                stage.scale({x:1, y:1});        
                globBBox = rep.getWidget().globalBoundingBox();
                stage.move({x: -1*globBBox.x, y: -1*globBBox.y});
                // potreba ulozit velikost posunu, protoze bbox si prepisem
                movedBy.x = globBBox.x;
                movedBy.y = globBBox.y;
                globBBox = rep.getWidget().globalBoundingBox();
                stage.setWidth(globBBox.width + 2*correction.x);
                stage.setHeight(globBBox.height + 2*correction.y);
         
                // umele pozadi, jinak by se z pruhledneho pozadi stalo cerne
                tmpLayer = new Kinetic.Layer();
                stage.add(tmpLayer);
                if(printWatermark) {
                    tmpImg = new Image();                 
                    if (printWatermark) {
                        tmpImg.src = Manta.Globals.contextPath + Manta.PresentationManager.getGlobalSettings('watermark', '');
                    } else {
                        tmpImg.src = '';    
                    }
                    tmpRect = new Kinetic.Rect({
                        x: Math.min(0, globBBox.x),
                        y: Math.min(0, globBBox.y),
                        width: globBBox.width + 2*correction.x,
                        height: globBBox.height + 2*correction.y,
                        fillPatternImage: tmpImg
                    });
                    tmpLayer.add(tmpRect);
                }
                tmpLayer.moveToBottom();

                // asynchronni, nesmime vracet stage do puvodniho stavu po tomhle volani, ale v callbacku uvnitr
                stage.toDataURL({
                    mimeType: "image/png",
                    x: correction.x,
                    y: correction.y,
                    width: globBBox.width + correction.x,
                    height: globBBox.height + correction.y,
                    callback: function(dataUrl) {
                      var dialogHeight;
                      // Maximalni vyska dialogu - o neco mensi, nez vyska okna
                      var maxDialogHeight = $(window).height() - 60;
                      var img;
                      var imgTargetHeight;
                      
                      var msg = $('<div style="text-align: center;"></div>');
                      msg.append('<img src="'+dataUrl+'" alt="Export preview." style="max-width:500px;max-height:300px"/> <br /><br />');
                      var byteLength = dataUrl.length * 0.75; // prevod na pocet bytů z base64
                      if(dataHrefSupported && Manta.Utils.browserSupportsDownloadAttribute() && byteLength < MAXIMUM_SIZE) {
                          msg.append('<a href="'+dataUrl+'" title="Download exported image." download="export.png" target="_blank">Download exported image.</a>');
                      } else {
                          msg.append('<p>Right click on image and select <em>"Save picture as..."</em> to download.</p>');
                      }

                      dialog.close();
                      dialog = new BootstrapDialog({
                          title: 'Exported PNG image',
                          message: msg,
                          buttons: [{
                              label: 'Close',
                              action: function(dialogItself){
                                  dialogItself.close();
                              }
                          }]
                      });
                      dialog.realize();
                      dialog.open();
                      // Implicitne je nastavena sirka obrazku a vyska se podle ni nastavi automaticky
                      // na zaklade pomeru stran puvodniho obrazku.
                      // Nicmene pokud je dopocitana vyska prilis velka, musime nastavit rozmery obrazku opacne
                      // - vysku napevno a sirku automaticky.
                      dialogHeight = dialog.getModalDialog().height();
                      if (dialogHeight > maxDialogHeight) {
                          // Pokud vyska dialogu presahla maximum, musime ji zmensit
                          // - toho docilime zmensenim vysky obrazku
                          img = dialog.getModalBody().find('img');
                          // Cilova vyska obrazku vznikne zmensenim aktualni vysky o rozdil mezi
                          // soucasnou a maximalni vyskou okna.
                          // Nicmene musi byt alespon 100px, aby byl videt aspon nejaky obrazek.
                          imgTargetHeight = Math.max(img.height() - (dialogHeight - maxDialogHeight), 100);
                          // Nastavime cilovou vysku a sirku nechame, aby se prizpusobila na zaklade pomeru stran obrazku
                          img.css('height', imgTargetHeight + 'px');
                          img.css('width', '');
                      }
                                    
                      // oprava prizpusobeni
                      stage.setHeight(oldHeight);
                      stage.setWidth(oldWidth);
                      stage.move({x: movedBy.x, y: movedBy.y});
                      stage.scale({x:oldScale.x, y:oldScale.y});
                      stage.offsetX(oldOffX);
                      stage.offsetY(oldOffY);
                      
                      // vyhazeni nepotrebnych pomocnych veci
                      if (tmpRect !== undefined) {
                          tmpRect.destroy();
                      }
                      tmpLayer.destroy();
                      
                      // tady uz je treba prekreslit
                      Manta.DataflowUI.redrawStage();
                      console.log('PNG export took: ' + ((new Date).getTime() - ns.startTime)/1000 + ' s');
                    }
                });
            },50);
        });
    };
    
    /**
     * Tvorba permalinku.
     * 
     * Zavisi na: bootstrap-dialog.min.js
     */
    ns.createPermalink = function (sessionIndex) {
        var link = window.location;
        var msg = '';
        var dialog;
        ns.startTime = (new Date).getTime();

        $.ajax({
            type: "POST",
            url : Manta.Globals.contextPath + "/viewer/dataflow/gen-link/" + sessionIndex,
            data: JSON.stringify(Manta.Globals.formData),
            async : false,
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            },
            success : function(data, textStatus, jqXHR) {
            	if (jqXHR.responseText.indexOf("DOCTYPE") === -1) {
	            	console.log("permalinklink = ", data.link);         	
	            	msg = $('<div style="text-align: center;"></div>');
	                msg.append('<input type="text" value="' + Manta.Utils.relativeUrlToAbsolute(Manta.Globals.contextPath + '/' + data.link) + '" title="Shortened link to this view." style="width:100%;" readonly="readonly"/>');
	                msg.append('<br/><br/><div class="text-muted">Press <kbd>CTRL + C</kbd> to copy the link to clipboard</div>');
            	} else {
            		location.reload();
            	}
            },
            error: function(jqXHR, textStatus, error) {
                msg = $('<div style="text-align: center;"></div>');
                msg.append('We are sorry, but creation of permalink failed. <br />Error: '+textStatus+':'+error);                
            }
        });
        dialog = new BootstrapDialog({
            title: 'Permanent link to this view',
            message: msg,
            buttons: [{
                label: 'Close',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }],
            animate: false,
			closable : false,
            onshown: function(dialogRef) {
                // Oznacime odkaz v textovem poli, aby to pri jeho kopirovani nemusel delat uzivatel
                dialogRef.getModalBody().find('input[type=text]')[0].select();
            }
        });
        dialog.realize();
        dialog.open(function() {alert('open!')});
    };
    
    /**
     * Zobrazi dany prikaz ve zdrojovem skriptu.
     */
    ns.showStmtInScript = function (id, sessionId) {
    	/** Zmenšení šířky dialogu oproti šířce obrazovky. */
    	var WIDTH_GAP = 400;
    	/** Zmenšení výšky dialogu oproti výšce obrazovky. */
    	var HEIGHT_GAP = 300;
    	/** O kolik je menší textarea s textem skriptu než dialog.*/
    	var DIALOG_WIDTH_PADDING = 100;
    	/** Poradove cislo zamereneho radku pri pocatecnim zaskrolovani  */
    	var FOCUSED_LINE_ORDER = 3;

        var editorDialog = $("#editorDialog");
        var editorDialogBody = $("#editorDialogBody");
    	
        $.ajax({
            type: "POST",
            url : Manta.Globals.contextPath + "/viewer/dataflow/stmt-in-script",
            data: JSON.stringify({componentId: id, sessionIndex: sessionId}),
            async : false,
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            },
            success : function(data, textStatus, jqXHR) {
            	if (jqXHR.responseText.indexOf("DOCTYPE") == -1) {
	            	if (data['sourceCode'] !== null) {
	            		editorDialogBody.empty();
		
		            	var editor = new CodeMirror(editorDialogBody.get(0), {
		                	value: data['sourceCode'],
		                	styleActiveLine: true,
		                	smartIndent: false,
		                	lineNumbers: true,
		                	readOnly: true,
		                	matchBrackets : true,
		                    autofocus: true,
		                	mode: "text/x-plsql",
		                	// Pole pro fulltextove hledani se po stisku "Enter" nezavre,
		                	// ale umozni najit dalsi vyskyt
		                	extraKeys: {"Ctrl-F": "findPersistent"},
		                	scrollbarStyle: "simple"
		                });
		            	
		            	// Schovame obsah dialogu, ktery je behem vykreslovani osklivy  
		            	editorDialogBody.find('.CodeMirror').css({
		            	    visibility: "hidden"
		            	});
		            	
		        		editorDialog.on('shown.bs.modal', function () {
		        		    editorDialogBody.find('.CodeMirror').css({
		        				width:($(window).width() - WIDTH_GAP) + 'px',
		        				height:($(window).height() - HEIGHT_GAP) + 'px',
		                        // Vykresleny dialog ma obsah jiz hezky => muzeme (a musime :-)) jej zobrazit  
                                visibility: ""
		        	        });
		        			
		        			editor.refresh();	        			
		        			//editor.markText({line: data.line - 1, ch: data.col - 1}, {line: data.line, ch: data.col - 1}, {className: 'markedText'});
		        			editor.setCursor(data.line - 1, data.col - 1);
		        			
		        			editorDialog.find('.modal-dialog').height('auto');
	                        editorDialog.find('.modal-dialog').width(($(window).width() - (WIDTH_GAP - DIALOG_WIDTH_PADDING)) + 'px');
	                        
	                        // Workaround skrolovani na oznaceny radek:
	                        // Aby oznaceny radek skoncil v horni casti nahledu:
	                        // 1) Zaskrolujeme nejdriv na posledni radek, ...
	                        editor.scrollIntoView(editor.lineCount() - 1, 0);
	                        // 2) ... potom kousek nad zamereny radek (aby nebyl prekryty polem pro vyhledavani) ...
                            editor.scrollIntoView(Math.max(data.line - FOCUSED_LINE_ORDER, 0), data.col - 1);
                            // 3) ... a nakonec primo na zamereny radek (pro pripad okna male vysky, aby nebyl schovan pod spodnim okrajem) 
                            editor.scrollIntoView(data.line - 1, data.col - 1);
	                        
	                        // Aby zafungovalo CTRL+F hned po otevreni dialogu
	                        editorDialogBody.find('textarea').focus();
		        		});
		        		
		        		editorDialog.modal();
	            	} else {
	            		Manta.DataflowUI._displayMessages([{text: 'Cannot display context for this element.', severity:'WARNING'}])
	            	}
            	} else {
            		location.reload();
            	}
            },
            error: function() {
            	Manta.DataflowUI._displayMessages([{text: 'Error during displaying statement in script.', severity:'ERROR'}])           
            }
        });
        
        
    };
    
    /**
     * Zobrazi dialog s napovedou.
     */
    ns.showHint = function (hint) {
        var hintDialog = $("#hintDialog");
        var hintDialogBody = $("#hintDialogBody");
        
        $.ajax({
            type: "POST",
            url : Manta.Globals.contextPath + "/viewer/hint/",
            data: {"hint": hint},
            async : false,
            dataType: "text",
            success : function(data, textStatus, jqXHR) {
                if (jqXHR.responseText.indexOf("DOCTYPE") == -1) {
                    // Pokud obdrzime napovedu ze serveru v poradku, zobrazime ji.
                    // V opacnem pripade neudelame nic.
                    if (jqXHR.responseText) {
                        $("#tips-and-tricks-group").show();

                        hintDialogBody.empty();
                        hintDialogBody.html(jqXHR.responseText);
                        
                        hintDialog.modal();
                    }
                }
            }
        });
    };
    
    ns.demoLogout = function(sessionId) {
        $.ajax({
            type: "GET",
            url : Manta.Globals.contextPath + "/viewer/logout/" + sessionId + "/",
            async : false,
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            }
       });
    };
    
    ns.collapseNodeToLevel = function(node, level) {
        while (node != null) {
            if (node.level === level) {
                node.collapse();
                break;
            }
            node = node.parent; 
        }
    };
    
}(Manta.Tools = Manta.Tools || {}, jQuery));
