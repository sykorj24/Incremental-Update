/**************************************************************************
* @namespace    Manta.Exceptions
* @author       Matyas Krutsky
* 
* Vyjimky.
* 
**************************************************************************/

(function(ns, $, undefined){
    
    /**************************************************************************
     * Spolecny predek pro vsechny vyjimky 
     */ 
    ns.Exception = function(excName, msg) {
        this.excName = excName;
        this.msg = msg;
    };    
    
    ns.Exception.prototype.toString = function() {
        return this.excName + ": " + this.msg;
    };
    
    ns.IllegalStateException = function(msg) {
        ns.Exception.call(this, "Illegal State", msg);
    };    
    ns.IllegalStateException.prototype = Object.create(ns.Exception.prototype);
    
    ns.IllegalArgumentException = function(msg) {
        ns.Exception.call(this, "Illegal Argument", msg);
    };    
    ns.IllegalArgumentException.prototype = Object.create(ns.Exception.prototype);
    
}(Manta.Exceptions = Manta.Exceptions || {}));