/**************************************************************************
 * @namespace    Manta.Cache
 * @author       Oldrich Nouza
 * 
 * Podpora cacheovani dat
 * 
 **************************************************************************/
var Manta = Manta || {};
(function(ns, $, undefined) {

    /**
     * Jednoducha cache hodnot prislusnych k elementum.
     * Elementy musi mit atribut <code>id</code>, ktery je identifikuje.
     * Pri dotazu na hodnotu je na vstupu element, k nemuz se ma hodnota vztahovat, a getter.
     * Pokud je hodnota v cache ulozena a cache je zapnuta, vrati se tato hodnota.
     * Pokud hodnota v cache ulozena neni a cache je zapnua, ziska se hodnota predanym getter a vlozi se do cache.
     * Pokud je cache vypnuta, vrati se hodnota ziskana predanym getterem, bez vlozeni do cache.
     */
    ns.SimpleCache = function() {
        this.data = {};
        this.on = false;
    };
    
    /**
     * Vycisti cache.
     */
    ns.SimpleCache.prototype.clean = function() {
        this.data = {};
    };
    
    /**
     * Zapne cache.
     */
    ns.SimpleCache.prototype.turnOn = function() {
        this.on = true;
    };
    
    /**
     * Vypne cache.
     */
    ns.SimpleCache.prototype.turnOff = function() {
        this.on = false;
    };
    
    /**
     * Vrati hodnotu prislusnou k danemu elementu dle pravidel popsanych v konstruktoru (viz vyse).
     * @param element Element, k nemuz se vztahuje dotazovana hodnota. Element musi mit atribut <code>id</code>.
     * @param getter {function} getter, jehoz zavolanim (bez parametru) se ziska hodnota, pokud neni v cache nebo je cache vypnuta.
     * @returns Hodnota prislusna k danemu elementu dle pravidel popsanych v konstruktoru (viz vyse).  
     */
    ns.SimpleCache.prototype.get = function(element, getter) {
        if (this.on) {
            cachedValue = this.data[element.id];
            if (cachedValue === undefined) {
                cachedValue = getter();
                this.data[element.id] = cachedValue; 
            }
            return cachedValue;
        } else {
            return getter();
        }
    };

    /**
     * Cache vyfiltrovanosti uzlu. 
     */
    ns.NODE_FILTERING_CACHE = new ns.SimpleCache(); 

}(Manta.Cache = Manta.Cache || {}, jQuery));