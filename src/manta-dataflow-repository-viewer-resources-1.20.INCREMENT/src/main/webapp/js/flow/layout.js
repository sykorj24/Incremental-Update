/**************************************************************************
* @namespace    Manta.Layout
* @author       Martin Podloucky
* 
* Rozvrhovani grafu do roviny. Obsahuje take provizorni layotovaci 
* algoritmus pro testovani a debugovani, nez bude hotovy nejaky lepsi.   
* 
**************************************************************************/

(function (ns) {

    /**************************************************************************
     * Zaklad pro vsechny layouty. 
     */
    ns.Base = function (graph) {
        this.graph = graph;
    };

    
}(Manta.Layout = Manta.Layout || {}));
