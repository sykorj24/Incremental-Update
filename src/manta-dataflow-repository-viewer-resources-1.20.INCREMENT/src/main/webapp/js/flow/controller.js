/**************************************************************************
* @namespace    Manta.Controller
* @author       Martin Podloucky, Martin Slapak
* 
* Controller umoznujici interaktivitu grafu.
* 
**************************************************************************/

(function(ns, $, undefined) {

    /*********************************************************************
     * Zakladni controller pro vsechny ostatni controllery.
     * 
     * @param 	model	Semanticky prvek, ke kteremu controller patri.
     * @param 	graphController	Takzvany korenovy controller (GraphController)
     */
    ns.Controller = function(model, graphController) {
        this.model = model;
        this.graphController = graphController;
        this.widget = null; // Graficka prezentace spojena s timto controllerem
    };

    /**
     * Vrati grafickou reprezentaci. Pokud neexistuje, vyrobi ji.
     * @returns grafika objektu (Kinetic node) 
     */
    ns.Controller.prototype.getWidget = function() {
        if (this.widget === null)
            this.widget = this.createWidget();

        return this.widget;
    };

    /**
     * Znevalidní widget objektu.
     */
    ns.Controller.prototype.revalidateWidgets = function() {
    };

    // --------------------------------------------------------------------

    /*********************************************************************
     * Zakladni controller pro vsechny controllery, ktere obsahuje 
     * podpolozky, tedy deti.
     * @param 	model	Semanticky prvek, ke kteremu controller patri.
     * @param 	parent	Rodicovsky controller.
     */
    ns.ContainerController = function(model, parent) {
        ns.Controller.call(this, model, parent.graphController);
    };

    ns.ContainerController.prototype = Object.create(ns.Controller.prototype);

    // --------------------------------------------------------------------

    /*********************************************************************
     * Top-level controller pro cely graf. Udrzuje globlani mapu vsech 
     * controlleru, aby slo vzdy snadno najit controller k danemu 
     * semantickemu prvku.
     * @param graph semanticky model grafu
     * @param stage Kineticti objekt stage
     */
    ns.GraphController = function(graph, stage) {
        ns.ContainerController.call(this, graph, this);
        this.stage = stage; // KineticJS stage
        this.nodeControllersMap = {}; // Mapa controlleru pro uzly
        this.edgeControllersMap = {}; // Mapa controlleru pro hrany
        this.selectedControllers = []; // Vybrane controllery
        this.focusedController = null; // Zamereny controller
        this.graphController = this; // Je sam svym top-level controllerem
        this.listeners = { // Posluchaci udalosti.
            'nodeSelected' : [],
            'edgeSelected' : [],
            'highlightFlow' : []
        };
    };

    ns.GraphController.prototype = Object.create(ns.ContainerController.prototype);

    /**
     * Top-level prezentor obsahujici cely graf.
     * @returns {Object}
     */
    ns.GraphController.prototype.createWidget = function() {
        return new Manta.Graphics.GraphWidget();
    };

    /**
     * Zrusi vybrani vsech objektu. 
     */
    ns.GraphController.prototype.clearSelection = function() {
        this.selectedControllers.map(function(p) {
            p.select(false);
        });
        this.selectedControllers.length = 0;
    };

    /**
     * Oznaci dany uzel jako vybrany.
     * @param node EP uzlu, ktery se ma vybrat
     */
    ns.GraphController.prototype.selectNode = function(node) {
        this.selectedControllers.pushOnce(node);
        $.each(this.listeners['nodeSelected'], function(index, listener) {
            listener(node);
        });
    };
    
    /**
     * Oznaci danou hranu jako vybrany.
     * @param node EP hrany, ktery se ma vybrat
     */
    ns.GraphController.prototype.selectEdge = function(edge) {
        this.selectedControllers.pushOnce(edge);
        $.each(this.listeners['edgeSelected'], function(index, listener) {
            listener(edge);
        });
    };

    /**
     * Zrusi focus uzlu (pokud jej nejaky ma).
     * @returns {Boolean} <code>true</code>, pokud existoval focus na nejaky uzel
     * a byl tim padem skutecne odebran, jinak <code>false</code>.
     */
    ns.GraphController.prototype.clearFocus = function() {
        var result = false;
        if (this.focusedController) {
            this.focusedController.focus(false);
            result = true;
        }
        this.focusedController = false;
        return result;
    };

    /**
     * Nastavi focus danemu uzlu. Pokud je focus na jinem uzlu, bude mu odebran.
     * @param node uzel, ktery ma dostat focus
     */
    ns.GraphController.prototype.focusNode = function(node) {
        if (this.focusedController !== node) {
            this.clearFocus();
        }
        this.focusedController = node;
    };

    /**
     * Zpropaguje event vybrane hrany.
     * @param startNodeController EP uzlu, od nejz se ma zvyraznovat
     * @param edgeController EP hrany, od niz se ma zvyraznovat
     */
    ns.GraphController.prototype.highlightFlowEvent = function(startNodeController, edgeController) {
        $.each(this.listeners['highlightFlow'], function(index, listener) {
            listener(startNodeController, edgeController);
        });
        // pokud neni dostupny controller hrany, zacinalo se uzlem, takze ho i vyberem
        if (typeof edgeController === 'undefined') {
            startNodeController.select(true);
        } else {
            edgeController.select(true);
        }
    };

    /**
     * Zaregistrue listener pro dane jmeno udalosti. Pokud takova udalost 
     * neexistuje, vyhodi IllegalArgumentException.
     * @param eventName	jmeno udalosti
     * @param func		callback funkce	
     */
    ns.GraphController.prototype.on = function(eventName, func) {
        if (!(eventName in this.listeners)) {
            throw new Manta.Exceptions.IllegalArgumentException('Event "' + eventName
                    + '" is undefined for GraphController');
        }
        this.listeners[eventName].push(func);
    };

    /**
     * Znevalidní widget všech uzlů a hran v controlleru.
     */
    ns.GraphController.prototype.revalidateWidgets = function() {
        var nodes = this.model.getVisibleNodes();
        for (var i = 0; i < nodes.length; i++) {
            var node = nodes[i];
            var nodeController = this.nodeControllersMap[node.id];
            if (nodeController != null) {
                nodeController.revalidateWidgets();
            } else {
                console.log(node, " has null controller");
            }           
            for (var j = 0; j < node.edgesOut.length; j++) {
                edge = node.edgesOut[j];
                this.edgeControllersMap[edge.id].revalidateWidgets();
            }
        }
    };

    /**
     * Provede aktualizaci vsech controlleru podle aktualniho grafu. Metoda garantuje,
     * ze po provedeni bude sedet semanticky model a mapa kontroleru presne 1:1.
     * Zaroven zavola refresh() na kazdem kontroleru.
     */
    ns.GraphController.prototype.refresh = function() {
        var i, j, nodes = this.model.getVisibleNodes(), node, edge, newNodeControllersMap = {}, newEdgeControllersMap = {};

        // aktualizujeme mapy controlleru podle aktualniho grafu
        for (i = 0; i < nodes.length; i++) {
            node = nodes[i];
            newNodeControllersMap[node.id] = this.getControllerForNode(node);
            // musim updateovat model, aby sedel s grafem (uzel mohl byt smazan
            // a nahrazen jinou instanci se stejnym id (viz shadow uzly)
            newNodeControllersMap[node.id].model = node;
            delete this.nodeControllersMap[node.id];
            for (j = 0; j < node.edgesOut.length; j++) {
                edge = node.edgesOut[j];
                newEdgeControllersMap[edge.id] = this.getControllerForEdge(edge);
                newEdgeControllersMap[edge.id].model = edge;
                newEdgeControllersMap[edge.id].refresh();
                delete this.edgeControllersMap[edge.id];
            }
        }

        // vsechny zbyle controllery uz nejsou v grafu, je potreba je zlikvidovat
        this._destroyControllers(this.nodeControllersMap);
        this._destroyControllers(this.edgeControllersMap);

        this.nodeControllersMap = newNodeControllersMap;
        this.edgeControllersMap = newEdgeControllersMap;

        // mapy controlleru jsou obnoveny, najdem vsechny nody nejvyssi urovne
        // a od nich hierarchicky refresheneme
        for (i = 0; i < nodes.length; i++) {
            if (nodes[i].getDepth() === 0) {
                this.refreshHierarchy(nodes[i]);
            }
        }
        // zakesovani na nejvyssi urovni
        this.getWidget().recacheAllHighestLevelNodes();
    };

    /**
     * Rekurzivne refreshne vsechny controllery od daneho uzlu nize.
     * @param fromNode model uzlu, od nejz se ma refreshvat
     */
    ns.GraphController.prototype.refreshHierarchy = function(fromNode) {
        var i;
        var fromEP = this.getControllerForNode(fromNode);
        fromEP.refresh();
        for (i = 0; i < fromNode.children.length; i++) {
            this.refreshHierarchy(fromNode.children[i]);
        }
    };

    /**
     * Podiva se do mapy controlleru uzlu a vrati controller pro zadany uzel
     * pokud existuje, pokud ne tak vytvori novy (ale nevlozi ho do mapy!).
     * @param node semanticky model uzlu
     * @returns EP pozadovaneho uzlu
     */
    ns.GraphController.prototype.getControllerForNode = function(node) {
        var nodeController = this.nodeControllersMap[node.id];
        if (nodeController === undefined) {
            nodeController = new Manta.Controller.NodeController(node, this);
        }
        return nodeController;
    };

    /**
     * Podiva se do mapy controlleru hran a vrati controller pro zadanou hranu
     * pokud existuje, pokud ne tak vytvori novy (ale nevlozi ho do mapy!).
     * @param edge semanticky model hrany
     * @returns EP pozadovane hrany
     */
    ns.GraphController.prototype.getControllerForEdge = function(edge) {
        var edgeController = this.edgeControllersMap[edge.id];
        if (edgeController === undefined) {
            edgeController = new Manta.Controller.EdgeController(edge, this);
        }
        return edgeController;
    };

    /**
     * Zameri stred obrazovky na stage v danem bode.
     * @parem point Cilovy bod
     */
    ns.GraphController.prototype.centerToPoint = function(point) {
        var windowWidth = this.stage.width();
        var windowHeight = this.stage.height();
        this.stage.setPosition({
            x : -point.x + windowWidth / 2,
            y : -point.y + windowHeight / 2
        });
    };

    /**
     * Zameri stred obrazovky stred uzlu s danym ID.
     * @param nodeId ID ciloveho uzlu
     */
    ns.GraphController.prototype.centerToNode = function(nodeId) {
        var nodeBounds;
        // Kontroler ciloveho uzlu
        var nodeController;
        // Cilovy uzel
        var targetNode = this.model.nodeMap[nodeId];
        var nodeTargetCoords;

        if (targetNode && !targetNode.isVisible() && targetNode.shadowOwner) {
            // Pokud cilovy uzel neni videt a ma alespon jeden stinovy uzel, zkusime prvni z nich
            targetNode = targetNode.shadows[0];
        }

        if (targetNode === undefined || !targetNode.isVisible()) {
            // id uzlu nebylo nalezeno (uzel neexistuje)
            return;
        }

        // Aktualizujeme kontroler ciloveho uzlu
        nodeController = this.getControllerForNode(targetNode);
        // Hranice ciloveho uzlu
        nodeBounds = nodeController.getWidget().getBounds();
        // Cilove souracnice ciloveho uzlu = souradnice teziste uzlu minus souradnice stage
        nodeTargetCoords = {
            x : nodeBounds.x + nodeBounds.width / 2 - this.stage.getPosition().x,
            y : nodeBounds.y + nodeBounds.height / 2 - this.stage.getPosition().y
        };
        this.centerToPoint(nodeTargetCoords);
    };

    /**
     * Projde objekty v mape a zavola na kazdem z nich destroy
     * @param controllersMap
     */
    ns.GraphController.prototype._destroyControllers = function(controllersMap) {
        var id;
        for (id in controllersMap) {
            if (controllersMap.hasOwnProperty(id)) {
                controllersMap[id].destroy();
            }
        }
    };

    // --------------------------------------------------------------------

    /*********************************************************************
     * Controller pro jednotlive uzly
     * @param node		semanticky uzel
     * @param parent	rodicovsky controller
     */
    ns.NodeController = function(node, parent) {
        ns.ContainerController.call(this, node, parent);
        this.selected = false;
        this.focused = false;
        this.visible = true;
    };

    ns.NodeController.prototype = Object.create(ns.ContainerController.prototype);

    /**
     * Znevalidni widget, pricemz odstrani jeho listenery a spol.
     */
    ns.NodeController.prototype.revalidateWidgets = function() {
        if (this.widget != null) {
            this.widget.destroy();
        }
        this.widget = null;
    };
    
    /** 
     * Aktualizuje prezentaci daneho semantickeho uzlu.
     */
    ns.NodeController.prototype.refresh = function() {
        var i, widget, label, parentEP, leavesId, leaf, showFollow = false;

        // pokud neni viditelny, neni co refreshit, takze ani nedojde volanim getWidget k vytvoreni grafiky
        if (this.widget === null && !this.model.isVisible())
            return false;

        widget = this.getWidget();
        // widget.setBounds(this.model.getAbsoluteBounds());
        widget.setBounds(this.model.bounds);

        widget.setVisible(this.model.isVisible());
        widget.updatePadding(this.model);
        // viditelnost follow buttonu
        if (this.model.children.length > 0) {
            leavesId = this.model.getLeavesId();
            for (i = 0; i < leavesId.length; i++) {
                leaf = this.graphController.model.nodeMap[leavesId[i]];
                // pouze pokud jsou vsechny listy v hierarchii ve stavu FINISHED nezobrazim
                showFollow = showFollow || leaf.status === Manta.Semantics.Node.NodeType.VISITED;
            }
        } else {
            showFollow = this.model.status === Manta.Semantics.Node.NodeType.VISITED;
        }
        widget.displayFollowButton(showFollow);

        widget.displayExpandButton(this.model.children.length > 0);
        widget.setExpandButtonState(this.model.expanded);

        label = this.model.label || '';
        // label = '(' + (this.model.id || '') + ') ' + label;

        widget.setLabel(label);

        parentEP = (this.model.parent === null) ? null : this.graphController.nodeControllersMap[this.model.parent.id];
        if (parentEP === null) {
            this.graphController.getWidget().addChild(widget);
        } else if (parentEP.model.isVisible()) {
            parentEP.getWidget().addChild(widget);
        }

        // toto snad ne, ale objekty se musi nutne pridavat i refreshovat ve spravnem poradi dle hierarchie
        widget.visual.moveToTop();
    };

    /**
     * Vytvori prezentor pro dany semanticky uzel. 
     * @returns {Object} graficka reprezentace uzlu (Kinetic node)
     */
    ns.NodeController.prototype.createWidget = function() {
        var gfxcnf = Manta.PresentationManager.getGFXConf(this.model);
        var widget = new Manta.Graphics.NodeWidget(this.model, gfxcnf);
        var self = this;
        var parentEP;

        // Je treba zaregistrovat reakce na udalosti.
        widget.onClick(function(evt) {
            var origEvt = evt.evt;
            // LEFT click
            if (origEvt.button === 0) {
                evt.cancelBubble = true;
                self.graphController.clearSelection();

                self.graphController.highlightFlowEvent(self, undefined);

                // self.select(!self.selected);
                self.select(true);
                // self.graphController.getWidget().visual.draw();
                // nutno prekreslovat vse
                Manta.DataflowUI.redrawStage();
            }
        });

        // obsluha kliknu na follow tlacitko
        widget.onFollowClick(function(evt) {
            var origEvt = evt.evt;
            // LEFT click
            if (origEvt.button === 0) {
                Manta.DataflowUI.activeUnselectHandler = false;
                var op = Window.operationContext.getOperation(Manta.Operations.Follow.NAME);
                evt.cancelBubble = true; // at nevyskoci kontextove menu z kinetic group celeho nodu
                $("#componentText").val(self.model.id);
                op.setNodeToCenterOnId(self.model.id);
                op.invoke(self.model.getLeavesId(), self.model.getLowestExpandedLevel(), 1, Manta.DataflowUI.getActiveFilters());
                Manta.DataflowUI.delayedActivateUnselectHandler(); 
            }
        });

        // obsluha kliknu na expand tlacitko
        widget.onExpandClick(function(evt) {
            var origEvt = evt.evt;
            // LEFT click
            if (origEvt.button === 0) {
                Manta.DataflowUI.activeUnselectHandler = false;
                var op = Window.operationContext.getOperation(Manta.Operations.ExpandCollapse.NAME);
                evt.cancelBubble = true; // at nevyskoci kontextove menu z kinetic group celeho nodu
                op.invoke(self.model);
                Window.operationContext.collapseManager.changeNode(self.model);
                Manta.DataflowUI.delayedActivateUnselectHandler();        
            }
        });

        // Uzly vrstvime do sebe, takze musime zjistit, zda jsme na topLevelu
        // podle toho pak urcime kam se ma aktualni uzel umistit
        parentEP = (this.model.parent === null) ? null : this.graphController.nodeControllersMap[this.model.parent.id];
        if (parentEP === null) {
            this.graphController.getWidget().addChild(widget);
        } else {
            parentEP.getWidget().addChild(widget);
        }

        return widget;
    };

    /**
     * Oznaci dany uzel jako vybrany.
     * @param state cilovy stav
     */
    ns.NodeController.prototype.select = function(state) {
        var fig = this.getWidget();
        fig.setSelected(state);
        this.selected = state;
        // pri odvybirani nechceme spustit udalost vyberu
        if (state)
            this.graphController.selectNode(this);
    };

    /**
     * Nastavi / odebere focus uzlu.
     * @param state <code>true</code>, pokud se ma focus uzlu nastavit, nebo <code>false</code>, pokud odebrat.
     */
    ns.NodeController.prototype.focus = function(state) {
        var fig = this.getWidget();
        fig.setFocused(state);
        this.focused = state;
        if (state) {
            // pri odebrani nechceme spustit udalost nastaveni focusu
            this.graphController.focusNode(this);
        }
    };

    /**
     * Zlikviduje dany controller a predevsim jeho grafickou reprezentaci tak, ze
     * ji odebere z jejiho grafickeho predka (typicky Layer)
     */
    ns.NodeController.prototype.destroy = function() {
        if (this.widget !== null) {
            this.widget.destroy();
        }
    };

    // --------------------------------------------------------------------

    /*********************************************************************
     * Controller pro jednotlive hrany
     * @param edge			semanticka hrana
     * @param graphController	top-level controller
     */
    ns.EdgeController = function(edge, graphController) {
        ns.Controller.call(this, edge, graphController);
    };

    ns.EdgeController.prototype = Object.create(ns.Controller.prototype);

    /**
     * Vytvori prezentor hrany a prida ho na Widget sveho predka.
     * @returns {Object} graficka reprezentace hrany (Kinetic node)
     */
    ns.EdgeController.prototype.createWidget = function() {
        var gfxcnf = Manta.PresentationManager.getGFXConf(this.model);
        var widget = new Manta.Graphics.EdgeWidget(this.model, gfxcnf);
        var self = this;

        widget.onClick(function(evt) {
            var origEvt = evt.evt;
            if (origEvt.button === 0) {
                // left click
                self.graphController.clearSelection();
                self.graphController.highlightFlowEvent(self.graphController.nodeControllersMap[self.model.source.id],
                        self);
            }
        });

        // graficke objekty pridam uz tady, abych zarucil, ze se urcite pridaji vzdycky jenom jednou
        this.graphController.getWidget().addChild(widget);

        return widget;
    };

    /**
     * Aktualizuje prezentaci controlleru podle semnatiky. Jde predevsim
     * o nastveni source a target anchor pro polyline, ktera predstavuje hranu.
     */
    ns.EdgeController.prototype.refresh = function() {
        var widget;

        // pokud neni viditelny, neni co refreshit, takze ani nedojde volanim getWidget k vytvoreni grafiky
        if (this.widget === null && this.model.isVisible() === false)
            return false;

        widget = this.getWidget();
        widget.sourceAnchor = this.model.source.getAbsoluteBounds();
        widget.targetAnchor = this.model.target.getAbsoluteBounds();
        widget.bendpoints = $.extend(true, [], this.model.bendpoints);
        widget.selfLoop = this.model.isSelfLoop();
        widget.setVisible(this.model.isVisible());

        widget.update();
    };
    
    /**
     * Oznaci dany uzel jako vybrany.
     * @param state cilovy stav
     */
    ns.EdgeController.prototype.select = function(state) {
        // pri odvybirani nechceme spustit udalost vyberu
        if (state) {
            this.graphController.selectEdge(this);
        }
    };

    /**
     * Zlikviduje dany controller a predevsim jeji grafickou reprezentaci tak, ze
     * ji odebere z jejiho grafickeho predka (typicky Layer)
     */
    ns.EdgeController.prototype.destroy = function() {
        if (this.widget !== null) {
            this.widget.visual.remove();
        }
    };

    /**
     * Obsluha operace Unfilter
     */
    ns.EdgeController.prototype.unfilter = function() {
        var op = Window.operationContext.getOperation(Manta.Operations.Unfilter.NAME);
        op.invoke(true, [ this.model ]);
    };

}(Manta.Controller = Manta.Controller || {}, jQuery));
