/*******************************************************************************
 * @namespace Manta.Operations
 * @author Tomas Fechtner
 * 
 * Operace pro zjisteni, jestli jsou uzly skryte danymi filtry.
 * 
 ******************************************************************************/

(function(ns) {

    ns.HiddenByFilters = function(context) {
        ns.BaseOperation.call(this, ns.HiddenByFilters.NAME, context);
    };

    ns.HiddenByFilters.prototype = Object.create(ns.BaseOperation.prototype);
    ns.HiddenByFilters.NAME = 'hiddenByFilters';

    /**
     * Zavola operaci na dane uzly pro dane filtry. 
     * 
     * @param componentIds mnozina id uzlu
     * @param activeFilters mnozina id aktivnich filtru
     * @param callback po obdrzeni odpovedi
     */
    ns.HiddenByFilters.prototype.invoke = function(componentIds, activeFilters, callback) {
        if(!componentIds || componentIds.length === 0) {
            this.finish();
            return;
        }

        this.resetGraph();
        request = {
            sessionIndex        : this.context.sessionIndex,
            componentIds        : componentIds,
            activeFilters       : activeFilters
        };
        this.context.connection.sendRequest(request, this.name, null, callback);
    };

    /**
     * 
     * @param response Odpoved ze serveru.
     * @param callback Funkce, ktera se zavola po zpracovani odpovedi
     */
    ns.HiddenByFilters.prototype.receiveResponse = function(response, callback) {
        for (var i in response.filterStates) {
            var id = i;
            var state = response.filterStates[i];
            var node = this.context.graph.getNode(id);
            if (node !== undefined) {
                node.owner.setFiltered(state);
            } else {
                console.log("Requested component with id " + filterState.id + " doesn't exist in graph.");
            }            
        }
        
        if (callback) {
            // vykresleni a zavolani finish deleguju na callback
            callback();
        } else {
            this.finish();
        }       
    };
}(Manta.Operations = Manta.Operations || {}));
