(function (ns) {

    ns.Iterator = function(arr, forward) {
        this.arr = arr;
        this.len = arr.length;
        this.forward = forward;
        this.index = forward ? 0 : this.len - 1;
    };

    ns.Iterator.prototype.hasNext = function() {
        return this.forward ? this.index < this.len : this.index >= 0;
    };

    ns.Iterator.prototype.next = function() {
        return this.arr[this.forward ? this.index++ : this.index--];
    };

    ns.toArray = function(obj) {
        var key,
            result = [];
        for(key in obj) {
            if(obj.hasOwnProperty(key)) {
                result.push(obj[key]);
            }
        }
        return result;
    };

    /**
     * Prevede relativni URL na absolutni v kontextu aktualni stranky.
     * @param relativeUrl Relativni URL
     * @returns Absolutni URL
     */
    ns.relativeUrlToAbsolute = function(relativeUrl) {
        // Pomocne ukotveni pro snadne ziskani URL pomoci jQuery
        var tempAnchor = $('<a href="' + relativeUrl + '"></a>');
        return tempAnchor[0].href;
    };

    /**
     * Vrati id technologie pro predany uzel
     * @param node uzel, pro ktery hledam id technologie na zaklade jeho pole
     * @returns {*} id technologie uzlu, nebo <code>undefined</code>, pokud
     * zadana technologie neni v seznamu resource.
     */
    ns.getTechId = function(node) {
        //return "" + node.topParentTech;
        return Manta.Globals.resourceMap[node.tech];
    };

    /**
     * Rozhodne zda uzel je startovnim uzlem na zaklade globalne nastavenych parametru
     * @param node
     * @returns {*|boolean|Boolean}
     */
    ns.isStartingNode = function(node) {
        return Manta.Globals.formData.selectedItems.contains(node.id);
    };

    /**
     * Prevede textovou reprezentaci urovne na numerickou
     * @param levelString textova reprezentace urovne (tj. "TOP"|"MIDDLE"|"BOTTOM")
     * @returns {number} cislo urovne, nebo vyhodi IllegalArgumentException, pokud
     * zadany levelString neni validni
     */
    ns.getNumericLevelValue = function(levelString) {
        switch (levelString) {
            case "TOP":
                return 0;
            case "MIDDLE":
                return 1;
            case "BOTTOM":
                return 2;
            default:
                throw new Manta.Exceptions.IllegalArgumentException("Level not supported: " + levelString);
        }
    };

    /**
     * Rozhdne zda pouzivany prohlizec podporuje atribut "download" pro anchor element
     * @returns {boolean} true, pokud prohlizec podporuje download atribut
     */
    ns.browserSupportsDownloadAttribute = function() {
        return document.createElement('a')['download'] !== undefined;
    };
    
    /**
     * Zjisti, zda pouzivany prohlizec podporuje data URI v atributu 'href' tagu 'a'.
     * Test provede zkusebim pozadavkem na testovaci data URI a po obdrzeni odpovedi provola
     * callback s vysledkem.
     * @param resultCallback Callback, ktery se po obdrzeni odpovedi testovaciho pozadavku provola s jednim parametrem.
     *                       Jeho hodnota je 'true', pokud je data URI podporovan, nebo 'false' v opacnem pripade.
     */
    ns.browserSupportsDataHref = function(resultCallback) {
    	// Na datech nezalezi, potrebujeme jen otestovat zda se stahnou => X postaci
        var request = $.get("data:text/plain,X");
        request.done(function() {
            resultCallback(true);
        });
        request.fail(function() {
            resultCallback(false);
        });
    };
    
    ns.getSelectedItem = function(grapController) {
        var selectedItem = grapController.model.nodeMap[Manta.Globals.formData.selectedItems[0]];
        while (selectedItem != null) {
            if (selectedItem.parent == null || selectedItem.parent.expanded) {
                return selectedItem;
            }
            selectedItem = selectedItem.parent;
        }
        
        return undefined;
    };
    
    ns.equalsById = function(a, b) {
        return a.id != undefined && b.id != undefined && a.id == b.id;
    }

}(Manta.Utils = Manta.Utils || {}));

/**
 * Pomocna funkce pro rychly pristup k prvnimu prvku pole.
 */
Array.prototype.first = function() {
	return this[0];
};

/**
 * Pomocna funkce pro rychly pristup k poslednimu prvku pole.
 */
Array.prototype.last = function() {
	return this[this.length-1];
};

/**
 * Pomocna funkce pro odstraneni zadaneho prvku z pole.
 * @param obj Objekt, ktery se ma z pole odstranit
 * @param equals Porovnavaci funkce. Pokud neni definovana, provede se porovnani overenim identity
 * @returns {boolean} <code>true</code>, pokud byl prvek nalezen a skutecne odstranen, jinak <code>false</code>
 */
Array.prototype.remove = function(obj, equals) {
    if (equals === undefined) {
        equals = function(a, b) {
            return a === b;
        }
    }
    
    var i;
    for(i = 0; i < this.length; i++) {
        if(equals(this[i], obj)) {
            this.splice(i, 1);
            return true;
        }
    }
    return false;
};

/**
 * Pripoji na konec pole jine pole. Pokud argument neni pole, nedela nic.
 * @param other pole (objekt typu Array), ktere se ma pripojit
 */
Array.prototype.append = function (other) {
    if(other instanceof Array)
        other.forEach(function(v) {this.push(v);}, this);
};

/**
 * Pripoji na konec pole jine pole tak, ze zadny prvek z pripojovaneho pole se nepripoji,
 * pokud uz se v poli vyskytuje. Pokud argument neni pole, nedela nic.
 * @param other pole (objekt typu Array), ktere se ma pripojit
 */
Array.prototype.appendUniqueElements = function (other) {
    if(other instanceof Array)
        other.forEach(function(v) {this.pushOnce(v)}, this);
};

/**
 * Rozhodne zda pole obsahuje zadany prvek
 * @param element pole, ktery hledame
 */
Array.prototype.contains = function (element) {
    return this.indexOf(element) >= 0;
};

/**
 * Vlozi objekt do pole prave jednou, pokud uz objekt je v poli, nedela nic
 * @param element pole, ktery vkladame
 */
Array.prototype.pushOnce = function (element) {
    if(!this.contains(element)) {
        this.push(element);
    }
};

/**
 * Provede unifikaci pole.
 * @returns {Array} unifikovane pole
 */
Array.prototype.unique = function() {
    var i, unique = [];
    for (i = 0; i < this.length; i++) {
        if (unique.indexOf(this[i]) === -1) {
            unique.push(this[i]);
        }
    }
    return unique;
};

/**
 * Vycentruje dialog vzhledem k elementu, ve kterem je umisten
 */
BootstrapDialog.prototype.center = function() {
    // Cilove souradnice leveho horniho rohu dialogu
    var targetTop = (this.getModal().height() - this.getModalDialog().height()) / 2;
    var targetLeft = (this.getModal().width() - this.getModalDialog().width()) / 2;
    // Standardne je pozice dialogu dana vlastnosti 'margin', proto ji patricne nastavime
    this.getModalDialog().css('margin', targetTop + 'px ' + targetLeft + 'px');
};