/*************************************************************************
 * Veskera logika hlavni stranky pro zobrazovani toku.
 *
 * @author	Martin Podloucky, Martin Slapak
 *
 *************************************************************************/
// defaultni globalni hodnoty:
window.Manta.Globals = window.Manta.Globals || {
    contextPath : "",
    formData: {
        level: 'BOTTOM',
        direction: 'BOTH',
        depth: 1,
        filter: 1,
        filterEdges: false,
        selectedItems: [ ]
    },
    resourceMap: {}
};

$(document).ready(function () {
    $(document).tooltip({ position: { my: "left+15 center", at: "right center" } });
});

(function (ns, $, undefined) {
    // ===========================================================================================================
    // promenne a inicializace
    // ===========================================================================================================
	// Inizializace zakladnich datovych struktur.
	var connection = new Manta.Server.Connection(Manta.Globals.contextPath + "/viewer/dataflow/");

	// ID session - pro kazdou zalozku prohlizece samostatne
    var sessionIndex = $('#session-index').data('sessionindex');

    // Manager pro filtrovani
    var filterManager = new Manta.FilterSettings.FilterManager();
    
	var graph = new Manta.Semantics.Graph(filterManager);
	
	filterManager.setGraph(graph);

    var basicNB = new Manta.PresentationManager.BasicNodeBounds();
    
    var refresher = new Manta.Refresher.Flow(sessionIndex);

    // velikost kroku pri zoomovani
    var zoomFactor = 1.1;
    // krajni meze zoomu
    var minimumScale = 0.3;
    var maximumScale = 1;
    // Povolena odchylka pri porovnavani zoomu vuci krajnim mezim.
    // Hodnota je urcena empiricky na zaklade vypozorovanych zaokrouhlovacich chyb pri opakovanem zoomovani.
    var scaleBoundsTolerance = 1E-8;
    
    var canvasWrapper = $('div#mainCanvas');

    // inicializace KineticJS stage
    // musi byt draggable, protoze kdyz by to bylo jen v ramci vrstvy, nejde posouvat chytem za prazdna mista!
    var stage = new Kinetic.Stage({
        container: 'mainCanvas',
        width: canvasWrapper.width(),
        height: canvasWrapper.height(),
        listening: true,
        draggable: true,
        dragDistance: 3
    });

    var lm = new Manta.Layout.Hierarchical(graph, 1024, 1024, basicNB);
    var rep = new Manta.Controller.GraphController(graph, stage);
    // pocatecni stav stage (pred posunutim kamkoliv pomoci drag)
    var origin = { x: 0, y: 0 };
        
    // Manager pro hromadne sbaleni a rozbaleni.
    var collapseManager = new Manta.CollapseSettings.CollapseManager(graph);
    
    // Manager, ktery spravuje stavy Undo/Redo
    var undoManager = new Manta.Undo.UndoManager(graph, filterManager, stage, collapseManager);
    
    // Sablony Handlebars
    // debug varianta: var nodeHeaderTemplate = Handlebars.compile('{{label}} ({{id}})');
    var nodeHeaderTemplate = Handlebars.compile($("#node-detail-header-template").html());
    var nodeDetailTemplate = Handlebars.compile($("#node-detail-template").html());
    var edgeHeaderTemplate = Handlebars.compile($("#edge-detail-header-template").html());
    var edgeDetailTemplate = Handlebars.compile($("#edge-detail-template").html());
    var contextMenuTemplate = Handlebars.compile($("#context-menu-template").html());
    var messagesTemplate = Handlebars.compile($("#messages-template").html());
    var autocompleteItemTemplate = Handlebars.compile($("#autocomplete-item-template").html());
    var colorLayerCheckboxTemplate = Handlebars.compile($("#color-layer-checkbox-template").html());

    // deklarace pomocnych promennych pouzivanych dale v kodu
    var lastTime;

    // sledovani vykonu pomoci meridla FPS
    var stats = new Stats();
    stats.setMode(0); // 0: fps, 1: ms
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.left = '150px';
    stats.domElement.style.top = '0px';
    $('#fps').append(stats.domElement);
    setInterval( function () {
        stats.begin();
        // your code goes here (my pocitame v jinych vlakench)
        stats.end();
    }, 1000 / 60 );

    // nactem si grafickou konfiguraci z JSON
    Manta.PresentationManager.loadConfig();
    // ... a aplikuju hned globalni nastaveni:
    if(Manta.PresentationManager.getGlobalSettings('showWatermark', false)) {
    	var url = Manta.Globals.contextPath + Manta.PresentationManager.getGlobalSettings('watermark', '');
        canvasWrapper.css('background', 'url("' + url + '") left top');
    }
    if(!Manta.PresentationManager.getGlobalSettings('showFilters', true)) {
        $('#options').hide();
    }
    if(!Manta.PresentationManager.getGlobalSettings('showPermalink', true)) {
        $('#permalinkMenuItem').hide();
    }

    // pridame vrstvy uzlu i hran do stage
    stage.add(rep.getWidget().nodeLayer);
    stage.add(rep.getWidget().edgeLayer);

    /**
     * Uschovna controlleru, abychom vedeli na co bylo kliknuto.
     */
    window.contextClickController = null; // TODO - odstranit globalni promennou!!!

    window.stage = stage; // TODO - odstranit globalni promennou!!!

    // unifikace requestAnimationFrame napric prohlizecema
    window.requestAnimationFrame = window.requestAnimationFrame
                                || window.mozRequestAnimationFrame
                                || window.webkitRequestAnimationFrame
                                || window.msRequestAnimationFrame;
    // workaround requestAnimationFrame pro stare browsery
    // podpora: http://caniuse.com/requestanimationframe
    if (!window.requestAnimationFrame) {
        lastTime = 0;
        window.requestAnimationFrame = function(callback) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }

    // Inicializace operaci
    Window.operationContext = new Manta.Operations.OperationContext(graph, connection, lm, rep, stage, sessionIndex,
        function(messages) {
            ns._displayMessages(messages);
        },
        undoManager, collapseManager
    );
    Window.operationContext.registerOperation(new Manta.Operations.Follow(Window.operationContext), true);
    Window.operationContext.registerOperation(new Manta.Operations.ExpandCollapse(Window.operationContext), true);
    Window.operationContext.registerOperation(new Manta.Operations.Filter(Window.operationContext), true);
    Window.operationContext.registerOperation(new Manta.Operations.Unfilter(Window.operationContext), true);
    Window.operationContext.registerOperation(new Manta.Operations.UndoRedo(Window.operationContext), false);
    Window.operationContext.registerOperation(new Manta.Operations.HiddenByFilters(Window.operationContext), false);

    // ===========================================================================================================
    // namespace funkce a fieldy
    // ===========================================================================================================

    /**
     * Flag pro aktivaci a deaktivaci handleru odkliku.
     */
    ns.activeUnselectHandler = false;

    /**
     * GraphController kvuli highligtu od uzlu z kontextoveho menu.
     */
    ns.rep = rep;

    /**
     * Prekresli kompletne stage. BatchDraw se zda bych rychlejsi nez samotne draw()
     */
    ns.redrawStage = function() {
        stage.batchDraw();
    };

    /**
     * Aktivuje se zpozdenim oblouzeni odznacovaciho kliku, kdyz se to udela hned,
     * stihne se to vykonat jeste v aktualnim propagovani udalosti, coz nechceme.
     */
    ns.delayedActivateUnselectHandler = function () {
        setTimeout(function() {
            ns.activeUnselectHandler = true;
        }, 100);
    };
    
    /**
     * Aktualizuje model aktivnosti vrstev barev zaklade aktualni stavu v GUI
     */
    ns.updateColorLayersEnability = function() {
        $('#color-layers-list').find('input').each(function() {
        	Manta.PresentationManager.setLayerChecked($(this).val(), $(this).prop('checked'));
        });
    }; 

    /**
    * Odfiltruje technologii podle jmena
    */
    ns.filterOutResource = function(resName) {
        $('#resource-list input[value="' + resName + '"]').prop('checked', false);
        filterManager.applyNewFilters();
    };
     
    /**
     * Vyvola operaci filtrovani na jeden uzel
     */
    ns.filterOutNode = function(nodeId) {
        var filter = Window.operationContext.getOperation(Manta.Operations.Filter.NAME);
        filter.invokeOnNode(nodeId);
    };
    
    /**
     * Vrati aktualne aktivni filtry.
     */
    ns.getActiveFilters = function() {
        return filterManager.getActiveFilters();
    };
    
    /**
     * Zacne vykreslovani od zvoleneho uzlu
     * napr. "?selectedItems=117756&level=BOTTOM&direction=BOTH&filter=1&depth=1&filterEdges=false"
     *
     * @param newStartNodeId ID uzlu od nejz se ma zacit vykreslovat.
     */
    ns.restartVisualizationFromNode = function(newStartNodeId) {
        window.location.href = Manta.Globals.contextPath + '/viewer/dataflow/?object=' +
            '&selectedItems=' + newStartNodeId +
            '&level=' + Manta.Globals.formData.level +
            '&direction=' + Manta.Globals.formData.direction +
            '&filter=' + Manta.Globals.formData.filter +
            '&filterEdges=' + Manta.Globals.formData.filterEdges +
            '&depth=' + Manta.Globals.formData.depth + 
            '&revision=' + Manta.Globals.formData.revision +
            '&olderRevision=' + Manta.Globals.formData.olderRevision;
    };

    /**
     * Zobrazi predana data v bootstrap dialogu
     * @param label
     * @param msg
     */
    ns.showAtribute = function(label, msg) {
        var dialog = new BootstrapDialog({
            title: label,
            message: '<pre>' + msg + '</pre>',
            buttons: [
                {
                    label: 'Close',
                    action: function(dialog) { dialog.close(); }
                }
            ],
            animate: false,
            onshown: function(dialogRef) {
                // Spocitame rozmery dulezite pro nastaveni optimalni vysky pro panelu se zpravou
                
                // Vyska dialogu vcetne vnejsiho okraje v okamziku jeho zobrazeni
                var modalHeightWithMargin = dialogRef.getModalDialog().outerHeight(true);
                
                // Vyska panelu se zpravou v okamziku zobrazeni dialogu
                var $messagePanel = dialogRef.getModalBody().find('pre');
                var messagePanelActualHeight = $messagePanel.height();
                
                // Minimalni vyska panelu se zpravou - jde o vysku s potenticalni jednoradkovou zpravou:
                // 1. Zapamatujeme si zobrazenou zpravu
                var message = $messagePanel.html();
                // 2. Docasne ji nahradime zpravou na jeden radek
                $messagePanel.html("X");
                // 3. Zjistime vysku panelu s jednoradkovou zpravou
                var messagePanelMinHeight = $messagePanel.height();
                // 4. Vratime do panelu puvodni zpravu
                $messagePanel.html(message);
                
                // Maximalni vyska panelu se zpravou - nastavime ji tak, aby vyska dialogu vcetne jeho vnejsich okraju nepresahla vysku okna 
                var messagePanelMaxHeight = ($(window)).height() - (modalHeightWithMargin - messagePanelActualHeight);
                
                // Nastavime vysku pole se zpravou na zaklade aktualni, minimalni a maximalni vysky
                $messagePanel.height(Math.max(messagePanelMinHeight, Math.min(messagePanelMaxHeight, messagePanelActualHeight)));
            }
        });
        dialog.open();
    };
    // ===========================================================================================================
    // privatni namespace funkce a fieldy
    // ===========================================================================================================

    /**
     * Aktualizuje absolutni pozice panelu Detail uzlu, Vyhledavani uzlu a Filtry
     */
    ns._updatePanelsPositions = function() {
        var messagesSelector = $('#messages').show();
        var detailSelector = $('#detail');
        var searchSelector = $('#search');

        var indent = (messagesSelector.height() > 0) ? + 10 : 0;
        // Panely Vyhledavani a Filtry chceme pod panel zprav a pokd je panel zprav videt, tak s patricnym odsazenim
        var componentsUnderMessagesTop = messagesSelector.offset().top + messagesSelector.height() + indent;
        $('#options').css('top', componentsUnderMessagesTop);
        searchSelector.css('top', componentsUnderMessagesTop);
        // Panel Detail chceme pod panel Vyhledavani a pokud je rozbalen, tak i odsadit.
        detailSelector.css('top', this._getDetailTargetTopPosition(detailSelector.attr('data-expanded')));
    };

    /**
     * Vrati pozici horniho okraje panelu Detail uzlu, na kterou se ma posunout.
     * To zavisi na aktualni pozici panelu Vyhledavani uzlu a skutecnosti, zda chceme panel Detail
     * od panelu Vyhledavani odsadit.
     * @param indent <code>true</code>, pokud chceme panel Detail odsadit, jinak <code>falase</code>.
     * @returns Pozice horniho okraje panelu Detail uzlu, na kterou se ma posunout.
     */
    ns._getDetailTargetTopPosition = function(indent) {
        var search = $('#search');
        var searchBottom = search.offset().top + search.height();
        return searchBottom + (indent ? 6 : 0);
    };

    /**
     * Zobrazi zpravy z odpovedi v panelu
     * @param messages Pole zprav, ktere se maji zobrazit
     */
    ns._displayMessages = function(messages) {
        var handlebarsMessages = {};
        // Mapa severit zprav na CSS tridy
        var severityToCssClass = {
            INFO: 'alert-info',
            WARNING: 'alert-warning',
            ERROR: 'alert-danger'
        };
        // Vytvorime kopii zprav, pricemz severity prevedeme na CSS tridy
        $.each(messages, function(index, item){
            handlebarsMessages[index] = {};
            handlebarsMessages[index].text = item.text;
            // Defaultni CSS trida
            handlebarsMessages[index].cssClass = 'alert-success';
            if (severityToCssClass[item.severity]) {
                handlebarsMessages[index].cssClass = severityToCssClass[item.severity];
            }
        });
        $('#messages').html(messagesTemplate(handlebarsMessages));
        // Aktualizujeme pozice panelu pod zpravami
        ns._updatePanelsPositions();
        // a zaregistrujeme zaviraci udalosti zobrazenych zprav
        ns._registerMessagesCloseEvent();
    };

    /**
     * Zaregistruje zaviraci udalosti zprav v panelu.
     * Je potreba prevolat po aktualizaci panelu zprav.
     */
    ns._registerMessagesCloseEvent = function() {
        $('button.close').on('click', function() {
            $(this).closest($('.alert')).remove();
            // Po zavreni panelu zprav musime aktualizovat pozice panelu pod nim.
            ns._updatePanelsPositions();
        });
    };
    
    ns._createColorLayersCheckboxes = function() {
         $('#color-layers-list').html(
    		 colorLayerCheckboxTemplate(Manta.PresentationManager.getColorLayers())
		 );    
    };
    
    // mouse event posledniho kliku 
    ns._lastClick = null;
    
    // Maximální vzdálenost pro double click, pozor neni to eukledovska metrika, ale pouze soucet rozdilu.
    ns._MAX_DBLCLICK_DISTANCE = 20;
    
    /**
     * Spocita vzdalenost mezi dvema mouse eventy.
     * @param e1 prvni porovnavany mouse event
     * @param e2 druhy porovnavany mouse event
     * @return vzdalenost mezi dvema kliky, pozor neni to eukledovska metrika, ale pouze soucet rozdilu
     */ 
    ns._calcDistance = function(e1, e2) {
        return Math.abs(e1.pageX - e2.pageX) + Math.abs(e1.pageY - e2.pageY); 
    }
    
    // TODO pravdepodobne jiz nepotrebne vzhledem k nastavovani ve follow
    ns._setBulkSettings = function(graph) {
        for (var i = 0; i < Manta.Globals.formData.selectedItems.length; i++) {
            var nodeId = Manta.Globals.formData.selectedItems[i];
            var node = graph.nodeMap[nodeId];
            var resource = node.topParentTech;
            $("#collapse_" + resource + "_custom").prop("checked", true);
        }
    }
    
    /**
     * Detekuje vrstvu aktualne vykresleneho datoveho toku
     * a upravi podle ni uzivatelske rozhrani
     */
    ns._detectLayer = function() {
        var selectedLayer,
            selectedItem,
            firstVisibleItem = undefined,
            filterItemsByLayer;
        
        /**
         * Vyfiltruje prvky seznamu podle vrstvy.
         * @param listId ID DOM objektu seznamu
         * @param itemClass CSS trida prvku seznamu
         */
        filterItemsByLayer = function(listId, itemClass) {
            // Projdeme vsechny prvky v sezamu
            $("#" + listId + " ." + itemClass).each(function() {
                // Vrstva, ke ktere se vztahuje prvek
                var layer = $(this).data("layer");
                // Priznak, zda jsme narazili na prvek vyhovujici vrstve toku 
                var matched = false;
                if (layer && selectedLayer && layer !== selectedLayer) {
                    if (!matched) {
                        // Dosud jsme nenarazili na prvek vyhovujici vrstve toku
                        // => musime zmenit CSS tridu 'checkbox' na jinou, kvuli stylovani bootstrap
                        // (prvni 'checkbox' prvek ma nastaven padding-top, dalsi uz ne)
                        if ($(this).hasClass("checkbox")) {
                            $(this).removeClass("checkbox");
                            $(this).addClass("checkbox-hidden");
                        }
                    }
                    $(this).hide();
                } else {
                    matched = true;
                    // Pokud byla prvku pri skryvani odebrana CSS trida 'checkbox', musime ji nyni vratit  
                    if ($(this).hasClass("checkbox-hidden")) {
                        $(this).removeClass("checkbox-hidden");
                        $(this).addClass("checkbox");
                    }
                    $(this).show();
                }
            });
        };
        
        startNode = Manta.Utils.getSelectedItem(rep);
        
        // Vrstva toku se ridi startovnim uzlem
        if (startNode != null) {
            selectedLayer = startNode.layer;
        } else {
            selectedLayer = undefined;
        }

        // Vyfiltrujeme z jednotlivych seznamu prvky, ktere lezi mimo vrstvu toku
        filterItemsByLayer("resource-list", "resource-item");
        filterItemsByLayer("filter-list", "filter-item");
        filterItemsByLayer("collapse-settings-list", "resource-item");
    };
    
    // ===========================================================================================================
    // udalosti stage
    // ===========================================================================================================
    // contentclick muze byt narozdil od clicku i do prazdneho mista na stage
    // ale nikdy nenastavi e.target (takze jej nelze pouzit pro odchyt kliku)
    stage.on('contentClick', function(e) {
        ns._lastClick = e;
        if (ns.activeUnselectHandler) {
            // kliknuti mimo objekty
            // odznacime vyber
            rep.clearSelection();
            $('#detail').fadeOut();
            Manta.Tools.cleanLastHighlightFlow();
            // odzvyraznime tok
            Manta.Tools.unHighlightAll(rep, false);
            ns.redrawStage();
        }
        // V kazdem pripade zavreme naseptavac uzlu
        $('#search').find('.autocomplete').data('ui-autocomplete').close();
    });

    // click jde jen na objekty kineticu, takze to musi byt oddelene od contentClicku
    stage.on('click', function(e) {
        var semanticType,
            id,
            srcController,
            content = {},
            menu = $('#contextMenu');

        // na neco se kliklo, tak zjistime na co (zprasujeme field "name" Kinetictich objektu)
        // nelze spolehat na rep.selectedControllers, protoze tam nejsou hrany
        var name = e.target.getName();
        if (typeof name === 'undefined') return false;
        name = name.split(Manta.Graphics.NODE_ID_SEPARATOR);
        semanticType = name[0];
        id = name[1];
        srcController = (semanticType === Manta.Semantics.SemanticType.NODE) ? rep.nodeControllersMap[id] : rep.edgeControllersMap[id];
        // ulozime si EP toho, na co se kliklo
        window.contextClickController = srcController;

        if (semanticType === Manta.Semantics.SemanticType.NODE) {
            // klikli jsme na uzel
            content.links = [
                {'title': 'Highlight flow from this element', 'action': "javascript:Manta.DataflowUI.rep.highlightFlowEvent(window.contextClickController, undefined);"},
            ];
            if(srcController.model.getHasSourceCode()) {
                content.links.push({'title': 'Show context for this element', 'action': "javascript:Manta.Tools.showStmtInScript('"+srcController.model.owner.id+"', "+sessionIndex+");"});
            }
            if(Manta.PresentationManager.getGlobalSettings('showFilters', true)) {
            	content.links.push({'title': 'Filter <strong>'+srcController.model.tech+'</strong> technology', 'action': "javascript: Manta.DataflowUI.filterOutResource('"+srcController.model.tech+"');"});
            }
            if(Manta.PresentationManager.getGlobalSettings('allowRestart', true)) {
            	content.links.push({'title': 'Restart visualization for this element', 'action': "javascript:Manta.DataflowUI.restartVisualizationFromNode('"+srcController.model.owner.id+"');"});
            }
            $.each(srcController.model.owner.nodeMapping, function(layer, mappingNodes) {
                if (mappingNodes.length === 1) {
                    content.links.push({'title': 'Switch to <strong>' + layer + '</strong> layer', 'action': "javascript:Manta.DataflowUI.restartVisualizationFromNode('"+mappingNodes[0].id+"');"});
                } else {
                    var item = {'title': 'Switch to <strong>' + layer + '</strong> layer', sublinks: []};
                    $.each(mappingNodes, function(index, mappingNode) {
                        item.sublinks.push({'title': mappingNode.pathString, 'action': "javascript:Manta.DataflowUI.restartVisualizationFromNode('"+mappingNode.id+"');"});
                    });
                    content.links.push(item);
                }
            });
            content.links.push({'title': 'Contract this element', 'action': "javascript:Manta.DataflowUI.filterOutNode('"+srcController.model.id+"');"});
        } else {
            // kliknuto na hranu
            content.links = [];
            if(srcController.model.aggregated) {
                content.links.push({title: 'Unfilter', action: 'javascript:window.contextClickController.unfilter();'});
            }
        }
        
        // right click a menu neni prazdne
        if (e.evt.button === 2 && content.links.length > 0) {
            // prevence defaultniho kontextoveho menu
            // nutne udelat pred zobrazenim contextoveho menu, jinak uz se event nezrusi
            e.evt.preventDefault();
            e.evt.stopPropagation();

            // dirty hack pro Firefox, aby nejdrive zpracoval document.click(), ktery ctxMenu skryva
            setTimeout(function() {
                menu.html(contextMenuTemplate(content));
                menu.css({
                    'display':'block',
                    'left': e.evt.pageX + 2,
                    'top': e.evt.pageY + 2
                });
                menu.show();
                $('.dropdown-submenu a').on("click", function(e){
                    $(this).next('ul').toggle();
                    e.stopPropagation();
                    e.preventDefault();
                });
            }, 100);

            return false;
        }
    });
    
    // dvojklik na objekt kineticu
    stage.on('dblclick', function(e) {
        var semanticType,
        id,
        srcController;
        
        // kontrola jestli je vzdalenost dostatecne mala, tedy jde o doubleclick a nikoliv dva kliky
        if (ns._lastClick == null || ns._calcDistance(ns._lastClick.evt, e.evt) > ns._MAX_DBLCLICK_DISTANCE) {
            return;
        }

        if(e.evt.button == 0) {
            // druhy klik je levym tlacitkem
            // na neco se kliklo, tak zjistime na co (zprasujeme field "name" Kinetictich objektu)
            // nelze spolehat na rep.selectedControllers, protoze tam nejsou hrany
            var name = e.target.getName();
            if (typeof name === 'undefined') return false;
            name = name.split(Manta.Graphics.NODE_ID_SEPARATOR);
            semanticType = name[0];
            id = name[1];
            srcController = (semanticType === Manta.Semantics.SemanticType.NODE) ? rep.nodeControllersMap[id] : rep.edgeControllersMap[id];
            if (semanticType === Manta.Semantics.SemanticType.NODE && srcController.model.getHasSourceCode()) {
                Manta.Tools.showStmtInScript(srcController.model.owner.id, sessionIndex);
            }
        }
    });

    /**
     * O posun se komplet stara pouze stage, menim jenom kursor
    */
    stage.on('dragstart', function() {
    	$(stage.container()).css('cursor', 'move');
    });

    /**
     * O posun se komplet stara pouze stage, menim jenom kursor
     */
    stage.on('dragend', function() {
        $(stage.container()).css('cursor', 'auto');
    });

    // ===========================================================================================================
    // udalosti a obsah stranky
    // ===========================================================================================================
    $(window).load(function() {
        var canvasWrapper = $('#mainCanvas');
        var searchPanel = $('#search').find('.autocomplete');
        var searchPanelItemSelected = false;

        // workaround aby se wrapper canvasu roztahl na celou vysku dostupne plochy
        // canvas naopak roztahovat nebudem, ale rovnou ho pripravime dostatecne velky
        $(window).resize(function () {
            var h = $(window).innerHeight();
            var w = $(window).innerWidth();
            var offsetTop = parseInt(canvasWrapper.css("margin-top"));
            canvasWrapper.css('height', (h - offsetTop));
            canvasWrapper.css('width', w);
            stage.height(h - offsetTop);
            stage.width(w);
        }).resize();

        // Search Panel
        searchPanel.autocomplete({
            minLength: 1, // pocet znaku pred odeslanim dotazu
            source: function(request, response) {
                var data = graph.findNodesByLabel(request.term);
                response(data);
            },
            focus: function(event, ui) {
                $('.typeahead').val(ui.item.label);
                return false;
            },
            select: function(event, ui) {
                // Polozka v naseptavaci odpovidajici vybranemu uzlu
                var selectedItem;
                var autocompleteUI = $('.ui-autocomplete');
                // Zamerime na vybrany uzel
                rep.centerToNode(ui.item.id);
                rep.getControllerForNode(ui.item).focus(true);
                // Prekreslime scenu
                ns.redrawStage();
                // Vyplnime vyhledavaci pole popiskem vybraneho uzlu
                searchPanel.val(ui.item.label);
                // Nastavime priznak vyberu uzlu, aby se naseptavac nezavrel
                searchPanelItemSelected = true;
                // Odznacime vsechny uzly v naseptavaci
                autocompleteUI.find('li a').removeClass('ui-state-selected');
                // Zjistime polozku v naseptavaci odpovidajici vybranemu uzlu
                selectedItem = autocompleteUI.find('li').filter(function(idnex, item) {
                    return $(item).attr('data-id') === String(ui.item.id);
                });
                // a oznacime ji.
                $(selectedItem).find('a').addClass('ui-state-selected');
                return false;
            },

            messages: {
                noResults:'',
                results: function(){}
            }

        }).data('ui-autocomplete')._renderItem = function(ul, node) {
            var item = {
                    id: node.id,
                    text: node.label,
                    pathString: '/' + node.path.join('/'),
                    type: node.type
            };
            return $(autocompleteItemTemplate(item)).appendTo(ul);
        };

        // Hack naseptavace, aby se nezaviral pri vybrani uzlu.
        // Zapamatujeme si puvodni zaviraci funkci
        searchPanel.data("ui-autocomplete").originalClose = searchPanel.data("ui-autocomplete").close;
        // a nahradime ji novou:
        searchPanel.data("ui-autocomplete").close = function(event) {
            if (!searchPanelItemSelected) {
                // Naseptavac je zaviran jinak nez vybranim uzlu => musime jej zavrit => prevolame puvodni funkci
                this.originalClose.apply( this, arguments );
                // Zrusime pripadny focus na uzel
                if (rep.clearFocus()) {
                    // Pokud byl na uzlu focus a uz neni, prekreslime scenu
                    ns.redrawStage();
                }
            }
            // Vypneme priznak vyberu uzlu
            searchPanelItemSelected = false;
        };
        
        // Naseptavani pri vstupu do vyhledavaciho pole
        searchPanel.on("click", function(){
            searchPanel.autocomplete( "search", searchPanel.val());
        });    

        // Options Panel: ---------------------------------------------------------------------------
        var options = $('#options');
        var collapsedHeight = 40;
        options.find('#optionsPane').show();
        options.css('width', '400px');
        var expandedHeight = Math.min(options.css('height', 'auto').height(), 540);
        options.height(collapsedHeight);
        options.css('width', '100px');
        options.find('#optionsPane').hide();

        var optionsExpanded = false;
        function toggleOptionsPane() {
            var btn = $('#collapseOptionsBtn');
            if (optionsExpanded) {
                options.animate({
                    width: 100,
                    height: collapsedHeight
                }).find('#optionsPane').hide();
                $(btn).find('.glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
                optionsExpanded = false;
            } else {
                options.animate({
                    width:400,
                    height: expandedHeight
                }, 400, 'swing', function() {$(this).find('#optionsPane').show(); });
                optionsExpanded = true;
                $(btn).find('.glyphicon').removeClass('glyphicon-plus').addClass('glyphicon-minus');
            }
        }

        options.find('#collapseOptionsBtn').click(toggleOptionsPane);
        $('#resource-apply').click(function() {
            filterManager.applyNewFilters();
            toggleOptionsPane();
        });
        $('#filter-apply').click(function() {
            filterManager.applyNewFilters();
            toggleOptionsPane();
        });
        $('#clean-manual').click(function() {
            filterManager.cleanManualVisibility();
            toggleOptionsPane();
        });
        // Inicializujeme model aktivnosti zdroju ve filtru
        filterManager.loadFromForm();
        

        $('#collapse-settings-list').find('input').change(function() {
            // Pri zmene stavu checkboxu libovolne vrstvy zpristupnime tlacitko "Apply".
            $('#collapse-settings-apply').prop('disabled', false);
        });
        $('#collapse-settings-apply').click(function() {
            // Klikntim na tlacitko "Apply" u vrstev toto tlacitko znepristupnime,
            $('#collapse-settings-apply').prop('disabled', true);
            collapseManager.apply();            
        });
        
        
        // Vykreslime uvodni stav vrstev
        ns._createColorLayersCheckboxes();
        
        $('#color-layers-list').find('input').change(function() {
            // Pri zmene stavu checkboxu libovolne vrstvy zpristupnime tlacitko "Apply".
            $('#color-layers-apply').prop('disabled', false);
        });
        $('#color-layers-apply').click(function() {
            // Klikntim na tlacitko "Apply" u vrstev toto tlacitko znepristupnime,
            $('#color-layers-apply').prop('disabled', true);
            // aktualizujeme model aktivnosti vrstev
            ns.updateColorLayersEnability();
            // ulozit stav
            undoManager.pushUndoState();
            // a prekreslime graf
            Window.operationContext.graphController.revalidateWidgets();
            Window.operationContext.graphController.refresh();
            Manta.DataflowUI.redrawStage();
            toggleOptionsPane();
        });

        // Detail Panel: ---------------------------------------------------------------------------
        var detail = $('#detail');
        var detailButton = detail.find('#collapseDetailContentBtn');
        detailButton.click(function(){
            if (detail.attr('data-expanded')) {
                detail.animate({
                    top: ns._getDetailTargetTopPosition(false),
                    height: 30
                }).find('.content').hide();
                detailButton.animate({
                    padding: "3px 10px"
                });
                $(this).find('.glyphicon').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                detail.removeAttr('data-expanded');
            } else {
                detail.animate({
                    top: ns._getDetailTargetTopPosition(true),
                    height: 340
                }, 400, 'swing', function() {$(this).find('.content').show(); });
                detailButton.animate({
                    padding: "10px"
                });
                detail.attr('data-expanded', true);
                $(this).find('.glyphicon').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            }
        });

        // obsluha udalosti vybrani uzlu
        rep.on('nodeSelected', function(e) {
            var detail = $('#detail');
            ns.activeUnselectHandler = false; // vypnem okamzite, abychom neodvybrali/neodzvyraznili vysledek teto akce
            detail.fadeIn().find('.header span').html(nodeHeaderTemplate(e.model));
            detail.fadeIn().find('.content').html(nodeDetailTemplate({
                    model: e.model,
                    attributes: e.widget.attributes,
                    pathString: '/' + e.model.path.join('/')
            }));
            ns.delayedActivateUnselectHandler(); // zapnem se spozdenim, aby dobublala udalost
        });
        
        // obsluha udalosti hrany uzlu
        rep.on('edgeSelected', function(e) {
            var detail = $('#detail');
            ns.activeUnselectHandler = false; // vypnem okamzite, abychom neodvybrali/neodzvyraznili vysledek teto akce
            detail.fadeIn().find('.header span').html(edgeHeaderTemplate(e.model));

            var attrs = [];
            var leafChildrenNumber = 0;
            var filtredNodes = [];
            findFiltredNodes(e.model, filtredNodes)
            var filtredNumber = filtredNodes.length;
            
            if (filtredNumber == 0) {
                leafChildrenNumber = countLeafEdgeChildren(e.model);            
                if (e.model.children.length <= 1 && e.widget.attributes !== undefined ) {
                    attrs = e.widget.attributes;
                } else {
                    attrs = [];
                }
            } 
            
            detail.fadeIn().find('.content').html(edgeDetailTemplate({
                'model': e.model,
                'childrenNumber': leafChildrenNumber,
                'filtredNumber': filtredNumber,
                'attrs': attrs    
            }));
            ns.delayedActivateUnselectHandler(); // zapnem se spozdenim, aby dobublala udalost
        });

        // obsluha udalosti zvyrazneni toku
        rep.on('highlightFlow', function(startNodeController, startEdgeController) {
            ns.activeUnselectHandler = false; // vypnem okamzite, abychom neodvybrali/neodzvyraznili vysledek teto akce
            Manta.Tools.highlightFlow(startNodeController, startEdgeController);
            ns.delayedActivateUnselectHandler(); // zapnem se spozdenim, aby dobublala udalost
        });
        
        function countLeafEdgeChildren(edge) {
            if (edge.children.length == 0) {
                return 1;
            } 
            
            var count = 0;
            for (var i = 0; i < edge.children.length; i++) {
                count += countLeafEdgeChildren(edge.children[i]);
            }
            
            return count;
        }
        
        function findFiltredNodes(edge, filtredNodes) {
            if (edge.children.length == 0) {
                if (edge.aggregated) {
                    for (var i = 0; i < edge.nodesOnPath.length; i++) {
                        filtredNodes.pushOnce(edge.nodesOnPath[i].id);
                    }
                }                
                return;
            } 
            
            var count = 0;
            for (var i = 0; i < edge.children.length; i++) {
                findFiltredNodes(edge.children[i], filtredNodes);
            }
        }

        // CONTEXT MENU -  ------------------------------------------------------------------------
        canvasWrapper.on('contextmenu', function() {
            return false;
        });

        // kdyz kliknem kamkoli mimo, skryjeme ctxMenu
        // dela problem v FF, ktery ma jine poradi handlovani udalosti, menu nutno zobrazovat s prodlevou
        $(document).click(function () {
            $('#contextMenu').hide();
        });

        // SLIDER -  ------------------------------------------------------------------------
        var levels = { "0": "Bottom", "1": "Middle", "2": "Top" };

        $(function () {
            $(".slider").slider({
                value: 0, min: 0, max: 2, step: 1
            }).each(function () {
                    // Get the options for this slider
                    var t = $(this).data();
                    var opt = t["ui-slider"].options;

                    // Get the number of possible values
                    var vals = opt.max - opt.min;

                    // Space out values
                    for (var i = 0; i <= vals; i++) {
                        var el = $('<label class="text-muted">' + levels[i] + '</label>').css('left', (i / vals * 97) + '%');
                        $(".slider").append(el);
                    }

                });
        });

        // ZOOM -------------------------------------------------------------------------------------------

        function zoom(x, y, delta) {
            var newscale = {};
            var mx = x - stage.x(),
                my = y - stage.y(),
                scale = stage.getScale();

            if(delta < 0) {
                newscale.x = scale.x / zoomFactor;
                newscale.y = scale.y / zoomFactor;
            } else {
                newscale.x = scale.x * zoomFactor;
                newscale.y = scale.y * zoomFactor;
            }

            // pohlidame limity
            newscale.x = Math.max(minimumScale, Math.min(maximumScale, newscale.x));
            newscale.y = Math.max(minimumScale, Math.min(maximumScale, newscale.y));

            // zneaktivnim tlacitka pri max/min zoomu
            $('#zoomIn').prop('disabled', Math.min(Math.abs(newscale.x - maximumScale), Math.abs(newscale.y - maximumScale)) <= scaleBoundsTolerance);
            $('#zoomOut').prop('disabled', Math.min(Math.abs(newscale.x - minimumScale), Math.abs(newscale.y - minimumScale)) <= scaleBoundsTolerance);

            origin.x = mx / scale.x + origin.x - mx / newscale.x;
            origin.y = my / scale.y + origin.y - my / newscale.y;

            stage.setOffset({x: origin.x, y: origin.y});
            stage.setScale({x: newscale.x, y: newscale.y});
            ns.redrawStage();
        }

        function zoomToCenter(delta) {
            var x = $(stage.container()).width()/2;
            var y = $(stage.container()).height()/2;
            zoom(x, y, delta);
        }
        
        /**
         * Nastavi pocatecni zoom sceny.
         * Predpoklada se vychozi zoom 1 (100 %).
         * @param targetZoom Zoom, ktery se ma nastavit. Hodnota je pocet procent / 100. 
         */
        function setInitialZoom(targetZoom) {
            var defaultInitialZoom = 1;
            var zoomSteps = 0;
            var i;
        
            // pohlidame limity
            targetZoom = Math.min(targetZoom, maximumScale);
            targetZoom = Math.max(targetZoom, minimumScale);
            // Urcime pocet odzoomovani, ktere je treba udelat pro dosazeni
            // ciloveho zoomu
            zoomSteps = Math.round(Math.log(defaultInitialZoom / targetZoom) / Math.log(zoomFactor));
            for (i = 0; i < zoomSteps; i++) {
                // a provedeme je
                zoomToCenter(-1);
            }
        }

        $('#zoomIn').click(function() {
            zoomToCenter(1);
        }).prop('disabled', true); // na zacatku zacinam na maximalnim zoomu...

        $('#zoomOut').click(function() {
            zoomToCenter(-1);
        });

        ns._refreshUndoRedoBtn = function(){
            $('#undo').toggleClass('disabled', !undoManager.canUndo());
            $('#redo').toggleClass('disabled', !undoManager.canRedo());
        };

        $('#undo').click(function() {
            var undo = Window.operationContext.getOperation(Manta.Operations.UndoRedo.NAME);
            undo.invoke(Manta.Undo.Direction.UNDO);
        });
        $('#redo').click(function() {
            var undo = Window.operationContext.getOperation(Manta.Operations.UndoRedo.NAME);
            undo.invoke(Manta.Undo.Direction.REDO);
        });
        
        $('#center').click(function() {
            var selectedItem = Manta.Utils.getSelectedItem(rep);
            if (selectedItem != null) {
                rep.centerToNode(selectedItem.id);
                ns.redrawStage();
            }
        });

        $('#tips-and-tricks').click(function() {
            Manta.Tools.showHint(Manta.Globals.formData.hint);
        });

        $(document).keydown(function(e) {
            // undo mapping:
            var undo = Window.operationContext.getOperation(Manta.Operations.UndoRedo.NAME);
            if (e.which === 90 && e.ctrlKey) {
                if (undoManager.canUndo()) {
                    undo.invoke(Manta.Undo.Direction.UNDO);
                }
            } else if (e.which === 89 && e.ctrlKey) {
                if (undoManager.canRedo()) {
                    undo.invoke(Manta.Undo.Direction.REDO);
                }
            }
        });

        /** Zprovoznit pri focusu jenom na canvas...
        canvasWrapper.keydown(function(e) {
            var step = 40;
            // arrows map
            if(e.which === 37){
                stage.setPosition({x: stage.getPosition().x + step, y: stage.getPosition().y});
                ns.redrawStage();
            }
            if(e.which === 38){
                stage.setPosition({x: stage.getPosition().x, y: stage.getPosition().y + step});
                ns.redrawStage();
            }
            if(e.which === 39){
                stage.setPosition({x: stage.getPosition().x - step, y: stage.getPosition().y});
                ns.redrawStage();
            }
            if(e.which === 40){
                stage.setPosition({x: stage.getPosition().x, y: stage.getPosition().y - step});
                ns.redrawStage();
            }
        });*/

        undoManager.on('change', ns._refreshUndoRedoBtn);
        ns._refreshUndoRedoBtn();

        // korektni funkce zoomu koleckem mysi v FF je zavisla na jQuery pluginu (jquery.mousewheel.min.js)
        $(stage.content).on('mousewheel', function(event){
            var evt = event.originalEvent;
            event.preventDefault();
            zoom(evt.clientX, evt.clientY, event.deltaY);
        });

        // POCATECNI INICIALIZACE

        ns._updatePanelsPositions();
        ns._registerMessagesCloseEvent();
        graph.initStartNodeIds(Manta.Globals.formData.selectedItems);
        
        // VYKRESLENI POCATECNICH DAT-------------------------------------------------------------------------
        var data = canvasWrapper.data('initialdata');
        if(typeof data !== 'undefined' && data !== "") {
            var follow = Window.operationContext.getOperation(Manta.Operations.Follow.NAME);
            follow.setNodeToCenterOnId(Manta.Globals.formData.selectedItems[0]);
            follow.receiveResponse(data);
            ns._setBulkSettings(graph);
        }
        // Detekujeme vrstvu datoveho toku
        ns._detectLayer();

        // Nastavime pocatecni zoom
        setInitialZoom(Manta.Globals.formData.zoom);

        // Zobrazeni napovedy pokud je pozadovana
        if (Manta.Globals.formData.hint != null) {
            Manta.Tools.showHint(Manta.Globals.formData.hint);
        }
    });

}(Manta.DataflowUI = Manta.DataflowUI || {}, jQuery));