$(document).ready(function () {
    $(document).tooltip({ position: { my: "left+15 center", at: "right center" } });    
});

// promazat zpravy a oznacene objekty od serveru, jestlize mame historii, tedy jsme se prisli z vizualizace
if (history.state != undefined) {
	$("#messages").empty();
	$(".selected-items").empty();
}

$(function() {
    /**
     * Defaultni vrstva 
     * @type {*}
     */
    var DEFAULT_LAYER = "Physical";
    /**
	 * Sablona pro zobrazeni detailu uzlu
	 * @type {*}
	 */
	var nodeDetailTemplate = Handlebars.compile($("#node-detail-template").html());

	/**
	 * Sablona pro polozku ve vybranych uzlech
	 * @type {*}
	 */
	var itemTemplate = Handlebars.compile($("#item-template").html());

    /**
     * Sablona pro polozku autocomplete
	 * @type {*}
     */ 
    var autocompleteItemTemplate = Handlebars.compile($("#autocomplete-item-template").html());
    
    /**
     * Sablona pro zpravy.
	 * @type {*}
     */ 
    var messagesTemplate = Handlebars.compile($("#messages-template").html());
    /**
     * Sablona pro vrstvy.
     * @type {*}
     */
    var layersTemplate = Handlebars.compile($("#layers-template").html());
    /**
     * Sablona obsahu dialogu pro primy vyber elementu.
     * @type {*}
     */
    var directAddTemplate = Handlebars.compile($("#direct-add-template").html());
    /**
     * Strom repozitare.
     */
    var tree = $('#tree');

    /**
     * Jiz vybrane uzly
     */
    var selectedItems = $.map($('#selected-nodes').find('input[name="selectedItems"]').remove(), function(item) {
        return $(item).val();
    });
    
    var focusedItem = null;
    /**
     * Vrstvy
     */
    var layers = [];
    /**
     * Vrstava vybrana v zalozce
     */
    var selectedLayer = undefined;
    /**
     * Vybrany oddelovac polozek v ceste k elementu pri primem vyberu
     */
    var selectedPathSeparator = undefined;
    
    var refresher = new Manta.Refresher.General();

    // nactem si grafickou konfiguraci
    Manta.PresentationManager.loadConfig();
    
    /**
     * Zkontroluje aktivnost visualize tlacitka a pripadne ho skryje/zobrazi. 
     */
    function checkVisualizeBtn() {
        if($('.selected-item:visible').length > 0 || focusedItem != null) {
            $('#visualize').prop('disabled', false);
            $('#visualizeDropDown').prop('disabled', false);
        } else {
            $('#visualize').prop('disabled', true);
            $('#visualizeDropDown').prop('disabled', true);
        }
    }
    
    /**
     * Zkontroluje a aktualizuje ovladaci prvky v panelu vybranych elementu. 
     */
    function checkSelectedElementsPanel() {
        if($('.selected-item:visible').length > 0) {
            $('#remove-all').show();
            $('.drop-text').hide();
        } else {
            $('#remove-all').hide();
            $('.drop-text').show();
        }
    }
    
	function showDetail(item) {
	    var attributes = {};
	    var attributeLabels;
	    
        // Nastavime popisky a hodnoty atributu.
        // Popisky jsou ulozeny v JSON konfiguraci, hodnoty primo ve vlastnostech uzlu.
        // Pokud atribut nema popisek definovan, bude popiskem jeho nazev.
        var technology = item.original.technologyType;
	    attributeLabels = Manta.PresentationManager.getAttributesByTechnologyAndType(technology, item.original.type);
	    $.each(item.original.attributes, function(name, value){
	        attributes[name] = {label: attributeLabels[name] ? attributeLabels[name] : name, value: value};
	    });
        
        if (!isRoot(item)) {
            focusedItem = item;
        } else {
            focusedItem = null;
        }
	    
	    item.original.pathString = '/' + item.original.path.join('/');
	    
	    $.each(item.original.nodeMapping, function(layer, mapping) {
	        $.each(mapping, function(index, mappedNode) {
	            mappedNode.pathString = '/' + mappedNode.path.join('/');
	        });
        });
        
	    $('#node-detail-placeholder')
	        .empty()
	        .append(nodeDetailTemplate({node: item, attributes: attributes}))
	        .slideDown(400, function() {
	        	// Musime aktualizovat pozici seznamu vybranych uzlu.
	        	updateSelectedNodesPosition(true);
	        	checkVisualizeBtn();	
	        });	   
	}

	function hideDetail() {
        focusedItem = null;
	    $('#node-detail-placeholder').slideUp('normal', function() {
	        $(this).empty();
	    	// Musime aktualizovat pozici seznamu vybranych uzlu.
	        updateSelectedNodesPosition(true);
	        checkVisualizeBtn();
	    });
	}

	/**
	 * Prida polozku do seznamu vybranych polozek
	 */
	function addItem(item) {
	    item.original.pathString = item.original.path.join('/');
	    if($('.selected-items input[value="' + item.id + '"]').length != 0) {
	        return;
	    }    
	    $(itemTemplate(item)).appendTo('.selected-items');    
	    checkSelectedElementsPanel();
        checkVisualizeBtn();
	}
	
	/**
	 * Zjisti, zda je dany uzel koren.
	 * @param node Dotazovany uzel 
	 * @returns {boolean} <code>true</code>, pokud je dany uzel koren, jinak <code>false</code>
	 */
	function isRoot(node) {
        return (node.original.path.length === 0);
	}

	/**
	 * Aktualizuje pozici seznamu vybranych uzlu.
	 * @param smooth <code>true</code>, pokud se ma seznam na novou pozici presunout plynule,
	 *               nebo <code>false</code>, pokud okamzite.
	 */
	function updateSelectedNodesPosition(smooth) {
		// primarne zkusime seznam nasmerovat pod pruh hlavicky okna
		$('#selected-nodes').position({
	    	my: "top",
	    	at: "top+85",
	    	of: $(window),
	    	using: function(destination) {
	    		// objekt predstavujici delici seznam od predchoziho oddilu
	    		var offset = $('#selected-nodes-offset');
	    		// aktualni rozdil vysky leveho sloupce a praveho (ve kterem se nachazi seznam)
	    		var columnsHeightDifference = $('#content-left').height() - $('#content-right').height();
	    		// Vertikalni posun seznamu vuci aktualni pozici.
	    		// Nesmi presahnout rozdil sloupcu, aby neskoncil "prilis nizko".
	    		var shift = Math.min(destination.top, columnsHeightDifference);
	    		// Novou velikost odsazeni spocitame prictenim aktulni k vypocitanemu posunu.
	    		// Zajistime, aby byla nezaporna.
	    		var newOffsetHeight = Math.max(shift + offset.height(), 0);
	    		// Nastavime odsazeni seznamu na vypocitaanou velikost:
	    		if (smooth) {
	    			// bud plynule, pokud je pozadovana plynulost,
	    			offset.animate({
	    				"height" : newOffsetHeight		
	    			}, 200);
	    		}
	    		else {
	    			// jinak okamzite.
	    			offset.height(newOffsetHeight);
	    		}
	    	}
	    });
	}

	/**
	 * Vrati absolutni cestu k cilovemu uzlu. Vysledkem je zretezeni nazvu uzlu,
	 * pres ktere vede cesta od korene k cilovemu uzlu, oddelenych lomitkem.
	 * Nazev ciloveho uzlu je soucasti cesty.
	 * 
	 * @param node Cilovy uzel
	 * @returns {Object} Absolutni cesta k cilovemu uzlu.
	 */
    function getNodePath(node) {
        if (node.id === '#') {
            // "virtualni" uzel - predek vsech korenu stromu, nezobrazuje se
            return {path: [], revision: Manta.Globals.revision}; 
        } else if (node.original['idsPath'] === 0) {
            return {path: [node.original.id], revision: Manta.Globals.revision};
        } else {
            return {path: node.original['idsPath'].concat([node.original.id]), revision: Manta.Globals.revision};
        }
    }
        
    /**
     * Nacte nastaveni ze session cookies (pokud je v nich ulozeno).
     */
    function loadPageContext() {
        var cookie = $.cookie('manta_welcome');
        if (cookie) {
            $('input[name=level][value=' + cookie.level + ']').prop('checked', true);
            $('input[name=direction][value=' + cookie.direction + ']').prop('checked', true);
            $('#filterEdgesCheckBox').prop('checked', cookie.filteredges);
            $('#filter').val(cookie.filter);
            $('#depth').val(cookie.depth);
            selectedPathSeparator = cookie.pathseparator;
        }
    }

    /**
     * Ulozi nastaveni do session cookies.
     */
    function savePageContext() {
        $.cookie('manta_welcome',
            {
                level: $('input[name=level]:checked').val(),
                direction: $('input[name=direction]:checked').val(),
                filteredges: $('#filterEdgesCheckBox').prop('checked'),
                filter: $('#filter').val(),
                depth: $('#depth').val(),
                pathseparator: selectedPathSeparator
            },
            {
                // MUSIME nastavit cestu, protoze impplicitne se nastavi aktulni cesta
                // - stavalo se, ze se ukladaly dve cookies manta_welcome, kazda s jinou cestou,
                // a pri cteni se nedeterministicky vybrala jedna z nich, casto ta spatna 
                path: Manta.Globals.contextPath
            }
        );
    }
    
    function displayMessages(messages) {
        var handlebarsMessages = {};
        // Mapa severit zprav na CSS tridy
        var severityToCssClass = {
            INFO: 'alert-info',
            WARNING: 'alert-warning',
            ERROR: 'alert-danger'
        };
        // Vytvorime kopii zprav, pricemz severity prevedeme na CSS tridy
        $.each(messages, function(index, item){
            handlebarsMessages[index] = {};
            handlebarsMessages[index].text = item.text;
            // Defaultni CSS trida
            handlebarsMessages[index].cssClass = 'alert-success';
            if (severityToCssClass[item.severity]) {
                handlebarsMessages[index].cssClass = severityToCssClass[item.severity];
            }
        });
        $('#messages').html(messagesTemplate(handlebarsMessages));
        
        $('button.close').on('click', function() {
            $(this).closest($('.alert')).remove();
        });
    }

    /**
     * Obohati uzly ve stromu o vlastnosti otebne k vykresleni
     * @param nodes
     */
    function processNodes(nodes) {
        // pro kazdy uzel nastavime ikonu podle JSON konfigurace
        $.each(nodes, function(index, node) {
            var type;
            if (node.technologyType == node.type) {
                type = 'Resource';
            } else {
                type = node.type;
            }
            
            node.icon = Manta.PresentationManager.getIconByTechnologyAndType(node['technologyType'], type).src;
            // Podstrceni nazvu uzlu s escapovanymi HTML znaky
            node.unescapedText = node.text;
            node.text = Handlebars.Utils.escapeExpression(node.text);
            // Informace o vrstve
            node.li_attr = node.li_attr|| {};
            node.li_attr["data-layer"] = node.layer;
        });
    }
    
    /**
     * Aktualizuje seznam vrstev na zaklade uzlu, ktere v nich lezi
     * @param nodes Zkoumane uzly
     */
    function updateLayers(nodes) {
        layers = [];
        // Projedeme vsechny uzly a zjistime jejich vrstvy
        $.each(nodes, function(index, node) {
            if (node.layer && layers.indexOf(node.layer) < 0) {
                layers.push(node.layer);
            }        
        });
        // Setridime vrstvy - prednost ma defaultni vrstva
        layers.sort(function(a, b) {
            if (a === DEFAULT_LAYER) {
                return -1;
            }
            else if (b === DEFAULT_LAYER) {
                return 1;
            }
            else {
                return a.localeCompare(b);
            }
        });
        if (layers.length >= 2) {
            // Existuji alespon dve vrstvy => zobrazime zalozky pro prepinani mezi nimi
            var layerToSelect;
            if (selectedLayer && layers.indexOf(selectedLayer) >= 0) {
                // Pokud existuje, zachovame jiz vybranou vrstvu
                layerToSelect = selectedLayer;
            } else if (history.state && history.state.selectedLayer && layers.indexOf(history.state.selectedLayer) >= 0) {
                // Pokud existuje, vybereme vrstvu ulozenou v historii
                layerToSelect = history.state.selectedLayer;
            } else {
                // V opacnem pripade vybereme prvni vrstvu
                layerToSelect = layers[0];
            }

            $("#layers").html(layersTemplate({layers: layers, selectedLayer: layerToSelect}));
            selectLayer(layerToSelect);
            $("#layers a").each(function() {
                $(this).click(function() {
                    selectLayer($(this).data("layer"));
                });
            });
        } else {
            // Existuje jedna nebo zadna vrstva => neni mezi cim prepinat => zalozky nemaji smysl
            $("#layers").empty();
            selectLayer(undefined);
        }
    }
    
    /**
     * Vybere danou vrstvu. Zobrazi ve strome repozitare uzly, ktere v dane vrstve lezi.
     * Ostatni uzly skryje.
     * @param layer Vrstva, ktera se ma vybrat
     */
    function selectLayer(layer) {
        // Priznak, zda je filtr vybrany v combo boxu schovan
        var selectedFilterOptionHidden;
        // Prvni viditelny filtr v combo boxu
        var firstVisibleFilterOption;

        selectedLayer = layer;
        if (!layer) {
            // Vrstva neni specifikovana => neni co resit
            return;
        }
        // Schovame uzly mimo vrstvu
        $("#tree li").filter(function() {
            return $(this).data("layer") !== layer;
        }).hide();
        // a pak zobrazime uzly ve vrstve
        $("#tree li").filter(function() {
            return $(this).data("layer") === layer;
        }).show();
        // Aktualizuje viditelnost vybranych prvku
        updateSelectedItemsVisibility();
        
        // Schovame v combo boxu filtru polozky, ktere neprislusi k vybrane vrstve
        selectedFilterOptionHidden = false;
        $("#filter option").each(function() {
            var filterLayers = $(this).data("layers");
            if (filterLayers.length === 0 || filterLayers.indexOf(layer) >= 0) {
                // Filtr prislusi ke vsem vrstvam nebo k vybrane vrstve
                if (!firstVisibleFilterOption) {
                    // Zapamatujeme si prvni viditelnou polozku
                    firstVisibleFilterOption = $(this); 
                }
                $(this).show();
            } else {
                // Filtr prislusi k vrstve, ktera neni vybrana
                if ($(this).is(":selected")) {
                    // a navic je vybran v combo boxu
                    selectedFilterOptionHidden = true;
                }
                $(this).hide();
            }
        });
        if (selectedFilterOptionHidden && firstVisibleFilterOption) {
            // V combo boxu je vybran filtr, ktery nove neni videt
            // => vybereme prvni viditelny  
            $("#filter").val(firstVisibleFilterOption.val());
        }
    }
    
    function serializeSelectedItems() { 
        var serialized = '';
        $('#selected-nodes').find('input[name="selectedItems"]').each(function() {
            serialized += $(this).val() + "_";
        });
        return serialized;
    }
    
    function appanedSerializedIds(serializedString, idList) {
        var elements = serializedString.split("_");
        for (var i in elements) {
            if (elements[i] != undefined && elements[i] != "") {
                var id = parseInt(elements[i], 10);
                if (id != 'NaN') {
                    idList.push(id);
                }
            }
        }
    }
    
    function loadDetailsforIds(idList) {
        if (idList.length > 0) {            
            var i, paramString = '', item;
            // schovam "Drop here" text, jelikoz uz jsou vybrane polozky
            checkSelectedElementsPanel();
    
            for(i=0; i < idList.length; i++) {
                if(paramString !== '') {
                    paramString += '&'
                }
                paramString += 'ids=' + idList[i];
            }
    
            // nactem si grafickou konfiguraci
            $.ajax({
                dataType: "json",
                url: Manta.Globals.contextPath + "/viewer/nodes/?" + paramString + "&revision=" + Manta.Globals.revision,
                success: function(data) {
                    processNodes(data);
                    for(i=0; i < data.length; i++) {
                        // musim simulovat chovani uzlu ve strome...
                        item = {
                            original: data[i],
                            icon: data[i].icon,
                            id: data[i].id
                        };
                        addItem(item);
                    }
                },
                error: function(data) {
                    displayMessages([{
                        severity: 'ERROR',
                        text: data.status + ': ' + data.text
                    }]);
                }
            });
        }
    }
    
    function changeRevisionNumber(revision) {
        Manta.Globals.revision = parseFloat(revision);
        $("#revisionNumberInForm").val(Manta.Globals.revision);
        $('#tree').jstree("destroy");
        $('#tree').empty();
        initJstree();
        removeAllSelectedItems();
    }
    
    function changeCompareRevisionNumber(revision) {
        Manta.Globals.compareRevision = parseFloat(revision);
        $("#compareRevisionNumberInForm").val(Manta.Globals.compareRevision);
    }
    
    $("#exportLink").bind("click", function() {    
        $('#vizForm').attr("action", Manta.Globals.contextPath + "/viewer/dataflow/direct-export");
        $('#vizForm').submit();
        // nastavit zpátky pro případ otevření v novém okně
        $('#vizForm').attr("action", Manta.Globals.contextPath + "/viewer/dataflow/");
    });

    // Chceme ukladat cookies jako JSON
    $.cookie.json = true;
    
    // Nacteme nastaveni z cookies
    loadPageContext();

	// Nastaveni vysky stromu uzlu pri zmene velikosti okna.
    //$(window).resize(function () {
        var windowHeight = $(window).innerHeight();
        // Minimalni vyska, pod kterou nepujdeme - aby bylo ve strome neco rozumne vydet.
        var minHeight = 260;
        // Spocitame vysku stromu na zaklade vysky okna a casti stranky nad a pod stromem
        var documentHeight = $('html').height();
        var treeHeight = $('#content').height() - $('#tree-title').outerHeight(true);
        treeHeight = Math.max(windowHeight - (documentHeight - treeHeight), minHeight);
        // Nastavime stromu spocitanou vysku
        $("#tree-view").height(treeHeight);
    //}).resize();
    // Vyvolame udalost zmeny velikosti okna, aby se vyska stromu urcila uz na zacatku
    //$(window).resize();

    // Napojime aktualizaci pozice seznamu vybranych uzlu pri zmene velikosti ci scrollovani okna
    $(window).bind("resize scroll", function() {
    	updateSelectedNodesPosition(false);
    });

    var autocomleteDiv = $('.autocomplete'); 
    autocomleteDiv.autocomplete({   
        minLength: 2, // pocet znaku pred odeslanim dotazu
        source: function(request, response) {
            var query = request.term;
            $.post(Manta.Globals.contextPath + '/viewer/typeahead', {'pattern': query, 'revision': Manta.Globals.revision}, function(data) {
                response(data);
            }, "json").fail(function(data) {
            	if (data.responseText.indexOf("<!DOCTYPE") != -1) {
					location.reload();
            	}
            });
        },
        focus: function(event, ui) {            
            $('.typeahead').val(ui.item.text);
            return false;
        },
        select: function(event, ui) {
            var actualIndex = 0;
            var actualNodeId;
            var selectedNode;
            var openNode = function () {
                if (layers.length >= 2 && ui.item.layer && selectedLayer !== ui.item.layer) {
                    // Mame vice vrstev a vrstva vybraneho uzlu neni aktivni => aktivujeme ji "kliknutim" na prislusnou zalozku
                    $('#layers li a[data-layer="' + ui.item.layer + '"]').click();
                }
                if(actualIndex < ui.item['idsPath'].length) {
                    // jeste nejsme u vybraneho uzlu, rekurzivne otevru dalsi patro
                	actualNodeId= ui.item['idsPath'][actualIndex];
                	actualIndex++;
                    tree.jstree('open_node', actualNodeId, openNode);
                } else {
                    // jsme u vybraneho uzlu, oznacim ho
                    tree.jstree('select_node', ui.item.id, false, false);  
                    selectedNode = $('#tree #'+ ui.item.id);
                    // zaskrolujeme na vybrany uzel - musime se zpozdenim, aby se strom stihl nacist (hodnota 200 ms je empiricka) 
                    setTimeout(function() {
                        tree.scrollTop (
                            selectedNode.offset().top - tree.offset().top + tree.scrollTop()
                        );                    
                    }, 200);
                }
            };
            tree.jstree('deselect_all');
            tree.jstree('close_all');
            $('.autocomplete').val(ui.item.text);
            
            openNode();
            return false;
        },
        messages: {
            noResults:'',
            results: function(){}
        }
        
    }).data('ui-autocomplete')._renderItem = function(ul, item) {
        item.pathString = item.path.join('/');
        return $(autocompleteItemTemplate(item)).appendTo(ul);
    };
    
    autocomleteDiv.on("click", function(){
        autocomleteDiv.autocomplete( "search", autocomleteDiv.val());
    });    
    
    // slider pro vyber hloubky hledani
    $('#slider').slider({
        value: $('#depth').val(),
        min: 0,
        max: Manta.Globals.maximalDepth,
        step: 1,
        slide: function(event, ui) {
            $('#depth').val(ui.value);
        }
    });
    
    // vytvoreni stromu objektu
    var initJstree = function() {
        tree.jstree({
            core: {
                data: function(node, childrenRecievedCallback) {
                    // handler obdrzeni detskych uzlu serveru
                    var onChildrenRecieved = function(childNodes) {
                        if (node.id === "#") {
                            // Jsme v koreni stromu => deti jsou zdroje
                            // => aktualizujeme podle nich vrstvy
                            updateLayers(childNodes);
                        }
                        processNodes(childNodes);
                        // upravene uzly predame callbacku nacteni detskych uzlu
                        childrenRecievedCallback.call(this, childNodes);
                    };
                    
                    var onError = function (data) {
                    	if (data.responseText.indexOf("<!DOCTYPE") != -1) {
        					location.reload();
                    	} else {
                    		childrenRecievedCallback.call(this, []);
                    		displayMessages(data['responseJSON'].messages);
                    	}
                    };
                    
                    // Nacteme detske uzly ze serveru
                    // FIXME verze
                    $.ajax({
                        dataType : "json",
                        url : Manta.Globals.contextPath + "/viewer/jstree",
                        data: getNodePath(node),
                        success: onChildrenRecieved,
                        error: onError
                    });
                }
            },       
            dnd: {
                is_draggable: function(data) {
                    var selectedNodeIds = $('#tree').jstree('get_selected');
                    var result = true;
                    if ($.inArray(data[0].original.id, selectedNodeIds) < 0) {
                        // Pretahovany uzel neni soucasti vyberu => pretahujeme pouze tento jeden uzel.
                        // Ten lze pretahnout, prave kdyz neni korenovy.
                        return !isRoot(data[0]);
                    }
                    $.each(selectedNodeIds, function(index, nodeId) {
                        // Pokud je pretahovany uzel soucasti vyberu, pretahujeme spolu s nim i ostatni vybrane uzly.
                        // Vybrane uzly lze pretahnout, prave kdyz zadny z nich neni korenovy.
                        if (typeof nodeId !== "undefined") {
                            var node = $('#tree').jstree('get_node', nodeId);
                            if (isRoot(node)) {
                                result = false;
                                // break
                                return false;
                            }
                        }
                    });                
                    return result;
                }
            },
            plugins: ["wholerow", "dnd"]
        });
        
        // pridani polozky do vybranych na double click
        tree.bind("dblclick.jstree", function (event) {
            var id = $(event.target).closest('li').attr('id');
            var item = $('#tree').jstree('get_node', id);
            if (!isRoot(item)) {
                addItem(item);         
            }
            checkSelectedElementsPanel();
        });

        // Zobrazeni detailu uzlu
        tree.bind("select_node.jstree", function (event, data) {
            showDetail(data.node);
        });
        
        // Kdyz je strom prirpaven, obcerstvime vybranou vrstvu
        tree.on('ready.jstree', function (e, data) {
            selectLayer(selectedLayer);
        });
        
        // Zajisteni drag & drop ze stromu do seznamu vybranych polozek
        $(document)
            .on('dnd_start.vakata', function () {
                $('.drop').addClass('drop-active');
                checkSelectedElementsPanel();
            })
            .on('dnd_move.vakata', function (e, data) {
                var t = $(data.event.target);
                var dropAllowed = false;
                if (!t.closest('#tree').length) {
                    if (t.closest('.drop').length) {
                        $.each(data.data.nodes, function(index, node) {
                            // Pokud alespon jeden pretahovany uzel neni korenem, povolime pretazeni,
                            // nicmene korenove uzly budou ignorovany
                            if (typeof node !== "undefined") {
                                var item = $('#tree').jstree('get_node', node);
                                if (!isRoot(item)) {
                                    dropAllowed = true;
                                }
                            }
                        });                    
                    }
                    if (dropAllowed) {
                        data.helper.find('.jstree-icon').removeClass('jstree-er').addClass('jstree-ok');
                    }
                    else {
                        data.helper.find('.jstree-icon').removeClass('jstree-ok').addClass('jstree-er');
                    }
                }
            })
            .on('dnd_stop.vakata', function (e, data) {
                var t = $(data.event.target);

                $('.drop').removeClass('drop-active');            
                
                if (!t.closest('#tree').length) {
                    if (t.closest('.drop').length) {
                        $.each(data.data.nodes, function(index, node) {
                            if (typeof node !== "undefined") {
                                var item = $('#tree').jstree('get_node', node);
                                if (!isRoot(item)) {
                                    // Pretahneme pouze uzly, ktere nejsou koreny
                                    addItem(item);
                                    checkSelectedElementsPanel();
                                }
                            }
                        });                    
                    }
                }
            });
    };
    initJstree();

    // odebrani polozky ze seznamu vybranych polozek
    $('.selected-items').on('click', '.remove-item', function() {
        $(this).closest('.selected-item').remove();
        checkSelectedElementsPanel();
        checkVisualizeBtn();
    });
    
    var removeAllSelectedItems = function() {
        $('.selected-item').remove();
        checkSelectedElementsPanel();
        checkVisualizeBtn();
    };

    /**
     * Odstrani viditelne vybrane polozky (cili ty, ktere nalezi do aktvini vrstvy).
     */
    var removeVisibleSelectedItems = function() {
        $('.selected-item:visible').remove();
        checkSelectedElementsPanel();
        checkVisualizeBtn();
    };

    /**
     * Aktualizuje viditelnost vybranych prvku.
     * Viditelne budou pouze prvky v aktivni vrstve, ostatni budou skryte. 
     */
    var updateSelectedItemsVisibility = function() {
        $('.selected-item').each(function() {
            if ($(this).data("layer") === selectedLayer) {
                $(this).show();
                // Zaktivnime viditelne pole. 
                $(this).find("input").prop("disabled", false);
            } else {
                $(this).hide();
                // Zneaktivnime  pole, aby se neviditelne prvky neodesilaly pri submitu. 
                $(this).find("input").prop("disabled", true);
            }
        });

        // Aktualizujeme uzivatelske rozhrani 
        checkSelectedElementsPanel();
        checkVisualizeBtn();
    };

    // odebrani viditelnych polozek ze seznamu vybranych polozek
    $('#remove-all').find('button').on('click', removeVisibleSelectedItems);
    
    // Schovani detailu uzlu
    $(document).click(function(e) {
        if($(e.target).closest('#tree').length === 0 &&
            $(e.target).closest('#node-detail-placeholder').length === 0 &&
            $(e.target).closest('.ui-autocomplete').length === 0 &&
            !$(e.target).is('button')){
            hideDetail();
        }
    });
    
    var addElementsFromDirectInput = function(dialogItself) {
    	var text = $("#direct-add-textarea").val();
        selectedPathSeparator = $("#path-separator").val();
    	$.post(
            Manta.Globals.contextPath + "/viewer/find-elements",
            {"elements": text, "pathSeparator": selectedPathSeparator, "revision": Manta.Globals.revision},
            function(data, textStatus, jqXHR) {
                var ignoredElements = 0;
                var lastIgnoredElement = undefined;
            	if (jqXHR.responseText.indexOf("DOCTYPE") === -1) {
            		dialogItself.close();

            		if (data.messages != undefined) {
	            		displayMessages(data.messages);
	            	}
	            	
	            	if (data.elements != undefined) {
	            		processNodes(data.elements);
	            		for (var i = 0; i < data.elements.length; i++) {
	            		    if (selectedLayer && selectedLayer !== data.elements[i].layer) {
	            		        // Pokud je vybrana vrstva a element lezi v jine, ignorujeme jej
	            		        ignoredElements++;
	            		        lastIgnoredElement = data.elements[i];
	            		        continue;
	            		    }
	            			var item = {
                                original: data.elements[i],
                                icon: data.elements[i].icon,
                                id: data.elements[i].id
	                        } 
	            			addItem(item);
	            		}
	            		// Pokud je alespon jeden element ignorovan, zobrazime varovani v zavislosti na poctu ignorovanych elementu
	                    if (ignoredElements === 1) {
	                        displayMessages([{severity: "WARNING", text: "Element '" + lastIgnoredElement.text + "' in '" + lastIgnoredElement.layer + "' layer was ignored."}]);
	                    } else if (ignoredElements > 1) {
                            displayMessages([{severity: "WARNING", text: ignoredElements + " elements oustside '" + selectedLayer + "' layer were ignored."}]);
                        }
	            	}
            	} else {
            		location.reload();
            	}            	
            }
        );
    }
    
    var showDirectAddDialog = function() {
    	var dialog = new BootstrapDialog({
    		size: BootstrapDialog.SIZE_LARGE,
            title: 'Add elements',
            cssClass: 'direct-add-dialog',
            message: directAddTemplate(),
            nl2br: false,
            buttons: [{
                	label: 'Add',
                	action: addElementsFromDirectInput
                },{
	                label: 'Close',
	                action: function(dialogItself){
	                    dialogItself.close();
	                }
                }
            ],
            animate: false,
			closable : false,
            onshown: function(dialogRef) {
                // Spocitame rozmery dulezite pro nastaveni optimalni vysky textoveho pole
                // Vyska dialogu vcetne vnejsiho okraje v okamziku jeho zobrazeni
                var modalHeightWithMargin = dialogRef.getModalDialog().outerHeight(true);
                var $textArea =  dialogRef.getModalBody().find('textarea');
                // Minimalni vyska textoveho pole - nastavime na vysku v okamziku zobrazeni dialogu 
                var textAreaMinHeight = $textArea.height();
                // Cilova vyska textoveho pole - nastavime ji tak, aby vyska dialogu vcetne jeho vnejsich okraju odpovidala vysce okna 
                var textAreaTargetHeight = ($(window)).height() - (modalHeightWithMargin - textAreaMinHeight);
                
                // Nastavime vysku textoveho pole na cilovou, s respektem minimalni vysky
                $textArea.height(Math.max(textAreaMinHeight, textAreaTargetHeight));
                
                // Oznacime textareu, aby to pri jeho kopirovani nemusel delat uzivatel
                $textArea[0].select();
                if (selectedPathSeparator) {
                    $("#path-separator").val(selectedPathSeparator);
                }
            }
        });
        dialog.realize();
        dialog.open();
    };
    
    /**
     * Nastavi filtrum v combo boxu informace o vrstvach
     */
    var initFilterLayers = function() {
        Manta.Globals.filterLayers.forEach(function(layers, filterGroupId) {
            $("#filter option[value = '" + filterGroupId + "']").data("layers", layers);
        });
    };
    
    // pridani polozek pomoci primeho vstupu z dialogu
    $('#direct-add').find('button').on('click', showDirectAddDialog);
    
    $('.enable-help').popover({trigger: 'hover'}).click(function(){return false;});
    
    $('form').submit(function() {
        if (Manta.Globals.revision < Manta.Globals.compareRevision) {
            displayMessages([{'text': 'Compare revision must be older than main revision.','severity': 'ERROR'}]);
            return false;
        }
        // Pri odeslani stranky ulozime nastaveni do cookies
        savePageContext();
    });

    /**
     * Pri klinuti na detail atributu otevru nove okno se zobrazenim atributu
     */
    $('#node-detail-placeholder').on('click', '.showAttribute', function() {
        var label = $(this).data('label');
        var msg = $(this).data('msg');
        var dialog = new BootstrapDialog({
            title: label,
            message: '<pre>' + msg + '</pre>',
            buttons: [
                {
                    label: 'Close',
                    action: function(dialog) { dialog.close(); }
                }
            ],
            animate: false
        });
        dialog.open();
    });

    $('#navbar-revision').attr("title", "");
    $('#navbar-revision').addClass("navbar-revision-welcome-page");
    $('#navbar-revision').html(
            '<select id="navbar-compare-revision-picker" class="selectpicker" data-live-search="true" data-size="15" data-width="320px" />'
            + '<div id="compare-btn" class="icon-compare" title="Compare Revisions"></div>'
            + '<select id="navbar-revision-picker" class="selectpicker" data-live-search="true" data-size="15" data-width="320px" />');
    var picker = $('#navbar-revision-picker');
    var comparePicker = $('#navbar-compare-revision-picker');    
    for (var i = Manta.Globals.revisionList.length - 1; i >= 0; i-- ) {
        var model = Manta.Globals.revisionList[i];
        var optionBody = '<option value="' + model.revision + '" title="Rev: ' + model.commitTime + '"> Revision: #' + model.revision+ 
                ', commited at ' + model.commitTime + '</option>';
        picker.append(optionBody);
        comparePicker.append(optionBody);
    }
    comparePicker.selectpicker('hide');
    
    picker.on( "change", function() {
        changeRevisionNumber($(this).val());
    });
    
    comparePicker.on( "change", function() {
        changeCompareRevisionNumber($(this).val());
    });
    
    $('#compare-btn').on( "click", function() {
        if ($('#compare-btn').hasClass("icon-compare")) {
            comparePicker.selectpicker('show');
            changeCompareRevisionNumber(comparePicker.selectpicker('val'));
            $('#compare-btn').addClass("icon-compare-active");
            $('#compare-btn').removeClass("icon-compare");
        } else {
            comparePicker.selectpicker('hide');
            changeCompareRevisionNumber(0);
            $('#compare-btn').addClass("icon-compare");
            $('#compare-btn').removeClass("icon-compare-active");
        }
    });
    
    $("#visualize").on("click", function(){
        if (focusedItem != null && $('.selected-item:visible').length === 0) {
            addItem(focusedItem);
        }
        
        var formData = {};
        formData.selectedItems = serializeSelectedItems();
        formData.level = $("input[name=level]:checked").val();
        formData.direction = $("input[name=direction]:checked").val();
        formData.filterEdges = $("input[name=filterEdges]").prop("checked");
        formData.filter = $("#filter").val();
        formData.depth = $("input[name=depth]").val();
        formData.revision = $("#revisionNumberInForm").val();
        formData.selectedLayer = selectedLayer;
        
        history.pushState(formData, '', location.href);
    });

    $(document).ready(function () {
        if (history.state != undefined) {
            if (history.state.level != undefined) {
                $("#level-" + history.state.level.toLowerCase()).prop("checked", true);
            }
            if (history.state.direction != undefined) {
                $("#direction-" + history.state.direction.toLowerCase()).prop("checked", true);
            }
            if (history.state.filterEdges != undefined) {
                $("input[name=filterEdges]").prop("checked", history.state.filterEdges);
            }
            if (history.state.filter != undefined) {
                $("#filter").val(history.state.filter);
            }
            if (history.state.depth != undefined) {
                $("input[name=depth]").val(history.state.depth);
            }            
            if (history.state.revision != undefined) {
                $('.selectpicker').selectpicker();
                $('.selectpicker').selectpicker('val', history.state.revision);
                changeRevisionNumber(history.state.revision);
            }
            
            appanedSerializedIds(history.state.selectedItems, selectedItems);
        } else if (Manta.Globals.revision != Manta.Globals.revisionList[Manta.Globals.revisionList.length-1].revision) {
            $('.selectpicker').selectpicker();
            $('.selectpicker').selectpicker('val', Manta.Globals.revision);
        } else if (Manta.Globals.revision != $("#revisionNumberInForm").val()) {
            // Zajistime nastaveni spravne revize v pripade duplikace zalozky prohlizece 
            changeRevisionNumber(Manta.Globals.revision);
        }
        
        loadDetailsforIds(selectedItems);
        initFilterLayers();
   });
});


