package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.DataFlowEdgesFinder.FlowFinderResult;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowEdge;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Component;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.FullComponent;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.AbstractVizualizeTest;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:mvc-dispatcher-servlet.xml" })
public class VerticesHolderTest extends AbstractVizualizeTest {

    @Autowired
    ResourceLoader resourceLoader;

    public VerticesHolderTest() {
        super();
        componentFactory.setTechnicalAttributesHolder(new DummyTechnicalAttributesHolder());
    }

    @Test
    public void testDiscoverSiblings() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                GraphCreation.createNode(transaction, table1, "t1c3", "Column", REVISION_1_000000);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());

                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 7, FlowLevel.BOTTOM, true);

                List<Vertex> newVisitedNodes = new ArrayList<>();
                List<FlowEdge> edges = new ArrayList<>();
                edges.add(new FlowEdge(t1c1, t2c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000));

                VerticesHolder verticesHolder = new VerticesHolder(levelMapProvider, transaction, flowState,
                        getRelationshipService(), componentFactory);
                verticesHolder.discoverSiblings(edges, newVisitedNodes);

                Set<Component> components = verticesHolder.generateComponents(new VertexEquivalencesHolder());

                Assert.assertEquals(2, components.size()); // snizeno kvuli vyhozeni unknown sourozencum
                Assert.assertEquals(2, newVisitedNodes.size());
                Assert.assertEquals("t1c2", newVisitedNodes.get(0).getProperty(NodeProperty.NODE_NAME.t()));
                Assert.assertEquals("t2c1", newVisitedNodes.get(1).getProperty(NodeProperty.NODE_NAME.t()));
                return null;
            }
        });
    }

    @Test
    public void testCreateComponentsForStartNodes() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());

                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 7, FlowLevel.BOTTOM, true);

                List<Vertex> newVisitedNodes = new ArrayList<>();

                VerticesHolder verticesHolder = new VerticesHolder(levelMapProvider, transaction, flowState,
                        getRelationshipService(), componentFactory);
                verticesHolder.createComponentsForStartNodes(Collections.singleton(t1c1), newVisitedNodes);

                Set<Component> generatedComponents = verticesHolder.generateComponents(new VertexEquivalencesHolder());
                Assert.assertEquals(2, generatedComponents.size());
                Assert.assertEquals(2, newVisitedNodes.size());
                Assert.assertEquals(NodeStatus.VISITED, flowState.getNodeStatus(t1c1));
                Assert.assertEquals(NodeStatus.VISITED, flowState.getNodeStatus(t1c2));
                return null;
            }
        });
    }

    @Test
    public void testCreateComponentsFromEdges() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());

                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 7, FlowLevel.BOTTOM, true);

                List<Vertex> newVisitedNodes = new ArrayList<>();
                List<FlowEdge> edges = new ArrayList<>();
                edges.add(new FlowEdge(t1c1, t2c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000));

                VerticesHolder verticesHolder = new VerticesHolder(levelMapProvider, transaction, flowState,
                        getRelationshipService(), componentFactory);
                verticesHolder.discoverSiblings(edges, newVisitedNodes);

                Set<Component> components = verticesHolder.generateComponents(new VertexEquivalencesHolder());

                Assert.assertEquals(2, components.size()); // jen dva, protoze se touto metodou nevkladaji ty uzly pro ktere sourozence hledame
                Assert.assertEquals(2, newVisitedNodes.size());
                Assert.assertEquals("t1c2", newVisitedNodes.get(0).getProperty(NodeProperty.NODE_NAME.t()));
                Assert.assertEquals("t2c1", newVisitedNodes.get(1).getProperty(NodeProperty.NODE_NAME.t()));
                return null;
            }
        });
    }

    @Test
    public void testProcessParentsCollapsing() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());

                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 7, FlowLevel.MIDDLE, true);

                List<Vertex> newVisitedNodes = new ArrayList<>();

                VerticesHolder verticesHolder = new VerticesHolder(levelMapProvider, transaction, flowState,
                        getRelationshipService(), componentFactory);
                verticesHolder.createComponentsForStartNodes(Collections.singleton(t1c1), newVisitedNodes);
                verticesHolder.processParents(Collections.singleton(t1c1));

                Set<Component> components = verticesHolder.generateComponents(new VertexEquivalencesHolder());
                Assert.assertEquals(4, components.size());
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c2, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table1,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, db, flowState,
                        levelMapProvider, getRelationshipService())));

                for (Component c : components) {
                    if (c.getId().equals(db.getId())) {
                        Assert.assertTrue(((FullComponent) c).isExpanded());
                    } else if (c.getId().equals(table1.getId())) {
                        Assert.assertTrue(((FullComponent) c).isExpanded());
                    }
                }
                return null;
            }
        });
    }

    @Test
    public void testProcessParentsMixed() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                Vertex table3 = GraphCreation.createNode(transaction, table1, "table3", "Table", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);
                GraphCreation.createEdge(t1c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());

                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 9, FlowLevel.MIDDLE, true);

                List<Vertex> newVisitedNodes = new ArrayList<>();

                VerticesHolder verticesHolder = new VerticesHolder(levelMapProvider, transaction, flowState,
                        getRelationshipService(), componentFactory);
                verticesHolder.createComponentsForStartNodes(Collections.singleton(t1c1), newVisitedNodes);
                verticesHolder.createComponentsForStartNodes(Collections.singleton(t3c1), newVisitedNodes);
                verticesHolder.processParents(Collections.singleton(t1c1));

                Set<Component> components = verticesHolder.generateComponents(new VertexEquivalencesHolder());
                Assert.assertEquals(6, components.size());
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c2, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t3c1, flowState,
                        getRelationshipService(), levelMapProvider)));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table1,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, db, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table3,
                        flowState, levelMapProvider, getRelationshipService())));

                for (Component c : components) {
                    if (c.getId().equals(db.getId())) {
                        Assert.assertTrue(((FullComponent) c).isExpanded());
                    } else if (c.getId().equals(table1.getId())) {
                        Assert.assertTrue(((FullComponent) c).isExpanded());
                    } else if (c.getId().equals(table3.getId())) {
                        Assert.assertFalse(((FullComponent) c).isExpanded());
                    }
                }
                return null;
            }
        });
    }

    @Test
    public void testCheckEdgeEnds() {
        cleanGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradata = GraphCreation.createResource(transaction, root, "Filesystem", "Filesystem", "t",
                        layer, REVISION_1_000000);
                Vertex folder = GraphCreation.createNode(transaction, teradata, "folder", "Directory", REVISION_1_000000);

                Vertex folder1 = GraphCreation.createNode(transaction, folder, "folder1", "Directory", REVISION_1_000000);
                Vertex column11 = GraphCreation.createNode(transaction, folder1, "Column11", "Column", REVISION_1_000000);

                Vertex column2 = GraphCreation.createNode(transaction, folder, "Column2", "Column", REVISION_1_000000);
                Vertex column21 = GraphCreation.createNode(transaction, column2, "Column21", "Column", REVISION_1_000000);
                Vertex column22 = GraphCreation.createNode(transaction, column2, "Column22", "Column", REVISION_1_000000);
                Vertex column221 = GraphCreation.createNode(transaction, column22, "Column221", "Column", REVISION_1_000000);

                GraphCreation.createEdge(column11, column21, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(column21, column221, EdgeLabel.DIRECT, REVISION_1_000000);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                flowState.addNode(column11);
                flowState.addNode(column21);
                flowState.addNode(column221);

                // nejprve si připravit referenční view
                initRef(transaction, column11, levelMapProvider, flowState, 7, FlowLevel.MIDDLE, true);

                FollowEdgesFinder finder = new FollowEdgesFinder(getRelationshipService(), levelMapProvider,
                        transaction, flowState, getDataSizeChecker(), getTerchnicalAttributes());
                Set<Vertex> newVisitedNodesOutput = new HashSet<>();
                FlowFinderResult finderResult = finder.findEdges(Collections.singleton(column11), Direction.OUT, 2,
                        newVisitedNodesOutput);
                Collection<FlowEdge> edges = finderResult.getEdges();
                finderResult.visitProcessedVertices(transaction, flowState);

                VerticesHolder verticesHolder = new VerticesHolder(levelMapProvider, transaction, flowState,
                        getRelationshipService(), componentFactory);
                verticesHolder.createComponentsFromEdges(edges, Collections.singleton(column11));
                verticesHolder.processParents(Collections.singleton(column11));
                verticesHolder.checkEdgeEnds(edges);

                Set<Component> components = verticesHolder.generateComponents(new VertexEquivalencesHolder());
                Assert.assertEquals(7, components.size());

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, folder,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, folder1,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, column11,
                        flowState, levelMapProvider, getRelationshipService())));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, column2,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(column21, flowState,
                        getRelationshipService(), levelMapProvider)));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(column22, flowState,
                        getRelationshipService(), levelMapProvider)));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(column221, flowState,
                        getRelationshipService(), levelMapProvider)));

                return null;
            }
        });
    }
}
