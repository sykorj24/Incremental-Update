package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.DataFlowEdgesFinder.FlowFinderResult;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowEdge;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.exception.TooManyEdgesException;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.ComponentFactory;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.AbstractVizualizeTest;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:mvc-dispatcher-servlet.xml" })
public class AggEdgesFinderTest extends AbstractVizualizeTest {

    @Autowired
    ResourceLoader resourceLoader;

    @Test
    public void testCreateAggEdges() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                Vertex t1c3 = GraphCreation.createNode(transaction, table1, "t1c3", "Column", REVISION_1_000000);
                Vertex t1c4 = GraphCreation.createNode(transaction, table1, "t1c4", "Column", REVISION_1_000000);
                GraphCreation.createEdge(t1c1, t2c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c1, t1c3, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c3, t1c4, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c4, t2c2, EdgeLabel.DIRECT, REVISION_1_000000);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                flowState.addNode(t1c1);
                flowState.addNode(t1c2);
                flowState.addNode(t2c1);
                flowState.addNode(t2c2);
                flowState.addNode(t1c3);
                flowState.addNode(t1c4);

                FollowEdgesFinder followFinder = new FollowEdgesFinder(getRelationshipService(),
                        getLevelMapProvider(resourceLoader), transaction, flowState, getDataSizeChecker(),
                        getTerchnicalAttributes());
                Set<Vertex> newVisitedNodes = new HashSet<>();
                FlowFinderResult finderResult = followFinder.findEdges(Collections.singleton(t1c1), Direction.OUT, 1,
                        newVisitedNodes);
                finderResult.visitProcessedVertices(transaction, flowState);

                ComponentFactory componentFactory = new ComponentFactory();

                AggEdgesFinder aggFinder = new AggEdgesFinder(transaction, flowState, new VerticesHolder(
                        getLevelMapProvider(resourceLoader), transaction, flowState, getRelationshipService(),
                        componentFactory), 2000, getTerchnicalAttributes());
                Collection<FlowEdge> aggEdges = null;
                try {
                    aggEdges = aggFinder.findEdges(newVisitedNodes, Direction.OUT);
                } catch (TooManyEdgesException e) {
                    throw new RuntimeException(e);
                }
                Assert.assertEquals(4, aggEdges.size());

                Assert.assertTrue(aggEdges
                        .contains(new FlowEdge(t1c3, t1c4, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_999999)));
                Assert.assertTrue(aggEdges
                        .contains(new FlowEdge(t1c4, t2c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_999999)));

                Assert.assertTrue(aggEdges
                        .contains(new FlowEdge(t2c1, t2c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_999999)));
                Assert.assertTrue(aggEdges
                        .contains(new FlowEdge(t1c2, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_999999)));

                return null;
            }
        });
    }
    
    @Test
    public void testCreateAggEdgesMax() {
        cleanGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "TeraType",
                        "Description", layer, REVISION_1_000000);
                Vertex db = GraphCreation.createNode(transaction, teradataRes, "db", "Database", REVISION_1_000000);
                
                Vertex table1 = GraphCreation.createNode(transaction, db, "table1", "Table", REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                Vertex t1c2 = GraphCreation.createNode(transaction, table1, "t1c2", "Column", REVISION_1_000000);
                
                Vertex table2 = GraphCreation.createNode(transaction, db, "table2", "Table", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
                
                Vertex table3 = GraphCreation.createNode(transaction, db, "table3", "Table", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);
                
                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c2, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, t1c1, EdgeLabel.DIRECT, REVISION_1_000000);                

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                flowState.addNode(t1c1);
                flowState.addNode(t1c2);
                flowState.addNode(t2c1);
                flowState.addNode(t3c1);
               
                flowState.visitNode(t1c1);
                flowState.visitNode(t1c2);                
                Set<Vertex> newVisitedNodes = new HashSet<>();
                newVisitedNodes.add(t2c1);               
                ComponentFactory componentFactory = new ComponentFactory();

                AggEdgesFinder aggFinder = new AggEdgesFinder(transaction, flowState, new VerticesHolder(
                        getLevelMapProvider(resourceLoader), transaction, flowState, getRelationshipService(),
                        componentFactory), 0, getTerchnicalAttributes());
                Collection<FlowEdge> aggEdges = new HashSet<>();
                boolean exceptionThrown = false;
                try {
                    aggEdges = aggFinder.findEdges(newVisitedNodes, Direction.OUT);
                } catch (TooManyEdgesException e) {
                    exceptionThrown = true;
                    aggEdges.addAll(aggFinder.findDirectEdges(newVisitedNodes, Direction.OUT));
                    aggEdges.addAll(aggFinder.findDirectEdges(newVisitedNodes, Direction.IN));
                }
                Assert.assertTrue(exceptionThrown);
                Assert.assertEquals(2, aggEdges.size());

                Assert.assertTrue(aggEdges
                        .contains(new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_999999)));
                Assert.assertTrue(aggEdges
                        .contains(new FlowEdge(t1c2, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_999999)));
                return null;
            }
        });
    }
}
