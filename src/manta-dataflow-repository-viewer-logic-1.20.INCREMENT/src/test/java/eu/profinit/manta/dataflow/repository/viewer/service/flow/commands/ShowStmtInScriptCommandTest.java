package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.List;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.AttributeNames;
import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

public class ShowStmtInScriptCommandTest extends AbstractVizualizeTest {
    
    private static final RevisionInterval REV_INTERVAL = new RevisionInterval(1, 2);
    
    
    @Test
    public void testFetchPosition() {
        
        cleanGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex superRoot = getSuperRootHandler().getRoot(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex res = GraphCreation.createResource(transaction, superRoot, "IFPC", "IFPC type", "IFPC desc", layer, REV_INTERVAL);

                // Test
                ShowStmtInScriptCommand cmd = new ShowStmtInScriptCommand();
                
                // Spatne labels
                Assert.assertEquals(null, cmd.fetchPosition(make("<1,2>", null, res, transaction), REV_INTERVAL));
                Assert.assertEquals(null, cmd.fetchPosition(make("<1,> a", null, res, transaction), REV_INTERVAL));
                Assert.assertEquals(null, cmd.fetchPosition(make("<,1> a", null, res, transaction), REV_INTERVAL));
                Assert.assertEquals(null, cmd.fetchPosition(make("<11 a", null, res, transaction), REV_INTERVAL));
                
                // Spravne labels
                Assert.assertEquals(new ShowStmtInScriptCommand.Position(1,2),
                    cmd.fetchPosition(make("<1,2>a", null, res, transaction), REV_INTERVAL));
                
                Assert.assertEquals(new ShowStmtInScriptCommand.Position(1,2),
                    cmd.fetchPosition(make("<1,2>a a", null, res, transaction), REV_INTERVAL));
                
                Assert.assertEquals(new ShowStmtInScriptCommand.Position(1,2),
                    cmd.fetchPosition(make("<1,2 >a", null, res, transaction), REV_INTERVAL));
                
                Assert.assertEquals(new ShowStmtInScriptCommand.Position(1,2),
                    cmd.fetchPosition(make("<1, 2 >a", null, res, transaction), REV_INTERVAL));
                
                Assert.assertEquals(new ShowStmtInScriptCommand.Position(1354,274),
                    cmd.fetchPosition(make("<1354, 274 >a", null, res, transaction), REV_INTERVAL));
                
                Assert.assertEquals(new ShowStmtInScriptCommand.Position(71,1),
                    cmd.fetchPosition(make("<71,1>Apply-SQL1", null, res, transaction), REV_INTERVAL));
                
                // Spatne attributes
                Assert.assertEquals(null, cmd.fetchPosition(make("a", "<1,2>", res, transaction), REV_INTERVAL));
                Assert.assertEquals(null, cmd.fetchPosition(make("a", "1,", res, transaction), REV_INTERVAL));
                Assert.assertEquals(null, cmd.fetchPosition(make("a", ",1", res, transaction), REV_INTERVAL));
                Assert.assertEquals(null, cmd.fetchPosition(make("a", "11 ", res, transaction), REV_INTERVAL));
                
                // Spravne attribtes
                Assert.assertEquals(new ShowStmtInScriptCommand.Position(3,4),
                    cmd.fetchPosition(make("<1,2>a", "3,4", res, transaction), REV_INTERVAL));
                
                Assert.assertEquals(new ShowStmtInScriptCommand.Position(3,4),
                    cmd.fetchPosition(make("<1,2 >a", "3,4 ", res, transaction), REV_INTERVAL));
                
                Assert.assertEquals(new ShowStmtInScriptCommand.Position(3,4),
                    cmd.fetchPosition(make("<1, 2 >a", "3, 4", res, transaction), REV_INTERVAL));
                
                Assert.assertEquals(new ShowStmtInScriptCommand.Position(3,4),
                    cmd.fetchPosition(make("<1354, 274 >a", " 3,4", res, transaction), REV_INTERVAL));
                
                Assert.assertEquals(new ShowStmtInScriptCommand.Position(3,4),
                    cmd.fetchPosition(make("<71,1>Apply-SQL1", "3 ,4", res, transaction), REV_INTERVAL));

                
                
                return null;
            }
        });
    }
    
    @Test
    public void testRevision() {
        cleanGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex superRoot = getSuperRootHandler().getRoot(transaction);

                RevisionInterval revInterval13 = new RevisionInterval(1, 3);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex res = GraphCreation.createResource(transaction, superRoot, "Teradata", "Teradata", "Teradata",
                        layer, revInterval13);
                Vertex db = GraphCreation.createNode(transaction, res, "db", "Database", revInterval13);
                Vertex procedure = GraphCreation.createNode(transaction, db, "proc", "Procedure", revInterval13);
                Vertex statement = GraphCreation.createNode(transaction, procedure, "stmt", "statement", revInterval13);

                GraphCreation.createNodeAttribute(transaction, db, ShowStmtInScriptCommand.SOURCE_LOCATION, "bla1",
                        new RevisionInterval(1));
                GraphCreation.createNodeAttribute(transaction, db, ShowStmtInScriptCommand.SOURCE_LOCATION, "bla1",
                        new RevisionInterval(1));

                GraphCreation.createNodeAttribute(transaction, procedure, ShowStmtInScriptCommand.SOURCE_LOCATION,
                        "bla1", new RevisionInterval(1));
                GraphCreation.createNodeAttribute(transaction, procedure, ShowStmtInScriptCommand.SOURCE_LOCATION,
                        "bla3", new RevisionInterval(3));

                ShowStmtInScriptCommand cmd = new ShowStmtInScriptCommand();
                List<Vertex> parents = GraphOperation.getAllParent(statement);
                Assert.assertEquals(new ShowStmtInScriptCommand.SourceFile("bla3", "utf-8"),
                        cmd.findSourceLocation(statement, parents, revInterval13));
                Assert.assertEquals(new ShowStmtInScriptCommand.SourceFile("bla1", "utf-8"),
                        cmd.findSourceLocation(statement, parents, new RevisionInterval(1)));

                parents = GraphOperation.getAllParent(procedure);
                Assert.assertEquals(new ShowStmtInScriptCommand.SourceFile("bla3", "utf-8"),
                        cmd.findSourceLocation(procedure, parents, revInterval13));

                parents = GraphOperation.getAllParent(db);
                Assert.assertNull(cmd.findSourceLocation(db, parents, revInterval13));
                Assert.assertNull(cmd.findSourceLocation(db, parents, new RevisionInterval(1)));

                return null;
            }
        });
    }
    
    /**
     * Vytvoř uzel k testování.
     *
     * @param name jméno / label
     * @param posAttribute atribut pozice nebo null
     * @param parent rodič
     * @param transaction transakce
     * 
     * @return uzel
     */
    private static Vertex make(String name, Object posAttribute, Vertex parent, TitanTransaction transaction) {
        Vertex result = GraphCreation.createNode(transaction, parent, name, "Test", REV_INTERVAL);
        if (posAttribute != null) {
            GraphCreation.createNodeAttribute(transaction, result, AttributeNames.NODE_SCRIPT_POSITION, posAttribute,
                REV_INTERVAL);
        }
        return result;
    }
}
