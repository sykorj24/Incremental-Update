package eu.profinit.manta.dataflow.repository.viewer.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.GraphFlow;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.AbstractVizualizeTest;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:mvc-dispatcher-servlet.xml")
public class ViewerOperationTest extends AbstractVizualizeTest {

    @Autowired
    ResourceLoader resourceLoader;

    @Test
    public void testHasAdjacentVerticesInFollow() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata", "Teradata", "Desc", layer, REVISION_1_000000);
                Vertex table = GraphCreation.createNode(transaction, resource, "Table", "Table", REVISION_1_000000);
                Vertex c1 = GraphCreation.createNode(transaction, table, "c1", "Column", REVISION_1_000000);
                Vertex c2 = GraphCreation.createNode(transaction, table, "c2", "Column", REVISION_1_000000);
                Vertex c3 = GraphCreation.createNode(transaction, table, "c3", "Column", REVISION_1_000000);
                Vertex c4 = GraphCreation.createNode(transaction, table, "c4", "Column", REVISION_1_000000);
                
                GraphCreation.createEdge(c1, c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c3, c1, EdgeLabel.FILTER, REVISION_1_000000);
                
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                initRef(transaction, c1, getLevelMapProvider(resourceLoader), flowState, 4, FlowLevel.BOTTOM, true);
                
                Assert.assertTrue(GraphFlow.hasAdjacentVerticesInRefView(c1, flowState));
                Assert.assertTrue(GraphFlow.hasAdjacentVerticesInRefView(c2, flowState));
                Assert.assertTrue(GraphFlow.hasAdjacentVerticesInRefView(c3, flowState));
                Assert.assertFalse(GraphFlow.hasAdjacentVerticesInRefView(c4, flowState));
                
                flowState.setFilterEdgesShown(false);

                Assert.assertTrue(GraphFlow.hasAdjacentVerticesInRefView(c1, flowState));
                Assert.assertTrue(GraphFlow.hasAdjacentVerticesInRefView(c2, flowState));
                Assert.assertFalse(GraphFlow.hasAdjacentVerticesInRefView(c3, flowState));
                Assert.assertFalse(GraphFlow.hasAdjacentVerticesInRefView(c4, flowState));

                return null;
            }
        });
    }
}
