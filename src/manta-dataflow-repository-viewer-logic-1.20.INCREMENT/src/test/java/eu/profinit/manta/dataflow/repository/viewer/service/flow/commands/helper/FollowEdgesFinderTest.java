package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.DataFlowEdgesFinder.FlowFinderResult;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowEdge;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.AbstractVizualizeTest;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:mvc-dispatcher-servlet.xml" })
public class FollowEdgesFinderTest extends AbstractVizualizeTest {

    @Autowired
    ResourceLoader resourceLoader;

    @Test
    public void testCreateFollowEdges() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                Vertex t1c3 = GraphCreation.createNode(transaction, table1, "t1c3", "Column", REVISION_1_000000);
                Vertex t2c3 = GraphCreation.createNode(transaction, table2, "t2c3", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t1c3, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c2, t2c3, EdgeLabel.DIRECT, REVISION_1_000000);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_999999, getHorizontalFilterProvider());
                flowState.addNode(t1c1);
                flowState.addNode(t1c2);
                flowState.addNode(t1c3);
                flowState.addNode(t2c1);
                flowState.addNode(t2c2);
                flowState.addNode(t2c3);

                FollowEdgesFinder finder = new FollowEdgesFinder(getRelationshipService(),
                        getLevelMapProvider(resourceLoader), transaction, flowState, getDataSizeChecker(),
                        getTerchnicalAttributes());
                Set<Vertex> visited = new HashSet<>();
                FlowFinderResult finderResult = finder.findEdges(Collections.singleton(t1c1), Direction.OUT, 2,
                        visited);
                Set<FlowEdge> edges = finderResult.getEdges();
                finderResult.visitProcessedVertices(transaction, flowState);

                Assert.assertEquals(1, visited.size());
                Assert.assertTrue(visited.contains(t2c2));

                Assert.assertEquals(5, edges.size());
                Assert.assertTrue(
                        edges.contains(new FlowEdge(t1c1, t1c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_999999)));
                Assert.assertTrue(
                        edges.contains(new FlowEdge(t1c1, t1c3, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_999999)));
                Assert.assertTrue(
                        edges.contains(new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.FILTER, REVISION_INTERVAL_1_000000_1_999999)));
                Assert.assertTrue(
                        edges.contains(new FlowEdge(t1c2, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_999999)));
                Assert.assertTrue(
                        edges.contains(new FlowEdge(t2c1, t2c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_999999)));

                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(t1c1));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(t1c2));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(t1c3));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(t2c1));
                Assert.assertEquals(NodeStatus.VISITED, flowState.getNodeStatus(t2c2));
                return null;
            }
        });
    }
}
