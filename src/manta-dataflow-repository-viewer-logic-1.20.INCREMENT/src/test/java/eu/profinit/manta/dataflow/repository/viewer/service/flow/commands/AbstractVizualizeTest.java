package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.Collections;

import org.springframework.core.io.ResourceLoader;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.GraphFlowAlgorithmExecutor;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.GraphFlowAlgorithmFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.routine.RoutineDataFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterEdgeGraphFlowFilter;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.GraphFlowFilter;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.filter.vertical.VerticalFilterProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.ComponentFactory;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.InitRequest;
import eu.profinit.manta.dataflow.repository.viewer.service.DataSizeChecker;
import eu.profinit.manta.dataflow.repository.viewer.service.FilteredRelationshipService;
import eu.profinit.manta.dataflow.repository.viewer.service.XmlLevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.service.filter.XmlVerticalFilterProvider.VertexPosition;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.GraphService;
import junit.framework.Assert;

public abstract class AbstractVizualizeTest extends TestTitanDatabaseProvider {

    private FilteredRelationshipService relationshipService = new FilteredRelationshipService();
    private DataSizeChecker dataSizeChecker;
    private DataSizeChecker dataSizeCheckerAlwaysLarge;
    protected ComponentFactory componentFactory = new ComponentFactory();

    public AbstractVizualizeTest() {
        super();
        init();
    }
    
    public AbstractVizualizeTest(String dir, boolean cleanDir) {
        super(dir, cleanDir);
        init();
    }

    private void init() {
        relationshipService.setFilterProvider(new DummyVerticalFilterProvider());

        dataSizeChecker = new DataSizeChecker();
        dataSizeChecker.setMaxSize(100000);
        dataSizeChecker.setMaximumStartNodes(150);
        dataSizeChecker.setMaximumAggEdges(2000);

        dataSizeCheckerAlwaysLarge = new DataSizeChecker();
        dataSizeCheckerAlwaysLarge.setMaxSize(0);
        dataSizeCheckerAlwaysLarge.setMaximumStartNodes(0);
        dataSizeCheckerAlwaysLarge.setMaximumStartNodesForExport(0);

        componentFactory.setTechnicalAttributesHolder(new DummyTechnicalAttributesHolder());
    }

    public GraphService getGraphService() {
        GraphService service = new GraphService();
        service.setDatabaseService(getDatabaseHolder());
        return service;
    }

    public static LevelMapProvider getLevelMapProvider(ResourceLoader resourceLoader) {
        XmlLevelMapProvider provider = new XmlLevelMapProvider();
        provider.setResourceLoader(resourceLoader);
        provider.setLevelsFileLocation("levels.xml");
        return provider;
    }

    protected void initRef(TitanTransaction transaction, Vertex t1c1, LevelMapProvider levelMapProvider,
            FlowStateViewer flowState, int expectedVertices, FlowLevel flowLevel, boolean filterEdges) {
        // Create command.
        InitReferenceViewCommand commandInitRef = new InitReferenceViewCommand();
        commandInitRef.setLevelMapProvider(levelMapProvider);
        commandInitRef.setRelationshipService(getRelationshipService());
        commandInitRef.setAlgorithmExecutor(getAlgorithmExecutor());

        // Create request
        InitRequest request = new InitRequest();
        request.setReferencePoints(Collections.singletonList((Long) t1c1.getId()));
        request.setFlowLevel(flowLevel);
        request.setFilterEdges(filterEdges);

        // Execute the command
        commandInitRef.execute(request, flowState, transaction);
        Assert.assertEquals(expectedVertices, flowState.getReferenceNodeIds().size());
    }

    protected GraphFlowAlgorithmExecutor getAlgorithmExecutor() {
        GraphFlowAlgorithmExecutor executor = new GraphFlowAlgorithmExecutor();
        GraphFlowFilter filter = new FilterEdgeGraphFlowFilter();
        GraphFlowAlgorithmFactory factory = new GraphFlowAlgorithmFactoryGeneric<RoutineDataFlowAlgorithm>(
                RoutineDataFlowAlgorithm.class, Collections.<GraphFlowFilter> singletonList(filter));
        executor.setAlgorithmFactories(Collections.<GraphFlowAlgorithmFactory> singletonList(factory));
        return executor;
    }

    public FilteredRelationshipService getRelationshipService() {
        return relationshipService;
    }

    public DataSizeChecker getDataSizeChecker() {
        return dataSizeChecker;
    }

    public DataSizeChecker getDataSizeCheckerAlwaysLarge() {
        return dataSizeCheckerAlwaysLarge;
    }

    public ComponentFactory getComponentFactory() {
        return componentFactory;
    }

    private class DummyVerticalFilterProvider implements VerticalFilterProvider {

        @Override
        public boolean isElementDisabled(String resourceName, String elementType, VertexPosition position) {
            return false;
        }

        @Override
        public boolean isElementDisabled(Vertex actualVertex, Vertex startVertex) {
            return false;
        }
    }

}
