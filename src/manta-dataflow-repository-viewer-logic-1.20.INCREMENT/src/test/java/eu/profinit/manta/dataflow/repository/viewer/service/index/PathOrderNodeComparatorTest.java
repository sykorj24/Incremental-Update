package eu.profinit.manta.dataflow.repository.viewer.service.index;

import java.util.Arrays;

import org.junit.Test;

import eu.profinit.manta.dataflow.repository.viewer.model.index.Node;
import junit.framework.Assert;

public class PathOrderNodeComparatorTest {

    private static final PathOrderNodeComparator COMPARATOR = new PathOrderNodeComparator();

    @Test
    public void testCompareSame() {

        Node leftNode = new Node();
        leftNode.setText("a");
        leftNode.setPath(Arrays.asList("a", "b"));

        Node rightNode = new Node();
        rightNode.setText("a");
        rightNode.setPath(Arrays.asList("a", "b"));

        Assert.assertEquals(0, COMPARATOR.compare(leftNode, rightNode));
    }

    @Test
    public void testCompareText1() {

        Node leftNode = new Node();
        leftNode.setText("a2");
        leftNode.setPath(Arrays.asList("a", "b"));

        Node rightNode = new Node();
        rightNode.setText("a1");
        rightNode.setPath(Arrays.asList("a", "b"));

        Assert.assertEquals(1, COMPARATOR.compare(leftNode, rightNode));
    }

    @Test
    public void testCompareText2() {

        Node leftNode = new Node();
        leftNode.setText("a1");
        leftNode.setPath(Arrays.asList("a", "b"));

        Node rightNode = new Node();
        rightNode.setText("a2");
        rightNode.setPath(Arrays.asList("a", "b"));

        Assert.assertEquals(-1, COMPARATOR.compare(leftNode, rightNode));
    }

    @Test
    public void testComparePathShorterLeft() {

        Node leftNode = new Node();
        leftNode.setText("a");
        leftNode.setPath(Arrays.asList("a", "b"));

        Node rightNode = new Node();
        rightNode.setText("a");
        rightNode.setPath(Arrays.asList("a", "b", "c"));

        Assert.assertEquals(-1, COMPARATOR.compare(leftNode, rightNode));
    }

    @Test
    public void testComparePathShorterRight() {

        Node leftNode = new Node();
        leftNode.setText("a");
        leftNode.setPath(Arrays.asList("a", "b", "c"));

        Node rightNode = new Node();
        rightNode.setText("a");
        rightNode.setPath(Arrays.asList("a", "b"));

        Assert.assertEquals(1, COMPARATOR.compare(leftNode, rightNode));
    }

    @Test
    public void testComparePathDifferent1() {
        Node leftNode = new Node();
        leftNode.setText("a");
        leftNode.setPath(Arrays.asList("a", "b", "c1"));

        Node rightNode = new Node();
        rightNode.setText("a");
        rightNode.setPath(Arrays.asList("a", "b", "c2"));

        Assert.assertEquals(-1, COMPARATOR.compare(leftNode, rightNode));
    }

    @Test
    public void testComparePathDifferent2() {
        Node leftNode = new Node();
        leftNode.setText("a");
        leftNode.setPath(Arrays.asList("a", "b", "c2"));

        Node rightNode = new Node();
        rightNode.setText("a");
        rightNode.setPath(Arrays.asList("a", "b", "c1"));

        Assert.assertEquals(1, COMPARATOR.compare(leftNode, rightNode));
    }

    @Test
    public void testComparePathDifferent3() {
        Node leftNode = new Node();
        leftNode.setText("a");
        leftNode.setPath(Arrays.asList("a", "b", "c2", "d1"));

        Node rightNode = new Node();
        rightNode.setText("a");
        rightNode.setPath(Arrays.asList("a", "b", "c1", "d2"));

        Assert.assertEquals(1, COMPARATOR.compare(leftNode, rightNode));
    }
}
