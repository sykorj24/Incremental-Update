package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Resource;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ResourceListRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ResourceListResponse;

public class ResourceListCommandTest extends AbstractVizualizeTest {
    @Test
    public void testCommand() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex superRoot = getSuperRootHandler().getRoot(transaction);

                Vertex resTeradata = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator()
                        .next();
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resIfcp = GraphCreation.createResource(transaction, superRoot, "IFPC", "IFPC type", "IFPC desc", layer, REVISION_1_000000);

                ResourceListCommand command = new ResourceListCommand();
                command.setRelationshipService(getRelationshipService());

                ResourceListRequest request = new ResourceListRequest();
                request.setRootId((Long) superRoot.getId());
                FlowStateViewer referenceView = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                ResourceListResponse response = command.execute(request, referenceView, transaction);

                Assert.assertEquals(2, response.getResourceList().size());
                Assert.assertEquals(resTeradata.getId(), getResource(response.getResourceList(), "Teradata").getId());
                Assert.assertEquals(resIfcp.getId(), getResource(response.getResourceList(), "IFPC").getId());

                return null;
            }
        });
    }

    @Test
    public void testCommandCompareRevisions() {
        cleanGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex superRoot = getSuperRootHandler().getRoot(transaction);

                RevisionInterval revisionInterval13 = new RevisionInterval(1, 3);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resTeradata = GraphCreation.createResource(transaction, superRoot, "Teradata", "Teradata",
                        "Teradata", layer, revisionInterval13);
                Vertex resIfcp1 = GraphCreation.createResource(transaction, superRoot, "IFPC", "IFPC type", "IFPC desc",
                        layer, REVISION_1_000000);
                Vertex resIfcp3 = GraphCreation.createResource(transaction, superRoot, "IFPC", "IFPC type", "IFPC desc",
                        layer, REVISION_3_000000);

                ResourceListCommand command = new ResourceListCommand();
                command.setRelationshipService(getRelationshipService());

                ResourceListRequest request = new ResourceListRequest();
                request.setRootId((Long) superRoot.getId());
                FlowStateViewer referenceView = new FlowStateViewer(revisionInterval13, getHorizontalFilterProvider());
                ResourceListResponse response = command.execute(request, referenceView, transaction);

                Assert.assertEquals(2, response.getResourceList().size());
                Assert.assertEquals(resTeradata.getId(), getResource(response.getResourceList(), "Teradata").getId());
                Assert.assertEquals(resIfcp3.getId(), getResource(response.getResourceList(), "IFPC").getId());

                Assert.assertTrue(
                        referenceView.getEquivalencesHolder().isEquivalenceFromOlderRev((Long) resIfcp1.getId()));
                Assert.assertFalse(
                        referenceView.getEquivalencesHolder().isEquivalenceFromOlderRev((Long) resIfcp3.getId()));

                Assert.assertEquals(resIfcp1.getId(),
                        referenceView.getEquivalencesHolder().getEquivalentVertexId((Long) resIfcp3.getId()));
                Assert.assertEquals(resIfcp3.getId(),
                        referenceView.getEquivalencesHolder().getEquivalentVertexId((Long) resIfcp1.getId()));

                return null;
            }
        });
    }

    // pomocne privatni metody

    /**
     * Vrati zdroj z daneho seznamu s danym jmenem.
     * @param resourceList Seznam zdroju
     * @param resourceName Nazev hledaneho zdroje
     * @return Zdroj s danym nazev v danem seznamu nebo {@code null}, pokud zadny takovy neni. 
     */
    private Resource getResource(List<Resource> resourceList, String resourceName) {
        for (Resource resource : resourceList) {
            if (resourceName.equals(resource.getName())) {
                return resource;
            }
        }
        return null;
    }
    
}
