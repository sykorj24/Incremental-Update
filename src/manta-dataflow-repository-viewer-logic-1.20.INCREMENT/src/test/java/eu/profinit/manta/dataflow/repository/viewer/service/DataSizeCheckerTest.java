package eu.profinit.manta.dataflow.repository.viewer.service;

import junit.framework.Assert;

import org.junit.Test;

public class DataSizeCheckerTest {
    @Test
    public void testIsTooLarge() {
        DataSizeChecker checker = new DataSizeChecker();
        checker.setEdgeCoeficient(3);
        checker.setProxyCoeficient(5);
        checker.setFullCoeficient(7);
        
        checker.setMaxSize(3);
        Assert.assertFalse(checker.isDataTooLarge(1, 0, 0));
        Assert.assertTrue(checker.isDataTooLarge(2, 0, 0));
        
        checker.setMaxSize(5);
        Assert.assertFalse(checker.isDataTooLarge(0, 1, 0));
        Assert.assertTrue(checker.isDataTooLarge(0, 2, 0));
        
        checker.setMaxSize(7);
        Assert.assertFalse(checker.isDataTooLarge(0, 0, 1));
        Assert.assertTrue(checker.isDataTooLarge(0, 0, 2));
        
        checker.setMaxSize(34);
        Assert.assertFalse(checker.isDataTooLarge(1, 2, 3));
        Assert.assertTrue(checker.isDataTooLarge(2, 2, 3));
    }
}
