package eu.profinit.manta.dataflow.repository.viewer.service.flow;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.AbstractVizualizeTest;
import junit.framework.Assert;

public class GraphServiceTest extends AbstractVizualizeTest {

    @Test
    public void testGetRootId() {
        createBasicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                GraphService service = new GraphService();
                service.setDatabaseService(getDatabaseHolder());
                service.setSuperRootHandler(getSuperRootHandler());

                Assert.assertEquals(getSuperRootHandler().getRoot(transaction).getId(), service.getRootId());
                return null;
            }
        });
    }

}
