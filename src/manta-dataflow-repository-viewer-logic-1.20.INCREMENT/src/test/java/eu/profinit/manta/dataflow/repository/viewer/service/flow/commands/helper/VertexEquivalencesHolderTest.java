package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowEdge;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Component;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.FlowEdgeViz;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.FullComponent;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.FullComponent.AttributeWithRevState;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.RevisionState;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.AbstractVizualizeTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:mvc-dispatcher-servlet.xml" })
public class VertexEquivalencesHolderTest extends AbstractVizualizeTest {

    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(VertexEquivalencesHolderTest.class);

    @Autowired
    ResourceLoader resourceLoader;

    @Test
    public void testVertices() {
        cleanGraph();
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                RevisionInterval revInt2_6 = new RevisionInterval(2, 6);
                RevisionInterval revInt4_6 = new RevisionInterval(4, 6);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex r26_res2 = GraphCreation.createResource(transaction, root, "res2", "r2", "", layer, revInt2_6);
                Vertex r2_db2 = GraphCreation.createNode(transaction, r26_res2, "db2", "Database", REVISION_2_000000);
                Vertex r2_t2 = GraphCreation.createNode(transaction, r2_db2, "table2", "Table", REVISION_2_000000);
                Vertex r2_t2c1 = GraphCreation.createNode(transaction, r2_t2, "t2c1", "Column", REVISION_2_000000);

                Vertex r46_db2 = GraphCreation.createNode(transaction, r26_res2, "db2", "Database", revInt4_6);
                Vertex r4_t2 = GraphCreation.createNode(transaction, r46_db2, "table2", "Table", REVISION_4_000000);
                GraphCreation.createNode(transaction, r4_t2, "t2c1", "Column", REVISION_4_000000);
                Vertex r6_t2 = GraphCreation.createNode(transaction, r46_db2, "table2", "Table", REVISION_6_000000);
                Vertex r6_t2c1 = GraphCreation.createNode(transaction, r6_t2, "t2c1", "Column", REVISION_6_000000);

                Long r2_t2c1_id = (Long) r2_t2c1.getId();
                Long r6_t2c1_id = (Long) r6_t2c1.getId();

                VertexEquivalencesHolder holder = new VertexEquivalencesHolder();
                Set<Long> vertexIds = new HashSet<>();
                vertexIds.add(r2_t2c1_id);
                vertexIds.add(r6_t2c1_id);
                holder.buildForVertices(transaction, vertexIds, REVISION_6_000000);

                Assert.assertEquals(r2_t2c1_id, holder.getEquivalentVertexId(r6_t2c1_id));
                Assert.assertEquals(r6_t2c1_id, holder.getEquivalentVertexId(r2_t2c1_id));
                Assert.assertTrue(holder.isEquivalenceFromOlderRev(r2_t2c1_id));
                Assert.assertFalse(holder.isEquivalenceFromOlderRev(r6_t2c1_id));

                return null;
            }
        });
    }

    @Test
    public void testEdges() {
        cleanGraph();
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                RevisionInterval revInt1_3 = new RevisionInterval(1, 3);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex r13_r = GraphCreation.createResource(transaction, root, "res", "r", "", layer, revInt1_3);
                Vertex r13_db = GraphCreation.createNode(transaction, r13_r, "db", "Database", revInt1_3);
                Vertex r13_t = GraphCreation.createNode(transaction, r13_db, "table", "Table", revInt1_3);

                Vertex r1_tc1 = GraphCreation.createNode(transaction, r13_t, "tc1", "Column", REVISION_1_000000);
                Vertex r3_tc1 = GraphCreation.createNode(transaction, r13_t, "tc1", "Column", REVISION_3_000000);

                Vertex r13_tc2 = GraphCreation.createNode(transaction, r13_t, "tc2", "Column", revInt1_3);
                Vertex r1_tc3 = GraphCreation.createNode(transaction, r13_t, "tc3", "Column", REVISION_1_000000);
                Vertex r13_tc4 = GraphCreation.createNode(transaction, r13_t, "tc4", "Column", revInt1_3);
                Vertex r13_tc5 = GraphCreation.createNode(transaction, r13_t, "tc5", "Column", revInt1_3);

                VertexEquivalencesHolder holder = new VertexEquivalencesHolder();
                Set<Long> vertexIds = new HashSet<>();
                vertexIds.add((Long) r1_tc1.getId());
                vertexIds.add((Long) r3_tc1.getId());
                vertexIds.add((Long) r13_tc2.getId());
                vertexIds.add((Long) r1_tc3.getId());
                vertexIds.add((Long) r13_tc4.getId());
                vertexIds.add((Long) r13_tc5.getId());
                holder.buildForVertices(transaction, vertexIds, REVISION_3_000000);

                Set<FlowEdge> edges = new HashSet<>();
                edges.add(new FlowEdge(r1_tc1, r13_tc2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000));
                edges.add(new FlowEdge(r3_tc1, r13_tc2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_3_000000_3_000000));
                edges.add(new FlowEdge(r13_tc2, r1_tc3, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000));
                edges.add(new FlowEdge(r1_tc3, r13_tc4, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000));
                edges.add(new FlowEdge(r13_tc2, r13_tc4, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_3_000000_3_000000));
                edges.add(new FlowEdge(r13_tc4, r13_tc5, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_3_000000));

                Set<FlowEdgeViz> mappedEdges = holder.remapEdges(transaction, edges, revInt1_3);
                Assert.assertEquals(5, mappedEdges.size());
                Assert.assertTrue(
                        mappedEdges.contains(new FlowEdgeViz(r3_tc1, r13_tc2, EdgeLabel.DIRECT, RevisionState.STABLE)));
                Assert.assertTrue(mappedEdges
                        .contains(new FlowEdgeViz(r13_tc2, r1_tc3, EdgeLabel.DIRECT, RevisionState.DELETED)));
                Assert.assertTrue(mappedEdges
                        .contains(new FlowEdgeViz(r1_tc3, r13_tc4, EdgeLabel.DIRECT, RevisionState.DELETED)));
                Assert.assertTrue(mappedEdges
                        .contains(new FlowEdgeViz(r13_tc4, r13_tc5, EdgeLabel.DIRECT, RevisionState.STABLE)));
                Assert.assertTrue(
                        mappedEdges.contains(new FlowEdgeViz(r13_tc2, r13_tc4, EdgeLabel.DIRECT, RevisionState.NEW)));

                return null;
            }
        });
    }

    @Test
    public void testAttrs() {
        cleanGraph();
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                RevisionInterval revInt1_5 = new RevisionInterval(1, 5);
                RevisionInterval revInt3_5 = new RevisionInterval(3, 5);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex r15_r = GraphCreation.createResource(transaction, root, "res", "r", "", layer, revInt1_5);
                Vertex r1_db = GraphCreation.createNode(transaction, r15_r, "db", "Database", REVISION_1_000000);
                Vertex r35_db = GraphCreation.createNode(transaction, r15_r, "db", "Database", revInt3_5);

                GraphCreation.createNodeAttribute(transaction, r1_db, "k1", "v1", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, r1_db, "k1", "v2", REVISION_1_000000);

                GraphCreation.createNodeAttribute(transaction, r1_db, "k2", "v1", REVISION_1_000000);

                GraphCreation.createNodeAttribute(transaction, r35_db, "k1", "v1", revInt3_5);
                GraphCreation.createNodeAttribute(transaction, r35_db, "k1", "v3", revInt3_5);

                GraphCreation.createNodeAttribute(transaction, r35_db, "k2", "v3", REVISION_3_000000);

                GraphCreation.createNodeAttribute(transaction, r35_db, "k2", "v1", REVISION_5_000000);
                GraphCreation.createNodeAttribute(transaction, r35_db, "k2", "v2", REVISION_3_000000);
                GraphCreation.createNodeAttribute(transaction, r35_db, "k2", "v2", REVISION_5_000000);

                FlowStateViewer flowState = new FlowStateViewer(revInt1_5, getHorizontalFilterProvider());
                flowState.addNode((Long) r1_db.getId());
                flowState.addNode((Long) r35_db.getId());
                flowState.addStartNode((Long) r1_db.getId());
                flowState.addStartNode((Long) r35_db.getId());
                flowState.createEquivalencesForRefView(transaction);

                VerticesHolder verticesHolder = new VerticesHolder(getLevelMapProvider(resourceLoader), transaction,
                        flowState, getRelationshipService(), componentFactory);
                Set<Vertex> startNodes = new HashSet<>();
                startNodes.add(r1_db);
                startNodes.add(r35_db);
                Collection<Vertex> newVisitedNodes = new ArrayList<>();
                verticesHolder.createComponentsForStartNodes(startNodes, newVisitedNodes);
                Set<Component> components = verticesHolder.generateComponents(flowState.getEquivalencesHolder());

                Assert.assertEquals(1, components.size());
                FullComponent comp = (FullComponent) components.iterator().next();
                Assert.assertEquals("db", comp.getLabel());

                Map<String, List<AttributeWithRevState>> attributes = comp.getAttributes();
                Assert.assertNull(attributes.get("revState")); // je stable

                LOGGER.info("k1: {}", attributes.get("k1"));
                LOGGER.info("k2: {}", attributes.get("k2"));

                Set<AttributeWithRevState> k1Expected = new HashSet<>();
                k1Expected.add(new AttributeWithRevState(RevisionState.STABLE, "v1"));
                k1Expected.add(new AttributeWithRevState(RevisionState.DELETED, "v2"));
                k1Expected.add(new AttributeWithRevState(RevisionState.NEW, "v3"));
                Assert.assertEquals(k1Expected, new HashSet<>(attributes.get("k1")));

                Set<AttributeWithRevState> k2Expected = new HashSet<>();
                k2Expected.add(new AttributeWithRevState(RevisionState.STABLE, "v1"));
                k2Expected.add(new AttributeWithRevState(RevisionState.NEW, "v2"));
                Assert.assertEquals(k2Expected, new HashSet<>(attributes.get("k2")));

                return null;
            }
        });
    }
}
