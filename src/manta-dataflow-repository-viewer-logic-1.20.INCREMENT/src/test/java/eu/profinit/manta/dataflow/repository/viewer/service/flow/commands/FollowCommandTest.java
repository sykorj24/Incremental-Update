package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.gremlin.java.GremlinPipeline;
import com.tinkerpop.pipes.PipeFunction;
import com.tinkerpop.pipes.branch.LoopPipe;
import com.tinkerpop.pipes.branch.LoopPipe.LoopBundle;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilter;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterType;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.unit.HorizontalFilterImpl;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.unit.ResourceFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowEdge;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Component;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.FlowEdgeViz;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.FullComponent;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.ProxyComponent;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.RevisionState;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ComponentsRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.FollowRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ComponentsResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.FollowResponse;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:mvc-dispatcher-servlet.xml" })
public class FollowCommandTest extends AbstractVizualizeTest {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FollowCommandTest.class);

    @Autowired
    ResourceLoader resourceLoader;

    @Test
    public void testPathStatement() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                Vertex t1c3 = GraphCreation.createNode(transaction, table1, "t1c3", "Column", REVISION_1_000000);
                GraphCreation.createEdge(t1c1, t2c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c1, t1c3, EdgeLabel.DIRECT, REVISION_1_000000);

                @SuppressWarnings("rawtypes")
                Iterator<List> paths = new GremlinPipeline<Vertex, List<Vertex>>(t1c1).as("loopIter")
                        .out(EdgeLabel.DIRECT.t())
                        .loop("loopIter", new PipeFunction<LoopPipe.LoopBundle<Vertex>, Boolean>() {
                            @Override
                            public Boolean compute(LoopBundle<Vertex> bundle) {
                                return bundle.getPath().size() < 4;
                            }
                        }, new PipeFunction<LoopPipe.LoopBundle<Vertex>, Boolean>() {
                            @Override
                            public Boolean compute(LoopBundle<Vertex> bundle) {
                                if (bundle.getObject().getEdges(Direction.OUT, EdgeLabel.DIRECT.t()).iterator()
                                        .hasNext()) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        }).path(new PipeFunction<Vertex, Vertex>() {
                            @Override
                            public Vertex compute(Vertex argument) {
                                return argument;
                            }
                        }).iterator();

                int count = 0;
                while (paths.hasNext()) {
                    paths.next();
                    count++;
                }

                Assert.assertEquals(3, count);

                return null;
            }
        });
    }

    @Test
    public void testConnectToOldVisited() {
        cleanGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradata = GraphCreation.createResource(transaction,
                        getSuperRootHandler().ensureRootExistance(transaction), "Teradata", "Teradata", "Teradata",
                        layer, REVISION_1_000000);
                Vertex table1 = GraphCreation.createNode(transaction, teradata, "table1", "Table", REVISION_1_000000);
                Vertex table2 = GraphCreation.createNode(transaction, teradata, "table2", "Table", REVISION_1_000000);
                Vertex table3 = GraphCreation.createNode(transaction, teradata, "table3", "Table", REVISION_1_000000);

                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                Vertex t1c2 = GraphCreation.createNode(transaction, table1, "t1c2", "Column", REVISION_1_000000);
                Vertex t1c3 = GraphCreation.createNode(transaction, table1, "t1c3", "Column", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t1c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c2, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(t1c1, t1c3, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c3, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);

                FlowStateViewer referenceView = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, referenceView, 8, FlowLevel.BOTTOM, true);

                Assert.assertTrue(referenceView.getReferenceNodeIds().contains(t1c1.getId()));
                Assert.assertTrue(referenceView.getReferenceNodeIds().contains(t1c2.getId()));
                Assert.assertTrue(referenceView.getReferenceNodeIds().contains(t1c3.getId()));
                Assert.assertTrue(referenceView.getReferenceNodeIds().contains(t2c1.getId()));
                Assert.assertTrue(referenceView.getReferenceNodeIds().contains(t3c1.getId()));
                Assert.assertTrue(referenceView.getReferenceNodeIds().contains(table1.getId()));
                Assert.assertTrue(referenceView.getReferenceNodeIds().contains(table2.getId()));
                Assert.assertTrue(referenceView.getReferenceNodeIds().contains(table3.getId()));

                // provést follow na t1c1
                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(getLevelMapProvider(resourceLoader));
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(FlowLevel.BOTTOM);
                FollowResponse response1 = commandFollow.execute(requestFollow1, referenceView, transaction);
                Assert.assertEquals(2, response1.getFlows().size());

                // nyní follow na t1c3
                FollowRequest requestFollow2 = new FollowRequest();
                requestFollow2.setComponentIds(Collections.singletonList((Long) t1c3.getId()));
                requestFollow2.setRadius(1);
                requestFollow2.setLevel(FlowLevel.BOTTOM);
                FollowResponse response2 = commandFollow.execute(requestFollow2, referenceView, transaction);
                Assert.assertEquals(4, response2.getFlows().size());

                Assert.assertTrue(response2.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t1c3, t3c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response2.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t1c1, t1c3, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Assert.assertTrue(response2.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t2c1, t3c1, Direction.OUT, EdgeLabel.FILTER, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response2.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t1c2, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                return null;
            }
        });
    }

    @Test
    public void testConnectVisitedSibling() {
        cleanGraph();
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction,
                        getSuperRootHandler().ensureRootExistance(transaction), "Teradata", "Teradata", "Teradata",
                        layer, REVISION_1_000000);

                Vertex table1 = GraphCreation.createNode(transaction, resource, "table1", "Table", REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);

                Vertex table2 = GraphCreation.createNode(transaction, resource, "table2", "Table", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
                Vertex t2c2 = GraphCreation.createNode(transaction, table2, "t2c2", "Column", REVISION_1_000000);

                Vertex table3 = GraphCreation.createNode(transaction, resource, "table3", "Table", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);
                Vertex t3c2 = GraphCreation.createNode(transaction, table3, "t3c2", "Column", REVISION_1_000000);
                Vertex t3c3 = GraphCreation.createNode(transaction, table3, "t3c3", "Column", REVISION_1_000000);

                Vertex table4 = GraphCreation.createNode(transaction, resource, "table4", "Table", REVISION_1_000000);
                Vertex t4c1 = GraphCreation.createNode(transaction, table4, "t4c1", "Column", REVISION_1_000000);
                Vertex t4c2 = GraphCreation.createNode(transaction, table4, "t4c2", "Column", REVISION_1_000000);

                Vertex table5 = GraphCreation.createNode(transaction, resource, "table5", "Table", REVISION_1_000000);
                Vertex t5c1 = GraphCreation.createNode(transaction, table5, "t5c1", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);

                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c2, t3c2, EdgeLabel.DIRECT, REVISION_1_000000);

                GraphCreation.createEdge(t4c1, t1c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t4c2, t1c1, EdgeLabel.DIRECT, REVISION_1_000000);

                GraphCreation.createEdge(t3c1, t4c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c2, t4c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, t5c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t5c1, t3c3, EdgeLabel.DIRECT, REVISION_1_000000);

                FlowStateViewer referenceView = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, referenceView, 14, FlowLevel.BOTTOM, true);

                // provést follow na t1c1
                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(getLevelMapProvider(resourceLoader));
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(FlowLevel.BOTTOM);
                FollowResponse response1 = commandFollow.execute(requestFollow1, referenceView, transaction);
                Assert.assertEquals(7, response1.getFlows().size());

                Assert.assertTrue(response1.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response1.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t4c1, t1c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response1.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t4c2, t1c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Assert.assertTrue(response1.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t2c1, t3c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response1.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t3c1, t4c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response1.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t2c2, t3c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response1.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t3c2, t4c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                // nyní follow na t2c1
                FollowRequest requestFollow2 = new FollowRequest();
                requestFollow2.setComponentIds(Collections.singletonList((Long) t2c1.getId()));
                requestFollow2.setRadius(1);
                requestFollow2.setLevel(FlowLevel.BOTTOM);
                FollowResponse response2 = commandFollow.execute(requestFollow2, referenceView, transaction);
                Assert.assertEquals(7, response2.getFlows().size());

                Assert.assertTrue(response2.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response2.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t2c1, t3c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response2.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t2c2, t3c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Assert.assertTrue(response2.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t3c1, t4c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response2.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t3c2, t4c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Assert.assertTrue(response2.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t3c1, t5c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response2.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t5c1, t3c3, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                return null;
            }
        });
    }

    @Test
    public void testCycles() {
        cleanGraph();
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction,
                        getSuperRootHandler().ensureRootExistance(transaction), "Teradata", "Teradata", "Teradata",
                        layer, REVISION_1_000000);

                Vertex table1 = GraphCreation.createNode(transaction, resource, "table1", "Table", REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);

                Vertex table2 = GraphCreation.createNode(transaction, resource, "table2", "Table", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t3c2", "Column", REVISION_1_000000);

                Vertex table3 = GraphCreation.createNode(transaction, resource, "table3", "Table", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);

                Vertex table4 = GraphCreation.createNode(transaction, resource, "table4", "Table", REVISION_1_000000);
                Vertex t4c1 = GraphCreation.createNode(transaction, table4, "t4c1", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, t4c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t4c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);

                FlowStateViewer referenceView = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, referenceView, 8, FlowLevel.BOTTOM, true);

                // provést follow na t1c1
                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(FlowLevel.BOTTOM);
                FollowResponse response1 = commandFollow.execute(requestFollow1, referenceView, transaction);
                Assert.assertEquals(1, response1.getFlows().size());

                Assert.assertTrue(response1.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                return null;
            }
        });
    }

    @Test
    public void testFlowLevel() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 7, FlowLevel.MIDDLE, false);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(flowState.getFlowLevel());
                FollowResponse response1 = commandFollow.execute(requestFollow1, flowState, transaction);

                Collection<Component> components = response1.getComponents();
                LOGGER.info("{}", components);
                Assert.assertEquals(7, components.size());
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c2, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2c2, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table1,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table2,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, db, flowState,
                        levelMapProvider, getRelationshipService())));

                return null;
            }
        });
    }

    @Test
    public void testFinishingEdgeVertices() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 7, FlowLevel.MIDDLE, false);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow1.setRadius(5);
                requestFollow1.setLevel(flowState.getFlowLevel());
                FollowResponse response1 = commandFollow.execute(requestFollow1, flowState, transaction);

                Assert.assertEquals(3, response1.getFlows().size());
                Assert.assertEquals(7, response1.getComponents().size());

                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(t1c1));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(t1c2));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(t2c1));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(t2c2));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(table1));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(table2));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(db));

                return null;
            }
        });
    }

    @Test
    public void testFilteringResources() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "TeraType",
                        "Description", layer, REVISION_1_000000);
                Vertex t1 = GraphCreation.createNode(transaction, teradataRes, "t1", "Column", REVISION_1_000000);
                Vertex t2 = GraphCreation.createNode(transaction, teradataRes, "t2", "Column", REVISION_1_000000);

                Vertex oracleRes = GraphCreation.createResource(transaction, root, "Oracle", "OracleType",
                        "OraDescription", layer, REVISION_1_000000);
                Vertex o1 = GraphCreation.createNode(transaction, oracleRes, "o1", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1, o1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(o1, t2, EdgeLabel.DIRECT, REVISION_1_000000);

                return null;
            }
        });

        getHorizontalFilterProvider().refreshFilters();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                Vertex t1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1").iterator().next();
                Vertex t2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2").iterator().next();
                Vertex o1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "o1").iterator().next();

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1, levelMapProvider, flowState, 3, FlowLevel.BOTTOM, false);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(flowState.getFlowLevel());
                requestFollow1.setActiveFilters(Collections.singleton("Oracle"));
                FollowResponse response1 = commandFollow.execute(requestFollow1, flowState, transaction);

                Set<FlowEdgeViz> flows = response1.getFlows();
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t1, o1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(o1, t2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Collection<Component> components = response1.getComponents();
                Assert.assertEquals(3, components.size());
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t2, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(o1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(t1));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(t2));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(o1));

                return null;
            }
        });
    }

    @Test
    public void testProxyParents() {
        cleanGraph();
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction,
                        getSuperRootHandler().ensureRootExistance(transaction), "Teradata", "Teradata", "Teradata",
                        layer, REVISION_1_000000);

                Vertex db1 = GraphCreation.createNode(transaction, resource, "db1", "Database", REVISION_1_000000);
                Vertex table1 = GraphCreation.createNode(transaction, db1, "table1", "Table", REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                Vertex t1c2 = GraphCreation.createNode(transaction, table1, "t1c2", "Column", REVISION_1_000000);

                Vertex db2 = GraphCreation.createNode(transaction, resource, "db2", "Database", REVISION_1_000000);
                Vertex table2 = GraphCreation.createNode(transaction, db2, "table2", "Table", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t1c2, EdgeLabel.DIRECT, REVISION_1_000000);

                FlowStateViewer referenceView = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, referenceView, 7, FlowLevel.BOTTOM, true);

                // provést follow na t1c1
                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow1.setRadius(0);
                requestFollow1.setLevel(FlowLevel.BOTTOM);
                FollowResponse response1 = commandFollow.execute(requestFollow1, referenceView, transaction);
                Assert.assertEquals(2, response1.getFlows().size());

                Assert.assertTrue(response1.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response1.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t2c1, t1c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                for (Component c : response1.getComponents()) {
                    if (c.getId().equals(db1.getId())) {
                        Assert.assertTrue(c instanceof FullComponent);
                        Assert.assertTrue(((FullComponent) c).isExpanded());
                    } else if (c.getId().equals(db2.getId())) {
                        //  Assert.assertTrue(c instanceof ProxyComponent);
                    } else if (c.getId().equals(table1.getId())) {
                        Assert.assertTrue(c instanceof FullComponent);
                        Assert.assertTrue(((FullComponent) c).isExpanded());
                    } else if (c.getId().equals(table2.getId())) {
                        Assert.assertTrue(c instanceof ProxyComponent);
                    } else if (c.getId().equals(t2c1.getId())) {
                        Assert.assertTrue(c instanceof ProxyComponent);
                    }
                }

                return null;
            }
        });
    }

    @Test
    public void testProcessParentsMixed() {
        cleanGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata", "Teradata", "t",
                        layer, REVISION_1_000000);

                Vertex db = GraphCreation.createNode(transaction, resource, "db", "Database", REVISION_1_000000);
                Vertex table1 = GraphCreation.createNode(transaction, db, "table1", "Table", REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);

                Vertex table3 = GraphCreation.createNode(transaction, table1, "table3", "Table", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);
                GraphCreation.createEdge(t1c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);

                Vertex table4 = GraphCreation.createNode(transaction, table1, "table4", "Table", REVISION_1_000000);
                Vertex t4c1 = GraphCreation.createNode(transaction, table4, "t4c1", "Column", REVISION_1_000000);
                GraphCreation.createEdge(t3c1, t4c1, EdgeLabel.DIRECT, REVISION_1_000000);

                Vertex table5 = GraphCreation.createNode(transaction, db, "table5", "Table", REVISION_1_000000);
                Vertex t5c1 = GraphCreation.createNode(transaction, table5, "t5c1", "Column", REVISION_1_000000);
                GraphCreation.createEdge(t4c1, t5c1, EdgeLabel.DIRECT, REVISION_1_000000);

                Vertex table6 = GraphCreation.createNode(transaction, db, "table6", "Table", REVISION_1_000000);
                Vertex t6c1 = GraphCreation.createNode(transaction, table6, "t6c1", "Column", REVISION_1_000000);
                GraphCreation.createEdge(t5c1, t6c1, EdgeLabel.DIRECT, REVISION_1_000000);

                GraphCreation.createEdge(t6c1, t1c1, EdgeLabel.DIRECT, REVISION_1_000000);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());

                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 11, FlowLevel.MIDDLE, true);

                // provést follow na t1c1
                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(FlowLevel.MIDDLE);
                FollowResponse response1 = commandFollow.execute(requestFollow1, flowState, transaction);

                Set<FlowEdgeViz> flows = response1.getFlows();
                Assert.assertEquals(5, flows.size());

                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t1c1, t3c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t6c1, t1c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t3c1, t4c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t4c1, t5c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t5c1, t6c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Collection<Component> components = response1.getComponents();
                Assert.assertEquals(11, components.size());

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, db, flowState,
                        levelMapProvider, getRelationshipService())));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table1,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table3,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(table4, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(table5, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table6,
                        flowState, levelMapProvider, getRelationshipService())));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t3c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t4c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t5c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t6c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                for (Component c : components) {
                    if (c.getId().equals(db.getId())) {
                        Assert.assertTrue(((FullComponent) c).isExpanded());
                    } else if (c.getId().equals(table1.getId())) {
                        Assert.assertTrue(((FullComponent) c).isExpanded());
                    } else if (c.getId().equals(table3.getId())) {
                        Assert.assertFalse(((FullComponent) c).isExpanded());
                    }
                }
                return null;
            }
        });
    }

    @Test
    public void testFilteredStartingNode() {
        cleanGraph();

        HorizontalFilterImpl filter = new HorizontalFilterImpl();
        filter.setId("customFilter");
        filter.setName("Custom Filter");
        filter.setOrder(0);
        filter.setType(HorizontalFilterType.CUSTOM);
        ResourceFilterUnit filterUnit = new ResourceFilterUnit(Collections.singleton("Oracle"));
        filter.setFilterUnits(Collections.<HorizontalFilterUnit> singletonList(filterUnit));

        getHorizontalFilterProvider().addFilters(Collections.<HorizontalFilter> singleton(filter));

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "TeraType",
                        "Description", layer, REVISION_1_000000);
                Vertex oracleRes = GraphCreation.createResource(transaction, root, "Oracle", "OracleType",
                        "OraDescription", layer, REVISION_1_000000);

                Vertex t1 = GraphCreation.createNode(transaction, teradataRes, "t1", "Column", REVISION_1_000000);
                Vertex t2 = GraphCreation.createNode(transaction, teradataRes, "t2", "Column", REVISION_1_000000);

                Vertex ot1 = GraphCreation.createNode(transaction, oracleRes, "ot1", "Table", REVISION_1_000000);
                Vertex o1 = GraphCreation.createNode(transaction, ot1, "o1", "Column", REVISION_1_000000);

                Vertex ot2 = GraphCreation.createNode(transaction, oracleRes, "ot2", "Table", REVISION_1_000000);
                Vertex o2 = GraphCreation.createNode(transaction, ot2, "o2", "Column", REVISION_1_000000);

                GraphCreation.createEdge(o1, o2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(o2, t1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1, t2, EdgeLabel.DIRECT, REVISION_1_000000);
                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, o1, levelMapProvider, flowState, 6, FlowLevel.BOTTOM, false);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) o1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(flowState.getFlowLevel());
                requestFollow1.setActiveFilters(Collections.singleton("customFilter"));
                FollowResponse response1 = commandFollow.execute(requestFollow1, flowState, transaction);

                Set<FlowEdgeViz> flows = response1.getFlows();
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(o1, o2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(o2, t1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Collection<Component> components = response1.getComponents();
                Assert.assertEquals(5, components.size());

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, ot1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(ot2, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, o1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(o2, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1, flowState,
                        levelMapProvider, getRelationshipService())));

                for (Component c : components) {
                    if (c.getId().equals(ot1.getId())) {
                        Assert.assertFalse(c.isFiltered());
                    } else if (c.getId().equals(ot2.getId())) {
                        Assert.assertTrue(c.isFiltered());
                    } else if (c.getId().equals(o1.getId())) {
                        Assert.assertFalse(c.isFiltered());
                    } else if (c.getId().equals(o2.getId())) {
                        Assert.assertTrue(c.isFiltered());
                    }
                }

                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(o1));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(o2));
                Assert.assertEquals(NodeStatus.VISITED, flowState.getNodeStatus(t1));

                return null;
            }
        });
    }

    @Test
    public void testTooLargeResponse() {
        cleanGraph();
        createBasicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 7, FlowLevel.BOTTOM, false);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeCheckerAlwaysLarge());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(flowState.getFlowLevel());
                FollowResponse response1 = commandFollow.execute(requestFollow1, flowState, transaction);

                Assert.assertEquals(0, response1.getComponents().size());
                Assert.assertEquals(0, response1.getFlows().size());
                Assert.assertFalse(response1.getDataInfo().isDataOk());

                return null;
            }
        });
    }

    @Test
    public void testFollowNotExistTop() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "TeraType",
                        "Description", layer, REVISION_1_000000);
                Vertex t1 = GraphCreation.createNode(transaction, teradataRes, "t1", "Table", REVISION_1_000000);
                Vertex t2 = GraphCreation.createNode(transaction, teradataRes, "t2", "Table", REVISION_1_000000);

                Vertex t1c1 = GraphCreation.createNode(transaction, t1, "t1", "Column", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, t2, "t2", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 4, FlowLevel.TOP, false);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(flowState.getFlowLevel());
                FollowResponse response1 = commandFollow.execute(requestFollow1, flowState, transaction);

                Set<FlowEdgeViz> childEdges = new HashSet<>();
                childEdges.add(
                        new FlowEdgeViz(new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000));

                Collection<Component> components = response1.getComponents();
                Assert.assertEquals(4, components.size());

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t2, flowState,
                        levelMapProvider, getRelationshipService())));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                return null;
            }
        });
    }

    //@Test
    public void testFollowSiblingsOnAggEdges() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "TeraType",
                        "Description", layer, REVISION_1_000000);
                Vertex table1 = GraphCreation.createNode(transaction, teradataRes, "table1", "Table", REVISION_1_000000);
                Vertex table2 = GraphCreation.createNode(transaction, teradataRes, "table2", "Table", REVISION_1_000000);
                Vertex table3 = GraphCreation.createNode(transaction, teradataRes, "table3", "Table", REVISION_1_000000);
                Vertex table4 = GraphCreation.createNode(transaction, teradataRes, "table4", "Table", REVISION_1_000000);

                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
                Vertex t2c2 = GraphCreation.createNode(transaction, table2, "t2c2", "Column", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);
                Vertex t3c2 = GraphCreation.createNode(transaction, table3, "t3c2", "Column", REVISION_1_000000);
                Vertex t4c1 = GraphCreation.createNode(transaction, table4, "t4c1", "Column", REVISION_1_000000);
                Vertex t4c2 = GraphCreation.createNode(transaction, table4, "t4c2", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c1, t2c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, t4c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t4c1, t2c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, t3c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c2, t4c2, EdgeLabel.DIRECT, REVISION_1_000000);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 11, FlowLevel.BOTTOM, false);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(flowState.getFlowLevel());
                FollowResponse response1 = commandFollow.execute(requestFollow1, flowState, transaction);

                Collection<Component> components = response1.getComponents();
                Assert.assertEquals(11, components.size());
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table1,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table2,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(table3, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(table4, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t2c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t2c2, flowState,
                        levelMapProvider, getRelationshipService())));

                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t3c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t3c2, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t4c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t4c2, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                Set<FlowEdgeViz> flows = response1.getFlows();
                Assert.assertEquals(3, flows.size());
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t2c1, t3c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t3c1, t4c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t4c1, t2c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                return null;
            }
        });
    }

    @Test
    public void testAggCycle() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "TeraType",
                        "Description", layer, REVISION_1_000000);
                Vertex table1 = GraphCreation.createNode(transaction, teradataRes, "table1", "Table", REVISION_1_000000);
                Vertex table2 = GraphCreation.createNode(transaction, teradataRes, "table2", "Table", REVISION_1_000000);
                Vertex table3 = GraphCreation.createNode(transaction, teradataRes, "table3", "Table", REVISION_1_000000);
                Vertex table4 = GraphCreation.createNode(transaction, teradataRes, "table4", "Table", REVISION_1_000000);

                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);
                Vertex t4c1 = GraphCreation.createNode(transaction, table4, "t4c1", "Column", REVISION_1_000000);

                Vertex t1c2 = GraphCreation.createNode(transaction, table1, "t1c2", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, t4c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t4c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);

                GraphCreation.createEdge(t2c1, t1c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c2, t1c1, EdgeLabel.DIRECT, REVISION_1_000000);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 9, FlowLevel.BOTTOM, false);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(flowState.getFlowLevel());
                FollowResponse response1 = commandFollow.execute(requestFollow1, flowState, transaction);

                Set<FlowEdgeViz> flows = response1.getFlows();
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t2c1, t3c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t3c1, t4c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t4c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                return null;
            }
        });
    }

    @Test
    public void testFollowCycle() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "TeraType",
                        "Description", layer, REVISION_1_000000);
                Vertex oracleRes = GraphCreation.createResource(transaction, root, "Oracle", "OracleType",
                        "OraDescription", layer, REVISION_1_000000);

                Vertex table1 = GraphCreation.createNode(transaction, teradataRes, "table1", "Table", REVISION_1_000000);
                Vertex table2 = GraphCreation.createNode(transaction, oracleRes, "table2", "Table", REVISION_1_000000);
                Vertex table3 = GraphCreation.createNode(transaction, oracleRes, "table3", "Table", REVISION_1_000000);

                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, t1c1, EdgeLabel.DIRECT, REVISION_1_000000);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 6, FlowLevel.BOTTOM, false);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(flowState.getFlowLevel());
                requestFollow1.setActiveFilters(Collections.singleton("Oracle"));
                ;
                FollowResponse response1 = commandFollow.execute(requestFollow1, flowState, transaction);

                Set<FlowEdgeViz> flows = response1.getFlows();
                Assert.assertEquals(3, flows.size());
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t2c1, t3c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t3c1, t1c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                return null;
            }
        });
    }

    @Test
    public void testFollow0Siblings() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                GraphCreation.createResource(transaction, root, "Teradata", "TeraType", "Description", layer, REVISION_1_000000);
                GraphCreation.createResource(transaction, root, "Oracle", "OracleType", "OraDescription", layer, REVISION_1_000000);
                return null;
            }
        });

        getHorizontalFilterProvider().refreshFilters();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex teradataRes = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator()
                        .next();
                Vertex oracleRes = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Oracle").iterator().next();

                Vertex table1 = GraphCreation.createNode(transaction, teradataRes, "table1", "Table", REVISION_1_000000);
                Vertex table2 = GraphCreation.createNode(transaction, oracleRes, "table2", "Table", REVISION_1_000000);
                Vertex table3 = GraphCreation.createNode(transaction, teradataRes, "table3", "Table", REVISION_1_000000);

                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                Vertex t1c2 = GraphCreation.createNode(transaction, table1, "t1c2", "Column", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
                Vertex t2c2 = GraphCreation.createNode(transaction, table2, "t2c2", "Column", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);
                Vertex t3c2 = GraphCreation.createNode(transaction, table3, "t3c2", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c1, t1c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c2, t2c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c2, t3c2, EdgeLabel.DIRECT, REVISION_1_000000);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 9, FlowLevel.BOTTOM, false);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow = new FollowRequest();
                requestFollow.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow.setRadius(1);
                requestFollow.setLevel(flowState.getFlowLevel());
                requestFollow.setActiveFilters(Collections.singleton("Oracle"));

                FollowResponse response = commandFollow.execute(requestFollow, flowState, transaction);

                Assert.assertEquals(9, response.getComponents().size());
                for (Component c : response.getComponents()) {
                    if (c.getId().equals(t2c1.getId())) {
                        Assert.assertTrue(c instanceof ProxyComponent);
                    } else if (c.getId().equals(t2c2.getId())) {
                        Assert.assertTrue(c instanceof ProxyComponent);
                    }
                }
                Assert.assertEquals(5, response.getFlows().size());

                requestFollow.setComponentIds(Collections.singletonList((Long) t2c1.getId()));
                requestFollow.setActiveFilters(Collections.<String> emptySet());
                FlowStateViewer flowStateBackup = new FlowStateViewer(flowState);
                response = commandFollow.execute(requestFollow, flowState, transaction);

                Assert.assertEquals(9, response.getComponents().size());
                for (Component c : response.getComponents()) {
                    if (c.getId().equals(t2c1.getId())) {
                        Assert.assertTrue(c instanceof FullComponent);
                    } else if (c.getId().equals(t2c2.getId())) {
                        Assert.assertTrue(c instanceof FullComponent);
                    }
                }
                Assert.assertEquals(4, response.getFlows().size());

                requestFollow.setComponentIds(Collections.singletonList((Long) t2c2.getId()));
                response = commandFollow.execute(requestFollow, flowStateBackup, transaction);
                Assert.assertEquals(9, response.getComponents().size());
                for (Component c : response.getComponents()) {
                    if (c.getId().equals(t2c1.getId())) {
                        Assert.assertTrue(c instanceof FullComponent);
                    } else if (c.getId().equals(t2c2.getId())) {
                        Assert.assertTrue(c instanceof FullComponent);
                    }
                }
                Assert.assertEquals(2, response.getFlows().size());

                return null;
            }
        });
    }

    @Test
    public void testDisabledEachResources() {
        createBasicGraph();

        getHorizontalFilterProvider().refreshFilters();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 7, FlowLevel.BOTTOM, false);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow = new FollowRequest();
                requestFollow.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow.setRadius(1);
                requestFollow.setLevel(flowState.getFlowLevel());
                requestFollow.setActiveFilters(Collections.singleton("Teradata"));
                FollowResponse response = commandFollow.execute(requestFollow, flowState, transaction);

                Assert.assertEquals(7, response.getComponents().size());
                for (Component c : response.getComponents()) {
                    if (c.getId().equals(db.getId())) {
                        Assert.assertTrue(c instanceof FullComponent);
                    } else if (c.getId().equals(table1.getId())) {
                        Assert.assertTrue(c instanceof FullComponent);
                    } else if (c.getId().equals(table2.getId())) {
                        Assert.assertTrue(c instanceof ProxyComponent);
                    } else if (c.getId().equals(t1c1.getId())) {
                        Assert.assertTrue(c instanceof FullComponent);
                    } else if (c.getId().equals(t1c2.getId())) {
                        Assert.assertTrue(c instanceof FullComponent);
                    } else if (c.getId().equals(t2c1.getId())) {
                        Assert.assertTrue(c instanceof ProxyComponent);
                    } else if (c.getId().equals(t2c2.getId())) {
                        Assert.assertTrue(c instanceof ProxyComponent);
                    }
                }
                Assert.assertEquals(3, response.getFlows().size());
                Assert.assertTrue(response.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t1c1, t1c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t1c2, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(response.getFlows()
                        .contains(new FlowEdgeViz(
                                new FlowEdge(t2c1, t2c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                return null;
            }
        });
    }

    @Test
    public void testPersistingFullStatusByComponentsCommand() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "TeraType",
                        "Description", layer, REVISION_1_000000);

                Vertex db1 = GraphCreation.createNode(transaction, teradataRes, "db1", "Database", REVISION_1_000000);
                Vertex db2 = GraphCreation.createNode(transaction, teradataRes, "db2", "Database", REVISION_1_000000);

                Vertex table1 = GraphCreation.createNode(transaction, db1, "table1", "Table", REVISION_1_000000);
                Vertex table2 = GraphCreation.createNode(transaction, db2, "table2", "Table", REVISION_1_000000);
                Vertex table3 = GraphCreation.createNode(transaction, db1, "table3", "Table", REVISION_1_000000);

                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 8, FlowLevel.TOP, false);

                // follow z t1c1

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow = new FollowRequest();
                requestFollow.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow.setRadius(1);
                requestFollow.setLevel(flowState.getFlowLevel());
                FollowResponse followResponse = commandFollow.execute(requestFollow, flowState, transaction);

                Set<FlowEdgeViz> flows = followResponse.getFlows();
                Assert.assertEquals(1, flows.size());
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Collection<Component> components = followResponse.getComponents();
                Assert.assertEquals(6, components.size());
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, db1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, db2, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table1,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(table2, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                // rozbalit db1

                ComponentsCommand componentsCommand = new ComponentsCommand();
                componentsCommand.setLevelMapProvider(levelMapProvider);
                componentsCommand.setRelationshipService(getRelationshipService());
                componentsCommand.setDataSizeChecker(getDataSizeChecker());
                componentsCommand.setComponentFactory(getComponentFactory());
                ComponentsRequest componentsRequest = new ComponentsRequest();
                componentsRequest.setComponentIDs(Collections.singletonList((Long) table1.getId()));
                ComponentsResponse componentsResponse = componentsCommand.execute(componentsRequest, flowState,
                        transaction);
                components = componentsResponse.getComponents();
                Assert.assertEquals(1, components.size());
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table1,
                        flowState, levelMapProvider, getRelationshipService())));

                // follow z t2c1

                requestFollow.setComponentIds(Collections.singletonList((Long) db2.getId()));
                followResponse = commandFollow.execute(requestFollow, flowState, transaction);

                flows = followResponse.getFlows();
                Assert.assertEquals(2, flows.size());
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t2c1, t3c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                components = followResponse.getComponents();
                Assert.assertEquals(8, components.size());
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, db1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, db2, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table1,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(table2, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table3,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t3c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                return null;
            }
        });
    }

    @Test
    public void testPersistingFullStatusByFollowCommand() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "Teradata",
                        "Description", layer, REVISION_1_000000);

                Vertex db1 = GraphCreation.createNode(transaction, teradataRes, "db1", "Database", REVISION_1_000000);
                Vertex db2 = GraphCreation.createNode(transaction, teradataRes, "db2", "Database", REVISION_1_000000);

                Vertex table1 = GraphCreation.createNode(transaction, db1, "table1", "Table", REVISION_1_000000);
                Vertex table2 = GraphCreation.createNode(transaction, db2, "table2", "Table", REVISION_1_000000);
                Vertex table3 = GraphCreation.createNode(transaction, db1, "table3", "Table", REVISION_1_000000);

                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 8, FlowLevel.TOP, false);

                // follow z t1c1

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow = new FollowRequest();
                requestFollow.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow.setRadius(1);
                requestFollow.setLevel(FlowLevel.MIDDLE);
                FollowResponse followResponse = commandFollow.execute(requestFollow, flowState, transaction);

                Set<FlowEdgeViz> flows = followResponse.getFlows();
                Assert.assertEquals(1, flows.size());
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Collection<Component> components = followResponse.getComponents();
                Assert.assertEquals(6, components.size());
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, db1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, db2, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table1,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table2,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                // follow z t2c1

                requestFollow.setComponentIds(Collections.singletonList((Long) t2c1.getId()));
                requestFollow.setLevel(FlowLevel.TOP);
                followResponse = commandFollow.execute(requestFollow, flowState, transaction);

                flows = followResponse.getFlows();
                Assert.assertEquals(2, flows.size());
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t2c1, t3c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                components = followResponse.getComponents();
                Assert.assertEquals(8, components.size());
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, db1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, db2, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table1,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table2,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, table3,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t3c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                return null;
            }
        });
    }

    @Test
    public void testFilteredStartingMiddle() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "Teradata",
                        "Description", layer, REVISION_1_000000);

                Vertex ttable1 = GraphCreation.createNode(transaction, teradataRes, "ttable1", "Table", REVISION_1_000000);

                Vertex t1 = GraphCreation.createNode(transaction, ttable1, "t1", "Column", REVISION_1_000000);
                Vertex t2 = GraphCreation.createNode(transaction, ttable1, "t2", "Column", REVISION_1_000000);

                Vertex oracleRes = GraphCreation.createResource(transaction, root, "Oracle", "OracleType",
                        "OraDescription", layer, REVISION_1_000000);

                Vertex otable1 = GraphCreation.createNode(transaction, oracleRes, "otable1", "Table", REVISION_1_000000);

                Vertex o1 = GraphCreation.createNode(transaction, otable1, "o1", "Column", REVISION_1_000000);
                Vertex o2 = GraphCreation.createNode(transaction, otable1, "o2", "Column", REVISION_1_000000);

                GraphCreation.createEdge(o1, o2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(o2, t1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1, t2, EdgeLabel.DIRECT, REVISION_1_000000);
                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, otable1, levelMapProvider, flowState, 6, FlowLevel.MIDDLE, false);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) otable1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(flowState.getFlowLevel());
                requestFollow1.setActiveFilters(Collections.singleton("Oracle"));
                FollowResponse response1 = commandFollow.execute(requestFollow1, flowState, transaction);

                Set<FlowEdgeViz> flows = response1.getFlows();
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(o1, o2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(o2, t1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Collection<Component> components = response1.getComponents();
                Assert.assertEquals(6, components.size());

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, otable1,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(o1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(o2, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, ttable1,
                        flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(o1));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(o2));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(t1));
                Assert.assertEquals(NodeStatus.FINISHED, flowState.getNodeStatus(t1));

                return null;
            }
        });
    }

    @Test
    public void testFollowTop() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "TeraType",
                        "Description", layer, REVISION_1_000000);

                Vertex dbA = GraphCreation.createNode(transaction, teradataRes, "dbA", "Database", REVISION_1_000000);
                Vertex dbB = GraphCreation.createNode(transaction, teradataRes, "dbB", "Database", REVISION_1_000000);

                Vertex t1 = GraphCreation.createNode(transaction, dbA, "table1", "Table", REVISION_1_000000);
                Vertex t2 = GraphCreation.createNode(transaction, dbA, "table2", "Table", REVISION_1_000000);
                Vertex t3 = GraphCreation.createNode(transaction, dbB, "table3", "Table", REVISION_1_000000);
                Vertex t4 = GraphCreation.createNode(transaction, dbA, "table4", "Table", REVISION_1_000000);
                Vertex t5 = GraphCreation.createNode(transaction, dbA, "table5", "Table", REVISION_1_000000);

                Vertex t1c1 = GraphCreation.createNode(transaction, t1, "t1c1", "Column", REVISION_1_000000);
                Vertex t1c2 = GraphCreation.createNode(transaction, t1, "t1c2", "Column", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, t2, "t2c1", "Column", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, t3, "t3c1", "Column", REVISION_1_000000);
                Vertex t4c1 = GraphCreation.createNode(transaction, t4, "t4c1", "Column", REVISION_1_000000);
                Vertex t5c1 = GraphCreation.createNode(transaction, t5, "t5c1", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t1c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c2, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, t4c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t4c1, t5c1, EdgeLabel.DIRECT, REVISION_1_000000);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 13, FlowLevel.TOP, false);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow = new FollowRequest();
                requestFollow.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow.setRadius(2);
                requestFollow.setLevel(flowState.getFlowLevel());
                FollowResponse response = commandFollow.execute(requestFollow, flowState, transaction);

                Set<FlowEdgeViz> flows = response.getFlows();
                Assert.assertEquals(4, flows.size());
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t1c1, t1c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t1c2, t2c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t2c1, t3c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));
                Assert.assertTrue(flows.contains(
                        new FlowEdgeViz(new FlowEdge(t3c1, t4c1, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000)));

                Collection<Component> components = response.getComponents();
                Assert.assertEquals(11, components.size());

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, dbA, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, dbB, flowState,
                        levelMapProvider, getRelationshipService())));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t2, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t3, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t4, flowState,
                        levelMapProvider, getRelationshipService())));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c2, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t3c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t4c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                return null;
            }
        });
    }

    @Test
    public void testFullRequestedVertices() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                GraphCreation.createResource(transaction, root, "Teradata", "TeraType", "Description", layer, REVISION_1_000000);
                GraphCreation.createResource(transaction, root, "Oracle", "TeraType", "Description", layer, REVISION_1_000000);
                return null;
            }
        });

        getHorizontalFilterProvider().refreshFilters();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex teradataRes = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator()
                        .next();
                Vertex oracleRes = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Oracle").iterator().next();

                Vertex dbA = GraphCreation.createNode(transaction, oracleRes, "dbA", "Database", REVISION_1_000000);
                Vertex dbB = GraphCreation.createNode(transaction, oracleRes, "dbB", "Database", REVISION_1_000000);
                Vertex dbC = GraphCreation.createNode(transaction, teradataRes, "dbC", "Database", REVISION_1_000000);

                Vertex t1 = GraphCreation.createNode(transaction, dbA, "table1", "Table", REVISION_1_000000);
                Vertex t2 = GraphCreation.createNode(transaction, dbB, "table2", "Table", REVISION_1_000000);
                Vertex t3 = GraphCreation.createNode(transaction, dbC, "table3", "Table", REVISION_1_000000);

                Vertex t1c1 = GraphCreation.createNode(transaction, t1, "t1c1", "Column", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, t2, "t2c1", "Column", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, t3, "t3c1", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow = new FollowRequest();
                requestFollow.setRadius(1);
                requestFollow.setActiveFilters(Collections.singleton("Oracle"));

                // varianta 1 - top pohled a startem je middle
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                initRef(transaction, t1, levelMapProvider, flowState, 9, FlowLevel.TOP, false);
                requestFollow.setLevel(flowState.getFlowLevel());
                requestFollow.setComponentIds(Collections.singletonList((Long) t1.getId()));
                FollowResponse response = commandFollow.execute(requestFollow, flowState, transaction);

                Collection<Component> components = response.getComponents();
                Assert.assertEquals(9, components.size());

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, dbA, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(dbB, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, dbC, flowState,
                        levelMapProvider, getRelationshipService())));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t3, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t1c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t3c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));

                // varianta 2 - bottom pohled a startem je middle
                flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                initRef(transaction, t1c1, levelMapProvider, flowState, 9, FlowLevel.BOTTOM, false);
                requestFollow.setLevel(flowState.getFlowLevel());
                requestFollow.setComponentIds(Collections.singletonList((Long) t1.getId()));
                response = commandFollow.execute(requestFollow, flowState, transaction);

                components = response.getComponents();
                Assert.assertEquals(9, components.size());

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, dbA, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(dbB, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, dbC, flowState,
                        levelMapProvider, getRelationshipService())));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t3, flowState,
                        levelMapProvider, getRelationshipService())));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t3c1, flowState,
                        levelMapProvider, getRelationshipService())));

                // varianta 3 - bottom pohled a startem je top
                flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                initRef(transaction, t1c1, levelMapProvider, flowState, 9, FlowLevel.BOTTOM, false);
                requestFollow.setLevel(flowState.getFlowLevel());
                requestFollow.setComponentIds(Collections.singletonList((Long) dbA.getId()));
                response = commandFollow.execute(requestFollow, flowState, transaction);

                components = response.getComponents();
                Assert.assertEquals(9, components.size());

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, dbA, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(dbB, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, dbC, flowState,
                        levelMapProvider, getRelationshipService())));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t3, flowState,
                        levelMapProvider, getRelationshipService())));

                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t1c1, flowState,
                        levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(componentFactory.createProxyComponent(t2c1, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader))));
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction, t3c1, flowState,
                        levelMapProvider, getRelationshipService())));

                return null;
            }
        });
    }

    @Test
    public void testUnfollowedCycle() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "TeraType",
                        "Description", layer, REVISION_1_000000);

                Vertex db = GraphCreation.createNode(transaction, teradataRes, "db", "Database", REVISION_1_000000);

                Vertex t1 = GraphCreation.createNode(transaction, db, "table1", "Table", REVISION_1_000000);
                Vertex t2 = GraphCreation.createNode(transaction, db, "table2", "Table", REVISION_1_000000);
                Vertex t3 = GraphCreation.createNode(transaction, db, "table3", "Table", REVISION_1_000000);
                Vertex t4 = GraphCreation.createNode(transaction, db, "table4", "Table", REVISION_1_000000);
                Vertex t5 = GraphCreation.createNode(transaction, db, "table5", "Table", REVISION_1_000000);

                Vertex t1c1 = GraphCreation.createNode(transaction, t1, "t1c1", "Column", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, t2, "t2c1", "Column", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, t3, "t3c1", "Column", REVISION_1_000000);
                Vertex t4c1 = GraphCreation.createNode(transaction, t4, "t4c1", "Column", REVISION_1_000000);
                Vertex t5c1 = GraphCreation.createNode(transaction, t5, "t5c1", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, t4c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t4c1, t1c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t5c1, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow = new FollowRequest();
                requestFollow.setRadius(3);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                initRef(transaction, t1, levelMapProvider, flowState, 11, FlowLevel.BOTTOM, false);
                requestFollow.setLevel(flowState.getFlowLevel());
                requestFollow.setComponentIds(Collections.singletonList((Long) t1.getId()));
                FollowResponse response = commandFollow.execute(requestFollow, flowState, transaction);

                Collection<Component> components = response.getComponents();
                LOGGER.info("{}", components);
                Assert.assertEquals(11, components.size());

                return null;
            }
        });
    }

    @Test
    public void testCompareGraph() {
        cleanGraph();
        final Map<String, Long> ids = createBasicComapringGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_COMPARE_INTERVAL,
                        getHorizontalFilterProvider());
                Vertex t1c1r3 = transaction.getVertex(ids.get("t1c1r3"));
                initRef(transaction, t1c1r3, levelMapProvider, flowState, 13, FlowLevel.BOTTOM, true);

                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());

                FollowRequest requestFollow = new FollowRequest();
                requestFollow.setRadius(2);
                requestFollow.setLevel(flowState.getFlowLevel());
                requestFollow.setComponentIds(Collections.singletonList(ids.get("t1c1r3")));
                FollowResponse response = commandFollow.execute(requestFollow, flowState, transaction);

                Collection<Component> components = response.getComponents();
                Assert.assertEquals(7, components.size());
                Assert.assertTrue(components.contains(componentFactory.createFullComponent(transaction,
                        transaction.getVertex(ids.get("db")), flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(
                        componentFactory.createFullComponent(transaction, transaction.getVertex(ids.get("table1")),
                                flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(
                        componentFactory.createFullComponent(transaction, transaction.getVertex(ids.get("table2")),
                                flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(
                        componentFactory.createFullComponent(transaction, transaction.getVertex(ids.get("t1c1r3")),
                                flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(
                        componentFactory.createFullComponent(transaction, transaction.getVertex(ids.get("t1c2")),
                                flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(
                        componentFactory.createFullComponent(transaction, transaction.getVertex(ids.get("t2c1r1")),
                                flowState, levelMapProvider, getRelationshipService())));
                Assert.assertTrue(components.contains(
                        componentFactory.createFullComponent(transaction, transaction.getVertex(ids.get("t2c2")),
                                flowState, levelMapProvider, getRelationshipService())));

                Collection<FlowEdgeViz> flows = response.getFlows();
                Assert.assertEquals(4, flows.size());
                Assert.assertTrue(flows.contains(new FlowEdgeViz(ids.get("t1c1r3"), ids.get("t1c2"), EdgeLabel.DIRECT,
                        Collections.<String, Object> emptyMap(), RevisionState.STABLE)));
                Assert.assertTrue(flows.contains(new FlowEdgeViz(ids.get("t1c2"), ids.get("t2c1r1"), EdgeLabel.DIRECT,
                        Collections.<String, Object> emptyMap(), RevisionState.DELETED)));
                Assert.assertTrue(flows.contains(new FlowEdgeViz(ids.get("t2c1r1"), ids.get("t2c2"), EdgeLabel.DIRECT,
                        Collections.<String, Object> emptyMap(), RevisionState.DELETED)));
                Assert.assertTrue(flows.contains(new FlowEdgeViz(ids.get("t1c2"), ids.get("t2c2"), EdgeLabel.DIRECT,
                        Collections.<String, Object> emptyMap(), RevisionState.NEW)));

                return null;
            }
        });
    }

}
