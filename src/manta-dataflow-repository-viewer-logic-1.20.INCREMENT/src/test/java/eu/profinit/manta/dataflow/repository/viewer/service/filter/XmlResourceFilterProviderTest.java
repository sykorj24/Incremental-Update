package eu.profinit.manta.dataflow.repository.viewer.service.filter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.profinit.manta.dataflow.repository.viewer.model.filter.ResourceFilter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:mvc-dispatcher-servlet.xml")
public class XmlResourceFilterProviderTest {
    @Autowired
    ResourceLoader resourceLoader;

    @Test
    public void testGetFiltersModel() {
        XmlResourceFilterProvider provider = new XmlResourceFilterProvider();
        provider.setFiltersFileLocation("horizontalFilters.xml");
        provider.setResourceLoader(resourceLoader);
        provider.init();

        List<Map<String, Object>> model = provider.getFiltersModel();

        Assert.assertEquals(3, model.size());
        Assert.assertEquals("Everything", model.get(0).get("name"));
        Assert.assertEquals("Only DB Objects", model.get(1).get("name"));
        Assert.assertEquals("Db Objects and DDLs", model.get(2).get("name"));
    }

    @Test
    public void testGetFilter() {
        XmlResourceFilterProvider provider = new XmlResourceFilterProvider();
        provider.setFiltersFileLocation("horizontalFilters.xml");
        provider.setResourceLoader(resourceLoader);
        provider.init();

        ResourceFilter filter = provider.getFilter(1);
        Assert.assertEquals("Everything", filter.getName());
        Assert.assertEquals(0, filter.getAllowedResources().size());

        filter = provider.getFilter(2);
        Assert.assertEquals("Only DB Objects", filter.getName());
        Assert.assertEquals(2, filter.getAllowedResources().size());
        Assert.assertTrue(filter.getAllowedResources().contains("Teradata"));
        Assert.assertTrue(filter.getAllowedResources().contains("Oracle"));

        Assert.assertEquals(ResourceFilter.DEFAULT_FILTER, provider.getFilter(123456));
    }

    @Test
    public void testGetAllResourcesModelEverything() {
        XmlResourceFilterProvider provider = new XmlResourceFilterProvider();
        provider.setFiltersFileLocation("horizontalFilters.xml");
        provider.setResourceLoader(resourceLoader);
        provider.init();

        Map<String, Object> resourceInDbMap = new HashMap<String, Object>();
        resourceInDbMap.put("Teradata", 0);
        resourceInDbMap.put("Filesystem", 1);

        ResourceFilter filter = provider.getFilter(1);
        Assert.assertEquals("Everything", filter.getName());

        List<Map<String, Object>> model = filter.getAllResourcesModel(resourceInDbMap);
        Assert.assertEquals(2, model.size());

        Map<String, Object> teraModel = new HashMap<>();
        teraModel.put("id", 0);
        teraModel.put("name", "Teradata");
        teraModel.put("isDisabled", false);

        Assert.assertTrue(model.contains(teraModel));

        Map<String, Object> oraModel = new HashMap<>();
        oraModel.put("id", 1);
        oraModel.put("name", "Filesystem");
        oraModel.put("isDisabled", false);

        Assert.assertTrue(model.contains(oraModel));
    }
    
    @Test
    public void testGetAllResourcesModelOnlyDb() {
        XmlResourceFilterProvider provider = new XmlResourceFilterProvider();
        provider.setFiltersFileLocation("horizontalFilters.xml");
        provider.setResourceLoader(resourceLoader);
        provider.init();

        Map<String, Object> resourceInDbMap = new HashMap<String, Object>();
        resourceInDbMap.put("Teradata", 0);
        resourceInDbMap.put("Filesystem", 1);

        ResourceFilter filter = provider.getFilter(2);
        Assert.assertEquals("Only DB Objects", filter.getName());

        List<Map<String, Object>> model = filter.getAllResourcesModel(resourceInDbMap);
        Assert.assertEquals(2, model.size());

        Map<String, Object> teraModel = new HashMap<>();
        teraModel.put("id", 0);
        teraModel.put("name", "Teradata");
        teraModel.put("isDisabled", false);

        Assert.assertTrue(model.contains(teraModel));

        Map<String, Object> fileModel = new HashMap<>();
        fileModel.put("id", 1);
        fileModel.put("name", "Filesystem");
        fileModel.put("isDisabled", true);

        Assert.assertTrue(model.contains(fileModel));
    }
}
