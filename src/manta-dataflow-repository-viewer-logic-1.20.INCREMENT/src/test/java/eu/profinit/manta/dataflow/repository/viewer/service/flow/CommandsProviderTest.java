package eu.profinit.manta.dataflow.repository.viewer.service.flow;

import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Test;

import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.Command;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.FollowCommand;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.InitReferenceViewCommand;

public class CommandsProviderTest {
    @Test
    public void testGetCommand() {
        CommandsProvider provider = new CommandsProvider();
        Set<Command<?, ?>> commandsSet = new HashSet<>();
        FollowCommand follow = new FollowCommand();
        commandsSet.add(follow);
        provider.setCommandsSet(commandsSet);

        Assert.assertEquals(follow, provider.getCommand(FollowCommand.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetCommandNotContain() {
        CommandsProvider provider = new CommandsProvider();
        Set<Command<?, ?>> commandsSet = new HashSet<>();
        FollowCommand follow = new FollowCommand();
        commandsSet.add(follow);
        provider.setCommandsSet(commandsSet);

        provider.getCommand(InitReferenceViewCommand.class);
    }
}
