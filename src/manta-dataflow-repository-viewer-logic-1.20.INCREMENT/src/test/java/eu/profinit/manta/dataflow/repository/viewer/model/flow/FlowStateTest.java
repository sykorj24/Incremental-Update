package eu.profinit.manta.dataflow.repository.viewer.model.flow;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.AbstractVizualizeTest;
import junit.framework.Assert;

public class FlowStateTest extends AbstractVizualizeTest {
    @Test
    public void testCopyConstructor() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex res = GraphCreation.createResource(transaction, root, "teradata", "type", "desc", layer, REVISION_1_000000);
                Vertex c1 = GraphCreation.createNode(transaction, res, "c1", "Column", REVISION_1_000000);
                Vertex c2 = GraphCreation.createNode(transaction, res, "c2", "Column", REVISION_1_000000);
                Vertex c3 = GraphCreation.createNode(transaction, res, "c3", "Column", REVISION_1_000000);
                Vertex c4 = GraphCreation.createNode(transaction, res, "c4", "Column", REVISION_1_000000);   
                GraphCreation.createEdge(c1, c2, EdgeLabel.DIRECT, REVISION_1_000000);
                
                FlowStateViewer origState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                origState.setFlowLevel(FlowLevel.MIDDLE);
                origState.setFilterEdgesShown(true);

                Set<String> activeFiltres = new HashSet<>();
                activeFiltres.add("r1");
                activeFiltres.add("r2");
                activeFiltres.add("r3");
                origState.setActiveFilters(activeFiltres);
                
                origState.addNode(c1);
                origState.addNode(c2);
                origState.addNode(c3);
                origState.visitNode(c2);
                origState.finishNode(c3);
                
                FlowStateViewer newState = new FlowStateViewer(origState);
                
                origState.setFlowLevel(FlowLevel.BOTTOM);
                origState.setFilterEdgesShown(false);
                activeFiltres.add("r4");
                origState.setActiveFilters(activeFiltres);
                activeFiltres.add("r5");
                origState.addNode(c4);
                origState.finishNode(c2);
                
                
                Assert.assertEquals(FlowLevel.MIDDLE, newState.getFlowLevel());
                Assert.assertTrue(newState.isFilterEdgesShown());
                Assert.assertEquals(3, newState.getActiveFilters().size());
                Assert.assertEquals(3, newState.getReferenceNodeIds().size());
                Assert.assertEquals(NodeStatus.UNVISITED, newState.getNodeStatus(c1));
                Assert.assertEquals(NodeStatus.VISITED, newState.getNodeStatus(c2));
                Assert.assertEquals(NodeStatus.FINISHED, newState.getNodeStatus(c3));
                
                return null;
            }
        });
    }
    
    @Test
    public void testFinishingNeighboursOfFinished() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex res = GraphCreation.createResource(transaction, root, "teradata", "type", "desc", layer, REVISION_1_000000);
                Vertex c1 = GraphCreation.createNode(transaction, res, "c1", "Column", REVISION_1_000000);
                Vertex c2 = GraphCreation.createNode(transaction, res, "c2", "Column", REVISION_1_000000);
                Vertex c3 = GraphCreation.createNode(transaction, res, "c3", "Column", REVISION_1_000000);
                Vertex c4 = GraphCreation.createNode(transaction, res, "c4", "Column", REVISION_1_000000);
                Vertex c5 = GraphCreation.createNode(transaction, res, "c5", "Column", REVISION_1_000000);
                
                GraphCreation.createEdge(c1, c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c2, c3, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c3, c4, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c4, c5, EdgeLabel.DIRECT, REVISION_1_000000);
                
                FlowStateViewer state = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                state.setFlowLevel(FlowLevel.BOTTOM);
                state.setFilterEdgesShown(true);
                
                state.addNode(c1);
                state.addNode(c2);
                state.addNode(c3);
                state.addNode(c4);
                state.addNode(c5);
                
                state.visitNode(c1);
                state.visitNode(c2);
                state.visitNode(c3);
                state.visitNode(c4);
                
                state.finishNode(c1);
                
                Assert.assertEquals(NodeStatus.FINISHED, state.getNodeStatus(c1));
                Assert.assertEquals(NodeStatus.FINISHED, state.getNodeStatus(c2));
                Assert.assertEquals(NodeStatus.FINISHED, state.getNodeStatus(c3));
                Assert.assertEquals(NodeStatus.VISITED, state.getNodeStatus(c4));
                Assert.assertEquals(NodeStatus.UNVISITED, state.getNodeStatus(c5));
                
                return null;
            }
        });
    }
    
    @Test
    public void testFinishingNeighboursOfVisited() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex res = GraphCreation.createResource(transaction, root, "teradata", "type", "desc", layer, REVISION_1_000000);
                Vertex c1 = GraphCreation.createNode(transaction, res, "c1", "Column", REVISION_1_000000);
                Vertex c2 = GraphCreation.createNode(transaction, res, "c2", "Column", REVISION_1_000000);
                Vertex c3 = GraphCreation.createNode(transaction, res, "c3", "Column", REVISION_1_000000);
                Vertex c4 = GraphCreation.createNode(transaction, res, "c4", "Column", REVISION_1_000000);
                Vertex c5 = GraphCreation.createNode(transaction, res, "c5", "Column", REVISION_1_000000);
                
                GraphCreation.createEdge(c1, c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c2, c3, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c2, c4, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c3, c5, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(c4, c5, EdgeLabel.DIRECT, REVISION_1_000000);
                
                FlowStateViewer state = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                state.setFlowLevel(FlowLevel.BOTTOM);
                state.setFilterEdgesShown(true);
                
                state.addNode(c1);
                state.addNode(c2);
                state.addNode(c3);
                state.addNode(c4);
                state.addNode(c5);
                
                state.visitNode(c1);
                state.visitNode(c2);
                state.visitNode(c3);
                state.visitNode(c4);
                
                state.finishNode(c5);
                
                Assert.assertEquals(NodeStatus.FINISHED, state.getNodeStatus(c1));
                Assert.assertEquals(NodeStatus.FINISHED, state.getNodeStatus(c2));
                Assert.assertEquals(NodeStatus.FINISHED, state.getNodeStatus(c3));
                Assert.assertEquals(NodeStatus.FINISHED, state.getNodeStatus(c4));
                Assert.assertEquals(NodeStatus.FINISHED, state.getNodeStatus(c5));
                
                return null;
            }
        });
    }
}
