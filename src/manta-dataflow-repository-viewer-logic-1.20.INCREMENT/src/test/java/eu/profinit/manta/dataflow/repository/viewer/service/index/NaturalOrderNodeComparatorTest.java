package eu.profinit.manta.dataflow.repository.viewer.service.index;

import org.junit.Test;

import junit.framework.Assert;
import eu.profinit.manta.dataflow.repository.viewer.model.index.Node;

public class NaturalOrderNodeComparatorTest {
    
    @Test
    public void testCaseInsensitive() {
        NaturalOrderNodeComparator comparator = new NaturalOrderNodeComparator();
        Node a = new Node();
        a.setText("A123B");
        Node b = new Node();
        b.setText("a123b");
        
        Assert.assertEquals(0, comparator.compare(a, b));
    }
}
