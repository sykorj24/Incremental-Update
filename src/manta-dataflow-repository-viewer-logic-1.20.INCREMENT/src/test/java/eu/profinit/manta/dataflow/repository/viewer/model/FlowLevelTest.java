package eu.profinit.manta.dataflow.repository.viewer.model;

import org.junit.Assert;
import org.junit.Test;

import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;

/**
 * Testy pro enum FlowLevel.
 * @author tfechtner
 *
 */
public class FlowLevelTest {
    /**
     * Test parsování správných jmen na enumy.
     */
    @Test
    public void testParseFromName() {
        Assert.assertEquals(FlowLevel.TOP, FlowLevel.parseFromName("top"));
        Assert.assertEquals(FlowLevel.BOTTOM, FlowLevel.parseFromName("boTTom"));
        Assert.assertEquals(FlowLevel.MIDDLE, FlowLevel.parseFromName("Middle"));
    }
    
    /**
     * Test vyhození výjimky při nexistujícím názvu. 
     */
    @Test(expected = IllegalArgumentException.class) 
    public void testParseFromWrongName() {
        Assert.assertEquals(FlowLevel.TOP, FlowLevel.parseFromName("tops"));
    }
    
    /**
     * Test vyhození výjimky při null. 
     */
    @Test(expected = IllegalArgumentException.class) 
    public void testParseFromNullName() {
        Assert.assertEquals(FlowLevel.TOP, FlowLevel.parseFromName(null));
    }
    
    /**
     * Test porovnatelnosti.
     */
    @Test
    public void testIsHigherThan() {
        Assert.assertTrue(FlowLevel.TOP.isHigherThan(FlowLevel.TOP));
        Assert.assertTrue(FlowLevel.TOP.isHigherThan(FlowLevel.MIDDLE));
        Assert.assertTrue(FlowLevel.TOP.isHigherThan(FlowLevel.BOTTOM));
        
        Assert.assertFalse(FlowLevel.MIDDLE.isHigherThan(FlowLevel.TOP));
        Assert.assertTrue(FlowLevel.MIDDLE.isHigherThan(FlowLevel.MIDDLE));
        Assert.assertTrue(FlowLevel.MIDDLE.isHigherThan(FlowLevel.BOTTOM));
        
        Assert.assertFalse(FlowLevel.BOTTOM.isHigherThan(FlowLevel.TOP));
        Assert.assertFalse(FlowLevel.BOTTOM.isHigherThan(FlowLevel.MIDDLE));
        Assert.assertTrue(FlowLevel.BOTTOM.isHigherThan(FlowLevel.BOTTOM));
        
        Assert.assertTrue(FlowLevel.BOTTOM.isHigherThan(null));
    }
}
