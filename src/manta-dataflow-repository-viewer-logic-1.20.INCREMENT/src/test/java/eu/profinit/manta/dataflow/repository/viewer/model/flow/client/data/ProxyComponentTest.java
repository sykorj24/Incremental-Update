package eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilter;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterType;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.unit.HorizontalFilterImpl;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.unit.ResourceFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.AbstractVizualizeTest;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:mvc-dispatcher-servlet.xml" })
public class ProxyComponentTest extends AbstractVizualizeTest {

    @Autowired
    ResourceLoader resourceLoader;

    @Test
    public void testConstructor() {
        HorizontalFilterImpl filter = new HorizontalFilterImpl();
        filter.setId("customFilter");
        filter.setName("Custom Filter");
        filter.setOrder(0);
        filter.setType(HorizontalFilterType.CUSTOM);
        ResourceFilterUnit filterUnit = new ResourceFilterUnit(Collections.singleton("res2"));
        filter.setFilterUnits(Collections.<HorizontalFilterUnit> singletonList(filterUnit));

        getHorizontalFilterProvider().addFilters(Collections.<HorizontalFilter> singleton(filter));

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                ComponentFactory componentFactory = new ComponentFactory();

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex res1 = GraphCreation.createResource(transaction, root, "res1", "t", "d", layer, REVISION_1_000000);
                Vertex res2 = GraphCreation.createResource(transaction, root, "res2", "t", "d", layer, REVISION_1_000000);

                Vertex table = GraphCreation.createNode(transaction, res1, "table", "Table", REVISION_1_000000);
                Vertex column = GraphCreation.createNode(transaction, table, res2, "column", "Column", REVISION_1_000000);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                flowState.addNode(column);
                flowState.visitNode(column);
                flowState.setActiveFilters(Collections.singleton("customFilter"));

                ProxyComponent component = componentFactory.createProxyComponent(column, flowState,
                        getRelationshipService(), getLevelMapProvider(resourceLoader));

                Assert.assertTrue(component.isProxy());
                Assert.assertEquals(column.getId(), component.getId());
                Assert.assertEquals(table.getId(), component.getParentID());
                Assert.assertEquals(res1.getId(), component.getTopParentTech());
                Assert.assertEquals(NodeStatus.FINISHED, component.getStatus());
                Assert.assertEquals("res2", component.getTech());
                Assert.assertTrue(component.isFiltered());

                component = componentFactory.createProxyComponent(column, flowState, getRelationshipService(),
                        getLevelMapProvider(resourceLoader));

                Assert.assertTrue(component.isProxy());
                Assert.assertEquals(column.getId(), component.getId());
                Assert.assertEquals(table.getId(), component.getParentID());
                Assert.assertEquals(res1.getId(), component.getTopParentTech());
                Assert.assertEquals(NodeStatus.FINISHED, component.getStatus());
                Assert.assertEquals("res2", component.getTech());
                Assert.assertTrue(component.isFiltered());

                return null;
            }
        });
    }
}
