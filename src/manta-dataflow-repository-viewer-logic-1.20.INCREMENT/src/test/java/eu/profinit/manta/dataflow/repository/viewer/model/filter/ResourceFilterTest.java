package eu.profinit.manta.dataflow.repository.viewer.model.filter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Test;

public class ResourceFilterTest {

    @Test
    public void testGetDisabledResource() {
        Map<String, Object> resourceInDbMap = initResourceMap();

        ResourceFilter filter = ResourceFilter.DEFAULT_FILTER;
        Assert.assertEquals(0, filter.getDisabledResource(resourceInDbMap).size());

        Set<String> allowedResources = new HashSet<>();
        allowedResources.add("Teradata");
        allowedResources.add("Oracle");
        filter = new ResourceFilter(0, "DB resources", 0, allowedResources);

        Set<Long> disabledResource = filter.getDisabledResource(resourceInDbMap);
        Assert.assertEquals(1, disabledResource.size());
        Assert.assertTrue(disabledResource.contains(1L));
    }

    @Test
    public void testGetAllResourcesModel() {
        Map<String, Object> resourceInDbMap = initResourceMap();

        Set<String> allowedResources = new HashSet<>();
        allowedResources.add("Teradata");
        allowedResources.add("Oracle");
        ResourceFilter filter = new ResourceFilter(0, "DB resources", 0, allowedResources);

        List<Map<String, Object>> resourceModel = filter.getAllResourcesModel(resourceInDbMap);
        Assert.assertEquals(3, resourceModel.size());

        for (Map<String, Object> m : resourceModel) {
            Long id = (Long) m.get("id");
            if (id == 0L) {
                Assert.assertEquals("Teradata", m.get("name"));
                Assert.assertEquals(false, m.get("isDisabled"));
            } else if (id == 1L) {
                Assert.assertEquals("IFPC", m.get("name"));
                Assert.assertEquals(true, m.get("isDisabled"));
            } else if (id == 2L) {
                Assert.assertEquals("Oracle", m.get("name"));
                Assert.assertEquals(false, m.get("isDisabled"));
            } else {
                Assert.fail();
            }
        }
    }

    private Map<String, Object> initResourceMap() {
        Map<String, Object> resourceInDbMap = new HashMap<String, Object>();
        resourceInDbMap.put("Teradata", 0L);
        resourceInDbMap.put("IFPC", 1L);
        resourceInDbMap.put("Oracle", 2L);
        return resourceInDbMap;
    }
}
