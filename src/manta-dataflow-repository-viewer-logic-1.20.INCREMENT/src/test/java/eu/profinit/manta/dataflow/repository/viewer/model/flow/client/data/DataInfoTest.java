package eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data;

import java.util.Collection;
import java.util.HashSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowEdge;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.service.DataSizeChecker;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.AbstractVizualizeTest;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:mvc-dispatcher-servlet.xml" })
public class DataInfoTest extends AbstractVizualizeTest {

    @Autowired
    ResourceLoader resourceLoader;

    @Test
    public void testBasicConstructor() {
        DataInfo dataInfo = new DataInfo(1, 2, 3, true, "bla");
        Assert.assertEquals(1, dataInfo.getEdges());
        Assert.assertEquals(2, dataInfo.getProxyComponents());
        Assert.assertEquals(3, dataInfo.getFullComponents());
        Assert.assertTrue(dataInfo.isDataOk());
        Assert.assertEquals("bla", dataInfo.getErrorMessage());
    }

    @Test
    public void testConstructorFromCollections() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                ComponentFactory componentFactory = new ComponentFactory();
                
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());

                Collection<Component> components = new HashSet<>();
                components.add(componentFactory.createFullComponent(transaction, t1c1, flowState, levelMapProvider,
                        getRelationshipService()));
                components.add(componentFactory.createFullComponent(transaction, t1c2, flowState, levelMapProvider,
                        getRelationshipService()));
                components.add(componentFactory.createProxyComponent(t2c1, flowState, getRelationshipService(),
                        getLevelMapProvider(resourceLoader)));

                Collection<FlowEdgeViz> edges = new HashSet<>();
                edges.add(
                        new FlowEdgeViz(new FlowEdge(t1c1, t1c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000));
                edges.add(
                        new FlowEdgeViz(new FlowEdge(t1c1, t2c1, Direction.OUT, EdgeLabel.FILTER, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000));
                edges.add(
                        new FlowEdgeViz(new FlowEdge(t2c1, t1c2, Direction.OUT, EdgeLabel.DIRECT, REVISION_INTERVAL_1_000000_1_000000),
                                REVISION_INTERVAL_1_000000_1_000000));

                DataSizeChecker checker = new DataSizeChecker();
                checker.setMaxSize(10000000);

                DataInfo dataInfo = new DataInfo(components, edges, checker);
                Assert.assertEquals(3, dataInfo.getEdges());
                Assert.assertEquals(1, dataInfo.getProxyComponents());
                Assert.assertEquals(2, dataInfo.getFullComponents());
                Assert.assertTrue(dataInfo.isDataOk());
                Assert.assertEquals("", dataInfo.getErrorMessage());

                checker.setMaxSize(0);

                dataInfo = new DataInfo(components, edges, checker);
                Assert.assertEquals(3, dataInfo.getEdges());
                Assert.assertEquals(1, dataInfo.getProxyComponents());
                Assert.assertEquals(2, dataInfo.getFullComponents());
                Assert.assertFalse(dataInfo.isDataOk());
                Assert.assertEquals(DataInfo.ERROR_MESSAGE_TOO_LARGE, dataInfo.getErrorMessage());

                return null;
            }
        });
    }
}
