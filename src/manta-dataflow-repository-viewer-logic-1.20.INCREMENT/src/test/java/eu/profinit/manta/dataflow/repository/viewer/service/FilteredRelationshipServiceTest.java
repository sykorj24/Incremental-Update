package eu.profinit.manta.dataflow.repository.viewer.service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.service.filter.XmlVerticalFilterProvider;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.AbstractVizualizeTest;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:mvc-dispatcher-servlet.xml")
public class FilteredRelationshipServiceTest extends AbstractVizualizeTest {
    @Autowired
    ResourceLoader resourceLoader;

    @Test
    public void testGetParent() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata DDL", "Teradata DDL",
                        "Desc", layer, REVISION_1_000000);
                Vertex schema = GraphCreation.createNode(transaction, resource, "Schema", "Schema", REVISION_1_000000);
                Vertex folder1 = GraphCreation.createNode(transaction, schema, "folder1", "Directory", REVISION_1_000000);
                Vertex folder2 = GraphCreation.createNode(transaction, folder1, "folder2", "Directory", REVISION_1_000000);
                Vertex script = GraphCreation.createNode(transaction, folder2, "script", "Script", REVISION_1_000000);
                Vertex body = GraphCreation.createNode(transaction, script, "body", "Body", REVISION_1_000000);
                Vertex insert = GraphCreation.createNode(transaction, body, "insert", "Insert", REVISION_1_000000);

                XmlVerticalFilterProvider provider = new XmlVerticalFilterProvider();
                provider.setFiltersFileLocation("verticalFilters.xml");
                provider.setResourceLoader(resourceLoader);
                provider.init();

                FilteredRelationshipService service = new FilteredRelationshipService();
                service.setFilterProvider(provider);

                Assert.assertEquals(script, service.getParent(insert));
                Assert.assertEquals(schema, service.getParent(script));
                Assert.assertEquals(schema, service.getParent(folder1));

                Assert.assertNull(service.getParent(schema));

                return null;
            }
        });
    }

    @Test
    public void testGetDirectChildren() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata DDL", "Teradata DDL",
                        "Desc", layer, REVISION_1_000000);
                Vertex schema = GraphCreation.createNode(transaction, resource, "Schema", "Schema", REVISION_1_000000);
                Vertex folder1 = GraphCreation.createNode(transaction, schema, "folder1", "Directory", REVISION_1_000000);
                Vertex folder2 = GraphCreation.createNode(transaction, folder1, "folder2", "Directory", REVISION_1_000000);
                Vertex script1 = GraphCreation.createNode(transaction, folder2, "script1", "Script", REVISION_1_000000);
                Vertex script2 = GraphCreation.createNode(transaction, folder2, "script2", "Script", REVISION_1_000000);
                Vertex script3 = GraphCreation.createNode(transaction, folder1, "script3", "Script", REVISION_1_000000);
                Vertex body = GraphCreation.createNode(transaction, script1, "body", "Body", REVISION_1_000000);
                Vertex insert1 = GraphCreation.createNode(transaction, body, "insert1", "Insert", REVISION_1_000000);
                Vertex insert2 = GraphCreation.createNode(transaction, body, "insert2", "Insert", REVISION_1_000000);

                XmlVerticalFilterProvider provider = new XmlVerticalFilterProvider();
                provider.setFiltersFileLocation("verticalFilters.xml");
                provider.setResourceLoader(resourceLoader);
                provider.init();

                FilteredRelationshipService service = new FilteredRelationshipService();
                service.setFilterProvider(provider);

                List<Vertex> schemaChildren = service.getDirectChildren(schema, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(3, schemaChildren.size());
                Assert.assertTrue(schemaChildren.contains(script1));
                Assert.assertTrue(schemaChildren.contains(script2));
                Assert.assertTrue(schemaChildren.contains(script3));

                List<Vertex> bodyChildren = service.getDirectChildren(body, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(2, bodyChildren.size());
                Assert.assertTrue(bodyChildren.contains(insert1));
                Assert.assertTrue(bodyChildren.contains(insert2));

                Assert.assertEquals(0, service.getDirectChildren(insert1, REVISION_INTERVAL_1_000000_1_000000).size());

                return null;
            }
        });
    }

    @Test
    public void testGetAllParent() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata DDL", "Teradata DDL",
                        "Desc", layer, REVISION_1_000000);
                Vertex schema = GraphCreation.createNode(transaction, resource, "Schema", "Schema", REVISION_1_000000);
                Vertex folder1 = GraphCreation.createNode(transaction, schema, "folder1", "Directory", REVISION_1_000000);
                Vertex folder2 = GraphCreation.createNode(transaction, folder1, "folder2", "Directory", REVISION_1_000000);
                Vertex script = GraphCreation.createNode(transaction, folder2, "script", "Script", REVISION_1_000000);
                Vertex body = GraphCreation.createNode(transaction, script, "body", "Body", REVISION_1_000000);
                Vertex insert = GraphCreation.createNode(transaction, body, "insert", "Insert", REVISION_1_000000);

                XmlVerticalFilterProvider provider = new XmlVerticalFilterProvider();
                provider.setFiltersFileLocation("verticalFilters.xml");
                provider.setResourceLoader(resourceLoader);
                provider.init();

                FilteredRelationshipService service = new FilteredRelationshipService();
                service.setFilterProvider(provider);

                List<Vertex> parents = service.getAllParents(insert);
                Assert.assertEquals(2, parents.size());
                Assert.assertTrue(parents.contains(script));
                Assert.assertTrue(parents.contains(schema));

                Assert.assertEquals(0, service.getAllParents(schema).size());

                return null;
            }
        });
    }

    @Test
    public void testIsList() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata DDL", "Teradata DDL",
                        "Desc", layer, REVISION_1_000000);
                Vertex schema = GraphCreation.createNode(transaction, resource, "Schema", "Schema", REVISION_1_000000);
                Vertex folder1 = GraphCreation.createNode(transaction, schema, "folder1", "Directory", REVISION_1_000000);
                Vertex folder2 = GraphCreation.createNode(transaction, folder1, "folder2", "Directory", REVISION_1_000000);
                Vertex script = GraphCreation.createNode(transaction, folder2, "script", "Script", REVISION_1_000000);
                Vertex script2 = GraphCreation.createNode(transaction, folder2, "script2", "Script", REVISION_1_000000);
                Vertex body = GraphCreation.createNode(transaction, script, "body", "Body", REVISION_1_000000);
                Vertex body2 = GraphCreation.createNode(transaction, script2, "body2", "Body", REVISION_1_000000);
                Vertex insert = GraphCreation.createNode(transaction, body, "insert", "Insert", REVISION_1_000000);

                XmlVerticalFilterProvider provider = new XmlVerticalFilterProvider();
                provider.setFiltersFileLocation("verticalFilters.xml");
                provider.setResourceLoader(resourceLoader);
                provider.init();

                FilteredRelationshipService service = new FilteredRelationshipService();
                service.setFilterProvider(provider);

                Assert.assertTrue(service.isList(insert, REVISION_INTERVAL_1_000000_1_000000));
                Assert.assertTrue(service.isList(script2, REVISION_INTERVAL_1_000000_1_000000));
                Assert.assertTrue(service.isList(body2, REVISION_INTERVAL_1_000000_1_000000));
                Assert.assertFalse(service.isList(resource, REVISION_INTERVAL_1_000000_1_000000));
                Assert.assertFalse(service.isList(script, REVISION_INTERVAL_1_000000_1_000000));

                return null;
            }
        });
    }

    @Test
    public void testIsAtomic() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata", "Teradata scripts",
                        "Desc", layer, REVISION_1_000000);
                Vertex schema = GraphCreation.createNode(transaction, resource, "Schema", "Schema", REVISION_1_000000);
                Vertex folder1 = GraphCreation.createNode(transaction, schema, "folder1", "Directory", REVISION_1_000000);
                Vertex folder2 = GraphCreation.createNode(transaction, folder1, "folder2", "Directory", REVISION_1_000000);
                Vertex script = GraphCreation.createNode(transaction, folder2, "script", "Script", REVISION_1_000000);
                Vertex script2 = GraphCreation.createNode(transaction, folder2, "script2", "Script", REVISION_1_000000);
                Vertex body = GraphCreation.createNode(transaction, script, "body", "Body", REVISION_1_000000);
                Vertex body2 = GraphCreation.createNode(transaction, script2, "body2", "Body", REVISION_1_000000);
                Vertex insert = GraphCreation.createNode(transaction, body, "insert", "BTEQ ColumnFlow", REVISION_1_000000);
                Vertex insert2 = GraphCreation.createNode(transaction, body, "insert2", "BTEQ ColumnFlow", REVISION_1_000000);
                GraphCreation.createNode(transaction, insert2, "body3", "Body", REVISION_1_000000);

                XmlVerticalFilterProvider provider = new XmlVerticalFilterProvider();
                provider.setFiltersFileLocation("verticalFilters.xml");
                provider.setResourceLoader(resourceLoader);
                provider.init();

                FilteredRelationshipService service = new FilteredRelationshipService();
                service.setFilterProvider(provider);

                Assert.assertTrue(service.isAtomic(insert, getLevelMapProvider(resourceLoader), REVISION_INTERVAL_1_000000_1_000000));
                Assert.assertFalse(service.isAtomic(insert2, getLevelMapProvider(resourceLoader), REVISION_INTERVAL_1_000000_1_000000));
                Assert.assertFalse(service.isAtomic(body2, getLevelMapProvider(resourceLoader), REVISION_INTERVAL_1_000000_1_000000));
                Assert.assertFalse(service.isAtomic(schema, getLevelMapProvider(resourceLoader), REVISION_INTERVAL_1_000000_1_000000));

                return null;
            }
        });
    }

    @Test
    public void testGetFirstVertexHighEnough() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata", "Teradata", "Desc", layer, REVISION_1_000000);
                Vertex db = GraphCreation.createNode(transaction, resource, "db", "Database", REVISION_1_000000);
                Vertex t1 = GraphCreation.createNode(transaction, db, "t1", "Table", REVISION_1_000000);
                Vertex t2 = GraphCreation.createNode(transaction, t1, "t2", "Table", REVISION_1_000000);
                Vertex c1 = GraphCreation.createNode(transaction, t2, "c1", "Column", REVISION_1_000000);

                Vertex c2 = GraphCreation.createNode(transaction, resource, "c2", "Column", REVISION_1_000000);

                XmlVerticalFilterProvider provider = new XmlVerticalFilterProvider();
                provider.setFiltersFileLocation("verticalFilters.xml");
                provider.setResourceLoader(resourceLoader);
                provider.init();

                FilteredRelationshipService service = new FilteredRelationshipService();
                service.setFilterProvider(provider);

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                Assert.assertEquals(db, service.getFirstVertexHighEnough(c1, FlowLevel.TOP, levelMapProvider));
                Assert.assertEquals(t2, service.getFirstVertexHighEnough(c1, FlowLevel.MIDDLE, levelMapProvider));
                Assert.assertEquals(c1, service.getFirstVertexHighEnough(c1, FlowLevel.BOTTOM, levelMapProvider));

                Assert.assertEquals(db, service.getFirstVertexHighEnough(t2, FlowLevel.TOP, levelMapProvider));
                Assert.assertEquals(db, service.getFirstVertexHighEnough(t1, FlowLevel.TOP, levelMapProvider));
                Assert.assertEquals(t2, service.getFirstVertexHighEnough(t2, FlowLevel.MIDDLE, levelMapProvider));
                Assert.assertEquals(t1, service.getFirstVertexHighEnough(t1, FlowLevel.MIDDLE, levelMapProvider));

                Assert.assertEquals(c2, service.getFirstVertexHighEnough(c2, FlowLevel.TOP, levelMapProvider));

                return null;
            }
        });
    }

    @Test
    public void testFilterStartNodes() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata", "Teradata DDL",
                        "Desc", layer, REVISION_1_000000);
                Vertex schema = GraphCreation.createNode(transaction, resource, "Schema", "Schema", REVISION_1_000000);
                Vertex folder1 = GraphCreation.createNode(transaction, schema, "folder1", "Directory", REVISION_1_000000);
                Vertex folder2 = GraphCreation.createNode(transaction, folder1, "folder2", "Directory", REVISION_1_000000);
                Vertex script = GraphCreation.createNode(transaction, folder2, "script", "Script", REVISION_1_000000);
                Vertex script2 = GraphCreation.createNode(transaction, folder2, "script2", "Script", REVISION_1_000000);
                Vertex body = GraphCreation.createNode(transaction, script, "body", "Body", REVISION_1_000000);
                Vertex body2 = GraphCreation.createNode(transaction, script2, "body2", "Body", REVISION_1_000000);
                Vertex insert = GraphCreation.createNode(transaction, body, "insert", "BTEQ ColumnFlow", REVISION_1_000000);
                Vertex insert2 = GraphCreation.createNode(transaction, body, "insert2", "BTEQ ColumnFlow", REVISION_1_000000);
                GraphCreation.createNode(transaction, insert2, "body3", "Body", REVISION_1_000000);

                XmlVerticalFilterProvider provider = new XmlVerticalFilterProvider();
                provider.setFiltersFileLocation("verticalFilters.xml");
                provider.setResourceLoader(resourceLoader);
                provider.init();

                FilteredRelationshipService service = new FilteredRelationshipService();
                service.setFilterProvider(provider);

                Collection<Vertex> result = service.filterStartNodes(transaction, Collections.singletonList((Long) insert2.getId()), REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(1, result.size());
                Assert.assertTrue(result.contains(insert2));
                
                result = service.filterStartNodes(transaction, Collections.singletonList((Long) folder1.getId()), REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(2, result.size());
                Assert.assertTrue(result.contains(script));
                Assert.assertTrue(result.contains(script2));
                                
                return null;
            }
        });
    }

}
