package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Component;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.FullComponent;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ComponentsRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.FollowRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ComponentsResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.FollowResponse;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:mvc-dispatcher-servlet.xml" })
public class ComponentsCommandTest extends AbstractVizualizeTest {

    @Autowired
    ResourceLoader resourceLoader;

    @Test
    public void testFinishingEdgeVertices() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 7, FlowLevel.BOTTOM, false);

                // teď follow
                FollowCommand commandFollow = new FollowCommand();
                commandFollow.setLevelMapProvider(levelMapProvider);
                commandFollow.setRelationshipService(getRelationshipService());
                commandFollow.setDataSizeChecker(getDataSizeChecker());
                commandFollow.setComponentFactory(getComponentFactory());
                commandFollow.setTechnicalAttributes(getTerchnicalAttributes());
                FollowRequest requestFollow1 = new FollowRequest();
                requestFollow1.setComponentIds(Collections.singletonList((Long) t1c1.getId()));
                requestFollow1.setRadius(1);
                requestFollow1.setLevel(flowState.getFlowLevel());
                FollowResponse responseFollow = commandFollow.execute(requestFollow1, flowState, transaction);

                Assert.assertEquals(1, responseFollow.getFlows().size());
                Assert.assertEquals(4, responseFollow.getComponents().size());

                // a nakonec component command
                ComponentsCommand commandComponents = new ComponentsCommand();
                commandComponents.setLevelMapProvider(levelMapProvider);
                commandComponents.setRelationshipService(getRelationshipService());
                commandComponents.setDataSizeChecker(getDataSizeChecker());
                commandComponents.setComponentFactory(getComponentFactory());
                ComponentsRequest requestComponents = new ComponentsRequest();
                List<Long> components = new ArrayList<>();
                components.add((Long) t1c1.getId());
                components.add((Long) t1c2.getId());
                requestComponents.setComponentIDs(components);
                ComponentsResponse responseComponents = commandComponents.execute(requestComponents, flowState,
                        transaction);

                List<Component> comps = responseComponents.getComponents();
                Assert.assertEquals(2, comps.size());

                FullComponent f0 = (FullComponent) comps.get(0);
                Assert.assertEquals(t1c1.getId(), f0.getId());
                Assert.assertEquals(NodeStatus.FINISHED, f0.getStatus());

                FullComponent f1 = (FullComponent) comps.get(1);
                Assert.assertEquals(t1c2.getId(), f1.getId());
                Assert.assertEquals(NodeStatus.VISITED, f1.getStatus());

                return null;
            }
        });
    }
    
    @Test
    public void testTooLarge() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                LevelMapProvider levelMapProvider = getLevelMapProvider(resourceLoader);

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                // nejprve si připravit referenční view
                initRef(transaction, t1c1, levelMapProvider, flowState, 7, FlowLevel.MIDDLE, false);
                
                // a teď component command
                ComponentsCommand commandComponents = new ComponentsCommand();
                commandComponents.setLevelMapProvider(levelMapProvider);
                commandComponents.setRelationshipService(getRelationshipService());
                commandComponents.setDataSizeChecker(getDataSizeCheckerAlwaysLarge());
                commandComponents.setComponentFactory(getComponentFactory());
                ComponentsRequest requestComponents = new ComponentsRequest();
                List<Long> components = new ArrayList<>();
                components.add((Long) t1c1.getId());
                components.add((Long) t1c2.getId());
                requestComponents.setComponentIDs(components);
                ComponentsResponse responseComponents = commandComponents.execute(requestComponents, flowState,
                        transaction);

                Assert.assertEquals(0, responseComponents.getComponents().size());
                Assert.assertFalse(responseComponents.getDataInfo().isDataOk());
                
                return null;
            }
        });
    }

}
