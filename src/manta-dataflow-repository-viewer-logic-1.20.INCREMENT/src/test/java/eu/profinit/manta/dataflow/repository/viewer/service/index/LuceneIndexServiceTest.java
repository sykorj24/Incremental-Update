package eu.profinit.manta.dataflow.repository.viewer.service.index;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.index.Node;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.AbstractVizualizeTest;
import junit.framework.Assert;

public class LuceneIndexServiceTest extends AbstractVizualizeTest {

    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(LuceneIndexServiceTest.class);

    /*
    public LuceneIndexServiceTest() {
        super("c:/apache-tomcat/webapps/manta-dataflow-server-1.10.3-SNAPSHOT/WEB-INF/db/", false);
    }
    
    @Test
    public void bla() {
        getDatabaseHolder().runInWriteTransaction(new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
    
                LuceneIndexService luceneIndexService = new LuceneIndexService();
                luceneIndexService.setDatabaseService(getDatabaseHolder());
                luceneIndexService.setSuperRootHandler(getSuperRootHandler());
                luceneIndexService.setTechnicalAttributesHolder(getTerchnicalAttributes());
    
                Vertex aa = transaction.getVertices(NodeProperty.NODE_NAME.t(), "JOBS").iterator().next();
                
                Map<Long, Node> nodeMap = transformToMap(luceneIndexService.findItems("JOBS", RevisionRootHandler.EVERY_REVISION_INTERVAL));
                LOGGER.info("{}", nodeMap.size());
                
                return null;
            }
        });
    }
      */
    @Test
    public void testFindItemsColumn() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                LuceneIndexService luceneIndexService = new LuceneIndexService();
                luceneIndexService.setDatabaseService(getDatabaseHolder());
                luceneIndexService.setSuperRootHandler(getSuperRootHandler());

                Map<Long, Node> nodeMap = transformToMap(luceneIndexService.findItems("t1", REVISION_INTERVAL_1_000000_1_000000));
                Assert.assertEquals(2, nodeMap.size());

                Node t1c1Node = nodeMap.get(t1c1.getId());
                Assert.assertEquals("t1c1", t1c1Node.getText());
                Assert.assertEquals("Column", t1c1Node.getType());
                Assert.assertEquals(t1c1.getId(), t1c1Node.getId());
                Assert.assertEquals("Teradata", t1c1Node.getPath().get(0));
                Assert.assertEquals("db", t1c1Node.getPath().get(1));
                Assert.assertEquals("table1", t1c1Node.getPath().get(2));

                Node t1c2Node = nodeMap.get(t1c2.getId());
                Assert.assertEquals("t1c2", t1c2Node.getText());
                Assert.assertEquals("Column", t1c2Node.getType());
                Assert.assertEquals(t1c2.getId(), t1c2Node.getId());
                Assert.assertEquals("Teradata", t1c2Node.getPath().get(0));
                Assert.assertEquals("db", t1c2Node.getPath().get(1));
                Assert.assertEquals("table1", t1c2Node.getPath().get(2));

                return null;
            }
        });
    }

    @Test
    public void testFindItemsSlash() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        createBasicGraph();

        final Object t1cslash = getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex t1cslash = GraphCreation.createNode(transaction, table1, "ab$+<,>a a.c$J/D", "Column",
                        REVISION_INTERVAL_1_000000_1_000000);
                return t1cslash.getId();
            }
        });

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                LuceneIndexService luceneIndexService = new LuceneIndexService();
                luceneIndexService.setDatabaseService(getDatabaseHolder());
                luceneIndexService.setSuperRootHandler(getSuperRootHandler());

                Map<Long, Node> nodeMap = transformToMap(
                        luceneIndexService.findItems("aB$+<,>a a.c$J/d", REVISION_INTERVAL_1_000000_1_000000));
                Assert.assertEquals(1, nodeMap.size());

                nodeMap = transformToMap(luceneIndexService.findItems("B$+<,>a a.c$J/", REVISION_INTERVAL_1_000000_1_000000));
                Assert.assertEquals(1, nodeMap.size());
                Node t1c1Node = nodeMap.get(t1cslash);
                Assert.assertEquals("ab$+<,>a a.c$J/D", t1c1Node.getText());
                Assert.assertEquals("Column", t1c1Node.getType());
                Assert.assertEquals(t1cslash, t1c1Node.getId());
                Assert.assertEquals("Teradata", t1c1Node.getPath().get(0));
                Assert.assertEquals("db", t1c1Node.getPath().get(1));
                Assert.assertEquals("table1", t1c1Node.getPath().get(2));

                return null;
            }
        });
    }

    @Test
    public void testFindItemsColumnDot() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        createBasicGraph();
        Map<String, Vertex> vertices = getDatabaseHolder()
                .runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Map<String, Vertex>>() {
                    @Override
                    public Map<String, Vertex> callMe(TitanTransaction transaction) {
                        Map<String, Vertex> result = new HashMap<>();

                        Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();

                        result.put("t1c3",
                                GraphCreation.createNode(transaction, table1, "ttt_ccc", "Column", REVISION_1_000000));
                        result.put("t1c4",
                                GraphCreation.createNode(transaction, table1, "ttt_ccc4", "Column", REVISION_1_000000));

                        return result;
                    }
                });
        LuceneIndexService luceneIndexService = new LuceneIndexService();
        luceneIndexService.setDatabaseService(getDatabaseHolder());
        luceneIndexService.setSuperRootHandler(getSuperRootHandler());

        Map<Long, Node> nodeMap = transformToMap(luceneIndexService.findItems("ttt_ccc", REVISION_INTERVAL_1_000000_1_000000));
        Assert.assertEquals(2, nodeMap.size());

        Node t1Node = nodeMap.get(vertices.get("t1c3").getId());
        Assert.assertEquals("ttt_ccc", t1Node.getText());
        Assert.assertEquals("Column", t1Node.getType());
        Assert.assertEquals(vertices.get("t1c3").getId(), t1Node.getId());
        Assert.assertEquals("Teradata", t1Node.getPath().get(0));
        Assert.assertEquals("db", t1Node.getPath().get(1));
        Assert.assertEquals("table1", t1Node.getPath().get(2));

        Node t2Node = nodeMap.get(vertices.get("t1c4").getId());
        Assert.assertEquals("ttt_ccc4", t2Node.getText());
        Assert.assertEquals("Column", t2Node.getType());
        Assert.assertEquals(vertices.get("t1c4").getId(), t2Node.getId());
        Assert.assertEquals("Teradata", t2Node.getPath().get(0));
        Assert.assertEquals("db", t2Node.getPath().get(1));
        Assert.assertEquals("table1", t2Node.getPath().get(2));
    }

    @Test
    public void testFindItemsTable() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();

                LuceneIndexService luceneIndexService = new LuceneIndexService();
                luceneIndexService.setDatabaseService(getDatabaseHolder());
                luceneIndexService.setSuperRootHandler(getSuperRootHandler());
                luceneIndexService.setTechnicalAttributesHolder(new DummyTechnicalAttributesHolder());
                Map<Long, Node> nodeMap = transformToMap(luceneIndexService.findItems("able", REVISION_INTERVAL_1_000000_1_000000));

                Node table1Node = nodeMap.get(table1.getId());
                Assert.assertEquals("table1", table1Node.getText());
                Assert.assertEquals("Table", table1Node.getType());
                Assert.assertEquals(table1.getId(), table1Node.getId());
                Assert.assertEquals("Teradata", table1Node.getPath().get(0));
                Assert.assertEquals("db", table1Node.getPath().get(1));

                Node table2Node = nodeMap.get(table2.getId());
                Assert.assertEquals("table2", table2Node.getText());
                Assert.assertEquals("Table", table2Node.getType());
                Assert.assertEquals(table2.getId(), table2Node.getId());
                Assert.assertEquals("Teradata", table2Node.getPath().get(0));
                Assert.assertEquals("db", table2Node.getPath().get(1));

                return null;
            }
        });
    }

    protected Map<Long, Node> transformToMap(List<Node> nodes) {
        Map<Long, Node> resultMap = new HashMap<>();
        for (Node n : nodes) {
            resultMap.put(n.getId(), n);
        }
        return resultMap;
    }

    @Test
    public void testFindItemsCaseInsensitive() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();

                LuceneIndexService luceneIndexService = new LuceneIndexService();
                luceneIndexService.setDatabaseService(getDatabaseHolder());
                luceneIndexService.setSuperRootHandler(getSuperRootHandler());
                luceneIndexService.setTechnicalAttributesHolder(new DummyTechnicalAttributesHolder());

                Map<Long, Node> nodeMap = transformToMap(luceneIndexService.findItems("aBLe", REVISION_INTERVAL_1_000000_1_000000));
                Assert.assertEquals(2, nodeMap.size());

                Node tabe1Node = nodeMap.get(table1.getId());
                Assert.assertEquals("table1", tabe1Node.getText());
                Assert.assertEquals("Table", tabe1Node.getType());
                Assert.assertEquals(table1.getId(), tabe1Node.getId());
                Assert.assertEquals("Teradata", tabe1Node.getPath().get(0));
                Assert.assertEquals("db", tabe1Node.getPath().get(1));

                Node table2Node = nodeMap.get(table2.getId());
                Assert.assertEquals("table2", table2Node.getText());
                Assert.assertEquals("Table", table2Node.getType());
                Assert.assertEquals(table2.getId(), table2Node.getId());
                Assert.assertEquals("Teradata", table2Node.getPath().get(0));
                Assert.assertEquals("db", table2Node.getPath().get(1));

                return null;
            }
        });
    }

    @Test
    public void testFindItemsEqualsPriority() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        createBasicGraph();

        Map<String, Vertex> vertices = getDatabaseHolder()
                .runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Map<String, Vertex>>() {
                    @Override
                    public Map<String, Vertex> callMe(TitanTransaction transaction) {
                        Map<String, Vertex> vertices = new HashMap<>();

                        Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                        vertices.put("table2",
                                transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next());
                        vertices.put("table1",
                                transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next());
                        vertices.put("tAble", GraphCreation.createNode(transaction, db, "tAble", "Table", REVISION_1_000000));

                        return vertices;
                    }
                });

        LuceneIndexService luceneIndexService = new LuceneIndexService();
        luceneIndexService.setDatabaseService(getDatabaseHolder());
        luceneIndexService.setSuperRootHandler(getSuperRootHandler());
        luceneIndexService.setTechnicalAttributesHolder(new DummyTechnicalAttributesHolder());

        Map<Long, Node> nodeMap = transformToMap(luceneIndexService.findItems("tAble", REVISION_INTERVAL_1_000000_1_000000));
        Assert.assertEquals(3, nodeMap.size());

        Assert.assertEquals("tAble", nodeMap.get(vertices.get("tAble").getId()).getText());
        Assert.assertEquals("table2", nodeMap.get(vertices.get("table2").getId()).getText());
        Assert.assertEquals("table1", nodeMap.get(vertices.get("table1").getId()).getText());
    }

    @Test
    public void testFindItemsNothing() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        createBasicGraph();

        LuceneIndexService luceneIndexService = new LuceneIndexService();
        luceneIndexService.setDatabaseService(getDatabaseHolder());
        luceneIndexService.setSuperRootHandler(getSuperRootHandler());

        List<Node> nodes = luceneIndexService.findItems("ASDGdfhsdfx", REVISION_INTERVAL_1_000000_1_000000);
        Assert.assertEquals(0, nodes.size());
    }

    @Test
    public void testGetNodesOnPathRoot() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                LuceneIndexService luceneIndexService = new LuceneIndexService();
                luceneIndexService.setDatabaseService(getDatabaseHolder());
                luceneIndexService.setSuperRootHandler(getSuperRootHandler());

                List<Node> nodes = luceneIndexService.getNodesOnPath(Collections.<Long> emptyList(),
                        REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(1, nodes.size());

                Vertex resVertex = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator()
                        .next();

                Assert.assertEquals("Teradata", nodes.get(0).getText());
                Assert.assertEquals(resVertex.getId(), nodes.get(0).getId());
                Assert.assertEquals(0, nodes.get(0).getPath().size());

                nodes = luceneIndexService.getNodesOnPath(Collections.<Long> emptyList(), REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(1, nodes.size());

                Assert.assertEquals("Teradata", nodes.get(0).getText());
                Assert.assertEquals(resVertex.getId(), nodes.get(0).getId());
                Assert.assertEquals(0, nodes.get(0).getPath().size());

                return null;
            }
        });
    }

    @Test
    public void testGetNodesOnPathTable() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                LuceneIndexService luceneIndexService = new LuceneIndexService();
                luceneIndexService.setDatabaseService(getDatabaseHolder());
                luceneIndexService.setSuperRootHandler(getSuperRootHandler());

                Vertex teradata = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator().next();
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                List<Long> path = new ArrayList<>();
                path.add((Long) teradata.getId());
                path.add((Long) db.getId());
                path.add((Long) table1.getId());
                List<Node> nodes = luceneIndexService.getNodesOnPath(path, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(2, nodes.size());

                Assert.assertEquals("t1c1", nodes.get(0).getText());
                Assert.assertEquals("Column", nodes.get(0).getType());
                Assert.assertEquals(t1c1.getId(), nodes.get(0).getId());
                Assert.assertEquals("Teradata", nodes.get(0).getPath().get(0));
                Assert.assertEquals("db", nodes.get(0).getPath().get(1));
                Assert.assertEquals("table1", nodes.get(0).getPath().get(2));

                Assert.assertEquals("t1c2", nodes.get(1).getText());
                Assert.assertEquals("Column", nodes.get(1).getType());
                Assert.assertEquals(t1c2.getId(), nodes.get(1).getId());
                Assert.assertEquals("Teradata", nodes.get(1).getPath().get(0));
                Assert.assertEquals("db", nodes.get(1).getPath().get(1));
                Assert.assertEquals("table1", nodes.get(1).getPath().get(2));

                return null;
            }
        });
    }

    @Test
    public void testGetNodesOnPathDifferentResource() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex res = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator().next();

                Vertex superRoot = getSuperRootHandler().getRoot(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex res2 = GraphCreation.createResource(transaction, superRoot, "res2", "r2", "rd2", layer, REVISION_1_000000);
                Vertex db2 = GraphCreation.createNode(transaction, res2, "db2", "Database", REVISION_1_000000);
                GraphCreation.createNode(transaction, db2, res, "table3", "Table", REVISION_1_000000);

                return null;
            }
        });
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                LuceneIndexService luceneIndexService = new LuceneIndexService();
                luceneIndexService.setDatabaseService(getDatabaseHolder());
                luceneIndexService.setSuperRootHandler(getSuperRootHandler());

                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex teradata = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator().next();
                List<Long> path = new ArrayList<>();
                path.add((Long) teradata.getId());
                List<Node> nodes = luceneIndexService.getNodesOnPath(path, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(1, nodes.size());

                Assert.assertEquals("db", nodes.get(0).getText());
                Assert.assertEquals("Database", nodes.get(0).getType());
                Assert.assertEquals(db.getId(), nodes.get(0).getId());
                Assert.assertEquals(1, nodes.get(0).getPath().size());
                Assert.assertEquals("Teradata", nodes.get(0).getPath().get(0));

                return null;
            }
        });
    }

    @Test
    public void testGetNodesOnPathNothing() {
        createBasicGraph();

        LuceneIndexService luceneIndexService = new LuceneIndexService();
        luceneIndexService.setDatabaseService(getDatabaseHolder());
        luceneIndexService.setSuperRootHandler(getSuperRootHandler());

        List<Long> path = new ArrayList<>();
        path.add(2L);
        path.add(152L);
        path.add(7892L);
        path.add(232L);
        path.add(4L);
        path.add(8L);
        List<Node> nodes = luceneIndexService.getNodesOnPath(path, REVISION_INTERVAL_1_000000_1_000000);
        Assert.assertEquals(0, nodes.size());
    }

    @Test
    public void testGetNodesOk() {
        createBasicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                LuceneIndexService luceneIndexService = new LuceneIndexService();
                luceneIndexService.setDatabaseService(getDatabaseHolder());
                luceneIndexService.setSuperRootHandler(getSuperRootHandler());

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                List<String> ids = new ArrayList<>();
                ids.add(t1c1.getId().toString());
                ids.add(t1c2.getId().toString());

                List<Node> nodes = luceneIndexService.getNodes(ids, REVISION_INTERVAL_1_000000_1_000000);

                Assert.assertEquals(2, nodes.size());
                Assert.assertEquals("t1c1", nodes.get(0).getText());
                Assert.assertEquals("t1c2", nodes.get(1).getText());

                return null;
            }
        });
    }

    @Test
    public void testGetNodesBad() {
        createBasicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                LuceneIndexService luceneIndexService = new LuceneIndexService();
                luceneIndexService.setDatabaseService(getDatabaseHolder());
                luceneIndexService.setSuperRootHandler(getSuperRootHandler());

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                List<String> ids = new ArrayList<>();
                ids.add(t1c1.getId().toString());
                ids.add(t1c2.getId().toString());
                ids.add("fasasf");
                ids.add("465432");

                List<Node> nodes = luceneIndexService.getNodes(ids, REVISION_INTERVAL_1_000000_1_000000);

                Assert.assertEquals(2, nodes.size());
                Assert.assertEquals("t1c1", nodes.get(0).getText());
                Assert.assertEquals("t1c2", nodes.get(1).getText());

                return null;
            }
        });
    }

    @Test
    public void testFindItemsByPath() {
        createBasicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                GraphCreation.createNode(transaction, db, "table3", "Table", REVISION_1_000000);
                GraphCreation.createNode(transaction, db, "table3", "View", REVISION_1_000000);
                return null;
            }
        });

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                LuceneIndexService luceneIndexService = new LuceneIndexService();
                luceneIndexService.setDatabaseService(getDatabaseHolder());
                luceneIndexService.setSuperRootHandler(getSuperRootHandler());
                luceneIndexService.setTechnicalAttributesHolder(getTerchnicalAttributes());

                List<Node> nodes;

                nodes = luceneIndexService.findItemsByPath(new String[] { "t1c1" }, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(1, nodes.size());
                Assert.assertEquals("t1c1", nodes.get(0).getText());

                nodes = luceneIndexService.findItemsByPath(new String[] { "T1C1" }, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(0, nodes.size());

                nodes = luceneIndexService.findItemsByPath(new String[] { "table1", "t1c1" }, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(1, nodes.size());
                Assert.assertEquals("t1c1", nodes.get(0).getText());

                nodes = luceneIndexService.findItemsByPath(new String[] { "table2", "t1c1" }, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(0, nodes.size());

                nodes = luceneIndexService.findItemsByPath(new String[] { "db", "table1", "t1c1" },
                        REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(1, nodes.size());
                Assert.assertEquals("t1c1", nodes.get(0).getText());

                nodes = luceneIndexService.findItemsByPath(new String[] { "db", "table1" }, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(1, nodes.size());
                Assert.assertEquals("table1", nodes.get(0).getText());

                nodes = luceneIndexService.findItemsByPath(new String[] { "bz" }, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(0, nodes.size());

                nodes = luceneIndexService.findItemsByPath(new String[] { "t1c1" }, REVISION_INTERVAL_2_000000_2_000000);
                Assert.assertEquals(0, nodes.size());

                nodes = luceneIndexService.findItemsByPath(new String[] { "table3" }, REVISION_INTERVAL_1_000000_1_000000);
                Assert.assertEquals(2, nodes.size());

                return null;
            }
        });
    }
}
