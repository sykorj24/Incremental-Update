package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.GenerateLinkResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.index.WelcomeFormModel;
import junit.framework.Assert;

public class GenerateLinkCommandTest extends AbstractVizualizeTest {
    @Test
    public void testGenLink() {

        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex divnoNazev = GraphCreation.createNode(transaction, table1, "t1\" c2", "Column", REVISION_1_000000);

                GenerateLinkCommand command = new GenerateLinkCommand();
                command.setUsageStatsCollector(getUsageStatsCollector());
                WelcomeFormModel request = new WelcomeFormModel();
                request.setDepth(2);
                request.setDirection(Direction.OUT);
                request.setFilter("1");
                request.setFilterEdges(true);
                request.setLevel(FlowLevel.MIDDLE);
                request.setSelectedItems(new String[] { t1c1.getId().toString(), t2c2.getId().toString(),
                        divnoNazev.getId().toString() });
                GenerateLinkResponse response = command.execute(request,
                        new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider()), transaction);

                String expected = "viewer/dataflow/link?depth=2&direction=OUT&filter=1&filterEdges=true&level=MIDDLE&"
                        + "si[0][0][0]=Teradata&si[0][0][1]=Teradata&" + "si[0][1][0]=db&si[0][1][1]=Database&"
                        + "si[0][2][0]=table1&si[0][2][1]=Table&" + "si[0][3][0]=t1c1&si[0][3][1]=Column&"
                        + "si[1][0][0]=Teradata&si[1][0][1]=Teradata&" + "si[1][1][0]=db&si[1][1][1]=Database&"
                        + "si[1][2][0]=table2&si[1][2][1]=Table&" + "si[1][3][0]=t2c2&si[1][3][1]=Column&"
                        + "si[2][0][0]=Teradata&si[2][0][1]=Teradata&" + "si[2][1][0]=db&si[2][1][1]=Database&"
                        + "si[2][2][0]=table1&si[2][2][1]=Table&" + "si[2][3][0]=t1%22%20c2&si[2][3][1]=Column";
                Assert.assertEquals(expected, response.getLink());
                return null;
            }
        });

    }

}
