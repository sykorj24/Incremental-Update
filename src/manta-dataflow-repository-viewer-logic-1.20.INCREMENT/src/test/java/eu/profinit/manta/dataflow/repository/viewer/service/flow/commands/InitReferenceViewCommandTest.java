package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message.Severity;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.InitRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.BaseResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.InitResponse;
import junit.framework.Assert;

public class InitReferenceViewCommandTest extends AbstractVizualizeTest {
    @Test
    public void testCommand() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                Vertex t2c3 = GraphCreation.createNode(transaction, table2, "t2c3", "Column", REVISION_1_000000);
                GraphCreation.createEdge(t2c3, t2c2, EdgeLabel.DIRECT, REVISION_1_000000);

                InitReferenceViewCommand command = new InitReferenceViewCommand();
                command.setAlgorithmExecutor(getAlgorithmExecutor());

                List<Long> referencePoints = new ArrayList<>();
                referencePoints.add((Long) table1.getId());

                InitRequest request = new InitRequest();
                request.setReferencePoints(referencePoints);
                FlowStateViewer referenceView = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                command.setRelationshipService(getRelationshipService());
                InitResponse response = command.execute(request, referenceView, transaction);
                Assert.assertTrue(response.isOk());

                Set<Long> referenceNodes = referenceView.getReferenceNodeIds();
                Assert.assertEquals(7, referenceNodes.size());
                Assert.assertTrue(referenceNodes.contains(t1c1.getId()));
                Assert.assertTrue(referenceNodes.contains(t1c2.getId()));
                Assert.assertTrue(referenceNodes.contains(t2c1.getId()));
                Assert.assertTrue(referenceNodes.contains(t1c2.getId()));
                Assert.assertTrue(referenceNodes.contains(table1.getId()));
                Assert.assertTrue(referenceNodes.contains(table2.getId()));
                Assert.assertTrue(referenceNodes.contains(db.getId()));
                return null;
            }
        });
    }

    @Test
    public void testFilterEdges() {
        cleanGraph();
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex res = GraphCreation.createResource(transaction, root, "res", "t", "d", layer, REVISION_1_000000);
                Vertex s = GraphCreation.createNode(transaction, res, "s", "Column", REVISION_1_000000);

                Vertex p1 = GraphCreation.createNode(transaction, res, "p1", "Column", REVISION_1_000000);
                Vertex p2 = GraphCreation.createNode(transaction, res, "p2", "Column", REVISION_1_000000);
                Vertex p3 = GraphCreation.createNode(transaction, res, "p3", "Column", REVISION_1_000000);

                Vertex l1 = GraphCreation.createNode(transaction, res, "l1", "Column", REVISION_1_000000);
                Vertex l2 = GraphCreation.createNode(transaction, res, "l2", "Column", REVISION_1_000000);
                Vertex l3 = GraphCreation.createNode(transaction, res, "l3", "Column", REVISION_1_000000);

                Vertex fl11 = GraphCreation.createNode(transaction, res, "fl11", "Column", REVISION_1_000000);
                Vertex fl21 = GraphCreation.createNode(transaction, res, "fl21", "Column", REVISION_1_000000);
                Vertex fl31 = GraphCreation.createNode(transaction, res, "fl31", "Column", REVISION_1_000000);
                Vertex fl32 = GraphCreation.createNode(transaction, res, "fl32", "Column", REVISION_1_000000);
                Vertex fl3b = GraphCreation.createNode(transaction, res, "fl3b", "Column", REVISION_1_000000);
                Vertex fl32b = GraphCreation.createNode(transaction, res, "fl32b", "Column", REVISION_1_000000);

                Vertex fp11 = GraphCreation.createNode(transaction, res, "fp11", "Column", REVISION_1_000000);
                Vertex fp21 = GraphCreation.createNode(transaction, res, "fp21", "Column", REVISION_1_000000);
                Vertex fp31 = GraphCreation.createNode(transaction, res, "fp31", "Column", REVISION_1_000000);
                Vertex fp32 = GraphCreation.createNode(transaction, res, "fp32", "Column", REVISION_1_000000);
                Vertex fp3b = GraphCreation.createNode(transaction, res, "fp3b", "Column", REVISION_1_000000);
                Vertex fp32b = GraphCreation.createNode(transaction, res, "fp32b", "Column", REVISION_1_000000);

                Vertex fs1 = GraphCreation.createNode(transaction, res, "fs1", "Column", REVISION_1_000000);
                Vertex fs2 = GraphCreation.createNode(transaction, res, "fs2", "Column", REVISION_1_000000);

                GraphCreation.createEdge(l1, s, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(l2, l1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(l3, l2, EdgeLabel.DIRECT, REVISION_1_000000);

                GraphCreation.createEdge(s, p1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(p1, p2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(p2, p3, EdgeLabel.DIRECT, REVISION_1_000000);

                GraphCreation.createEdge(fl11, l1, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(fl21, l2, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(fl31, l3, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(fl32, l3, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(l3, fl3b, EdgeLabel.FILTER, REVISION_1_000000);

                GraphCreation.createEdge(p1, fp11, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(p2, fp21, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(p3, fp31, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(p3, fp32, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(fp3b, p3, EdgeLabel.FILTER, REVISION_1_000000);

                GraphCreation.createEdge(fs1, s, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(s, fs2, EdgeLabel.FILTER, REVISION_1_000000);

                GraphCreation.createEdge(fl32b, fl32, EdgeLabel.FILTER, REVISION_1_000000);
                GraphCreation.createEdge(fp32, fp32b, EdgeLabel.FILTER, REVISION_1_000000);

                InitReferenceViewCommand command = new InitReferenceViewCommand();
                command.setAlgorithmExecutor(getAlgorithmExecutor());

                List<Long> referencePoints = new ArrayList<>();
                referencePoints.add((Long) s.getId());

                InitRequest request = new InitRequest();
                request.setReferencePoints(referencePoints);
                request.setFilterEdges(true);

                FlowStateViewer referenceView = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                command.setRelationshipService(getRelationshipService());
                InitResponse response = command.execute(request, referenceView, transaction);
                Assert.assertTrue(response.isOk());

                Set<Long> referenceNodes = referenceView.getReferenceNodeIds();

                for (Long id : referenceNodes) {
                    System.err.println(transaction.getVertex(id).getProperty(NodeProperty.NODE_NAME.t()));
                }

                Assert.assertEquals(17, referenceNodes.size());
                Assert.assertTrue(referenceNodes.contains(s.getId()));
                Assert.assertTrue(referenceNodes.contains(p1.getId()));
                Assert.assertTrue(referenceNodes.contains(p2.getId()));
                Assert.assertTrue(referenceNodes.contains(p3.getId()));
                Assert.assertTrue(referenceNodes.contains(l1.getId()));
                Assert.assertTrue(referenceNodes.contains(l2.getId()));
                Assert.assertTrue(referenceNodes.contains(l3.getId()));

                Assert.assertTrue(referenceNodes.contains(fp11.getId()));
                Assert.assertTrue(referenceNodes.contains(fp21.getId()));
                Assert.assertTrue(referenceNodes.contains(fp31.getId()));
                Assert.assertTrue(referenceNodes.contains(fp32.getId()));
                Assert.assertTrue(referenceNodes.contains(fl11.getId()));
                Assert.assertTrue(referenceNodes.contains(fl21.getId()));
                Assert.assertTrue(referenceNodes.contains(fl31.getId()));
                Assert.assertTrue(referenceNodes.contains(fl32.getId()));

                Assert.assertTrue(referenceNodes.contains(fs1.getId()));
                Assert.assertTrue(referenceNodes.contains(fs2.getId()));

                Assert.assertFalse(referenceNodes.contains(fl3b.getId()));
                Assert.assertFalse(referenceNodes.contains(fp3b.getId()));

                Assert.assertFalse(referenceNodes.contains(fl32b.getId()));
                Assert.assertFalse(referenceNodes.contains(fp32b.getId()));

                return null;
            }
        });
    }

    @Test
    public void testTooManyStarts() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                InitReferenceViewCommand command = new InitReferenceViewCommand();

                List<Long> referencePoints = new ArrayList<>();
                referencePoints.add((Long) table1.getId());

                InitRequest request = new InitRequest();
                request.setReferencePoints(referencePoints);
                request.setMaximumStartNodes(getDataSizeCheckerAlwaysLarge().getMaximumStartNodes());
                FlowStateViewer referenceView = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                command.setRelationshipService(getRelationshipService());
                BaseResponse response = command.execute(request, referenceView, transaction);
                Assert.assertNotSame(BaseResponse.OK, response);
                Assert.assertEquals(Severity.ERROR, response.getMessages().get(0).getSeverity());

                Assert.assertEquals(0, referenceView.getReferenceNodeIds().size());
                return null;
            }
        });
    }

    @Test
    public void testCommandCompareRevs() {
        cleanGraph();
        final Map<String, Long> idsMap = createBasicComapringGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                InitReferenceViewCommand command = new InitReferenceViewCommand();
                command.setAlgorithmExecutor(getAlgorithmExecutor());

                List<Long> referencePoints = new ArrayList<>();
                referencePoints.add(idsMap.get("t1c1r3"));

                InitRequest request = new InitRequest();
                request.setReferencePoints(referencePoints);
                request.setFilterEdges(true);
                FlowStateViewer referenceView = new FlowStateViewer(REVISION_COMPARE_INTERVAL,
                        getHorizontalFilterProvider());
                command.setRelationshipService(getRelationshipService());
                InitResponse response = command.execute(request, referenceView, transaction);
                Assert.assertTrue(response.isOk());

                Set<Long> referenceNodes = referenceView.getReferenceNodeIds();
                Assert.assertEquals(13, referenceNodes.size());
                Assert.assertTrue(referenceNodes.contains(idsMap.get("db")));
                Assert.assertTrue(referenceNodes.contains(idsMap.get("table1")));
                Assert.assertTrue(referenceNodes.contains(idsMap.get("table2")));
                Assert.assertTrue(referenceNodes.contains(idsMap.get("table3")));
                Assert.assertTrue(referenceNodes.contains(idsMap.get("table4")));
                Assert.assertTrue(referenceNodes.contains(idsMap.get("t1c1r1")));
                Assert.assertTrue(referenceNodes.contains(idsMap.get("t1c1r3")));
                Assert.assertTrue(referenceNodes.contains(idsMap.get("t1c2")));
                Assert.assertTrue(referenceNodes.contains(idsMap.get("t2c1r1")));
                Assert.assertTrue(referenceNodes.contains(idsMap.get("t2c2")));
                Assert.assertTrue(referenceNodes.contains(idsMap.get("t3c1")));
                Assert.assertTrue(referenceNodes.contains(idsMap.get("t3c2")));
                Assert.assertTrue(referenceNodes.contains(idsMap.get("t4c1")));
                Assert.assertFalse(referenceNodes.contains(idsMap.get("t3c3r2")));
                return null;
            }
        });
    }
}
