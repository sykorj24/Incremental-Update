package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ProcessLinkRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ProcessLinkResponse;
import junit.framework.Assert;

public class ProcessLinkCommandTest extends AbstractVizualizeTest{
    @Test
    public void testTransformOk() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                ProcessLinkCommand command = new ProcessLinkCommand();
                command.setRevisionRootHandler(getRevisionRootHandler());
                ProcessLinkRequest request = new ProcessLinkRequest();
                request.setDepth(2);
                request.setDirection(Direction.OUT);
                request.setFilter("1");
                request.setFilterEdges(true);
                request.setLevel(FlowLevel.MIDDLE);
                request.setRootId((Long) getSuperRootHandler().getRoot(transaction).getId());
                List<List<List<String>>> si = new ArrayList<List<List<String>>>();
                List<List<String>> si0 = new ArrayList<List<String>>();
                si0.add(Arrays.asList("Teradata", "Teradata"));
                si0.add(Arrays.asList("db", "Database"));
                si0.add(Arrays.asList("table1", "Table"));
                si0.add(Arrays.asList("t1c1", "Column"));
                si.add(si0);
                
                List<List<String>> si1 = new ArrayList<List<String>>();
                si1.add(Arrays.asList("Teradata", "Teradata"));
                si1.add(Arrays.asList("db", "Database"));
                si1.add(Arrays.asList("table2", "Table"));
                si1.add(Arrays.asList("t2c2", "Column"));
                si.add(si1);
                
                request.setSi(si);
                
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                ProcessLinkResponse response = command.execute(request, flowState , transaction);
                String[] transofmerStarts = response.getFormModel().getSelectedItems();
                
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();
                
                Assert.assertEquals(2, transofmerStarts.length);
                Assert.assertEquals(t1c1.getId().toString(), transofmerStarts[0]);
                Assert.assertEquals(t2c2.getId().toString(), transofmerStarts[1]);

                return null;
            }
        });
    }
    
    @Test
    public void testTransformBad() {
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                ProcessLinkCommand command = new ProcessLinkCommand();
                command.setRevisionRootHandler(getRevisionRootHandler());
                ProcessLinkRequest request = new ProcessLinkRequest();
                request.setDepth(2);
                request.setDirection(Direction.OUT);
                request.setFilter("1");
                request.setFilterEdges(true);
                request.setLevel(FlowLevel.MIDDLE);
                request.setRootId((Long) getSuperRootHandler().getRoot(transaction).getId());
                
                request.setSi(null);                
                
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                ProcessLinkResponse response = command.execute(request, flowState, transaction);
                Assert.assertEquals(0, response.getFormModel().getSelectedItems().length);
                
                request.setSi(Collections.<List<List<String>>>emptyList());                
                response = command.execute(request, flowState, transaction);
                Assert.assertEquals(0, response.getFormModel().getSelectedItems().length);
                
                request.setSi(Collections.singletonList(Collections.singletonList(Collections.singletonList("cha"))));
                response = command.execute(request, flowState, transaction);
                Assert.assertEquals(0, response.getFormModel().getSelectedItems().length);
                
                request.setSi(Collections.singletonList(Collections.singletonList(Arrays.asList("cha", "cha"))));
                response = command.execute(request, flowState, transaction);
                Assert.assertEquals(0, response.getFormModel().getSelectedItems().length);
                
                List<List<List<String>>> si = new ArrayList<List<List<String>>>();
                List<List<String>> si0 = new ArrayList<List<String>>();
                si0.add(Arrays.asList("Teradata", "TeraType"));
                si0.add(Arrays.asList("db", "Database"));
                si0.add(Arrays.asList("table1"));
                si0.add(Arrays.asList("t1c1", "Column"));
                si.add(si0);            
                request.setSi(si);
                response = command.execute(request, flowState, transaction);
                Assert.assertEquals(0, response.getFormModel().getSelectedItems().length);
                
                si = new ArrayList<List<List<String>>>();
                si0 = new ArrayList<List<String>>();
                si0.add(Arrays.asList("Teradata"));
                si0.add(Arrays.asList("db", "Database"));
                si0.add(Arrays.asList("table1"));
                si0.add(Arrays.asList("t1c1", "Column"));
                si.add(si0);            
                request.setSi(si);
                response = command.execute(request, flowState, transaction);
                Assert.assertEquals(0, response.getFormModel().getSelectedItems().length);
                
                si = new ArrayList<List<List<String>>>();
                si0 = new ArrayList<List<String>>();
                si0.add(Arrays.asList("Teradata"));
                si0.add(Arrays.asList("db", "Database"));
                si0.add(Arrays.asList("table1"));
                si0.add(Arrays.asList("t1c5", "Column"));
                si.add(si0);                
                request.setSi(si);
                response = command.execute(request, flowState, transaction);
                Assert.assertEquals(0, response.getFormModel().getSelectedItems().length);

                return null;
            }
        });
    }
    
    @Test
    public void testTransformResourceParent() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        GraphCreation.createTechnicalRevision(getDatabaseHolder(), getRevisionRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teraDdl = GraphCreation.createResource(transaction, root, "Teradata DDL", "TeraDDL", "", layer, REVISION_1_000000);
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "TeraType",
                        "Description", layer, REVISION_1_000000);
                
                Vertex folder1 = GraphCreation.createNode(transaction, teraDdl, "Folder1", "Directory", REVISION_1_000000); 
                Vertex table1b = GraphCreation.createNode(transaction, folder1, teradataRes, "table1", "Table", REVISION_1_000000);
                Vertex t1bc1 = GraphCreation.createNode(transaction, table1b, "t1bc1", "Column", REVISION_1_000000);
                
                Vertex table1 = GraphCreation.createNode(transaction, teradataRes, "table1", "Table", REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                
                ProcessLinkCommand command = new ProcessLinkCommand();
                command.setRevisionRootHandler(getRevisionRootHandler());
                ProcessLinkRequest request = new ProcessLinkRequest();
                request.setDepth(2);
                request.setDirection(Direction.OUT);
                request.setFilter("1");
                request.setFilterEdges(true);
                request.setLevel(FlowLevel.MIDDLE);
                request.setRootId((Long) getSuperRootHandler().getRoot(transaction).getId());
                List<List<List<String>>> si = new ArrayList<List<List<String>>>();
                List<List<String>> si0 = new ArrayList<List<String>>();
                si0.add(Arrays.asList("Teradata", "TeraType"));
                si0.add(Arrays.asList("table1", "Table"));
                si0.add(Arrays.asList("t1c1", "Column"));
                si.add(si0);
                
                List<List<String>> si1 = new ArrayList<List<String>>();
                si1.add(Arrays.asList("Teradata DDL", "TeraDDL"));
                si1.add(Arrays.asList("Folder1", "Directory"));
                si1.add(Arrays.asList("table1", "Table"));
                si1.add(Arrays.asList("t1bc1", "Column"));
                si.add(si1);
                
                request.setSi(si);
                
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                ProcessLinkResponse response = command.execute(request, flowState, transaction);
                String[] transofmerStarts = response.getFormModel().getSelectedItems();
                
                Assert.assertEquals(2, transofmerStarts.length);
                Assert.assertEquals(t1c1.getId().toString(), transofmerStarts[0]);
                Assert.assertEquals(t1bc1.getId().toString(), transofmerStarts[1]);

                return null;
            }
        });
    }
}
