package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ExportRequest;
import eu.profinit.manta.platform.configuration.ZipFileDecompresser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:mvc-dispatcher-servlet.xml" })
public class ExportCommandTest extends AbstractVizualizeTest {

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(ExportCommandTest.class);

    private static final String ENCODING = "utf-8";

    @Autowired
    ResourceLoader resourceLoader;

    @Test
    public void testBottomNoFilter() throws IOException {
        createTestGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                initRef(transaction, t1c1, getLevelMapProvider(resourceLoader), flowState, 17, FlowLevel.BOTTOM, true);
                executeExport(transaction, flowState);
                return null;
            }
        });

        String edgeExpected = FileUtils
                .readFileToString(new File("src/test/resources/export/bottomNoFilterRelations.txt"));
        String edgeOutput = FileUtils.readFileToString(new File("target/relations.csv"));
        testFilesContentEqual(edgeExpected, edgeOutput);

        String verticesExpected = FileUtils
                .readFileToString(new File("src/test/resources/export/bottomNoFilterVertices.txt"));
        String verticesOutput = FileUtils.readFileToString(new File("target/vertices.csv"));
        testFilesContentEqual(verticesExpected, verticesOutput);
    }

    @Test
    public void testMiddleNoFilter() throws IOException {
        createTestGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                initRef(transaction, t1c1, getLevelMapProvider(resourceLoader), flowState, 17, FlowLevel.MIDDLE, true);
                executeExport(transaction, flowState);
                return null;
            }
        });

        String edgeExpected = FileUtils
                .readFileToString(new File("src/test/resources/export/middleNoFilterRelations.txt"));
        String edgeOutput = FileUtils.readFileToString(new File("target/relations.csv"));
        testFilesContentEqual(edgeExpected, edgeOutput);

        String verticesExpected = FileUtils
                .readFileToString(new File("src/test/resources/export/middleNoFilterVertices.txt"));
        String verticesOutput = FileUtils.readFileToString(new File("target/vertices.csv"));
        testFilesContentEqual(verticesExpected, verticesOutput);
    }

    @Test
    public void testTopNoFilter() throws IOException {
        createTestGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                initRef(transaction, t1c1, getLevelMapProvider(resourceLoader), flowState, 17, FlowLevel.TOP, true);
                executeExport(transaction, flowState);
                return null;
            }
        });

        String edgeExpected = FileUtils
                .readFileToString(new File("src/test/resources/export/topNoFilterRelations.txt"));
        String edgeOutput = FileUtils.readFileToString(new File("target/relations.csv"));
        testFilesContentEqual(edgeExpected, edgeOutput);

        String verticesExpected = FileUtils
                .readFileToString(new File("src/test/resources/export/topNoFilterVertices.txt"));
        String verticesOutput = FileUtils.readFileToString(new File("target/vertices.csv"));
        testFilesContentEqual(verticesExpected, verticesOutput);
    }

    @Test
    public void testMiddleFilterOracle() throws IOException {
        createTestGraphForFiltres();
        getHorizontalFilterProvider().refreshFilters();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                initRef(transaction, t1c1, getLevelMapProvider(resourceLoader), flowState, 11, FlowLevel.MIDDLE, true);
                flowState.setActiveFilters(Collections.singleton("Oracle"));
                executeExport(transaction, flowState);
                return null;
            }
        });

        String edgeExpected = FileUtils
                .readFileToString(new File("src/test/resources/export/middleFilterOracleRelations.txt"));
        String edgeOutput = FileUtils.readFileToString(new File("target/relations.csv"));
        testFilesContentEqual(edgeExpected, edgeOutput);

        String verticesExpected = FileUtils
                .readFileToString(new File("src/test/resources/export/middleFilterOracleVertices.txt"));
        String verticesOutput = FileUtils.readFileToString(new File("target/vertices.csv"));
        testFilesContentEqual(verticesExpected, verticesOutput);
    }

    @Test
    public void testMiddleFilterOracleFromTable() throws IOException {
        createTestGraphForFiltres();
        getHorizontalFilterProvider().refreshFilters();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                initRef(transaction, table1, getLevelMapProvider(resourceLoader), flowState, 11, FlowLevel.MIDDLE, true);
                flowState.setActiveFilters(Collections.singleton("Oracle"));
                executeExport(transaction, flowState);
                return null;
            }
        });

        String edgeExpected = FileUtils
                .readFileToString(new File("src/test/resources/export/middleFilterOracleRelations.txt"));
        String edgeOutput = FileUtils.readFileToString(new File("target/relations.csv"));
        testFilesContentEqual(edgeExpected, edgeOutput);

        String verticesExpected = FileUtils
                .readFileToString(new File("src/test/resources/export/middleFilterOracleVertices.txt"));
        String verticesOutput = FileUtils.readFileToString(new File("target/vertices.csv"));
        testFilesContentEqual(verticesExpected, verticesOutput);
    }
  

    private void executeExport(TitanTransaction transaction, FlowStateViewer flowState) {
        //kam se bude ukládat zazipovaný soubor vytvořený později příkazem command.execute
        FileOutputStream outputStream = null;
        File zipFile = new File("target/output.zip");
        try {
            outputStream = new FileOutputStream(zipFile);
        } catch (FileNotFoundException e1) {
            LOGGER.error("File not found.", e1);
            Assert.fail();
        }

        //export
        ExportCommand command = new ExportCommand();
        command.setTechnicalAttributesHolder(new DummyTechnicalAttributesHolder());
        command.setLevelMapProvider(getLevelMapProvider(resourceLoader));
        command.setUsageStatsCollector(getUsageStatsCollector());
        command.setRelationshipService(getRelationshipService());
        ReflectionTestUtils.setField(command, "csvSeparator", ExportCommand.CsvSeparator.COMMA);
        ExportRequest request = new ExportRequest();
        request.setOutputStream(outputStream);
        command.execute(request, flowState, transaction);

        //dekomprese
        File directory = new File("target");
        ZipFileDecompresser decompresser = new ZipFileDecompresser();
        decompresser.decompress(zipFile, directory, ENCODING);
    }

    /**
     * Testuje XmlLevelMapProvider, jestli umí správně přiřadit groupname pro zvolenou technologii
     */
    @Test
    public void testXmlLevelMapProvider() {
        Assert.assertEquals(getLevelMapProvider(resourceLoader).getGroupType("IFPC"), Collections.singleton("IFPC Workflow"));
    }

    private void createTestGraph() {
        cleanGraph();
        createBasicGraph(false);

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex teradataRes = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator()
                        .next();

                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                Vertex newNode = GraphCreation.createNode(transaction, table1, "cha aa  \"['", "Column", REVISION_1_000000);
                GraphCreation.createEdge(t1c1, newNode, EdgeLabel.DIRECT, REVISION_1_000000);

                Vertex root = getSuperRootHandler().getRoot(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex oracleRes = GraphCreation.createResource(transaction, root, "Oracle", "Oracle", "Description",
                        layer, REVISION_1_000000);
                Vertex dbO = GraphCreation.createNode(transaction, oracleRes, "db-O", "Database", REVISION_1_000000);
                Vertex table3 = GraphCreation.createNode(transaction, dbO, "table3", "Table", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, table3, "TABLE_TYPE", "MY_OWN", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, t3c1, "first", "1", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, t3c1, "second", "2", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, t3c1, "third", "3", REVISION_1_000000);
                Vertex t3c2 = GraphCreation.createNode(transaction, table3, "t3c2", "Column", REVISION_1_000000);
                Vertex t3c3 = GraphCreation.createNode(transaction, table3, "t3c3", "Column", REVISION_1_000000);
                GraphCreation.createEdge(t1c1, t3c3, EdgeLabel.DIRECT, REVISION_1_000000); //tenhle se vypisuje
                GraphCreation.createEdge(t3c2, t2c1, EdgeLabel.DIRECT, REVISION_1_000000); //není v REF_VIEW
                GraphCreation.createEdge(t3c2, t3c1, EdgeLabel.FILTER, REVISION_1_000000); //není v REF_VIEW
                GraphCreation.createEdge(t3c1, t1c1, EdgeLabel.FILTER, REVISION_1_000000); //vstupní v REF_VIEW (11 zmen na 12) - taky na výstup

                Vertex warehouse = GraphCreation.createNode(transaction, teradataRes, "\"Data warehouse\"", "DWH",
                        REVISION_1_000000);
                Vertex inner_db = GraphCreation.createNode(transaction, warehouse, "\"Inner DB\"", "Database",
                        REVISION_1_000000);
                Vertex mart1 = GraphCreation.createNode(transaction, inner_db, "mart1", "Mart", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, mart1, "mart", "prvni", REVISION_1_000000);
                Vertex mart2 = GraphCreation.createNode(transaction, inner_db, "mart2", "Mart", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, mart2, "mart", "druhy", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, mart2, "mart", "hlavni", REVISION_1_000000); //tento atribut se má taky vypsat
                Vertex table4 = GraphCreation.createNode(transaction, mart2, "inner_table", "Table", REVISION_1_000000);
                Vertex t4c1 = GraphCreation.createNode(transaction, table4, "t4c1", "Column", REVISION_1_000000);
                GraphCreation.createEdge(t2c2, t4c1, EdgeLabel.DIRECT, REVISION_1_000000);
                return null;
            }
        });
    }
    
    private void createTestGraphForFiltres() {
        cleanGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);

                getRevisionRootHandler().createMajorRevision(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex teradataRes = GraphCreation.createResource(transaction, root, "Teradata", "Teradata",
                        "Description", layer, REVISION_1_000000);
                Vertex db = GraphCreation.createNode(transaction, teradataRes, "db", "Database", REVISION_1_000000);
                
                Vertex oracleRes = GraphCreation.createResource(transaction, root, "Oracle", "Oracle",
                        "Description", layer, REVISION_1_000000);
                Vertex schema = GraphCreation.createNode(transaction, oracleRes, "schema", "Database", REVISION_1_000000);

                Vertex table1 = GraphCreation.createNode(transaction, db, "table1", "Table", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, table1, "TABLE_TYPE", "TABLE", REVISION_1_000000);
                Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
                Vertex t1c2 = GraphCreation.createNode(transaction, table1, "t1c2", "Column", REVISION_1_000000);

                Vertex table2 = GraphCreation.createNode(transaction, db, "table2", "Table", REVISION_1_000000);
                GraphCreation.createNodeAttribute(transaction, table2, "TABLE_TYPE", "VIEW", REVISION_1_000000);
                Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
                Vertex t2c2 = GraphCreation.createNode(transaction, table2, "t2c2", "Column", REVISION_1_000000);
                

                Vertex table3 = GraphCreation.createNode(transaction, schema, "table3", "Table", REVISION_1_000000);
                Vertex t3c1 = GraphCreation.createNode(transaction, table3, "t3c1", "Column", REVISION_1_000000);
                Vertex t3c2 = GraphCreation.createNode(transaction, table3, "t3c2", "Column", REVISION_1_000000);

                GraphCreation.createEdge(t1c1, t1c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t1c2, t3c1, EdgeLabel.DIRECT, REVISION_1_000000);
                
                GraphCreation.createEdge(t3c1, t3c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c2, t2c1, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t3c1, t2c2, EdgeLabel.FILTER, REVISION_1_000000);
                
                GraphCreation.createEdge(t2c1, t2c2, EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(t2c2, t1c1, EdgeLabel.DIRECT, REVISION_1_000000);

                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);
                
                return null;
            }
        });
        
    }

    private void testFilesContentEqual(String expected, String current) {

        List<String> expectedList = new ArrayList<>(Arrays.asList(expected.split("\r?\n")));
        List<String> currentList = new ArrayList<>(Arrays.asList(current.split("\r?\n")));

        Collections.sort(expectedList);
        Collections.sort(currentList);

        Assert.assertEquals(StringUtils.join(expectedList, "\n"), StringUtils.join(currentList, "\n"));
    }
}