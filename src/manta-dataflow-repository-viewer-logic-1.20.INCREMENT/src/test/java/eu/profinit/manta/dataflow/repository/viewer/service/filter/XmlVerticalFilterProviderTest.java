package eu.profinit.manta.dataflow.repository.viewer.service.filter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.service.filter.XmlVerticalFilterProvider.VertexPosition;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.AbstractVizualizeTest;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:mvc-dispatcher-servlet.xml")
public class XmlVerticalFilterProviderTest extends AbstractVizualizeTest {
    @Autowired
    ResourceLoader resourceLoader;

    @Test
    public void testGetFiltersModel() {
        XmlVerticalFilterProvider provider = new XmlVerticalFilterProvider();
        provider.setFiltersFileLocation("verticalFilters.xml");
        provider.setResourceLoader(resourceLoader);
        provider.init();

        Assert.assertTrue(provider.isElementDisabled("Filesystem", "Directory", VertexPosition.MIDDLE));
        Assert.assertFalse(provider.isElementDisabled("Filesystem", "D", VertexPosition.MIDDLE));
        Assert.assertTrue(provider.isElementDisabled("T", "a", VertexPosition.MIDDLE));
        Assert.assertTrue(provider.isElementDisabled("T", "b", VertexPosition.MIDDLE));
        Assert.assertFalse(provider.isElementDisabled("T", "x", VertexPosition.MIDDLE));
    }

    @Test
    public void testIsDisabledByVertex() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                XmlVerticalFilterProvider provider = new XmlVerticalFilterProvider();
                provider.setFiltersFileLocation("verticalFilters.xml");
                provider.setResourceLoader(resourceLoader);
                provider.init();

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata DDL", "Teradata DDL", "Ter", layer, REVISION_1_000000);
                Vertex directory = GraphCreation.createNode(transaction, resource, "ddd", "Directory", REVISION_1_000000);
                Vertex bla = GraphCreation.createNode(transaction, resource, "bla", "bla", REVISION_1_000000);

                Assert.assertTrue(provider.isElementDisabled(directory, directory));
                Assert.assertFalse(provider.isElementDisabled(bla, bla));

                return null;
            }
        });
    }
    
    @Test
    public void testIsSameTypeInSeq() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                XmlVerticalFilterProvider provider = new XmlVerticalFilterProvider();
                provider.setFiltersFileLocation("verticalFilters.xml");
                provider.setResourceLoader(resourceLoader);
                provider.init();

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata DDL", "Ter", "Ter", layer, REVISION_1_000000);
                Vertex d1 = GraphCreation.createNode(transaction, resource, "d1", "Directory", REVISION_1_000000);
                Vertex d2 = GraphCreation.createNode(transaction, d1, "d2", "Directory", REVISION_1_000000);
                
                Vertex s1 = GraphCreation.createNode(transaction, d2, "s1", "SomethingElse", REVISION_1_000000);
                
                Vertex d3 = GraphCreation.createNode(transaction, s1, "d3", "Directory", REVISION_1_000000);                
                Vertex d4 = GraphCreation.createNode(transaction, d3, "d4", "Directory", REVISION_1_000000);
                
                Assert.assertFalse(provider.isSameTypeInSeq(d1, d1));
                Assert.assertTrue(provider.isSameTypeInSeq(d1, d2));
                Assert.assertTrue(provider.isSameTypeInSeq(d1, s1));
                Assert.assertTrue(provider.isSameTypeInSeq(d1, d3));
                
                Assert.assertFalse(provider.isSameTypeInSeq(d2, s1));
                Assert.assertFalse(provider.isSameTypeInSeq(d2, d3));
                Assert.assertFalse(provider.isSameTypeInSeq(d2, d4));
                Assert.assertFalse(provider.isSameTypeInSeq(s1, s1));
                Assert.assertFalse(provider.isSameTypeInSeq(s1, d4));                
                
                Assert.assertTrue(provider.isSameTypeInSeq(d3, d4));
                Assert.assertFalse(provider.isSameTypeInSeq(d4, d4));
                
                return null;
            }
        });
    }
    
    @Test
    public void testFindPosition() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                XmlVerticalFilterProvider provider = new XmlVerticalFilterProvider();
                provider.setFiltersFileLocation("verticalFilters.xml");
                provider.setResourceLoader(resourceLoader);
                provider.init();

                Vertex root = getSuperRootHandler().ensureRootExistance(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, Layer.DEFAULT.getName(), Layer.DEFAULT.getType());
                Vertex resource = GraphCreation.createResource(transaction, root, "Teradata DDL", "Ter", "Ter", layer, REVISION_1_000000);
                Vertex d1 = GraphCreation.createNode(transaction, resource, "d1", "Directory", REVISION_1_000000);
                Vertex d2 = GraphCreation.createNode(transaction, d1, "d2", "Directory", REVISION_1_000000);
                
                Vertex s1 = GraphCreation.createNode(transaction, d2, "s1", "SomethingElse", REVISION_1_000000);
                
                Vertex d3 = GraphCreation.createNode(transaction, s1, "d3", "Directory", REVISION_1_000000);                
                Vertex d4 = GraphCreation.createNode(transaction, d3, "d4", "Directory", REVISION_1_000000);
                Vertex d5 = GraphCreation.createNode(transaction, d4, "d5", "Directory", REVISION_1_000000);
                
                Vertex s2 = GraphCreation.createNode(transaction, d5, "s2", "SomethingElse", REVISION_1_000000);
                
                Assert.assertEquals(VertexPosition.ONLY_ONE, provider.findPosition(d1, d1));
                Assert.assertEquals(VertexPosition.LAST, provider.findPosition(d1, d2));
                Assert.assertEquals(VertexPosition.LAST, provider.findPosition(d1, s1));
                Assert.assertEquals(VertexPosition.FIRST, provider.findPosition(d2, s1));
                Assert.assertEquals(VertexPosition.FIRST, provider.findPosition(d2, d2));
                
                Assert.assertEquals(VertexPosition.MIDDLE, provider.findPosition(d4, s2));
                Assert.assertEquals(VertexPosition.MIDDLE, provider.findPosition(d4, d5));
                Assert.assertEquals(VertexPosition.FIRST, provider.findPosition(d4, d4));
                
                Assert.assertEquals(VertexPosition.FIRST, provider.findPosition(d4, d1));
                                
                return null;
            }
        });
    }
}
