package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilter;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterType;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterUnit;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.unit.HorizontalFilterImpl;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.unit.TypeFilterUnit;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.HiddenByFiltersRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.HiddenByFiltersResponse;

public class HiddenByFiltersCommandTest extends AbstractVizualizeTest {

    @Test
    public void testExecute() {
        createBasicGraph();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                HorizontalFilterProvider provider = getHorizontalFilterProvider();

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                Set<Long> componentIds = new HashSet<>();
                componentIds.add((Long) t1c1.getId());
                componentIds.add((Long) t1c2.getId());
                componentIds.add((Long) t2c1.getId());
                componentIds.add((Long) t2c2.getId());
                componentIds.add(123456L);

                HorizontalFilterImpl filter = new HorizontalFilterImpl();
                filter.setId("customFilter");
                filter.setName("Custom Filter");
                filter.setOrder(0);
                filter.setType(HorizontalFilterType.CUSTOM);
                HorizontalFilterUnit filterUnit = new TypeFilterUnit(null, Collections.singleton("Table"));
                filter.setFilterUnits(Collections.<HorizontalFilterUnit> singletonList(filterUnit));
                provider.addFilters(Collections.<HorizontalFilter> singleton(filter));

                FlowStateViewer flowState = new FlowStateViewer(REVISION_INTERVAL_1_000000_1_000000, getHorizontalFilterProvider());
                flowState.addStartNode((Long) t1c1.getId());
                
                HiddenByFiltersRequest request = new HiddenByFiltersRequest();
                request.setComponentIds(componentIds);
                request.setActiveFilters(Collections.singleton("customFilter"));

                HiddenByFiltersCommand cmd = new HiddenByFiltersCommand();
                cmd.setFilterProvider(provider);
                HiddenByFiltersResponse response = cmd.execute(request, flowState, transaction);
                
                Map<Long, Boolean> states = response.getFilterStates();
                Assert.assertEquals(4, states.size());
                Assert.assertFalse(states.get(t1c1.getId()));
                Assert.assertTrue(states.get(t1c2.getId()));
                Assert.assertTrue(states.get(t2c1.getId()));
                Assert.assertTrue(states.get(t2c2.getId()));

                return null;
            }
        });
    }
}
