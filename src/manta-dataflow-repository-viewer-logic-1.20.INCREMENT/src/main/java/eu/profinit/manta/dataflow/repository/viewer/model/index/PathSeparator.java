package eu.profinit.manta.dataflow.repository.viewer.model.index;

/**
 * Ciselnik oddelovacu polozek v ceste k elementu
 *   
 * @author onouza
  */
public enum PathSeparator {
    /** Carka */
    COMMA(',', "Comma (,)"),
    /** Tecka */
    DOT('.', "Dot (.)"),
    /** Dvojtecka */
    COLON(':', "Colon (:)"),
    /** Strednik */
    SEMICOLON(';', "Semicolon (;)"),
    /** Lomitko */
    SLASH('/', "Slash (/)"),
    /** Zpetne lomitko */
    BACKSLASH('\\', "Backslash (\\)"),
    /** Znak tabulatoru */
    TAB_CHAR('\t', "Tab Character"), 
    /** Mezera */
    SPACE(' ', "Space");
   
    /**
     * Znak oddelovace
     */
    final private char character;
    /**
     * Popis oddelovace
     */
    final private String description;

    /**
     * Konstruktor.
     * 
     * @param character Znak oddelovace
     * @param description Popis oddelovace
     */
    private PathSeparator(char character, String description) {
        this.character = character;
        this.description = description;
    }

    // gettery
    
    /**
     * @return Znak oddelovace
     */
    public char getCharacter() {
        return character;
    }

    /**
     * @return Popis oddelovace
     */
    public String getDescription() {
        return description;
    }

}
