package eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data;

/**
 * Immutable třída pro definici zprávy ze serveru.
 * @author tfechtner
 *
 */
public final class Message {
    /** Závažnost zprávy. */
    private final Severity severity;
    /** Text zprávy. */
    private final String text;
    
    /**
     * @param severity  Závažnost zprávy.
     * @param text Text zprávy.
     */
    public Message(Severity severity, String text) {
        super();
        this.severity = severity;
        this.text = text;
    }
    
    /**
     * @return  Závažnost zprávy.
     */
    public Severity getSeverity() {
        return severity;
    }

    /**
     * @return Text zprávy.
     */
    public String getText() {
        return text;
    }

    /**
     * Závažnost zprávy.
     * @author tfechtner
     *
     */
    public enum Severity {
        /** Informativní hláška. */
        INFO, 
        /** Upozornění na nějaký problém či akci serveru. */
        WARNING, 
        /** Chyba kvůi chybným datům nebo problému na serveru.*/
        ERROR;
    }
}
