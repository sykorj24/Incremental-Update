package eu.profinit.manta.dataflow.repository.viewer.service.flow;

import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ProcessLinkRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.index.WelcomeFormModel;

/**
 * Služba ověřující korektnost dotazu pro konkrétní flow z pohledu bezpečnosti.
 * @author tfechtner
 *
 */
public interface RequestAccessSecurityService {
    
    /**
     * Ověří dotaz, jestli je v pořádku.
     * @param request dotaz k ověření
     * @return true, jestliže je dotaz oprávněný
     */
    boolean isCorrect(WelcomeFormModel request); 
    
    /**
     * Ověří dotaz, jestli je v pořádku.
     * @param request dotaz k ověření
     * @return true, jestliže je dotaz oprávněný
     */
    boolean isCorrect(ProcessLinkRequest request);
}
