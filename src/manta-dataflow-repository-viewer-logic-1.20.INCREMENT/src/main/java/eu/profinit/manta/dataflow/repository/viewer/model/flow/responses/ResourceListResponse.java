package eu.profinit.manta.dataflow.repository.viewer.model.flow.responses;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.Validate;

import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Resource;

/**
 * Response pro command na zjištění seznamu resource v db.
 * @author tfechtner
 * @author onouza
 */
public class ResourceListResponse extends AbstractServerResponse {
    
    /**
     * Seznam zdroju
     */
    private List<Resource> resourceList = new ArrayList<>();
    
    /**
     * Default konstruktor předávající předkovi název commandu.
     */
    public ResourceListResponse() {
        super("resourceList");
    }

    // gettery / settery

    /**
     * @return Seznam zdroju
     */
    public List<Resource> getResourceList() {
        return resourceList;
    }

    /**
     * @param resourceList Seznam zdroju
     */
    public void setResourceList(List<Resource> resourceList) {
        Validate.notNull(resourceList);
        this.resourceList = resourceList;
    }

}
