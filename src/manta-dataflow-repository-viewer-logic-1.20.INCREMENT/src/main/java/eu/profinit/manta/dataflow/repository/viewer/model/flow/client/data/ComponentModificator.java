package eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data;


/**
 * Služba pro specifickou úpravu komponenty po její instanciaci.
 * @author tfechtner
 *
 */
public interface ComponentModificator {

    /**
     * Provede modifikaci.
     * @param component výchozí instance
     * @return nová upravená instance
     * @param <T> typ instance
     */
    <T extends Component> T getModification(final T component);

}
