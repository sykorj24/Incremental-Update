package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.AttributeNames;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message.Severity;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ShowStmtInScriptRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ShowStmtInScriptResponse;
import eu.profinit.manta.dataflow.repository.viewer.service.FilteredRelationshipService;
import eu.profinit.manta.dataflow.repository.viewer.service.ViewerOperation;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;
import eu.profinit.manta.platform.web.core.security.SecurityHelper;

/**
 * Command umožňující zobrazit příkaz odpovídající danému uzlu v příslušném zdrojovém skriptu.
 * @author tfechtner
 *
 */
public class ShowStmtInScriptCommand extends AbstractCommand<ShowStmtInScriptRequest, ShowStmtInScriptResponse> {
    /** Název akce pro usage stats pro zobrazení zdrojového kodu. */
    private static final String FLOW_VIS_SOURCE_CODE = "flow_vis_sourceCode";
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(ShowStmtInScriptCommand.class);
    /** Encoding skriptů. */
    private static final String ENCODING = "utf-8";
    /** Název atributu, kde je uloženo umístění skriptu. */
    public static final String SOURCE_LOCATION = "sourceLocation";
    /** Název atributu, kde je uloženo kodování skriptu. */
    public static final String SOURCE_ENCODING = "sourceEncoding";
    /** Pattern pro naparsovani pozice ve skriptu z atributu. */
    public static final Pattern ATTRIBUTE_POSITION_PATTERN = Pattern.compile("\\s*([0-9]+)\\s*,\\s*([0-9]+)\\s*");
    /** Pattern pro naparsovani pozice ve skriptu z labelu. */
    public static final Pattern LABEL_POSITION_PATTERN = Pattern.compile("^<([0-9]+),\\s*([0-9]+)\\s*>\\s*(\\S+\\s*)+");

    @Autowired
    private FilteredRelationshipService relationshipService;
    @Autowired
    private UsageStatsCollector usageStatsCollector;
    @Autowired
    private SourceRootHandler sourceRootHandler;

    @Override
    public ShowStmtInScriptResponse execute(ShowStmtInScriptRequest request, FlowStateViewer flowState,
            TitanTransaction transaction) {
        ShowStmtInScriptResponse response = new ShowStmtInScriptResponse();

        Vertex vertex = transaction.getVertex(request.getComponentId());
        if (vertex == null) {
            response.addMessage(new Message(Severity.ERROR,
                    "Requested element with id " + request.getComponentId() + " does not exist."));
            return response;
        }

        if (flowState == null || flowState.getNodeStatus(vertex) == NodeStatus.UNKNOWN) {
            response.addMessage(new Message(Severity.ERROR,
                    "Requested element with id " + request.getComponentId() + " is not in this data flow."));
            return response;
        }

        List<Vertex> parents = relationshipService.getAllParents(vertex);

        SourceFile sourceFile = findSourceLocation(vertex, parents, flowState.getRevisionInterval());
        if (sourceFile == null) {
            response.addMessage(new Message(Severity.ERROR, "Requested element " + ViewerOperation.getLabel(vertex)
                    + " does not have defined source location."));
            return response;
        }

        String sourceCode = loadSourceCode(sourceFile);
        if (sourceCode == null) {
            response.addMessage(new Message(Severity.ERROR,
                    "Cannot load source code for the element " + ViewerOperation.getLabel(vertex) + "."));
            return response;
        }

        Map<String, Object> usageParams = new HashMap<>();
        usageParams.put("node", ViewerOperation.getType(vertex));
        usageParams.put("resource", ViewerOperation.getType(GraphOperation.getResource(vertex)));
        usageParams.put("flowStateGuid", flowState.getGuid());
        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_VIS_SOURCE_CODE, usageParams);

        Position pos = findPosition(vertex, parents, flowState.getRevisionInterval());
        if (pos == null) {
            // pokud jsme nalezli zdrojový skript (dostali jsme se až sem), tak ho zobrazíme alespoň na začátku
            pos = new Position(1, 1);
        }

        response.setLine(pos.getLine());
        response.setCol(pos.getCol());
        response.setSourceCode(sourceCode);

        return response;
    }

    /**
     * Nalezne pozici příkazu ve skriptu. Nejprve se prozkoumá zkoumaný uzel a pak postupně jeho předci.
     * @param vertex uzel, pro který se hledá pozice
     * @param parents předci uzlu, kteří se postupně prohledávají odspodu
     * @param revisionInterval interval revizí daného flowState
     * @return pozice ve skriptu nebo null, pokud nebyla pozice nalezena
     */
    private Position findPosition(Vertex vertex, List<Vertex> parents, RevisionInterval revisionInterval) {
        Position pos = fetchPosition(vertex, revisionInterval);
        if (pos != null) {
            return pos;
        }

        for (Vertex parent : parents) {
            pos = fetchPosition(parent, revisionInterval);
            if (pos != null) {
                return pos;
            }
        }

        return null;
    }

    /**
     * Získá pozici ve skriptu z textu.
     * 
     * @param vertex uzel, z něhož se získává pozice
     * @param revisionInterval interval revizí daného flowState
     * 
     * @return pozice ve skriptu nebo null, pokud uzel informaci o pozici neobsahuje
     */
    protected Position fetchPosition(Vertex vertex, RevisionInterval revisionInterval) {
        // try to fetch position from attribute
        Object posAttribute = GraphOperation.getFirstNodeAttribute(vertex, AttributeNames.NODE_SCRIPT_POSITION,
                revisionInterval);
        if (posAttribute instanceof String) {
            Matcher attrMatcher = ATTRIBUTE_POSITION_PATTERN.matcher((String) posAttribute);
            if (attrMatcher.matches()) {
                int line = Integer.parseInt(attrMatcher.group(1));
                int col = Integer.parseInt(attrMatcher.group(2));

                return new Position(line, col);
            }
        }

        // try to find position in label
        String label = ViewerOperation.getLabel(vertex);
        Matcher matcher = LABEL_POSITION_PATTERN.matcher(label);
        if (matcher.matches()) {
            int line = Integer.parseInt(matcher.group(1));
            int col = Integer.parseInt(matcher.group(2));

            return new Position(line, col);
        }
        return null;
    }

    /**
     * Získá umístění skriptu.
     * @param vertex uzel, pro který se skript hledá
     * @param parents předkové uzlu
     * @param revisionInterval interval revizí daného flowState
     * @return umístění skriptu nebo null pokud se umístění nepodařilo nalézt
     */
    protected SourceFile findSourceLocation(Vertex vertex, List<Vertex> parents, RevisionInterval revisionInterval) {
        Object attrValue = getNodeAttribute(vertex, SOURCE_LOCATION, revisionInterval);
        if (attrValue != null) {
            return new SourceFile(attrValue.toString(), findSourceEncoding(vertex, revisionInterval));
        } else {
            for (Vertex parent : parents) {
                attrValue = getNodeAttribute(parent, SOURCE_LOCATION, revisionInterval);
                if (attrValue != null) {
                    return new SourceFile(attrValue.toString(), findSourceEncoding(parent, revisionInterval));
                }
            }
        }

        return null;
    }

    /**
     * Získat atribut uzlu s přihlednutím k porovnávání verzí.
     * @param vertex vrchol, pro který se atribut hledá
     * @param attributeKey  klíč hledaného atributu
     * @param revisionInterval interval revizí, pro který se atribut hledá
     * @return hodnota atributu nebo null při neexistenci/chybě
     */
    private Object getNodeAttribute(Vertex vertex, String attributeKey, RevisionInterval revisionInterval) {
        List<Edge> attrEdges = new ArrayList<>();

        Iterable<Edge> edges = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, revisionInterval,
                EdgeLabel.HAS_ATTRIBUTE.t());
        for (Edge edge : edges) {
            Vertex attribute = edge.getVertex(Direction.IN);
            Object attribueName = attribute.getProperty(NodeProperty.ATTRIBUTE_NAME.t());
            if (attributeKey.equals(attribueName)) {
                attrEdges.add(edge);
            }
        }

        if (attrEdges.size() == 0) {
            return null;
        } else if (attrEdges.size() == 1) {
            Edge edge = attrEdges.get(0);
            return getAttributeValue(edge);
        } else {
            List<Edge> attrEdgesLastRev = new ArrayList<>();
            RevisionInterval lastRevision = new RevisionInterval(revisionInterval.getEnd());
            for (Edge edge : attrEdges) {
                if (RevisionUtils.isEdgeInRevisionInterval(edge, lastRevision)) {
                    attrEdgesLastRev.add(edge);
                }
            }

            if (attrEdgesLastRev.size() == 1) {
                Edge edge = attrEdgesLastRev.get(0);
                return getAttributeValue(edge);
            } else {
                List<Object> attrList = new ArrayList<>();
                for (Edge edge : attrEdges) {
                    attrList.add(getAttributeValue(edge));
                }

                LOGGER.error(getErrorForMoreSourceLocations(vertex, attrList));
                return null;
            }
        }
    }

    /**
     * Získá hodnotu atributu pro atributovou hranu.
     * @param edge atributová hrana, jejíž atribut se zjišťuje
     * @return hodnota atributu
     */
    private Object getAttributeValue(Edge edge) {
        return edge.getVertex(Direction.IN).getProperty(NodeProperty.ATTRIBUTE_VALUE.t());
    }

    /**
     * Metoda se pokusí najít kódování zdrojového skriptu.
     * Pokud metoda nenajde právě jeden odpovídající atribut, vrátí výchozí hodnotu.
     * @param vertex vrchol, kde se hledá kódování
     * @param revisionInterval interval revizí daného flowState
     * @return kódování zdroje nebo výchozí, pokud neexistuje
     */
    private String findSourceEncoding(Vertex vertex, RevisionInterval revisionInterval) {
        Object attrValue = getNodeAttribute(vertex, SOURCE_ENCODING, revisionInterval);
        if (attrValue != null) {
            return attrValue.toString();
        } else {
            return ENCODING;
        }
    }

    /**
     * Vrátí hlášku pro případ, kdy je více zdrojových lokací.
     * @param node uzel, pro který se zdrojová loakce hledá
     * @param attrList seznam atributů obsahující zdrojové lokace
     * @return hláška o více lokacích pro jeden uzel
     */
    private String getErrorForMoreSourceLocations(Vertex node, List<Object> attrList) {
        StringBuilder sb = new StringBuilder();
        sb.append("There are more source locations for the node ").append(ViewerOperation.getLabel(node));
        sb.append(":");
        for (Object attr : attrList) {
            sb.append(attr.toString());
            sb.append(", ");
        }

        return sb.toString();
    }

    /**
     * Načte skript z daného umístění.
     * @param sourceFile zdrojový skript
     * @return řetězec představující načtený skript nebo null pokud se skript nepodařilo načíst
     */
    private String loadSourceCode(SourceFile sourceFile) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();

        try {
            File sourceFileHandler = new File(sourceRootHandler.getSourceCodeDir(), sourceFile.getLocation());
            reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(sourceFileHandler), sourceFile.getEncoding()));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append(System.lineSeparator());
            }
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Unsupported encoding for source code script.", e);
            return null;
        } catch (FileNotFoundException e) {
            LOGGER.error("Source code script does not found.", e);
            return null;
        } catch (IOException e) {
            LOGGER.error("Error during reading source code script.", e);
            return null;
        } finally {
            IOUtils.closeQuietly(reader);
        }

        return sb.toString();
    }

    /**
     * Datová třída pro reprezentaci zdrojového skriptu.
     * @author tfechtner
     *
     */
    public static class SourceFile {
        /** Umístění skriptu. */
        private final String location;
        /** Kódování skriptu. */
        private final String encoding;

        /**
         * @param location Umístění skriptu
         * @param encoding Kódování skriptu
         */
        public SourceFile(String location, String encoding) {
            super();
            this.location = location;
            this.encoding = encoding;
        }

        /**
         * @return Umístění skriptu
         */
        public String getLocation() {
            return location;
        }

        /**
         * @return Kódování skriptu
         */
        public String getEncoding() {
            return encoding;
        }

        @Override
        public int hashCode() {
            HashCodeBuilder builder = new HashCodeBuilder();
            builder.append(location);
            builder.append(encoding);
            return builder.toHashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (obj.getClass() != getClass()) {
                return false;
            }
            ShowStmtInScriptCommand.SourceFile other = (ShowStmtInScriptCommand.SourceFile) obj;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(location, other.location);
            builder.append(encoding, other.encoding);
            return builder.isEquals();
        }
    }

    /**
     * Immutable datová třída pro reprezentaci pozice ve skriptu.
     * @author tfechtner
     *
     */
    public static class Position {
        /** Řádek ve skriptu.*/
        private final int line;
        /** Sloupec ve skriptu.*/
        private final int col;

        /**
         * @param line řádek ve skriptu
         * @param col sloupec ve skriptu
         */
        public Position(int line, int col) {
            this.line = line;
            this.col = col;
        }

        /**
         * @return řádek ve skriptu
         */
        public int getLine() {
            return line;
        }

        /**
         * @return sloupec ve skriptu
         */
        public int getCol() {
            return col;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + col;
            result = prime * result + line;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Position other = (Position) obj;
            if (col != other.col) {
                return false;
            }
            if (line != other.line) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "<" + line + "," + col + ">";
        }
    }
}
