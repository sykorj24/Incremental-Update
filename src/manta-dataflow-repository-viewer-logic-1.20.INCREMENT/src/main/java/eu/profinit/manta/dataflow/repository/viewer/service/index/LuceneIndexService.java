package eu.profinit.manta.dataflow.repository.viewer.service.index;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanIndexQuery.Result;
import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.controller.FlowController;
import eu.profinit.manta.dataflow.repository.viewer.model.index.Node;
import eu.profinit.manta.dataflow.repository.viewer.service.ViewerOperation;

/**
 * Třída pro indexované vyhledávání v grafové db.
 * @author tfechtner
 *
 */
public class LuceneIndexService implements IndexService {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(LuceneIndexService.class);

    /** Výchozí počet maximálně napovídaných objektů. */
    private static final int DEFAULT_MAXIMUM_OBJECTS = 100;
    /** Vychozi maximalni pocet objektu prohledavanych podle cesty */
    private static final int DEFAULT_MAXIMUM_PATH_OBJECTS = 100000;

    private final Comparator<Node> comparatorNaturalOrder = new NaturalOrderNodeComparator();

    private final Comparator<Node> comparatorPathOrder = new PathOrderNodeComparator();

    /** Počet maximálně napovídaných objektů. */
    private int maximumObjects = DEFAULT_MAXIMUM_OBJECTS;
    /** Maximalni pocet objektu prohledavanych podle cesty */
    private int maximumPathObjects = DEFAULT_MAXIMUM_PATH_OBJECTS;

    @Autowired
    private DatabaseHolder databaseService;

    @Autowired
    private SuperRootHandler superRootHandler;

    @Autowired
    private TechnicalAttributesHolder technicalAttributesHolder;

    @Override
    public List<Node> findItems(final String pattern, final RevisionInterval revisionInterval) {
        return databaseService.runInTransaction(TransactionLevel.READ, new TransactionCallback<List<Node>>() {
            @Override
            public List<Node> callMe(TitanTransaction transaction) {

                List<Node> resultList = new ArrayList<>();

                String escapedRegexp = escape(pattern);

                // nejprve presna shoda
                String escapedPattern = escapedRegexp;
                fulltextSearch(transaction, escapedPattern, revisionInterval, resultList);
                
                if (resultList.size() < maximumObjects) {
                    // pote prefix
                    escapedPattern = escapedRegexp + ".*";
                    fulltextSearch(transaction, escapedPattern, revisionInterval, resultList);
                }

                if (resultList.size() < maximumObjects) {
                    // a  nakonec podretezec
                    escapedPattern = ".*" + escapedRegexp + ".*";
                    fulltextSearch(transaction, escapedPattern, revisionInterval, resultList);
                }

                return resultList;
            }

            @Override
            public String getModuleName() {
                return FlowController.MODULE_NAME;
            }
        });

    }

    /**
     * Oescapuje všechny znaky pro lucence index.
     * @param s řetězec k oescopování
     * @return oescapovaný řetězec
     */
    private String escape(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            // These characters are part of the query syntax and must be escaped
            if (c == '\\' || c == '+' || c == '-' || c == '!' || c == '(' || c == ')' || c == ':' || c == '^'
                    || c == '[' || c == ']' || c == '\"' || c == '{' || c == '}' || c == '~' || c == '*' || c == '?'
                    || c == '|' || c == '&' || c == '.' || c == '$' || c == '/' || c == '<' || c == '>' || c == '\'') {
                sb.append('\\');
            }
            sb.append(c);
        }
        return sb.toString();
    }

    /**
     * Nalezne objekty odpovidající danému patternu.
     * @param transaction transakce do db
     * @param escapedPattern hledaný vzor
     * @param limit Maximalni pocet objektu ve vysledku
     * @return iterátor na objekty
     */
    private Iterable<Result<Vertex>> findObjects(TitanTransaction transaction, String escapedPattern, int limit) {
        return transaction.indexQuery("search", "v." + NodeProperty.NODE_NAME.t() + ":(/" + escapedPattern + "/)")
                .limit(limit).vertices();
    }

    @Override
    public List<Node> getNodesOnPath(final List<Long> path, final RevisionInterval revisionInterval) {

        return databaseService.runInTransaction(TransactionLevel.READ, new TransactionCallback<List<Node>>() {
            @Override
            public List<Node> callMe(TitanTransaction transaction) {
                Vertex vertex = ViewerOperation.findVertexByPath(superRootHandler.getRoot(transaction), path,
                        revisionInterval);

                List<Node> resultList = new ArrayList<>();
                if (vertex != null) {
                    boolean isVertexResource = VertexType.getType(vertex) == VertexType.RESOURCE;

                    List<Vertex> children = GraphOperation.getDirectChildren(vertex, revisionInterval);
                    for (Vertex child : children) {
                        // pokud je parent resource, chceme pouze potomky bez otce
                        if (!isVertexResource || GraphOperation.getParent(child) == null) {
                            resultList.add(createNodeFromVertex(child, revisionInterval));
                        }
                    }
                }
                Collections.sort(resultList, comparatorNaturalOrder);
                return resultList;
            }

            @Override
            public String getModuleName() {
                return FlowController.MODULE_NAME;
            }
        });
    }

    @Override
    public List<Node> getNodes(final List<String> ids, final RevisionInterval revisionInterval) {
        return databaseService.runInTransaction(TransactionLevel.READ, new TransactionCallback<List<Node>>() {
            @Override
            public String getModuleName() {
                return FlowController.MODULE_NAME;
            }

            @Override
            public List<Node> callMe(TitanTransaction transaction) {
                List<Node> resultNodes = new ArrayList<>();

                for (String stringId : ids) {
                    Long id = null;
                    try {
                        id = Long.valueOf(stringId);
                    } catch (NumberFormatException e) {
                        LOGGER.warn("The id " + stringId + " cannot be parsed.");
                    }

                    if (id != null) {
                        Vertex vertex = transaction.getVertex(id);
                        if (vertex != null && VertexType.getType(vertex) == VertexType.NODE) {
                            resultNodes.add(createNodeFromVertex(vertex, revisionInterval));
                        } else {
                            LOGGER.warn("Vertex with id " + id + " does not exist.");
                        }
                    }
                }

                return resultNodes;
            }
        });
    }

    @Override
    public List<Node> findItemsByPath(final String[] path, final RevisionInterval revisionInterval) {

        return databaseService.runInTransaction(TransactionLevel.READ, new TransactionCallback<List<Node>>() {
            @Override
            public String getModuleName() {
                return FlowController.MODULE_NAME;
            }

            @Override
            public List<Node> callMe(TitanTransaction transaction) {
                List<Node> resultNodes = new ArrayList<>();

                String escapedPattern = "\"" + escape(path[path.length - 1]) + "\"";
                Iterable<Result<Vertex>> vertices = findObjects(transaction, escapedPattern, maximumPathObjects);
                for (Result<Vertex> foundObject : vertices) {
                    Vertex vertex = foundObject.getElement();
                    if (RevisionUtils.isVertexInRevisionInterval(vertex, revisionInterval)
                            && isParentsCorrect(vertex, path)) {
                        resultNodes.add(createNodeFromVertex(vertex, revisionInterval));
                    }
                }

                return resultNodes;
            }
        });
    }

    /**
     * Ověří, že daný uzel má odpovídající hierarchii předků jako daná cesta.
     * @param vertex vrchol k ověření
     * @param path cesta, vůči níž se ověřuje
     * @return true, jestli vrchol odpovídá hierarchické cestě
     */
    private boolean isParentsCorrect(final Vertex vertex, final String[] path) {
        Vertex actualVertex = vertex;

        for (int index = path.length - 1; index >= 0; index--) {
            if (actualVertex == null) {
                return false;
            }

            if (!GraphOperation.getName(actualVertex).equals(path[index])) {
                return false;
            }

            actualVertex = GraphOperation.getParent(actualVertex);
        }
        return true;
    }

    /**
     * Factory metoda pro vytvoření {@link Node} z {@link Vertex}.
     * @param vertex zdrojový vertex
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return vytvořený node s daty
     */
    private Node createNodeFromVertex(Vertex vertex, RevisionInterval revisionInterval) {
        Node node = new Node();

        node.setId((Long) vertex.getId());
        node.setText(ViewerOperation.getLabel(vertex));
        node.setLayer(ViewerOperation.getLayer(vertex));
        node.setTechnology(ViewerOperation.getTechnology(vertex));
        node.setTechnologyType(ViewerOperation.getTechnologyType(vertex));
        node.setType(ViewerOperation.getType(vertex));
        node.setPath(ViewerOperation.getPath(vertex));
        node.setIdsPath(getIdsPath(vertex));

        Map<String, List<Object>> allNodeAttributes = GraphOperation.getAllNodeAttributes(vertex, revisionInterval);
        Iterator<String> iterator = allNodeAttributes.keySet().iterator();
        while (iterator.hasNext()) {
            if (technicalAttributesHolder.isNodeAttributeTechnical(iterator.next())) {
                iterator.remove();
            }
        }
        node.setAttributes(allNodeAttributes);

        List<Vertex> descendentVertices = GraphOperation.getDirectChildren(vertex, revisionInterval);

        boolean hasChild = false;
        boolean isVertexResource = VertexType.getType(vertex) == VertexType.RESOURCE;
        for (Vertex child : descendentVertices) {
            // pokud je parent resource, chceme pouze potomky bez otce
            if (!isVertexResource || GraphOperation.getParent(child) == null) {
                hasChild = true;
                break;
            }
        }
        node.setChildren(hasChild);

        List<Node> empty = Collections.emptyList();
        node.setDescendants(empty);
        
        node.setNodeMapping(ViewerOperation.getNodeMapping(vertex, revisionInterval));

        return node;

    }

    /**
     * Vrátí cestu k danému vertexu jako seznam ID předků.
     * @param vertex vertex, pro který se hledá path
     * @return seznam ID předků, odpovídající cestě v grafu
     */
    private List<Long> getIdsPath(Vertex vertex) {
        List<Long> path = new ArrayList<>();

        if (vertex.getProperty(NodeProperty.RESOURCE_NAME.t()) == null) {
            Vertex resource = GraphOperation.getTopParentResource(vertex);
            path.add(resource != null ? (Long) resource.getId() : null);
            List<Vertex> parents = GraphOperation.getAllParent(vertex);
            for (int i = parents.size() - 1; i >= 0; i--) {
                path.add((Long) parents.get(i).getId());
            }
        }

        return path;
    }
    
    /**
     * Provede fulltextove hledani uzlu podle daneho vyrazu. Vysledek seradi podle cesty a prida na konec daneho seznamu.
     * Celkovy pocet uzlu ve vyslednem seznamu je omezen hodnotou {@link #maximumObjects}.
     * 
     * @param transaction DB transakce 
     * @param escapedPattern Hledany regularni vyraz s escape sekvencemi 
     * @param revisionInterval Interval revizi hledanych uzlu
     * @param resultList Seznam, ke kteremu se ma pridat vysledek.
     */
    private void fulltextSearch(TitanTransaction transaction, String escapedPattern, RevisionInterval revisionInterval, List<Node> resultList) {
        List<Node> partialResultList = new ArrayList<>();
        // Provedeme dotaz do databaze pro fulltextove hledani
        Iterable<Result<Vertex>> vertices = findObjects(transaction, escapedPattern, maximumObjects);
        for (Result<Vertex> v : vertices) {
            if (RevisionUtils.isVertexInRevisionInterval(v.getElement(), revisionInterval)) {
                // Uzel musi byt v danem intervalu revizi
                Node node = createNodeFromVertex(v.getElement(), revisionInterval);
                if (!resultList.contains(node)) {
                    // Pokud uz je uzel v seznamu, znovu ho nepridavame
                    partialResultList.add(node);
                    if (resultList.size() + partialResultList.size() == maximumObjects) {
                        // Pocet nalezenych uzlu dosahl limitu => koncime
                        break;
                    }
                }
            }
        }
        // Seradime nove ziskane uzly
        Collections.sort(partialResultList, comparatorPathOrder);
        // a pridame je do vysledneho seznamu
        resultList.addAll(partialResultList);
    }

    /**
     * @param databaseService služba pro poskytování transakce
     */
    public void setDatabaseService(DatabaseHolder databaseService) {
        this.databaseService = databaseService;
    }

    /**
     * @param superRootHandler držák na super root grafu v db
     */
    public void setSuperRootHandler(SuperRootHandler superRootHandler) {
        this.superRootHandler = superRootHandler;
    }

    /**
     * @param technicalAttributesHolder služba na zjištění technických atributů
     */
    public void setTechnicalAttributesHolder(TechnicalAttributesHolder technicalAttributesHolder) {
        this.technicalAttributesHolder = technicalAttributesHolder;
    }

    /**
     * @return Počet maximálně napovídaných objektů.
     */
    public int getMaximumObjects() {
        return maximumObjects;
    }

    /**
     * @param maximumObjects Počet maximálně napovídaných objektů. 
     */
    public void setMaximumObjects(int maximumObjects) {
        this.maximumObjects = maximumObjects;
    }

    /**
     * @return Maximalni pocet objektu prohledavanych podle cesty
     */
    public int getMaximumPathObjects() {
        return maximumPathObjects;
    }

    /**
     * @param maximumPathObjects Maximalni pocet objektu prohledavanych podle cesty
     */
    public void setMaximumPathObjects(int maximumPathObjects) {
        this.maximumPathObjects = maximumPathObjects;
    }
    
}
