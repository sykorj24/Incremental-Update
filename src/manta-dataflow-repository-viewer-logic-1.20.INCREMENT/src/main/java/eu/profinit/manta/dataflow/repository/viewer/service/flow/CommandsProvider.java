package eu.profinit.manta.dataflow.repository.viewer.service.flow;

import java.util.Set;

import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.Command;

/**
 * Třída držící všechny instance commandů dostupných pro zbytek aplikace. 
 * @author tfechtner
 *
 */
public class CommandsProvider {
    /** Množina commandů.*/
    private Set<Command<?, ?>> commandsSet;

    /**
     * Vrátí command odpovídající dané třídě.
     * @param command třída hledaného command
     * @return daný command nebo null
     * 
     * @param <T> typ commandu
     */
    @SuppressWarnings("unchecked")
    public <T> T getCommand(Class<T> command) {
        for (Command<?, ?> c : commandsSet) {
            if (command.isAssignableFrom(c.getClass())) {
                return (T) c;
            }
        }

        throw new IllegalArgumentException("The command " + command.getCanonicalName() + " is not supported.");
    }

    /**
     * @param commandsSet Množina commandů.
     */
    public void setCommandsSet(Set<Command<?, ?>> commandsSet) {
        this.commandsSet = commandsSet;
    }
}
