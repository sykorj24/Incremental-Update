package eu.profinit.manta.dataflow.repository.viewer.model.filter;

import java.util.List;
import java.util.Map;


/**
 * Rozhraní definující poskytovalete filtrů resourců.
 * @author tfechtner
 *
 */
public interface ResourceFilterProvider {
    /**
     * Pokusí se získat filtr s daným jménem.
     * @param filterId id hledaného filtru
     * @return datová struktura hledaného filtru, {@link ResourceFilter#DEFAULT_FILTER} pokud nebyl nalezen
     */
    ResourceFilter getFilter(Integer filterId);

    /**
     * Vytvoří model o filtrech pro view.
     * @return seznam jednotlivých filtrů, kde pro každý filtr je mapa informací o něm
     */
    List<Map<String, Object>> getFiltersModel();   
}
