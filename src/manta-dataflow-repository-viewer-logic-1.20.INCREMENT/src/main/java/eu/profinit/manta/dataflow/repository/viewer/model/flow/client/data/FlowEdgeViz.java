package eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowEdge;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Datová immutable třída pro definici hrany pro klienta v rámci vizualizace. <br>
 * @author tfechtner
 *
 */
public class FlowEdgeViz {
    /** Id zdrojového uzlu hrany. */
    private final Long source;
    /** Id koncového uzlu hrany. */
    private final Long target;
    /** Výsledný label hrany. */
    private final EdgeLabel label;
    /** Atributy hrany. */
    private final Map<String, Object> attrs;
    /** Stav hrany v revizi. */
    private final RevisionState revState;

    /**
     * @param source Id zdrojového uzlu hrany
     * @param target Id koncového uzlu hrany
     * @param label Výsledný label hrany
     * @param attrs Atributy hrany
     * @param revState stav hrany v revizi
     */
    public FlowEdgeViz(Long source, Long target, EdgeLabel label, Map<String, Object> attrs, RevisionState revState) {
        super();
        this.source = source;
        this.target = target;
        this.label = label;
        this.attrs = new HashMap<>(attrs);
        this.revState = revState;
    }

    /**
     * @param source zdrojový uzlu hrany
     * @param target koncový uzlu hrany
     * @param label label hrany
     * @param revState stav hrany v revizi
     */
    public FlowEdgeViz(Vertex source, Vertex target, EdgeLabel label, RevisionState revState) {
        super();
        this.source = (Long) source.getId();
        this.target = (Long) target.getId();
        this.label = label;
        this.attrs = Collections.emptyMap();
        this.revState = revState;
    }

    /**
     * @param flowEdge originální univerzální reprezentace hrany
     * @param comparingInterval interval revizí, ke kterému se instance bude vztahovat
     */
    public FlowEdgeViz(FlowEdge flowEdge, RevisionInterval comparingInterval) {
        this.source = flowEdge.getSource();
        this.target = flowEdge.getTarget();
        this.label = flowEdge.getLabel();
        this.attrs = new HashMap<>(flowEdge.getAttrs());
        this.revState = RevisionState.resolve(flowEdge.getRevInterval(), comparingInterval);
    }

    /**
     * @return Id zdrojového uzlu hrany
     */
    public Long getSource() {
        return source;
    }

    /**
     * @return Id koncového uzlu hrany
     */
    public Long getTarget() {
        return target;
    }

    /**
     * @return Výsledný label hrany
     */
    public EdgeLabel getLabel() {
        return label;
    }

    /**
     * @return Atributy hrany
     */
    public Map<String, Object> getAttrs() {
        return attrs;
    }

    /**
     * @return Stav hrany v revizi.
     */
    public RevisionState getRevState() {
        return revState;
    }

    @Override
    public String toString() {
        return source + "-" + label + "-" + target;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(source);
        builder.append(target);
        builder.append(label);
        return builder.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        FlowEdgeViz other = (FlowEdgeViz) obj;
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(source, other.source);
        builder.append(target, other.target);
        builder.append(label, other.label);
        return builder.isEquals();
    }

}
