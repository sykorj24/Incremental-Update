package eu.profinit.manta.dataflow.repository.viewer.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller pro kontrolu přihlášení ve vizualizaci.
 * @author tfechtner
 *
 */
@Controller
@RequestMapping("/viewer")
public class LoginCheckController {

    @RequestMapping(value = "check", method = RequestMethod.GET)
    public ResponseEntity<String> checkLogin() {
        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }
}
