package eu.profinit.manta.dataflow.repository.viewer.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterGroup;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterGroupProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterType;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionModel;
import eu.profinit.manta.dataflow.repository.connector.titan.service.jackson.CustomObjectMapper;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.viewer.exception.CorruptedRequestException;
import eu.profinit.manta.dataflow.repository.viewer.exception.FlowFormException;
import eu.profinit.manta.dataflow.repository.viewer.exception.NoCommitedDataException;
import eu.profinit.manta.dataflow.repository.viewer.model.FlowStateException;
import eu.profinit.manta.dataflow.repository.viewer.model.Job;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateManager;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.RefViewUsageStats;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.RefViewUsageStats.FollowSize;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.DataInfo;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Resource;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message.Severity;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ComponentsRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ExportRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.FollowRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.HiddenByFiltersRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.InitRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ProcessLinkRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ResourceListRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ShowStmtInScriptRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.BaseResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ComponentsResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.FollowResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.GenerateLinkResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.HiddenByFiltersResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.InitResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ProcessLinkResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ResourceListResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ShowStmtInScriptResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.index.WelcomeFormModel;
import eu.profinit.manta.dataflow.repository.viewer.security.ViewerRoles;
import eu.profinit.manta.dataflow.repository.viewer.service.DataSizeChecker;
import eu.profinit.manta.dataflow.repository.viewer.service.filter.HorizontalFilterHelper;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.CommandsProvider;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.GraphService;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.RequestAccessSecurityService;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.ComponentsCommand;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.ExportCommand;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.FollowCommand;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.GenerateLinkCommand;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.HiddenByFiltersCommand;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.InitReferenceViewCommand;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.ProcessLinkCommand;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.ResourceListCommand;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.ShowStmtInScriptCommand;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;
import eu.profinit.manta.platform.web.core.security.SecurityHelper;

/**
 * Controller, ktery se stara o JSON dotazy behem zobrazeni vizualizace.
 * Beana má session scope, definováno v konfiguraci.
 * 
 * @author Matyas Krutsky <matyas.krutsky@profinit.eu>
 * @author tfechtner
 */
@Controller
@RequestMapping("viewer/dataflow")
public class FlowController {
    /** Název akce pro usage stats o nutnosti kontrakce follow odpovědi. */
    private static final String FLOW_VIS_CONTRACTION_EVENT = "flow_vis_contractionEvent";
    /** Název akce pro usage stats pro init view. */
    private static final String FLOW_VIS_REF_VIEW = "flow_vis_refView";
    /** Název akce pro usage stats pro export png. */
    private static final String FLOW_VIS_EXPORT_PNG = "flow_vis_exportPng";
    /** Název akce pro usage stats pro přímý export. */
    private static final String FLOW_VIS_EXPORT_DIRECT = "flow_vis_exportDirect";
    /** Chybová hláška, co sep ošle uživateli, když dojde k neočekávané chybě. */
    private static final String UNEXPECTED_ERROR = "An unexpected error occurred. "
            + "Please, contact the application administrator.";
    /** Hláška, když se přes všechno zmenšování follow nepodařilo najít takové nastavení, že data nejsou moc velká. */
    private static final String ERROR_NO_DATA = "It is not possible to find a small enough response. "
            + "Please, try another action.";
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(FlowController.class);
    /** Název vizualizačního modulu. */
    public static final String MODULE_NAME = "mr_viewer";
    /** Mapa stavů flow specifické pro jendotlivé konverzace v dané session.*/
    private Map<Integer, FlowStateManager> flowStateMap = new HashMap<>();
    /** Mapa zámků pro jednotlivé session. */
    private ConcurrentHashMap<Integer, Object> lockMap = new ConcurrentHashMap<>();
    /** Index dalšího flow state do mapy. */
    private AtomicInteger flowStateIndex = new AtomicInteger(0);
    /** Adresa stránky volané při chybě o iniciální follow. */
    private String initFollowErrorPage;
    /** Maximální hloubka iniciálního follow. */
    private int maximalDepth;

    /** Maximalni pocet zapamatovanych undo stavu. */
    @Autowired
    @Qualifier("undoCapacity")
    private Integer undoCapacity;
    /** Sluzba pro praci s grafem.  */
    @Autowired
    private GraphService graphService;
    /** Provider poskytující commandy pro jednotlivé requesty. */
    @Autowired
    private CommandsProvider commandsProvider;
    /** Provider poskytující seznam resource filtrů. */
    @Autowired
    private HorizontalFilterProvider horizontalFilterProvider;
    /** Provider poskytující seznam skupin horizontálních filtrů. */
    @Autowired
    private HorizontalFilterGroupProvider horizontalFilterGroupProvider;
    /** Helper poskytující metody pro práci s horizontálními filtry a jejich skupinami. */
    @Autowired
    private HorizontalFilterHelper horizontalFilterHelper;
    /** Služba ověřující korektnost dotazů na flow z pohledu bezpečnosti. */
    @Autowired(required = false)
    private RequestAccessSecurityService securityService;
    /** Služba pro ukládání statsitik použití. */
    @Autowired
    private UsageStatsCollector usageStatsCollector;
    /** Kontroler pro nejrůznější záklopky. */
    @Autowired
    private DataSizeChecker dataSizeChecker;

    /**
     * Vstup do vizualizacniho zobrazeni.
     * @param model model pro view
     * @param form data z formuláře
     * @param redirectAttributes atributy posílané při přesměrování
     * @return nazev ftl šablony
     */
    @Secured(ViewerRoles.VIEWER_DATAFLOW)
    @RequestMapping(method = RequestMethod.GET)
    public String createFlowFromForm(ModelMap model, @ModelAttribute WelcomeFormModel form,
            RedirectAttributes redirectAttributes) {
        if (securityService == null || securityService.isCorrect(form)) {
            try {
                RefViewUsageStats refViewUsageStats = new RefViewUsageStats();
                return createFollow(model, form, redirectAttributes, refViewUsageStats);
            } catch (Exception e) {
                throw new FlowFormException(e);
            }
        } else {
            throw new CorruptedRequestException("The request was corrupted.");
        }
    }

    /**
     * Spustí follow z permalinku. 
     * @param model model pro view
     * @param request data z odkazu
     * @param redirectAttributes atributy posílané při přesměrování
     * @return nazev ftl šablony
     */
    @Secured(ViewerRoles.VIEWER_DATAFLOW)
    @RequestMapping(value = "/link", method = RequestMethod.GET)
    public String createFlowFromLink(ModelMap model, @ModelAttribute ProcessLinkRequest request,
            RedirectAttributes redirectAttributes) {
        if (securityService == null || securityService.isCorrect(request)) {
            try {
                RefViewUsageStats refViewUsageStats = new RefViewUsageStats();
                refViewUsageStats.setFromPermaLink(true);

                request.setRootId(graphService.getRootId());
                ProcessLinkCommand command = commandsProvider.getCommand(ProcessLinkCommand.class);
                ProcessLinkResponse response = graphService.executeJob(new Job<>(command, request, null));
                return createFollow(model, response.getFormModel(), redirectAttributes, refViewUsageStats);
            } catch (Exception e) {
                throw new FlowFormException(e);
            }
        } else {
            throw new CorruptedRequestException("The request was corrupted.");
        }
    }

    /**
     * Vytvoří flow.
     * @param model model pro view
     * @param form data z formuláře
     * @param redirectAttributes atributy posílané při přesměrování
     * @param refViewUsageStats informace o operaci pro uložení statistik použití
     * @return nazev ftl šablony
     * @throws FlowStateException  neexistující flowState
     */
    private String createFollow(ModelMap model, WelcomeFormModel form, RedirectAttributes redirectAttributes,
            RefViewUsageStats refViewUsageStats) throws FlowStateException {

        if (form.getDepth() > getMaximalDepth()) {
            redirectAttributes.addFlashAttribute("errorMessage",
                    new Message(Severity.ERROR, "The maximal depth of initial follow is " + getMaximalDepth() + "."));
            redirectAttributes.addFlashAttribute("formData", form);
            return "redirect:" + initFollowErrorPage;
        }

        if (form.getOlderRevision() != 0 && form.getOlderRevision() >= form.getRevision()) {
            redirectAttributes.addFlashAttribute("errorMessage",
                    new Message(Severity.ERROR, "The other revision must be older than the main one."));
            redirectAttributes.addFlashAttribute("formData", form);
            return "redirect:" + initFollowErrorPage;
        }

        // nový session index a zámek pro tuto konverzaci
        Integer sessionIndex = flowStateIndex.getAndIncrement();
        Object lock = initLock(sessionIndex);

        synchronized (lock) {
            // poslat data z formuláře pro možný restart flow či návrat zpět
            model.put("formData", form);
            model.put("sessionIndex", sessionIndex);
            RevisionModel revisionModel = graphService.getSpecificRevisionModel(form.getRevision());
            model.put("revisionModel", revisionModel);
            if (form.getOlderRevision() != 0) {
                RevisionModel olderRevisionModel = graphService.getSpecificRevisionModel(form.getOlderRevision());
                model.put("olderRevisionModel", olderRevisionModel);
            }

            List<Message> messagesForClient = new ArrayList<>();

            // inicializovat flow state včetně referenčního view
            List<Long> potentionalStartNodes = new ArrayList<>();
            for (String stringId : form.getSelectedItems()) {
                try {
                    potentionalStartNodes.add(Long.parseLong(stringId));
                } catch (NumberFormatException e) {
                    LOGGER.warn("Unparsable id of start node.", e);
                }
            }

            refViewUsageStats.updateForm(form);
            graphService.fillStartNodeTypes(potentionalStartNodes, refViewUsageStats);

            long refViewStartTime = System.currentTimeMillis();
            InitResponse initFlowResponse = initFlowState(potentionalStartNodes, form, sessionIndex,
                    dataSizeChecker.getMaximumStartNodes());
            if (!initFlowResponse.isOk()) {
                if (initFlowResponse.getMessages().size() > 0) {
                    Message message = initFlowResponse.getMessages().get(0);
                    redirectAttributes.addFlashAttribute("errorMessage", message);
                    refViewUsageStats.setRefViewError(message.getText());
                }
                redirectAttributes.addFlashAttribute("formData", form);

                usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_VIS_REF_VIEW, refViewUsageStats);
                return "redirect:" + initFollowErrorPage;
            }
            messagesForClient.addAll(initFlowResponse.getMessages());

            // načíst aktuální flowState
            long flowStateCursor = getFlowStateManager(sessionIndex).getCursor();
            FlowStateViewer flowState = getFlowState(sessionIndex, flowStateCursor);

            // statistiky o ref view
            refViewUsageStats.setRefViewSize(flowState.getReferenceNodeIds().size());
            refViewUsageStats.setRefViewTime(System.currentTimeMillis() - refViewStartTime);
            refViewUsageStats.setFlowStateGuid(flowState.getGuid());

            Set<String> activeFilters = calcActiveFilters(form);

            model.put("resourceList", createResourceListModel(loadResourceList(sessionIndex)));

            // poslat horizontal filtry
            List<Map<String, Object>> resourceFiltersListModel = horizontalFilterHelper
                    .generateModelForFilters(HorizontalFilterType.RESOURCE, activeFilters, horizontalFilterProvider);
            model.put("resourceFilterList", resourceFiltersListModel);

            List<Map<String, Object>> customFilterListModel = horizontalFilterHelper
                    .generateModelForFilters(HorizontalFilterType.CUSTOM, activeFilters, horizontalFilterProvider);
            model.put("customFilterList", customFilterListModel);

            // poslat množinu startovních uzlů
            List<Long> startNodes = new ArrayList<>(flowState.getTotalStartNodes());
            model.put("startingComponents", startNodes);

            // provést follow
            FollowRequest request = new FollowRequest();
            request.setComponentIds(startNodes);
            request.setActiveFilters(activeFilters);
            request.setLevel(form.getLevel());
            request.setRadius(form.getDepth());
            request.setSessionIndex(sessionIndex);
            request.setServerState(flowStateCursor);

            FollowResponse followResponse = doFollow(request, refViewUsageStats);
            if (FollowResponse.NO_START_NODES.equals(followResponse)) {
                redirectAttributes.addFlashAttribute("errorMessage", followResponse.getMessages().get(0));
                redirectAttributes.addFlashAttribute("formData", form);
                return "redirect:" + initFollowErrorPage;
            }
            messagesForClient.addAll(followResponse.getMessages());

            // poslat případné zprávy
            if (!messagesForClient.isEmpty()) {
                model.put("messages", messagesForClient);
            }

            // uložit statistiky o follow
            DataInfo dataInfo = followResponse.getDataInfo();
            if (dataInfo != null) {
                refViewUsageStats.setInitFollowSize(new FollowSize(dataInfo.getFullComponents(),
                        dataInfo.getProxyComponents(), dataInfo.getEdges()));
            }
            usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_VIS_REF_VIEW, refViewUsageStats);

            // poslat data z úvodního follow
            CustomObjectMapper mapper = new CustomObjectMapper();
            try {
                model.put("initialData", mapper.writeValueAsString(followResponse).replace("\"", "&quot;"));
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }

            model.put("undoCapacity", this.undoCapacity);
        }
        return "manta-dataflow-repository-viewer/dataflow";
    }

    /**
     * Vytvoří model pro resource nastavení.
     * @param resourceList seznam resourců
     * @return model pro view
     */
    private List<Map<String, Object>> createResourceListModel(List<Resource> resourceList) {
        List<Map<String, Object>> model = new ArrayList<>();

        for (Resource resource : resourceList) {
            Map<String, Object> resourceModel = new HashMap<>();
            resourceModel.put("id", resource.getId());
            resourceModel.put("name", resource.getName());
            resourceModel.put("layer", resource.getLayer());
            model.add(resourceModel);
        }

        return model;
    }

    /**
     * Získá množinu id aktivních filtrů.
     * @param form request z formuláře
     * @return množina id filtrů, nikdy null
     */
    private Set<String> calcActiveFilters(WelcomeFormModel form) {
        Set<String> activeFilters;
        HorizontalFilterGroup group = horizontalFilterGroupProvider.findGroup(form.getFilter());
        if (group != null) {
            activeFilters = new HashSet<>(group.getFilterIds());
        } else {
            activeFilters = Collections.emptySet();
        }
        return activeFilters;
    }

    /**
     * Načte seznam resource pro danou session.
     * @param sessionIndex index session
     * @return Seznam resource
     * @throws FlowStateException chyba ve flow state
     */
    protected List<Resource> loadResourceList(Integer sessionIndex) throws FlowStateException {
        ResourceListRequest resourceListRequest = new ResourceListRequest();
        resourceListRequest.setRootId(graphService.getRootId());
        ResourceListCommand commandResourceList = commandsProvider.getCommand(ResourceListCommand.class);
        ResourceListResponse resourceListResponse = graphService
                .executeJob(new Job<>(commandResourceList, resourceListRequest, getCurrentFlowState(sessionIndex)));
        List<Resource> resourceList = resourceListResponse.getResourceList();
        return resourceList;
    }

    /**
     * Provede follow, přičemž kontroluje výstup a případně se pokusí zmenšit nastavení, 
     * aby data nebyla příliš velka.
     * @param request request na follow
     * @param refViewUsageStats informace o operaci pro uložení statistik použití
     * @return response z výsledného follow
     * @throws FlowStateException neexistující flowState
     */
    private FollowResponse doFollow(FollowRequest request, RefViewUsageStats refViewUsageStats)
            throws FlowStateException {
        FlowStateViewer tempFlowState;
        boolean isFirstTry = true;

        String message = null;
        Severity severity = Severity.WARNING;

        long initFollowStartTime = System.currentTimeMillis();

        FlowStateViewer originalFlowState = getFlowState(request.getSessionIndex(), request.getServerState());
        FollowCommand commandFollow = commandsProvider.getCommand(FollowCommand.class);
        FollowResponse followResponse;

        // zkoušet follow dokud nebudou data dostatečně malá
        do {
            tempFlowState = new FlowStateViewer(originalFlowState);

            // zmenšit rozsah, pokud to není první pokus
            if (!isFirstTry) {
                message = request.lowerSize();
                if (message == null) {
                    // už žádné další zmenšení a stále špatně -> zabít cyklus
                    followResponse = new FollowResponse();
                    message = ERROR_NO_DATA;
                    severity = Severity.ERROR;
                    break;
                }
            } else {
                isFirstTry = false;
            }
            followResponse = graphService.executeJob(new Job<>(commandFollow, request, tempFlowState));
        } while (!followResponse.getDataInfo().isDataOk());
        setFlowState(request.getSessionIndex(), tempFlowState);

        if (refViewUsageStats != null) {
            refViewUsageStats.setInitFollowTime(System.currentTimeMillis() - initFollowStartTime);
        }

        // případná zpráva o zmenšení vstupu
        if (message != null) {
            followResponse.addMessage(new Message(severity, message));
            if (refViewUsageStats != null) {
                refViewUsageStats.setScopeMessage(message);
            } else {
                usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_VIS_CONTRACTION_EVENT,
                        Collections.singletonMap("message", message));
            }
        }
        followResponse.setServerState(getFlowStateManager(request.getSessionIndex()).getCursor());
        return followResponse;
    }

    /**
     * Najde {@link FlowStateManager} pro dany index.
     * Pokud je index null, vezme se jako index číslo 0. <br>
     * Pokud pod daným indexem neexistuje flow state manager, vyhodí se výjimka.
     * @param index index, pro který se hledá flow state
     * @return flow state manager pro dany index
     * @throws FlowStateException
     */
    private FlowStateManager getFlowStateManager(Integer index) throws FlowStateException {
        FlowStateManager flowStateManager;
        if (index != null) {
            flowStateManager = flowStateMap.get(index);
        } else {
            flowStateManager = flowStateMap.get(0);
        }

        if (flowStateManager != null) {
            return flowStateManager;
        } else {
            LOGGER.warn("Failed to load flowStateManager for the index " + index + ".");
            throw new FlowStateException("Session has probably timed out. Please try to refresh the page.");
        }
    }

    /**
     * Získá flow state pro daný index a server state (timestamp). <br>
     * Pokud je index null, vezme se jako index číslo 0. <br>
     * Pokud pod daným indexem neexistuje flow state, nebo flow state manager
     * neobsahuje flow state pro dany timestamp, vyhodí se výjimka.
     * @param index index, pro který se hledá flow state
     * @param serverState stav serveru, ktery chci ziskat
     * @return flow state pro daný index a server state
     * @throws FlowStateException pokud neexistuje flow state pro daný index a server state
     */
    private FlowStateViewer getFlowState(Integer index, Long serverState) throws FlowStateException {
        FlowStateManager flowStateManager = getFlowStateManager(index);

        if (flowStateManager != null && flowStateManager.isValidFlowState(serverState)) {
            return flowStateManager.getFlowStateAndSetCursor(serverState);
        } else {
            LOGGER.warn("Failed to load flowState for the index " + index + ".");
            throw new FlowStateException("Session has probably timed out. Please try to refresh the page.");
        }
    }

    /**
     * Získá flow state pro daný index a aktuálně nastaveý server state ve flow state mananger. <br>
     * Pokud je index null, vezme se jako index číslo 0. <br>
     * Pokud pod daným indexem neexistuje flow state, nebo flow state manager nema nastaveny zadny
     * aktualni server state, vyhodi se vyjimka
     * @param index index, pro který se hledá flow state
     * @return flow state pro daný index
     * @throws FlowStateException pokud neexistuje flow state pro daný index
     */
    protected FlowStateViewer getCurrentFlowState(Integer index) throws FlowStateException {
        FlowStateManager flowStateManager = getFlowStateManager(index);

        if (flowStateManager != null && flowStateManager.hasValidCurrentState()) {
            return flowStateManager.getCurrentFlowState();
        } else {
            LOGGER.warn("Failed to load flowState for the index " + index + ".");
            throw new FlowStateException("Session has probably timed out. Please try to refresh the page.");
        }
    }

    /**
     * Nastaví pro daný index flow state a vlozi ho do fronty flow state pod aktualnim timestamp.
     * Tento flow state je zaroven nastaven jako aktualni na flow state manager pro dany index.
     * @param index index, pro který se instance nastavuje
     * @param flowState nastavovaná instance flow state
     */
    protected void setFlowState(Integer index, FlowStateViewer flowState) {
        if (index != null && flowState != null) {
            FlowStateManager flowStateManager;
            if (!flowStateMap.containsKey(index)) {
                flowStateManager = new FlowStateManager(undoCapacity);
                flowStateMap.put(index, flowStateManager);
            } else {
                flowStateManager = flowStateMap.get(index);
            }
            Long timestamp = new Date().getTime();
            flowStateManager.pushFlowState(timestamp, flowState);
        } else {
            throw new IllegalArgumentException("Index and flow state must not be null.");
        }
    }

    /**
     * Provede follow.
     * @param request Json pozadavek na okoli
     * @return zpracovana odpoved na request typicky ve formatu JSON (dle Accept header)
     * @throws FlowStateException  neexistující flowState
     */
    @Secured(ViewerRoles.VIEWER_DATAFLOW)
    @RequestMapping(value = "/follow", method = RequestMethod.POST)
    @ResponseBody
    public FollowResponse follow(@RequestBody FollowRequest request) throws FlowStateException {
        Object lock = getLock(request.getSessionIndex());
        synchronized (lock) {
            return doFollow(request, null);
        }
    }

    /**
     * Inicializuje stav flow.
     * @param componentIds seznam startovních id komponent
     * @param formModel konfigurační data z volání kontroleru
     * @param sessionIndex index session pro získání příslušného flowState
     * @param maximumStartNodes maximální počet startovnách uzlů
     * @return výsledek spočtení referenčního view
     */
    protected InitResponse initFlowState(List<Long> componentIds, WelcomeFormModel formModel, Integer sessionIndex,
            int maximumStartNodes) {
        InitRequest initRequest = new InitRequest();
        initRequest.setReferencePoints(componentIds);
        initRequest.setDirection(formModel.getDirection());
        initRequest.setFilterEdges(formModel.isFilterEdges());
        initRequest.setFlowLevel(formModel.getLevel());
        initRequest.setMaximumStartNodes(maximumStartNodes);

        RevisionInterval revisionInterval;
        if (formModel.getOlderRevision() != 0) {
            revisionInterval = new RevisionInterval(formModel.getOlderRevision(), formModel.getRevision());
        } else {
            revisionInterval = new RevisionInterval(formModel.getRevision());
        }
        FlowStateViewer flowState = new FlowStateViewer(revisionInterval, horizontalFilterProvider);

        InitReferenceViewCommand command = commandsProvider.getCommand(InitReferenceViewCommand.class);
        InitResponse response = graphService.executeJob(new Job<>(command, initRequest, flowState));
        setFlowState(sessionIndex, flowState);

        return response;
    }

    /**      
     * @param request Json pozadavek na okoli
     * @return zpracovana odpoved na request typicky ve formatu JSON (dle Accept header)
     * @throws FlowStateException neexistující flowState
     */
    @Secured(ViewerRoles.VIEWER_DATAFLOW)
    @RequestMapping(value = "/components", method = RequestMethod.POST)
    @ResponseBody
    public ComponentsResponse components(@RequestBody ComponentsRequest request) throws FlowStateException {
        Object lock = getLock(request.getSessionIndex());
        synchronized (lock) {
            ComponentsCommand command = commandsProvider.getCommand(ComponentsCommand.class);
            FlowStateViewer flowState = getFlowState(request.getSessionIndex(), request.getServerState());
            ComponentsResponse response = graphService.executeJob(new Job<>(command, request, flowState));
            setFlowState(request.getSessionIndex(), flowState);
            response.setServerState(getFlowStateManager(request.getSessionIndex()).getCursor());
            return response;
        }
    }
    
    /**
     * Zjištění skrytí vrcholů pro dané filtry.
     * požadavek requestu
     * @param httpResponse http response pro pro dané filtry
     * @throws FlowStateException neexistující flow state
     */
    @Secured(ViewerRoles.VIEWER_DATAFLOW)
    @RequestMapping(value = "/hiddenByFilters", method = RequestMethod.POST)
    @ResponseBody
    public HiddenByFiltersResponse isHiddenByFilters(@RequestBody HiddenByFiltersRequest request)
            throws FlowStateException {
        Object lock = getLock(request.getSessionIndex());
        synchronized (lock) {
            HiddenByFiltersCommand command = commandsProvider.getCommand(HiddenByFiltersCommand.class);
            FlowStateViewer flowState = getCurrentFlowState(request.getSessionIndex());
            HiddenByFiltersResponse response = graphService.executeJob(new Job<>(command, request, flowState));
            return response;
        }
    }

    /**
     * Export referenčího pohledu.
     * @param request požadavek requestu
     * @param httpResponse http response pro poslání datového toku
     * @throws FlowStateException neexistující flow state
     * @throws IOException chyba při poslání chyby
     */
    @Secured(ViewerRoles.VIEWER_DATAFLOW)
    @RequestMapping(value = "/export-csv", method = RequestMethod.GET)
    public void export(@ModelAttribute ExportRequest request, HttpServletResponse httpResponse)
            throws FlowStateException, IOException {
        Object lock = getLock(request.getSessionIndex());
        synchronized (lock) {
            httpResponse.setContentType("application/octet-stream;charset=utf-8");
            httpResponse.setHeader("Content-Disposition", "attachment; filename=\"export.zip\"");

            ExportCommand command = commandsProvider.getCommand(ExportCommand.class);
            // nastavit output stream http response, aby do něj mohl command zapisovat
            request.setOutputStream(httpResponse.getOutputStream());
            graphService.executeJob(new Job<>(command, request, getCurrentFlowState(request.getSessionIndex())));
        }
    }

    /**
     * Přímý export s vlastním výpočtem referenčního view.
     * @param form dataz formuláře obsahující statistiky pro výpočet view
     * @param httpResponse http response pro nasypání výstupu 
     * @param redirectAttributes přesměrovávací atributů
     * @return případná adresa pro přesměrování při chybě
     * @throws IOException chyba při zápis výstupu
     */
    @Secured(ViewerRoles.VIEWER_DATAFLOW)
    @RequestMapping(value = "/direct-export", method = RequestMethod.GET)
    public String directExport(@ModelAttribute WelcomeFormModel form, HttpServletResponse httpResponse,
            RedirectAttributes redirectAttributes) throws IOException {

        if (securityService == null || securityService.isCorrect(form)) {
            try {
                RefViewUsageStats refViewUsageStats = new RefViewUsageStats();

                if (form.getDepth() > getMaximalDepth()) {
                    redirectAttributes.addFlashAttribute("errorMessage", new Message(Severity.ERROR,
                            "The maximal depth of initial follow is " + getMaximalDepth() + "."));
                    redirectAttributes.addFlashAttribute("formData", form);
                    return "redirect:" + initFollowErrorPage;
                }

                // nový session index a zámek pro tuto konverzaci
                Integer sessionIndex = flowStateIndex.getAndIncrement();
                Object lock = initLock(sessionIndex);

                synchronized (lock) {
                    // inicializovat flow state včetně referenčního view
                    List<Long> potentionalStartNodes = new ArrayList<>();
                    for (String stringId : form.getSelectedItems()) {
                        try {
                            potentionalStartNodes.add(Long.parseLong(stringId));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("Unparsable id of start node.", e);
                        }
                    }

                    refViewUsageStats.updateForm(form);
                    graphService.fillStartNodeTypes(potentionalStartNodes, refViewUsageStats);

                    long refViewStartTime = System.currentTimeMillis();
                    InitResponse initFlowResponse = initFlowState(potentionalStartNodes, form, sessionIndex,
                            dataSizeChecker.getMaximumStartNodesForExport());
                    if (!initFlowResponse.isOk()) {
                        if (initFlowResponse.getMessages().size() > 0) {
                            Message message = initFlowResponse.getMessages().get(0);
                            redirectAttributes.addFlashAttribute("errorMessage", message);
                            refViewUsageStats.setRefViewError(message.getText());
                        }
                        redirectAttributes.addFlashAttribute("formData", form);

                        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_VIS_EXPORT_DIRECT,
                                refViewUsageStats);
                        return "redirect:" + initFollowErrorPage;
                    }
                    // načíst aktuální flowState
                    long flowStateCursor = getFlowStateManager(sessionIndex).getCursor();
                    FlowStateViewer flowState = getFlowState(sessionIndex, flowStateCursor);

                    // určit aktivní filtry resource a nastavit to do ref view
                    flowState.setActiveFilters(calcActiveFilters(form));

                    // statistiky o ref view
                    refViewUsageStats.setRefViewSize(flowState.getReferenceNodeIds().size());
                    refViewUsageStats.setRefViewTime(System.currentTimeMillis() - refViewStartTime);
                    refViewUsageStats.setFlowStateGuid(flowState.getGuid());

                    // uložit statistiky o follow
                    usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_VIS_EXPORT_DIRECT,
                            refViewUsageStats);

                    httpResponse.setContentType("application/octet-stream;charset=utf-8");
                    httpResponse.setHeader("Content-Disposition", "attachment; filename=\"export.zip\"");

                    ExportCommand command = commandsProvider.getCommand(ExportCommand.class);
                    ExportRequest exportRequest = new ExportRequest();
                    exportRequest.setSessionIndex(sessionIndex);
                    exportRequest.setOutputStream(httpResponse.getOutputStream());
                    graphService.executeJob(new Job<>(command, exportRequest, flowState));
                    return "";
                }
            } catch (Exception e) {
                throw new FlowFormException(e);
            }
        } else {
            throw new CorruptedRequestException("The request was corrupted.");
        }
    }

    /**
     * Export zobrazené vizualizace jako PNG.
     * Metoda slouží jen pro uložení statistik, veškerou práci provádí klient. 
     * @param sessionIndex index session pro získání FLowState
     * @return ok response
     * @throws FlowStateException FlowStateException neexistující session id
     */
    @Secured(ViewerRoles.VIEWER_DATAFLOW)
    @RequestMapping(value = "/export-png/{sessionIndex}", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse exportPng(@PathVariable Integer sessionIndex) throws FlowStateException {
        FlowStateViewer flowState = getCurrentFlowState(sessionIndex);
        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_VIS_EXPORT_PNG,
                Collections.singletonMap("flowStateGuid", flowState.getGuid()));
        return BaseResponse.OK;
    }

    /**
     * Generování permalinku. 
     * @param request obsahující data ve formátu vstupního formuláře 
     * @param sessionIndex index session pro získání FLowState
     * @return response obsahující link
     * @throws FlowStateException FlowStateException neexistující session id
     */
    @Secured(ViewerRoles.VIEWER_DATAFLOW)
    @RequestMapping(value = "/gen-link/{sessionIndex}", method = RequestMethod.POST)
    @ResponseBody
    public GenerateLinkResponse generateLink(@RequestBody WelcomeFormModel request, @PathVariable Integer sessionIndex)
            throws FlowStateException {
        FlowStateViewer flowState = getCurrentFlowState(sessionIndex);
        GenerateLinkCommand command = commandsProvider.getCommand(GenerateLinkCommand.class);
        return graphService.executeJob(new Job<>(command, request, flowState));
    }

    /**
     * Zobrazí statement v příslušném skriptu.
     * @param request požadavek requestu
     * @return zpracovana odpoved na request typicky ve formatu JSON (dle Accept header)
     * @throws FlowStateException neexistující session id
     */
    @Secured(ViewerRoles.VIEWER_DATAFLOW)
    @RequestMapping(value = "/stmt-in-script", method = RequestMethod.POST)
    @ResponseBody
    public ShowStmtInScriptResponse showStatementInScript(@RequestBody ShowStmtInScriptRequest request)
            throws FlowStateException {
        ShowStmtInScriptCommand command = commandsProvider.getCommand(ShowStmtInScriptCommand.class);
        return graphService.executeJob(new Job<>(command, request, getCurrentFlowState(request.getSessionIndex())));
    }

    /**
     * Zkontroluje, že session s daným indexem existuje.
     * @param sessionIndex kontrolovaná session
     * @return vždy vrátí status 200, pokud session existuje tak navíc OK
     */
    @Secured(ViewerRoles.VIEWER_DATAFLOW)
    @RequestMapping(value = "/check/{sessionIndex}", method = RequestMethod.GET)
    public ResponseEntity<String> checkLogin(@PathVariable int sessionIndex) {
        try {
            long flowStateCursor = getFlowStateManager(sessionIndex).getCursor();
            if (flowStateCursor < 0) {
                return new ResponseEntity<String>("Wrong session index.", HttpStatus.OK);
            }
        } catch (FlowStateException e) {
            return new ResponseEntity<String>("Wrong session index.", HttpStatus.OK);
        }

        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }

    /**
     * Získá zámek pro danou session.
     * @param sessionId id session, pro kterou se hledá zámek
     * @return zámek pro danou session, nikdy null
     * @throws FlowStateException neexistující lock
     * @throws IllegalArgumentException pro danou session neexistuje zámek
     */
    private synchronized Object getLock(int sessionId) throws FlowStateException {
        Object lock = lockMap.get(sessionId);
        if (lock != null) {
            return lock;
        } else {
            LOGGER.warn("Failed to load lock for the session " + sessionId + ".");
            throw new FlowStateException("Session has probably timed out. Please try to refresh the page.");
        }
    }

    /**
     * Inicializuje zámek pro danou session.
     * @param sessionId session, pro kterou se inicializuje zámek
     * @return nový zámek pro danou session
     * @throws IllegalStateException pro danou session už existuje zámek
     */
    private synchronized Object initLock(int sessionId) {
        Object newLock = new Object();
        if (lockMap.putIfAbsent(sessionId, newLock) == null) {
            return newLock;
        } else {
            throw new IllegalStateException("The lock for the session " + sessionId + " has already been created.");
        }
    }

    /**
     * @return Adresa stránky volané při chybě o iniciální follow.
     */
    public String getInitFollowErrorPage() {
        return initFollowErrorPage;
    }

    /**
     * @param initFollowErrorPage Adresa stránky volané při chybě o iniciální follow. 
     */
    public void setInitFollowErrorPage(String initFollowErrorPage) {
        this.initFollowErrorPage = initFollowErrorPage;
    }

    /**
     * @return Maximální hloubka iniciálního follow.
     */
    public int getMaximalDepth() {
        return maximalDepth;
    }

    /**
     * @param maximalDepth Maximální hloubka iniciálního follow.
     */
    public void setMaximalDepth(int maximalDepth) {
        this.maximalDepth = maximalDepth;
    }

    /**
     * @return Sluzba pro praci s grafem. 
     */
    protected GraphService getGraphService() {
        return graphService;
    }

    /**
     * @return Provider poskytující commandy pro jednotlivé requesty. 
     */
    protected CommandsProvider getCommandsProvider() {
        return commandsProvider;
    }

    // ----------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------
    // Ošetření chybových stavů.
    // ----------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------

    /**
     * Ošetření případu, kdy je obdržen chybný request.
     * @param exception vyhozená výjimka
     * @return json odpověď popisující problém
     */
    @ExceptionHandler(CorruptedRequestException.class)
    public ModelAndView corruptedRequest(CorruptedRequestException exception) {
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorNumber", HttpStatus.BAD_REQUEST.value());
        mav.addObject("errorMessage", exception.getMessage());
        return mav;
    }

    /**
     * Ošetření případu, kdy je obdržen request využívající nezaindexovaný flowState.
     * @param exception vyhozená výjimka
     * @return json odpověď popisující problém
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(FlowStateException.class)
    @ResponseBody
    public BaseResponse flowStateNotExist(FlowStateException exception) {
        return new BaseResponse("flowStateError", new Message(Severity.ERROR, exception.getMessage()));
    }

    /**
     * Ošetření výjimek vyletících při zpracování GET linku nebo z formuláře.
     * @param exception vyhozená výjimka
     * @param request http request pro odeslání přesměrovávacích atributů
     * @return adresa pro přesměrování
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(FlowFormException.class)
    public View allFormException(FlowFormException exception, HttpServletRequest request) {
        LOGGER.error("Exception has occurred during proccesing form data.", exception);
        RedirectView rw = new RedirectView(initFollowErrorPage, true);
        FlashMap outputFlashMap = RequestContextUtils.getOutputFlashMap(request);
        if (outputFlashMap != null) {
            outputFlashMap.put("errorMessage", new Message(Severity.ERROR, UNEXPECTED_ERROR));
        }
        return rw;
    }

    /**
     * Ošetření výjimek při neexistenci dat.
     * @param exception vyhozená výjimka
     * @param request http request pro odeslání přesměrovávacích atributů
     * @return adresa pro přesměrování
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(NoCommitedDataException.class)
    public View noDataException(NoCommitedDataException exception, HttpServletRequest request) {
        LOGGER.warn("There is no commited data.", exception);
        RedirectView rw = new RedirectView(initFollowErrorPage, true);
        FlashMap outputFlashMap = RequestContextUtils.getOutputFlashMap(request);
        if (outputFlashMap != null) {
            outputFlashMap.put("errorMessage", new Message(Severity.ERROR, exception.getMessage()));
        }
        return rw;
    }

    /**
     * Ošetření zbylých výjimek, které vyletí z jsonu.
     * @param exception vyhozená výjimka
     * @return json odpověď popisující problém
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public BaseResponse allException(Exception exception) {
        LOGGER.error("Exception has occurred during JSON request.", exception);
        return new BaseResponse("error", new Message(Severity.ERROR, UNEXPECTED_ERROR));
    }
}
