package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowEdge;
import eu.profinit.manta.dataflow.repository.core.model.EdgeIdentification;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.FlowEdgeViz;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.RevisionState;

/**
 * Správce ekvivalencí vrcholů v rámci intervalu revizí.
 * @author tfechtner
 *
 */
public class VertexEquivalencesHolder {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(VertexEquivalencesHolder.class);

    /** Mapování id ekvivalentních vrcholů.*/
    private final Map<Long, Long> mapIds;
    /** Množina id ekvivalentní vrcholů, které nemají platnost v nejnovější revizi. */
    private final Set<Long> equivalentOlderIds;

    /**
     * Výchozí konstruktor inicializující vnitřní stav.
     */
    public VertexEquivalencesHolder() {
        mapIds = new HashMap<>();
        equivalentOlderIds = new HashSet<>();
    }

    /**
     * Copy-constructor.
     * @param other instance, ze které se zkopíruje stav
     */
    public VertexEquivalencesHolder(VertexEquivalencesHolder other) {
        mapIds = new HashMap<>(other.mapIds);
        equivalentOlderIds = new HashSet<>(other.equivalentOlderIds);
    }

    /**
     * Naplní vnitřní strukturu ekvivalencí z daných vrcholů.
     * @param transaction transakce pro přístup k db
     * @param vertexIds množina id vrcholů, pro které se hledají ekvivalence
     * @param mainRevision číslo nejnovější revize
     */
    public void buildForVertices(TitanTransaction transaction, Set<Long> vertexIds, double mainRevision) {
        long time = System.currentTimeMillis();
        HashMap<VertexFullIdentification, VertexLayerIdentification> verticesMap = new HashMap<>();
        for (Long currentId : vertexIds) {
            Vertex currentVertex = transaction.getVertex(currentId);
            VertexFullIdentification currentFullId = new VertexFullIdentification(currentVertex);

            VertexLayerIdentification savedLayerId = verticesMap.get(currentFullId);
            if (savedLayerId == null) {
                verticesMap.put(currentFullId, currentFullId.getNode());
            } else {
                if (savedLayerId.getRevisionInterval().getEnd() >= mainRevision) {
                    // uložený vrchol ma platnost i v hlavní revizi -> je to správně
                    equivalentOlderIds.add(currentId);
                } else if (currentFullId.getNode().getRevisionInterval().getEnd() >= mainRevision) {
                    // aktuální vrchol ma platnost i v hlavní revizi -> vyměnit
                    verticesMap.put(currentFullId, currentFullId.getNode());
                    equivalentOlderIds.add(savedLayerId.getId());
                } else {
                    // ani jeden vrchol nemá platnost v koncové revizi
                    throw new IllegalStateException("None of vertices [" + currentFullId + "," + savedLayerId
                            + "] is valid in rev " + mainRevision + ".");
                }

                // poznačit obousměrné mapování
                mapIds.put(savedLayerId.getId(), currentId);
                mapIds.put(currentId, savedLayerId.getId());
            }
        }
        LOGGER.info("Found {} equivalences in {} ms.", equivalentOlderIds.size(), System.currentTimeMillis() - time);
    }

    /**
     * Zaregistruje dvojici ekvivalentních resource.
     * @param mainResource id resource v hlavní revizi
     * @param equivalentResource id resource, který je ekvivalencí ze starší revize
     */
    public void registerResourcePair(Long mainResource, Long equivalentResource) {
        mapIds.put(mainResource, equivalentResource);
        mapIds.put(equivalentResource, mainResource);
        equivalentOlderIds.add(equivalentResource);
    }

    /**
     * Přemapuje hrany na ekvivalentní uzly platné v hlavní revizi, zjistí stav hran vůči revizím.
     * Odfiltruje hrany se stavem RevisionState.INNER.
     * @param transaction transakce pro přístup k db
     * @param edges hrany k přemapování
     * @param revisionInterval interval revizí, ke kterým se hrany vztahují
     * @return přemapované hrany, nikdy null
     */
    public Set<FlowEdgeViz> remapEdges(TitanTransaction transaction, Set<FlowEdge> edges,
            RevisionInterval revisionInterval) {

        if (revisionInterval.getStart() == revisionInterval.getEnd()) {
            Set<FlowEdgeViz> resultEdges = new HashSet<>();
            for (FlowEdge edge : edges) {
                resultEdges.add(new FlowEdgeViz(edge, revisionInterval));
            }
            return resultEdges;
        }

        // vytvořit mapování
        Map<EdgeIdentification, List<FlowEdge>> mapEdges = new HashMap<>();
        for (FlowEdge edge : edges) {
            EdgeIdentification eId = createMappedEdgeId(edge);
            List<FlowEdge> savedEdges = mapEdges.get(eId);
            if (savedEdges == null) {
                savedEdges = new ArrayList<>();
                mapEdges.put(eId, savedEdges);
            }
            savedEdges.add(edge);
        }

        // vytvořit hrany z mapování
        Set<FlowEdgeViz> resultEdges = new HashSet<>();
        for (Entry<EdgeIdentification, List<FlowEdge>> edgeEntry : mapEdges.entrySet()) {
            EdgeIdentification eId = edgeEntry.getKey();
            Map<String, Object> attrs = new HashMap<>();
            for (FlowEdge edge : edgeEntry.getValue()) {
                attrs.putAll(edge.getAttrs());
            }

            double revStart = RevisionRootHandler.REVISION_MAX_NUMBER;
            double revEnd = 0;
            for (FlowEdge edge : edgeEntry.getValue()) {
                revStart = Math.min(revStart, edge.getRevInterval().getStart());
                revEnd = Math.max(revEnd, edge.getRevInterval().getEnd());
            }

            RevisionState revState = RevisionState.resolve(new RevisionInterval(revStart, revEnd), revisionInterval);
            if (revState != RevisionState.INNER) {
                FlowEdgeViz vizEdge = new FlowEdgeViz(eId.getStartId(), eId.getEndId(), eId.getLabel(), attrs,
                        revState);
                resultEdges.add(vizEdge);
            }
        }

        return resultEdges;
    }

    /**
     * Vytvoř identifikaci hrany s přemapováním.
     * @param edge hrana k přemapování
     * @return identifikace hrany, nikdy null
     */
    private EdgeIdentification createMappedEdgeId(FlowEdge edge) {
        Long source;
        if (equivalentOlderIds.contains(edge.getSource())) {
            source = mapIds.get(edge.getSource());
        } else {
            source = edge.getSource();
        }

        Long target;
        if (equivalentOlderIds.contains(edge.getTarget())) {
            target = mapIds.get(edge.getTarget());
        } else {
            target = edge.getTarget();
        }

        return new EdgeIdentification(source, target, edge.getLabel());
    }

    /**
     * Pokus se nalézt ekvivalentní id vrcholu. 
     * @param originalId id originálního vrcholu
     * @return id ekvivalentního vrcholu nebo null, když originál nemá ekvivalent
     */
    public Long getEquivalentVertexId(Long originalId) {
        return mapIds.get(originalId);
    }

    /**
     * Ověři, jestli je vrchol s daným id ekvivalentní z jiné než hlavní revize.
     * @param id id vrcholu k ověření
     * @return true, jestliže je vrchol ekvivalentní a nemá platnost v hlavní revizi
     */
    public boolean isEquivalenceFromOlderRev(Long id) {
        return equivalentOlderIds.contains(id);
    }

    /**
     * Kompletní identifikace vrcholu včetně jeho resource a všech předků.
     * @author tfechtner
     *
     */
    private static final class VertexFullIdentification {
        private final VertexLayerIdentification node;
        private final List<VertexLayerIdentification> parents;
        private final VertexLayerIdentification resource;

        VertexFullIdentification(Vertex vertex) {
            Edge controlEdge = GraphOperation.getNodeControlEdge(vertex);
            node = new VertexLayerIdentification(vertex, controlEdge);

            ArrayList<VertexLayerIdentification> parentsTemp = new ArrayList<>();
            Vertex parentVertex = controlEdge.getVertex(Direction.IN);
            while (VertexType.getType(parentVertex) == VertexType.NODE) {
                Edge parentControlEdge = GraphOperation.getNodeControlEdge(parentVertex);
                parentsTemp.add(new VertexLayerIdentification(parentVertex, parentControlEdge));
                parentVertex = parentControlEdge.getVertex(Direction.IN);
            }
            parents = Collections.unmodifiableList(parentsTemp);

            Edge resourceControlEdge = GraphOperation.getResourceControlEdge(parentVertex);
            resource = new VertexLayerIdentification(parentVertex, resourceControlEdge);
        }

        public VertexLayerIdentification getNode() {
            return node;
        }

        @Override
        public String toString() {
            return node + "(" + parents + "/" + resource + ")";
        }

        @Override
        public int hashCode() {
            HashCodeBuilder builder = new HashCodeBuilder();
            builder.append(node);
            builder.append(parents);
            builder.append(resource);
            return builder.toHashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (obj.getClass() != getClass()) {
                return false;
            }
            VertexEquivalencesHolder.VertexFullIdentification other = (VertexEquivalencesHolder.VertexFullIdentification) obj;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(node, other.node);
            builder.append(parents, other.parents);
            builder.append(resource, other.resource);
            return builder.isEquals();
        }
    }

    /**
     * Identifikace vrcholu s revizí, ale bez předků.
     * @author tfechtner
     *
     */
    private static final class VertexLayerIdentification {
        private final Long id;
        private final String name;
        private final String type;
        private final RevisionInterval revisionInterval;

        public VertexLayerIdentification(Vertex vertex, Edge controEdge) {
            this.id = (Long) vertex.getId();
            this.name = GraphOperation.getName(vertex);
            this.type = GraphOperation.getType(vertex);
            this.revisionInterval = RevisionUtils.getRevisionInterval(controEdge);
        }

        public Long getId() {
            return id;
        }

        public RevisionInterval getRevisionInterval() {
            return revisionInterval;
        }

        @Override
        public String toString() {
            return "[" + id + ":" + name + "(" + type + ")" + revisionInterval + "]";
        }

        @Override
        public int hashCode() {
            HashCodeBuilder builder = new HashCodeBuilder();
            builder.append(name);
            builder.append(type);
            return builder.toHashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (obj.getClass() != getClass()) {
                return false;
            }
            VertexEquivalencesHolder.VertexLayerIdentification other = (VertexEquivalencesHolder.VertexLayerIdentification) obj;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(name, other.name);
            builder.append(type, other.type);
            return builder.isEquals();
        }
    }
}
