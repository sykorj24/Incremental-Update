package eu.profinit.manta.dataflow.repository.viewer.model.index;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import eu.profinit.manta.dataflow.repository.viewer.model.MappingNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Matyas Krutsky <matyas.krutsky@profinit.eu>
 */
@XmlRootElement
public class Node {

    private List<String> path = new LinkedList<>();

    /**
     * Identifikace uzlu pomocí posloupnosti ID předků a následně daného uzlu.
     */
    private List<Long> idsPath = new LinkedList<>();
    
    /**
     * Property, ktera urcuje popisek uzlu.
     * 
     * nazev je vyhodne zachovat kvuli klientske komponente jstree
     */
    private String text;

    private Long id;
    
    /** Vrstva, do ktere patri uzel */
    private String layer;
    
    /**
     * Reální technologie uzlu.
     */
    private String technology;

    /**
     * Typ technologie daneho uzlu.
     */
    private String technologyType;

    /**
     * Property, ktera urcuje typ uzlu.
     *
     * nazev je vyhodne zachovat kvuli klientske komponente jstree
     */
    private String type;

    private List<Node> descendants;
    
    /** Mapa atributů uzlu. */
    private Map<String, List<Object>> attributes;

    /**
     * Property, ktera urcuje zda uzel ma nejake potomky.
     *
     * property je zde predevsim kvuli jstree - je lehci nastavit parametr 
     * zde nez na klientske strane
     */
    private boolean children;

    /**
     * Mapovani na uzly ve vrstvach.
     * Klicem je nazev vrstvy, hodnota je seznam uzlu mapovanych timto uzlem
     * ci mapujicich tento uzel v prislusne vrstve.
     */
    private Map<String, List<MappingNode>> nodeMapping;

    /**
     * @return text uzlu
     */
    @XmlAttribute(name = "text")
    public String getText() {
        return text;
    }

    /**
     * @param text  text uzlu
     */
    public void setText(String text) {
        this.text = text;
    }
    
    /**
     * @return Vrstva, do ktere patri uzel
     */
    @XmlAttribute(name = "layer")
    public String getLayer() {
        return layer;
    }

    /**
     * @param layer Vrstva, do ktere patri uzel
     */
    public void setLayer(String layer) {
        this.layer = layer;
    }

    /**
     * @return Reální technologie uzlu.
     */
    @XmlAttribute(name = "technology")
    public String getTechnology() {
        return technology;
    }

    /**
     * @param technology Reální technologie uzlu.
     */
    public void setTechnology(String technology) {
        this.technology = technology;
    }
    
    /**
     * @return Typ technologie daného uzlu.
     */
    @XmlAttribute(name = "technologyType")
    public String getTechnologyType() {
        return technologyType;
    }

    /**
     * @param technologyType Typ technologie daného uzlu.
     */
    public void setTechnologyType(String technologyType) {
        this.technologyType = technologyType;
    }

    /**
     * @return typ uzlu
     */
    @XmlAttribute(name = "type")
    public String getType() {
        return type;
    }

    /**
     * @param type typ uzlu
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return potomci uzlu
     */
    @XmlElement(name = "node")
    public List<Node> getDescendants() {
        return descendants;
    }

    /**
     * @param descendants potomci uzlu
     */
    public void setDescendants(List<Node> descendants) {
        this.descendants = descendants;
    }
        
    /**
     * @return atributy uzlu
     */
    @XmlElement(name = "attributes")
    public Map<String, List<Object>> getAttributes() {
        return attributes;
    }

    /**
     * @param attributes atributy uzlu
     */
    public void setAttributes(Map<String, List<Object>> attributes) {
        this.attributes = attributes;
    }

    /**
     * @return Mapovani na uzly ve vrstvach.
     *         Klicem je nazev vrstvy, hodnota je seznam uzlu mapovanych timto uzlem
     *         ci mapujicich tento uzel v prislusne vrstve.
     */
    @XmlElement(name = "nodeMapping")
    public Map<String, List<MappingNode>> getNodeMapping() {
        return nodeMapping;
    }

    public void setNodeMapping(Map<String, List<MappingNode>> nodeMapping) {
        this.nodeMapping = nodeMapping;
    }

    /**
     * @param s přidat s k cestě
     */
    public void addToPath(String s) {
        getPath().add(s);
    }

    /**
     * @return identifikace uzlu pomocí posloupnosti předků a následně daného uzlu
     */
    public List<String> getPath() {
        return path;
    }

    /**
     * @param path identifikace uzlu pomocí posloupnosti předků a následně daného uzlu
     */
    public void setPath(List<String> path) {
        this.path = path;
    }
    
    /**
     * @return identifikace uzlu pomocí posloupnosti ID předků a následně daného uzlu
     */
    public List<Long> getIdsPath() {
        return idsPath;
    }

    /**
     * @param idsPath identifikace uzlu pomocí posloupnosti ID předků a následně daného uzlu
     */
    public void setIdsPath(List<Long> idsPath) {
        this.idsPath = idsPath;
    }

    /**
     * @return unikátní id uzlu z db
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id unikátní id uzlu z db
     */
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return getText() + "[" + getId() + "]";
    }

    /**
     * @return true, jestliže má nějaké potomky
     */
    public boolean isChildren() {
        return children;
    }

    /**
     * @param children true, jestliže má nějaké potomky
     */
    public void setChildren(boolean children) {
        this.children = children;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Node other = (Node) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
