package eu.profinit.manta.dataflow.repository.viewer.service.filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilter;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterGroup;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterType;

/**
 * Pomocná třída pro práci s horizontálními filtry.
 * @author tfechtner
 *
 */
public class HorizontalFilterHelper {

    /**
     * Vytvoří model pro množinu skupin filtrů.
     * @param groupSet
     * @param horizontalFilterProvider Poskytovatel horizontalnich filtru
     * @return
     */
    public List<Map<String, Object>> generateModelForGroups(Set<HorizontalFilterGroup> groupSet, HorizontalFilterProvider horizontalFilterProvider) {
        
        // Vazby filtru na vrstvy
        Map<String, String> filterLayers = new HashMap<>();

        List<HorizontalFilter> filterList = horizontalFilterProvider.getFilters();
        for (HorizontalFilter filter : filterList) {
            filterLayers.put(filter.getId(), filter.getLayer());
        }
        
        List<Map<String, Object>> model = new ArrayList<>();

        for (HorizontalFilterGroup group : groupSet) {
            Map<String, Object> groupModel = new HashMap<>();
            groupModel.put("id", group.getId());
            groupModel.put("order", group.getOrder());
            groupModel.put("name", group.getName());
           
            // Nastavime vazby skupiny filtru na vrstvy
            // podle toho, ke kterym vrstvam patri jeji filtry.
            // Pokud nema skupina specifikovane vrstvy, pak se vaze na vsechny.
            Set<String> layers = new HashSet<>();
            for (String filterId : group.getFilterIds()) {
                if (!filterLayers.containsKey(filterId)) {
                    // Ve skupine je ID filtru ktery neexistuje => ingnorujeme
                    continue;
                }
                String layer = filterLayers.get(filterId);
                if (layer == null) {
                    // filtr nema specifikovanou vrstvu => patri do vsech vrstev
                    // => skupina se tez vaze na vsechny vrstvy
                    layers = new HashSet<>();
                    break;
                }
                layers.add(layer);
            }
            groupModel.put("layers", layers);
            
            model.add(groupModel);
        }

        return model;
    }

    /**
     * @param filterType
     * @param activeFilters
     * @param horizontalFilterProvider
     * @return
     */
    public List<Map<String, Object>> generateModelForFilters(HorizontalFilterType filterType,
            Set<String> activeFilters, HorizontalFilterProvider horizontalFilterProvider) {
        List<Map<String, Object>> model = new ArrayList<>();

        List<HorizontalFilter> filterList = horizontalFilterProvider.getFilters();
        for (HorizontalFilter filter : filterList) {
            if (filter.getType() != filterType) {
                continue;
            }

            Map<String, Object> filterModel = new HashMap<>();
            filterModel.put("id", filter.getId());
            filterModel.put("normalizedId", filter.getId().replaceAll("[^A-Za-z0-9]", "_"));
            filterModel.put("name", filter.getName());
            filterModel.put("isFilterActive", activeFilters.contains(filter.getId()));
            filterModel.put("layer", filter.getLayer());
            model.add(filterModel);
        }

        return model;
    }
}
