package eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.service.FilteredRelationshipService;

/**
 * Datová třída pro proxy reprezentaci uzlu.
 * @author tfechtner
 *
 */
public final class ProxyComponent extends Component {

    /**
     * @param vertex zdrojový vertex
     * @param relationshipService služba pro práci s předky
     * @param flowState stav flow
     * @param levelMapProvider provider pro získání definice úrovně a atomicity uzlu
     */
    /*package*/ ProxyComponent(Vertex vertex, FlowStateViewer flowState,
            FilteredRelationshipService relationshipService, LevelMapProvider levelMapProvider) {
        super(vertex, true, relationshipService, flowState, levelMapProvider);
    }

    /**
     * @param id Id uzlu.
     * @param proxy Přepínáš, jestliže jde o proxy objekt.
     * @param parentID Id předka.
     * @param status Aktuální status uzlu v rámci view.
     * @param tech Technologie neboli resource uzlu.
     * @param filtered True, jestliže je daný uzel vyfiltrovaný.
     * @param topParentTech Id top parent resource.
     * @param level Úroveň uzlu ve flow. 
     */
    public ProxyComponent(Object id, boolean proxy, Object parentID, NodeStatus status, String tech, boolean filtered,
            Object topParentTech, int level) {
        super(id, proxy, parentID, status, tech, filtered, topParentTech, level);
    }

    @Override
    public String toString() {
        return "v[" + getId() + "](P:" + getParentID() + ", s: " + getStatus() + ")";
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ProxyComponent other = (ProxyComponent) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }
}
