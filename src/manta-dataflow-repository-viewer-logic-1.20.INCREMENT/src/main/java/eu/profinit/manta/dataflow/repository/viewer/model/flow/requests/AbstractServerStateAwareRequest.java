package eu.profinit.manta.dataflow.repository.viewer.model.flow.requests;

/**
 * Request, ktery vyzaduje nastaveni specifickeho flow state na serveru. Tento stav se muze
 * lisit od aktualniho flow state serveru, pokud napr. uzivatel stiskne na klientovi
 * tlacitko undo.
 */
public abstract class AbstractServerStateAwareRequest implements ServerRequest {

    private Long serverState;

    /**
     * @return Stav server, na ktery aktualne odkazuje stav klienta vzhledem k Undo/Redo pozici
     */
    public Long getServerState() {
        return serverState;
    }

    /**
     * @param serverState Stav server, na ktery aktualne odkazuje stav klienta vzhledem k Undo/Redo pozici
     */
    public void setServerState(Long serverState) {
        this.serverState = serverState;
    }
}
