package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;

/**
 * Třída udržují informace o view stavu vrcholů. <br>
 * Sem patří jejich proxicita a rozbalenost. 
 * @author tfechtner
 *
 */
public class VertexViewStatusHolder {
    /** Mapa id uzlů na jejich aktuální stav. */
    private final Map<Long, VertexViewStatus> vertices = new HashMap<>();
    /** Stav daného flow. */
    private FlowStateViewer flowState;

    /**
     * @param flowState stav daného flow
     */
    public VertexViewStatusHolder(FlowStateViewer flowState) {
        this.flowState = flowState;
    }

    /**
     * Uloží do flowState, které uzly byly již poslány jako full.
     */
    public void persistFullNodes() {
        for (Long id : getKnownVertices()) {
            if (getStatus(id) == ProxyStatus.FULL) {
                flowState.markNodeAsFull(id);
            }
        }
    }

    /**
     * @return množina id všech vrcholů, o kterých je udržován stav
     */
    public Set<Long> getKnownVertices() {
        return Collections.unmodifiableSet(vertices.keySet());
    }

    /**
     * Ověří, jestli by měl být předek daného uzlu proxy.
     * @param node zkoumaný uzel
     * @return true, jestliže by měl být předek proxy, false v opačném případě nebo jestli se neví
     */
    public boolean isShouldBeParentProxy(Vertex node) {
        VertexViewStatus vertexStatus = vertices.get(node.getId());
        return vertexStatus != null && vertexStatus.isShouldBeParentProxy();
    }

    /**
     * Ověří, jestli by měl být daný uzel rozbalen.
     * @param node zkoumaný uzel
     * @return true, jestliže by měl být rozbalen, false v opačném případě nebo jestli se neví
     */
    public boolean isExpanded(Vertex node) {
        VertexViewStatus viewStatus = vertices.get(node.getId());
        return viewStatus != null && viewStatus.isExpanded();
    }

    /**
     * Přidá uzel do mapy se stavy uzlů jako full uzel.
     * @param vertex uzel, který se má vložit
     * @param isExpanded true, jestliže má být rozbalen
     */
    public void addFull(Vertex vertex, boolean isExpanded) {
        Long id = (Long) vertex.getId();
        vertices.put(id, new VertexViewStatus(id, false, isExpanded, false));
    }

    /**
     * Přidá uzel do mapy se stavy uzlů jako proxy uzel.
     * @param vertex uzel, který se má vložit
     * @param shouldBeParentProxy Příznak, jestli mají být předci také proxy.
     */
    public void addProxy(Vertex vertex, boolean shouldBeParentProxy) {
        Long id = (Long) vertex.getId();
        if (!vertices.containsKey(id)) {
            vertices.put(id, new VertexViewStatus(id, true, true, shouldBeParentProxy));
        }
    }

    /**
     * Vrátí stav vrcholu, co se týče proxicity.
     * @param node zkoumaný vrchol
     * @return stav vrcholu, nikdy null
     */
    public ProxyStatus getStatus(Vertex node) {
        return getStatus((Long) node.getId());
    }

    /**
     * Vrátí stav vrcholu, co se týče proxicity.
     * @param id id zkoumaného vrcholu
     * @return stav vrcholu, nikdy null
     */
    public ProxyStatus getStatus(Long id) {
        if (flowState.isNodeSentAsFull(id)) {
            return ProxyStatus.FULL;
        } else {
            VertexViewStatus nodeStatus = vertices.get(id);
            if (nodeStatus == null) {
                return ProxyStatus.UNKNOWN;
            } else if (nodeStatus.isProxy()) {
                return ProxyStatus.PROXY;
            } else {
                return ProxyStatus.FULL;
            }
        }
    }

    /**
     * Informace jestli je vrchol proxy nebo ne.
     * @author tfechtner
     *
     */
    public static enum ProxyStatus {
        /** Vrchol je jen jako proxy komponenta se základními informacemi. */
        PROXY,
        /** Vrchol musí být kompletní komponenta se všemi informacemi. */
        FULL,
        /** Dosud neznámý status, může být jak proxy tak full. */
        UNKNOWN;

        /**
         * @return true, jestliže je nebo může být vrchol proxy
         */
        public boolean couldBeProxy() {
            return this == PROXY || this == UNKNOWN;
        }

        /**
         * @return true, jestliže je nebo může být vrchol full
         */
        public boolean couldBeFull() {
            return this == FULL || this == UNKNOWN;
        }
    }

    /**
     * Status uzlu v rámci daného follow.
     * @author tfechtner
     *
     */
    private static final class VertexViewStatus {
        /** Id uzlu. */
        private final Long id;
        /** Příznak, jestli je uzel proxy. */
        private final boolean isProxy;
        /** Příznak, jestli je uzel rozbalen. */
        private final boolean isExpanded;
        /** Příznak, jestli mají být předci také proxy. */
        private final boolean shouldBeParentProxy;

        /**
         * @param id Id uzlu.
         * @param isProxy Příznak, jestli je uzel proxy.
         * @param isExpanded Příznak, jestli je uzel rozbalen.
         * @param shouldBeParentProxy Příznak, jestli mají být předci také proxy.
         */
        private VertexViewStatus(Long id, boolean isProxy, boolean isExpanded, boolean shouldBeParentProxy) {
            super();
            this.id = id;
            this.isProxy = isProxy;
            this.isExpanded = isExpanded;
            this.shouldBeParentProxy = shouldBeParentProxy;
        }

        /**
         * @return Příznak, jestli je uzel proxy.
         */
        public boolean isProxy() {
            return isProxy;
        }

        /**
         * @return Příznak, jestli je uzel rozbalen.
         */
        public boolean isExpanded() {
            return isExpanded;
        }

        /**
         * @return Příznak, jestli mají být předci také proxy.
         */
        public boolean isShouldBeParentProxy() {
            return shouldBeParentProxy;
        }

        @Override
        public String toString() {
            return "vs[" + id + "](P:" + isProxy + ",E:" + isExpanded + ",PP:" + shouldBeParentProxy + ")";
        }

        @Override
        public int hashCode() {
            return id.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            VertexViewStatus other = (VertexViewStatus) obj;
            if (id == null) {
                if (other.id != null) {
                    return false;
                }
            } else if (!id.equals(other.id)) {
                return false;
            }
            return true;
        }
    }
}
