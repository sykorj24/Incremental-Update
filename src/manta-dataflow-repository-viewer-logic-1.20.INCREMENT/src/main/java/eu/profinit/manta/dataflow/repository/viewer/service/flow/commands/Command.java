package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ServerRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ServerResponse;

/**
 * Rozhraní definující commandy. 
 * To jsou takové třídy, které implementují jednotlivé algoritmy vizualizace.  
 * @author tfechtner
 *
 * @param <Q> typ requestu
 * @param <S> typ reposnse
 */
public interface Command<Q extends ServerRequest, S extends ServerResponse> {
    
    /** 
     * Provede command na daných parametrech.
     * @param request requst
     * @param flowState stav konkrétního flow
     * @param transaction transakce pro práci s databází
     * @return výstup práce commandu
     */
    S execute(Q request, FlowStateViewer flowState, TitanTransaction transaction);
}
