package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.HiddenByFiltersRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.HiddenByFiltersResponse;

/**
 * Operace pro zjištění odfiltrovanosti daných uzlů danými filtry.
 * @author tfechtner
 *
 */
public class HiddenByFiltersCommand extends AbstractCommand<HiddenByFiltersRequest, HiddenByFiltersResponse> {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(HiddenByFiltersCommand.class);

    /** Provider horizontálních filtrů. */
    @Autowired
    private HorizontalFilterProvider filterProvider;

    @Override
    public HiddenByFiltersResponse execute(HiddenByFiltersRequest request, FlowStateViewer flowState,
            TitanTransaction transaction) {

        Map<Long, Boolean> stateMap = new HashMap<>();

        for (Long id : request.getComponentIds()) {
            Vertex vertex = transaction.getVertex(id);
            if (vertex != null) {
                boolean state = !flowState.isTotalStartNode(id) && !flowState.isTotalStartNodeParent(id)
                        && filterProvider.isFiltered(vertex,
                        flowState.getRevisionInterval(),
                        request.getActiveFilters());
                stateMap.put(id, state);
            } else {
                LOGGER.warn("Vertex with id {} doesn't exist.", id);
            }
        }

        HiddenByFiltersResponse response = new HiddenByFiltersResponse();
        response.setFilterStates(stateMap);
        return response;
    }

    /**
     * @return Provider horizontálních filtrů
     */
    public HorizontalFilterProvider getFilterProvider() {
        return filterProvider;
    }

    /**
     * @param filterProvider Provider horizontálních filtrů
     */
    public void setFilterProvider(HorizontalFilterProvider filterProvider) {
        this.filterProvider = filterProvider;
    }
}
