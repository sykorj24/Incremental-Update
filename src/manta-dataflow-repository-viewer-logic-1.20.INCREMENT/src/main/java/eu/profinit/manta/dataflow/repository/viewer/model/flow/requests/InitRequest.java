package eu.profinit.manta.dataflow.repository.viewer.model.flow.requests;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.tinkerpop.blueprints.Direction;

import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;

/******************************************************************************
 *   Dotaz pro inicializaci referencniho pohledu.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class InitRequest implements ServerRequest {
    /** Výchozí počet maximálního počtu startovních uzlů. */
    private static final int DEFAULT_START_NODES = 100;
    /** Startovní uzly.*/
    private List<Long> referencePoints = new ArrayList<>();    
    /** Prohledávaný směr flow.*/
    private Direction direction = Direction.BOTH;
    /** Příznak jestli se mají zobrazovat filtr hrany.*/
    private boolean filterEdges = false;
    /** Zobrazovaná úroveň flow. */
    private FlowLevel flowLevel = FlowLevel.BOTTOM;
    /** Maximální počet startovnách uzlů. */
    private int maximumStartNodes = DEFAULT_START_NODES;

    /**
     * @return IDcka vychozich referencnich bodu
     */
    public List<Long> getReferencePoints() {
        return referencePoints;
    }

    /**
     * @param referencePoints IDcka vychozich referencnich bodu
     */
    public void setReferencePoints(List<Long> referencePoints) {
        this.referencePoints = referencePoints;
    }

    /**
     * @return Prohledávaný směr flow.
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * @param direction Prohledávaný směr flow.
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * @return true, jestliže mají být zobrazeny filter hrany
     */
    public boolean isFilterEdges() {
        return filterEdges;
    }

    /**
     * @param filterEdges true, jestliže mají být zobrazeny filter hrany
     */
    public void setFilterEdges(boolean filterEdges) {
        this.filterEdges = filterEdges;
    }

    /**
     * @return Zobrazovaná úroveň flow.
     */
    public FlowLevel getFlowLevel() {
        return flowLevel;
    }

    /**
     * @param flowLevel Zobrazovaná úroveň flow.
     */
    public void setFlowLevel(FlowLevel flowLevel) {
        this.flowLevel = flowLevel;
    }

    /**
     * @return Maximální počet startovnách uzlů.
     */
    public int getMaximumStartNodes() {
        return maximumStartNodes;
    }

    /**
     * @param maximumStartNodes Maximální počet startovnách uzlů.
     */
    public void setMaximumStartNodes(int maximumStartNodes) {
        this.maximumStartNodes = maximumStartNodes;
    }    
}
