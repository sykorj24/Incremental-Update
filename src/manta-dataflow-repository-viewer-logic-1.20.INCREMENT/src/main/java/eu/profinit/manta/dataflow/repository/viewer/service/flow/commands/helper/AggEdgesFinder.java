package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper;

import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowEdge;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;
import eu.profinit.manta.dataflow.repository.viewer.exception.TooManyEdgesException;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper.AggStateHolder.IncomingVertex;

/**
 * Třída pro hledání hran na cestách mezi již navštívenými uzly. 
 * Hledání probíhá ve dvou fázích: <br>
 * <b>[1. fáze]</b> 
 * <ul>
 *   <li>Vychází se ze startovních uzlů a postupně se do šířky prohledává celý graf. </li>
 *   <li>Během procházení se u jednotlivých uzlů ukládají všechny uzly, z nichž se do daného uzlu přišlo
 *      tzv incoming vertices. Kromě samotného uzlu se musí ukládat i label hrany.
 *   </li>
 *   <li> Konkrétní větev prohledávání končí, pokud je navštíven {@link NodeStatus#VISITED} nebo 
 *      {@link NodeStatus#FINISHED} uzel. Ten je označen jako cílový.
 *   </li>   
 *   <li>Druhý triviální konec, když už nejsou žádní další sousedé v daném směru.</li>
 * </ul>  
 * <b>[2. fáze]</b>
 * <ul>
 *    <li>Startuje se v cílových uzlech a zpětným průchodem se cestuje přes incoming vertices</li>
 *    <li>Vždy mezi daným uzlem a jeho příchozími se vytváří hrany.</li>
 *    <li>Končí se pouze triviálně v momentě, kdy uzel nemá žádné příchozí uzly.</li>
 * </ul>     
 * 
 * @author tfechtner
 *
 */
public class AggEdgesFinder {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(AggEdgesFinder.class);
    /** Fronta uzlů ke zpracování. */
    private final Deque<Long> queueToProcess = new LinkedList<>();
    /** Nalezené hrany. */
    private final Set<FlowEdge> collectedEdges = new HashSet<>();
    /** Cílové VISITED nebo FINISHED uzly, kam se v rámci hledání došlo. */
    private final Set<Long> targetedVertices = new HashSet<>();
    /** Transakce pro práci s db. */
    private final TitanTransaction transaction;
    /** Aktuální stav flow. */
    private final FlowStateViewer flowState;
    /** Objekt pro udržování stavu uzlů v rámci celého follow. */
    private final VerticesHolder verticesHolder;
    /** Objekt pro udržování stavu uzlů v rámci daného hledání agg hran. */
    private AggStateHolder aggStateHolder;
    /** Maximální počet agregovaných hran v jednom směru. */
    private final int edgeMaximum;
    /** Držák na technické atributy. */
    private final TechnicalAttributesHolder technicalAttributes;

    /**
     * @param transaction Transakce pro práci s db.
     * @param flowState Aktuální stav flow.
     * @param verticesHolder Objekt pro udržování stavu uzlů v rámci celého follow.
     * @param edgeMaximum Maximální počet agregovaných hran v jednom směru.
     * @param technicalAttributes držák na technické atributy
     */
    public AggEdgesFinder(TitanTransaction transaction, FlowStateViewer flowState, VerticesHolder verticesHolder,
            int edgeMaximum, TechnicalAttributesHolder technicalAttributes) {
        super();
        this.transaction = transaction;
        this.flowState = flowState;
        this.verticesHolder = verticesHolder;
        this.edgeMaximum = edgeMaximum;
        this.technicalAttributes = technicalAttributes;
    }

    /**
     * Nalezne hrany na cestách ze startovních uzlů do již navštívených uzlů.
     * @param startNodes startovní uzly hledání
     * @param direction směr hledání
     * @return nově nalezené hrany
     * @throws TooManyEdgesException překročení maximálního počtu agregovaných hran
     */
    public Set<FlowEdge> findEdges(final Set<Vertex> startNodes, Direction direction) throws TooManyEdgesException {
        // vyčistit pracovní memeber fieldy        
        collectedEdges.clear();
        targetedVertices.clear();
        aggStateHolder = new AggStateHolder();

        // prvotní nastartování pro startovní uzly
        queueToProcess.clear();
        for (Vertex v : startNodes) {
            Long id = (Long) v.getId();
            queueToProcess.add(id);
        }
        // prohledat graf
        lookForPaths(direction);

        // nyní začínáme v cílových visited/finished uzlech
        queueToProcess.clear();
        for (Long id : targetedVertices) {
            queueToProcess.add(id);
        }
        // posbírat hrany
        collectEdges(direction);

        // zjistit komponenty z hran
        verticesHolder.createComponentsFromEdges(collectedEdges, startNodes);

        return collectedEdges;
    }

    /**
     * Nalezne přímé hrany ze startovních uzlů do již navštívených uzlů.
     * @param startNodes startovní uzly hledání
     * @param direction směr hledání
     * @return nově nalezené hrany
     */
    public Set<FlowEdge> findDirectEdges(Set<Vertex> startNodes, Direction direction) {
        // vyčistit pracovní member fieldy        
        collectedEdges.clear();

        for (Vertex actualVertex : startNodes) {
            Iterable<Edge> edgeSet = GraphOperation.getAdjacentEdges(actualVertex, direction,
                    flowState.getRevisionInterval(), GraphOperation.getDataflowLabels(flowState.isFilterEdgesShown()));
            for (Edge edge : edgeSet) {
                Vertex adjacentVertex = edge.getVertex(direction.opposite());
                NodeStatus nodeStatus = flowState.getNodeStatus(adjacentVertex);
                if (nodeStatus == NodeStatus.VISITED || nodeStatus == NodeStatus.FINISHED) {
                    Map<String, Object> attrs = FlowEdge.fetchAttributes(edge, technicalAttributes);
                    rememberEdgeWithoutControl(actualVertex, adjacentVertex, direction, edge.getLabel(), attrs,
                            RevisionUtils.getRevisionInterval(edge));
                }
            }
        }

        return collectedEdges;
    }

    /**
     * Vyhledá cesty do visited a finished uzlů.
     * @param direction směr pohybu
     */
    private void lookForPaths(Direction direction) {
        while (queueToProcess.size() > 0) {
            Long id = queueToProcess.pollFirst();

            Vertex actualVertex = transaction.getVertex(id);
            if (actualVertex != null) {
                processNeighborhoodOfVertex(direction, actualVertex);
            } else {
                LOGGER.warn("Vertex with id " + id + " does not exist.");
            }
        }
    }

    /**
     * Posbírá cesty zpětným pohybem z cílových visited a finished uzlů.
     * @param direction směr pohybu, který byl použit v rámci hledání cest
     * @throws TooManyEdgesException překročení maximálního počtu agregovaných hran
     */
    private void collectEdges(Direction direction) throws TooManyEdgesException {
        while (queueToProcess.size() > 0) {
            Long id = queueToProcess.pollFirst();

            Vertex actualVertex = transaction.getVertex(id);
            if (actualVertex != null) {
                processIncomingVertices(direction, actualVertex);
            } else {
                LOGGER.warn("Vertex with id " + id + " does not exist.");
            }
        }
    }

    /**
     * Zpracuje okolí uzlu. <br>
     * <ul>
     *   <li>- pokud se nejedná o {@link NodeStatus#UNKNOWN} souseda, tak se do něj poznačí aktuální jako příchozí</li>
     *   <li>- {@link NodeStatus#VISITED} a {@link NodeStatus#FINISHED} sousedi se uloží jako cíloví</li>
     *   <li>- ještě nenavštívený {@link NodeStatus#UNVISITED} se přidá do fronty</li>
     * </ul> 
     * @param direction směrp pohybu
     * @param actualVertex zpracovávaný uzel
     */
    private void processNeighborhoodOfVertex(Direction direction, Vertex actualVertex) {
        Iterable<Edge> edgeSet = GraphOperation.getAdjacentEdges(actualVertex, direction,
                flowState.getRevisionInterval(), GraphOperation.getDataflowLabels(flowState.isFilterEdgesShown()));
        for (Edge edge : edgeSet) {
            Vertex adjacentVertex = edge.getVertex(direction.opposite());
            NodeStatus nodeStatus = flowState.getNodeStatus(adjacentVertex);
            Map<String, Object> attrs;
            switch (nodeStatus) {
            case UNKNOWN:
                // není v referenčním view -> ignorovat
                continue;
            case UNVISITED:
                attrs = FlowEdge.fetchAttributes(edge, technicalAttributes);
                aggStateHolder.addIncomingVertex(new IncomingVertex(actualVertex, edge.getLabel(), attrs,
                        RevisionUtils.getRevisionInterval(edge)), adjacentVertex);
                if (!aggStateHolder.isVisited(adjacentVertex)) {
                    aggStateHolder.visit(adjacentVertex);
                    queueToProcess.addLast((Long) adjacentVertex.getId());
                }
                break;
            case VISITED:
            case FINISHED:
                attrs = FlowEdge.fetchAttributes(edge, technicalAttributes);
                aggStateHolder.addIncomingVertex(new IncomingVertex(actualVertex, edge.getLabel(), attrs,
                        RevisionUtils.getRevisionInterval(edge)), adjacentVertex);
                targetedVertices.add((Long) adjacentVertex.getId());
                break;
            default:
                throw new IllegalStateException("Vertex " + adjacentVertex + " has unknown state: " + nodeStatus + ".");
            }
        }
    }

    /**
     * Zpracuje příchozí uzly do daného uzlu. <br>
     * <ul>
     *   <li>- pokud se nejedná o {@link NodeStatus#UNKNOWN} souseda, tak se uloží spojnice do něj vedoucí</li>
     *   <li>- ještě nezpracovaný {@link NodeStatus#UNVISITED} se přidá do fronty</li>
     * </ul>
     * @param direction směr pohybu původního hledání cest
     * @param actualVertex zpracovávaný uzel
     * @throws TooManyEdgesException překročení maximálního počtu agregovaných hran
     */
    private void processIncomingVertices(Direction direction, Vertex actualVertex) throws TooManyEdgesException {
        Set<IncomingVertex> incomingVertices = aggStateHolder.getIncomingVertices(actualVertex);

        for (IncomingVertex incomingVertex : incomingVertices) {
            Vertex incomingTitanVertex = transaction.getVertex(incomingVertex.getId());
            if (incomingTitanVertex == null) {
                LOGGER.warn("Vertex with id " + incomingVertex.getId() + " does not exist.");
                continue;
            }

            NodeStatus nodeStatus = flowState.getNodeStatus(incomingTitanVertex);
            switch (nodeStatus) {
            case UNKNOWN:
                // není v referenčním view -> nemělo by docházet.
                LOGGER.warn("Try to collect edge into unvisited vertex.");
                continue;
            case UNVISITED:
                if (!aggStateHolder.isResloved(incomingTitanVertex)) {
                    aggStateHolder.resolve(incomingTitanVertex);
                    queueToProcess.addLast(incomingVertex.getId());
                }
                rememberEdge(actualVertex, incomingTitanVertex, direction.opposite(), incomingVertex.getLabel(),
                        incomingVertex.getAttrs(), incomingVertex.getRevisionInterval());
                break;
            case VISITED:
            case FINISHED:
                rememberEdge(actualVertex, incomingTitanVertex, direction.opposite(), incomingVertex.getLabel(),
                        incomingVertex.getAttrs(), incomingVertex.getRevisionInterval());
                break;
            default:
                throw new IllegalStateException("Vertex " + incomingVertex + " has unknown state: " + nodeStatus + ".");
            }
        }
    }

    /**
     * Zapamatuje hranu.
     * @param startVertex start hrany
     * @param endVertex konec hrany
     * @param direction směr hrany
     * @param label label hrany
     * @param attributes atributy hrany
     * @throws TooManyEdgesException překročení maximálního počtu agregovaných hran
     */
    private void rememberEdge(Vertex startVertex, Vertex endVertex, Direction direction, String label,
            Map<String, Object> attributes, RevisionInterval revisionInterval) throws TooManyEdgesException {
        if (collectedEdges.size() > edgeMaximum) {
            throw new TooManyEdgesException(collectedEdges.size());
        }
        rememberEdgeWithoutControl(startVertex, endVertex, direction, label, attributes, revisionInterval);
    }

    /**
     * Zapamatuje hranu.
     * @param startVertex start hrany
     * @param endVertex konec hrany
     * @param direction směr hrany
     * @param label label hrany
     * @param attributes atributy hrany
     * @param revisionInterval interval revizí, ve kterých má hrana platnost 
     */
    private void rememberEdgeWithoutControl(Vertex startVertex, Vertex endVertex, Direction direction, String label,
            Map<String, Object> attributes, RevisionInterval revisionInterval) {
        collectedEdges.add(new FlowEdge(startVertex, endVertex, direction, EdgeLabel.parseFromDbType(label),
                revisionInterval, attributes));
    }
}
