package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowEdge;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.GraphFlow;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.TypeInfo;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Component;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.ComponentFactory;
import eu.profinit.manta.dataflow.repository.viewer.service.FilteredRelationshipService;
import eu.profinit.manta.dataflow.repository.viewer.service.ViewerOperation;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper.VertexViewStatusHolder.ProxyStatus;

/**
 * Třída pro udržování seznamu a stavu vertexů nalezených v rámci follow.
 * @author tfechtner
 *
 */
public class VerticesHolder {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(VerticesHolder.class);

    /** Transakce pro přístup do db. */
    private final TitanTransaction transaction;
    /** Provider pro zjištění úrovně jednotlivých typů v technologiích. */
    private final LevelMapProvider levelMapProvider;
    /** Stav flow. */
    private final FlowStateViewer flowState;
    /** Služba pro práci s předky. */
    private final FilteredRelationshipService relationshipService;
    /** Vnitřní třída pro ověřování stavu konců hran. */
    private final EdgeEndsChecker edgeEndsChecker = new EdgeEndsChecker();
    /** Objet držící stavy vrcholů. */
    private final VertexViewStatusHolder statusHolder;
    /** Množina id parentů, v kterých už byly vyhledání sourozenci.*/
    private final Set<Long> exploredParentsForSiblings = new HashSet<>();
    /** Faktory pro tvorbu jednotlivých komponent. */
    private final ComponentFactory componentFactory;
    /** Mapa potomků jednotlivých uzlů, používáno jako cache pro zrychlení db. */
    private final Map<Long, List<Vertex>> cachedChildren = new HashMap<>();

    /**
     * @param levelMapProvider provider pro zjištění úrovně jednotlivých typů v technologiích  
     * @param transaction transakce pro přístup db
     * @param flowState stav daného flow
     * @param relationshipService služba pro práci s předky
     * @param componentFactory Faktory pro tvorbu jednotlivých komponent.
     */
    public VerticesHolder(LevelMapProvider levelMapProvider, TitanTransaction transaction, FlowStateViewer flowState,
            FilteredRelationshipService relationshipService, ComponentFactory componentFactory) {
        this.levelMapProvider = levelMapProvider;
        this.transaction = transaction;
        this.flowState = flowState;
        this.relationshipService = relationshipService;
        this.statusHolder = new VertexViewStatusHolder(flowState);
        this.componentFactory = componentFactory;
    }

    /**
     * Vygeneruje seznam komponent pro view.
     * @param vertexEquivalencesHolder správce ekvivalencí vrcholů
     * @return seznam komponent
     */
    public Set<Component> generateComponents(VertexEquivalencesHolder vertexEquivalencesHolder) {
        Set<Component> components = new HashSet<>();

        for (Long id : statusHolder.getKnownVertices()) {
            if (vertexEquivalencesHolder.isEquivalenceFromOlderRev(id)) {
                continue;
            }

            Vertex actualVertex = transaction.getVertex(id);
            if (statusHolder.getStatus(id) == ProxyStatus.PROXY) {
                components.add(componentFactory.createProxyComponent(actualVertex, flowState, relationshipService,
                        levelMapProvider));
            } else {
                boolean isExpanded = statusHolder.isExpanded(actualVertex);
                components.add(componentFactory.createFullComponent(transaction, actualVertex, flowState,
                        levelMapProvider, isExpanded, relationshipService));
            }
        }

        return components;
    }

    /**
     * Vytvoří komponenty pro startovní uzly a jejich sourozence.
     * @param startNodes startovní uzly
     * @param newVisitedNodes vstupně/výstupní parametr pro nově navštívené uzly
     */
    public void createComponentsForStartNodes(Collection<Vertex> startNodes, Collection<Vertex> newVisitedNodes) {
        for (Vertex node : startNodes) {
            // přidat uzel do seznamu
            addVertex(node);
            // pořešit jeho stav
            if (GraphFlow.hasAdjacentVerticesInRefView(node, flowState)) {
                // má nějaké sousedy -> měl by být nastaven nejméně na visited
                NodeStatus status = flowState.getNodeStatus(node);
                if (status == NodeStatus.UNVISITED) {
                    flowState.visitNode(node);
                    newVisitedNodes.add(node);
                }
            } else {
                // nemá sousedy a pouštěly jsme na nej follow (je přece startovní) -> finish him
                flowState.finishNode(node);
                // do new visited uzlů ho dávat nemusíme, protože přece nemá žádné sousedy
                //   -> žádná agg hrana nemůže existovat
            }
            // objevit sourozence
            discoverSiblingOfNode(node, newVisitedNodes);
        }
    }

    /** 
     * Nalezne sourozence pro konce obdržených hran. Pokud jsou nalezení sourozenci nově navštíveni, jsou
     * přidání do pole s takovými uzly, pro další zpracování.
     * @param edges seznam hran, znichž se identifikují uzly, pro které se hledají sourozenci
     * @param newVisitedNodes výstupní parametr, kam se případně přidávají nově navštívení sourozenci
     */
    public void discoverSiblings(Collection<FlowEdge> edges, Collection<Vertex> newVisitedNodes) {
        // pro všechny nalezené hrany
        for (FlowEdge edge : edges) {
            discoverSiblingOfNode(transaction.getVertex(edge.getSource()), newVisitedNodes);
            discoverSiblingOfNode(transaction.getVertex(edge.getTarget()), newVisitedNodes);
        }
    }

    /**
     * Nalezne aotimické sourozence pro daný atomický uzel. <br> 
     * Pokud jde o nově navštívený, je přidán do seznamu nově navštívených.
     * @param actualNode uzel, pro který jsou hledáni sourozenci
     * @param newVisitedNodes výstupní parametr, kam se případně přidávají nově navštívení sourozenci
     */
    private void discoverSiblingOfNode(Vertex actualNode, Collection<Vertex> newVisitedNodes) {
        if (actualNode != null) {
            // sourozence objevujeme pouze pro atomic nevyfiltrované uzly
            if (relationshipService.isAtomic(actualNode, levelMapProvider, flowState.getRevisionInterval())
                    && !flowState.isVertexFiltered(actualNode)) {
                // prověříme předka, tedy pokud nějaký je
                Vertex parent = relationshipService.getParent(actualNode);
                // pokud jsme ještě neprozkoumali tohoto předka
                if (parent != null && !exploredParentsForSiblings.contains(parent.getId())) {
                    exploredParentsForSiblings.add((Long) parent.getId());
                    // navštívit parenta, protože již vidíme jeho potomky
                    flowState.visitNode(parent);
                    // jeho potomky chceme
                    List<Vertex> children = getChildren(parent);
                    for (Vertex child : children) {
                        if (!actualNode.getId().equals(child.getId())) {
                            NodeStatus childStatus = flowState.getNodeStatus(child);
                            // zobrazovat jen sourozence, co jsou v referencnim view a zároveň jsou atomic
                            if (childStatus != NodeStatus.UNKNOWN && relationshipService.isAtomic(child,
                                    levelMapProvider, flowState.getRevisionInterval())) {
                                if (statusHolder.getStatus(actualNode) == ProxyStatus.PROXY) {
                                    statusHolder.addProxy(child, statusHolder.isShouldBeParentProxy(actualNode));
                                } else {
                                    addVertex(child);
                                }

                                // pokusit se navštivit jen nenavštívené uzly
                                if (childStatus == NodeStatus.UNVISITED && flowState.visitNode(child)) {
                                    newVisitedNodes.add(child);
                                }
                            }
                        }
                    }

                }
            }
        } else {
            LOGGER.warn("During discovering sibling the node of the edge  is null.");
        }
    }

    /** 
     * Vytvoří komponenty na základě nových hran.
     * @param edges seznam nových hran
     * @param startNodes množina startovních uzlů daného follow
     */
    public void createComponentsFromEdges(Collection<FlowEdge> edges, Set<Vertex> startNodes) {
        for (FlowEdge e : edges) {
            Long sourceId = e.getSource();
            Vertex sourceVertex = transaction.getVertex(sourceId);
            boolean isSourceVertexStart = startNodes.contains(sourceVertex);
            if (flowState.getNodeStatus(sourceVertex) == NodeStatus.UNVISITED) {
                boolean shouldBeParentProxySource;
                if (isSourceVertexStart) {
                    shouldBeParentProxySource = false;
                } else {
                    shouldBeParentProxySource = true;
                }
                statusHolder.addProxy(sourceVertex, shouldBeParentProxySource);
            } else {
                addVertex(sourceVertex, isSourceVertexStart, isSourceVertexStart);
            }

            Long targetId = e.getTarget();
            Vertex targetVertex = transaction.getVertex(targetId);
            boolean isTargetVertexStart = startNodes.contains(targetVertex);
            if (flowState.getNodeStatus(targetVertex) == NodeStatus.UNVISITED) {
                boolean shouldBeParentProxySource;
                if (isTargetVertexStart) {
                    shouldBeParentProxySource = false;
                } else {
                    shouldBeParentProxySource = true;
                }
                statusHolder.addProxy(targetVertex, shouldBeParentProxySource);
            } else {
                addVertex(targetVertex, isTargetVertexStart, isTargetVertexStart);
            }
        }
    }

    /**
     * Vytvoří komponnty pro všechny předky všech obdržených komponent. <br>
     * Při zpracování nastaví správný status všem předkům.
     * @param startNodes množina startovních uzlů
     */
    public void processParents(Collection<Vertex> startNodes) {
        Set<Long> verticesToProcess = new HashSet<>(statusHolder.getKnownVertices());
        Set<Long> startNodeIds = new HashSet<>();
        for (Vertex vertex : startNodes) {
            startNodeIds.add((Long) vertex.getId());
        }

        ParentProcessor processor = new ParentProcessor();

        // napočítat potomky předků
        for (Long id : verticesToProcess) {
            List<Vertex> parents = relationshipService.getAllParents(transaction.getVertex(id));
            boolean isStartNode = startNodeIds.contains(id);
            processor.addParents(parents, isStartNode);
        }

        // zpracovat potomky
        for (Long id : verticesToProcess) {
            processor.processParent(transaction.getVertex(id));
        }

        processor.checkState();
    }

    /**
     * Ověří stav konců hran. Více viz {@link EdgeEndsChecker#checkEdgeEnds(FlowEdge)}.
     * @param edges hrany k ověření
     */
    public void checkEdgeEnds(Collection<FlowEdge> edges) {
        for (FlowEdge edge : edges) {
            edgeEndsChecker.checkEdgeEnds(edge);
        }
    }

    /**
     * Uloží do flowState, které uzly byly již poslány jako full.
     */
    public void persistFullNodes() {
        statusHolder.persistFullNodes();
    }

    /**
     * Převede všechny atomic potomky z proxy na full.
     * @param children seznam potomků k převedení
     */
    private void transformAtomicChildrenToFull(List<Vertex> children) {
        for (Vertex child : children) {
            if (statusHolder.getStatus(child) == ProxyStatus.PROXY
                    && relationshipService.isAtomic(child, levelMapProvider, flowState.getRevisionInterval())) {
                statusHolder.addFull(child, false);
                flowState.visitNode(child);
            }
        }
    }

    /**
     * Stav rozbalenosti uzlu.
     * @author tfechtner
     *
     */
    private enum ExpandStatus {
        /** Uzel má být rozbalen. */
        EXPANDED(true),
        /** Uzel má být sbalen. */
        COLLAPSED(false),
        /** Uzel je v mezistavu, má jak proxy tak full atomic potomky. */
        MIXED(true);

        private boolean isExpanded;

        private ExpandStatus(boolean isExpanded) {
            this.isExpanded = isExpanded;
        }

        /**
         * @return true, jestliže má být rozbalen
         */
        public boolean isExpanded() {
            return isExpanded;
        }
    }

    /**
     * Zjistí, jaký by měl být stav rozbalení uzlu, na zákaldě jeho potomků. <br>
     * - {@link ExpandStatus#EXPANDED} - pokud ani jeden potomek není proxy <br>
     * - {@link ExpandStatus#COLLAPSED} - všichni potomci jsou proxy <br>
     * - {@link ExpandStatus#MIXED} - někteří atomic potomci jsou proxy a někteří ne
     * @param children seznam potomků uzlu
     * @return cílený stav rozbalení
     */
    private ExpandStatus getExpandStatus(List<Vertex> children) {
        int proxyAtomicChildren = 0;
        int fullAtomicChildren = 0;
        //int proxyNonAtomicChildren = 0;
        int fullNonAtomicChildren = 0;
        for (Vertex child : children) {
            ProxyStatus vertexPStatus = statusHolder.getStatus(child);

            switch (vertexPStatus) {
            case FULL:
                if (relationshipService.isAtomic(child, levelMapProvider, flowState.getRevisionInterval())) {
                    fullAtomicChildren++;
                } else {
                    fullNonAtomicChildren++;
                }
                break;
            case PROXY:
                if (relationshipService.isAtomic(child, levelMapProvider, flowState.getRevisionInterval())) {
                    proxyAtomicChildren++;
                } // else proxy non atomic uzly nas nezajimaji
                break;
            case UNKNOWN:
                // unknown stav nic neurčuje
                break;
            default:
                throw new IllegalArgumentException("Undefined vertex proxy statux " + vertexPStatus + ".");
            }
        }

        if (fullAtomicChildren > 0 || fullNonAtomicChildren > 0) {
            if (proxyAtomicChildren > 0) {
                return ExpandStatus.MIXED;
            } else {
                return ExpandStatus.EXPANDED;
            }
        } else {
            return ExpandStatus.COLLAPSED;
        }
    }

    /**
     * Nastaví status uzlu. <br>
     * Pokud jsou všichni potomci {@link NodeStatus#FINISHED}, je uzel také {@link NodeStatus#FINISHED}. <br>
     * Pokud je alespoň jeden potomek {@link NodeStatus#VISITED}, je uzel také {@link NodeStatus#VISITED}. <br>
     * @param node uzel pro nastavení statusu 
     */
    private void updateStatus(Vertex node) {
        NodeStatus nodeStatus = flowState.getNodeStatus(node);
        if (nodeStatus != NodeStatus.FINISHED) {
            List<Vertex> children = getChildren(node);

            int visited = 0;
            int finished = 0;
            int unknown = 0;
            for (Vertex child : children) {
                NodeStatus childStatus = flowState.getNodeStatus(child);
                switch (childStatus) {
                case VISITED:
                    visited++;
                    break;
                case FINISHED:
                    finished++;
                    break;
                case UNKNOWN:
                    unknown++;
                    break;
                // ostatni stavy nas nezajimaji
                case UNVISITED:
                    break;
                default:
                    break;
                }
            }

            if (children.size() - unknown == finished) {
                flowState.finishNode(node);
            } else if (visited > 0 || finished > 0) {
                flowState.visitNode(node);
            }
        }
    }

    /**
     * Přidá uzel do mapy se stavy uzlů.
     * @param node uzel pro vložení
     */
    private void addVertex(Vertex node) {
        addVertex(node, true, false);
    }

    /**
     * Přidá uzel do mapy se stavy uzlů. <br>
     * Metoda zajistí jestli se má vložit proxy nebo full.
     * @param node uzel pro vložení
     * @param canBeExpanded true, jestliže může být rozbalen rozbalen
     * @param isStartNode true, jestliže jde o startovní uzel
     */
    private void addVertex(Vertex node, boolean canBeExpanded, boolean isStartNode) {
        TypeInfo typeInfo = levelMapProvider.getTypeInfo(ViewerOperation.getTechnologyType(node),
                ViewerOperation.getType(node));

        List<Vertex> children = getChildren(node);

        boolean isResourceDisabled = !isStartNode && flowState.isVertexFiltered(node);

        boolean isRoot = relationshipService.getParent(node) == null;
        boolean isNodeLevelHighEnough = isRoot || typeInfo.getLevel().isHigherThan(flowState.getFlowLevel());
        boolean shouldBeProxy = isAllChildrenShouldBeProxy(node);
        boolean isOneChildFull = isAtLeastOneChildFull(children);

        boolean normalStatus = !isResourceDisabled && isNodeLevelHighEnough && !shouldBeProxy;

        boolean inOneSiblingFull = isAtLeastOneSiblingFull(node);
        boolean inOneSiblingFullNotFiltered = inOneSiblingFull && !isResourceDisabled && !shouldBeProxy;

        boolean isTotalStart = flowState.isTotalStartNode((Long) node.getId())
                || flowState.isTotalStartNodeParent((Long) node.getId());

        if (isTotalStart || isOneChildFull || inOneSiblingFullNotFiltered || normalStatus) {
            boolean shouldBeExpanded = isHigherLevelOrNotLastEqual(children, typeInfo.getLevel());
            boolean isExpanded = canBeExpanded && shouldBeExpanded || isTotalStart;
            statusHolder.addFull(node, isExpanded);
        } else {
            statusHolder.addProxy(node, shouldBeProxy);
        }
    }

    /**
     * Ověří, že by měl být uzel rozbalen na základě své úrovně. <br>
     * Algoritmus je následující:
     * <ul>
     *  <li>má ostře vyšší úrověň než je úroveň flow -> rozbalit</li>
     *  <li>má stejnou úroveň a zároveň má alespoň jednoho full potomka stejné úrovně -> rozbalit</li>
     *  <li>v ostatních případech nerozbalovat</li>
     * </ul>    
     * @param children potomci daného uzlu
     * @param nodeLevel úroveň zkoumaného uzlu
     * @return true, jestliže jsou podmínky meody splněny
     */
    private boolean isHigherLevelOrNotLastEqual(List<Vertex> children, FlowLevel nodeLevel) {
        FlowLevel flowLevel = flowState.getFlowLevel();

        if (nodeLevel.equals(flowLevel)) {
            for (Vertex child : children) {
                if (statusHolder.getStatus(child) == ProxyStatus.FULL) {
                    FlowLevel childLevel = levelMapProvider
                            .getTypeInfo(ViewerOperation.getTechnologyType(child), ViewerOperation.getType(child))
                            .getLevel();
                    if (childLevel.equals(flowLevel)) {
                        return true;
                    }
                }
            }
            return false;
        } else {
            return nodeLevel.isHigherThan(flowLevel);
        }
    }

    /**
     * Zjistí, jestli pro daný vrchol existuje, alespoň jeden sourozenec, který je full.
     * @param node zkoumaný uzel
     * @return true, jestliže, alespoň jeden sourozenc je full
     */
    private boolean isAtLeastOneSiblingFull(Vertex node) {
        List<Vertex> siblingList = getChildren(relationshipService.getParent(node));
        for (Vertex sibling : siblingList) {
            if (sibling.equals(node)) {
                continue;
            }
            if (statusHolder.getStatus(sibling) == ProxyStatus.FULL) {
                return true;
            }
        }
        return false;
    }

    /**
     * Zjistí, jestli alespoň jeden potomek není proxy.
     * @param children seznam potomků
     * @return true, jestliže alespoň jeden potomek je full
     */
    private boolean isAtLeastOneChildFull(List<Vertex> children) {
        for (Vertex child : children) {
            if (statusHolder.getStatus(child) == ProxyStatus.FULL) {
                return true;
            }
        }
        return false;
    }

    /**
     * Zjistí jestliže mají všichni potoci flag {@link VertexStatus#shouldBeParentProxy}.
     * @param node zkoumaný parent
     * @return true, jestliže mají všichni potomci daný flag, nebo je jejich {@link VertexStatus} null,
     * přičemž musí být alespoň jeden status potomka různý od null
     */
    private boolean isAllChildrenShouldBeProxy(Vertex node) {
        List<Vertex> children = getChildren(node);
        boolean atLeastOneShouldBe = false;
        for (Vertex child : children) {
            ProxyStatus proxyStatus = statusHolder.getStatus(child);
            if (proxyStatus != ProxyStatus.UNKNOWN) {
                if (!statusHolder.isShouldBeParentProxy(child)) {
                    return false;
                } else {
                    atLeastOneShouldBe = true;
                }
            }
        }
        return atLeastOneShouldBe;
    }

    /**
     * Získá potomky pro daný uzel, přičemž používá cache.
     * @param parent uzel, pro který se hledají potomci
     * @return potomci daného uzlu nebo prázdný list, pokud je parent null
     */
    private List<Vertex> getChildren(Vertex parent) {
        if (parent != null) {
            List<Vertex> children = cachedChildren.get(parent.getId());
            if (children == null) {
                children = relationshipService.getDirectChildren(parent, flowState.getRevisionInterval());
                cachedChildren.put((Long) parent.getId(), children);
            }
            return children;
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * Třída pro optimalizované zpracování předky.
     * Pracuje tak, že si nejprve napočítá, kolik uvažovaných potomků mají všichni předci.
     * Pokud dojde k vyřešení nějaké uzlu, sníží se počet čekajících u jeho předka.
     * Pokud má takový předek už nula čekajících potomků je také zpracován.
     * @author tfechtner
     *
     */
    private class ParentProcessor {
        /** Mapa počítadel potomků pro jednotlivé předky. */
        private Map<Long, ChildCounter> parentStatusMap = new HashMap<>();
        /** Množina všech startovních uzlů včetně hirerachie. */
        private Set<Long> startNodesSet = new HashSet<>();
        /** Mapa rozbalenosti uzlů. */
        private Map<Long, ExpandStatus> expandStatusMap = new HashMap<>();

        /**
         * Přidá do seznamu všechny předky.
         * @param parents seznam předků k přidání v sekvenci od prvního po root
         * @param isStartNode true, jestliže jde o startovní hierarchii
         */
        public void addParents(List<Vertex> parents, boolean isStartNode) {
            boolean isMerged = false;
            for (Vertex p : parents) {
                ChildCounter counter = parentStatusMap.get(p.getId());
                if (counter == null) {
                    counter = new ChildCounter();
                    parentStatusMap.put((Long) p.getId(), counter);
                } else {
                    // pokračovat pouze pokud jsme se připojili do již existujícího stromu nově
                    if (!isMerged) {
                        isMerged = true;
                    } else {
                        break;
                    }
                }
                counter.addChild();
            }

            if (isStartNode) {
                for (Vertex p : parents) {
                    startNodesSet.add((Long) p.getId());
                }
            }
        }

        /**
         * Ověří, že všechny počítadla jsou v pořádku dokončena.
         */
        public void checkState() {
            for (Entry<Long, ChildCounter> entry : parentStatusMap.entrySet()) {
                entry.getValue().check(entry.getKey());
            }
        }

        /**
         * Zpracuje předka daného uzlu.
         * @param actualVertex uzel, jehož předek se má zpracovat
         */
        public void processParent(Vertex actualVertex) {
            Vertex parent = relationshipService.getParent(actualVertex);
            if (parent != null) {
                ChildCounter counter = parentStatusMap.get(parent.getId());
                if (counter == null) {
                    throw new IllegalStateException("ChildCounter for vertex " + parent + " is null.");
                }

                if (counter.childResolved()) {
                    process(parent);
                }
            }
        }

        /**
         * Zpracuje jeden uzel a případně jeho předka.
         * @param actualVertex uzel ke zpracování
         */
        private void process(Vertex actualVertex) {
            if (actualVertex != null) {
                Long actualId = (Long) actualVertex.getId();
                updateStatus(actualVertex);

                List<Vertex> children = getChildren(actualVertex);
                ExpandStatus expandStatus = getExpandStatus(children);
                expandStatusMap.put(actualId, expandStatus);
                if (expandStatus == ExpandStatus.MIXED) {
                    transformAtomicChildrenToFull(children);
                }
                addVertex(actualVertex, expandStatus.isExpanded(), startNodesSet.contains(actualId));
                processParent(actualVertex);
                // znovu project ještě potomky, kvůli podmínce isAtLeastOneSiblingFull
                processChildren(children);
            }
        }

        /**
         * Znovu zpracuje potomky, ale pouze takové, u nichž známe expand status (nejsou to listy).
         * @param children množina potomků ke zpracování
         */
        private void processChildren(List<Vertex> children) {
            for (Vertex child : children) {
                Long childId = (Long) child.getId();
                ExpandStatus expandStatus = expandStatusMap.get(childId);
                if (expandStatus != null) {
                    addVertex(child, expandStatus.isExpanded(), startNodesSet.contains(childId));
                }
            }
        }
    }

    /**
     * Počítadlo potomků.
     * @author tfechtner
     *
     */
    private static class ChildCounter {
        /** Počet potomků, které patří do daného follow. */
        private int count = 0;

        /**
         * Přidá potomka do počítadla.
         */
        public void addChild() {
            count++;
        }

        /**
         * Označí, že byl jeden potomek vyřešen.
         * @return true, jestliže už byli všichni potomci vyřešeni
         */
        public boolean childResolved() {
            count--;

            if (count == 0) {
                return true;
            } else if (count < 0) {
                throw new IllegalStateException("Child count has been decreased to zero.");
            } else {
                return false;
            }
        }

        /**
         * Zkontroluje, že už byl daný čítač vyřešen.
         * @param id id vrcholu
         */
        public void check(Long id) {
            if (count != 0) {
                throw new IllegalStateException("The vertex " + id + " has counter " + count + ".");
            }
        }

        @Override
        public String toString() {
            return "ChildCounter [c:" + count + "]";
        }
    }

    /**
     * Třída zajišťující ověřování stavu (proxy/full) konců hran. <br>
     * Cíl je, aby šlo hranu vykreslit i když má společného předka a některý z konců hrany je proxy.
     * @author tfechtner
     *
     */
    private final class EdgeEndsChecker {
        /**
         * Metoda ověří, že koncové (start i cíl) vrcholy hrany jsou zobrazitelné. <br>
         * Případy:
         * <ol>
         *  <li>alespoň jeden z konců je unvisited -> ok </li> 
         *  <li>oba konce nejsou proxy -> ok </li>
         *  <li>oba konce, včetně celé jejich hierarchie předků jsou proxy -> ok </li>
         *  <li>jeden z konců včetně celé hierarchie předků je proxy, přičemž druhý nikoliv -> ok??? </li>
         *  <li>proxy konec patří do vyfiltrovaného resource -> ok </li>
         *  <li>
         *      alespoň jeden z konců je proxy a v jeho hierarchii se vyskytuje neproxy uzel -> možný problém:
         *      <ol>
         *          <li>nechť <b>SP</b> je první společný předek pro oba konce</li>
         *          <li>pokud SP neexistuje -> ok a konec</li>
         *          <li>pokud jsou oba uzly proxy a všechni jejich předci 
         *          před společný uzel jsou také proxy -> ok</li>
         *          <li>pro proxy konce provedeme následující:
         *              <ol>
         *                  <li>jestliže pro daný konec existuje alespoň jeden neproxy předek před SP -> ok</li>
         *                  <li>nechť <b>NP</b> je nejvzdálenější předek daného konce před SP</li>
         *                  <li>když NP existuje -> změnit jeho stav z proxy na full -> ok</li>
         *                  <li>NP neexistuje -> změnit stav daného konce z proxy na full</li>
         *              </ol>
         *         </li>     
         *      </ol>
         *  </li>
         * </ol> 
         * @param edge hrana k ověření
         */
        public void checkEdgeEnds(FlowEdge edge) {
            Long sourceId = edge.getSource();
            Long targetId = edge.getTarget();

            NodeStatus sourceNodeStatus = flowState.getNodeStatus(edge.getSource());
            NodeStatus targetNodeStatus = flowState.getNodeStatus(edge.getTarget());
            if (sourceNodeStatus == NodeStatus.UNVISITED || targetNodeStatus == NodeStatus.UNVISITED) {
                // ok případ 1
                return;
            }

            ProxyStatus sourceStatus = statusHolder.getStatus(sourceId);
            ProxyStatus targetStatus = statusHolder.getStatus(targetId);

            if (sourceStatus == ProxyStatus.PROXY || targetStatus == ProxyStatus.PROXY) {
                Vertex sourceNode = transaction.getVertex(sourceId);
                Vertex targetNode = transaction.getVertex(targetId);
                if (sourceNode == null || targetNode == null) {
                    return;
                }

                boolean isWholeSourceHierarchyProxy = isHierarchyProxy(sourceNode, null);
                boolean isWholeTargetHierarchyProxy = isHierarchyProxy(targetNode, null);

                if (!isWholeSourceHierarchyProxy || !isWholeTargetHierarchyProxy) {
                    boolean isSourceProxyAndNotFiltered = sourceStatus == ProxyStatus.PROXY
                            && !flowState.isVertexFiltered(sourceNode);
                    boolean isTargetProxyAndNotFiltered = targetStatus == ProxyStatus.PROXY
                            && !flowState.isVertexFiltered(targetNode);

                    if (isSourceProxyAndNotFiltered || isTargetProxyAndNotFiltered) {
                        Vertex commonAncestor = findCommonAncestor(sourceNode, targetNode);
                        if (commonAncestor != null) {
                            boolean isSourceHierarchyBeforeProxy = isHierarchyProxy(sourceNode, commonAncestor);
                            boolean isTargetHierarchyBeforeProxy = isHierarchyProxy(targetNode, commonAncestor);

                            if (!isSourceHierarchyBeforeProxy || !isTargetHierarchyBeforeProxy) {
                                // source je proxy -> prověřit 
                                if (sourceStatus == ProxyStatus.PROXY) {
                                    checkNode(sourceNode, commonAncestor);
                                }
                                // target je proxy -> prověřit
                                if (targetStatus == ProxyStatus.PROXY) {
                                    checkNode(targetNode, commonAncestor);
                                }
                            } // else případ 6.3 -> ok
                        } // else případ 6.2 -> ok  
                    } // else případ 5 -> ok
                } // else případ 3 a 4 -> ok                
            } // else případ 2 -> ok
        }

        /**
         * Ověří jestli je uzel a hierarchie až před zarážku proxy.
         * @param node ověřovaný uzel
         * @param commonAncestor předek, před který se kontroluje
         * @return true, jestliže je uzel a jeho hirerachie před zarážku proxy
         */
        private boolean isHierarchyProxy(Vertex node, Vertex commonAncestor) {
            if (statusHolder.getStatus(node) == ProxyStatus.PROXY) {
                List<Vertex> parents = relationshipService.getAllParents(node);
                for (Vertex p : parents) {
                    if (commonAncestor != null && commonAncestor.equals(p)) {
                        break;
                    }
                    if (statusHolder.getStatus(p).couldBeFull()) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        }

        /**
         * Nalezne prvního společného předka pro dané uzly.
         * @param node1 první uzel pro porovnání (na pořadí nezáleží)
         * @param node2 druhý uzel pro porovnání (na pořadí nezáleží) 
         * @return první společný předek nebo null, pokud žádný neexistuje
         */
        private Vertex findCommonAncestor(Vertex node1, Vertex node2) {
            List<Vertex> parents1 = relationshipService.getAllParents(node1);
            List<Vertex> parents2 = relationshipService.getAllParents(node2);

            for (Vertex testedVertex : parents1) {
                if (parents2.contains(testedVertex)) {
                    return testedVertex;
                }
            }

            return null;
        }

        /**
         * Zkontroluje jestli daný konec hrany odpovídá požadavkům viz 
         * {@link EdgeEndsChecker#checkEdgeEnds(FlowEdge)}.<br>
         * Metoda pokrývá krok 6.4. 
         * @param node kontrolovaný uzel
         * @param commonAncestor společný předek konců hran
         */
        private void checkNode(Vertex node, Vertex commonAncestor) {
            List<Vertex> parents = relationshipService.getAllParents(node);
            Vertex fullParent = findFullParentBefore(parents, commonAncestor);
            if (fullParent == null) {
                Vertex furthermostParent = findFurthermostParentBefore(parents, commonAncestor);
                if (furthermostParent != null) {
                    // případ 6.4.3
                    statusHolder.addFull(furthermostParent, false);
                } else {
                    // případ 6.4.4
                    statusHolder.addFull(node, false);
                }
            } // else případ 6.4.1 -> ok            
        }

        /**
         * Najde prvního full předka, který je ještě před zarážkou.
         * @param parents seznam předků
         * @param commonAncestor předek, před kterým se nový hledá
         * @return první full předek, který je ještě před zařážkou, 
         */
        private Vertex findFullParentBefore(List<Vertex> parents, Vertex commonAncestor) {
            for (Vertex v : parents) {
                if (v.equals(commonAncestor)) {
                    return null;
                } else {
                    if (statusHolder.getStatus(v) == ProxyStatus.FULL) {
                        return v;
                    }
                }
            }

            return null;
        }

        /**
         * Najde nejvzdálenějšího předka, který je ještě před zarážkou.
         * @param parents seznam předků
         * @param commonAncestor předek, před kterým se nový hledá
         * @return nejvzdálenější předek před zařážkou, pokud takový neexistuje vrátí null
         */
        private Vertex findFurthermostParentBefore(List<Vertex> parents, Vertex commonAncestor) {
            Vertex furthermost = null;
            for (Vertex v : parents) {
                if (v.equals(commonAncestor)) {
                    break;
                }
                furthermost = v;
            }

            return furthermost;
        }
    }
}