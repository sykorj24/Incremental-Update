package eu.profinit.manta.dataflow.repository.viewer.exception;

import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;

/**
 * Výjimka pro případ, kdy nejsou commitnutá žádná data.
 * @author tfechtner
 *
 */
public class NoCommitedDataException extends RuntimeException {
    
    private static final long serialVersionUID = -2922258512358671467L;

    /**
     * Konstruktor přejímající hlášku z {@link RevisionRootHandler#NO_REVISION_IN_REP}.
     */
    public NoCommitedDataException() {        
        super(RevisionRootHandler.NO_REVISION_IN_REP);
    }

    /**
     * @param message textace chyby
     */
    public NoCommitedDataException(String message) {
        super(message);
    }

}
