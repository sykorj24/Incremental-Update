package eu.profinit.manta.dataflow.repository.viewer.exception;

/**
 * Výjimka vyhazovaná v případě příliš mnoho agregovaných hran.
 * @author tfechtner
 *
 */
public class TooManyEdgesException extends Exception {
    private static final long serialVersionUID = 600743940271270567L;

    private final int edgeCount;

    /**
     * @param edgeCount počet hran způsobujících výjimku
     */
    public TooManyEdgesException(int edgeCount) {
        super();
        this.edgeCount = edgeCount;
    }

    /**
     * @return  počet hran způsobujících výjimku
     */
    public int getEdgeCount() {
        return edgeCount;
    }
}
