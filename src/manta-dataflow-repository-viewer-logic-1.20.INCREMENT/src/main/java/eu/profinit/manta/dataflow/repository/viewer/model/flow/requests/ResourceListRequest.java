package eu.profinit.manta.dataflow.repository.viewer.model.flow.requests;


/**
 * Request pro command na zjištění seznamu resource.
 * @author tfechtner
 *
 */
public class ResourceListRequest implements ServerRequest {
    /** Id kořene grafu databáze. */
    private Long rootId;

    /**
     * @return id kořene grafu databáze
     */
    public Long getRootId() {
        return rootId;
    }

    /**
     * @param rootId id kořene grafu databáze
     */
    public void setRootId(Long rootId) {
        this.rootId = rootId;
    }
}
