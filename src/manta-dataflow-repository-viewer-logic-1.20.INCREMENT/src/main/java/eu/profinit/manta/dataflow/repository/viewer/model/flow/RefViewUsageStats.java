package eu.profinit.manta.dataflow.repository.viewer.model.flow;

import java.util.HashMap;
import java.util.Map;

import eu.profinit.manta.dataflow.repository.utils.NodeTypeDef;
import eu.profinit.manta.dataflow.repository.viewer.model.index.WelcomeFormModel;

/**
 * @author tfechtner
 *
 */
public class RefViewUsageStats {
    /** Doba počítání ref view v ms.*/
    private long refViewTime;
    /** Počet uzlů v ref view. */
    private int refViewSize;
    /** Případná chyba v rámci ref view. */
    private String refViewError;
    /** Doba počítání init follow v ms. */
    private long initFollowTime;
    /** Velikost vizualizace po prvním follow.  */
    private FollowSize initFollowSize;
    /** True, jestliže jde o vizualizaci z permalinku. */
    private boolean isFromPermaLink = false;
    /** Zpráva obsahující chybu o zmenšení.  */
    private String scopeMessage;
    /** Nastavení formuláře pro vizualizaci. */
    private WelcomeFormModel form = new WelcomeFormModel();
    /** množina startovních uzlů s počty výskytů. */
    private Map<NodeTypeDef, Integer> startNodeMap = new HashMap<>();
    /** GUID flow stavu. */
    private String flowStateGuid;

    /**
     * @return GUID flow stavu
     */
    public String getFlowStateGuid() {
        return flowStateGuid;
    }

    /**
     * @param flowStateGuid GUID flow stavu
     */
    public void setFlowStateGuid(String flowStateGuid) {
        this.flowStateGuid = flowStateGuid;
    }

    /**
     * @return Doba počítání ref view v ms.
     */
    public long getRefViewTime() {
        return refViewTime;
    }

    /**
     * @param refViewProcessTime Doba počítání ref view v ms.
     */
    public void setRefViewTime(long refViewProcessTime) {
        this.refViewTime = refViewProcessTime;
    }

    /**
     * @return True, jestliže jde o vizualizaci z permalinku
     */
    public boolean isFromPermaLink() {
        return isFromPermaLink;
    }

    /**
     * @param isGeneratedFromPermaLink True, jestliže jde o vizualizaci z permalinku
     */
    public void setFromPermaLink(boolean isGeneratedFromPermaLink) {
        this.isFromPermaLink = isGeneratedFromPermaLink;
    }

    /**
     * @return Doba počítání init follow v ms
     */
    public long getInitFollowTime() {
        return initFollowTime;
    }

    /**
     * @param initFollowProcessTime Doba počítání init follow v ms
     */
    public void setInitFollowTime(long initFollowProcessTime) {
        this.initFollowTime = initFollowProcessTime;
    }

    /**
     * @return Zpráva obsahující chybu o zmenšení
     */
    public String getScopeMessage() {
        return scopeMessage;
    }

    /**
     * @param scopeMessage Zpráva obsahující chybu o zmenšení
     */
    public void setScopeMessage(String scopeMessage) {
        this.scopeMessage = scopeMessage;
    }

    /**
     * @param updatedForm Zpráva obsahující chybu o zmenšení
     */
    public void updateForm(WelcomeFormModel updatedForm) {
        form.setLevel(updatedForm.getLevel());
        form.setDirection(updatedForm.getDirection());
        form.setFilterEdges(updatedForm.isFilterEdges());
        form.setDepth(updatedForm.getDepth());
        form.setFilter(updatedForm.getFilter());
        form.setRevision(updatedForm.getRevision());
        form.setOlderRevision(updatedForm.getOlderRevision());
        form.setSelectedItems(null);
        form.setHash(null);
    }

    /**
     * @return Nastavení formuláře pro vizualizaci
     */
    public WelcomeFormModel getForm() {
        return form;
    }

    /**
     * @return Počet uzlů v ref view
     */
    public int getRefViewSize() {
        return refViewSize;
    }

    /**
     * @param refViewSize Počet uzlů v ref view
     */
    public void setRefViewSize(int refViewSize) {
        this.refViewSize = refViewSize;
    }

    /**
     * @return Případná chyba v rámci ref view
     */
    public String getRefViewError() {
        return refViewError;
    }

    /**
     * @param refViewError Případná chyba v rámci ref view
     */
    public void setRefViewError(String refViewError) {
        this.refViewError = refViewError;
    }

   /**
     * @return Velikost vizualizace po prvním follow
     */
    public FollowSize getInitFollowSize() {
        return initFollowSize;
    }

    /**
     * @param initFollowSize Velikost vizualizace po prvním follow
     */
    public void setInitFollowSize(FollowSize initFollowSize) {
        this.initFollowSize = initFollowSize;
    }

    /**
     * @return množina startovních uzlů s počty výskytů
     */
    public Map<NodeTypeDef, Integer> getStartNodes() {
        return startNodeMap;
    }
    
    /**
     * @param startNodeMap množina startovních uzlů s počty výskytů
     */
    public void setStartNodeMap(Map<NodeTypeDef, Integer> startNodeMap) {
        this.startNodeMap = startNodeMap;
    }

    /**
     * DTO o velikosti follow.
     * @author tfechtner
     *
     */
    public static class FollowSize {
        /** počet plných komponent. */
        private final int fullComponents;
        /** počet proxy komponent. */
        private final int proxyComponents;
        /** počet hran. */
        private final int edges;

        /**
         * @param fullComponentCount počet plných komponent
         * @param proxyComponentCount počet proxy komponent
         * @param edgeCount počet hran
         */
        public FollowSize(int fullComponentCount, int proxyComponentCount, int edgeCount) {
            super();
            this.fullComponents = fullComponentCount;
            this.proxyComponents = proxyComponentCount;
            this.edges = edgeCount;
        }

        /**
         * @return počet plných komponent
         */
        public int getFullComponents() {
            return fullComponents;
        }

        /**
         * @return počet proxy komponent
         */
        public int getProxyComponents() {
            return proxyComponents;
        }

        /**
         * @return počet hran
         */
        public int getEdges() {
            return edges;
        }
    }
}
