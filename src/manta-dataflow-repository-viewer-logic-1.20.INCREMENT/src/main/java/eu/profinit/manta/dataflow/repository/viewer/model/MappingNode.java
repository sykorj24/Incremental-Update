package eu.profinit.manta.dataflow.repository.viewer.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Odkaz na uzel, ktery se ucastni mapovani.
 *
 * @author onouza
 */
public class MappingNode {
    /** ID odkazovaneho uzlu */
    private Long id;
    /** Cesta k odakzovanemu uzlu (vcetne nazvu tohoto uzlu) */
    private List<String> path = new ArrayList<>();
    
    /**
     * @return ID odkazovaneho uzlu
     */
    public Long getId() {
        return id;
    }

    /**
     * @param ID odkazovaneho uzlu
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return Cesta k odakzovanemu uzlu (vcetne nazvu tohoto uzlu)
     */
    public List<String> getPath() {
        return path;
    }

    /**
     * @param path Cesta k odakzovanemu uzlu (vcetne nazvu tohoto uzlu)
     */
    public void setPath(List<String> path) {
        this.path = path;
    }
    
}
