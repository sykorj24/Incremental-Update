package eu.profinit.manta.dataflow.repository.viewer.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * Initiator pro viewer.
 * @author tfechtner
 *
 */
public class ViewerModelInitiator extends HandlerInterceptorAdapter {
    /** Link, kam směřuje logo v hlavičce. Může obsahovat {contextPath}, za což se doplní context path requestu. */
    private String logoLink;

    @Override
    public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler,
            final ModelAndView modelAndView) throws Exception {
        if (modelAndView == null || modelAndView.getViewName().startsWith("redirect")) {
            return; //neni co nastavovat, nezpracovaval controller, nebo je to redirect
        }

        modelAndView.addObject("logoLink", logoLink.replace("{contextPath}", request.getContextPath()));
    }

    /**
     * @return link, kam směřuje logo v hlavičce.
     */
    public String getLogoLink() {
        return logoLink;
    }

    /**
     * @param logoLink link, kam směřuje logo v hlavičce. 
     * Může obsahovat {contextPath}, za což se doplní context path requestu.
     */
    public void setLogoLink(String logoLink) {
        this.logoLink = logoLink;
    }
}
