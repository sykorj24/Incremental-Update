package eu.profinit.manta.dataflow.repository.viewer.model.index;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message;

/**
 * Odpověď na přímý výběr elementů.
 * @author tfechtner
 *
 */
public class FindElementsResponse {
    /** Množina nalezených elementů. */
    private Set<Node> elements = Collections.emptySet();
    /** Seznam zpráv s problémy v rámci hledání. */
    private List<Message> messages = Collections.emptyList();

    /** Default konstruktor. */
    public FindElementsResponse() {
    }
    
    /**
     * Konstruktor pro případ jedné zprávy, typicky odpověď v případě vážné chyby.
     * @param message zpráva k zobrazení
     */
    public FindElementsResponse(Message message) {
        messages = new ArrayList<>();
        messages.add(message);
    }

    /**
     * @return Množina nalezených elementů.
     */
    public Set<Node> getElements() {
        return elements;
    }

    /**
     * @param elements Množina nalezených elementů.
     */
    public void setElements(Set<Node> elements) {
        this.elements = elements;
    }

    /**
     * @return aeznam zpráv s problémy v rámci hledání.
     */
    public List<Message> getMessages() {
        return messages;
    }

    /**
     * @param messages aeznam zpráv s problémy v rámci hledání.
     */
    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
