package eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Stav objektu v příslušeném intervalu revizí.
 * @author tfechtner
 *
 */
public enum RevisionState {
    /** Platnost &lt;start, end&gt;. */
    STABLE,
    /** Platnost (start, end). */
    INNER,
    /** Platnost &lt;start, end). */
    DELETED,
    /** Platnost (start, end&gt;. */
    NEW;

    /**
     * Zjistí platnost vrcholu.
     * @param vertex zkoumaný vrchol
     * @param comparingInterval vztažený interval revizí
     * @return platnost vrcholu v daném intervalu
     */
    public static RevisionState resolve(Vertex vertex, RevisionInterval comparingInterval) {
        if (comparingInterval.getStart() == comparingInterval.getEnd()) {
            return RevisionState.STABLE;
        }

        Edge controlEdge = GraphOperation.getNodeControlEdge(vertex);
        return resolve(controlEdge, comparingInterval);
    }

    /**
     * Zjistí platnost hrany.
     * @param edge zkoumaná hrana
     * @param comparingInterval vztažený interval revizí
     * @return platnost hrany v daném intervalu
     */
    public static RevisionState resolve(Edge edge, RevisionInterval comparingInterval) {
        if (comparingInterval.getStart() == comparingInterval.getEnd()) {
            return RevisionState.STABLE;
        }

        RevisionInterval elementInterval = RevisionUtils.getRevisionInterval(edge);
        return resolve(elementInterval, comparingInterval);
    }

    /**
     * Zjistí platnost intervalu.
     * @param elementInterval zkoumaný interval
     * @param comparingInterval vztažený interval revizí
     * @return platnost zkoumaného intervalu vůči vztaženému
     */
    public static RevisionState resolve(RevisionInterval elementInterval, RevisionInterval comparingInterval) {
        if (elementInterval.getStart() > comparingInterval.getStart()
                && elementInterval.getEnd() < comparingInterval.getEnd()) {
            return RevisionState.INNER;
        } else if (elementInterval.getStart() <= comparingInterval.getStart()
                && elementInterval.getEnd() >= comparingInterval.getEnd()) {
            return RevisionState.STABLE;
        } else if (elementInterval.getStart() <= comparingInterval.getStart()
                && elementInterval.getEnd() < comparingInterval.getEnd()) {
            return RevisionState.DELETED;
        } else if (elementInterval.getStart() > comparingInterval.getStart()
                && elementInterval.getEnd() >= comparingInterval.getEnd()) {
            return RevisionState.NEW;
        } else {
            throw new IllegalStateException("Uncomparable rev intervals - element: " + elementInterval.toString()
                    + ", orig: " + comparingInterval.toString() + ".");
        }
    }

}
