package eu.profinit.manta.dataflow.repository.viewer.service.index;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.viewer.model.index.Node;

import java.util.List;

/**
 * Sluzba zajistuje vyhledavani uzlu v databazi.
 * 
 * @author Matyas Krutsky <matyas.krutsky@profinit.eu>
 */
public interface IndexService {

    /**
     * Vyhleda uzly, ktere odpovidaji zadanemu vyhledavacimu patternu.
     * @param pattern neprazdny string podle ktereho hledame uzly 
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return Seznam uzlu odpovidajicich paternu, nebo prazdny seznam, 
     * pokud nejsou nalezeny zadne uzly
     */
    List<Node> findItems(String pattern, RevisionInterval revisionInterval);

    /**
     * Vyhleda uzly na zadane ceste podle hierarchie. 
     * @param path cesta k pozadovanym uzlum v podobě idéček vrcholů 
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return seznam uzlu na zadane ceste nebo prazdny seznam pokud nejsou 
     * na zadane ceste zadne uzly, nebo je cesta nevalidni/neexistuje
     */
    List<Node> getNodesOnPath(List<Long> path, RevisionInterval revisionInterval);

    /**
     * Vyhleda uzly podle jejich id.
     * @param ids seznam id uzlu, ktere chci dohledat, pokud uzel s predanym id neni nalezen, id je ignorovano
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return pole uzlu, ktera maji zadana idcka, nebo prazdne pole, pokud je seznam id prazdny, nebo zadny
     * uzel s predanym id neni nalezen
     */
    List<Node> getNodes(List<String> ids, RevisionInterval revisionInterval);
    
    /**
     * Nalezne všechny uzly odpovídající dané cestě. Cesta se myslí kvalifikované jméno.<br>
     * Přičemž se prohledává odspodu, tedy není třeba plně kvalifikovaná cesta. 
     * @param path cesta pro hledaný uzel
     * @param revisionInterval interval revizí, ve kterých se uzly hledají
     * @return seznam nalezených uzlů, nikdy null
     */
    List<Node> findItemsByPath(String[] path, RevisionInterval revisionInterval);
}
