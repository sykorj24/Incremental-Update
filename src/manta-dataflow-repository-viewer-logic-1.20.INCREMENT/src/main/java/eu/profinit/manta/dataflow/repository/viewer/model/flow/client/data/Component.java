package eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.service.FilteredRelationshipService;
import eu.profinit.manta.dataflow.repository.viewer.service.ViewerOperation;

/**
 * Předek pro datové třídy obsahující data o uzlech pro klienta.
 * @author tfechtner
 *
 */
public abstract class Component {
    /** Id uzlu. */
    private final Object id;
    /** Přepínáš, jestliže jde o proxy objekt. */
    private final boolean proxy;
    /** Id předka.*/
    private final Object parentID;
    /** Aktuální status uzlu v rámci view. */
    private final NodeStatus status;
    /** Technologie neboli resource uzlu. */
    private final String tech;
    /** True, jestliže je daný uzel vyfiltrovaný. */
    private final boolean filtered;
    /** Id top parent resource. */
    private final Object topParentTech;
    /** Úroveň uzlu ve flow. */
    private final int level;

    /**
     * @param vertex uzel, pro který se vytváří komponenta
     * @param isProxy true, jestliže je komponenta proxy
     * @param relationshipService služba pro práci s předky 
     * @param flowState stav daného flow
     * @param levelMapProvider provider pro získání definice úrovně a atomicity uzlu
     */
    protected Component(Vertex vertex, boolean isProxy, 
            FilteredRelationshipService relationshipService, FlowStateViewer flowState,
            LevelMapProvider levelMapProvider) {
        this.id = vertex.getId();
        this.proxy = isProxy;
        this.parentID = getParent(vertex, relationshipService);
        this.status = flowState.getNodeStatus(vertex);
        this.tech = ViewerOperation.getTechnology(vertex);
        Vertex topParentResource = GraphOperation.getTopParentResource(vertex);
        this.topParentTech = topParentResource.getId();
        this.filtered = !flowState.isTotalStartNode((Long) this.id) && !flowState.isTotalStartNodeParent((Long) this.id)
                && flowState.isVertexCustomFiltered(vertex);
        this.level = ViewerOperation.getLevel(vertex, levelMapProvider).getLevel();
    }

    /**
     * @param id Id uzlu.
     * @param proxy Přepínáš, jestliže jde o proxy objekt.
     * @param parentID Id předka.
     * @param status Aktuální status uzlu v rámci view.
     * @param tech Technologie neboli resource uzlu.
     * @param filtered True, jestliže je daný uzel vyfiltrovaný.
     * @param topParentTech Id top parent resource.
     * @param level Úroveň uzlu ve flow. 
     */
    protected Component(Object id, boolean proxy, Object parentID, NodeStatus status, String tech, boolean filtered,
            Object topParentTech, int level) {
        super();
        this.id = id;
        this.proxy = proxy;
        this.parentID = parentID;
        this.status = status;
        this.tech = tech;
        this.filtered = filtered;
        this.topParentTech = topParentTech;
        this.level = level;
    }

    /**
     * @return id uzlu
     */
    public Object getId() {
        return id;
    }

    /**
     * @return true, jestliže jde o proxy objekt
     */
    public boolean isProxy() {
        return proxy;
    }

    /**
     * @return parentID
     */
    public Object getParentID() {
        return parentID;
    }

    /**
     * @return Aktuální status uzlu v rámci view.
     */
    public NodeStatus getStatus() {
        return status;
    }

    /**
     * @return Technologie neboli resource uzlu.
     */
    public String getTech() {
        return tech;
    }

    /**
     * @return true, jestliže je daný uzel vyfiltrován
     */
    public boolean isFiltered() {
        return filtered;
    }

    /**
     * @return Id top parent resource.
     */
    public Object getTopParentTech() {
        return topParentTech;
    }

    /**
     * @return úroveň uzlu ve follow (TOP, MIDDLE, BOTTOM)
     */
    public int getLevel() {
        return level;
    }

    /**
     * Získá id předka pro daný uzel.
     * @param node grafová reprezentace zkoumaného uzlu
     * @param relationshipService služba pro práci s předky 
     * @return id předka, nebo -1 jestliže předka nemá
     */
    protected static Long getParent(Vertex node, FilteredRelationshipService relationshipService) {
        Vertex parent = relationshipService.getParent(node);
        if (parent != null && parent.getProperty(NodeProperty.NODE_NAME.t()) != null) {
            return (Long) parent.getId();
        } else {
            return -1L;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Component other = (Component) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
}
