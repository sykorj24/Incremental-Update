package eu.profinit.manta.dataflow.repository.viewer.model.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Filtr resourců.
 * @author tfechtner
 *
 */
public final class ResourceFilter {

    /** Výchozí filtr povolující všechny resource. */
    public static final ResourceFilter DEFAULT_FILTER = new ResourceFilter(-1, "Default filter", 0,
            Collections.<String> emptySet());

    /** Id filtru. */
    private final int id;
    /** Jméno filtru. */
    private final String name;
    /** Pořadí filtrů, 0 je nejvyšší. */
    private final int order;
    /** Množina povolených resource v daném filtru. Pokud je prádná, jsou povoleny všechny. */
    private final Set<String> allowedResources;

    /**
     * @param id id filtru
     * @param name Jméno filtru.
     * @param order Pořadí filtrů, 0 je nejvyšší.
     * @param allowedResources Množina povolených resource v daném filtru. Pokud je prádná, jsou povoleny všechny.
     */
    public ResourceFilter(int id, String name, int order, Set<String> allowedResources) {
        super();
        this.id = id;
        this.name = name;
        this.order = order;
        if (allowedResources != null) {
            this.allowedResources = allowedResources;
        } else {
            this.allowedResources = new HashSet<>();
        }
    }

    /**
     * @return Id filtru.
     */
    public int getId() {
        return id;
    }

    /**
     * @return Jméno filtru.
     */
    public String getName() {
        return name;
    }
    
    /**
     * @return Pořadí filtrů, 0 je nejvyšší.
     */
    public int getOrder() {
        return order;
    }

    /**
     * @return Množina povolených resource v daném filtru. Pokud je prádná, jsou povoleny všechny.
     */
    public Set<String> getAllowedResources() {
        return allowedResources;
    }

    /**
     * @param newAllowedResourceName jméno nově povoleného resource
     */
    public void addAllowedResource(String newAllowedResourceName) {
        allowedResources.add(newAllowedResourceName);
    }

    /**
     * Sestaví informační mapu pro model/view.
     * @return informační mapa o objektu, klíče: id, name
     */
    public Map<String, Object> getModel() {
        Map<String, Object> resultModel = new HashMap<String, Object>();
        resultModel.put("id", getId());
        resultModel.put("order", getOrder());
        resultModel.put("name", getName());
        return resultModel;
    }

    /**
     * Vrátí množinu DB ID všech deaktivovaných reosurce.
     * @param resourceInDbMap mapa všech jmen resource v DB na jejich DB ID
     * @return množina DB ID deaktivovaných resource
     */
    public Set<Long> getDisabledResource(Map<String, Object> resourceInDbMap) {
        if (allowedResources.isEmpty()) {
            return Collections.emptySet();
        } else {
            Set<Long> disabledResources = new HashSet<>();

            Set<Entry<String, Object>> resourceInDbEntrySet = resourceInDbMap.entrySet();
            for (Entry<String, Object> entry : resourceInDbEntrySet) {
                if (isResourceDisabled(entry.getKey())) {
                    disabledResources.add((Long) entry.getValue());
                }
            }
            return disabledResources;
        }
    }

    /**
     * Získá model pro view o všech resource v db nastavených podle daného filtru.
     * @param resourceInDbMap mapa všech jmen resource v DB na jejich DB ID
     * @return seznam map pro všechny resource v db, klíče: id, name, isDisabled (jestli deaktivován)
     */
    public List<Map<String, Object>> getAllResourcesModel(Map<String, Object> resourceInDbMap) {
        List<Map<String, Object>> resultModel = new ArrayList<>();
        
        Set<Entry<String, Object>> resourceInDbEntrySet = resourceInDbMap.entrySet();
        for (Entry<String, Object> entry : resourceInDbEntrySet) {
            Map<String, Object> resourceModel = new HashMap<>();
            resourceModel.put("id", entry.getValue());
            resourceModel.put("name", entry.getKey());
            resourceModel.put("isDisabled", isResourceDisabled(entry.getKey()));
            resultModel.add(resourceModel);            
        }        
        
        return resultModel;
    }

    /**
     * Zjistí jestli je v tomto filtru resource s daným jménem deaktivován.
     * @param resourceName jméno zkoumaného filtru
     * @return true, jestliže je daný resource filtrován
     */
    private boolean isResourceDisabled(String resourceName) {
        if (allowedResources.isEmpty()) {
            return false;
        } else {
            return !allowedResources.contains(resourceName); 
        }
    }
}
