package eu.profinit.manta.dataflow.repository.viewer.service.index;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

import eu.profinit.manta.dataflow.repository.viewer.model.index.Node;

/**
 * Comparator řadící podle cesty uzlů lexikograficky. 
 * Podobného efektu by se dosáhlo slitím obou cest a jejich porovnáním.
 * @author tfechtner
 *
 */
public class PathOrderNodeComparator implements Comparator<Node>, Serializable {

    private static final long serialVersionUID = 4410923537377915882L;

    @Override
    public int compare(Node leftNode, Node rightNode) {
        List<String> leftPath = leftNode.getPath();
        List<String> rightPath = rightNode.getPath();

        for (int i = 0; i < leftPath.size(); i++) {
            if (rightPath.size() <= i) {
                return 1;
            }

            int elementsCompare = leftPath.get(i).compareTo(rightPath.get(i));
            if (elementsCompare != 0) {
                return elementsCompare;
            }
        }

        if (leftPath.size() < rightPath.size()) {
            return -1;
        } else {
            return leftNode.getText().compareTo(rightNode.getText());
        }
    }

}
