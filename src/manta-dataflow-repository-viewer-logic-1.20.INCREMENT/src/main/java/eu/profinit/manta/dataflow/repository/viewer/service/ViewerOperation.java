package eu.profinit.manta.dataflow.repository.viewer.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.gremlin.java.GremlinPipeline;
import com.tinkerpop.pipes.branch.LoopPipe.LoopBundle;

import eu.profinit.manta.dataflow.model.Layer;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowEdge;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.MappingNode;

/**
 * Třída poskytující operace nad grafovou databází specifické pro vizualizaci. 
 * @author tfechtner
 *
 */
public final class ViewerOperation {
    private static final String UNKNOWN_LAYER = Layer.DEFAULT.getName();
    /** Název resource, pokud není žádný dostupný. */
    private static final String UNKNOWN_RESOURCE = "Default";
    /** Název typu vrcholu, pokud vrchol typ nemá.*/
    private static final String UKNOWN_TYPE = "Unknown Type";

    private ViewerOperation() {
    }

    /**
     * Vrátí úroveň uzlu - top/middle/bottom.
     * @param node zkoumaný uzel
     * @param levelMapProvider provider pro informace o úrovních
     * @return úroveň uzlu
     */
    public static FlowLevel getLevel(Vertex node, LevelMapProvider levelMapProvider) {
        return levelMapProvider.getTypeInfo(getTechnologyType(node), getType(node)).getLevel();
    }

    /**
     * Vrátí label uzlu. Což záleží na typu uzlu: <br>
     * - node - jméno uzlu <br>
     * - resource - jméno resource <br>
     * - atribut uzlu - klíč atributu 
     * @param node zkoumaný uzel
     * @return jméno uzlu, null pokud je node null
     */
    public static String getLabel(Vertex node) {
        return GraphOperation.getName(node);
    }

    /**
     * Vrátí typ daného uzlu. 
     * @param node zkoumaný uzel
     * @return typ uzlu nebo {@link #UKNOWN_TYPE}, pokud uzel typ nemá
     */
    public static String getType(Vertex node) {
        if (node == null) {
            throw new IllegalArgumentException("The argument is null.");
        }

        Object type = node.getProperty(NodeProperty.NODE_TYPE.t());
        if (type != null) {
            return type.toString();
        } else {
            type = node.getProperty(NodeProperty.RESOURCE_TYPE.t());
            if (type != null) {
                return type.toString();
            } else {
                return UKNOWN_TYPE;
            }
        }
    }

    /**
     * Vrati vrstvu, do ktere patri dany uzel.
     * @param node Zkoumany uzel
     * @return Vrstva, do ktere patri dany uzel.
     */
    public static String getLayer(Vertex node) {
        Vertex layer = GraphOperation.getLayer(node);
        if (layer != null) {
            return layer.getProperty(NodeProperty.LAYER_NAME.t());
        } else {
            return UNKNOWN_LAYER;
        }
    }

    /**
     * Vrátí technologii uzlu, tedy název resource, do které uzel patří.
     * @param node zkoumaný uzel
     * @return název resource nebo {@link #UNKNOWN_RESOURCE}, pokud uzel resource nemá
     */
    public static String getTechnology(Vertex node) {
        Vertex resource = GraphOperation.getResource(node);
        if (resource != null) {
            return resource.getProperty(NodeProperty.RESOURCE_NAME.t());
        } else {
            return UNKNOWN_RESOURCE;
        }
    }

    /**
     * Vrátí typ technologie uzlu, tedy typ resource, do které uzel patří.
     * @param node zkoumaný uzel
     * @return typ resource nebo {@link #UNKNOWN_RESOURCE}, pokud uzel resource nemá
     */
    public static String getTechnologyType(Vertex node) {
        Vertex resource = GraphOperation.getResource(node);
        if (resource != null) {
            return resource.getProperty(NodeProperty.RESOURCE_TYPE.t());
        } else {
            return UNKNOWN_RESOURCE;
        }
    }

    /**
     * Najde vertex v grafu podle cesty.
     * @param superRoot super root grafu
     * @param path cesta hledaného grafu v podobě id jednotlivých vrcholů
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return hledaný vertex nebo null, pokud nebyl nalezen
     */
    public static Vertex findVertexByPath(Vertex superRoot, List<Long> path, RevisionInterval revisionInterval) {
        return findVertexByPath(superRoot, path, revisionInterval, 0);
    }

    /**
     * Najde vertex v grafu podle cesty, rekurzivně.
     * @param actualVertex aktuálně prohledávaný uzel
     * @param path cesta hledaného grafu v podobě id jednotlivých vrcholů
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @param index index v cestě, kde se právě hleda
     * @return hledaný vertex nebo null, pokud nebyl nalezen
     */
    private static Vertex findVertexByPath(Vertex actualVertex, List<Long> path, RevisionInterval revisionInterval,
            int index) {
        if (index >= path.size()) {
            return actualVertex;
        } else {
            List<Vertex> children = GraphOperation.getDirectChildren(actualVertex, revisionInterval);
            for (Vertex child : children) {
                if (child.getId().equals(path.get(index))) {
                    return findVertexByPath(child, path, revisionInterval, index + 1);
                }
            }
            return null;
        }
    }

    /**
     * Vrátí cestu k danému vertexu jako seznam jmen předků, v pořadí od resource.
     * @param vertex vertex, pro který se hledá path
     * @return seznam předků, odpovídající cestě v grafu
     */
    public static List<String> getPath(Vertex vertex) {
        List<String> path = new ArrayList<>();

        if (vertex.getProperty(NodeProperty.RESOURCE_NAME.t()) == null) {
            path.add(ViewerOperation.getLabel(GraphOperation.getTopParentResource(vertex)));
            List<Vertex> parents = GraphOperation.getAllParent(vertex);
            for (int i = parents.size() - 1; i >= 0; i--) {
                path.add(ViewerOperation.getLabel(parents.get(i)));
            }
        }

        return path;
    }

    /**
     * Zjistí jestli je obdržená cesta skutečně cesta. 
     * @param bundle objekt z loop pipy k ověření
     * @return true, jestliže je cesta bez opakování
     */
    public static boolean isPathSimple(LoopBundle<Vertex> bundle) {
        Vertex newVertex = bundle.getObject();
        for (Object element : bundle.getPath()) {
            if (element.equals(newVertex)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Vytvoří začátek pipy pro cyklení.
     * @param startNodes startovní uzly
     * @param direction směr postupu flow
     * @param isFilterEdgesShown true, jestliže se mají brát i filter hrany
     * @return vytvořená pipe
     */
    public static GremlinPipeline<Vertex, Vertex> createPipe(final Collection<Vertex> startNodes, Direction direction,
            boolean isFilterEdgesShown) {
        GremlinPipeline<Vertex, Vertex> pipe = new GremlinPipeline<Vertex, Vertex>(startNodes).as("loopIter");

        String[] labels = GraphOperation.getDataflowLabelAsStrings(isFilterEdgesShown);
        // vybrat iterační krok podle směru pohybu
        if (direction == Direction.OUT) {
            pipe = pipe.outE(labels).inV();
        } else {
            pipe = pipe.inE(labels).outV();
        }
        return pipe;
    }

    /**
     * Zjistí label pro výslednou nadřazenou hranu z podhran. <br>
     * Hrana je {@link EdgeLabel#FILTER}, pokud je mezi podhrany alespoň jedna {@link EdgeLabel#FILTER}.<br> 
     * V opačném případě je {@link EdgeLabel#DIRECT}.
     * @param subEdges seznam podhran, z kterých se počítá výsledný label
     * @return label nadřazené hrany, nikdy null
     */
    public static EdgeLabel computeLabel(Collection<FlowEdge> subEdges) {
        for (FlowEdge e : subEdges) {
            if (e.getLabel() == EdgeLabel.FILTER) {
                return EdgeLabel.FILTER;
            }
        }
        return EdgeLabel.DIRECT;
    }
    
    /**
     * Vrati mapovani daneho uzlu v jednotlivych vrstvach.
     * Vysledkem je mapa, kde klicem je nazev vrstvy
     * a hodnota je seznam uzlu mapovanych stavenou komponentou
     * ci mapujicich stavenou komponentu v prislusne vrstve.
     * @param node Zkoumany uzel
     * @param revisionInterval Interval revizi, pro ktery se hleda mapovani
     * @return Mapovani daneho uzlu v jednotlivych vrstvach.
     */
    public static Map<String, List<MappingNode>> getNodeMapping(Vertex node, RevisionInterval revisionInterval) {
        // Vsechny uzly pridruzene skrze mapovaci vazby
        List<Vertex> adjacents = GraphOperation.getAdjacentVertices(node, Direction.BOTH, revisionInterval, EdgeLabel.MAPS_TO);
        Map<String, List<MappingNode>> result = new HashMap<>();
        for (Vertex adjacent : adjacents) {
            // Zjistime vrstvu pridruzeneho uzlu
            String layer = ViewerOperation.getLayer(adjacent);
            List<MappingNode> layerMapping = result.get(layer);
            if (layerMapping == null) {
                // Jeste pro ni nemame mapovani => vytvorime je 
                layerMapping = new ArrayList<>();
                result.put(layer, layerMapping);
            }
            // Zjistime cetu k pridruzenemu uzlu
            List<String> path = ViewerOperation.getPath(adjacent);
            // V ceste chceme i sammotny nazev uzlu
            path.add(ViewerOperation.getLabel(adjacent));
            // Pridame do mapovani odkaz na pridruzeny uzel
            MappingNode mappedNode = new MappingNode();
            mappedNode.setId((Long)adjacent.getId());
            mappedNode.setPath(path);
            layerMapping.add(mappedNode);
        }
        return result;
    }

    
}
