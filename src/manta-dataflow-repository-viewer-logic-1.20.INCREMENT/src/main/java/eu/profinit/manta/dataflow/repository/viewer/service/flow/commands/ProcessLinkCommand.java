package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ProcessLinkRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ProcessLinkResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.index.WelcomeFormModel;

/**
 * Převede model z permalinku na model z welcome stránky obsahující místo cest k startovním uzlům jejich id.
 * @author tfechtner
 *
 */
public class ProcessLinkCommand extends AbstractCommand<ProcessLinkRequest, ProcessLinkResponse> {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessLinkCommand.class);

    @Autowired
    private RevisionRootHandler revisionRootHandler;

    @Override
    public ProcessLinkResponse execute(ProcessLinkRequest request, FlowStateViewer flowState,
            TitanTransaction transaction) {
        WelcomeFormModel formModel = new WelcomeFormModel();
        formModel.setDepth(request.getDepth());
        formModel.setDirection(request.getDirection());
        formModel.setFilter(request.getFilter());
        formModel.setFilterEdges(request.isFilterEdges());
        formModel.setLevel(request.getLevel());
        formModel.setZoom(request.getZoom());

        Double revision = revisionRootHandler.getHeadNumber(transaction);
        if (revision != null) {
            formModel.setRevision(revision);
        } else {
            formModel.setRevision(0);
        }

        Vertex root = transaction.getVertex(request.getRootId());
        formModel.setSelectedItems(transformSelectedItems(request.getSi(), root,
                new RevisionInterval(formModel.getRevision())));

        ProcessLinkResponse response = new ProcessLinkResponse();
        response.setFormModel(formModel);
        return response;
    }

    /**
     * Ztransformuje vybrané uzly z fomratů předků a typu na jejich id.
     * @param selectedItemsParts seznam vybraných uzlů v podobě předků a jejich typů
     * @param root root databáze
     * @param revisionInterval interval revizí daného flowState
     * @return seznam id-eček, nikdy null
     */
    private String[] transformSelectedItems(List<List<List<String>>> selectedItemsParts, Vertex root,
            RevisionInterval revisionInterval) {
        List<String> transformedItems = new ArrayList<>();
        if (selectedItemsParts != null) {
            for (List<List<String>> selectedItem : selectedItemsParts) {
                if (!isFormatOk(selectedItem)) {
                    LOGGER.warn("Format of selected items is not correct.");
                    continue;
                }

                String transformedItem = transformSelectedItem(selectedItem, root, revisionInterval);
                if (transformedItem != null) {
                    transformedItems.add(transformedItem);
                }
            }
        }

        return transformedItems.toArray(new String[transformedItems.size()]);
    }

    /**
     * Zkontroluje, jestli jsou selected item v pořádku. <br> 
     * Musí mít alespoň délku dva a každý podpole musí mít délku dva.
     * @param selectedItem kontrolovaný vstup 
     * @return true, jestliže jsou vstupy korektní
     */
    private boolean isFormatOk(List<List<String>> selectedItem) {
        if (selectedItem.size() < 2) {
            return false;
        }

        for (List<String> item : selectedItem) {
            if (item.size() != 2) {
                return false;
            }
        }

        return true;
    }

    /**
     * Převede reprezentaci identifikace uzlu z jeho předků s typy na db id.
     * @param selectedItemParts posloupnost předků, začínajících resourcem, pro každý jméno a typ 
     * @param root kořen databáze
     * @param revisionInterval interval revizí daného flowState
     * @return id nalezeného uzlu nebo null
     */
    private String transformSelectedItem(List<List<String>> selectedItemParts, Vertex root,
            RevisionInterval revisionInterval) {
        Vertex resource = findResource(root, selectedItemParts.get(0).get(0), selectedItemParts.get(0).get(1),
                revisionInterval);
        if (resource == null) {
            LOGGER.warn("Resource " + selectedItemParts.get(0) + " was not found.");
            return null;
        }

        Vertex actualNode = resource;
        for (int i = 1; i < selectedItemParts.size(); i++) {
            actualNode = findChildNode(actualNode, selectedItemParts.get(i).get(0), selectedItemParts.get(i).get(1),
                    revisionInterval);
            if (actualNode == null) {
                LOGGER.warn("Node " + selectedItemParts.get(i) + " was not found.");
                break;
            }
        }
        if (actualNode != null && actualNode != resource) {
            return actualNode.getId().toString();
        } else {
            LOGGER.warn("Node " + selectedItemParts + " was not found.");
            return null;
        }
    }

    /**
     * Nalezne potomka s dnaým jménem a typem.
     * @param root předek, jehož potomek se hledá
     * @param name jméno potomka
     * @param type typ potomka
     * @param revisionInterval interval revizí daného flowState
     * @return nalezený potomek nebo null
     */
    private Vertex findChildNode(Vertex root, String name, String type, RevisionInterval revisionInterval) {
        boolean isRootResource = root.getProperty(NodeProperty.RESOURCE_NAME.t()) != null;

        List<Vertex> children = GraphOperation.getDirectChildren(root, revisionInterval);
        for (Vertex child : children) {
            boolean isSameNode = child.getProperty(NodeProperty.NODE_NAME.t()).equals(name)
                    && child.getProperty(NodeProperty.NODE_TYPE.t()).equals(type);
            if (isSameNode && (!isRootResource || GraphOperation.getParent(child) == null)) {
                return child;
            }
        }
        return null;
    }

    /**
     * Nalezne resource s daným jménem a typem.
     * @param root root databáze
     * @param name jméno resource
     * @param type typ resource
     * @param revisionInterval interval revizí daného flowState
     * @return nalezený resource nebo null
     */
    private Vertex findResource(Vertex root, String name, String type, RevisionInterval revisionInterval) {
        List<Vertex> children = GraphOperation.getDirectChildren(root, revisionInterval);
        for (Vertex child : children) {
            if (child.getProperty(NodeProperty.RESOURCE_NAME.t()).equals(name)
                    && child.getProperty(NodeProperty.RESOURCE_TYPE.t()).equals(type)) {
                return child;
            }
        }
        return null;
    }

    /**
     * @return držák na revision root
     */
    public RevisionRootHandler getRevisionRootHandler() {
        return revisionRootHandler;
    }

    /**
     * @param revisionRootHandler držák na revision root
     */
    public void setRevisionRootHandler(RevisionRootHandler revisionRootHandler) {
        this.revisionRootHandler = revisionRootHandler;
    }
}
