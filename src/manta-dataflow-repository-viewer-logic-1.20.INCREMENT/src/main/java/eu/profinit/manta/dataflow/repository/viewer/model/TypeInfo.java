package eu.profinit.manta.dataflow.repository.viewer.model;

import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;


/**
 * Konfigurace vlastnosti uzlu na zaklade jeho urovne.
 * 
 * @see eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider
 */
public class TypeInfo {

    /**
     * Textova reprezentace urovne (napr. "top", "middle", "bottom")
     */
    private FlowLevel level;
    /**
     * Jestliže je uzel již atomický a nemůže mít nkidy potomky.
     */
    private boolean atomic;

    /**     
     * @return Textova reprezentace urovne (napr. "top", "middle", "bottom") 
     */
    public FlowLevel getLevel() {
        return level;
    }

    /**
     * @param level Textova reprezentace urovne (napr. "top", "middle", "bottom")
     */
    public void setLevel(FlowLevel level) {
        this.level = level;
    }

    /**
     * @return Jestliže je uzel již atomický a nemůže mít nkidy potomky.
     */
    public boolean isAtomic() {
        return atomic;
    }

    /**
     * @param atomic Jestliže je uzel již atomický a nemůže mít nkidy potomky.
     */
    public void setAtomic(boolean atomic) {
        this.atomic = atomic;
    }
}
