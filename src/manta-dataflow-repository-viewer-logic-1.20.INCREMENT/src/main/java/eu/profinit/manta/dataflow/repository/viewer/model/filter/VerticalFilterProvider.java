package eu.profinit.manta.dataflow.repository.viewer.model.filter;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.viewer.service.filter.XmlVerticalFilterProvider.VertexPosition;

/**
 * Rozhraní definující poskytovalete vertikálních filtrů.
 * @author tfechtner
 *
 */
public interface VerticalFilterProvider {

    /**
     * Ověří, jestli je daný element v daném resource deaktivován.
     * @param resourceType typ ověřovaného resource
     * @param elementType ověřovaný element
     * @param position pozice elementu v sekvenci elementů daného typu
     * @return true, jestliže je daná dvojice deaktivovaná
     */
    boolean isElementDisabled(String resourceType, String elementType, VertexPosition position);

    /**
     * Ověří, jestli je daný vrchol deaktivován.
     * @param actualVertex vertex k ověření
     * @param startVertex startovní vrchol, ke kterému se počítají pozice
     * @return true, jestliže je daný vrchol deaktivovan
     */
    boolean isElementDisabled(Vertex actualVertex, Vertex startVertex);
}
