package eu.profinit.manta.dataflow.repository.viewer.exception;

/**
 * Exception pro odlišení výjimek vyhozených během zpracování dotazů z formuláře.
 * @author tfechtner
 *
 */
public class FlowFormException extends RuntimeException {
    private static final long serialVersionUID = 2822676328585999775L;
    
    /**
     * @param message zpráva výjimky
     */
    public FlowFormException(String message) {
        super(message);
    }

    /**
     * @param e způsobující výjimka
     */
    public FlowFormException(Exception e) {
        super(e);
    }

}
