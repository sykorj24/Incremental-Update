package eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data;

/**
 * Model zdroje.
 * 
 * @author onouza
 */
public class Resource {

    /** ID uzlu zdroje v databazi */
    private Object id;
    /** Nazev zdroje */
    private String name;
    /** Nazev vrstvy, do ktere zdroj patri */
    private String layer;
    
    // gettery / settery

    /**
     * @return ID uzlu zdroje v databazi
     */
    public Object getId() {
        return id;
    }

    /**
     * @param id ID uzlu zdroje v databazi
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     * @return Nazev zdroje
     */
    public String getName() {
        return name;
    }

    /**
     * @param name Nazev zdroje
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Nazev vrstvy, do ktere zdroj patri
     */
    public String getLayer() {
        return layer;
    }

    /**
     * @param layer Nazev vrstvy, do ktere zdroj patri
     */
    public void setLayer(String layer) {
        this.layer = layer;
    }

}
