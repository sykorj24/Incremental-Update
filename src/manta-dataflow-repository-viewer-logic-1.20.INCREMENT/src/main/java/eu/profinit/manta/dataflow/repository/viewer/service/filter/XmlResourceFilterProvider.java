package eu.profinit.manta.dataflow.repository.viewer.service.filter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.xml.parsers.SAXParser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import eu.profinit.manta.dataflow.repository.viewer.model.filter.ResourceFilter;
import eu.profinit.manta.dataflow.repository.viewer.model.filter.ResourceFilterProvider;

/**
 * Třída pro poskytování filtrů resourců načítaných z xml konfigurace.
 * @author tfechtner
 *
 */
public class XmlResourceFilterProvider extends AbstractXmlProvider implements ResourceFilterProvider {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlResourceFilterProvider.class);
    /** Mapa načtených filtrů. */
    private Map<Integer, ResourceFilter> filtersMap;

    /**
     * Provede inicizalizaci třídy.
     */
    @PostConstruct
    public void init() {
        filtersMap = extractDataFromXml();
        if (filtersMap == null) {
            filtersMap = Collections.emptyMap();
        }
    }

    @Override
    public List<Map<String, Object>> getFiltersModel() {
        List<Map<String, Object>> resultModel = new ArrayList<>();
        for (ResourceFilter filter : filtersMap.values()) {
            resultModel.add(filter.getModel());
        }

        return resultModel;
    }

    @Override
    public ResourceFilter getFilter(Integer filterId) {
        ResourceFilter filter = filtersMap.get(filterId);
        if (filter != null) {
            return filter;
        } else {
            return ResourceFilter.DEFAULT_FILTER;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Map<Integer, ResourceFilter> handleParsing(SAXParser saxParser, InputStream stream) throws SAXException,
            IOException {
        FilterMapHandler handler = new FilterMapHandler();
        saxParser.parse(stream, handler);
        return handler.getFilterMap();
    }

    /**
     * Třída pro naparsování souboru s filtry.
     *
     */
    private static class FilterMapHandler extends DefaultHandler {
        /** Mapa s filtry, kde klíčem je jejich název. */
        private final Map<Integer, ResourceFilter> filtersMap = new HashMap<>();

        private Integer lastId = null;

        @Override
        public void startElement(String uri, String localName, String qualifiedName, Attributes attributes)
                throws SAXException {
            if ("filter".equals(qualifiedName)) {
                Integer filterId = parseIntegerAttr("id", attributes);
                Integer filterOrder = parseIntegerAttr("order", attributes);                

                String filterName = attributes.getValue("name");
                if (filterName == null) {
                    throw new IllegalArgumentException("The horizontal filter does not have name.");
                }

                lastId = filterId;
                filtersMap.put(filterId, new ResourceFilter(filterId, filterName, filterOrder, null));
            } else if ("technology".equals(qualifiedName)) {
                ResourceFilter filter = filtersMap.get(lastId);
                String resourceName = attributes.getValue("name");
                if (filter != null) {
                    filter.addAllowedResource(resourceName);
                } else {
                    LOGGER.warn("Does not exist filter for " + resourceName + ".");
                }
            }
        }

        private Integer parseIntegerAttr(String key, Attributes attributes) {
            String string = null;
            try {
                string = attributes.getValue(key);
                if (string == null) {
                    throw new IllegalArgumentException("The horizontal filter does not have " + key + " attribute.");
                }
                return Integer.valueOf(string);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("The " + key + " for the horizontal filter is not integer.");
            }
        }

        /**
         * @return vrátí načtenou mapu filtru
         */
        public Map<Integer, ResourceFilter> getFilterMap() {
            return filtersMap;
        }
    }
}
