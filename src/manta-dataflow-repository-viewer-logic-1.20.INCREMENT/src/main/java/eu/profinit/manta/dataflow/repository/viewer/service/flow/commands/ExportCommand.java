package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import au.com.bytecode.opencsv.CSVWriter;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowEdge;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.FlowEdgeViz;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.RevisionState;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ExportRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.BaseResponse;
import eu.profinit.manta.dataflow.repository.viewer.service.FilteredRelationshipService;
import eu.profinit.manta.dataflow.repository.viewer.service.ViewerOperation;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;
import eu.profinit.manta.platform.web.core.security.SecurityHelper;

/**
 * Command provádějící export referenčního pohledu.
 * @author tfechtner
 * @author mdvorak2
 *
 */
public class ExportCommand extends AbstractCommand<ExportRequest, BaseResponse> {

    /** Název akce pro usage stats pro export csv. */
    private static final String FLOW_VIS_EXPORT_CSV = "flow_vis_exportCsv";
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(ExportCommand.class);

    /** Název souboru, kam se nasypou vrcholy.*/
    private static final String VERTICES_CSV_NAME = "vertices.csv";
    /** Hlavička pro csv s vrcholy. */
    private static final String[] VERTICES_HEADER = new String[] { "FullNodeName", "ColumnName", "ColumnType",
            "ObjectName", "ObjectType", "GroupName", "GroupType", "ResourceName", "ResourceType", "RevisionState",
            "AttributeName", "AttributeValue" };

    /** Název souboru, kam se nasypou hrany.*/
    private static final String RELATIONS_CSV_NAME = "relations.csv";
    /** Hlavička pro csv s hrany. */
    private static final String[] RELATIONS_HEADER = new String[] { "Type", "SourcePath", "TargetPath",
            "SourceColumnName", "SourceColumnType", "TargetColumnName", "TargetColumnType", "SourceObjectName",
            "SourceObjectType", "TargetObjectName", "TargetObjectType", "SourceGroupName", "SourceGroupType",
            "TargetGroupName", "TargetGroupType", "SourceResourceName", "SourceResourceType", "TargetResourceName",
            "TargetResourceType", "RevisionState" };

    /** Kódování exportu. */
    private static final String ENCODING = "utf-8";

    @Autowired
    private TechnicalAttributesHolder technicalAttributesHolder;
    /** Služba pro práci s vertikálními filtry. */
    @Autowired
    private FilteredRelationshipService relationshipService;
    /** Služba pro ukládání statsitik použití. */
    @Autowired
    private UsageStatsCollector usageStatsCollector;
    /** Oddelovac v CSV souborech exportu */
    @Value("${viewer.export.csvSeparator:COMMA}")
    private CsvSeparator csvSeparator;

    @Override
    public BaseResponse execute(ExportRequest request, FlowStateViewer flowState, TitanTransaction transaction) {

        String nameDelim = request.getNameDelimiter();
        ZipArchiveOutputStream zipStream = new ZipArchiveOutputStream(request.getOutputStream());

        try {
            Set<FlowEdge> unfilterVerticesEdges = new HashSet<>();
            unfilterVerticesEdges.addAll(findEdgesUsingFilters(flowState, transaction, Direction.IN));
            unfilterVerticesEdges.addAll(findEdgesUsingFilters(flowState, transaction, Direction.OUT));

            Set<FlowEdge> flowLevelEdges = handleFlowLevel(flowState, transaction, unfilterVerticesEdges);
            Set<FlowEdgeViz> mappedEdges = flowState.getEquivalencesHolder().remapEdges(transaction, flowLevelEdges,
                    flowState.getRevisionInterval());

            Map<Long, Integer> depthMap = new HashMap<Long, Integer>();
            exportRelations(flowState, transaction, nameDelim, zipStream, mappedEdges, depthMap);
            exportElements(flowState, transaction, nameDelim, zipStream, mappedEdges, depthMap);
        } finally {
            IOUtils.closeQuietly(zipStream);
        }

        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_VIS_EXPORT_CSV,
                Collections.singletonMap("flowStateGuid", flowState.getGuid()));

        return BaseResponse.OK;
    }

    /**
     * Upraví flow hrany tak, aby jejich konce splňovaly podmínku na úroveň. 
     * @param flowState flowState, který mimo jin drží i požadovanou flow úroveň
     * @param transaction transakce pro přístup k databázi
     * @param flowEdges množina hran k upravění
     * @return množina upravených hran, nikdy null
     */
    private Set<FlowEdge> handleFlowLevel(FlowStateViewer flowState, TitanTransaction transaction,
            Set<FlowEdge> flowEdges) {
        Set<FlowEdge> resultEdges = new HashSet<>();

        for (FlowEdge edge : flowEdges) {
            Vertex sourceVertex = transaction.getVertex(edge.getSource());
            Vertex targetVertex = transaction.getVertex(edge.getTarget());
            Vertex sourceFirstLevelEnough = relationshipService.getFirstVertexHighEnough(sourceVertex,
                    flowState.getFlowLevel(), getLevelMapProvider());
            Vertex targetFirstLevelEnough = relationshipService.getFirstVertexHighEnough(targetVertex,
                    flowState.getFlowLevel(), getLevelMapProvider());
            resultEdges.add(new FlowEdge(sourceFirstLevelEnough, targetFirstLevelEnough, Direction.OUT, edge.getLabel(),
                    edge.getRevInterval()));
        }

        return resultEdges;
    }

    /**
     * Nalezne flow hrany pro startovní uzly v rámci daného referenční view s daným směrem.
     * @param flowState zkoumané referenční view
     * @param transaction transakce pro přístup k db
     * @param direction směr, pro který se hrany hledají
     * @return množina nalezených hran, nikdy null
     */
    private Set<FlowEdge> findEdgesUsingFilters(FlowStateViewer flowState, TitanTransaction transaction,
            Direction direction) {
        Set<FlowEdge> edges = new HashSet<>();

        Set<Vertex> totalStartVertices = new HashSet<>();
        for (Long id : flowState.getTotalStartNodes()) {
            List<Vertex> lists = GraphOperation.getAllLists(transaction.getVertex(id), flowState.getRevisionInterval());
            totalStartVertices.addAll(lists);
        }

        boolean isComparing = flowState.getRevisionInterval().isComparing();
        RevisionInterval startRevInterval = new RevisionInterval(flowState.getRevisionInterval().getStart());
        RevisionInterval endRevInterval = new RevisionInterval(flowState.getRevisionInterval().getEnd());

        Deque<Vertex> vertexDeque = new LinkedList<Vertex>(totalStartVertices);
        Set<Object> visitedVertices = new HashSet<>();
        while (!vertexDeque.isEmpty()) {
            Vertex currentVertex = vertexDeque.pollFirst();

            if (visitedVertices.add(currentVertex.getId())) {
                if (isComparing) {
                    edges.addAll(processOneVertex(currentVertex, vertexDeque, flowState, direction, totalStartVertices,
                            startRevInterval));
                    edges.addAll(processOneVertex(currentVertex, vertexDeque, flowState, direction, totalStartVertices,
                            endRevInterval));
                } else {
                    edges.addAll(processOneVertex(currentVertex, vertexDeque, flowState, direction, totalStartVertices,
                            flowState.getRevisionInterval()));
                }
            }
        }

        return edges;
    }

    /**
     * Nalezne flow hrany pro daný uzel s daným směrem.
     * @param currentVertex zpracovávaný vrchol
     * @param vertexDeque vrcholy pro další zpracování
     * @param flowState zkoumané referenční view 
     * @param direction směr, pro který se hrany hledají
     * @param totalStartVertices množina startovních vrcholů celého ref view
     * @param revInterval interval revizí, které se porovnávají
     */
    private Set<FlowEdge> processOneVertex(Vertex currentVertex, Deque<Vertex> vertexDeque,
            FlowStateViewer flowState, Direction direction, Set<Vertex> totalStartVertices,
            RevisionInterval revInterval) {

        Set<FlowEdge> edges = new HashSet<>();

        Map<Vertex, EnumSet<EdgeLabel>> adjacentVertices = GraphOperation.getAdjacentUnfilteredVertices(currentVertex,
                totalStartVertices, flowState, direction, revInterval);

        for (Entry<Vertex, EnumSet<EdgeLabel>> adjacentVertexEntry : adjacentVertices.entrySet()) {
            Vertex adjacentVertex = adjacentVertexEntry.getKey();
            for (EdgeLabel label : adjacentVertexEntry.getValue()) {
                edges.add(new FlowEdge(currentVertex, adjacentVertex, direction, label, revInterval));
            }
            vertexDeque.push(adjacentVertex);
        }

        return edges;
    }

    /**
     * Vyexportuje vrcholy.
     * @param flowState flowstate, který se exportuje
     * @param transaction transakce pro přístup do db
     * @param nameDelim oddělovač v kvalifikovaných jménech
     * @param zipStream výstupní zip stream.
     * @param flowEdges množina hran, pro které se exportují vrcholy
     * @param depthMap mapa hloubek, indexovaná id vrcholů
     */
    private void exportElements(FlowStateViewer flowState, TitanTransaction transaction, String nameDelim,
            ZipArchiveOutputStream zipStream, Set<FlowEdgeViz> flowEdges, Map<Long, Integer> depthMap) {
        ZipArchiveEntry zipEntry2 = new ZipArchiveEntry(VERTICES_CSV_NAME);
        BufferedWriter writer = null;

        try {
            zipStream.putArchiveEntry(zipEntry2);
            writer = new BufferedWriter(new OutputStreamWriter(zipStream, ENCODING));
            CSVWriter csvWriter = new CSVWriter(writer, csvSeparator.getCharacter());
            csvWriter.writeNext(VERTICES_HEADER);

            Set<Long> printedNodeIds = new HashSet<>();
            for (FlowEdgeViz edge : flowEdges) {

                Vertex sourceVertex = transaction.getVertex(edge.getSource());
                printNodeWithHierarchy(sourceVertex, printedNodeIds, csvWriter, nameDelim, flowState, depthMap);

                Vertex endVertex = transaction.getVertex(edge.getTarget());
                printNodeWithHierarchy(endVertex, printedNodeIds, csvWriter, nameDelim, flowState, depthMap);
            }
            csvWriter.flush();
            zipStream.closeArchiveEntry();
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Error during exporting vertices.", e);
        } catch (IOException e) {
            LOGGER.error("Error during exporting vertices.", e);
        }
    }

    /**
     * Vytiskne vrchol a všechny jeho předky.
     * @param vertex vrchol k vytisknutí
     * @param printedNodeIds množina id již vytisknutých vrcholů
     * @param csvWriter writer použitý pro tisk
     * @param nameDelim oddělovač v kvalifikovaných jménech
     * @param flowState flowstate, který se exportuje
     * @param depthMap mapa hloubek, indexovaná id vrcholů
     */
    private void printNodeWithHierarchy(Vertex vertex, Set<Long> printedNodeIds, CSVWriter csvWriter, String nameDelim,
            FlowStateViewer flowState, Map<Long, Integer> depthMap) {
        Vertex currentVertex = vertex;

        while (currentVertex != null && VertexType.getType(currentVertex) == VertexType.NODE) {
            Long currentVertexId = (Long) currentVertex.getId();
            if (!printedNodeIds.contains(currentVertexId)) {
                Edge controlEdge = GraphOperation.getNodeControlEdge(currentVertex);
                RevisionState revState = RevisionState.resolve(controlEdge, flowState.getRevisionInterval());

                // existuje i evkvivalentni vrchol -> je zde tedy platnost v obou revizich
                if (flowState.getEquivalencesHolder().getEquivalentVertexId(currentVertexId) != null) {
                    revState = RevisionState.STABLE;
                }
                printVertex(currentVertex, csvWriter, nameDelim, flowState.getRevisionInterval(), depthMap, revState);
                printedNodeIds.add(currentVertexId);

                currentVertex = controlEdge.getVertex(Direction.IN);
            } else {
                break;
            }
        }
    }

    /**
     * Vyexportuje relace.
     * @param flowState flowstate, který se exportuje
     * @param transaction transakce pro přístup do db
     * @param nameDelim oddělovač v kvalifikovaných jménech
     * @param zipStream výstupní zip stream.
     * @param flowEdges množina hran, pro které se exportují vrcholy
     * @param depthMap mapa hloubek, indexovaná id vrcholů
     */
    private void exportRelations(FlowStateViewer flowState, TitanTransaction transaction, String nameDelim,
            ZipArchiveOutputStream zipStream, Set<FlowEdgeViz> flowEdges, Map<Long, Integer> depthMap) {
        try {
            ZipArchiveEntry zipEntry1 = new ZipArchiveEntry(RELATIONS_CSV_NAME);
            zipStream.putArchiveEntry(zipEntry1);

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(zipStream, ENCODING));
            CSVWriter csvWriter = new CSVWriter(writer, csvSeparator.getCharacter());
            csvWriter.writeNext(RELATIONS_HEADER);

            for (FlowEdgeViz edge : flowEdges) {
                printEdge(csvWriter, transaction, edge, nameDelim, flowState.getRevisionInterval(), depthMap);
            }
            csvWriter.flush();
            zipStream.closeArchiveEntry();
        } catch (IOException e) {
            LOGGER.error("Error during exporting relations.", e);
        }
    }

    /**
     * Zapíše do writeru hranu.
     * @param writer výstupní writer
     * @param edge hrana k zapsání
     * @param nameDelimiter oddělovač ve jménech vrcholů
     * @param revisionInterval interval revizí, pro který se exportuje
     * @param depthMap mapa hloubek, indexovaná id vrcholů
     * @throws IOException chyba při zápisu do writeru
     */
    private void printEdge(CSVWriter writer, TitanTransaction transaction, FlowEdgeViz edge, String nameDelimiter,
            RevisionInterval revisionInterval, Map<Long, Integer> depthMap) throws IOException {

        Vertex source = transaction.getVertex(edge.getSource());
        Vertex target = transaction.getVertex(edge.getTarget());

        String sourceQualifiedName = getVertexQualifiedName(source, nameDelimiter);
        String targetQualifiedName = getVertexQualifiedName(target, nameDelimiter);

        EdgeInfo edgeInfo = new EdgeInfo(edge.getLabel(), sourceQualifiedName, targetQualifiedName, edge.getRevState());

        int sourceDepth = levelsDown(source, revisionInterval, depthMap);
        edgeInfo.getSource().init(source, sourceDepth);

        int targetDepth = levelsDown(source, revisionInterval, depthMap);
        edgeInfo.getTarget().init(target, targetDepth);

        writer.writeNext(edgeInfo.getRowFormat());
    }

    /**
     * Vrátí kvalifikované jméno vrcholu.
     * @param vertex vrchol k vypsání
     * @param nameDelimiter oddělovač mezi jednotlivými části jména
     * @return kvalifikované jméno vrcholu
     */
    protected String getVertexQualifiedName(Vertex vertex, String nameDelimiter) {
        StringBuilder sb = new StringBuilder();
        sb.append(getVertexName(GraphOperation.getTopParentResource(vertex)));
        sb.append(nameDelimiter);
        List<Vertex> parents = GraphOperation.getAllParent(vertex);
        for (int i = parents.size() - 1; i >= 0; i--) {
            sb.append(parents.get(i).getProperty(NodeProperty.NODE_NAME.t()));
            sb.append(nameDelimiter);
        }
        sb.append(vertex.getProperty(NodeProperty.NODE_NAME.t()));

        return sb.toString();
    }

    /**
     * Najde předka, který reprezentuje danou group.
     * Použitp pro výpis SourceGroupName,SourceGroupType,TargetGroupName,TargetGroupType.
     * @param startVertex vrchol, od kterého se začíná hledat
     * @return vrchol, jehož typ odpovídá grouptype příslušné technologie (soubor levels.xml)
     *         pokud ho nenajde, vrací null
     */
    private Vertex findGroupVertex(Vertex startVertex) {
        if (startVertex == null) {
            return null;
        }

        Vertex ressourceVertex = GraphOperation.getTopParentResource(startVertex);
        Set<String> correctTypes = getLevelMapProvider().getGroupType(ViewerOperation.getType(ressourceVertex));
        if (correctTypes == null) {
            return null;
        }
        Vertex vertex = startVertex;
        while (vertex != null) {
            if (correctTypes.contains(ViewerOperation.getType(vertex))) {
                return vertex;
            }
            Vertex v = GraphOperation.getParent(vertex);
            if (v == null) {
                // FIXME opravdu může být resource jako group?
                Vertex r = GraphOperation.getResource(vertex);
                if (r == vertex) {
                    return null; //ošetření proti zacyklení - resource ukazuje sám na sebe
                } else {
                    vertex = r;
                }
            } else {
                vertex = v;
            }
        }
        return null;
    }

    /**
     * Zápis všech údajů o vrcholu do CSV za účelem části 1b.
     * @param vertex vrchol, o němž chceme zapsat údaje
     * @param csvWriter výstupní CSVWriter
     * @param nameDelimiter oddělovač v cestách jako Databáze<oddělovač>Tabulka<oddělovač>Sloupec
     * @param revInterval interval revizí, pro které se export provádí
     * @param depthMap mapa hloubek jednotlivych vrcholu
     * @param revState revision state dan0ho vrcholu
     */
    private void printVertex(Vertex vertex, CSVWriter csvWriter, String nameDelimiter, RevisionInterval revInterval,
            Map<Long, Integer> depthMap, RevisionState revState) {
        int down = levelsDown(vertex, revInterval, depthMap);

        VertexInfo vertexInfo = new VertexInfo(getVertexQualifiedName(vertex, nameDelimiter), revState);
        vertexInfo.init(vertex, down);

        Map<String, List<Object>> attributes = GraphOperation.getAllNodeAttributes(vertex, revInterval);
        if (attributes.isEmpty()) {
            csvWriter.writeNext(vertexInfo.getRowFormat()); //no attribute -> the only one record
        } else {
            boolean isPrintedAtLeastOnce = false;
            for (Entry<String, List<Object>> attr : attributes.entrySet()) {
                if (technicalAttributesHolder.isNodeAttributeTechnical(attr.getKey())) {
                    continue;
                }
                for (int i = 0; i < attr.getValue().size(); i++) { //possible multi-attribute
                    String attrKey = attr.getKey(); //assigning attributeName
                    String attrValue = attr.getValue().get(i).toString(); //assigning attributeValue
                    csvWriter.writeNext(vertexInfo.getRowFormat(attrKey, attrValue));
                    isPrintedAtLeastOnce = true;
                }
            }

            if (!isPrintedAtLeastOnce) {
                csvWriter.writeNext(vertexInfo.getRowFormat());
            }
        }
    }

    /**
     * Spočítá kolik úrovní (hran) dolů v hierarchii ještě existuje.
     * Metoda je rekurzivní (realizuje Depth-First-Search)
     * @param vertex vrchol, od kterého se má hledat (dolů)
     * @param revisionInterval interval revizí, pro které se export provádí
     * @param depthMap mapa hloubek jednotlivych vrcholu
     * @return počet úrovní
     */
    private int levelsDown(Vertex vertex, RevisionInterval revisionInterval, Map<Long, Integer> depthMap) {
        Long vertexId = (Long) vertex.getId();

        Integer cachedValue = depthMap.get(vertexId);
        if (cachedValue != null) {
            return cachedValue;
        }

        List<Vertex> children = GraphOperation.getDirectChildren(vertex, revisionInterval);
        int calculatedDepth;
        if (children.isEmpty()) {
            calculatedDepth = 0;
        } else {
            int maxDepth = 0;
            for (Vertex child : children) {
                int depth = levelsDown(child, revisionInterval, depthMap);
                if (depth > maxDepth) {
                    maxDepth = depth;
                }
            }
            calculatedDepth = maxDepth + 1;
        }

        depthMap.put(vertexId, calculatedDepth);
        return calculatedDepth;
    }

    /**
     * Vrátí jméno pro daný vrchol.
     * @param vertex vrchol, pro který se zjišťuje jméno
     * @return jméno vrcholu
     */
    protected String getVertexName(Vertex vertex) {
        return GraphOperation.getName(vertex);
    }

    /**
     * @return držák na technické atributy
     */
    public TechnicalAttributesHolder getTechnicalAttributesHolder() {
        return technicalAttributesHolder;
    }

    /**
     * @param technicalAttributesHolder držák na technické atributy
     */
    public void setTechnicalAttributesHolder(TechnicalAttributesHolder technicalAttributesHolder) {
        this.technicalAttributesHolder = technicalAttributesHolder;
    }

    /**
     * @return Služba pro ukládání statsitik použití. 
     */
    public UsageStatsCollector getUsageStatsCollector() {
        return usageStatsCollector;
    }

    /**
     * @param usageStatsCollector Služba pro ukládání statsitik použití. 
     */
    public void setUsageStatsCollector(UsageStatsCollector usageStatsCollector) {
        this.usageStatsCollector = usageStatsCollector;
    }

    /**
     * @return Služba pro práci s vertikálními filtry. 
     */
    public FilteredRelationshipService getRelationshipService() {
        return relationshipService;
    }

    /**
     * @param relationshipService Služba pro práci s vertikálními filtry. 
     */
    public void setRelationshipService(FilteredRelationshipService relationshipService) {
        this.relationshipService = relationshipService;
    }

    /**
     * DTO pro shromáždění informací o hraně pro export.
     * @author tfechtner
     *
     */
    private class EdgeInfo {
        /** Csv info o cíly hrany. */
        private final VertexInfo source;
        /** Csv info o cíly hrany. */
        private final VertexInfo target;
        /** Label hrany. */
        private final EdgeLabel edgeLabel;
        /** Stav hrany v revizi. */
        private final RevisionState revisionState;

        /**
         * @param edgeLabel Label hrany
         * @param sourceQualifiedName plně kvalifikované zdroje hrany
         * @param targetQualifiedName plně kvalifikované cíle hrany
         * @param revisionState Stav hrany v revizi
         */
        EdgeInfo(EdgeLabel edgeLabel, String sourceQualifiedName, String targetQualifiedName,
                RevisionState revisionState) {
            this.edgeLabel = edgeLabel;
            this.source = new VertexInfo(sourceQualifiedName, revisionState);
            this.target = new VertexInfo(targetQualifiedName, revisionState);
            this.revisionState = revisionState;
        }

        /**
         * @return csv info o zdroji hrany
         */
        public VertexInfo getSource() {
            return source;
        }

        /**
         * @return csv info o cíly hrany
         */
        public VertexInfo getTarget() {
            return target;
        }

        /**
         * @return csv formát pro danou hranu.
         */
        public String[] getRowFormat() {
            List<String> row = new ArrayList<>();
            row.add(edgeLabel.getGraphRepr());

            row.add(source.getFullNodeName());
            row.add(target.getFullNodeName());

            row.add(source.getColumnName());
            row.add(source.getColumnType());
            row.add(target.getColumnName());
            row.add(target.getColumnType());

            row.add(source.getObjectName());
            row.add(source.getObjectType());
            row.add(target.getObjectName());
            row.add(target.getObjectType());

            row.add(source.getGroupName());
            row.add(source.getGroupType());
            row.add(target.getGroupName());
            row.add(target.getGroupType());

            row.add(source.getResourceName());
            row.add(source.getResourceType());
            row.add(target.getResourceName());
            row.add(target.getResourceType());

            row.add(revisionState.toString());

            return row.toArray(new String[row.size()]);
        }
    }

    /**
     * DTO pro shromáždění informací o vrcholu pro export.
     * @author tfechtner
     *
     */
    private class VertexInfo {
        /** Název sloupce. */
        private String columnName = "";
        /** Typ sloupce. */
        private String columnType = "";
        /** Název objektu. */
        private String objectName = "";
        /** Typ objektu. */
        private String objectType = "";
        /** Název group uzlu. */
        private String groupName = "";
        /** Typ group uzlu. */
        private String groupType = "";
        /** Název resource. */
        private String resourceName = "";
        /** Typ resource. */
        private String resourceType = "";
        /** Plně kvalifikované jméno vrcholu. */
        private final String fullNodeName;
        /** Revision state daného vrcholu. */
        private final RevisionState revisionState;

        /**
         * @param fullNodeName Plně kvalifikované jméno vrcholu.
         * @param revState 
         */
        VertexInfo(String fullNodeName, RevisionState revState) {
            super();
            this.fullNodeName = fullNodeName;
            this.revisionState = revState;
        }

        /**
         * @param vertex vrchol reprezenutjící sloupec
         */
        public void setColumn(Vertex vertex) {
            if (vertex != null) {
                columnName = getVertexName(vertex);
                columnType = GraphOperation.getType(vertex);
            }
        }

        /**
         * @param vertex vrchol reprezenutjící objekt
         */
        public void setObject(Vertex vertex) {
            if (vertex != null) {
                objectName = getVertexName(vertex);
                objectType = GraphOperation.getType(vertex);
            }
        }

        /**
         * @param vertex vrchol reprezenutjící group
         */
        public void setGroup(Vertex vertex) {
            if (vertex != null) {
                groupName = getVertexName(vertex);
                groupType = GraphOperation.getType(vertex);
            }
        }

        /**
         * @param vertex vrchol, pro ketrý se ukládá resource
         */
        public void setResourceOfVertex(Vertex vertex) {
            if (vertex != null) {
                Vertex resource = GraphOperation.getResource(vertex);
                resourceName = getVertexName(resource);
                resourceType = GraphOperation.getType(resource);
            }
        }

        public void init(Vertex vertex, int downLevel) {
            setGroup(findGroupVertex(vertex));
            setResourceOfVertex(vertex);

            if (downLevel == 0) {
                //jedná se nejspíš o sloupec tabulky
                setColumn(vertex);
                setObject(GraphOperation.getParent(vertex));
            } else if (downLevel == 1) {
                //jedná se nejspíš o tabulku (údaj pro sloupec prázdný)
                setObject(vertex);
            }
        }

        /**
         * @return Název sloupce. 
         */
        public String getColumnName() {
            return columnName;
        }

        /**
         * @return Typ sloupce. 
         */
        public String getColumnType() {
            return columnType;
        }

        /**
         * @return Název objektu. 
         */
        public String getObjectName() {
            return objectName;
        }

        /**
         * @return Typ objektu
         */
        public String getObjectType() {
            return objectType;
        }

        /**
         * @return Název group uzlu. 
         */
        public String getGroupName() {
            return groupName;
        }

        /**
         * @return Typ group uzlu. 
         */
        public String getGroupType() {
            return groupType;
        }

        /**
         * @return Název resource.
         */
        public String getResourceName() {
            return resourceName;
        }

        /**
         * @return Typ resource
         */
        public String getResourceType() {
            return resourceType;
        }

        /**
         * @return plně kvalifikované jméno uzlu
         */
        public String getFullNodeName() {
            return fullNodeName;
        }

        /**
         * @return csv formát pro daný vrchol
         */
        public String[] getRowFormat() {
            return getRowFormat("", "");
        }

        /**
         * Vypíše csv formát pro daný vrchol s daným atributem.
         * @param attrKey atribut klíč
         * @param attrValue atribut value
         * @return csv formát pro daný vrchol
         */
        public String[] getRowFormat(String attrKey, String attrValue) {
            return new String[] { fullNodeName, columnName, columnType, objectName, objectType, groupName, groupType,
                    resourceName, resourceType, revisionState.toString(), attrKey, attrValue };
        }
    }
    
    /**
     * Ciselnik oddelovacu v CSV souborech
     *   
     * @author onouza
      */
    public static enum CsvSeparator {
        /** Carka */
        COMMA(','),
        /** Tecka */
        DOT('.'),
        /** Dvojtecka */
        COLON(':'),
        /** Strednik */
        SEMICOLON(';'),
        /** Lomitko */
        SLASH('/'),
        /** Znak tabulatoru */
        TAB_CHAR('\t'), 
        /** Mezera */
        SPACE(' ');
       
        /**
         * Znak oddelovace
         */
        private final char character;

        /**
         * Konstruktor.
         * 
         * @param character Znak oddelovace
         */
        private CsvSeparator(char character) {
            this.character = character;
        }

        // gettery
        
        /**
         * @return Znak oddelovace
         */
        private char getCharacter() {
            return character;
        }
    }
    
}
