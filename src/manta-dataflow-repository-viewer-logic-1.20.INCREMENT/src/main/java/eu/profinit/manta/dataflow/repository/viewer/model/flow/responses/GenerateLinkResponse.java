package eu.profinit.manta.dataflow.repository.viewer.model.flow.responses;

/**
 * Response pro commadn generující permalinky, třída nese výsledný link.
 * @author tfechtner
 *
 */
public class GenerateLinkResponse extends BaseResponse {

    /** Vygenerovaný permalink. */
    private String link;
    
    /**
     * Default konstruktor předávající předkovi název commandu.
     */
    public GenerateLinkResponse() {
        super("generate-link");        
    }

    /**
     * @return Vygenerovaný permalink. 
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link Vygenerovaný permalink. 
     */
    public void setLink(String link) {
        this.link = link;
    }
}
