package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Třída pro udržování přehledu o stavu uzlů v rámci hledání hran mezi již navštívenými uzly. <br>
 * @author tfechtner
 *
 */
public class AggStateHolder {
    /** Mapa stavů pro jednotlivé uzly, kde klíčem je jejich id. */
    private Map<Long, AggState> stateMap = new HashMap<>();

    /**
     * Získá stav pro daný uzel. Pokud neexistuje, je založen.
     * @param vertex uzel, pro který se stav zjišťuje
     * @return stav uzlu, nikdy null
     */
    private AggState getState(Vertex vertex) {
        Long id = (Long) vertex.getId();
        AggState status = stateMap.get(id);

        if (status != null) {
            return status;
        } else {
            AggState newStatus = new AggState();
            stateMap.put(id, newStatus);
            return newStatus;
        }
    }

    /**
     * Přidá pro daný cílový uzel nový příchozí.
     * @param incomingVertex vkládaný příchozí uzel
     * @param targetVertex cílový uzel, kam se příchozí vkládá
     */
    public void addIncomingVertex(IncomingVertex incomingVertex, Vertex targetVertex) {
        AggState targetState = getState(targetVertex);
        targetState.addIncomingVertex(incomingVertex);
    }

    /**
     * Ověří, jestli byl již uzel navštíven.
     * @param actualVertex ověřovaný uzel
     * @return true, jestliže byl uzel navštíven
     */
    public boolean isVisited(Vertex actualVertex) {
        return getState(actualVertex).isVisited();
    }

    /**
     * Poznačí uzel jako navštívený.
     * @param actualVertex navštěvovaný uzel
     */
    public void visit(Vertex actualVertex) {
        getState(actualVertex).visit();
    }

    /**
     * Ověří, jestli byl již uzel vyřešen.
     * @param actualVertex ověřovaný uzel
     * @return true, jestliže byl uzel vyřešen
     */
    public boolean isResloved(Vertex actualVertex) {
        return getState(actualVertex).isResolved();
    }

    /**
     * Označí uzel jako vyřešený.
     * @param actualVertex řešený uzel
     */
    public void resolve(Vertex actualVertex) {
        getState(actualVertex).resolve();
    }

    /**
     * Získá množinu příchozích uzlů.
     * @param actualVertex uzel, pro který se množina vrací
     * @return množina příchozích uzlů, nikdy null
     */
    public Set<IncomingVertex> getIncomingVertices(Vertex actualVertex) {
        return getState(actualVertex).getIncomingVertices();
    }

    @Override
    public String toString() {
        return "{" + stateMap + "}";
    }

    /**
     * Stav uzlu v rámci hledání hran mezi již visited uzly.
     * @author tfechtner
     *
     */
    public static final class AggState {
        /** Množina příchozích uzlů. */
        private final Set<IncomingVertex> incomingVertices = new HashSet<>();
        /** Jestliže byl již uzel zpracován. */
        private boolean isResolved = false;
        /** Jestliže byl již uzel navštíven v rámci heldání cest. */
        private boolean isVisited = false;

        /**
         * Přidá příchozí uzel.
         * @param incomingVertex vkládaný příchozí uzel
         */
        public void addIncomingVertex(IncomingVertex incomingVertex) {
            incomingVertices.add(incomingVertex);
        }

        /**
         * Označí uzel jako vyřešený.
         */
        public void resolve() {
            isResolved = true;
        }

        /**
         * Ověří, jestli byl již uzel vyřešen.
         * @return true, jestliže byl uzel vyřešen
         */
        public boolean isResolved() {
            return isResolved;
        }

        /**
         * Poznačí uzel jako navštívený.
         */
        public void visit() {
            isVisited = true;
        }

        /**
         * Ověří, jestli byl již uzel navštíven.
         * @return true, jestliže byl uzel navštíven
         */
        public boolean isVisited() {
            return isVisited;
        }

        /**
         *  Získá množinu příchozích uzlů.
         * @return množina příchozích uzlů, nikdy null
         */
        public Set<IncomingVertex> getIncomingVertices() {
            return Collections.unmodifiableSet(incomingVertices);
        }

        @Override
        public String toString() {
            return "{v:" + isVisited() + ",r:" + isResolved() + "," + incomingVertices + "}";
        }
    }

    /**
     * Immutable datová třída pro reprezentaci příchozího uzlu.
     * @author tfechtner
     *
     */
    public static final class IncomingVertex {
        /** Id uzlu. */
        private final Long id;
        /** Label hrany, která s příchozím uzlem cíl spojuje. */
        private final String label;
        /** Atributy příslušné hrany. */
        private final Map<String, Object> attrs;
        /** Interval revizí příchozí hrany. */
        private final RevisionInterval revisionInterval;

        /**
         * @param vertex titan reprezentace příchozího uzlu
         * @param label Label hrany, která s příchozím uzlem cíl spojuje.
         * @param attrs Atributy příslušné hrany
         */
        public IncomingVertex(Vertex vertex, String label, Map<String, Object> attrs,
                RevisionInterval revisionInterval) {
            this((Long) vertex.getId(), label, attrs, revisionInterval);
        }

        /**
         * @param id Id uzlu.
         * @param label Label hrany, která s příchozím uzlem cíl spojuje.
         * @param attrs Atributy příslušné hrany
         * @param revisionInterval  interval revizí příchozí hrany
         */
        public IncomingVertex(Long id, String label, Map<String, Object> attrs, RevisionInterval revisionInterval) {
            super();
            this.id = id;
            this.label = label;
            this.attrs = attrs;
            this.revisionInterval = revisionInterval;
        }

        /**
         * @return Id uzlu.
         */
        public Long getId() {
            return id;
        }

        /**
         * @return Label hrany, která s příchozím uzlem cíl spojuje.
         */
        public String getLabel() {
            return label;
        }

        /**
         * @return Atributy příslušné hrany
         */
        public Map<String, Object> getAttrs() {
            return attrs;
        }

        /**
         * @return interval revizí příchozí hrany
         */
        public RevisionInterval getRevisionInterval() {
            return revisionInterval;
        }

        @Override
        public String toString() {
            return "IncV[" + id + ":" + label + ";" + revisionInterval.toString() + "]";
        }

        @Override
        public int hashCode() {
            HashCodeBuilder builder = new HashCodeBuilder();
            builder.append(id);
            builder.append(label);
            builder.append(revisionInterval);
            return builder.toHashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (obj.getClass() != getClass()) {
                return false;
            }
            AggStateHolder.IncomingVertex other = (AggStateHolder.IncomingVertex) obj;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(id, other.id);
            builder.append(label, other.label);
            builder.append(revisionInterval, other.revisionInterval);
            return builder.isEquals();
        }
    }
}