package eu.profinit.manta.dataflow.repository.viewer.service;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelsConfigurationNotFoundException;
import eu.profinit.manta.dataflow.repository.viewer.model.TypeInfo;

/**
 * Implementace poskytovatele konfigurace urovni, ktera pouziva XML soubor jako
 * svuj datovy zdroj.
 * 
 * @author Matyas Krutsky <matyas.krutsky@profinit.eu>
 */
public class XmlLevelMapProvider implements LevelMapProvider, ResourceLoaderAware {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlLevelMapProvider.class);

    private static final String DEFAULT_RESOURCE = "Default";

    /** Výchozí type info pro neznámé dvojice technologie:typ. Je nastaven jako bottom non-atomic.*/
    private final TypeInfo defaultTypeInfo;

    private Map<String, TypeInfo> levelsMap;

    private Map<String, Set<String>> groupMap;

    private String levelsFileLocation;

    private ResourceLoader resourceLoader;

    /**
     * Default constructor.
     */
    public XmlLevelMapProvider() {
        defaultTypeInfo = new TypeInfo();
        defaultTypeInfo.setLevel(FlowLevel.BOTTOM);
        defaultTypeInfo.setAtomic(false);
    }

    @Override
    public synchronized Map<String, TypeInfo> getLevelsMap() {
        if (levelsMap == null) {
            initializeMaps();
        }
        return levelsMap;
    }

    @Override
    public TypeInfo getTypeInfo(String technology, String type) {
        Map<String, TypeInfo> map = getLevelsMap();
        TypeInfo typeInfo = map.get(technology + ":" + type);
        if (typeInfo != null) {
            return typeInfo;
        } else {
            // zkusime default technologii
            typeInfo = map.get(DEFAULT_RESOURCE + ":" + type);
            if (typeInfo != null) {
                return typeInfo;
            } else {
                LOGGER.warn("Unknown TypeInfo for pair " + technology + ":" + type + ".");
                return defaultTypeInfo;
            }
        }
    }

    /**
     * inicializuje položky groupMap a levelsMap.
     */
    private void initializeMaps() {
        try {
            InputStream stream = resourceLoader.getResource(levelsFileLocation).getInputStream();
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            LevelMapHandler handler = new LevelMapHandler();
            saxParser.parse(stream, handler);

            this.groupMap = handler.getGroupMap();
            this.levelsMap = handler.getLevelMap();
        } catch (Exception e) {
            throw new LevelsConfigurationNotFoundException("File with level configuration not found!", e);
        }
    }

    /**
     * @param levelsFileLocation umístění souboru s úrovněmi
     */
    public void setLevelsFileLocation(String levelsFileLocation) {
        this.levelsFileLocation = levelsFileLocation;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    /**
     * Třída pro naparsování souboru s úrovněmi.
     *
     */
    private static class LevelMapHandler extends DefaultHandler {
        private Map<String, TypeInfo> levelMap = new HashMap<String, TypeInfo>();
        private String tech;
        private Map<String, Set<String>> groupMap = new HashMap<>();

        @Override
        public void startElement(String uri, String localName, String qualifiedName, Attributes attributes)
                throws SAXException {

            if ("technology".equals(qualifiedName)) {
                tech = attributes.getValue("name");
                String grouptype = attributes.getValue("grouptype");
                if (grouptype != null) {
                    groupMap.put(tech, new HashSet<>(Arrays.asList(grouptype.split("\\|"))));
                }
            } else if ("map".equals(qualifiedName)) {
                String type = attributes.getValue("type");
                TypeInfo info = new TypeInfo();
                info.setLevel(FlowLevel.parseFromName(attributes.getValue("level")));
                info.setAtomic("true".equals(attributes.getValue("atomic")));

                levelMap.put(tech + ":" + type, info);
            }
        }

        public Map<String, TypeInfo> getLevelMap() {
            return levelMap;
        }

        public Map<String, Set<String>> getGroupMap() {
            return groupMap;
        }
    }

    @Override
    public synchronized Set<String> getGroupType(String technology) {
        if (groupMap == null) {
            initializeMaps();
        }
        Set<String> result = groupMap.get(technology);
        return result != null ? result : groupMap.get(DEFAULT_RESOURCE);
    }

}
