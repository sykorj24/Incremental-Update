package eu.profinit.manta.dataflow.repository.viewer.model.flow.responses;

/**
 * Odpověď na inicializaci ref view.
 * @author tfechtner
 *
 */
public class InitResponse extends BaseResponse {
    /** True, jestliže je odpověď v pořádku. */
    private boolean isOk = true;
    
    /**
     * 
     */
    public InitResponse() {
        super("init-refview");
    }

    /**
     * @return True, jestliže je odpověď v pořádku. 
     */
    public boolean isOk() {
        return isOk;
    }

    /**
     * @param isResponseOk True, jestliže je odpověď v pořádku. 
     */
    public void setOk(boolean isResponseOk) {
        this.isOk = isResponseOk;
    }
}
