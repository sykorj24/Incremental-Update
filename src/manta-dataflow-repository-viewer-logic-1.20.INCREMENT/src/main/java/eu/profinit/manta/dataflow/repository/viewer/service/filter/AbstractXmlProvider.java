package eu.profinit.manta.dataflow.repository.viewer.service.filter;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;
import org.xml.sax.SAXException;

/**
 * Společný předek pro providery nastavení z xml souboru.
 * @author tfechtner
 *
 */
public abstract class AbstractXmlProvider implements ResourceLoaderAware {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractXmlProvider.class);
    /** Spring resource loader. */
    private ResourceLoader resourceLoader;
    /** Umístění souboru s filtry. */
    private String filtersFileLocation;

    /**
     * @param filtersFileLocation Umístění souboru s filtry.
     */
    public void setFiltersFileLocation(String filtersFileLocation) {
        this.filtersFileLocation = filtersFileLocation;
    }

    /**
     * Vtvoří mapu s filtry ze souboru.
     * @return mapa s filtry, nikdy null
     * @param <T> datový typ odpovědi
     */
    protected final <T> T extractDataFromXml() {
        if (resourceLoader != null && filtersFileLocation != null) {
            try {
                InputStream stream = resourceLoader.getResource(filtersFileLocation).getInputStream();
                SAXParserFactory factory = SAXParserFactory.newInstance();
                SAXParser saxParser = factory.newSAXParser();

                return handleParsing(saxParser, stream);
            } catch (SAXException e) {
                LOGGER.error("File with configuration is not correct XML.", e);
                return null;
            } catch (IOException e) {
                LOGGER.error("File with configuration is not found.", e);
                return null;
            } catch (ParserConfigurationException e) {
                LOGGER.error("Cannot instantiate SAX parser.", e);
                return null;
            }
        } else {
            LOGGER.error("Resource loader and file location must not be null.");
            return null;
        }
    }

    /**
     * Metoda pro zpracování vstupního xmlka.
     * @param saxParser sax parser pro zpracování stremu
     * @param stream stream dat ke zpracování
     * @return výstup parsování podle konkrétní implementace
     * @throws SAXException chyba při parsování
     * @throws IOException chyba při tení vstupu
     * @param <T> typ výstupu
     */
    protected abstract <T> T handleParsing(SAXParser saxParser, InputStream stream) throws SAXException, IOException;

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
}
