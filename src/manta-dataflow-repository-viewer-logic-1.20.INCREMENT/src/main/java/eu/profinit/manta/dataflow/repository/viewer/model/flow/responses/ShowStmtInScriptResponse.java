package eu.profinit.manta.dataflow.repository.viewer.model.flow.responses;

/**
 * Response na žádost o zobrazení příkazu v daném skriptu. 
 * @author tfechtner
 *
 */
public class ShowStmtInScriptResponse extends BaseResponse {

    /** Číslo řádku, na kterém se nachází příkaz ve skriptu. */
    private int line;    
    /** Číslo sloupce, na kterém se nachází příkaz ve skriptu. */
    private int col;
    /** Text zdrojového souboru. */
    private String sourceCode;
        
    /**
     * Default konstruktor předávající předkovi název commandu.
     */
    public ShowStmtInScriptResponse() {
        super("show-stmt-in-script");
    }

    /**
     * @return Číslo řádku, na kterém se nachází příkaz ve skriptu.
     */
    public int getLine() {
        return line;
    }

    /**
     * @param line Číslo řádku, na kterém se nachází příkaz ve skriptu.
     */
    public void setLine(int line) {
        this.line = line;
    }

    /**
     * @return Číslo sloupce, na kterém se nachází příkaz ve skriptu.
     */
    public int getCol() {
        return col;
    }

    /**
     * @param col Číslo sloupce, na kterém se nachází příkaz ve skriptu.
     */
    public void setCol(int col) {
        this.col = col;
    }

    /**
     * @return Text zdrojového souboru.
     */
    public String getSourceCode() {
        return sourceCode;
    }

    /**
     * @param sourceCode Text zdrojového souboru.
     */
    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }
}
