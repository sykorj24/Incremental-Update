package eu.profinit.manta.dataflow.repository.viewer.model.flow.requests;

import java.util.List;

/******************************************************************************
 *   Dotaz pro ziskani separatnich komponent. 
 */

public class ComponentsRequest extends AbstractServerStateAwareRequest {

    private List<Long> componentIDs; 
    private Integer sessionIndex;

    /**
     * @return IDcka komponent, ktere chceme vratit
     */
    public List<Long> getComponentIDs() {
        return componentIDs;
    }

    /**
     * @param componentIDs IDcka komponent, ktere chceme vratit
     */
    public void setComponentIDs(List<Long> componentIDs) {
        this.componentIDs = componentIDs;
    }
    
    /**
     * @return index session pro získání příslušného flowState
     */
    public Integer getSessionIndex() {
        return sessionIndex;
    }

    /**
     * @param sessionIndex index session pro získání příslušného flowState
     */
    public void setSessionIndex(Integer sessionIndex) {
        this.sessionIndex = sessionIndex;
    }

}
