package eu.profinit.manta.dataflow.repository.viewer.security;

/**
 * Seznam rolí specifických pro modul viewer.
 * @author tfechtner
 *
 */
public final class ViewerRoles {

    private ViewerRoles() {
    }

    /**
     * Role vieweru, pro zobrazení datových toků.
     */
    public static final String VIEWER_DATAFLOW = "ROLE_VIEWER_DATAFLOW";

    /**
     * Role vieweru, pro zobrazení katalogu.
     */
    public static final String VIEWER_CATALOG = "ROLE_VIEWER_CATALOG";
}
