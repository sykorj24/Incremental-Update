package eu.profinit.manta.dataflow.repository.viewer.model.flow.responses;

import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message.Severity;

/******************************************************************************
 *  Zakladni trida pro vsechny odpovedi serveru.    
 */

public class BaseResponse extends AbstractServerResponse {
    /** Ok odpověď. */
    public static final BaseResponse OK = new BaseResponse("message", new Message(Severity.INFO, "OK"));
        
    /**
     * @param type Typ odpovedi 
     */
    public BaseResponse(String type) {
        super(type);
    }

    /**
     * Konstruktor pro vytvoření odpovědí s jednou odpovědí.
     * @param type typ odpovědi
     * @param message zpráva pro vložení
     */
    public BaseResponse(String type, Message message) {
        super(type);
        addMessage(message);
    }
    
}
