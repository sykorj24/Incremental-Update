package eu.profinit.manta.dataflow.repository.viewer.exception;

/**
 * Výjimka pro případ poškozeného requestu.
 * @author tfechtner
 *
 */
public class CorruptedRequestException extends RuntimeException {

    private static final long serialVersionUID = 4307062863707467370L;

    /**
     * @param message text výjimky
     */
    public CorruptedRequestException(String message) {
        super(message);
    }

}
