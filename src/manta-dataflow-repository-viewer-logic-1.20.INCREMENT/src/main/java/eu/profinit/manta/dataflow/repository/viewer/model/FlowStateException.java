package eu.profinit.manta.dataflow.repository.viewer.model;

/**
 * Výjimka značící problém s flowState.
 * @author tfechtner
 *
 */
public class FlowStateException extends Exception {
    private static final long serialVersionUID = 8150737552452264037L;

    /**
     * @param message zpráva k zalogování
     */
    public FlowStateException(String message) {
        super(message);
    }

    /**
     * @param message zpráva k zalogování
     * @param cause způsobující výjimka 
     */
    public FlowStateException(String message, Throwable cause) {
        super(message, cause);
    }
}
