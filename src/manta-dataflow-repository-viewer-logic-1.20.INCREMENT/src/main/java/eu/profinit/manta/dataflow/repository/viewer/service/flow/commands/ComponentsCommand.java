package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Component;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.ComponentFactory;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.DataInfo;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ComponentsRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ComponentsResponse;
import eu.profinit.manta.dataflow.repository.viewer.service.DataSizeChecker;
import eu.profinit.manta.dataflow.repository.viewer.service.FilteredRelationshipService;

/**
 * Algoritmus pro ziskani komponent podle zadanych id.
 * 
 * @author Matyas Krutsky <matyas.krutsky@profinit.eu>
 */
public class ComponentsCommand extends AbstractCommand<ComponentsRequest, ComponentsResponse> {
    @Autowired
    private FilteredRelationshipService relationshipService;

    @Autowired
    private DataSizeChecker dataSizeChecker;

    @Autowired
    private ComponentFactory componentFactory;

    @Override
    public ComponentsResponse execute(ComponentsRequest request, FlowStateViewer flowState, TitanTransaction trans) {
        List<Component> components = new ArrayList<>();

        for (long id : request.getComponentIDs()) {
            if (flowState.getNodeStatus(id) != NodeStatus.UNKNOWN) {
                Vertex node = trans.getVertex(id);
                components.add(componentFactory.createFullComponent(trans, node, flowState, getLevelMapProvider(),
                        relationshipService));
                flowState.markNodeAsFull(id);
            }
        }

        DataInfo dataInfo = new DataInfo(components, null, dataSizeChecker);

        ComponentsResponse response = new ComponentsResponse();
        response.setDataInfo(dataInfo);
        if (dataInfo.isDataOk()) {
            response.setComponents(components);
        } else {
            response.setComponents(Collections.<Component> emptyList());
        }
        return response;
    }

    /**
     * @param relationshipService služba pro práci s předky 
     */
    public void setRelationshipService(FilteredRelationshipService relationshipService) {
        this.relationshipService = relationshipService;
    }

    /**
     * @param dataSizeChecker služba pro kontrolu velikosti odpovědi
     */
    public void setDataSizeChecker(DataSizeChecker dataSizeChecker) {
        this.dataSizeChecker = dataSizeChecker;
    }

    /**
     * @param componentFactory továrna na komponenty
     */
    public void setComponentFactory(ComponentFactory componentFactory) {
        this.componentFactory = componentFactory;
    }
    
    
}
