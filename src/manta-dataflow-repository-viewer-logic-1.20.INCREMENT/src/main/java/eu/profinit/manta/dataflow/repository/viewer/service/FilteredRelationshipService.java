package eu.profinit.manta.dataflow.repository.viewer.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.filter.vertical.VerticalFilterProvider;

/**
 * Služba zajišťující aplikaci vertikálních filtru na všechny grafové operace týkající se předek-potomek vztahů.
 * @author tfechtner
 *
 */
public class FilteredRelationshipService {
    @Autowired
    private VerticalFilterProvider filterProvider;

    /**
     * @param filterProvider provider pro vertikální filtraci
     */
    public void setFilterProvider(VerticalFilterProvider filterProvider) {
        this.filterProvider = filterProvider;
    }

    /**
     * Získá prvního nedeaktivovaného předka.
     * @param node uzel, pro který se hledá předek 
     * @return první nedeaktivovaný předek, nebo null, pokud takový neexistuje
     */
    public Vertex getParent(final Vertex node) {
        if (node != null) {
            Vertex parent = GraphOperation.getParent(node);
            while (parent != null && filterProvider.isElementDisabled(parent, node)) {
                parent = GraphOperation.getParent(parent);
            }

            if (parent != null) {
                return parent;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Seznam přímých potomků, přičemž se přeskakují deaktivované uzly. <br>
     * Pokud je takový potomek deaktivován, rekurzivně se prověří jeho přímí potomci.
     * @param rootNode uzel, pro který se hledají potomci
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return seznam přímých potomků, nikdy null
     */
    public List<Vertex> getDirectChildren(Vertex rootNode, RevisionInterval revisionInterval) {
        List<Vertex> resultChildrenList = new ArrayList<Vertex>();

        if (rootNode != null) {
            Deque<Vertex> nodeDeque = new LinkedList<Vertex>();
            
            // do fronty přidáme rovnou potomky, ať nemusíme dále brát root vpotaz
            nodeDeque.addAll(GraphOperation.getDirectChildren(rootNode, revisionInterval));

            while (nodeDeque.size() > 0) {
                Vertex actualNode = nodeDeque.pollLast();

                if (!filterProvider.isElementDisabled(actualNode, actualNode)) {
                    // není filtrovaný -> přidat jako přímého potomka
                    resultChildrenList.add(actualNode);
                } else {
                    // je filtrovaný -> project jeho potomky
                    nodeDeque.addAll(GraphOperation.getDirectChildren(actualNode, revisionInterval));
                }
            }
        }

        return resultChildrenList;
    }

    /**
     * Získá všechny nedekativované předky v pořadí od přímého předka po root. 
     * @param node uzel, pro kterého se předci hledají
     * @return seznam předků, nikdy null
     */
    public List<Vertex> getAllParents(Vertex node) {
        List<Vertex> parentList = new ArrayList<Vertex>();

        if (node != null) {
            Vertex actualNode = node;
            while ((actualNode = GraphOperation.getParent(actualNode)) != null) {
                // přidat do seznamu pouze pokud není filtrovaný
                if (!filterProvider.isElementDisabled(actualNode, node)) {
                    parentList.add(actualNode);
                }
            }
        }

        return parentList;
    }

    /**
     * Zjistí jestli je daný uzel list grafu, přičemž nebere v potaz uzly odfiltrované veritkálním filtrem .
     * @param rootNode uzel, pro který se zjišťuje jestli je listem
     * @param revisionInterval interval revizí daného flowState
     * @return true, jestliže je uzel list
     */
    public boolean isList(Vertex rootNode, RevisionInterval revisionInterval) {
        if (rootNode != null) {
            Deque<Vertex> nodeDeque = new LinkedList<Vertex>();
            nodeDeque.add(rootNode);

            while (nodeDeque.size() > 0) {
                Vertex actualNode = nodeDeque.pollLast();

                List<Vertex> children = GraphOperation.getDirectChildren(actualNode, revisionInterval);
                for (Vertex child : children) {
                    if (filterProvider.isElementDisabled(child, child)) {
                        nodeDeque.add(child);
                    } else {
                        return false;
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Vrátí jestli je daný uzel atomický.
     * @param node zkoumaný uzel
     * @param levelMapProvider provider pro informace o úrovních
     * @param revisionInterval interval revizí daného flowState
     * @return true, jestliže je atomický
     */
    public boolean isAtomic(Vertex node, LevelMapProvider levelMapProvider, RevisionInterval revisionInterval) {
        boolean isPredicatedAtomic = levelMapProvider.getTypeInfo(ViewerOperation.getTechnologyType(node),
                ViewerOperation.getType(node)).isAtomic();

        if (isPredicatedAtomic) {
            return isList(node, revisionInterval);
        } else {
            return false;
        }
    }

    /**
     * Vrátí všechny sourozence daného uzlu.
     * @param vertex vrchol, pro který se hledají sourozenci
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return množina sourozenců, nikdy null
     */
    public Set<Vertex> getSiblings(Vertex vertex, RevisionInterval revisionInterval) {
        if (vertex == null) {
            return Collections.emptySet();
        }

        Vertex parent = getParent(vertex);
        if (parent == null) {
            return Collections.emptySet();
        }

        Set<Vertex> siblings = new HashSet<>();
        List<Vertex> children = getDirectChildren(parent, revisionInterval);
        for (Vertex child : children) {
            if (!child.getId().equals(vertex.getId())) {
                siblings.add(child);
            }
        }

        return siblings;
    }

    /**
     * Nalezne první vrchol, který má větší rovno úroveň jak je požadována. <br>
     * Začíná se zkoumat od daného vrcholu a pak se pokračuje v předcích od nejbližšího   
     * @param vertex vrchol jenž zkoumáme
     * @param flowLevel požadovaná úroveň
     * @param levelMapProvider provider pro zjišťování úrovní
     * @return vrchol s hledanou úrovní nebo kořen hierarchie, pokud nikdo takovou úroveň nemá
     */
    public Vertex getFirstVertexHighEnough(Vertex vertex, FlowLevel flowLevel, LevelMapProvider levelMapProvider) {

        while (!ViewerOperation.getLevel(vertex, levelMapProvider).isHigherThan(flowLevel)) {
            Vertex parent = this.getParent(vertex);
            if (parent != null) {
                vertex = parent;
            } else {
                break;
            }
        }

        return vertex;
    }

    /**
     * Aplikuje na množinu startovních uzlů vertikální filtry.
     * Pokud je nějaký uzel stratovní vyfiltrován, startovními se stanou místo něho všichni přímí potomci.
     * @param transaction transakce pro přístup k db
     * @param startNodeIds množina id startovních uzlu k vyfiltrování
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return množina startovních uzlů, nikdy null
     */
    public Collection<Vertex> filterStartNodes(TitanTransaction transaction, List<Long> startNodeIds,
            RevisionInterval revisionInterval) {
        Set<Vertex> startNodes = new HashSet<>();

        if (startNodeIds != null) {
            for (Long vertexId : startNodeIds) {
                Vertex vertex = transaction.getVertex(vertexId);
                if (vertex != null) {
                    if (filterProvider.isElementDisabled(vertex, vertex)) {
                        startNodes.addAll(getDirectChildren(vertex, revisionInterval));
                    } else {
                        startNodes.add(vertex);
                    }
                }
            }
        }

        return startNodes;
    }
}
