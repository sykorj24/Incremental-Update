package eu.profinit.manta.dataflow.repository.viewer.model.flow.requests;

import java.util.Set;

/**
 * Dotaz pro operaci na zjišťování odfiltrování uzlů.
 * @author tfechtner
 *
 */
public class HiddenByFiltersRequest implements ServerRequest {
    /** Množina id komponent pro ověření. */
    private Set<Long> componentIds;
    /** Množina id aktivních filtrů. */
    private Set<String> activeFilters;
    /** Session id. */
    private Integer sessionIndex;

    /**
     * @return Množina id komponent pro ověření
     */
    public Set<Long> getComponentIds() {
        return componentIds;
    }

    /**
     * @param componentIds Množina id komponent pro ověření
     */
    public void setComponentIds(Set<Long> componentIds) {
        this.componentIds = componentIds;
    }

    /**
     * @return Množina id aktivních filtrů
     */
    public Set<String> getActiveFilters() {
        return activeFilters;
    }

    /**
     * @param activeFilters Množina id aktivních filtrů
     */
    public void setActiveFilters(Set<String> activeFilters) {
        this.activeFilters = activeFilters;
    }

    /**
     * @return Session id
     */
    public Integer getSessionIndex() {
        return sessionIndex;
    }

    /**
     * @param sessionIndex Session id
     */
    public void setSessionIndex(Integer sessionIndex) {
        this.sessionIndex = sessionIndex;
    }
}
