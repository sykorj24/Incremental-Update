package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.GenerateLinkResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.index.WelcomeFormModel;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;
import eu.profinit.manta.platform.web.core.security.SecurityHelper;

/**
 * Command pro vytvoření permalinku na základě dat z welcome formuláře.
 * @author tfechtner
 *
 */
public class GenerateLinkCommand extends AbstractCommand<WelcomeFormModel, GenerateLinkResponse> {
    /** Název akce pro usage stats pro vygenerování permalinku. */
    private static final String FLOW_VIS_PERMA_LINK = "flow_vis_permaLink";
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(GenerateLinkCommand.class);

    /** Služba pro ukládání statsitik použití. */
    @Autowired
    private UsageStatsCollector usageStatsCollector;

    @Override
    public GenerateLinkResponse execute(WelcomeFormModel request, FlowStateViewer flowState,
            TitanTransaction transaction) {
        StringBuilder sb = new StringBuilder();

        sb.append("depth=");
        sb.append(request.getDepth());
        sb.append("&direction=");
        sb.append(request.getDirection());
        sb.append("&filter=");
        sb.append(request.getFilter());
        sb.append("&filterEdges=");
        sb.append(request.isFilterEdges());
        sb.append("&level=");
        sb.append(request.getLevel());

        generatedSelectedItems(request.getSelectedItems(), transaction, sb);

        String url = "";
        try {
            URI uri = new URI(null, null, "viewer/dataflow/link", sb.toString(), null);
            url = uri.toASCIIString();
        } catch (URISyntaxException e) {
            LOGGER.error("The error has occured during generating permalink.", e);
        }

        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_VIS_PERMA_LINK,
                Collections.singletonMap("flowStateGuid", flowState.getGuid()));

        GenerateLinkResponse response = new GenerateLinkResponse();
        response.setLink(url);
        return response;
    }

    /**
     * Vygeneruje parametry reprezentující vybrané uzly.
     * @param selectedItems pole id vybraných uzlů
     * @param transaction transakce pro přístup do db
     * @param sb builder, kam se zapisuje výstup
     */
    private void generatedSelectedItems(String[] selectedItems, TitanTransaction transaction, StringBuilder sb) {
        int index = 0;
        for (String stringId : selectedItems) {
            try {
                Long id = Long.valueOf(stringId);
                Vertex vertex = transaction.getVertex(id);
                if (vertex != null) {
                    genererateOneItem(vertex, index, sb);
                    index++;
                }
            } catch (NumberFormatException e) {
                LOGGER.error("Cannot parse selected item id " + stringId + ".", e);
            }
        }
    }

    /**
     * Vygeneruje parametry reprezentující jeden startovní uzel.
     * @param vertex daný vrchol
     * @param index pořadí vrcholu v seznamu
     * @param sb builder, kam se zapisuje výstup
     */
    private void genererateOneItem(Vertex vertex, int index, StringBuilder sb) {
        Vertex resource = GraphOperation.getTopParentResource(vertex);
        generateResource(resource, index, sb);

        int parentIndex = 1;
        List<Vertex> parents = GraphOperation.getAllParent(vertex);
        for (int i = parents.size() - 1; i >= 0; i--) {
            generateVertex(parents.get(i), index, parentIndex, sb);
            parentIndex++;
        }
        generateVertex(vertex, index, parentIndex, sb);
    }

    /**
     * Vygeneruje parametr pro jednoho předka/vrchol.
     * @param vertex daný vrchol nebo jeho předek
     * @param index  pořadí startovního vrcholu v seznamu
     * @param parentIndex pořadí v posloupnosti předků, kde nejvyšší index je pro vrchol samotný
     * @param sb builder, kam se zapisuje výstup
     */
    private void generateVertex(Vertex vertex, int index, int parentIndex, StringBuilder sb) {
        sb.append("&si[").append(index).append("][").append(parentIndex).append("][0]=");
        sb.append(vertex.getProperty(NodeProperty.NODE_NAME.t()));
        sb.append("&si[").append(index).append("][").append(parentIndex).append("][1]=");
        sb.append(vertex.getProperty(NodeProperty.NODE_TYPE.t()));
    }

    /**
     * Vygeneruje parametr pro resource.
     * @param vertex vertex představující resource
     * @param index pořadí startovního vrcholu v seznamu
     * @param sb builder, kam se zapisuje výstup
     */
    private void generateResource(Vertex vertex, int index, StringBuilder sb) {
        sb.append("&si[").append(index).append("][0][0]=");
        sb.append(vertex.getProperty(NodeProperty.RESOURCE_NAME.t()));
        sb.append("&si[").append(index).append("][0][1]=");
        sb.append(vertex.getProperty(NodeProperty.RESOURCE_TYPE.t()));
    }

    /**
     * @return Služba pro ukládání statsitik použití. 
     */
    public UsageStatsCollector getUsageStatsCollector() {
        return usageStatsCollector;
    }

    /**
     * @param usageStatsCollector Služba pro ukládání statsitik použití. 
     */
    public void setUsageStatsCollector(UsageStatsCollector usageStatsCollector) {
        this.usageStatsCollector = usageStatsCollector;
    }
    
    
}
