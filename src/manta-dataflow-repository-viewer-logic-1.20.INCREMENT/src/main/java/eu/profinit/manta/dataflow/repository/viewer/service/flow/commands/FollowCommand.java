package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.DataFlowEdgesFinder.FlowFinderResult;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowEdge;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;
import eu.profinit.manta.dataflow.repository.viewer.exception.FollowFinderException;
import eu.profinit.manta.dataflow.repository.viewer.exception.TooManyEdgesException;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Component;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.ComponentFactory;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.DataInfo;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.FlowEdgeViz;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message.Severity;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.FollowRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.FollowResponse;
import eu.profinit.manta.dataflow.repository.viewer.service.DataSizeChecker;
import eu.profinit.manta.dataflow.repository.viewer.service.FilteredRelationshipService;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper.AggEdgesFinder;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper.FollowEdgesFinder;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper.VerticesHolder;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;
import eu.profinit.manta.platform.web.core.security.SecurityHelper;

/**
 * Algoritmus pro ziskani okoli uzlu podle zadanych parametru.
 */
public class FollowCommand extends AbstractCommand<FollowRequest, FollowResponse> {
    /** Název akce pro usage stats o nutnosti vypnutí agg hran z důvodu jejich množství. */
    private static final String FLOW_VIS_AGG_EDGES_ISSUE = "flow_vis_aggEdgesIssue";
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(FollowCommand.class);
    /** Zpráva pro uživatele, posílaná v případě nalezení příliš mnoha agg hran. */
    private static final String TOO_MANY_EDGES_MESSAGE = "Aggregated paths are not shown. There were too many of them.";

    @Autowired
    private FilteredRelationshipService relationshipService;

    @Autowired
    private DataSizeChecker dataSizeChecker;

    @Autowired
    private ComponentFactory componentFactory;

    @Autowired
    private UsageStatsCollector usageStatsCollector;

    @Autowired
    private TechnicalAttributesHolder technicalAttributes;

    @Override
    public FollowResponse execute(FollowRequest request, FlowStateViewer flowState, TitanTransaction transaction) {
        FollowResponse response = new FollowResponse();
        // uložit aktualizace z requestu do flowState
        flowState.setActiveFilters(request.getActiveFilters());
        flowState.setFlowLevel(request.getLevel());

        // zjistit pointery na požadované uzle
        List<Vertex> requestedNodes = findStartNodes(request.getComponentIds(), transaction, response);
        if (requestedNodes.isEmpty()) {
            return FollowResponse.NO_START_NODES;
        }

        // doplnit případné ekvivalentní uzly z porovnávací revize
        if (flowState.getRevisionInterval().getStart() != flowState.getRevisionInterval().getEnd()) {
            Set<Vertex> equivalentNodes = new HashSet<>();
            for (Vertex requested : requestedNodes) {
                Long equivalentId = flowState.getEquivalencesHolder().getEquivalentVertexId((Long) requested.getId());
                if (equivalentId != null) {
                    equivalentNodes.add(transaction.getVertex(equivalentId));
                }
            }
            requestedNodes.addAll(equivalentNodes);
        }

        Set<Vertex> startNodes = new HashSet<>();
        // startovní uzly jsou všechny listy požadovaných uzlů, musí být však v referenčním view
        for (Vertex v : requestedNodes) {
            List<Vertex> potentionalStartNodes = GraphOperation.getAllLists(v, flowState.getRevisionInterval());
            for (Vertex potentionalNode : potentionalStartNodes) {
                // ověřit, že je v referenčním view
                if (flowState.getNodeStatus(potentionalNode) != NodeStatus.UNKNOWN) {
                    startNodes.add(potentionalNode);
                }
            }
        }

        // pole pro výsledné hrany
        Set<FlowEdge> edges = new HashSet<FlowEdge>();

        Set<Vertex> newVisitedNodes = new HashSet<>();
        if (request.getRadius() > 0) {
            // najít hrany v rámci dalšího follow
            try {
                FollowEdgesFinder finder = new FollowEdgesFinder(relationshipService, getLevelMapProvider(),
                        transaction, flowState, dataSizeChecker, technicalAttributes);

                FlowFinderResult resultOut = finder.findEdges(startNodes, Direction.OUT, request.getRadius(),
                        newVisitedNodes);
                edges.addAll(resultOut.getEdges());

                FlowFinderResult resultIn = finder.findEdges(startNodes, Direction.IN, request.getRadius(),
                        newVisitedNodes);
                edges.addAll(resultIn.getEdges());

                // je třeba navštívit až po doběhnutí obou směrů 
                resultOut.visitProcessedVertices(transaction, flowState);
                resultIn.visitProcessedVertices(transaction, flowState);
            } catch (FollowFinderException e) {
                LOGGER.warn("FollowEdgesFinder error: {}", e.getMessage());
                return FollowResponse.TOO_LARGE_FOLLOW;
            }
        } else {
            // označit startovní uzly jako visited, protože se sice pošlou, ale flow se z nich nespustí
            for (Vertex node : startNodes) {
                if (flowState.visitNode(node)) {
                    newVisitedNodes.add(node);
                }
            }
        }

        VerticesHolder verticesHolder = new VerticesHolder(getLevelMapProvider(), transaction, flowState,
                relationshipService, componentFactory);

        // přidat startovní uzly a jejich sourozence
        verticesHolder.createComponentsForStartNodes(startNodes, newVisitedNodes);
        // objevit sourozence z hran
        verticesHolder.discoverSiblings(edges, newVisitedNodes);

        // zjistit komponenty z hran
        verticesHolder.createComponentsFromEdges(edges, startNodes);

        // nalézt hrany z/do visited uzlů
        AggEdgesFinder aggEdgesFinder = new AggEdgesFinder(transaction, flowState, verticesHolder,
                dataSizeChecker.getMaximumAggEdges(), technicalAttributes);

        if (!flowState.isAggEdgesDisabled()) {
            Set<FlowEdge> aggEdges = new HashSet<FlowEdge>();
            try {
                aggEdges.addAll(aggEdgesFinder.findEdges(newVisitedNodes, Direction.OUT));
                aggEdges.addAll(aggEdgesFinder.findEdges(newVisitedNodes, Direction.IN));
                edges.addAll(aggEdges);
            } catch (TooManyEdgesException e) {
                response.addMessage(new Message(Severity.WARNING, TOO_MANY_EDGES_MESSAGE));
                flowState.setAggEdgesDisabled(true);
                LOGGER.warn("Too many aggregating edges has been found (" + e.getEdgeCount()
                        + ") -> aggregating edges disabled.");

                usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_VIS_AGG_EDGES_ISSUE);

                edges.addAll(aggEdgesFinder.findDirectEdges(newVisitedNodes, Direction.OUT));
                edges.addAll(aggEdgesFinder.findDirectEdges(newVisitedNodes, Direction.IN));
            }
        } else {
            edges.addAll(aggEdgesFinder.findDirectEdges(newVisitedNodes, Direction.OUT));
            edges.addAll(aggEdgesFinder.findDirectEdges(newVisitedNodes, Direction.IN));
        }

        // zkontrolovat nově navštívené uzly, jestli nemají být rovnou finished
        for (Vertex newVisited : newVisitedNodes) {
            if (flowState.getNodeStatus(newVisited) == NodeStatus.VISITED) {
                flowState.tryToFinishNode(newVisited);
            }
        }

        // přidat předky
        verticesHolder.processParents(startNodes);
        // zkontrolovat viditelnost konců hran
        verticesHolder.checkEdgeEnds(edges);
        // uložit situaci kolem proxy stavů
        verticesHolder.persistFullNodes();

        // naplnit odpověď
        Set<Component> generatedComponents = verticesHolder.generateComponents(flowState.getEquivalencesHolder());

        // přemapovat hrany na ekvivalence v hlavní revizi
        Set<FlowEdgeViz> vizEdges = flowState.getEquivalencesHolder().remapEdges(transaction, edges,
                flowState.getRevisionInterval());

        DataInfo dataInfo = new DataInfo(generatedComponents, vizEdges, dataSizeChecker);
        response.setDataInfo(dataInfo);
        if (dataInfo.isDataOk()) {
            response.setComponents(generatedComponents);
            response.setFlows(vizEdges);
        } else {
            response.setComponents(Collections.<Component> emptySet());
            response.setFlows(Collections.<FlowEdgeViz> emptySet());
        }

        return response;
    }

    /**
     * @param relationshipService služba pro práci s předky 
     */
    public void setRelationshipService(FilteredRelationshipService relationshipService) {
        this.relationshipService = relationshipService;
    }

    /**
     * @param dataSizeChecker služba pro kontrolu velikosti odpovědi
     */
    public void setDataSizeChecker(DataSizeChecker dataSizeChecker) {
        this.dataSizeChecker = dataSizeChecker;
    }

    /**
     * @param componentFactory továrna na komponenty
     */
    public void setComponentFactory(ComponentFactory componentFactory) {
        this.componentFactory = componentFactory;
    }

    /**
     * @param technicalAttributes držák na technické atributy
     */
    public void setTechnicalAttributes(TechnicalAttributesHolder technicalAttributes) {
        this.technicalAttributes = technicalAttributes;
    }

    /**
     * Najde startovní uzly.
     * @param startNodeIds seznam id startovních uzlů
     * @param transaction transakce pro přístup db
     * @param resp odpověď na command
     * @return seznam starvoních uzlů, nmikdy null
     */
    private List<Vertex> findStartNodes(List<Long> startNodeIds, TitanTransaction transaction, FollowResponse resp) {
        List<Vertex> startNodes = new ArrayList<Vertex>();
        for (Long id : startNodeIds) {
            Vertex v = transaction.getVertex(id);
            if (v != null) {
                VertexType type = VertexType.getType(v);
                if (type == VertexType.NODE) {
                    startNodes.add(v);
                } else {
                    resp.addMessage(new Message(Severity.WARNING,
                            "The element with id " + id + " has a wrong type:" + type + "."));
                }
            } else {
                resp.addMessage(new Message(Severity.WARNING, "The element with id " + id + " does not exist."));
            }
        }
        return startNodes;
    }
}
