package eu.profinit.manta.dataflow.repository.viewer.model.flow.responses;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message;

/**
 * Abtrsaktní předek pro všechny serverové odpovědi přidávající možnost poslat zprávu. 
 * @author tfechtner
 *
 */
public abstract class AbstractServerResponse implements ServerResponse {
    private final String type;
    
    private final List<Message> messages = new ArrayList<>();

    private long serverState;

    /**
    * @param type Typ odpovedi 
    */
    protected AbstractServerResponse(String type) {
        this.type = type;
    }

    /**
    * @return Typ odpovedi 
    */
    public String getType() {
        return type;
    }
    
    /**
     * @return seznam zpráv v dané odpovědi
     */
    public List<Message> getMessages() {
        return Collections.unmodifiableList(messages);
    }
 
    /**
     * Přidá novou zprávu do odpovědi.
     * @param newMessage nově přidávaná zpráva
     */
    public void addMessage(Message newMessage) {
        messages.add(newMessage);
    }

    /**
     * @return timestamp, ktery urcuje flowState - stav serveru vzhledem k Undo
     */
    public long getServerState() {
        return serverState;
    }

    /**
     * @param serverState timestamp, ktery urcuje flowState - stav serveru vzhledem k Undo
     */
    public void setServerState(long serverState) {
        this.serverState = serverState;
    }
}
