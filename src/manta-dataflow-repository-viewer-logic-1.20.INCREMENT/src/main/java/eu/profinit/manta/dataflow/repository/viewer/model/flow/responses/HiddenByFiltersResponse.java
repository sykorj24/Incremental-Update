package eu.profinit.manta.dataflow.repository.viewer.model.flow.responses;

import java.util.Map;

/**
 * Odpověď pro operaci na zjišťování odfiltrování uzlů.
 * @author tfechtner
 *
 */
public class HiddenByFiltersResponse extends BaseResponse {
    /** Mapa stavů, kde id je id uzlu a hodnota jestli je odfiltrován. */
    private Map<Long, Boolean> filterStates;

    /**
     * Konstruktor nastavující název operace.
     */
    public HiddenByFiltersResponse() {
        super("hiddenByFilters");
    }

    /**
     * @return Mapa stavů, kde id je id uzlu a hodnota jestli je odfiltrován
     */
    public Map<Long, Boolean> getFilterStates() {
        return filterStates;
    }

    /**
     * @param filterStates Mapa stavů, kde id je id uzlu a hodnota jestli je odfiltrován
     */
    public void setFilterStates(Map<Long, Boolean> filterStates) {
        this.filterStates = filterStates;
    }

}
