package eu.profinit.manta.dataflow.repository.viewer.model;

/**
 * @author Matyas Krutsky <matyas.krutsky@profinit.eu>
 */
public class LevelsConfigurationNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 722254742381051958L;

    /**
     * 
     */
    public LevelsConfigurationNotFoundException() {
    }

    /**
     * @param message zpráva k zalogování
     */
    public LevelsConfigurationNotFoundException(String message) {
        super(message);
    }

    /**
     * @param message zpráva k zalogování
     * @param cause způsobující výjimka 
     */
    public LevelsConfigurationNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
