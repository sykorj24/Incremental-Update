package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import org.springframework.beans.factory.annotation.Autowired;

import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ServerRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ServerResponse;

/**
 * Společný abstraktní předek pro všechny commandy.
 * @author tfechtner
 *
 * @param <Q> typ requestu
 * @param <S> typ response
 */
public abstract class AbstractCommand<Q extends ServerRequest, S extends ServerResponse> implements Command<Q, S> {
    @Autowired
    private LevelMapProvider levelMapProvider;

    /**
     * @return provider pro zjištění úrovně jednotlivých typů v technologiích  
     */
    public LevelMapProvider getLevelMapProvider() {
        return levelMapProvider;
    }

    /**
     * @param levelMapProvider provider pro zjištění úrovně jednotlivých typů v technologiích
     */
    public void setLevelMapProvider(LevelMapProvider levelMapProvider) {
        this.levelMapProvider = levelMapProvider;
    }
        
}
