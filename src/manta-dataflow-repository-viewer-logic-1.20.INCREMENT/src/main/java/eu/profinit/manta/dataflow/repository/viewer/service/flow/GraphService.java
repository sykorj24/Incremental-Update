package eu.profinit.manta.dataflow.repository.viewer.service.flow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionModel;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.viewer.controller.FlowController;
import eu.profinit.manta.dataflow.repository.viewer.exception.FlowFormException;
import eu.profinit.manta.dataflow.repository.viewer.exception.NoCommitedDataException;
import eu.profinit.manta.dataflow.repository.viewer.model.Job;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.RefViewUsageStats;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ServerRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ServerResponse;

/**
 * Služba pro spouštění jednotlivých jobů oproti držené databázi.
 * @author tfechtner
 *
 */
public class GraphService {

    @Autowired
    private DatabaseHolder databaseService;

    @Autowired
    private SuperRootHandler superRootHandler;

    @Autowired
    private RevisionRootHandler revisionRootHandler;

    /**
     * Vykonná job transakci a jeho výsledek vrátí.
     * @param job job k vykonnání 
     * @return výsledek obdrženého jobu
     * 
     * @param <Q> typ requestu
     * @param <S> typ response
     */
    public <Q extends ServerRequest, S extends ServerResponse> S executeJob(final Job<Q, S> job) {
        return databaseService.runInTransaction(TransactionLevel.READ, new TransactionCallback<S>() {
            @Override
            public S callMe(TitanTransaction transaction) {
                return job.getCommand().execute(job.getRequest(), job.getFlowState(), transaction);
            }

            @Override
            public String getModuleName() {
                return FlowController.MODULE_NAME;
            }
        });
    }

    /**
     * Získá seznam všech commitnutých revizí seřazených vzestupně dle čísla revize.
     * @throws NoCommitedDataException žádná commitnutá verze
     * @return seznam commitnutých revizí, nikdy null
     */
    public List<RevisionModel> getCommitedRevisionModels() {
        List<RevisionModel> revisionModelList = databaseService
                .runInTransaction(TransactionLevel.READ, new TransactionCallback<List<RevisionModel>>() {
                    @Override
                    public String getModuleName() {
                        return FlowController.MODULE_NAME;
                    }

                    @Override
                    public List<RevisionModel> callMe(TitanTransaction transaction) {
                        List<RevisionModel> modelList = new ArrayList<>();

                        Vertex revisionRoot = revisionRootHandler.getRoot(transaction);
                        List<Vertex> revisionVertices = GraphOperation.getAdjacentVertices(revisionRoot, Direction.OUT,
                                RevisionRootHandler.EVERY_REVISION_INTERVAL, EdgeLabel.HAS_REVISION);
                        for (Vertex vertex : revisionVertices) {
                            RevisionModel model = new RevisionModel(vertex);
                            if (model.isCommitted()) {
                                modelList.add(model);
                            }
                        }

                        Collections.sort(modelList);

                        return modelList;
                    }
                });

        if (!revisionModelList.isEmpty()) {
            return revisionModelList;
        } else {
            throw new NoCommitedDataException();
        }
    }

    /**
     * Získá model pro specifickou revizi.
     * @param revision číslo revize, která se má najít
     * @return model pro specifickou revizi, nebo null pokud neexistuje
     */
    public RevisionModel getSpecificRevisionModel(final double revision) {
        RevisionModel revisionModel = databaseService.runInTransaction(TransactionLevel.READ,
                new TransactionCallback<RevisionModel>() {
            @Override
            public String getModuleName() {
                return FlowController.MODULE_NAME;
            }

            @Override
            public RevisionModel callMe(TitanTransaction transaction) {
                return revisionRootHandler.getSpecificModel(transaction, revision);
            }
        });

        if (revisionModel != null) {
            if (revisionModel.isCommitted()) {
                return revisionModel;
            } else {
                throw new FlowFormException("The revision " + revision + " is not commited yet.");
            }
        } else {
            throw new FlowFormException("The revision " + revision + " does not exist.");
        }
    }

    /**
     * Naplní dané typy uzlů a resourců do objektu udržující statistiky použití. 
     * @param vertexIdList seznam id uzlů, které se mají uložit
     * @param refViewUsageStats objekt držicí statistiky, kam se plní data
     */
    public void fillStartNodeTypes(final List<Long> vertexIdList, final RefViewUsageStats refViewUsageStats) {
        databaseService.runInTransaction(TransactionLevel.READ, new TransactionCallback<Object>() {
            @Override
            public String getModuleName() {
                return FlowController.MODULE_NAME;
            }

            @Override
            public Object callMe(TitanTransaction transaction) {
                refViewUsageStats.setStartNodeMap(GraphOperation.getNodeTypeCounts(transaction, vertexIdList));
                return null;
            }
        });
    }

    /**
     * @return id kořene stromu v databázi
     */
    public Long getRootId() {
        return superRootHandler.getRoot(databaseService);
    }

    /**
     * @param databaseService service pro přístupu k db
     */
    public void setDatabaseService(DatabaseHolder databaseService) {
        this.databaseService = databaseService;
    }

    /**
     * @param superRootHandler držák na super root grafu
     */
    public void setSuperRootHandler(SuperRootHandler superRootHandler) {
        this.superRootHandler = superRootHandler;
    }
}
