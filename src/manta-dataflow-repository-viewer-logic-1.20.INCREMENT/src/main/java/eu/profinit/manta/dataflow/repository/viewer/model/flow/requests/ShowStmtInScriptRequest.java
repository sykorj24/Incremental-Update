package eu.profinit.manta.dataflow.repository.viewer.model.flow.requests;

/**
 * Request pro zobrazení příkazu v příslušném skriptu.
 * @author tfechtner
 *
 */
public class ShowStmtInScriptRequest implements ServerRequest {
    /** Id komponenty, která se má zobrazit. */
    private Long componentId;
    /** Session pro získání příslušného flowState. */
    private Integer sessionIndex;

    /**
     * @return Id komponenty, která se má zobrazit. 
     */
    public Long getComponentId() {
        return componentId;
    }

    /**
     * @param componentId Id komponenty, která se má zobrazit. 
     */
    public void setComponentId(Long componentId) {
        this.componentId = componentId;
    }

    /**
     * @return index session pro získání příslušného flowState
     */
    public Integer getSessionIndex() {
        return sessionIndex;
    }

    /**
     * @param sessionIndex index session pro získání příslušného flowState
     */
    public void setSessionIndex(Integer sessionIndex) {
        this.sessionIndex = sessionIndex;
    }

}
