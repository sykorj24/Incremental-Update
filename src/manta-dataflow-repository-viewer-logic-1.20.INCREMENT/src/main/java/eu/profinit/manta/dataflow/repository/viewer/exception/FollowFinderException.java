package eu.profinit.manta.dataflow.repository.viewer.exception;

/**
 * Výjimka informující o příliš velkém follow.
 * @author tfechtner
 *
 */
public class FollowFinderException extends RuntimeException {

    private static final long serialVersionUID = -7890599941655396117L;

    /**
     * @param message zpráva výjimky
     * @param cause důvod
     */
    public FollowFinderException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message zpráva výjimky
     */
    public FollowFinderException(String message) {
        super(message);
    }

}
