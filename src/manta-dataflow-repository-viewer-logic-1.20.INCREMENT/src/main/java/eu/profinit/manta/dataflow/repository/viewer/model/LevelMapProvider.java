package eu.profinit.manta.dataflow.repository.viewer.model;

import java.util.Map;
import java.util.Set;

/**
 * Poskytovtel konfigurace urovne uzlu - z datoveho zdroje nacte nastaveni
 * urovni pro jednotlive technologie uzlu a k nim potom priradi jejich 
 * chovani definovane v {@link TypeInfo}. 
 * 
 * @see eu.profinit.manta.dataflow.repository.viewer.model.TypeInfo
 * 
 * @author Matyas Krutsky <matyas.krutsky@profinit.eu>
 */
public interface LevelMapProvider {

    /**
     * @return mapa konfigurace urovni ve formatu
     * <pre>
     *     ["technologie:typ" -> chovani urovne ({@link TypeInfo})]     
     * </pre>
     */
    Map<String, TypeInfo> getLevelsMap();
    
    /**
     * Vrátí {@link TypeInfo} pro danou dvojici technologie a typu.
     * @param technology název technologie
     * @param type název typu
     * @return typ info pro danou dvojici nebo null
     */
    TypeInfo getTypeInfo(String technology, String type);
    
    /**
     * Pro zadanou technologii vrátí seznam grouptype ze souboru levels.xml.
     * @param technology název technologie
     * @return seznam grouptype pro danou technologii, nebo <code>null</code>, pokud není definován
     */
    Set<String> getGroupType(String technology);
    
}
