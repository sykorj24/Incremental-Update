package eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.MappingNode;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.service.FilteredRelationshipService;
import eu.profinit.manta.dataflow.repository.viewer.service.ViewerOperation;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.ShowStmtInScriptCommand;

/**
 * Datová immutable třída pro reprezentaci kompletního uzlu pro klienta.
 * @author tfechtner
 *
 */
public final class FullComponent extends Component {
    /** Název/zobrazovaný label uzlu. */
    private final String label;
    /** Vrstva, do ktere spada uzel */
    private final String layer;
    /** Jestli je uzel atomický = nemá potomky. */
    private final boolean atomic;
    /** Typ uzlu - informace z parseru. */
    private final String type;
    /** Příznak, jestliže má být uzel rozbalen. */
    private final boolean expanded;
    /** Mapa atributů uzlu. */
    private final Map<String, List<AttributeWithRevState>> attributes;
    /** Cesta z předků k uzlu. */
    private final List<String> path;
    /** Typ technologie. */
    private final String techType;
    /** True, jestliže má source code.*/
    private final boolean hasSource;
    /**
     * Mapovani na uzly ve vrstvach.
     * Klicem je nazev vrstvy, hodnota je seznam uzlu mapovanych timto uzlem
     * ci mapujicich tento uzel v prislusne vrstve.
     */
    private final Map<String, List<MappingNode>> nodeMapping;

    /**
     * @param transaction transakce pro pístup k db
     * @param vertex reprezentace uzlu z databáze
     * @param flowState stav daného flow
     * @param levelMapProvider provider pro získání definice úrovně a atomicity uzlu
     * @param isExpanded true, jestliže má být uzel rozbalen
     * @param relationshipService služba pro práci s předky 
     */
    /*package*/ FullComponent(TitanTransaction transaction, Vertex vertex, FlowStateViewer flowState,
            LevelMapProvider levelMapProvider, boolean isExpanded, FilteredRelationshipService relationshipService) {
        super(vertex, false, relationshipService, flowState, levelMapProvider);

        this.label = ViewerOperation.getLabel(vertex);
        this.layer = ViewerOperation.getLayer(vertex);
        this.type = ViewerOperation.getType(vertex);
        this.atomic = relationshipService.isAtomic(vertex, levelMapProvider, flowState.getRevisionInterval());
        this.techType = ViewerOperation.getTechnologyType(vertex);
        this.expanded = isExpanded;
        this.attributes = getAllNodeAttributesInRev(transaction, vertex, flowState);
        this.path = ViewerOperation.getPath(vertex);
        this.hasSource = findIfHasSource();
        this.nodeMapping = ViewerOperation.getNodeMapping(vertex, flowState.getRevisionInterval());

        RevisionState revState = RevisionState.resolve(vertex, flowState.getRevisionInterval());
        // druhá část podmínky znamená, že nemá ekvivalentní vrchol, tzn nemá platnost v obou částech revize
        if (revState != RevisionState.STABLE
                && flowState.getEquivalencesHolder().getEquivalentVertexId((Long) vertex.getId()) == null) {
            this.attributes.put("revState", Collections
                    .<AttributeWithRevState> singletonList(new AttributeWithRevState(RevisionState.STABLE, revState)));
        }
    }

    private boolean findIfHasSource() {
        return attributes.containsKey(ShowStmtInScriptCommand.SOURCE_LOCATION)
                && attributes.containsKey(ShowStmtInScriptCommand.SOURCE_ENCODING);
    }

    /**
     * Nalezne všechny atributy pro daný uzel v daném intervalu.
     * Atributy jsou obohaceny o jejich stav v dané revizi.
     * @param transaction transakce pro pístup k db 
     * @param vertex vrchol, pro který se hledají atributy
     * @param flowState stav daného flow
     * @return mapa atributů, nikdy null
     */
    private Map<String, List<AttributeWithRevState>> getAllNodeAttributesInRev(TitanTransaction transaction,
            Vertex vertex, FlowStateViewer flowState) {
        Map<String, List<AttributeWithRevState>> attributes = new HashMap<>();
        fetchAttributes(vertex, attributes, flowState.getRevisionInterval());

        if (flowState.getRevisionInterval().isComparing()) {
            // načíst atributy pro ekvivalentní vrchol
            Long equivalentVertexId = flowState.getEquivalencesHolder().getEquivalentVertexId((Long) vertex.getId());
            if (equivalentVertexId != null) {
                Vertex equivalentVertex = transaction.getVertex(equivalentVertexId);
                fetchAttributes(equivalentVertex, attributes, flowState.getRevisionInterval());
            }

            Map<String, List<AttributeWithRevState>> groupedAttributes = groupAttributes(attributes);
            return groupedAttributes;
        } else {
            return attributes;
        }
    }

    /**
     * Získá atributy pro daný vrchol.
     * @param vertex vrchol, pro který se hledají atributy
     * @param attributes vstupně/výstupní mapa, kam se plní atributy
     * @param revInterval interval revizí, v kterém se hledají revize
     */
    private void fetchAttributes(Vertex vertex, Map<String, List<AttributeWithRevState>> attributes,
            RevisionInterval revInterval) {
        Iterable<Edge> attributeEdges = RevisionUtils.getAdjacentEdges(vertex, Direction.OUT, revInterval,
                EdgeLabel.HAS_ATTRIBUTE.t());
        for (Edge attributeEdge : attributeEdges) {
            Vertex attribute = attributeEdge.getVertex(Direction.IN);
            String attributeName = attribute.getProperty(NodeProperty.ATTRIBUTE_NAME.t()).toString();
            Object attributeValue = attribute.getProperty(NodeProperty.ATTRIBUTE_VALUE.t());
            RevisionState revState = RevisionState.resolve(attributeEdge, revInterval);
            AttributeWithRevState valueWithState = new AttributeWithRevState(revState, attributeValue);

            List<AttributeWithRevState> savedAttributes = attributes.get(attributeName);
            if (savedAttributes == null) {
                savedAttributes = new ArrayList<AttributeWithRevState>();
                attributes.put(attributeName, savedAttributes);
            }

            savedAttributes.add(valueWithState);
        }
    }

    /**
     * Seskupí platnost hodnot atributů.
     * @param attributes původní mapa atributů
     * @return seskupená mapa atributů
     */
    private Map<String, List<AttributeWithRevState>> groupAttributes(
            Map<String, List<AttributeWithRevState>> attributes) {
        Map<String, List<AttributeWithRevState>> groupedAttributes = new HashMap<>();
        // pro všechny klíče atributů
        for (Entry<String, List<AttributeWithRevState>> entry : attributes.entrySet()) {

            // spojit atributy s prerusenou platnosti a vyhazet inner atributy
            Map<Object, AttributeWithRevState> mappedValues = new HashMap<>();
            for (AttributeWithRevState attr : entry.getValue()) {
                if (attr.getRevState() == RevisionState.INNER) {
                    // inner atributy vůbec nezobrazovat
                    continue;
                }

                AttributeWithRevState savedValue = mappedValues.get(attr.getValue());
                if (savedValue != null) {
                    // už nějaký exisutje -> spojit platnost
                    mappedValues.put(attr.getValue(), new AttributeWithRevState(RevisionState.STABLE, attr.getValue()));
                } else {
                    // neexistuje -> jen přidat
                    mappedValues.put(attr.getValue(), attr);
                }
            }

            groupedAttributes.put(entry.getKey(), new ArrayList<>(mappedValues.values()));
        }
        return groupedAttributes;
    }

    /**
     * @param builder builder třídy s příslušnými hodnotami
     */
    private FullComponent(FullComponentBuilder builder) {
        super(builder.id, builder.proxy, builder.parentID, builder.status, builder.tech, builder.filtered,
                builder.topParentTech, builder.level);
        this.label = builder.label;
        this.layer = builder.layer;
        this.atomic = builder.atomic;
        this.type = builder.type;
        this.expanded = builder.expanded;
        this.attributes = builder.attributes;
        this.path = builder.path;
        this.techType = builder.techType;
        this.hasSource = builder.hasSource;
        this.nodeMapping = builder.nodeMapping;
    }

    /**
     * Builder pro ruční nsatavení všech prvků FullComponent. 
     * @author tfechtner
     *
     */
    public static class FullComponentBuilder {
        /** Id uzlu. */
        private Object id;
        /** Přepínáš, jestliže jde o proxy objekt. */
        private boolean proxy;
        /** Id předka.*/
        private Object parentID;
        /** Aktuální status uzlu v rámci view. */
        private NodeStatus status;
        /** Technologie neboli resource uzlu. */
        private String tech;
        /** True, jestliže je daný uzel vyfiltrovaný. */
        private boolean filtered;
        /** Id top parent resource. */
        private Object topParentTech;
        /** Název/zobrazovaný label uzlu. */
        private String label;
        /** Vrstva, do ktere spada staveny uzel */
        private String layer;
        /** Jestli je uzel atomický = nemá potomky. */
        private boolean atomic;
        /** Typ uzlu - informace z parseru. */
        private String type;
        /** Příznak, jestliže má být uzel rozbalen. */
        private boolean expanded;
        /** Mapa atributů uzlu. */
        private Map<String, List<AttributeWithRevState>> attributes;
        /** Úroveň uzlu ve flow. */
        private int level;
        /** Cesta z předků k uzlu. */
        private List<String> path;
        /** Typ technologie. */
        private String techType;
        /** True, jestliže má source code.*/
        private boolean hasSource;
        /**
         * Mapovani na uzly ve vrstvach.
         * Klicem je nazev vrstvy, hodnota je seznam uzlu mapovanych stavenym uzlem
         * ci mapujicich staveny uzel v prislusne vrstve.
         */
        private Map<String, List<MappingNode>> nodeMapping;

        /**
         * Nastaví všechny atributy z obdržené komponenty.
         * @param component komponenta ke zkopírování
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder setFromComponent(FullComponent component) {
            this.id(component.getId()).proxy(component.isProxy()).parentID(component.getParentID())
                    .status(component.getStatus()).tech(component.getTech()).filtered(component.isFiltered())
                    .topParentTech(component.getTopParentTech()).label(component.getLabel())
                    .atomic(component.isAtomic()).type(component.getType()).expanded(component.isExpanded())
                    .attributes(component.getAttributes()).level(component.getLevel()).path(component.getPath())
                    .techType(component.getTechType()).hasSource(component.getHasSource()).nodeMapping(component.getNodeMapping());

            return this;
        }

        /**
         * @param idParam Id uzlu. 
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder id(Object idParam) {
            this.id = idParam;
            return this;
        }

        /**
         * @param proxyParam Přepínáč, jestliže jde o proxy objekt.
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder proxy(boolean proxyParam) {
            this.proxy = proxyParam;
            return this;
        }

        /**
         * @param parentIDParam Id předka.
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder parentID(Object parentIDParam) {
            this.parentID = parentIDParam;
            return this;
        }

        /**
         * @param statusParam Aktuální status uzlu v rámci view. 
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder status(NodeStatus statusParam) {
            this.status = statusParam;
            return this;
        }

        /**
         * @param techParam Technologie neboli resource uzlu. 
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder tech(String techParam) {
            this.tech = techParam;
            return this;
        }

        /**
         * @param filteredParam True, jestliže je daný uzel vyfiltrovaný. 
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder filtered(boolean filteredParam) {
            this.filtered = filteredParam;
            return this;
        }

        /**
         * @param topParentTechParam Id top parent resource. 
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder topParentTech(Object topParentTechParam) {
            this.topParentTech = topParentTechParam;
            return this;
        }

        /**
         * @param labelParam Název/zobrazovaný label uzlu. 
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder label(String labelParam) {
            this.label = labelParam;
            return this;
        }

        /**
         * @param layerParam Vrstva, do ktere spada staveny uzel.
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder layer(String layerParam) {
            this.layer = layerParam;
            return this;
        }

        /**
         * @param atomicParam Jestli je uzel atomický = nemá potomky. 
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder atomic(boolean atomicParam) {
            this.atomic = atomicParam;
            return this;
        }

        /**
         * @param typeParam Typ uzlu - informace z parseru. 
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder type(String typeParam) {
            this.type = typeParam;
            return this;
        }

        /**
         * @param expandedParam Příznak, jestliže má být uzel rozbalen. 
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder expanded(boolean expandedParam) {
            this.expanded = expandedParam;
            return this;
        }

        /**
         * @param attributesParam Mapa atributů uzlu. 
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder attributes(Map<String, List<AttributeWithRevState>> attributesParam) {
            this.attributes = attributesParam;
            return this;
        }

        /**
         * @param levelParam Úroveň uzlu ve flow.
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder level(int levelParam) {
            this.level = levelParam;
            return this;
        }

        /**
         * @param pathParam Cesta z předků k uzlu. 
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder path(List<String> pathParam) {
            this.path = pathParam;
            return this;
        }

        /**
         * @param techTypeParam Typ technologie. 
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder techType(String techTypeParam) {
            this.techType = techTypeParam;
            return this;
        }

        /**
         * @param hasSource True, jestliže má source code
         * @return instance urpaveného builderu
         */
        public FullComponentBuilder hasSource(boolean hasSource) {
            this.hasSource = hasSource;
            return this;
        }

        /**
         * @param nodeMappingParam Mapovani na uzly ve vrstvach.
         *                         Klicem je nazev vrstvy, hodnota je seznam uzlu mapovanych stavenym uzlem
         *                         ci mapujicich staveny uzel v prislusne vrstve.
         * @return instance urpaveneho builderu
         */
        public FullComponentBuilder nodeMapping(Map<String, List<MappingNode>> nodeMappingParam) {
            this.nodeMapping = nodeMappingParam; 
            return this;
        }

        /**
         * @return vytvoří instanci fullcomponent
         */
        public FullComponent build() {
            return new FullComponent(this);
        }
    }

    /**
     * Hodnota atributu obohacená o informaci o stavu v revizi.
     * @author tfechtner
     *
     */
    public static final class AttributeWithRevState {
        /** Stav v revizi. */
        private final RevisionState revState;
        /** Hodnota atributu. */
        private final Object value;

        /**
         * @param revState Stav v revizi
         * @param value Hodnota atributu
         */
        public AttributeWithRevState(RevisionState revState, Object value) {
            super();
            this.revState = revState;
            this.value = value;
        }

        /**
         * @return Stav v revizi
         */
        public RevisionState getRevState() {
            return revState;
        }

        /**
         * @return Hodnota atributu
         */
        public Object getValue() {
            return value;
        }

        @Override
        public String toString() {
            return revState + ":" + value;
        }

        @Override
        public int hashCode() {
            HashCodeBuilder builder = new HashCodeBuilder();
            builder.append(revState);
            builder.append(value);
            return builder.toHashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (obj.getClass() != getClass()) {
                return false;
            }
            FullComponent.AttributeWithRevState other = (FullComponent.AttributeWithRevState) obj;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(revState, other.revState);
            builder.append(value, other.value);
            return builder.isEquals();
        }
    }

    /**
     * @return Název/zobrazovaný label uzlu.
     */
    public String getLabel() {
        return label;
    }

    /**
     * @return Vrstva, do ktere spada uzel
     */
    public String getLayer() {
        return layer;
    }

    /**
     * @return Jestli je uzel atomický = nemá potomky.
     */
    public boolean isAtomic() {
        return atomic;
    }

    /**
     * @return Typ uzlu - informace z parseru.
     */
    public String getType() {
        return type;
    }

    /**
     * @return true, jestliže má být uzel rozbalen
     */
    public boolean isExpanded() {
        return expanded;
    }

    /**
     * @return mapa všech atribut uzlů
     */
    public Map<String, List<AttributeWithRevState>> getAttributes() {
        return attributes;
    }

    /**
     * @return cesta k uzlu
     */
    public List<String> getPath() {
        return path;
    }

    /**
     * @return Typ technologie.
     */
    public String getTechType() {
        return techType;
    }

    /**
     * @return true, jestliže má source code
     */
    public boolean getHasSource() {
        return hasSource;
    }

    /**
     * @return Mapovani na uzly ve vrstvach.
     *         Klicem je nazev vrstvy, hodnota je seznam uzlu mapovanych stavenym uzlem
     *         ci mapujicich staveny uzel v prislusne vrstve.
     */
    public Map<String, List<MappingNode>> getNodeMapping() {
        return nodeMapping;
    }

    @Override
    public String toString() {
        return label + "[" + getId() + "]" + "(P:" + getParentID() + ", S:" + getStatus() + ", A:" + atomic + ", R:"
                + getTech() + ", T:" + type + ", E:" + expanded + ")";
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FullComponent other = (FullComponent) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }
}
