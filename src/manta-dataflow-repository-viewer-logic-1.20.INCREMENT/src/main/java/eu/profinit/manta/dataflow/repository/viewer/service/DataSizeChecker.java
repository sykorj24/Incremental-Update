package eu.profinit.manta.dataflow.repository.viewer.service;

/**
 * Třída kontrolující velikost odpovědi. 
 * @author tfechtner
 *
 */
public final class DataSizeChecker {
    /** Koeficient pro veliksot hrany. */
    private double edgeCoeficient = 1;
    /** Koeficient pro velikost proxy komponenty. */
    private double proxyCoeficient = 1;
    /** Koeficient pro velikost full komponenty. */
    private double fullCoeficient = 1;
    /** Maximální velikost odpovědi. */
    private long maxSize;
    /** Maximání počet startovních uzlů pro referenční view. */
    private int maximumStartNodes;
    /** Maximání počet startovních uzlů pro referenční view pro přímý export. */
    private int maximumStartNodesForExport;
    /** Maximální počet agregovaných hran v jednom směru. */
    private int maximumAggEdges;

    /**
     * Ověří jestli není příliš mnoho dat. 
     * @param edgeCount počet hran
     * @param proxyCount počet proxy komponent
     * @param fullCount počet full komponent 
     * @return true, jestliže je přiliš mnoho dat
     */
    public boolean isDataTooLarge(int edgeCount, int proxyCount, int fullCount) {
        long size = Math.round(edgeCount * edgeCoeficient + proxyCount * proxyCoeficient + fullCount * fullCoeficient);
        return size > maxSize;
    }
    
    /**
     * Ověří jestli daný počet uzlů nepřekročil maximum pro follow.
     * @param nodes počet uzlů, které se ověřuje
     * @return true, jestliže je toto množství příliš velké
     */
    public boolean isTooManyFollowedNodes(int nodes) {
        long size = Math.round(nodes * proxyCoeficient);
        return size > maxSize;
    }

    /**
     * @param edgeCoeficient Koeficient pro veliksot hrany.
     */
    public void setEdgeCoeficient(double edgeCoeficient) {
        this.edgeCoeficient = edgeCoeficient;
    }

    /**
     * @param proxyCoeficient Koeficient pro velikost proxy komponenty.
     */
    public void setProxyCoeficient(double proxyCoeficient) {
        this.proxyCoeficient = proxyCoeficient;
    }

    /**
     * @param fullCoeficient Koeficient pro velikost full komponenty.
     */
    public void setFullCoeficient(double fullCoeficient) {
        this.fullCoeficient = fullCoeficient;
    }

    /**
     * @param maxSize Maximální velikost odpovědi.
     */
    public void setMaxSize(long maxSize) {
        this.maxSize = maxSize;
    }

    /**
     * @return maximání počet startovních uzlů pro referenční view
     */
    public int getMaximumStartNodes() {
        return maximumStartNodes;
    }

    /**
     * @param maximumStartNodes maximání počet startovních uzlů pro referenční view
     */
    public void setMaximumStartNodes(int maximumStartNodes) {
        this.maximumStartNodes = maximumStartNodes;
    }

    /**
     * @return Maximální počet agregovaných hran v jednom směru.
     */
    public int getMaximumAggEdges() {
        return maximumAggEdges;
    }

    /**
     * @param maximumAggEdges Maximální počet agregovaných hran v jednom směru.
     */
    public void setMaximumAggEdges(int maximumAggEdges) {
        this.maximumAggEdges = maximumAggEdges;
    }

    /** 
     * @return Maximání počet startovních uzlů pro referenční view pro přímý export. 
     */
    public int getMaximumStartNodesForExport() {
        return maximumStartNodesForExport;
    }

    /**
     * @param maximumStartNodesForExport Maximání počet startovních uzlů pro referenční view pro přímý export. 
     */
    public void setMaximumStartNodesForExport(int maximumStartNodesForExport) {
        this.maximumStartNodesForExport = maximumStartNodesForExport;
    }
}
