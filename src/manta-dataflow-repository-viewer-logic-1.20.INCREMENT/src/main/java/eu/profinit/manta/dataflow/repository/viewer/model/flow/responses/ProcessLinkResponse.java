package eu.profinit.manta.dataflow.repository.viewer.model.flow.responses;

import eu.profinit.manta.dataflow.repository.viewer.model.index.WelcomeFormModel;

/**
 * Odpověď se zpracovaným modelem z permalinku na form model.
 * @author tfechtner
 *
 */
public class ProcessLinkResponse extends AbstractServerResponse {

    private WelcomeFormModel formModel;

    /**
     * Default konstruktor předávající předkovi název commandu.
     */
    public ProcessLinkResponse() {
        super("process-link");
    }

    /**
     * @return form model odpovídající datům z welcome stránky
     */
    public WelcomeFormModel getFormModel() {
        return formModel;
    }

    /**
     * @param formModel model odpovídající datům z welcome stránky
     */
    public void setFormModel(WelcomeFormModel formModel) {
        this.formModel = formModel;
    }

}
