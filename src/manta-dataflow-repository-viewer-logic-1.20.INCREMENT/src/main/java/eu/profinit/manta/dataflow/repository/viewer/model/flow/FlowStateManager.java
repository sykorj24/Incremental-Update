package eu.profinit.manta.dataflow.repository.viewer.model.flow;

import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Sravce flow state stavu, ktery udrzuje frontu vsech ulozenych flow state a
 * take aktualni flow state. Flow state manager je specificky pro kazdou session
 * (pro kazdou klientskou konverzaci).
 *
 * <p>Trida neni thread-safe.</p>
 */
public class FlowStateManager {

    /** maximalni kapacita fronty stavu. */
    private final int capacity;

    /** ulozene stavy, identifikovane pomoci timestamp v poradi od nejstarsiho po nejnovejsi. */
    private final SortedMap<Long, FlowStateViewer> states = new TreeMap<>();

    /** oznaceni (timestamp) aktualniho flow state. */
    private long cursor;

    /**
     * @param capacity maximalni kapacita fronty stavu.
     */
    public FlowStateManager(int capacity) {
        this.capacity = capacity;
    }

    /**
     * Smaze vsechny stavy, ktere jsou novejsi nez aktualne nastaveny stav (=redo stavy).
     * Vlozi novy stav do fronty pod danym oznacenim. Pokud je presazena maximalni kapacita
     * stavu odmaze nejstarsi stav a nastavi vlozeny stav jako aktualni.
     * @param key oznaceni stavu (timestamp), pod kterym se vlozi d fronty stavu
     * @param state stav, ktery se ma ulozit
     */
    public void pushFlowState(long key, FlowStateViewer state) {
        Set<Long> tailSet = states.tailMap(cursor).keySet();
        Long[] tail = tailSet.toArray(new Long[tailSet.size()]);

        // smazu vsechny redo stavy (stavy za aktualnim kurzorem)
        for (long tailKey : tail) {
            if (tailKey != cursor) {
                states.remove(tailKey);
            }
        }

        // vlozim novy stav
        states.put(key, new FlowStateViewer(state));

        // zkontroluju velikost fronty
        while (states.size() > capacity) {
            states.remove(states.firstKey());
        }

        // nastavim novy stav jako aktualni
        cursor = key;
    }

    /**
     * @return oznaceni (timestamp) aktualne nastaveneho stavu
     */
    public long getCursor() {
        return cursor;
    }

    /**
     * Vrati flow state pro zadane oznaceni (timestamp) a zaroven ho rovnou
     * nastavi jako aktualni.
     * @param key oznaceni pozadovaneho stavu
     * @return pozadovany flow state nebo {@code null}, pokud stav neexistuje
     */
    public FlowStateViewer getFlowStateAndSetCursor(Long key) {
        this.cursor = key;
        return getCurrentFlowState();
    }

    /**
     * @param key oznaceni pozadovaneho stavu
     * @return {@code true} pokud exiszuje ulozeny stav pod danym oznacenim, jinak {@code false}
     */
    public boolean isValidFlowState(Long key) {
        return states.containsKey(key);
    }

    /**
     * @return aktualne nastaveny flow state
     */
    public FlowStateViewer getCurrentFlowState() {
        return states.get(cursor);
    }

    /**
     * @return {@code true}, pokud manager ma aktualne nastaevny validni flow state, jiank {@code false}
     */
    public boolean hasValidCurrentState() {
        return states.containsKey(cursor);
    }
}
