package eu.profinit.manta.dataflow.repository.viewer.model.index;

import org.apache.commons.lang3.ArrayUtils;

/**
 * Model primeho vyberu elementu.
 * 
 * @author onouza
 */
public class DirectSelectionModel {

    /**
     * Mozne oddelovace polozek v ceste k elementu 
     */
    private final PathSeparator[] pathSeparators = PathSeparator.values();
    /**
     * Vybrany oddelovac polozek v ceste k elementu 
     */
    private PathSeparator selectedPathSeparator;
    
    // gettery / settery
    
    /**
     * @return Mozne oddelovace polozek v ceste k elementu
     */
    public PathSeparator[] getPathSeparators() {
        return ArrayUtils.clone(pathSeparators);
    }
    
    /**
     * @return Vybrany oddelovac polozek v ceste k elementu
     */
    public PathSeparator getSelectedPathSeparator() {
        return selectedPathSeparator;
    }

    /**
     * @param selectedPathSeparator Vybrany oddelovac polozek v ceste k elementu
     */
    public void setSelectedPathSeparator(PathSeparator selectedPathSeparator) {
        this.selectedPathSeparator = selectedPathSeparator;
    }
}
