package eu.profinit.manta.dataflow.repository.viewer.service.filter;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.xml.parsers.SAXParser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.viewer.model.filter.vertical.VerticalFilterProvider;
import eu.profinit.manta.dataflow.repository.viewer.service.ViewerOperation;

/**
 * Třída pro poskytování vertikálních filtrů načítaných z xml konfigurace.
 * @author tfechtner
 *
 */
public class XmlVerticalFilterProvider extends AbstractXmlLoader implements VerticalFilterProvider {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlVerticalFilterProvider.class);
    /** Oddělovač tehnologie a jména elementu. */
    private static final String DELIMITER = ":";
    /** Mapa filtrovaných elementů a módu jejich filtrace. */
    private Map<String, DisabledMode> disabledElements;

    /**
     * Enum určující, na kterých pozicích je daný element filtrován. 
     * @author tfechtner
     *
     */
    public enum DisabledMode {
        /** Filtrovat všechny elementy daného typu. */
        ALL("all") {
            @Override
            public boolean isDisabled(VertexPosition position) {
                return true;
            }
        },
        /** Nefiltrovat první výskyt elementu, tedy ten nejnižší v hierarchii. */
        WITHOUT_BOTTOM("withoutBottom") {
            @Override
            public boolean isDisabled(VertexPosition position) {
                return position != VertexPosition.FIRST && position != VertexPosition.ONLY_ONE;
            }
        },
        /** Nefiltrovat poslední výskyt elementu, tedy ten nejvyšší v hierarchii. */
        WITHOUT_TOP("withoutTop") {
            @Override
            public boolean isDisabled(VertexPosition position) {
                return position != VertexPosition.LAST && position != VertexPosition.ONLY_ONE;
            }
        };

        /** Textová reprezentace enumu.  */
        private final String string;

        /**
         * @param string Textová reprezentace enumu.
         */
        private DisabledMode(String string) {
            this.string = string;
        }

        /**
         * Vrátí enum odpovídající danému texu. 
         * @param text text k naparsování
         * @return enum odpovídající textu
         * @throw IllegalArgumentException textu neodpovídá žádný enum
         */
        public static DisabledMode parseFromString(String text) {
            for (DisabledMode d : values()) {
                if (d.string.equals(text)) {
                    return d;
                }
            }
            throw new IllegalArgumentException("Unknown string value of enum: " + text + ".");
        }

        /**
         * Ověří jestli je daná pozice filtrovaná.
         * @param position pozice elementu ve stromě
         * @return true, jestliže je daná pozice filtrovaná 
         */
        public abstract boolean isDisabled(VertexPosition position);
    }

    /**
     * Pozice elementu v sekvenci daných elementů.
     * @author tfechtner
     *
     */
    public enum VertexPosition {
        /** První v řadě, tedy ten nejníž v hierarchii. */
        FIRST,
        /** Poslední v řadě, tedy ten nejvýš v hierarchii. */
        LAST,
        /** Mezi {@link #FIRST} a {@link #LAST}. */
        MIDDLE,
        /** Pokud je prvek daného typu jediný. */
        ONLY_ONE;
    }

    /**
     * Provede inicizalizaci třídy.
     */
    @PostConstruct
    public void init() {
        disabledElements = extractDataFromXml();
        if (disabledElements == null) {
            disabledElements = Collections.emptyMap();
        }
    }

    @Override
    public boolean isElementDisabled(String resourceType, String elementType, VertexPosition position) {
        DisabledMode mode = disabledElements.get(resourceType + DELIMITER + elementType);
        return mode == null ? false : mode.isDisabled(position);
    }

    @Override
    public boolean isElementDisabled(Vertex actualVertex, Vertex startNode) {
        VertexPosition position = findPosition(actualVertex, startNode);
        String stringType = ViewerOperation.getType(actualVertex);
        String technology = ViewerOperation.getTechnologyType(actualVertex);
        return isElementDisabled(technology, stringType, position);
    }

    /**
     * Určí pozici vrcholu v rámci sekvence vrcholů stejného typu vůči startovnímu vrcholu.
     * @param actualVertex určovaný vrchol
     * @param startVertex startovní vrchol
     * @return pozice určovaného vrcholu, nikdy null
     */
    protected final VertexPosition findPosition(Vertex actualVertex, Vertex startVertex) {
        if (actualVertex == null) {
            throw new IllegalArgumentException("The argument actual vertex must not be null.");
        }
        if (startVertex == null) {
            throw new IllegalArgumentException("The argument start vertex must not be null.");
        }
        Vertex parent = GraphOperation.getParent(actualVertex);
        String actualType = ViewerOperation.getType(actualVertex);
        boolean isParentSameType = parent != null && ViewerOperation.getType(parent).equals(actualType);

        if (isParentSameType) {
            if (actualVertex.getId().equals(startVertex.getId())) {
                return VertexPosition.FIRST;
            } else {
                if (isSameTypeInSeq(actualVertex, startVertex)) {
                    return VertexPosition.MIDDLE;
                } else {
                    return VertexPosition.FIRST;
                }
            }
        } else {
            if (actualVertex.getId().equals(startVertex.getId())) {
                return VertexPosition.ONLY_ONE;
            } else {
                if (isSameTypeInSeq(actualVertex, startVertex)) {
                    return VertexPosition.LAST;
                } else {
                    return VertexPosition.ONLY_ONE;
                }
            }
        }
    }

    /**
     * Zjistí jestli mezi startovnám a koncovým uzlem je uzel stejného typu jako koncový.
     * Přičemž sekvence těchto typů MUSÍ být s koncovým uzlem nepřerušená. <br>
     * Jinými slovy, jestli je několik uzlů stejného typu přesně za sebou a mezi ně patří i 
     * koncový uzel.
     * @param endParentVertex koncový uzel, jehož typ se zkoumá
     * @param startVertex startovní uzel
     * @return true, jestliže taková sekvence existuje
     */
    protected final boolean isSameTypeInSeq(Vertex endParentVertex, Vertex startVertex) {
        String type = ViewerOperation.getType(endParentVertex);

        boolean isSameType = false;
        Vertex actualVertex = startVertex;
        while (actualVertex != null && !actualVertex.getId().equals(endParentVertex.getId())) {
            isSameType = ViewerOperation.getType(actualVertex).equals(type);
            actualVertex = GraphOperation.getParent(actualVertex);
        }

        // ověřit null, to se může stát, pokud startVertex není potomek endParentVertex
        if (actualVertex != null) {
            return isSameType;
        } else {
            return false;        
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Map<String, DisabledMode> handleParsing(SAXParser saxParser, InputStream stream) throws SAXException,
            IOException {
        FilterHandler handler = new FilterHandler();
        saxParser.parse(stream, handler);
        return handler.getDisabledMap();
    }

    /**
     * Třída pro naparsování souboru s filtry.
     *
     */
    private static class FilterHandler extends DefaultHandler {

        private final Map<String, DisabledMode> disabledMap = new HashMap<>();

        private String resourceName;

        @Override
        public void startElement(String uri, String localName, String qualifiedName, Attributes attributes)
                throws SAXException {

            if ("technology".equals(qualifiedName)) {
                resourceName = attributes.getValue("name");
            } else if ("element".equals(qualifiedName)) {
                String elementName = attributes.getValue("name");
                DisabledMode mode;
                String stringMode = attributes.getValue("mode");
                if (stringMode != null) {
                    mode = DisabledMode.parseFromString(stringMode);
                } else {
                    mode = DisabledMode.ALL;
                }
                if (resourceName != null) {
                    disabledMap.put(resourceName + DELIMITER + elementName, mode);
                } else {
                    LOGGER.warn("Does not exist filter for " + elementName + ".");
                }
            }
        }

        /**
         * @return vrátí načtenou mapu filtru
         */
        public Map<String, DisabledMode> getDisabledMap() {
            return disabledMap;
        }
    }
}
