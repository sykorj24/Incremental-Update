package eu.profinit.manta.dataflow.repository.viewer.model.flow.requests;

import java.util.List;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;

/******************************************************************************
 *   Dotaz pro ziskani okoli uzlu.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FollowRequest extends AbstractServerStateAwareRequest {

    private static final String LOWERING_SIZE_STEP_1 = "The level of detail was decreased to medium.";
    private static final String LOWERING_SIZE_STEP_2 = "The level of detail was decreased to medium "
            + "and the maximum distance was set to one.";
    private static final String LOWERING_SIZE_STEP_3 = "The level of detail was decreased to low "
            + "and the maximum distance was set to one.";
    private static final String LOWERING_SIZE_STEP_4 = "The level of detail was decreased to low "
            + "and the maximum distance was set to zero.";

    private List<Long> componentIds;
    private int radius;
    private FlowLevel level;
    private Set<String> activeFilters;
    private Integer sessionIndex;

    /**
     * @return id objektů, znichž se má spustit follow
     */
    public List<Long> getComponentIds() {
        return componentIds;
    }

    /**
     * @param componentIds id objektů, znichž se má spustit follow
     */
    public void setComponentIds(List<Long> componentIds) {
        this.componentIds = componentIds;
    }

    /**
     * @return Polomer okoli
     */
    public int getRadius() {
        return radius;
    }

    /**
     * @param radius Polomer okoli
     */
    public void setRadius(int radius) {
        this.radius = radius;
    }

    /**
     * @return Pozadovana uroven okoli
     */
    public FlowLevel getLevel() {
        return level;
    }

    /**
     * @param level Pozadovana uroven okoli
     */
    public void setLevel(FlowLevel level) {
        this.level = level;
    }

    /**
     * @return množina id aktivních filtrů
     */
    public Set<String> getActiveFilters() {
        return activeFilters;
    }

    /**
     * @param activeFilters množina id aktivních filtrů
     */
    public void setActiveFilters(Set<String> activeFilters) {
        this.activeFilters = activeFilters;
    }

    /**
     * @return index session pro získání příslušného flowState
     */
    public Integer getSessionIndex() {
        return sessionIndex;
    }

    /**
     * @param sessionIndex index session pro získání příslušného flowState
     */
    public void setSessionIndex(Integer sessionIndex) {
        this.sessionIndex = sessionIndex;
    }

    /**
     * Provede zmenšení rozsahu follow. <br>
     * Kroky změnšování: <br>
     * 1. zmenšení hloubky na middle. <br>
     * 2. zkrácení délky na délku 1 <br>
     * 3. zmenšení hloubky na top. <br>
     * 4. zkrácení délky na 0.
     * @return zpráva popisující provedenou změnu nebo null pokud nedošlo ke změně
     */
    public String lowerSize() {
        String message;

        if (level == FlowLevel.BOTTOM) {
            level = FlowLevel.MIDDLE;
            message = LOWERING_SIZE_STEP_1;
        } else if (radius > 1) {
            radius = 1;
            message = LOWERING_SIZE_STEP_2;
        } else if (level == FlowLevel.MIDDLE) {
            level = FlowLevel.TOP;
            message = LOWERING_SIZE_STEP_3;
        } else if (radius > 0) {
            radius = 0;
            message = LOWERING_SIZE_STEP_4;
        } else {
            message = null;
        }

        return message;
    }

}
