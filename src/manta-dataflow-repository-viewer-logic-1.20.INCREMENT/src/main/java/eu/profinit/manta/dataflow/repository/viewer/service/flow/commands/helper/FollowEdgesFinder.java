package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.DataFlowEdgesFinder;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;
import eu.profinit.manta.dataflow.repository.viewer.exception.FollowFinderException;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.service.DataSizeChecker;
import eu.profinit.manta.dataflow.repository.viewer.service.FilteredRelationshipService;

/**
 * Třída pro hledání follow hran. <br>
 * Oproti svému předkovi {@link DataFlowEdgesFinder} bere v potaz i úroveň vrcholů. 
 * @author tfechtner
 *
 */
public class FollowEdgesFinder extends DataFlowEdgesFinder {
    /** Služba pro práci s hierarchickými vztahy. */
    private final FilteredRelationshipService relationshipService;
    /** Služba pro zjištění úroveň ve flow. */
    private final LevelMapProvider levelMapProvider;
    /** Třída pro kontrolu velikosti odpovědí.  */
    private final DataSizeChecker dataSizeChecker;
    /** Počet navštívených vrcholů v rámci tohoto follow. */
    private int visitedNodes = 0;
    
    /**
     * @param relationshipService Služba pro práci s hierarchickými vztahy.
     * @param levelMapProvider Služba pro zjištění úroveň ve flow.
     * @param transaction Transakce pro práci s db.
     * @param flowState Aktuální stav flow.
     * @param dataSizeChecker struktura pro kontrolu velikosti odpovědi
     * @param technicalAttributes Držák na technické atributy
     */
    public FollowEdgesFinder(FilteredRelationshipService relationshipService, LevelMapProvider levelMapProvider,
            TitanTransaction transaction, FlowStateViewer flowState, DataSizeChecker dataSizeChecker,
            TechnicalAttributesHolder technicalAttributes) {
        super(transaction, flowState, technicalAttributes);
        this.relationshipService = relationshipService;
        this.levelMapProvider = levelMapProvider;
        this.dataSizeChecker = dataSizeChecker;
    }
    
    @Override
    protected FlowStateViewer getFlowState() {
        // toto přetypování je bezpečné, protože jediný konstruktor přijímá FlowStateViewer
        return (FlowStateViewer) super.getFlowState();
    }

    /**
     * Spočte vzdálenost mezi dvěma uzly s ohledem na filtry a <b>úroveň flow a uzlů</b>.
     * @param actualVertex aktuální cílový uzel
     * @param sourceVertex zdrojový uzel
     * @return přímá vzdálenost mezi uzly s ohledem na filtr a úroveň
     */
    @Override
    protected Integer calcDeltaDistance(Vertex actualVertex, Vertex sourceVertex) {
        if (getFlowState().isVertexFiltered(actualVertex)) {
            // vyfiltrovaný uzeů -> nepočítá se
            return 0;
        } else {
            Vertex actualFirstLevelEnough = relationshipService.getFirstVertexHighEnough(actualVertex,
                    getFlowState().getFlowLevel(), levelMapProvider);
            Vertex sourceFirstLevelEnough = relationshipService.getFirstVertexHighEnough(sourceVertex,
                    getFlowState().getFlowLevel(), levelMapProvider);
            // porovnat uzly na dané úrovni
            if (actualFirstLevelEnough.getId().equals(sourceFirstLevelEnough.getId())) {
                return 0;
            } else {
                return 1;
            }
        }
    }

    @Override
    protected void checkLimits(NodeStatus nodeStatus) {
        if (nodeStatus == NodeStatus.UNVISITED || nodeStatus == NodeStatus.VISITED) {
            visitedNodes++;
            
            if (dataSizeChecker.isTooManyFollowedNodes(visitedNodes)) {
                throw new FollowFinderException("Too many visited nodes during follow.");
            }
        }
    }
}
