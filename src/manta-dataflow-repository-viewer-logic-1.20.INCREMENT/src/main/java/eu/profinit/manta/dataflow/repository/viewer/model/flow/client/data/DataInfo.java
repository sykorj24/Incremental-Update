package eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data;

import java.util.Collection;

import eu.profinit.manta.dataflow.repository.viewer.service.DataSizeChecker;

/**
 * Datová immutable třída pro informace o počtu posílaných objektů v odpovědi.
 * @author tfechtner
 *
 */
public final class DataInfo {
    /** Hláška pro případ kdy jsou data moc velká. */
    public static final String ERROR_MESSAGE_TOO_LARGE = "Response is too large.";
    /** Počet hran v odpovědi. */
    private final int edges;
    /** Počet proxy komponent v odpovědi. */
    private final int proxyComponents;
    /** Počet plných komponent v odpovědi. */
    private final int fullComponents;
    /** True, jestliže je odpověď ok. */
    private final boolean isDataOk;
    /** Případná chybová hláška. */
    private final String errorMessage;

    /**
     * @param edges Počet hran v odpovědi.
     * @param proxyComponents Počet proxy komponent v odpovědi.
     * @param fullComponents Počet plných komponent v odpovědi.
     * @param isDataOk True, jestliže je odpověď ok.
     * @param errorMessage Případná chybová hláška.
     */
    public DataInfo(int edges, int proxyComponents, int fullComponents, boolean isDataOk, String errorMessage) {
        super();
        this.edges = edges;
        this.proxyComponents = proxyComponents;
        this.fullComponents = fullComponents;
        this.isDataOk = isDataOk;
        this.errorMessage = errorMessage;
    }

    /**
     * @param components množina komponent v odpovědi
     * @param vizEdges množina hran v odpovědi
     * @param checker třída pro kontrolu velikosti odpovědi
     */
    public DataInfo(Collection<Component> components, Collection<FlowEdgeViz> vizEdges, DataSizeChecker checker) {
        int proxyCount = 0;
        int fullCount = 0;
        if (components != null) {
            for (Component c : components) {
                if (c instanceof FullComponent) {
                    fullCount++;
                } else if (c instanceof ProxyComponent) {
                    proxyCount++;
                } else {
                    throw new IllegalArgumentException("Unknown type of component " + c.getClass() + ".");
                }
            }
        }

        int edgeCount = 0;
        if (vizEdges != null) {
            edgeCount = vizEdges.size();
        }
        this.edges = edgeCount;
        this.proxyComponents = proxyCount;
        this.fullComponents = fullCount;
        this.isDataOk = !checker.isDataTooLarge(edgeCount, proxyCount, fullCount);
        if (this.isDataOk) {
            this.errorMessage = "";
        } else {
            this.errorMessage = ERROR_MESSAGE_TOO_LARGE;
        }
    }

    /**
     * @return Počet hran v odpovědi.
     */
    public int getEdges() {
        return edges;
    }

    /**
     * @return Počet proxy komponent v odpovědi.
     */
    public int getProxyComponents() {
        return proxyComponents;
    }

    /**
     * @return Počet plných komponent v odpovědi.
     */
    public int getFullComponents() {
        return fullComponents;
    }

    /**
     * @return True, jestliže je odpověď ok.
     */
    public boolean isDataOk() {
        return isDataOk;
    }

    /**
     * @return Případná chybová hláška.
     */
    public String getErrorMessage() {
        return errorMessage;
    }
}
