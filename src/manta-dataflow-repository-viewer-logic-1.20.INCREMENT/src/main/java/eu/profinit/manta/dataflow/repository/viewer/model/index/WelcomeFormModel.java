package eu.profinit.manta.dataflow.repository.viewer.model.index;

import java.util.Arrays;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Direction;

import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ServerRequest;

/**
 * @author Tomas Vik <tomas.vik@profinit.eu>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WelcomeFormModel implements ServerRequest {

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(WelcomeFormModel.class);
    
    private String[] selectedItems;

    private FlowLevel level = FlowLevel.MIDDLE;

    private Direction direction = Direction.BOTH;

    private boolean filterEdges;

    private int depth;
    
    private String filter;
    
    private String hash = ""; 
    
    private double revision;

    private double olderRevision = 0.0;
    
    private String hint;
    
    private double zoom = 1.0;

    /**
     * @return startovní uzly v podobě path
     */
    public String[] getSelectedItems() {
        if (selectedItems != null) {
            return Arrays.copyOf(selectedItems, selectedItems.length);
        } else {
            return new String[0];
        }
    }

    /**
     * @param selectedItems startovní uzly v podobě path
     */
    public void setSelectedItems(String[] selectedItems) {
        if (selectedItems != null) {
            this.selectedItems = Arrays.copyOf(selectedItems, selectedItems.length);
        } else {
            this.selectedItems = null;
        }
    }

    /**
     * @return úroveň zobrazení
     */
    public FlowLevel getLevel() {
        return level;
    }

    /**
     * @param level úroveň zobrazení 
     */
    public void setLevel(FlowLevel level) {
        if (level != null) {
            this.level = level;
        }
    }

    /**
     * @return směr zobrazovaných toků ze startovních uzlů
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * @param direction směr zobrazovaných toků ze startovních uzlů
     */
    public void setDirection(Direction direction) {
        if (direction != null) {
            this.direction = direction;
        }
    }

    /**
     * @return true, jestliže se mají zobrazovat filtrační hrany
     */
    public boolean isFilterEdges() {
        return filterEdges;
    }

    /**
     * @param filterEdges true, jestliže se mají zobrazovat filtrační hrany
     */
    public void setFilterEdges(boolean filterEdges) {
        this.filterEdges = filterEdges;
    }

    /**
     * @return počáteční delka toků ze startovních uzlů
     */
    public int getDepth() {
        return depth;
    }

    /**
     * @param depth počáteční delka toků ze startovních uzlů
     */
    public void setDepth(int depth) {
        this.depth = depth;
    }

    /**
     * @return id zvoleného filtru pro resources 
     */
    public String getFilter() {
        return filter;
    }

    /**
     * @param filter id zvoleného filtru pro resources
     */
    public void setFilter(String filter) {
        this.filter = filter;
    }

    /**
     * @return kontrolní hash dotazu
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash kontrolní hash dotazu
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return číslo revize, která se má použít
     */
    public double getRevision() {
        return revision;
    }

    /**
     * @param revision číslo revize, která se má použít
     */
    public void setRevision(double revision) {
        this.revision = revision;
    }

    /**
     * @return číslo starší revize, se kterou se může porovnávat
     */
    public double getOlderRevision() {
        return olderRevision;
    }

    /**
     * @param olderRevision číslo starší revize, se kterou se může porovnávat
     */
    public void setOlderRevision(double olderRevision) {
        this.olderRevision = olderRevision;
    }

    /**
     * @return ID napovedy pro ukazkovou galerii
     */
    public String getHint() {
        return hint;
    }

    /**
     * @param hint ID napovedy pro ukazkovou galerii
     */
    public void setHint(String hint) {
        this.hint = hint;
    }

    /**
     * @return Počáteční zoom scény. Hodnota je počet procent / 100.
     */
    public double getZoom() {
        return zoom;
    }

    /**
     * @param Počáteční zoom scény. Hodnota je počet procent / 100. Parametr je záměrně {@link Object},
     *        aby se ošetřily nečíselné hodnoty. V takovýchto případech bude zachována hodnota původní.
     */
    public void setZoom(Object zoom) {
        try {
            this.zoom = zoom != null ? Double.valueOf(zoom.toString()) : 1.0;
        }
        catch (NumberFormatException ex) {
            LOGGER.warn("Wrong zoom value: " + zoom);
        }
    }
    
}
