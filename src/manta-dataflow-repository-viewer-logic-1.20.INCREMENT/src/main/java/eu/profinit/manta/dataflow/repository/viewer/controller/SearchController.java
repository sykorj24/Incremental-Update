package eu.profinit.manta.dataflow.repository.viewer.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import au.com.bytecode.opencsv.CSVParser;
import au.com.bytecode.opencsv.CSVReader;
import cern.colt.Arrays;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterGroup;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterGroupProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionModel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.utils.CsvHelper;
import eu.profinit.manta.dataflow.repository.viewer.exception.NoCommitedDataException;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message.Severity;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.BaseResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.index.DirectSelectionModel;
import eu.profinit.manta.dataflow.repository.viewer.model.index.FindElementsResponse;
import eu.profinit.manta.dataflow.repository.viewer.model.index.Node;
import eu.profinit.manta.dataflow.repository.viewer.model.index.PathSeparator;
import eu.profinit.manta.dataflow.repository.viewer.security.ViewerRoles;
import eu.profinit.manta.dataflow.repository.viewer.service.filter.HorizontalFilterHelper;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.GraphService;
import eu.profinit.manta.dataflow.repository.viewer.service.index.IndexService;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;
import eu.profinit.manta.platform.web.core.security.SecurityHelper;

/**
 * Controller pro uvodni obrazovku - dohledavani objektu v databazi a 
 * nastaveni parametru vizualizace.
 * 
 * @author Matyas Krutsky <matyas.krutsky@profinit.eu>
 */
@Controller
@RequestMapping("/viewer")
public class SearchController {
    /** Zpráva zobrazená uživateli v případě nalezení žádného elementu k dané hierarchické cestě. */
    private static final String NO_ELEMENTS_MESSAGE = "No elements found for path: ";
    /** Zpráva zobrazená uživateli v případě nalezení více elementů k jedné hierarchické cestě. */
    private static final String MORE_ELEMENTS_MESSAGE = "There are more elements for path: ";
    /** Kodování vstupu pro přímý výběr. */
    private static final String ENCODING = "UTF-8";
    /** Název akce pro usage stats pro zobrazení katalogu. */
    private static final String FLOW_CATALOG_SHOW = "flow_catalog_show";
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchController.class);

    @Autowired
    private IndexService index;
    /** Provider poskytyující seznam resource fitlrů. */
    /** Sluzba pro praci s grafem.  */
    @Autowired
    private GraphService graphService;
    /** Služba pro ukládání statsitik použití. */
    @Autowired
    private UsageStatsCollector usageStatsCollector;
    @Autowired
    private HorizontalFilterHelper horizontalFilterHelper;
    @Autowired
    private HorizontalFilterGroupProvider horizontalFilterGroupProvider;
    @Autowired
    private HorizontalFilterProvider horizontalFilterProvider;

    /** Maximální hloubka iniciálního follow. */
    private int maximalDepth;
    /** Defaultni oddelovac polozek v ceste k elementu pri primem vyberu. */
    private PathSeparator defaultPathSeparator = PathSeparator.COMMA;

    /** 
     * Vykresleni formulare pro vyber kontextu.
     * @param model model pro view
     * @return název ftl šablony
     */
    @Secured(ViewerRoles.VIEWER_CATALOG)
    @RequestMapping(method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        model.addAttribute("message", "Welcome...!");
        Set<HorizontalFilterGroup> groupSet = horizontalFilterGroupProvider.getGroups();
        model.put("filters", horizontalFilterHelper.generateModelForGroups(groupSet, horizontalFilterProvider));
        model.put("maximalDepth", maximalDepth);

        List<RevisionModel> revisionModelList = graphService.getCommitedRevisionModels();
        model.put("revisionModelList", revisionModelList);
        model.put("lastRevisionModel", revisionModelList.get(revisionModelList.size() - 1));
        
        DirectSelectionModel directSelectionModel = new DirectSelectionModel();
        directSelectionModel.setSelectedPathSeparator(defaultPathSeparator);
        model.put("directSelectionModel", directSelectionModel);

        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_CATALOG_SHOW);
        return "manta-dataflow-repository-viewer/welcome";
    }

    /**
     * Nalezne všechny uzly odpovídající danému vzrou.
     * @param pattern vzor pro vyhledání
     * @param revision číslo revize, pro kterou se mají hledat výsledky
     * @return seznam uzlů odpovídající vzoru
     */
    @Secured(ViewerRoles.VIEWER_CATALOG)
    @RequestMapping(value = "/typeahead", method = RequestMethod.POST)
    @ResponseBody
    public List<Node> getAutocomplete(@RequestParam("pattern") String pattern,
            @RequestParam("revision") String revisionString) {
        double revision = 0.0;
        try {
            revision = Double.parseDouble(revisionString);
        } catch (NumberFormatException e) {
            LOGGER.error("Cannot parse revision number.", e);
            return Collections.<Node> emptyList();
        }

        return index.findItems(pattern, new RevisionInterval(revision, revision));
    }

    /**
     * Přímí potomci uzlu identifikovaného cestou. 
     * @param requestParams parametry dotazu
     * @return seznam potomků
     */
    @Secured(ViewerRoles.VIEWER_CATALOG)
    @RequestMapping(value = "/jstree", method = RequestMethod.GET)
    @ResponseBody
    public List<Node> getTreeLevel(@RequestParam MultiValueMap<String, String> requestParams) {
        double revision = 0.0;
        try {
            revision = Double.parseDouble(requestParams.getFirst("revision"));
        } catch (NumberFormatException e) {
            LOGGER.error("Cannot parse revision number.", e);
            return Collections.<Node> emptyList();
        }
        RevisionInterval revisionInterval = new RevisionInterval(revision, revision);

        List<String> pathString = requestParams.get("path[]");
        if (pathString != null) {
            List<Long> path = new ArrayList<>();
            for (String stringId : pathString) {
                try {
                    path.add(Long.valueOf(stringId));
                } catch (NumberFormatException e) {
                    LOGGER.error("Cannot parse path number.", e);
                    return Collections.<Node> emptyList();
                }
            }
            return index.getNodesOnPath(path, revisionInterval);
        } else {
            return index.getNodesOnPath(Collections.<Long> emptyList(), revisionInterval);
        }
    }

    /**
     * Ziskej uzly podle id.
     * @param ids id uzlu
     * @param revision číslo revize, pro kterou se mají hledat výsledky
     * @return seznam uzlu
     */
    @Secured(ViewerRoles.VIEWER_CATALOG)
    @RequestMapping(value = "/nodes", method = RequestMethod.GET)
    @ResponseBody
    public List<Node> getNodes(@RequestParam List<String> ids, @RequestParam("revision") double revision) {
        if (ids != null && !ids.isEmpty()) {
            return index.getNodes(ids, new RevisionInterval(revision, revision));
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * Získá uzlu podle cest.
     * @param elementsAsString csv-like zadání cest k uzlům.
     * @param pathSeparator Oddelovac polozek v ceste k elementu pri primem vyberu.
     * @param revision číslo revize, pro kterou se mají hledat výsledky
     * @return seznam uzlů
     */
    @Secured(ViewerRoles.VIEWER_CATALOG)
    @RequestMapping(value = "/find-elements", method = RequestMethod.POST)
    @ResponseBody
    public FindElementsResponse findElements(@RequestParam("elements") String elementsAsString,
            @RequestParam("pathSeparator") PathSeparator pathSeparator,
            @RequestParam("revision") double revision) {

        CSVReader reader;
        try {
            InputStream inputStream = IOUtils.toInputStream(elementsAsString, ENCODING);
            // NULL_CHARACTER proto ze escapovaci znak nepotrbujeme
            // - staci escapovat uvozovky jejich zdvojenim
            reader = CsvHelper.getCSVReader(new InputStreamReader(inputStream, ENCODING),
                    pathSeparator.getCharacter(), CsvHelper.QUOTE_CHAR, CSVParser.NULL_CHARACTER);
        } catch (IOException e) {
            String message = "Error during creating stream for reading.";
            LOGGER.error(message, e);
            return new FindElementsResponse(new Message(Severity.ERROR, message));
        }

        String[] row;
        try {
            List<Message> messages = new ArrayList<>();
            Set<Node> elements = new HashSet<>();

            while ((row = reader.readNext()) != null) {
                if (row.length == 0) {
                    continue;
                }

                List<Node> foundElements = index.findItemsByPath(row, new RevisionInterval(revision, revision));
                if (foundElements.size() == 0) {
                    messages.add(new Message(Severity.WARNING, NO_ELEMENTS_MESSAGE + Arrays.toString(row)));
                } else if (foundElements.size() > 1) {
                    messages.add(new Message(Severity.INFO, MORE_ELEMENTS_MESSAGE + Arrays.toString(row)));
                }

                elements.addAll(foundElements);
            }

            FindElementsResponse result = new FindElementsResponse();
            result.setElements(elements);
            result.setMessages(messages);
            return result;
        } catch (IOException e) {
            String message = "Error during reading row from the input text.";
            LOGGER.error(message, e);
            return new FindElementsResponse(new Message(Severity.ERROR, message));
        }

    }

    /**
     * Ošetření výjimek, které vyletí v r8mci zpracování dotazu z ajaxu.
     * @param exception vyhozená výjimka
     * @return json odpověď popisující problém
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public BaseResponse allException(Exception exception) {
        LOGGER.error("Exception has occurred during JSON request.", exception);

        String text = "Unexpected error.";
        if (StringUtils.isNotBlank(exception.getMessage())) {
            text += " Detail: " + exception.getMessage();
        }
        return new BaseResponse("error", new Message(Severity.ERROR, text));
    }

    /**
     * Ošetření výjimek při neexistenci dat.
     * @param exception vyhozená výjimka
     * @param request http request pro odeslání přesměrovávacích atributů
     * @return adresa pro přesměrování
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(NoCommitedDataException.class)
    @ResponseBody
    public BaseResponse noDataException(NoCommitedDataException exception, HttpServletRequest request) {
        LOGGER.warn("There is no commited data.", exception);
        return new BaseResponse("error", new Message(Severity.ERROR, exception.getMessage()));
    }

    /**
     * @return Maximální hloubka iniciálního follow.
     */
    public int getMaximalDepth() {
        return maximalDepth;
    }

    /**
     * @param maximalDepth Maximální hloubka iniciálního follow.
     */
    public void setMaximalDepth(int maximalDepth) {
        this.maximalDepth = maximalDepth;
    }

    /**
     * @return Defaultni oddelovac polozek v ceste k elementu pri primem vyberu
     */
    public PathSeparator getDefaultPathSeparator() {
        return defaultPathSeparator;
    }

    /**
     * @param defaultPathSeparator Defaultni oddelovac polozek v ceste k elementu pri primem vyberu
     */
    public void setDefaultPathSeparator(PathSeparator defaultPathSeparator) {
        this.defaultPathSeparator = defaultPathSeparator;
    }
    
}
