package eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;
import eu.profinit.manta.dataflow.repository.viewer.model.LevelMapProvider;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.service.FilteredRelationshipService;

/**
 * Faktory pro tvorbu jendnotlivých komponent.
 * @author tfechtner
 *
 */
public class ComponentFactory {
    /** Služba provádějící modifikace instance tesně po jejím vytvoření. */
    @Autowired(required = false)
    private ComponentModificator componentModificator;

    @Autowired
    private TechnicalAttributesHolder technicalAttributesHolder;

    /**
     * Factory metoda pro vytvoření komponenty.
     * @param transaction transakce pro pístup k db
     * @param vertex reprezentace uzlu z databáze
     * @param flowState stav daného flow
     * @param levelMapProvider provider pro získání definice úrovně a atomicity uzlu
     * @param relationshipService služba pro práci s předky 
     * @return nově vytvořená komponenta
     */
    public FullComponent createFullComponent(TitanTransaction transaction, Vertex vertex, FlowStateViewer flowState,
            LevelMapProvider levelMapProvider, FilteredRelationshipService relationshipService) {
        return createFullComponent(transaction, vertex, flowState, levelMapProvider, true, relationshipService);
    }

    /**
     * Factory metoda pro vytvoření komponenty.
     * @param transaction transakce pro pístup k db
     * @param vertex reprezentace uzlu z databáze
     * @param flowState stav daného flow
     * @param levelMapProvider provider pro získání definice úrovně a atomicity uzlu
     * @param isExpanded true, jestliže má být uzel rozbalen
     * @param relationshipService služba pro práci s předky 
     * @return nově vytvořená komponenta
     */
    public FullComponent createFullComponent(TitanTransaction transaction, Vertex vertex, FlowStateViewer flowState,
            LevelMapProvider levelMapProvider, boolean isExpanded, FilteredRelationshipService relationshipService) {
        FullComponent component = new FullComponent(transaction, vertex, flowState, levelMapProvider, isExpanded,
                relationshipService);

        Iterator<String> iterator = component.getAttributes().keySet().iterator();
        while (iterator.hasNext()) {
            if (technicalAttributesHolder.isNodeAttributeTechnical(iterator.next())) {
                iterator.remove();
            }
        }

        if (componentModificator != null) {
            return componentModificator.getModification(component);
        } else {
            return component;
        }
    }

    /**
     * Vytvoří instanci komponenty.
     * @param vertex zdrojový vertex
     * @param relationshipService služba pro práci s předky
     * @param flowState stav flow
     * @param levelMapProvider provider pro získání definice úrovně a atomicity uzlu
     * @return nová instance
     */
    public ProxyComponent createProxyComponent(Vertex vertex, FlowStateViewer flowState,
            FilteredRelationshipService relationshipService, LevelMapProvider levelMapProvider) {
        ProxyComponent component = new ProxyComponent(vertex, flowState, relationshipService, levelMapProvider);
        if (componentModificator != null) {
            return componentModificator.getModification(component);
        } else {
            return component;
        }
    }

    /**
     * @param technicalAttributesHolder služba na zjištění technických atributů
     */
    public void setTechnicalAttributesHolder(TechnicalAttributesHolder technicalAttributesHolder) {
        this.technicalAttributesHolder = technicalAttributesHolder;
    }
}
