package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState.NodeStatus;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.GraphFlow;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.GraphFlowAlgorithmExecutor;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmException;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message.Severity;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.InitRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.InitResponse;
import eu.profinit.manta.dataflow.repository.viewer.service.FilteredRelationshipService;

/**
 * Algoritmus pro vytvoreni referencniho podgrafu databaze.
 * 
 * @see eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer
 */
public class InitReferenceViewCommand extends AbstractCommand<InitRequest, InitResponse> {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(InitReferenceViewCommand.class);
    /** Hláška pro případ, kdy je příliš startovních uzlů. */
    private static final String TOO_MANY_STARTING_LIST_NODES = "Elements with too many leaf elements are selected. "
            + "Please, remove some elements and try again. ";
    /** Hláška pro případ, kdy spadnul algortmus pro výpočet ref view.*/
    private static final String ALGORITHM_FAILED_MESSAGE = "The problem has occured during initialization "
            + "of visualization. Some relations can be hidden.";

    @Autowired
    private FilteredRelationshipService relationshipService;

    /** Exekutor pro spouštní algoritmů v rámci hledání ref view. */
    private GraphFlowAlgorithmExecutor algorithmExecutor;

    @Override
    public InitResponse execute(InitRequest request, FlowStateViewer flowState, TitanTransaction transaction) {

        InitResponse response = new InitResponse();

        // profiltrovat vertikálními filtry
        Collection<Vertex> potentionalNodes = relationshipService.filterStartNodes(transaction,
                request.getReferencePoints(), flowState.getRevisionInterval());

        // ověřit existenci a dostupnost startovních uzlů
        List<Vertex> startNodes = new ArrayList<>();
        for (Vertex vertex : potentionalNodes) {
            if (vertex != null && isVertexAccessible(vertex, request, flowState)) {
                startNodes.add(vertex);
            }
        }

        // doplnit případné ekvivalentní uzly z porovnávací revize
        startNodes.addAll(GraphOperation.findEquivalentVertices(startNodes, flowState.getRevisionInterval().getEnd(),
                flowState.getRevisionInterval().getStart()));

        // naplnit flowState startovními uzly a jejich předky
        for (Vertex vertex : startNodes) {
            flowState.addStartNode((Long) vertex.getId());
            List<Vertex> startNodeParents = relationshipService.getAllParents(vertex);
            for (Vertex parent : startNodeParents) {
                flowState.addStartNodeParent((Long) parent.getId());
            }
        }

        // získat startovní listy
        Set<Vertex> startLeavesNodeSet = getStartNodes(startNodes, flowState.getRevisionInterval());

        // stráž na maximum listů
        if (startLeavesNodeSet.size() > request.getMaximumStartNodes()) {
            response.addMessage(new Message(Severity.ERROR, TOO_MANY_STARTING_LIST_NODES));
            response.setOk(false);
            return response;
        }

        // spočíst referenční view
        try {
            GraphFlow.fillReferenceView(startLeavesNodeSet, transaction, flowState, request.getDirection(),
                    request.isFilterEdges(), algorithmExecutor);
        } catch (GraphFlowAlgorithmException e) {
            LOGGER.error("Error during flow algorithm execution.", e);
            response.addMessage(new Message(Severity.ERROR, ALGORITHM_FAILED_MESSAGE));
            for (Object id : e.getOutputNodes()) {
                flowState.addNode((Long) id);
            }
        }

        // pročistit uzly, které existovaly dočasně mezi revizemi
        flowState.cleanVerticesInInnerRevisions(transaction);

        Set<Long> leaves = new HashSet<>(flowState.getReferenceNodeIds());
        // ověřit a případně přidat předky
        for (Long nodeId : leaves) {
            Vertex node = transaction.getVertex(nodeId);
            Vertex parent = relationshipService.getParent(node);
            // jestliže prvního předka ještě neznáme, přidat i předky
            if (parent != null && flowState.getNodeStatus(parent) == NodeStatus.UNKNOWN) {
                List<Vertex> parents = relationshipService.getAllParents(node);
                for (Vertex pNode : parents) {
                    flowState.addNode(pNode);
                }
            }
        }

        // nalézt ekvivalence v revizích
        flowState.createEquivalencesForRefView(transaction);

        // nastavit konfigurace z requestu
        flowState.setFilterEdgesShown(request.isFilterEdges());
        flowState.setFlowLevel(request.getFlowLevel());

        return response;
    }

    /**
     * Ověří, jestli je daný vrchol přístupný pro daný request a flowstate.
     * @param vertex ověřovaný vertex
     * @param request request pro danou tvorbu ref view
     * @param flowState aktuální flowState
     * @return true, jestliže je vertex přístupný
     */
    protected boolean isVertexAccessible(Vertex vertex, InitRequest request, FlowStateViewer flowState) {
        return true;
    }

    /**
     * Vrati vsechny listy dosazitelne ze zadanych uzlu.  
     * @param nodes seznam uzlů, pro něž chceme najít listové potomky
     * @param revisionInterval interval revizí daného flowState
     * @return seznam listových potomků, nikdy null
     */
    private Set<Vertex> getStartNodes(List<Vertex> nodes, RevisionInterval revisionInterval) {
        Set<Vertex> startListNodeSet = new HashSet<Vertex>();

        // získat všechny listové startovní uzly
        for (Vertex node : nodes) {
            startListNodeSet.addAll(GraphOperation.getAllLists(node, revisionInterval));
        }

        return startListNodeSet;
    }

    /**
     * @param relationshipService služba pro práci s předky 
     */
    public void setRelationshipService(FilteredRelationshipService relationshipService) {
        this.relationshipService = relationshipService;
    }

    /**
     * @return Exekutor pro spouštní algoritmů v rámci hledání ref view.
     */
    public GraphFlowAlgorithmExecutor getAlgorithmExecutor() {
        return algorithmExecutor;
    }

    /**
     * @param algorithmExecutor Exekutor pro spouštní algoritmů v rámci hledání ref view.
     */
    public void setAlgorithmExecutor(GraphFlowAlgorithmExecutor algorithmExecutor) {
        this.algorithmExecutor = algorithmExecutor;
    }
}
