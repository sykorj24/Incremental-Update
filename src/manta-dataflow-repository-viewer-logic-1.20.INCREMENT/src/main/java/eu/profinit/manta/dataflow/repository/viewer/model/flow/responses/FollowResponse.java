package eu.profinit.manta.dataflow.repository.viewer.model.flow.responses;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Component;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.DataInfo;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.FlowEdgeViz;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Message.Severity;

/**
 * Odpověď na follow command.
 *
 */
public class FollowResponse extends BaseResponse {

    /** Chybová odpověď, když nebyl dodán žádný korektní uzel. */
    public static final FollowResponse NO_START_NODES = new FollowResponse(
            new Message(Severity.ERROR, "Invalid element URL."), new DataInfo(0, 0, 0, true, ""));
    /** Chybová odpověď, když follow operace navštíví příliš mnoho uzlů. */
    public static final FollowResponse TOO_LARGE_FOLLOW = new FollowResponse(
            new Message(Severity.ERROR, "The follow operation visited too many nodes."),
            new DataInfo(0, 0, 0, false, "The follow operation visited too many nodes."));

    private Set<Component> components = new HashSet<>();
    private Set<FlowEdgeViz> flows = new HashSet<>();
    private DataInfo dataInfo;

    /**
     * Default konstruktor předávající předkovi název commandu.
     */
    public FollowResponse() {
        super("follow");
    }

    /**
     * @param message zpráva pro vložení
     * @param dataInfo info o datech
     */
    public FollowResponse(Message message, DataInfo dataInfo) {
        super("follow", message);
        this.dataInfo = dataInfo;
    }

    /**
     * @return seznam nových a upravených komponent v rámci tohoto follow
     */
    public Collection<Component> getComponents() {
        return components;
    }

    /**
     * @param components seznam nových a upravených komponent v rámci tohoto follow
     */
    public void setComponents(Set<Component> components) {
        this.components = components;
    }

    /**
     * @return seznam nových hran v rámci tohoto follow
     */
    public Set<FlowEdgeViz> getFlows() {
        return flows;
    }

    /**
     * @param flows seznam nových hran v rámci tohoto follow
     */
    public void setFlows(Set<FlowEdgeViz> flows) {
        this.flows = flows;
    }

    /**
     * @return informace o množství posílaných dat
     */
    public DataInfo getDataInfo() {
        return dataInfo;
    }

    /**
     * @param dataInfo informace o množství posílaných dat
     */
    public void setDataInfo(DataInfo dataInfo) {
        this.dataInfo = dataInfo;
    }
}
