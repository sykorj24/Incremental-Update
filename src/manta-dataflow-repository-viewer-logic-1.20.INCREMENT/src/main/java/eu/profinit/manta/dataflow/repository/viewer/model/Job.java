package eu.profinit.manta.dataflow.repository.viewer.model;

import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ServerRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ServerResponse;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.Command;

/**
 * Datová immutable třída pro definici jednoho konkrétního úkolu k vykonání.
 * @author tfechtner
 *
 * @param <Q> typ requestu
 * @param <S> typ response
 */
public class Job<Q extends ServerRequest, S extends ServerResponse> {
    /** Příkaz, který se má provést. */
    private final Command<Q, S> command;
    /** Request s daty pro příkaz. */
    private final Q request;
    /** Stav daného flow, obsahující referenční view, úroveň flow atd. */
    private final FlowStateViewer flowState;

    /**
     * @param command Příkaz, který se má provést.
     * @param request Request s daty pro příkaz.
     * @param flowState Stav daného flow, obsahující referenční view, úroveň flow atd.
     */
    public Job(Command<Q, S> command, Q request, FlowStateViewer flowState) {
        super();
        this.command = command;
        this.request = request;
        this.flowState = flowState;
    }

    /**
     * @return Příkaz, který se má provést.
     */
    public Command<Q, S> getCommand() {
        return command;
    }

    /**
     * @return Request s daty pro příkaz.
     */
    public Q getRequest() {
        return request;
    }

    /**
     * @return Stav daného flow, obsahující referenční view, úroveň flow atd.
     */
    public FlowStateViewer getFlowState() {
        return flowState;
    }

}
