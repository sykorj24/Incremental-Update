package eu.profinit.manta.dataflow.repository.viewer.model.flow;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.FlowState;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.viewer.service.flow.commands.helper.VertexEquivalencesHolder;

/**
 * Třída udržující stav zobrazení daného flow, sem patří referenční view, seznam zneaktivněných resource atd.
 */
public class FlowStateViewer extends FlowState {
    /** Zobrazovaná úroveň flow. */
    private FlowLevel flowLevel = FlowLevel.BOTTOM;
    /** Množina vrcholů, které již byly poslány jako full. */
    private final Set<Long> fullNodeSet;
    /** Množina id úplně startovních vrcholů od uživatele. */
    private final Set<Long> startNodes;
    /** Id předků startovních uzlů. */
    private final Set<Long> startNodesParents;
    /** Příznak, jestli se mají z rychlostních důvodů nepočítat aggregované hrany. */
    private boolean isAggEdgesDisabled = false;
    /** Správce ekvivalencí mezi vrcholy v různých revizích. */
    private final VertexEquivalencesHolder equivalencesHolder;

    /**
     * @param revisionInterval interval revizí, pro které se flow state používá
     * @param horizontalFilterProvider provider filtrů
     */
    public FlowStateViewer(RevisionInterval revisionInterval, HorizontalFilterProvider horizontalFilterProvider) {
        super(revisionInterval, horizontalFilterProvider);
        fullNodeSet = new HashSet<Long>();
        startNodes = new HashSet<Long>();
        startNodesParents = new HashSet<Long>();
        equivalencesHolder = new VertexEquivalencesHolder();
    }

    /**
     * Copy constructor, který provede deep-copy.
     * @param other kopírovaný stav
     */
    public FlowStateViewer(FlowStateViewer other) {
        super(other);
        this.fullNodeSet = new HashSet<>(other.fullNodeSet);
        this.startNodes = new HashSet<>(other.startNodes);
        this.startNodesParents = new HashSet<>(other.startNodesParents);
        this.flowLevel = other.getFlowLevel();
        this.isAggEdgesDisabled = other.isAggEdgesDisabled;
        this.equivalencesHolder = new VertexEquivalencesHolder(other.equivalencesHolder);
    }

    /**
     * @return Zobrazovaná úroveň flow.
     */
    public FlowLevel getFlowLevel() {
        return flowLevel;
    }

    /**
     * @param flowLevel Zobrazovaná úroveň flow.
     */
    public void setFlowLevel(FlowLevel flowLevel) {
        this.flowLevel = flowLevel;
    }

    /**
     * Nastaví startovní uzel.
     * @param newId id nového startovního uzlu
     */
    public void addStartNode(Long newId) {
        startNodes.add(newId);
    }

    /**
     * Nastaví předka startovních uzlů.
     * @param newId id nového předka startovního uzlu
     */
    public void addStartNodeParent(Long newId) {
        startNodesParents.add(newId);
    }

    /**
     * Ověří jestli je uzel totálně startovní, tedy jestli jde o uzel, který spustil celé flow.
     * @param id id zkoumaného uzlu
     * @return true, jestliže je totálně startovní
     */
    public boolean isTotalStartNode(Long id) {
        return startNodes.contains(id);
    }

    /**
     * @return kolekce id totálně startovních uzlů
     */
    public Collection<Long> getTotalStartNodes() {
        return Collections.unmodifiableSet(startNodes);
    }

    /**
     * Ověří jestli je uzel mezi předky totálně startovních uzlů, tedy uzlů, které spustily celé flow.
     * @param id id zkoumaného uzlu
     * @return true, jestliže se jedná o předka některého totálně startovního uzlu
     */
    public boolean isTotalStartNodeParent(Long id) {
        return startNodesParents.contains(id);
    }

    /**
     * Získá status uzlu v daném view.
     * @param vertex uzel, jehož status se zjišťuje
     * @return aktuální status uzlu, nikdy null
     */
    @Override
    public NodeStatus getNodeStatus(Vertex vertex) {
        return getNodeStatus((Long) vertex.getId());
    }

    /**
     * Ověří, že vrchol s daným id byl již poslán jako full.
     * @param vertexId id zkoumaného vrcholu
     * @return true, jestliže byl vrchol poslán již jako full
     */
    public boolean isNodeSentAsFull(Long vertexId) {
        return fullNodeSet.contains(vertexId);
    }

    /**
     * Označí uzel, že už byl poslán jako full.
     * @param vertexId id označovaného uzlu
     */
    public void markNodeAsFull(Long vertexId) {
        fullNodeSet.add(vertexId);
    }

    /**
     * @return Příznak, jestli se mají z rychlostních důvodů nepočítat aggregované hrany.
     */
    public boolean isAggEdgesDisabled() {
        return isAggEdgesDisabled;
    }

    /**
     * @param isAggEdgesDisabledParam Příznak, jestli se mají z rychlostních důvodů nepočítat aggregované hrany.
     */
    public void setAggEdgesDisabled(boolean isAggEdgesDisabledParam) {
        this.isAggEdgesDisabled = isAggEdgesDisabledParam;
    }

    /**
     * Odstraní z referenčního view vrcholy mající platnost pouze uprostřed intervalu revizí.
     * @param transaction transakce pro přístup k db
     */
    public void cleanVerticesInInnerRevisions(TitanTransaction transaction) {
        if (getRevisionInterval().getStart() == getRevisionInterval().getEnd()) {
            return;
        }

        Iterator<Long> verticesIterator = getReferenceNodeIds().iterator();
        while (verticesIterator.hasNext()) {
            Long vertexId = verticesIterator.next();
            Vertex vertex = transaction.getVertex(vertexId);
            Edge controlEdge = GraphOperation.getNodeControlEdge(vertex);

            Double edgeTranStart = controlEdge.getProperty(EdgeProperty.TRAN_START.t());
            Double edgeTranEnd = controlEdge.getProperty(EdgeProperty.TRAN_END.t());

            if (edgeTranStart > getRevisionInterval().getStart() && edgeTranEnd < getRevisionInterval().getEnd()) {
                verticesIterator.remove();
            }
        }
    }

    /**
     * Vytvoří si ekvivalence pro dané referenční view.
     * @param transaction transakce pro přístup k db
     */
    public void createEquivalencesForRefView(TitanTransaction transaction) {
        if (getRevisionInterval().getStart() == getRevisionInterval().getEnd()) {
            return;
        }
        equivalencesHolder.buildForVertices(transaction, getReferenceNodeIds(), getRevisionInterval().getEnd());
    }

    /**
     * @return Správce ekvivalencí mezi vrcholy v různých revizích.
     */
    public VertexEquivalencesHolder getEquivalencesHolder() {
        return equivalencesHolder;
    }
}
