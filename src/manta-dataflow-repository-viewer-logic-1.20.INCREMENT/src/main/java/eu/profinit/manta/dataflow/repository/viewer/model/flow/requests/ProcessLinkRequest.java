package eu.profinit.manta.dataflow.repository.viewer.model.flow.requests;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Direction;

import eu.profinit.manta.dataflow.repository.core.model.FlowLevel;

/**
 * Request pro zpracování permalinku do flow.
 * @author tfechtner
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessLinkRequest implements ServerRequest {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessLinkRequest.class);
    
    private FlowLevel level;

    private Direction direction;

    private boolean filterEdges;

    private int depth;
    
    private String filter;
    
    private String hash;
    
    private double zoom = 1.0;

    /**
     * Seznam startovnách uzlů jako plně kvalifikované jméno. <br>
     * [[resourceName, resourceTyp], [ancestor1Name, ancestor1Type], ... [nodeName, nodeType]]
     */
    private List<List<List<String>>> si;
    
    private Long rootId;
    
    /**
     * @return seznam startovnách uzlů jako plně kvalifikované jméno
     */
    public List<List<List<String>>> getSi() {
        return si;
    }

    /**
     * @param si seznam startovnách uzlů jako plně kvalifikované jméno
     */
    public void setSi(List<List<List<String>>> si) {
        this.si = si;
    }

    /**
     * @return úroveň zobrazení
     */
    public FlowLevel getLevel() {
        return level;
    }

    /**
     * @param level úroveň zobrazení 
     */
    public void setLevel(FlowLevel level) {
        this.level = level;
    }

    /**
     * @return směr zobrazovaných toků ze startovních uzlů
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * @param direction směr zobrazovaných toků ze startovních uzlů
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * @return true, jestliže se mají zobrazovat filtrační hrany
     */
    public boolean isFilterEdges() {
        return filterEdges;
    }

    /**
     * @param filterEdges true, jestliže se mají zobrazovat filtrační hrany
     */
    public void setFilterEdges(boolean filterEdges) {
        this.filterEdges = filterEdges;
    }

    /**
     * @return počáteční delka toků ze startovních uzlů
     */
    public int getDepth() {
        return depth;
    }

    /**
     * @param depth počáteční delka toků ze startovních uzlů
     */
    public void setDepth(int depth) {
        this.depth = depth;
    }

    /**
     * @return id zvoleného filtru pro resources 
     */
    public String getFilter() {
        return filter;
    }

    /**
     * @param filter id zvoleného filtru pro resources
     */
    public void setFilter(String filter) {
        this.filter = filter;
    }

    /**
     * @return id rootu databáze
     */
    public Long getRootId() {
        return rootId;
    }

    /**
     * @param rootId id rootu databáze
     */
    public void setRootId(Long rootId) {
        this.rootId = rootId;
    }

    /**
     * @return kontrolní hash dotazu
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash kontrolní hash dotazu
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return Počáteční zoom scény. Hodnota je počet procent / 100.
     */
    public double getZoom() {
        return zoom;
    }

    /**
     * @param Počáteční zoom scény. Hodnota je počet procent / 100. Parametr je záměrně {@link Object},
     *        aby se ošetřily nečíselné hodnoty. V takovýchto případech bude zachována hodnota původní.
     */
    public void setZoom(Object zoom) {
        try {
            this.zoom = zoom != null ? Double.valueOf(zoom.toString()) : 1.0;
        }
        catch (NumberFormatException ex) {
            LOGGER.warn("Wrong zoom value: " + zoom);
        }
    }
    
}
