package eu.profinit.manta.dataflow.repository.viewer.service.flow.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.FlowStateViewer;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Resource;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.requests.ResourceListRequest;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.responses.ResourceListResponse;
import eu.profinit.manta.dataflow.repository.viewer.service.FilteredRelationshipService;

/**
 * Command pro získání seznamu resource v databázi. 
 * @author tfechtner
 *
 */
public class ResourceListCommand extends AbstractCommand<ResourceListRequest, ResourceListResponse> {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceListCommand.class);

    @Autowired
    private FilteredRelationshipService relationshipService;

    @Override
    public ResourceListResponse execute(ResourceListRequest request, FlowStateViewer flowState,
            TitanTransaction transaction) {
        Map<String, Resource> resourceMap = new HashMap<>();

        Vertex root = transaction.getVertex(request.getRootId());
        List<Edge> resourceEdges = GraphOperation.getAdjacentEdges(root, Direction.IN, flowState.getRevisionInterval(),
                EdgeLabel.HAS_RESOURCE);
        RevisionInterval endRevision = new RevisionInterval(flowState.getRevisionInterval().getEnd());

        for (Edge edge : resourceEdges) {
            Vertex resource = edge.getVertex(Direction.OUT);
            Object resourceId = resource.getId();
            String resourceName = resource.getProperty(NodeProperty.RESOURCE_NAME.t()).toString();
            Vertex layer = GraphOperation.getLayer(resource);
            String layerName = layer.getProperty(NodeProperty.LAYER_NAME.t()).toString();

            Resource resourceModel = new Resource();
            resourceModel.setId(resourceId);
            resourceModel.setName(resourceName);
            resourceModel.setLayer(layerName);
            
            Resource savedResource = resourceMap.get(resourceName);
            if (savedResource != null) {
                // resource s tímto jménem již byl potkán
                if (flowState.getRevisionInterval().isComparing()) {
                    // je to comparing
                    if (RevisionUtils.isEdgeInRevisionInterval(edge, endRevision)) {
                        // hrana má platnost v hlavní revizi -> nahradit
                        resourceMap.put(resourceName, resourceModel);
                        flowState.getEquivalencesHolder().registerResourcePair((Long) resourceId,
                                (Long)savedResource.getId());
                    } else {
                        // nemá platnost, tím pádem je předchozí resource vybrán správně
                        flowState.getEquivalencesHolder().registerResourcePair((Long)savedResource.getId(),
                                (Long) resourceId);
                    }
                } else {
                    // není comparing = fujtafl chyba
                    LOGGER.error("Two resources with same name in non-comparing revision interval - {}: {} and {}.",
                            resourceName, resourceMap.get(resourceName), resourceId);
                }
            } else {
                resourceMap.put(resourceName, resourceModel);
            }
        }

        ResourceListResponse response = new ResourceListResponse();
        response.setResourceList(new ArrayList<>(resourceMap.values()));
        return response;
    }

    /**
     * @param relationshipService služba pro práci s předky 
     */
    public void setRelationshipService(FilteredRelationshipService relationshipService) {
        this.relationshipService = relationshipService;
    }

    /**
     * @return služba pro práci s předky 
     */
    public FilteredRelationshipService getRelationshipService() {
        return relationshipService;
    }
}
