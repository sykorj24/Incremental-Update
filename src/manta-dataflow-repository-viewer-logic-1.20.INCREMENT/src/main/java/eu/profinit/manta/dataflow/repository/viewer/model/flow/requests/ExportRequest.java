package eu.profinit.manta.dataflow.repository.viewer.model.flow.requests;

import java.io.OutputStream;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Request pro export command. <br>
 * Kromě modifikátorů export obsahuje i výstupní field {@link #outputStream}, který slouží k předání
 * výstupního streamu dat z export commandu.
 * @author tfechtner
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExportRequest implements ServerRequest {
    /** Index session pro získání příslušného flow state.*/
    private Integer sessionIndex;
    /** Výstupní stream, kam se zpíšou data z commandu. Jde tedy o výstupní strukturu. */
    private OutputStream outputStream; 
    /** Oddělovač v jednotlivých částech jmen vrcholů. */
    private String nameDelimiter = ".";
    
    /**
     * @return index session pro získání příslušného flowState
     */
    public Integer getSessionIndex() {
        return sessionIndex;
    }

    /**
     * @param sessionIndex index session pro získání příslušného flowState
     */
    public void setSessionIndex(Integer sessionIndex) {
        this.sessionIndex = sessionIndex;
    }

    /**
     * @return Výstupní stream, kam se zpíšou data z commandu. Jde tedy o výstupní strukturu.
     */
    public OutputStream getOutputStream() {
        return outputStream;
    }

    /**
     * @param outputStream Výstupní stream, kam se zpíšou data z commandu. Jde tedy o výstupní strukturu.
     */ 
    public void setOutputStream(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    /**
     * @return Oddělovač v jednotlivých částech jmen vrcholů.
     */
    public String getNameDelimiter() {
        return nameDelimiter;
    }

    /**
     * @param nameDelimiter Oddělovač v jednotlivých částech jmen vrcholů.
     */
    public void setNameDelimiter(String nameDelimiter) {
        this.nameDelimiter = nameDelimiter;
    }    
}
