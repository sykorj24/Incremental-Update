package eu.profinit.manta.dataflow.repository.viewer.model.flow.responses;

import java.util.List;

import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.Component;
import eu.profinit.manta.dataflow.repository.viewer.model.flow.client.data.DataInfo;

/**
 * Odpověď pro dotaz na jednotlivé komponenty.
 *
 */
public class ComponentsResponse extends BaseResponse {

    private List<Component> components;
    
    private DataInfo dataInfo;

    /**
     * Default konstruktor předávající předkovi název commandu.
     */
    public ComponentsResponse() {
        super("components");
    }
    
    /**
     * @return seznam s plným popisem komponent
     */
    public List<Component> getComponents() {
        return components;
    }

    /**
     * @param components  seznam s plným popisem komponent
     */
    public void setComponents(List<Component> components) {
        this.components = components;
    }

    /**
     * @return informace o množství posílaných dat
     */
    public DataInfo getDataInfo() {
        return dataInfo;
    }

    /**
     * @param dataInfo informace o množství posílaných dat
     */
    public void setDataInfo(DataInfo dataInfo) {
        this.dataInfo = dataInfo;
    }
}