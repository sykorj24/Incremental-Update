package eu.profinit.manta.dataflow.repository.exporter.server.visitor;

import java.io.CharArrayWriter;
import java.io.PrintWriter;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphTraverser;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

/**
 * Test visitoru pro export.
 * @author tfechtner
 *
 */
public class ExporterVisitorTest extends TestTitanDatabaseProvider {

    /**
     * Otestuje na exportu basic grafu. 
     */
    @Test
    public void testExport() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex root = getSuperRootHandler().getRoot(transaction);
                Vertex logicalLayer = GraphCreation.createLayer(transaction, "Logical", "Logical");
                Vertex layerCloned = GraphCreation.createLayer(transaction, "Logical", "Logical");
                Vertex res2 = GraphCreation.createResource(transaction, root, "res2", "res2Type", "res2Desc", logicalLayer, REVISION_2);
                Vertex res3 = GraphCreation.createResource(transaction, root, "res3", "res3Type", "res3Desc", layerCloned, REVISION_2);
                Vertex application = GraphCreation.createNode(transaction, res2, "OFSAA", "Application", REVISION_2);
                Vertex stage = GraphCreation.createNode(transaction, application, "CORE", "Stage", REVISION_2);
                Vertex asset = GraphCreation.createNode(transaction, stage, "Party", "Asset", REVISION_2);
                Vertex attribute = GraphCreation.createNode(transaction, asset, "HNWI Flag", "Attribute", REVISION_2);

                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                GraphCreation.createEdge(t2c1, attribute, DatabaseStructure.EdgeLabel.MAPS_TO, REVISION_INTERVAL_2);
                
                String logicalLayerId = logicalLayer.getId().toString();
                String resId2 = res2.getId().toString();
                String resId3 = res3.getId().toString();
                String appId = application.getId().toString();
                String stageId = stage.getId().toString();
                String assetId = asset.getId().toString();
                String attributeId = attribute.getId().toString();
                
                GraphTraverser traverser = new GraphTraverserDfsPrefixResourceFirst();

                ExporterVisitor visitor = new ExporterVisitor(new DummyTechnicalAttributesHolder());
                CharArrayWriter writer = new CharArrayWriter();
                visitor.setWriter(new PrintWriter(writer));
                traverser.traverse(visitor, getSuperRootHandler().getRoot(transaction), REVISION_INTERVAL_2);

                String physicalLayerId = transaction.getVertices(NodeProperty.LAYER_NAME.t(), "Physical").iterator().next()
                        .getId().toString();
                String resId = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Teradata").iterator().next()
                        .getId().toString();
                String dbId = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next().getId()
                        .toString();
                String t1Id = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next().getId()
                        .toString();
                String t1c1Id = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next().getId()
                        .toString();
                String t1c2Id = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next().getId()
                        .toString();
                String t2Id = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next().getId()
                        .toString();
                String t2c1Id = t2c1.getId().toString();
                String t2c2Id = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next().getId()
                        .toString();

                CharArrayWriter expected = new CharArrayWriter();
                PrintWriter printWriterExpected = new PrintWriter(expected);
                printWriterExpected.print("\"layer\",\"" + physicalLayerId + "\",\"Physical\",\"Physical\"\n");
                printWriterExpected.print("\"layer\",\"" + logicalLayerId + "\",\"Logical\",\"Logical\"\n");
                printWriterExpected.print("\"resource\",\"" + resId + "\",\"Teradata\",\"Teradata\",\"Description\",\"" + physicalLayerId + "\"\n");
                printWriterExpected.print("\"resource\",\"" + resId2 + "\",\"res2\",\"res2Type\",\"res2Desc\",\"" + logicalLayerId + "\"\n");
                printWriterExpected.print("\"resource\",\"" + resId3 + "\",\"res3\",\"res3Type\",\"res3Desc\",\"" + logicalLayerId + "\"\n");
                printWriterExpected.print("\"node\",\"" + dbId + "\",\"\",\"db\",\"Database\",\"" + resId +"\"\n");

                printWriterExpected.print("\"node\",\"" + t1Id + "\",\"" + dbId + "\",\"table1\",\"Table\",\"" + resId+"\"\n");
                printWriterExpected.print("\"node\",\"" + t1c1Id + "\",\"" + t1Id + "\",\"t1c1\",\"Column\",\"" + resId+"\"\n");
                printWriterExpected.print("\"node\",\"" + t1c2Id + "\",\"" + t1Id + "\",\"t1c2\",\"Column\",\"" + resId+"\"\n");
                printWriterExpected.print("\"node_attribute\",\"" + t1Id + "\",\"TABLE_TYPE\",\"TABLE\"\n");

                printWriterExpected.print("\"node\",\"" + t2Id + "\",\"" + dbId + "\",\"table2\",\"Table\",\"" + resId+"\"\n");
                printWriterExpected.print("\"node\",\"" + t2c1Id + "\",\"" + t2Id + "\",\"t2c1\",\"Column\",\"" + resId+"\"\n");
                printWriterExpected.print("\"node\",\"" + t2c2Id + "\",\"" + t2Id + "\",\"t2c2\",\"Column\",\"" + resId+"\"\n");
                printWriterExpected.print("\"node_attribute\",\"" + t2Id + "\",\"TABLE_TYPE\",\"VIEW\"\n");

                printWriterExpected.print("\"node\",\"" + appId + "\",\"\",\"OFSAA\",\"Application\",\"" + resId2 +"\"\n");

                printWriterExpected.print("\"node\",\"" + stageId + "\",\"" + appId + "\",\"CORE\",\"Stage\",\"" + resId2+"\"\n");
                printWriterExpected.print("\"node\",\"" + assetId + "\",\"" + stageId + "\",\"Party\",\"Asset\",\"" + resId2+"\"\n");
                printWriterExpected.print("\"node\",\"" + attributeId + "\",\"" + assetId + "\",\"HNWI Flag\",\"Attribute\",\"" + resId2+"\"\n");
                
                printWriterExpected.print("\"edge\",\"0\",\"" + t1c1Id + "\",\"" + t1c2Id + "\",\"DIRECT\",\""
                        + resId + "\"\n");
                printWriterExpected.print("\"edge\",\"1\",\"" + t1c1Id + "\",\"" + t2c1Id + "\",\"FILTER\",\"" + resId+"\"\n");
                printWriterExpected.print("\"edge\",\"2\",\"" + t1c2Id + "\",\"" + t2c1Id + "\",\"DIRECT\",\"" + resId+"\"\n");
                printWriterExpected.print("\"edge\",\"3\",\"" + t2c1Id + "\",\"" + t2c2Id + "\",\"DIRECT\",\"" + resId+"\"\n");
                printWriterExpected.print("\"edge\",\"4\",\"" + t2c1Id + "\",\"" + attributeId + "\",\"MAPS_TO\",\"" + resId+"\"\n");

                Assert.assertEquals(expected.toString(), writer.toString());
                
                return null;
            }
        });
    }
    
}
