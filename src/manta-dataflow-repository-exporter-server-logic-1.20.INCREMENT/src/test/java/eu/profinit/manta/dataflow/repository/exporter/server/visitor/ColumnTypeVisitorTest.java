package eu.profinit.manta.dataflow.repository.exporter.server.visitor;

import java.io.CharArrayWriter;
import java.io.PrintWriter;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphTraverser;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphTraverserDfsPrefix;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import junit.framework.Assert;

/**
 * Test visitoru pro mapování sloupců.
 * @author tfechtner
 *
 */
public class ColumnTypeVisitorTest extends TestTitanDatabaseProvider {

    
    /**
     * Otestuje mapování sloupců. 
     */
    @Test
    public void testMapping() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                GraphCreation.createNodeAttribute(transaction, t1c1, ColumnTypeVisitor.ATTR_COLUMN_TYPE, "I", REVISION_2);
                GraphCreation.createNodeAttribute(transaction, t1c1, ColumnTypeVisitor.ATTR_COLUMN_LENGTH, "2", REVISION_2);
                GraphCreation.createNodeAttribute(transaction, t1c1, ColumnTypeVisitor.ATTR_COLUMN_CHARSET, "0", REVISION_2);
                
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                GraphCreation.createNodeAttribute(transaction, t1c2, ColumnTypeVisitor.ATTR_COLUMN_TYPE, "D", REVISION_2);
                GraphCreation.createNodeAttribute(transaction, t1c2, ColumnTypeVisitor.ATTR_COLUMN_LENGTH, "4", REVISION_2);
                GraphCreation.createNodeAttribute(transaction, t1c2, ColumnTypeVisitor.ATTR_COLUMN_CHARSET, "1", REVISION_2);
                
                GraphTraverser traverser = new GraphTraverserDfsPrefix();

                ColumnTypeVisitor visitor = new ColumnTypeVisitor(transaction, REVISION_INTERVAL_2);
                CharArrayWriter writer = new CharArrayWriter();
                visitor.setWriter(new PrintWriter(writer));
                traverser.traverse(visitor, getSuperRootHandler().getRoot(transaction), REVISION_INTERVAL_2);
                
                CharArrayWriter expected = new CharArrayWriter();
                PrintWriter printWriterExpected = new PrintWriter(expected);
                printWriterExpected.print("\"db\",\"table1\",\"t1c1\",\"db\",\"table1\",\"t1c1\",\"I\",\"2\",\"0\"\n");
                printWriterExpected.print("\"db\",\"table1\",\"t1c2\",\"db\",\"table1\",\"t1c2\",\"D\",\"4\",\"1\"\n");
                printWriterExpected.print("\"db\",\"table1\",\"t1c2\",\"db\",\"table2\",\"t2c1\",\"D\",\"4\",\"1\"\n");
                printWriterExpected.print("\"db\",\"table1\",\"t1c2\",\"db\",\"table2\",\"t2c2\",\"D\",\"4\",\"1\"\n");
                
                Assert.assertEquals(expected.toString(), writer.toString());
                return null;
            }
        });
    }
    
    /**
     * Otestuje neexistující atributy. 
     */
    @Test
    public void testMappingNonExistent() {
        cleanGraph();
        createBasicGraph();
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                GraphTraverser traverser = new GraphTraverserDfsPrefix();
                
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                GraphCreation.createNodeAttribute(transaction, t1c1, ColumnTypeVisitor.ATTR_COLUMN_TYPE, "I", REVISION_2);
                GraphCreation.createNodeAttribute(transaction, t1c1, ColumnTypeVisitor.ATTR_COLUMN_LENGTH, "2", REVISION_2);

                ColumnTypeVisitor visitor = new ColumnTypeVisitor(transaction, REVISION_INTERVAL_2);
                CharArrayWriter writer = new CharArrayWriter();
                visitor.setWriter(new PrintWriter(writer));
                traverser.traverse(visitor, getSuperRootHandler().getRoot(transaction), REVISION_INTERVAL_2);
                
                CharArrayWriter expected = new CharArrayWriter();
                PrintWriter printWriterExpected = new PrintWriter(expected);
                printWriterExpected.print("\"db\",\"table1\",\"t1c1\",\"db\",\"table1\",\"t1c1\",\"I\",\"2\",\"\"\n");
                printWriterExpected.print("\"db\",\"table1\",\"t1c2\",\"db\",\"table1\",\"t1c2\",\"\",\"\",\"\"\n");
                printWriterExpected.print("\"db\",\"table1\",\"t1c2\",\"db\",\"table2\",\"t2c1\",\"\",\"\",\"\"\n");
                printWriterExpected.print("\"db\",\"table1\",\"t1c2\",\"db\",\"table2\",\"t2c2\",\"\",\"\",\"\"\n");
                
                Assert.assertEquals(expected.toString(), writer.toString());
                return null;
            }
        });
    }

}
