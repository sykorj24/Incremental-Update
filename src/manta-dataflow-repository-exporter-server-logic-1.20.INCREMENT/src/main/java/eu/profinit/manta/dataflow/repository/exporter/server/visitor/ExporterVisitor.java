package eu.profinit.manta.dataflow.repository.exporter.server.visitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.ItemTypes;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;
import eu.profinit.manta.dataflow.repository.utils.OccuranceCountHolder;

/**
 * Visitor pro vypsání struktury grafu do writeru.
 * @author tfechtner
 *
 */
public final class ExporterVisitor extends AbstractWriterVisitor {
    /** Čítač pro generování id hran. */
    private int edgeCounter;
    /** Služba pro zjištení, jestli jde o technický parametr. */
    private TechnicalAttributesHolder terchnicalAttributes;
    /** Mapa exportovaných typů vrcholů a jejich počty. */
    private OccuranceCountHolder<VertexType> vertexTypeCounts = new OccuranceCountHolder<VertexType>();
    /** Mapa exportovaných typů hran a jejich počty. */
    private OccuranceCountHolder<EdgeLabel> edgeTypeCounts = new OccuranceCountHolder<EdgeLabel>();
    /** Mapa nazvu vrstev na ID primarni vrstvy */
    private Map<String, Object> primaryLayers = new HashMap<>();
    /** Mapa ID vrstvy na ID priparni vrstvy */
    private Map<Object, Object> layerMappig = new HashMap<>();

    /**
     * @param terchnicalAttributes Služba pro zjištení, jestli jde o technický parametr. 
     */
    public ExporterVisitor(TechnicalAttributesHolder terchnicalAttributes) {
        super();
        this.terchnicalAttributes = terchnicalAttributes;
    }

    @Override
    public void visitLayer(Vertex layer) {
        // Zjistime, zda jsme jiz neexportovali vrstvu se stejnym nazvem 
        String layerName = getFormatedProperty(layer, NodeProperty.LAYER_NAME);
        Object primaryLayerId = primaryLayers.get(layerName);
        if (primaryLayerId != null) {
            // Pokud ano, namapujeme jeji ID na ID exportovane vrstvy.
            // Tim sloucime stejne vrstvy do jednoho zaznamu
            layerMappig.put(layer.getId(), primaryLayerId);
            return;
        }
        
        List<String> csvRecord = new ArrayList<String>();
        csvRecord.add(ItemTypes.LAYER.getText());
        csvRecord.add(String.valueOf(layer.getId()));
        csvRecord.add(layerName);
        csvRecord.add(getFormatedProperty(layer, NodeProperty.LAYER_TYPE));
        getCsvWriter().writeNext(csvRecord.toArray(new String[csvRecord.size()]));

        // Zapamatujeme si exportovanou vrstvu jako primarni.
        primaryLayers.put(layerName, layer.getId());
        layerMappig.put(layer.getId(), layer.getId());
        
        vertexTypeCounts.addOccurance(VertexType.LAYER);
    }

    @Override
    public void visitResource(Vertex resource) {
        Vertex layerVertex = GraphOperation.getLayer(resource);
        List<String> csvRecord = new ArrayList<String>();
        csvRecord.add(ItemTypes.RESOURCE.getText());
        csvRecord.add(String.valueOf(resource.getId()));
        csvRecord.add(getFormatedProperty(resource, NodeProperty.RESOURCE_NAME));
        csvRecord.add(getFormatedProperty(resource, NodeProperty.RESOURCE_TYPE));
        csvRecord.add(getFormatedProperty(resource, NodeProperty.RESOURCE_DESCRIPTION));
        
        // Zjistime ID primarni vrstvy
        Object layerId; 
        if (layerVertex != null) {
            layerId = layerMappig.get(layerVertex.getId());
        } else {
            layerId = null;
        }
        // a zapiseme je.
        csvRecord.add(layerId != null ? layerId.toString() : "");
        
        getCsvWriter().writeNext(csvRecord.toArray(new String[csvRecord.size()]));

        vertexTypeCounts.addOccurance(VertexType.RESOURCE);
    }

    private String getFormatedProperty(Vertex vertex, NodeProperty propertyName) {
        Object value = vertex.getProperty(propertyName.t());
        if (value != null) {
            return value.toString();
        } else {
            return "";
        }
    }

    @Override
    public void visitNode(Vertex node) {
        Vertex resourceVertex = GraphOperation.getResource(node);
        Vertex parentVertex = GraphOperation.getParent(node);
        if (resourceVertex != null && resourceVertex.equals(parentVertex)) {
            parentVertex = null;
        }

        List<String> csvRecord = new ArrayList<String>();
        csvRecord.add(ItemTypes.NODE.getText());
        csvRecord.add(String.valueOf(node.getId()));
        csvRecord.add(parentVertex != null ? parentVertex.getId().toString() : "");
        csvRecord.add(getFormatedProperty(node, NodeProperty.NODE_NAME));
        csvRecord.add(getFormatedProperty(node, NodeProperty.NODE_TYPE));
        csvRecord.add(resourceVertex != null ? resourceVertex.getId().toString() : "");
        getCsvWriter().writeNext(csvRecord.toArray(new String[csvRecord.size()]));

        vertexTypeCounts.addOccurance(VertexType.NODE);
    }

    @Override
    public void visitAttribute(Vertex attribute) {
        Object attrName = attribute.getProperty(NodeProperty.ATTRIBUTE_NAME.t());

        if (attrName != null && !terchnicalAttributes.isNodeAttributeTechnical(attrName.toString())) {
            Vertex ownerVertex = GraphOperation.getAttributeOwner(attribute);
            List<String> csvRecord = new ArrayList<String>();
            csvRecord.add(ItemTypes.NODE_ATTRIBUTE.getText());
            csvRecord.add(ownerVertex.getId().toString());
            csvRecord.add(getFormatedProperty(attribute, NodeProperty.ATTRIBUTE_NAME));
            csvRecord.add(getFormatedProperty(attribute, NodeProperty.ATTRIBUTE_VALUE));
            getCsvWriter().writeNext(csvRecord.toArray(new String[csvRecord.size()]));

        }

        vertexTypeCounts.addOccurance(VertexType.ATTRIBUTE);
    }

    @Override
    public void visitEdge(Edge edge) {
        EdgeLabel edgeLabel = EdgeLabel.parseFromDbType(edge.getLabel());

        Vertex sourceVertex = edge.getVertex(Direction.OUT);
        Vertex targetVertex = edge.getVertex(Direction.IN);

        List<String> csvRecord = new ArrayList<String>();
        csvRecord.add(ItemTypes.EDGE.getText());
        csvRecord.add(String.valueOf(edgeCounter));
        csvRecord.add(String.valueOf(sourceVertex.getId()));
        csvRecord.add(String.valueOf(targetVertex.getId()));
        csvRecord.add(edgeLabel != null ? edgeLabel.getGraphRepr() : edge.getLabel());

        Vertex srouceNodeResource = GraphOperation.getResource(sourceVertex);
        if (srouceNodeResource != null) {
            csvRecord.add(String.valueOf(srouceNodeResource.getId()));
        } else {
            csvRecord.add("");
        }
        getCsvWriter().writeNext(csvRecord.toArray(new String[csvRecord.size()]));

        for (String key : edge.getPropertyKeys()) {
            if (!terchnicalAttributes.isEdgeAttributeTechnical(key)) {
                List<String> csvRecordEdge = new ArrayList<String>();
                csvRecordEdge.add(ItemTypes.EDGE_ATTRIBUTE.getText());
                csvRecordEdge.add(String.valueOf(edgeCounter));
                csvRecordEdge.add(key);
                csvRecordEdge.add(edge.getProperty(key).toString());
                getCsvWriter().writeNext(csvRecordEdge.toArray(new String[csvRecordEdge.size()]));
            }
        }

        edgeTypeCounts.addOccurance(edgeLabel);
        edgeCounter++;
    }

    /**
     * @return mapa exportovaných typů vrcholů a jejich počty 
     */
    public Map<VertexType, Integer> getVertexTypeCounts() {
        return vertexTypeCounts.getCounts();
    }

    /**
     * @return mapa exportovaných typů hran a jejich počty
     */
    public Map<EdgeLabel, Integer> getEdgeTypeCounts() {
        return edgeTypeCounts.getCounts();
    }
}