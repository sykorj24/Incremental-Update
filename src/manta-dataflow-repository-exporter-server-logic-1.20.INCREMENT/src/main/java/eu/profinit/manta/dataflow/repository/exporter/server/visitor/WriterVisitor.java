package eu.profinit.manta.dataflow.repository.exporter.server.visitor;

import java.io.Writer;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;

/**
 * Graph visitor, který potřebuje nastavit writer pro výstup.
 * @author tfechtner
 *
 */
public interface WriterVisitor extends GraphVisitor {
    /**
     * @param writer Writer, kam se vypisuje výstup visitoru.
     */
    void setWriter(Writer writer);
    
    /**
     * @param transaction transakce, ve které je writer vykonáván
     */
    void setTransaction(TitanTransaction transaction);
}
