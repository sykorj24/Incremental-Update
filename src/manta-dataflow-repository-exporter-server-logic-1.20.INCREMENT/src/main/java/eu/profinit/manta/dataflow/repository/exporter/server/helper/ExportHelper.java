package eu.profinit.manta.dataflow.repository.exporter.server.helper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphTraverser;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.exporter.server.visitor.GraphTraverserDfsPrefixResourceFirst;
import eu.profinit.manta.dataflow.repository.exporter.server.visitor.WriterVisitor;

/**
 * Základní helper pro export grafu.
 * @author tfechtner
 *
 */
public class ExportHelper {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(ExportHelper.class);
    /** Použité kodování. */
    private static final String ENCODING = "utf-8";

    @Autowired
    private ServletContext servletContext;

    /** Generátor náhodných čísel pro jméno souboru. */
    private Random rand = new Random();

    /**
     * Vytvoří soubor obsahující vyexportované objekty z grafu.
     * @param databaseHolder service pro databázi
     * @param superRootHandler držák super rootu grafu
     * @param visitor visitor použitý pro vyexporování grafu
     * @param licenseKey požadovaný licence klíč pro danou operaci
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return dočasný soubor s exportem
     */
    public File generateExportFile(DatabaseHolder databaseHolder, final SuperRootHandler superRootHandler,
            final WriterVisitor visitor, final String licenseKey, final RevisionInterval revisionInterval) {
        return generateExportFile(databaseHolder, superRootHandler, new GraphTraverserDfsPrefixResourceFirst(), visitor,
                licenseKey, revisionInterval);
    }

    /**
     * Vytvoří soubor obsahující vyexportované objekty z grafu.
     * @param databaseHolder service pro databázi
     * @param superRootHandler držák super rootu grafu
     * @param traverser traverser použitý pro traverzování grafem shora dolů
     * @param visitor visitor použitý pro vyexporování grafu
     * @param licenseKey požadovaný licence klíč pro danou operaci
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return dočasný soubor s exportem
     */
    public File generateExportFile(DatabaseHolder databaseHolder, final SuperRootHandler superRootHandler,
            final GraphTraverser traverser, final WriterVisitor visitor, final String licenseKey,
            final RevisionInterval revisionInterval) {

        return databaseHolder.runInTransaction(TransactionLevel.READ, new TransactionCallback<File>() {
            @Override
            public File callMe(TitanTransaction transaction) {
                File tempFile = new File(createTempFileName());
                PrintWriter writer = null;
                try {
                    writer = new PrintWriter(new BufferedWriter(new FileWriterWithEncoding(tempFile, ENCODING)));

                    Vertex superRoot = superRootHandler.getRoot(transaction);
                    visitor.setWriter(writer);
                    visitor.setTransaction(transaction);
                    traverser.traverse(visitor, superRoot, revisionInterval);
                } catch (IOException e) {
                    LOGGER.error("Cannot create the temporary file for export.", e);
                } finally {
                    IOUtils.closeQuietly(writer);
                }
                return tempFile;
            }

            @Override
            public String getModuleName() {
                return licenseKey;
            }
        });
    }

    /**
     * @return vytvoří název dočasného souboru pro export
     */
    public String createTempFileName() {
        String filePath = servletContext.getRealPath("WEB-INF/tempExportFile" + rand.nextInt() + ".csv");
        return filePath;
    }

    /**
     * Pošle soubor pomocí {@link HttpServletResponse} klientovi.
     * @param fileToSend soubor k odeslání
     * @param response response objekt
     */
    public void sendExportFile(File fileToSend, HttpServletResponse response) {
        response.setContentType("text/csv;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=\"export.csv\"");

        OutputStream out = null;
        try {
            out = response.getOutputStream();
        } catch (IOException e) {
            LOGGER.error("Cannot aquire response output stream.", e);
        }

        if (out != null) {
            BufferedWriter writer = null;
            BufferedReader reader = null;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(out, ENCODING));
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileToSend), ENCODING));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    writer.write(line);
                    writer.newLine();
                }

            } catch (FileNotFoundException e) {
                LOGGER.error("File, which should be sent, does not exist.", e);
            } catch (IOException e) {
                LOGGER.error("Error during sending file.", e);
            } finally {
                IOUtils.closeQuietly(reader);
                IOUtils.closeQuietly(writer);
            }
        }
    }

    /**
     * @param servletContext servlet kontext aplikace
     */
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
}
