package eu.profinit.manta.dataflow.repository.exporter.server.visitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterResult;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.EdgeTypeTraversing;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowItem;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowTraverser;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowTraverserDfs;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowVisitor;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Visitor pro nalezení typů sloupců. <br>
 * Pro sloupce ve view se hledá nejbližší sloupce z tabulek, z nichž vede do 
 * daného sloupce ve view přímý datový tok. <br>
 * Pro sloupce v tabulkách se bere typ těchto konkrétnách sloupců. 
 * @author tfechtner
 *
 */
public class ColumnTypeVisitor extends AbstractWriterVisitor {
    /** Jméno atributu pro znakovou sadou sloupce. */
    public static final String ATTR_COLUMN_CHARSET = "COLUMN_CHARSET";
    /** Jméno atributu pro délku datového typu sloupce. */
    public static final String ATTR_COLUMN_LENGTH = "COLUMN_LENGTH";
    /** Jméno atributu pro datový typ sloupce. */
    public static final String ATTR_COLUMN_TYPE = "COLUMN_TYPE";

    /** Typ uzlu značící sloupec. */
    private static final String NODE_TYPE_COLUMN = "Column";
    /** Jméno atributu pro typ tabulky. */
    private static final String TABLE_TYPE = "TABLE_TYPE";
    /** Hodnota typu tabulky pro view. */
    private static final String TABLE_TYPE_VIEW = "VIEW";
    /** Hodnota typu tabulky pro skutečnou tabulku. */
    private static final String TABLE_TYPE_TABLE = "TABLE";

    /** Traverser pro pohyb po datových tocích. */
    private final FlowTraverser flowTraverser = new FlowTraverserDfs();
    /** Transakce pro přístup k db. */
    private final TitanTransaction transaction;
    /** Interval revizí, pro které se pracuje. */
    private final RevisionInterval revisionInterval;

    /**
     * @param transaction Transakce pro přístup k db.
     * @param revisionInterval Interval revizí, pro které se pracuje.
     */
    public ColumnTypeVisitor(TitanTransaction transaction, RevisionInterval revisionInterval) {
        super();
        this.transaction = transaction;
        this.revisionInterval = revisionInterval;
    }

    @Override
    public void visitNode(Vertex node) {
        if (node.getProperty(NodeProperty.NODE_TYPE.t()).equals(NODE_TYPE_COLUMN)) {
            Object tableType = GraphOperation.getFirstNodeAttribute(GraphOperation.getParent(node), TABLE_TYPE,
                    revisionInterval);
            if (TABLE_TYPE_TABLE.equals(tableType)) {
                printColumn(node, node);
            } else if (TABLE_TYPE_VIEW.equals(tableType)) {
                MappedColumnsVisitor visitor = new MappedColumnsVisitor(revisionInterval);
                flowTraverser.traverse(visitor, transaction, Collections.singletonList(node.getId()),
                        EdgeTypeTraversing.DIRECT, Direction.IN, revisionInterval);

                Set<Vertex> mappings = visitor.getMappings();
                for (Vertex tableColumn : mappings) {
                    printColumn(tableColumn, node);
                }
            }
        }
    }

    /**
     * Zapíše sloupec s mapováním a datovým typem.
     * @param sourceColumn sloupec, z kterého jde datový tok
     * @param targetColumn sloupec, do kterého směřuje datový tok 
     */
    private void printColumn(Vertex sourceColumn, Vertex targetColumn) {
        Vertex sourceTable = GraphOperation.getParent(sourceColumn);
        Vertex sourceDb = GraphOperation.getParent(sourceTable);

        List<String> csvRecord = new ArrayList<String>();

        csvRecord.add(GraphOperation.getName(sourceDb));
        csvRecord.add(GraphOperation.getName(sourceTable));
        csvRecord.add(GraphOperation.getName(sourceColumn));

        Vertex targetTable = GraphOperation.getParent(targetColumn);
        Vertex targetDb = GraphOperation.getParent(targetTable);

        csvRecord.add(GraphOperation.getName(targetDb));
        csvRecord.add(GraphOperation.getName(targetTable));
        csvRecord.add(GraphOperation.getName(targetColumn));

        csvRecord.add(getAttributeValue(sourceColumn, ATTR_COLUMN_TYPE));
        csvRecord.add(getAttributeValue(sourceColumn, ATTR_COLUMN_LENGTH));
        csvRecord.add(getAttributeValue(sourceColumn, ATTR_COLUMN_CHARSET));

        getCsvWriter().writeNext(csvRecord.toArray(new String[csvRecord.size()]));
    }

    /**
     * Získá hodnotu atributu s daným klíčem pro daný vrchol.
     * @param vertex vrchol, jehož atribut se hledá
     * @param attrKey klíč atributu, který se hledá
     * @return hodnota atirbutu jako string nebo prázdný řetězec, když takový atribut vrchol nemá
     */
    protected String getAttributeValue(Vertex vertex, String attrKey) {
        Object attrValue = GraphOperation.getFirstNodeAttribute(vertex, attrKey, revisionInterval);
        if (attrValue != null) {
            return attrValue.toString();
        } else {
            return "";
        }
    }

    @Override
    public void visitAttribute(Vertex attribute) {
        // NOP
    }

    @Override
    public void visitLayer(Vertex layer) {
        // NOOP        
    }

    @Override
    public void visitResource(Vertex resource) {
        // NOP
    }

    @Override
    public void visitEdge(Edge edge) {
        // NOP
    }

    /**
     * Visitor hledající mapování pro daný sloupec.
     * @author tfechtner
     *
     */
    private static class MappedColumnsVisitor implements FlowVisitor {

        /** Seznam mapovaných sloupců. */
        private Set<Vertex> mappings = new HashSet<Vertex>();
        /** Interval revizí, pro které se pracuje. */
        private final RevisionInterval revisionInterval;

        /**
         * @param revisionInterval interval pro který se hledá mapování
         */
        MappedColumnsVisitor(RevisionInterval revisionInterval) {
            super();
            this.revisionInterval = revisionInterval;
        }

        @Override
        public FilterResult visitAndContinue(Vertex node, FlowItem flowItem) {
            Object tableType = GraphOperation.getFirstNodeAttribute(GraphOperation.getParent(node), TABLE_TYPE,
                    revisionInterval);
            if (TABLE_TYPE_TABLE.equals(tableType)) {
                mappings.add(node);
                return FilterResult.NOK_STOP;
            } else {
                return FilterResult.OK_CONTINUE;
            }
        }

        /**
         * @return Seznam mapovaných sloupců.
         */
        public Set<Vertex> getMappings() {
            return mappings;
        }
    }
}
