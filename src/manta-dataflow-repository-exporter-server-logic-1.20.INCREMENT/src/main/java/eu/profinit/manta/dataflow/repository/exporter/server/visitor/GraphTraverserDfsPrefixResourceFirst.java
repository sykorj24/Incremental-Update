package eu.profinit.manta.dataflow.repository.exporter.server.visitor;

import java.util.List;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphTraverserDfsPrefix;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Upravený graf traverser, který nejrpve projde vrstvy, pote resource, a nakonec vse ostatni. 
 * @author tfechtner
 *
 */
public class GraphTraverserDfsPrefixResourceFirst extends GraphTraverserDfsPrefix {

    @Override
    public void traverse(GraphVisitor visitor, Vertex superRoot, RevisionInterval revisionInterval) {
        List<Vertex> resourceList = GraphOperation.getAdjacentVertices(superRoot, Direction.IN, revisionInterval,
                EdgeLabel.HAS_RESOURCE);
        
        processLayers(visitor, resourceList, revisionInterval);

        for (Vertex resource : resourceList) {
            processResourceOnly(visitor, resource);
        }

        for (Vertex resource : resourceList) {
            processResource(visitor, resource, revisionInterval);
        }

        for (Vertex resource : resourceList) {
            processResourceEdges(visitor, resource, revisionInterval);
        }
    }

    /**
     * Zpracuje vrstvy obsahujici dane resource
     * @param visitor Navstevnik, ktery ma vrstvy navstivit
     * @param resourceList Resource, jejichz  vrstvy se maji zpracovat
     * @param revisionInterval Interval revizi pro pruchod grafem
     */
    private void processLayers(GraphVisitor visitor, List<Vertex> resourceList, RevisionInterval revisionInterval) {
        for (Vertex resource : resourceList) {
            processLayersOfResource(visitor, resource, revisionInterval);
        }
    }

    private void processResourceOnly(GraphVisitor visitor, Vertex resource) {
        visitor.visitResource(resource);        
    }

    @Override
    protected void processResource(GraphVisitor visitor, Vertex resource, RevisionInterval revisionInterval) {
        processResourceChildren(visitor, resource, revisionInterval);
    }
}
