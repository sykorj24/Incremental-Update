package eu.profinit.manta.dataflow.repository.exporter.server.visitor;

import java.io.PrintWriter;
import java.io.Writer;

import com.thinkaurelius.titan.core.TitanTransaction;

import au.com.bytecode.opencsv.CSVWriter;
import eu.profinit.manta.dataflow.repository.utils.CsvHelper;

/**
 * Společná implementace pro {@link WriterVisitor} implementující settery a gettery pro 
 * {@link PrintWriter} a {@link TitanTransaction} jako memeber field.
 * 
 * @author tfechtner
 *
 */
public abstract class AbstractWriterVisitor implements WriterVisitor {
    /** Writer, kam se vypisuje výstup visitoru. */
    private CSVWriter writer;
    /** Transakce, ve které visitor pracuje. */
    private TitanTransaction transaction;
    
    @Override
    public void setWriter(Writer writer) {
        this.writer = CsvHelper.getCSVWriter(writer);
    }
    
    /**
     * @return CSV writer, který provádí zapisování dat
     */
    protected CSVWriter getCsvWriter() {
        return writer;
    }

    @Override
    public void setTransaction(TitanTransaction transaction) {
        this.transaction = transaction;
    }

    /**
     * @return Transakce, ve které visitor pracuje.
     */
    public TitanTransaction getTransaction() {
        return transaction;
    }
    
    
}
