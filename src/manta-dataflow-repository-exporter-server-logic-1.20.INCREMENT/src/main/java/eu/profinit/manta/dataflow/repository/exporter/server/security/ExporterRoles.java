package eu.profinit.manta.dataflow.repository.exporter.server.security;

/**
 * Seznam rolí specifických pro modul exporter.
 * @author tfechtner
 *
 */
public final class ExporterRoles {
    
    private ExporterRoles() {        
    }
    
    /**
     * Role exporteru, který exportuje libovolná data.
     */
    public static final String EXPORTER = "ROLE_EXPORTER";
}
