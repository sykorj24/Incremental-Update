package eu.profinit.manta.dataflow.repository.merger.server.helper.merger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import au.com.bytecode.opencsv.CSVReader;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;

/**
 * Unit testy {@link FootprintCsvReader}.
 * 
 * @author ONouza
 *
 */
@RunWith(JMockit.class)
public class FootprintCsvReaderTest {

    @Mocked
    private CSVReader csvReader;

    private FootprintCsvReader footprintCsvReader;

    @Before
    public void setUp() {
        footprintCsvReader = new FootprintCsvReader(csvReader);
    }

    /**
     * Unit test {@link FootprintCsvReader#readNext()}
     * - bezny scenar
     * @throws IOException
     */
    @Test
    public void testReadNext() throws IOException {
        // Data
        final String[] line1 = {"1"};
        final String[] line2 = {"2"};
        final String[] line3 = {"3"};
        final String[] lineA = {"A"};
        final String[] lineB = {"B"};
        final String[] lineC = {"C"};
        
        // Chovani
        new Expectations() {{
            csvReader.readNext();
            times = 7;
            result = line1;
            result = line2;
            result = line3;
            result = lineA;
            result = lineB;
            result = lineC;
            result = null;
        }};
        
        // Volani
        List<String[]> result = new ArrayList<>();
        
        result.add(footprintCsvReader.readNext());
        result.add(footprintCsvReader.readNext());
        result.add(footprintCsvReader.readNext());
        
        footprintCsvReader.closeFootprint();
        
        result.add(footprintCsvReader.readNext());
        result.add(footprintCsvReader.readNext());
        result.add(footprintCsvReader.readNext());

        result.add(footprintCsvReader.readNext());
        result.add(footprintCsvReader.readNext());
        result.add(footprintCsvReader.readNext());

        result.add(footprintCsvReader.readNext());

        // Kontrola
        
        Assert.assertEquals("Pocet prectenych hodnot se lisi oproti ocekavani.", 10, result.size());
        
        Assert.assertEquals("Prectena hodnota na pozici 0 se lisi oproti ocekavani.", "1", result.get(0)[0]);
        Assert.assertEquals("Prectena hodnota na pozici 1 se lisi oproti ocekavani.", "2", result.get(1)[0]);
        Assert.assertEquals("Prectena hodnota na pozici 2 se lisi oproti ocekavani.", "3", result.get(2)[0]);
        
        Assert.assertEquals("Prectena hodnota na pozici 3 se lisi oproti ocekavani.", "1", result.get(3)[0]);
        Assert.assertEquals("Prectena hodnota na pozici 4 se lisi oproti ocekavani.", "2", result.get(4)[0]);
        Assert.assertEquals("Prectena hodnota na pozici 5 se lisi oproti ocekavani.", "3", result.get(5)[0]);
        
        Assert.assertEquals("Prectena hodnota na pozici 6 se lisi oproti ocekavani.", "A", result.get(6)[0]);
        Assert.assertEquals("Prectena hodnota na pozici 7 se lisi oproti ocekavani.", "B", result.get(7)[0]);
        Assert.assertEquals("Prectena hodnota na pozici 8 se lisi oproti ocekavani.", "C", result.get(8)[0]);

        Assert.assertNull("Prectena hodnota na pozici 9 ma byt null.", result.get(9));
    }

    /**
     * Unit test {@link FootprintCsvReader#readNext()}
     * - konec souboru ve fazi stopovani
     * @throws IOException
     */
    @Test
    public void testReadNextEndOfFile() throws IOException {
        // Data
        final String[] line1 = {"1"};
        final String[] line2 = {"2"};
        final String[] line3 = {"3"};
        
        // Chovani
        new Expectations() {{
            csvReader.readNext();
            times = 4;
            result = line1;
            result = line2;
            result = line3;
            result = null;
        }};
        
        // Volani
        
        List<String[]> result = new ArrayList<>();
        
        result.add(footprintCsvReader.readNext());
        result.add(footprintCsvReader.readNext());
        result.add(footprintCsvReader.readNext());
        result.add(footprintCsvReader.readNext());
        
        footprintCsvReader.closeFootprint();
        
        result.add(footprintCsvReader.readNext());
        result.add(footprintCsvReader.readNext());
        result.add(footprintCsvReader.readNext());
        result.add(footprintCsvReader.readNext());

        // Kontrola
        
        Assert.assertEquals("Pocet prectenych hodnot se lisi oproti ocekavani.", 8, result.size());
        
        Assert.assertEquals("Prectena hodnota na pozici 0 se lisi oproti ocekavani.", "1", result.get(0)[0]);
        Assert.assertEquals("Prectena hodnota na pozici 1 se lisi oproti ocekavani.", "2", result.get(1)[0]);
        Assert.assertEquals("Prectena hodnota na pozici 2 se lisi oproti ocekavani.", "3", result.get(2)[0]);
        
        Assert.assertNull("Prectena hodnota na pozici 3 ma byt null.", result.get(3));

        Assert.assertEquals("Prectena hodnota na pozici 4 se lisi oproti ocekavani.", "1", result.get(4)[0]);
        Assert.assertEquals("Prectena hodnota na pozici 5 se lisi oproti ocekavani.", "2", result.get(5)[0]);
        Assert.assertEquals("Prectena hodnota na pozici 6 se lisi oproti ocekavani.", "3", result.get(6)[0]);
        
        Assert.assertNull("Prectena hodnota na pozici 7 ma byt null.", result.get(7));
    }
}
