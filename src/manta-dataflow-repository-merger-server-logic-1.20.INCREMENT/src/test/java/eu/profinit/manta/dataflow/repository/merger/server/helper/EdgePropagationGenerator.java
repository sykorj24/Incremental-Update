package eu.profinit.manta.dataflow.repository.merger.server.helper;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;

import au.com.bytecode.opencsv.CSVWriter;

@Ignore
public class EdgePropagationGenerator {
    private int levels = 5;
    private int children = 10;
    private int edges = 200000;
    private AtomicInteger nodeIdHolder = new AtomicInteger(0);

    @Test
    public void generate() throws UnsupportedEncodingException, FileNotFoundException {
        CSVWriter nodesWriter = null;
        CSVWriter edgeWriter = null;

        try {
            nodesWriter = new CSVWriter(
                    new BufferedWriter(new OutputStreamWriter(new FileOutputStream("target/node.csv"), "utf-8")));
            edgeWriter = new CSVWriter(
                    new BufferedWriter(new OutputStreamWriter(new FileOutputStream("target/edge.csv"), "utf-8")));

            createNodes(null, 1, nodesWriter);
            createEdges(edgeWriter);

        } finally {
            IOUtils.closeQuietly(nodesWriter);
            IOUtils.closeQuietly(edgeWriter);
        }

    }

    private void createEdges(CSVWriter edgeWriter) {
        Random rand = new Random();
        for (int i = 0; i < edges; i++) {
            int start = rand.nextInt(nodeIdHolder.get()) + 1;
            int end = rand.nextInt(nodeIdHolder.get()) + 1;
            edgeWriter.writeNext(
                    new String[] { String.valueOf(i), String.valueOf(start), String.valueOf(end), "DIRECT", "0" });
        }
    }

    private void createNodes(Integer parentId, int level, CSVWriter nodesWriter) {
        if (level > levels) {
            return;
        }

        for (int i = 0; i < children; i++) {
            int nodeId = nodeIdHolder.incrementAndGet();

            String parentIdString;
            if (parentId != null) {
                parentIdString = String.valueOf(parentId);
            } else {
                parentIdString = "";
            }

            nodesWriter
                    .writeNext(new String[] { String.valueOf(nodeId), parentIdString, "node" + nodeId, "type", "0" });
            createNodes(nodeId, level + 1, nodesWriter);
        }
    }
}
