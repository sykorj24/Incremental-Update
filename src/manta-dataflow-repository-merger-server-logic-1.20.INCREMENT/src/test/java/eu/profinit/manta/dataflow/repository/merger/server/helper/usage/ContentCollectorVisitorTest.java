package eu.profinit.manta.dataflow.repository.merger.server.helper.usage;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.helper.usage.ContentCollectorVisitor.AttributeType;
import eu.profinit.manta.dataflow.repository.utils.NodeTypeDef;
import junit.framework.Assert;

public class ContentCollectorVisitorTest extends TestTitanDatabaseProvider {

    @Test
    public void testExecution() throws FileNotFoundException {
        createBasicGraph(false);
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Integer>() {
            @Override
            public Integer callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                GraphCreation.createResource(transaction, root, "Res2Name", "Res2Type", "bla", layer, REVISION_1_000000);
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();
                
                Edge edge = GraphCreation.createEdge(t1c1, t2c2, EdgeLabel.FILTER, REVISION_1_000000);
                edge.setProperty("test", "test");
                
                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);
                return null;
            }
        });

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Integer>() {
            @Override
            public Integer callMe(TitanTransaction transaction) {
                ContentCollectorHelper helper = new ContentCollectorHelper();
                Map<String, Object> result = helper.collectContent(getDatabaseHolder(), getSuperRootHandler(),
                        getTerchnicalAttributes(), REVISION_1_000000);

                Map<String, Integer> resourcesExpected = new HashMap<String, Integer>();
                resourcesExpected.put("Teradata", 1);
                resourcesExpected.put("Res2Type", 1);

                Map<NodeTypeDef, Integer> nodesExpected = new HashMap<NodeTypeDef, Integer>();
                nodesExpected.put(new NodeTypeDef("Column", "Teradata"), 4);
                nodesExpected.put(new NodeTypeDef("Table", "Teradata"), 2);
                nodesExpected.put(new NodeTypeDef("Database", "Teradata"), 1);

                Map<EdgeLabel, Integer> edgesExpected = new HashMap<EdgeLabel, Integer>();
                edgesExpected.put(EdgeLabel.DIRECT, 3);
                edgesExpected.put(EdgeLabel.FILTER, 2);

                Map<AttributeType, Integer> attrExpected = new HashMap<AttributeType, Integer>();
                attrExpected.put(AttributeType.VERTEX, 2);
                attrExpected.put(AttributeType.EDGE, 1);

                Assert.assertEquals(REVISION_1_000000, result.get("revision"));
                Assert.assertEquals(resourcesExpected, result.get("resources"));
                Assert.assertEquals(nodesExpected, result.get("nodes"));
                Assert.assertEquals(edgesExpected, result.get("edges"));
                Assert.assertEquals(attrExpected, result.get("attributes"));

                return null;
            }
        });

    }
}
