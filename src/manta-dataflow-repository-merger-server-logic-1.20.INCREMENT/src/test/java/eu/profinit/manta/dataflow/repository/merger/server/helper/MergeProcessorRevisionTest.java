package eu.profinit.manta.dataflow.repository.merger.server.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.Transformer;
import org.hamcrest.core.Every;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.common.collect.Iterables;
import com.thinkaurelius.titan.core.TitanTransaction;
import com.thinkaurelius.titan.core.attribute.Cmp;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeMapping;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.OneFileMerger;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.ProcessingResult;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.StandardMergerProcessor;
import eu.profinit.manta.platform.scriptmetadata.service.ScriptMetadataService;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;

/**
 * Test operace merge vzhledem k revizim.
 * 
 * @author pholecek
 * @author jsykora
 */
@RunWith(JMockit.class)
public class MergeProcessorRevisionTest extends TestTitanDatabaseProvider {

    // could not resolve null pointer exception when using mocking
    // need to initialize scriptMetadataService
    @Mocked
    private ScriptMetadataService scriptMetadataService = new ScriptMetadataService() {

        @Override
        public void storeScripts(Collection<String> arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public int revertUncommittedScripts() {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int countCommittedScripts(int arg0, Transformer<Collection<String>, Collection<String>> arg1) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int commitScripts(int arg0, int arg1, Transformer<Collection<String>, Collection<String>> arg2) {
            // TODO Auto-generated method stub
            return 0;
        }
    };

    /**
     * Test merger processor when performing both incremental update and full update.
     * <p> 
     * Incremental update has on the input a subgraph representing changes in the new revision (not the a version of the whole graph).
     * <p>
     * Full update has on the input a new version of the whole graph
     * 
     * @throws FileNotFoundException
     */
    @Test
    public void testMixedUpdatesMerging() throws FileNotFoundException {
        cleanGraph();

        StandardMergerProcessor mergerProcessor = new StandardMergerProcessor();
        mergerProcessor.setSuperRootHandler(getSuperRootHandler());
        mergerProcessor.setSourceRootHandler(getSourceRootHandler());
        mergerProcessor.setRevisionRootHandler(getRevisionRootHandler());

        OneFileMerger mergerHelper = new OneFileMerger(getDatabaseHolder(), getSuperRootHandler(),
                getRevisionRootHandler(), getSourceRootHandler(), mergerProcessor, getLicenseHolder(),
                scriptMetadataService, 2000, false);

        // ---------------------------------------------------------------------------------------
        // ------------------------------- REVISION 1.000000 START -------------------------------
        // ---------------------------------------------------------------------------------------

        // create revision 1.000000
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().createMajorRevision(transaction);
                return null;
            }
        });

        // merge into revision 1.000000 (full update)
        Map<String, Object> result = mergerHelper
                .process(new FileInputStream(new File("src/test/resources/0_fullUpdate.csv")), REVISION_1_000000);

        // commit revision 1.000000
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);
                return null;
            }
        });

        /*
         * Metadata repository after the first merge into revision 1.000000:
         * 
         * <1.000000, 1.999999>
         *      resource---------------->layer
         *         |
         * <1.000000, 1.999999>                 
         *        db
         *         |
         *         +-----------------------------------------------+
         *         |                                               |
         * <1.000000, 1.999999>   <1.000000, 1.999999>    <1.000000, 1.999999>   <1.000000, 1.999999>
         *       table1----------------->TABLE                   table2----------------->VIEW
         *         |                                               |
         *         +----------------------+                        +-------------------------+
         *         |                      |                        |                         |
         * <1.000000, 1.999999>  <1.000000, 1.999999>     <1.000000, 1.999999>      <1.000000, 1.999999>
         *       t1c1                    t1c2                     t2c1---+                  t2c2
         *       |  |                    A  |                     A  A   |                    A
         *       |  |<1.000000, 1.999999>|  | <1.000000, 1.999999>|  |   |<1.000000, 1.999999>|
         *       |  +--------direct------+  +-------direct--------+  |   +------direct--------+
         *       |                                                   |
         *       |                  <1.000000, 1.999999>             |
         *       +------------------------filter---------------------+
         *       
         *       
         */

        // check repository after the first merge
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex resource = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "resName").iterator().next();
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex table1Attribute = GraphOperation
                        .getAdjacentVertices(table1, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE)
                        .iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                Vertex table2Attribute = GraphOperation
                        .getAdjacentVertices(table2, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE)
                        .iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();

                Edge t1c1_direct_t1c2 = RevisionUtils
                        .getAdjacentEdges(t1c1, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();
                Edge t1c1_flow_t2c1 = RevisionUtils
                        .getAdjacentEdges(t1c1, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.FILTER.t()).iterator()
                        .next();
                Edge t1c2_direct_t2c1 = RevisionUtils
                        .getAdjacentEdges(t1c2, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();
                Edge t2c1_direct_t2c2 = RevisionUtils
                        .getAdjacentEdges(t2c1, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();

                checkVertexRevision(resource, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(db, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(table1, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(table1Attribute, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(t1c1, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(t1c2, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(table2, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(table2Attribute, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(t2c1, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(t2c2, REVISION_1_000000, REVISION_1_999999);

                checkEdgeRevision(t1c1_direct_t1c2, REVISION_1_000000, REVISION_1_999999);
                checkEdgeRevision(t1c1_flow_t2c1, REVISION_1_000000, REVISION_1_999999);
                checkEdgeRevision(t1c2_direct_t2c1, REVISION_1_000000, REVISION_1_999999);
                checkEdgeRevision(t2c1_direct_t2c2, REVISION_1_000000, REVISION_1_999999);

                return null;
            }
        });

        // ---------------------------------------------------------------------------------------
        // -------------------------------- REVISION 1.000000 END -------------------------------- 
        // ---------------------------------------------------------------------------------------

        // ---------------------------------------------------------------------------------------
        // ------------------------------- REVISION 1.000001 START -------------------------------
        // ---------------------------------------------------------------------------------------

        // create revision 1.000001
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().createMinorRevision(transaction);
                return null;
            }
        });

        // merge into revision 1.000001 (incremental update)
        // add table3 (VIEW), t3c1 and a direct flow from t2c2 to t3c1
        result = mergerHelper.process(new FileInputStream(new File("src/test/resources/1_incrementalUpdate.csv")),
                REVISION_1_000001);

        // commit revision 1.000001
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000001);
                return null;
            }
        });

        /*
         * Metadata repository after the second merge into revision 1.000001:
         * 
         * <1.000000, 1.999999>
         *      resource---------------->layer
         *         |
         * <1.000000, 1.999999>                 
         *        db
         *         |
         *         +-----------------------------------------------+----------------------------------------------------+
         *         |                                               |                                                    |
         * <1.000000, 1.999999>   <1.000000, 1.999999>    <1.000000, 1.999999>   <1.000000, 1.999999>          <1.000001, 1.999999>    <1.000001, 1.999999>
         *       table1----------------->TABLE                   table2----------------->VIEW                         table3----------------->VIEW
         *         |                                               |                                                    |
         *         +----------------------+                        +---------------------------+                        |
         *         |                      |                        |                           |                        |
         * <1.000000, 1.999999>  <1.000000, 1.999999>     <1.000000, 1.999999>        <1.000000, 1.999999>     <1.000001, 1.999999>
         *       t1c1                    t1c2                     t2c1---+                    t2c2                     t3c1
         *       |  |                    A  |                     A  A   |                    A  |                     A
         *       |  |<1.000000, 1.999999>|  |<1.000000, 1.999999> |  |   |<1.000000, 1.999999>|  |<1.000001, 1.999999> |
         *       |  +--------direct------+  +-------direct--------+  |   +------direct--------+  +--------direct-------+
         *       |                                                   |
         *       |                <1.000000, 1.999999>               |
         *       +------------------------filter---------------------+
         *       
         *       
         */

        // check repository after the second merge
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex resource = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "resName").iterator().next();
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex table1Attribute = GraphOperation
                        .getAdjacentVertices(table1, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE)
                        .iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                Vertex table2Attribute = GraphOperation
                        .getAdjacentVertices(table2, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE)
                        .iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();
                Vertex table3 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table3").iterator().next();
                Vertex table3Attribute = GraphOperation
                        .getAdjacentVertices(table3, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE)
                        .iterator().next();
                Vertex t3c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t3c1").iterator().next();

                Edge t1c1_direct_t1c2 = RevisionUtils
                        .getAdjacentEdges(t1c1, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();
                Edge t1c1_filter_t2c1 = RevisionUtils
                        .getAdjacentEdges(t1c1, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.FILTER.t()).iterator()
                        .next();
                Edge t1c2_direct_t2c1 = RevisionUtils
                        .getAdjacentEdges(t1c2, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();
                Edge t2c1_direct_t2c2 = RevisionUtils
                        .getAdjacentEdges(t2c1, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();
                Edge t2c2_direct_t3c1 = RevisionUtils
                        .getAdjacentEdges(t2c2, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();

                checkVertexRevision(resource, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(db, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(table1, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(table1Attribute, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(t1c1, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(t1c2, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(table2, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(table2Attribute, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(t2c1, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(t2c2, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(table3, REVISION_1_000001, REVISION_1_999999);
                checkVertexRevision(table3Attribute, REVISION_1_000001, REVISION_1_999999);
                checkVertexRevision(t3c1, REVISION_1_000001, REVISION_1_999999);

                checkEdgeRevision(t1c1_direct_t1c2, REVISION_1_000000, REVISION_1_999999);
                checkEdgeRevision(t1c1_filter_t2c1, REVISION_1_000000, REVISION_1_999999);
                checkEdgeRevision(t1c2_direct_t2c1, REVISION_1_000000, REVISION_1_999999);
                checkEdgeRevision(t2c1_direct_t2c2, REVISION_1_000000, REVISION_1_999999);
                checkEdgeRevision(t2c2_direct_t3c1, REVISION_1_000001, REVISION_1_999999);
                return null;
            }
        });

        // ---------------------------------------------------------------------------------------
        // -------------------------------- REVISION 1.000001 END -------------------------------- 
        // ---------------------------------------------------------------------------------------

        // ---------------------------------------------------------------------------------------
        // ------------------------------- REVISION 1.000002 START -------------------------------
        // ---------------------------------------------------------------------------------------

        // create revision 1.000002
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().createMinorRevision(transaction);
                return null;
            }
        });

        // merge into revision 1.000002 (incremental update)
        // add t3c2 into table3
        // add direct flow from t2c1 to t3c2
        // remove t3c1 from table3 (+ remove direct flow from t2c2 to t3c1)
        // remove t2c2 from table2 (incoming direct flows to t2c1 are removed as well --> this is expected side effect)
        result = mergerHelper.process(new FileInputStream(new File("src/test/resources/2_incrementalUpdate.csv")),
                REVISION_1_000002);

        // commit revision 1.000002
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000002);
                return null;
            }
        });

        /*
         * Metadata repository after the third merge into revision 1.000002:
         * 
         * <1.000000, 1.999999>
         *      resource---------------->layer
         *         |
         * <1.000000, 1.999999>                 
         *        db
         *         |
         *         +-----------------------------------------------+-----------------------------------------------------+
         *         |                                               |                                                     |
         * <1.000000, 1.999999>   <1.000000, 1.999999>    <1.000000, 1.999999>   <1.000000, 1.999999>           <1.000001, 1.999999>    <1.000001, 1.999999>
         *       table1----------------->TABLE                   table2----------------->VIEW                         table3----------------->VIEW
         *         |                                               |                                                     |
         *         +----------------------+                        +----------------------------+                        +-------------------------+
         *         |                      |                        |                            |                        |                         |
         * <1.000000, 1.999999>  <1.000000, 1.999999>     <1.000000, 1.999999>         <1.000000, 1.000001>     <1.000001, 1.000001>      <1.000002, 1.999999>
         *       t1c1                    t1c2                     t2c1---+                    t2c2                     t3c1                      t3c2
         *       |  |                    A  |                     A  A   |                    A  |                     A                           A
         *       |  |<1.000000, 1.999999>|  |<1.000000, 1.000001> |  |   |<1.000000, 1.000001>|  |<1.000001, 1.000001> |                           |
         *       |  +--------direct------+  +-------direct--------+  |   +------direct--------+  +--------direct-------+                           |
         *       |                                                   |   |                                                                         |
         *       |                <1.000000, 1.000001>               |   |                          <1.000002, 1.999999>                           |
         *       +------------------------filter---------------------+   +---------------------------------direct----------------------------------+
         *       
         *       
         */

        // check repository after the third merge
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex resource = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "resName").iterator().next();
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex table1Attribute = GraphOperation
                        .getAdjacentVertices(table1, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE)
                        .iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                Vertex table2Attribute = GraphOperation
                        .getAdjacentVertices(table2, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE)
                        .iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();
                Vertex table3 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table3").iterator().next();
                Vertex table3Attribute = GraphOperation
                        .getAdjacentVertices(table3, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE)
                        .iterator().next();
                Vertex t3c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t3c1").iterator().next();
                Vertex t3c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t3c2").iterator().next();

                Edge t1c1_direct_t1c2 = RevisionUtils
                        .getAdjacentEdges(t1c1, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();
                Edge t1c1_filter_t2c1 = RevisionUtils
                        .getAdjacentEdges(t1c1, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.FILTER.t()).iterator()
                        .next();
                Edge t1c2_direct_t2c1 = RevisionUtils
                        .getAdjacentEdges(t1c2, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();
                Edge t2c1_direct_t2c2 = RevisionUtils
                        .getAdjacentEdges(t2c2, Direction.IN, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();
                Edge t2c2_direct_t3c1 = RevisionUtils
                        .getAdjacentEdges(t2c2, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();
                Edge t2c1_direct_t3c2 = RevisionUtils
                        .getAdjacentEdges(t3c2, Direction.IN, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();

                checkVertexRevision(resource, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(db, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(table1, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(table1Attribute, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(t1c1, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(t1c2, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(table2, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(table2Attribute, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(t2c1, REVISION_1_000000, REVISION_1_999999);
                checkVertexRevision(t2c2, REVISION_1_000000, REVISION_1_000001);
                checkVertexRevision(table3, REVISION_1_000001, REVISION_1_999999);
                checkVertexRevision(table3Attribute, REVISION_1_000001, REVISION_1_999999);
                checkVertexRevision(t3c1, REVISION_1_000001, REVISION_1_000001);
                checkVertexRevision(t3c2, REVISION_1_000002, REVISION_1_999999);

                checkEdgeRevision(t1c1_direct_t1c2, REVISION_1_000000, REVISION_1_999999);
                checkEdgeRevision(t1c1_filter_t2c1, REVISION_1_000000, REVISION_1_000001);
                checkEdgeRevision(t1c2_direct_t2c1, REVISION_1_000000, REVISION_1_000001);
                checkEdgeRevision(t2c1_direct_t2c2, REVISION_1_000000, REVISION_1_000001);
                checkEdgeRevision(t2c2_direct_t3c1, REVISION_1_000001, REVISION_1_000001);
                checkEdgeRevision(t2c1_direct_t3c2, REVISION_1_000002, REVISION_1_999999);
                return null;
            }
        });

        // ---------------------------------------------------------------------------------------
        // -------------------------------- REVISION 1.000002 END -------------------------------- 
        // ---------------------------------------------------------------------------------------

        // ---------------------------------------------------------------------------------------
        // ------------------------------- REVISION 2.000000 START -------------------------------
        // ---------------------------------------------------------------------------------------

        // create revision 2.000000
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().createMajorRevision(transaction);
                return null;
            }
        });

        // merge into revision 2.000000 (full update)
        // add back the two missing flows t1c2_direct_t2c1 and t1c1_filter_t2c1 + the rest from the previous revision 1.000002
        result = mergerHelper.process(new FileInputStream(new File("src/test/resources/3_fullUpdate.csv")),
                REVISION_2_000000);

        // commit revision 2.000000
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().commitRevision(transaction, REVISION_2_000000);
                return null;
            }
        });

        /*
         * Metadata repository after the fourth merge into revision 2.000000
         * 
         * <1.000000, 2.999999>
         *      resource---------------->layer
         *         |
         * <1.000000, 2.999999>                 
         *        db
         *         |
         *         +-----------------------------------------------+-----------------------------------------------------+
         *         |                                               |                                                     |
         * <1.000000, 2.999999>   <1.000000, 2.999999>    <1.000000, 2.999999>   <1.000000, 2.999999>           <1.000001, 2.999999>    <1.000001, 2.999999>
         *       table1----------------->TABLE                   table2----------------->VIEW                         table3----------------->VIEW
         *         |                                               |                                                     |
         *         +----------------------+                        +----------------------------+                        +-------------------------+
         *         |                      |                        |                            |                        |                         |
         * <1.000000, 2.999999>  <1.000000, 2.999999>     <1.000000, 2.999999>         <1.000000, 1.000001>     <1.000001, 1.000001>      <1.000002, 2.999999>
         *       t1c1                    t1c2                     t2c1---+                    t2c2                     t3c1                      t3c2
         *       |  |                    A  |                     A  A   |                    A  |                     A                           A
         *       |  |<1.000000, 2.999999>|  |<1.000000, 1.000001> |  |   |<1.000000, 1.000001>|  |<1.000001, 1.000001> |                           |
         *       |  +--------direct------+  +-------direct--------+  |   +------direct--------+  +--------direct-------+                           |
         *       |                          |                     |  |   |                                                                         |
         *       |                          |<2.000000, 2.999999> |  |   |                          <1.000002, 2.999999>                           |
         *       |                          +-------direct--------+  |   +---------------------------------direct----------------------------------+
         *       |                                                   |
         *       |                <1.000000, 1.000001>               |   
         *       +------------------------filter---------------------+   
         *       |                                                   |
         *       |                <2.000000, 2.999999>               |
         *       +------------------------filter---------------------+
         *       
         */

        // check repository after the third merge
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex resource = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "resName").iterator().next();
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                Vertex table1Attribute = GraphOperation
                        .getAdjacentVertices(table1, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE)
                        .iterator().next();
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();
                Vertex table2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table2").iterator().next();
                Vertex table2Attribute = GraphOperation
                        .getAdjacentVertices(table2, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE)
                        .iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();
                Vertex table3 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table3").iterator().next();
                Vertex table3Attribute = GraphOperation
                        .getAdjacentVertices(table3, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.HAS_ATTRIBUTE)
                        .iterator().next();
                Vertex t3c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t3c1").iterator().next();
                Vertex t3c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t3c2").iterator().next();

                Edge t1c1_direct_t1c2 = RevisionUtils
                        .getAdjacentEdges(t1c1, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();
                Iterable<Edge> t1c1_filterEdges = RevisionUtils.getAdjacentEdges(t1c1, Direction.OUT,
                        EVERY_REVISION_INTERVAL, EdgeLabel.FILTER.t());
                Iterable<Edge> t1c2_directEdges = RevisionUtils.getAdjacentEdges(t1c2, Direction.OUT,
                        EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t());
                Edge t2c1_direct_t2c2 = RevisionUtils
                        .getAdjacentEdges(t2c2, Direction.IN, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();
                Edge t2c2_direct_t3c1 = RevisionUtils
                        .getAdjacentEdges(t2c2, Direction.OUT, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();
                Edge t2c1_direct_t3c2 = RevisionUtils
                        .getAdjacentEdges(t3c2, Direction.IN, EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT.t()).iterator()
                        .next();

                checkVertexRevision(resource, REVISION_1_000000, REVISION_2_999999);
                checkVertexRevision(db, REVISION_1_000000, REVISION_2_999999);
                checkVertexRevision(table1, REVISION_1_000000, REVISION_2_999999);
                checkVertexRevision(table1Attribute, REVISION_1_000000, REVISION_2_999999);
                checkVertexRevision(t1c1, REVISION_1_000000, REVISION_2_999999);
                checkVertexRevision(t1c2, REVISION_1_000000, REVISION_2_999999);
                checkVertexRevision(table2, REVISION_1_000000, REVISION_2_999999);
                checkVertexRevision(table2Attribute, REVISION_1_000000, REVISION_2_999999);
                checkVertexRevision(t2c1, REVISION_1_000000, REVISION_2_999999);
                checkVertexRevision(t2c2, REVISION_1_000000, REVISION_1_000001);
                checkVertexRevision(table3, REVISION_1_000001, REVISION_2_999999);
                checkVertexRevision(table3Attribute, REVISION_1_000001, REVISION_2_999999);
                checkVertexRevision(t3c1, REVISION_1_000001, REVISION_1_000001);
                checkVertexRevision(t3c2, REVISION_1_000002, REVISION_2_999999);

                checkEdgeRevision(t1c1_direct_t1c2, REVISION_1_000000, REVISION_2_999999);
                Assert.assertEquals(2, Iterables.size(t1c1_filterEdges));
                for (Edge filterEdge : t1c1_filterEdges) {
                    if (filterEdge.getProperty(EdgeProperty.TRAN_START.t()).equals(REVISION_1_000000)) {
                        // old filter edge
                        checkEdgeRevision(filterEdge, REVISION_1_000000, REVISION_1_000001);
                    } else {
                        // new filter edge
                        checkEdgeRevision(filterEdge, REVISION_2_000000, REVISION_2_999999);
                    }
                }
                Assert.assertEquals(2, Iterables.size(t1c2_directEdges));
                for (Edge directEdge : t1c2_directEdges) {
                    if (directEdge.getProperty(EdgeProperty.TRAN_START.t()).equals(REVISION_1_000000)) {
                        // old filter edge
                        checkEdgeRevision(directEdge, REVISION_1_000000, REVISION_1_000001);
                    } else {
                        // new filter edge
                        checkEdgeRevision(directEdge, REVISION_2_000000, REVISION_2_999999);
                    }
                }
                checkEdgeRevision(t2c1_direct_t2c2, REVISION_1_000000, REVISION_1_000001);
                checkEdgeRevision(t2c2_direct_t3c1, REVISION_1_000001, REVISION_1_000001);
                checkEdgeRevision(t2c1_direct_t3c2, REVISION_1_000002, REVISION_2_999999);
                return null;
            }
        });

        // ---------------------------------------------------------------------------------------
        // -------------------------------- REVISION 2.000000 END -------------------------------- 
        // ---------------------------------------------------------------------------------------
    }

    private void checkVertexRevision(Vertex vertex, Double startRevision, Double endRevision) {
        Edge controlEdge = GraphOperation.getControlEdge(vertex);
        Assert.assertEquals(startRevision, controlEdge.getProperty(EdgeProperty.TRAN_START.t()));
        Assert.assertEquals(endRevision, controlEdge.getProperty(EdgeProperty.TRAN_END.t()));
    }

    private void checkEdgeRevision(Edge edge, Double startRevision, Double endRevision) {
        Assert.assertEquals(startRevision, edge.getProperty(EdgeProperty.TRAN_START.t()));
        Assert.assertEquals(endRevision, edge.getProperty(EdgeProperty.TRAN_END.t()));
    }

    /**
     * Original test only for full updates (major revisions, on the input whole new graph structure)
     * 
     * @throws FileNotFoundException
     */
    @Test
    public void testProcess() throws FileNotFoundException {
        cleanGraph();

        StandardMergerProcessor mergerProcessor = new StandardMergerProcessor();
        mergerProcessor.setSuperRootHandler(getSuperRootHandler());
        mergerProcessor.setSourceRootHandler(getSourceRootHandler());
        mergerProcessor.setRevisionRootHandler(getRevisionRootHandler());

        OneFileMerger mergerHelper = new OneFileMerger(getDatabaseHolder(), getSuperRootHandler(),
                getRevisionRootHandler(), getSourceRootHandler(), mergerProcessor, getLicenseHolder(),
                scriptMetadataService, 2000, false);

        // create revision 1.000000
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().createMajorRevision(transaction);
                return null;
            }
        });

        // check, whether it is possible to merge into revision 1.000000
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().checkMergeConditions(transaction, REVISION_1_000000);
                return null;
            }
        });

        // 5 nodes: super root, revision root, source root revision node 0.000000 and revision node 1.000000
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(INIT_GRAPH_VERTEX_COUNT + 1, getVertexCount(transaction));
                return null;
            }
        });

        // merge into revision 1.000000 (full update)
        Map<String, Object> result = mergerHelper
                .process(new FileInputStream(new File("src/test/resources/revision_1.csv")), REVISION_1_000000);

        testCommonObjects(result);
        testRevision1();

        // commit revision 1.000000
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);
                return null;
            }
        });

        // check number of vertices
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(RevisionTestConstants.REV_1_VERTEX_COUNT, getVertexCount(transaction));
                return null;
            }
        });

        // create revision 2.000000
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().createMajorRevision(transaction);
                return null;
            }
        });

        // check number of vertices (one new revision node)
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(RevisionTestConstants.REV_2_VERTEX_COUNT, getVertexCount(transaction));
                return null;
            }
        });

        // merge into revision 2.000000 (full update)
        mergerHelper.process(new FileInputStream(new File("src/test/resources/revision_2.csv")), REVISION_2_000000);

        testRevision2();

        // commit revision 2.000000
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().commitRevision(transaction, REVISION_2_000000);
                return null;
            }
        });

        // check number of vertices
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(RevisionTestConstants.REV_2_VERTEX_COUNT, getVertexCount(transaction));
                return null;
            }
        });

        // create revision 3.000000
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().createMajorRevision(transaction);
                return null;
            }
        });

        // merge into revision 3.000000
        Map<String, Object> result3 = mergerHelper
                .process(new FileInputStream(new File("src/test/resources/revision_3.csv")), REVISION_3_000000);

        testCommonObjects(result3);
        testRevision3();

        // commit revision 3.000000
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().commitRevision(transaction, REVISION_3_000000);
                return null;
            }
        });

        // check number of vertices
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(RevisionTestConstants.REV_3_VERTEX_COUNT, getVertexCount(transaction));
                return null;
            }
        });

    }

    /**
     * Zkontroluje, zda existuji cast objektu spolecna pro vsechny revize.
     * 
     * @param mergerResult 
     */
    public void testCommonObjects(final Map<String, Object> mergerResult) {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex oracleRes = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Oracle").iterator().next();
                Vertex oraclePlSqlRes = transaction.getVertices(NodeProperty.RESOURCE_NAME.t(), "Oracle PL/SQL")
                        .iterator().next();
                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "Oracle DB").iterator().next();
                Vertex A_SQL = transaction.getVertices(NodeProperty.NODE_NAME.t(), "A.SQL").iterator().next();
                Vertex a_sql = transaction.getVertices(NodeProperty.NODE_NAME.t(), "a.sql").iterator().next();
                Vertex tabSource = transaction.getVertices(NodeProperty.NODE_NAME.t(), "TAB_SOURCE").iterator().next();
                Vertex tabDest = transaction.getVertices(NodeProperty.NODE_NAME.t(), "TAB_DEST").iterator().next();
                Vertex insert = transaction.getVertices(NodeProperty.NODE_NAME.t(), "<1,1>INSERT").iterator().next();
                Vertex a = transaction.getVertices(NodeProperty.NODE_NAME.t(), "A").iterator().next();
                Vertex x = transaction.getVertices(NodeProperty.NODE_NAME.t(), "X").iterator().next();
                Vertex xa = transaction.getVertices(NodeProperty.NODE_NAME.t(), "XA").iterator().next();
                transaction.getVertices(NodeProperty.ATTRIBUTE_NAME.t(), "sourceLocation").iterator().next();
                transaction.getVertices(NodeProperty.ATTRIBUTE_NAME.t(), "sourceEncoding").iterator().next();
                transaction.getVertices(NodeProperty.NODE_NAME.t(), "X").iterator().next()
                        .getEdges(Direction.OUT, EdgeLabel.DIRECT.t()).iterator().next();
                transaction.getVertices(NodeProperty.NODE_NAME.t(), "A").iterator().next()
                        .getEdges(Direction.IN, EdgeLabel.DIRECT.t()).iterator().next();

                Assert.assertEquals("Oracle Database",
                        GraphOperation.getResource(oracleRes).getProperty(NodeProperty.RESOURCE_DESCRIPTION.t()));
                Assert.assertEquals("Oracle PL/SQL scripts",
                        GraphOperation.getResource(oraclePlSqlRes).getProperty(NodeProperty.RESOURCE_DESCRIPTION.t()));
                Assert.assertEquals("Oracle",
                        GraphOperation.getResource(db).getProperty(NodeProperty.RESOURCE_NAME.t()));
                Assert.assertEquals("Oracle Database",
                        GraphOperation.getResource(db).getProperty(NodeProperty.RESOURCE_DESCRIPTION.t()));
                Assert.assertEquals("Oracle",
                        GraphOperation.getResource(A_SQL).getProperty(NodeProperty.RESOURCE_NAME.t()));
                Assert.assertEquals("Oracle",
                        GraphOperation.getResource(tabSource).getProperty(NodeProperty.RESOURCE_NAME.t()));
                Assert.assertEquals("Oracle",
                        GraphOperation.getResource(tabDest).getProperty(NodeProperty.RESOURCE_NAME.t()));
                Assert.assertEquals("Oracle PL/SQL",
                        GraphOperation.getResource(insert).getProperty(NodeProperty.RESOURCE_NAME.t()));
                Assert.assertEquals("Oracle",
                        GraphOperation.getResource(a).getProperty(NodeProperty.RESOURCE_NAME.t()));
                Assert.assertEquals("Oracle",
                        GraphOperation.getResource(x).getProperty(NodeProperty.RESOURCE_NAME.t()));
                Assert.assertEquals("Oracle PL/SQL",
                        GraphOperation.getResource(xa).getProperty(NodeProperty.RESOURCE_NAME.t()));

                Map<String, List<Object>> attributes = GraphOperation.getAllNodeAttributes(a_sql,
                        REVISION_INTERVAL_1_000000_1_000000);
                ProcessingResult processingResult = (ProcessingResult) mergerResult.get("processingReport");
                Set<SourceCodeMapping> requestedSourceCodes = processingResult.getRequestedSourceCodes();
                if (!requestedSourceCodes.isEmpty()) {
                    Assert.assertEquals(requestedSourceCodes.iterator().next().getDbId(),
                            attributes.get("sourceLocation").get(0));
                }
                Assert.assertEquals("utf8", attributes.get("sourceEncoding").get(0));

                return null;
            }
        });
    }

    /**
     * Test, zda existuji data specificka pro danou revizi.
     */
    public void testRevision1() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(RevisionTestConstants.REV_1_VERTEX_COUNT, getVertexCount(transaction));
                transaction.getVertices(NodeProperty.NODE_NAME.t(), "C").iterator().next();
                transaction.getVertices(NodeProperty.NODE_NAME.t(), "Z").iterator().next();
                transaction.getVertices(NodeProperty.NODE_NAME.t(), "ZC").iterator().next();
                return null;
            }
        });
    }

    /**
     * Test, zda existuji data specificka pro danou revizi.
     */
    public void testRevision2() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(RevisionTestConstants.REV_2_VERTEX_COUNT, getVertexCount(transaction));
                Vertex tabSource = transaction.getVertices(NodeProperty.NODE_NAME.t(), "TAB_SOURCE").iterator().next();
                Vertex tabDest = transaction.getVertices(NodeProperty.NODE_NAME.t(), "TAB_DEST").iterator().next();
                Vertex insert = transaction.getVertices(NodeProperty.NODE_NAME.t(), "<1,1>INSERT").iterator().next();

                /*
                 * Kontrola, jestli ma tabulka DEST_TAB v revizi 2 pouze sloupce A a B
                 */
                Iterator<Vertex> columnIter = tabDest.query().direction(Direction.IN).labels(EdgeLabel.HAS_PARENT.t())
                        .has(EdgeProperty.TRAN_START.t(), Cmp.LESS_THAN_EQUAL, REVISION_2_000000)
                        .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, REVISION_2_000000).vertices()
                        .iterator();
                Assert.assertTrue(columnIter.hasNext());

                while (columnIter.hasNext()) {
                    Vertex v = columnIter.next();
                    boolean nameBool = v.getProperty(NodeProperty.NODE_NAME.t()).equals("A")
                            || v.getProperty(NodeProperty.NODE_NAME.t()).equals("B");
                    Assert.assertTrue(nameBool);
                }

                /*
                 * Kontrola, jestli ma tabulka SOURCE_TAB v revizi 2 pouze sloupce X a Y
                 */
                columnIter = tabSource.query().direction(Direction.IN).labels(EdgeLabel.HAS_PARENT.t())
                        .has(EdgeProperty.TRAN_START.t(), Cmp.LESS_THAN_EQUAL, REVISION_2_000000)
                        .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, REVISION_2_000000).vertices()
                        .iterator();
                Assert.assertTrue(columnIter.hasNext());

                while (columnIter.hasNext()) {
                    Vertex v = columnIter.next();
                    boolean nameBool = v.getProperty(NodeProperty.NODE_NAME.t()).equals("X")
                            || v.getProperty(NodeProperty.NODE_NAME.t()).equals("Y");
                    Assert.assertTrue(nameBool);
                }

                /*
                 * Kontrola, jestli ma INSERT v revizi 2 poze sloupce XA a YB
                 */
                columnIter = insert.query().direction(Direction.IN).labels(EdgeLabel.HAS_PARENT.t())
                        .has(EdgeProperty.TRAN_START.t(), Cmp.LESS_THAN_EQUAL, REVISION_2_000000)
                        .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, REVISION_2_000000).vertices()
                        .iterator();
                Assert.assertTrue(columnIter.hasNext());

                while (columnIter.hasNext()) {
                    Vertex v = columnIter.next();
                    boolean nameBool = v.getProperty(NodeProperty.NODE_NAME.t()).equals("XA")
                            || v.getProperty(NodeProperty.NODE_NAME.t()).equals("YB");
                    Assert.assertTrue(nameBool);
                }

                return null;
            }
        });
    }

    /**
     * Test, zda existuji data specificka pro danou revizi
     */
    public void testRevision3() {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(RevisionTestConstants.REV_3_VERTEX_COUNT, getVertexCount(transaction));
                Vertex tabSource = transaction.getVertices(NodeProperty.NODE_NAME.t(), "TAB_SOURCE").iterator().next();
                Vertex tabDest = transaction.getVertices(NodeProperty.NODE_NAME.t(), "TAB_DEST").iterator().next();
                Vertex insert = transaction.getVertices(NodeProperty.NODE_NAME.t(), "<1,1>INSERT").iterator().next();

                /*
                 * Kontrola, jestli ma tabulka DEST_TAB v revizi 3 sloupce A a B a D
                 */
                Iterator<Vertex> columnIter = tabDest.query().direction(Direction.IN).labels(EdgeLabel.HAS_PARENT.t())
                        .has(EdgeProperty.TRAN_START.t(), Cmp.LESS_THAN_EQUAL, REVISION_3_000000)
                        .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, REVISION_3_000000).vertices()
                        .iterator();
                Assert.assertTrue(columnIter.hasNext());

                while (columnIter.hasNext()) {
                    Vertex v = columnIter.next();
                    boolean nameBool = v.getProperty(NodeProperty.NODE_NAME.t()).equals("A")
                            || v.getProperty(NodeProperty.NODE_NAME.t()).equals("B")
                            || v.getProperty(NodeProperty.NODE_NAME.t()).equals("D");
                    Assert.assertTrue(nameBool);
                }

                /*
                 * Kontrola, jestli ma tabulka SOURCE_TAB v revizi 3 pouze sloupce X a Y a W
                 */
                columnIter = tabSource.query().direction(Direction.IN).labels(EdgeLabel.HAS_PARENT.t())
                        .has(EdgeProperty.TRAN_START.t(), Cmp.LESS_THAN_EQUAL, REVISION_3_000000)
                        .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, REVISION_3_000000).vertices()
                        .iterator();
                Assert.assertTrue(columnIter.hasNext());

                while (columnIter.hasNext()) {
                    Vertex v = columnIter.next();
                    boolean nameBool = v.getProperty(NodeProperty.NODE_NAME.t()).equals("X")
                            || v.getProperty(NodeProperty.NODE_NAME.t()).equals("Y")
                            || v.getProperty(NodeProperty.NODE_NAME.t()).equals("W");
                    Assert.assertTrue(nameBool);
                }

                /*
                 * Kontrola, jestli ma INSERT v revizi 3 pouze sloupce XA a YB a WD
                 */
                columnIter = insert.query().direction(Direction.IN).labels(EdgeLabel.HAS_PARENT.t())
                        .has(EdgeProperty.TRAN_START.t(), Cmp.LESS_THAN_EQUAL, REVISION_3_000000)
                        .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, REVISION_3_000000).vertices()
                        .iterator();
                Assert.assertTrue(columnIter.hasNext());

                while (columnIter.hasNext()) {
                    Vertex v = columnIter.next();
                    boolean nameBool = v.getProperty(NodeProperty.NODE_NAME.t()).equals("XA")
                            || v.getProperty(NodeProperty.NODE_NAME.t()).equals("YB")
                            || v.getProperty(NodeProperty.NODE_NAME.t()).equals("WD");
                    Assert.assertTrue(nameBool);
                }

                return null;
            }
        });
    }

}