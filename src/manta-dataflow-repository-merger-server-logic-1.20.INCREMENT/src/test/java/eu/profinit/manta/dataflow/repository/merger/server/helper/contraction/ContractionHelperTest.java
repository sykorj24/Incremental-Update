package eu.profinit.manta.dataflow.repository.merger.server.helper.contraction;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.Transformer;
import org.apache.commons.configuration.BaseConfiguration;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.connection.DatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.connection.SimpleDatabaseHolder;
import eu.profinit.manta.dataflow.repository.connector.titan.service.ExecutorServiceTerminator;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseConfiguration;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.OneFileMerger;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.StandardMergerProcessor;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.ProcessorResult.ActionCounter;
import eu.profinit.manta.platform.licensing.LicenseHolder;
import eu.profinit.manta.platform.licensing.LicenseLoaderImpl;
import eu.profinit.manta.platform.scriptmetadata.service.ScriptMetadataService;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class ContractionHelperTest extends TestTitanDatabaseProvider {

    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ContractionHelperTest.class);

    // could not resolve null pointer exception when using mocking
    // need to initialize scriptMetadataService
    @Mocked
    private ScriptMetadataService scriptMetadataService = new ScriptMetadataService() {

        @Override
        public void storeScripts(Collection<String> arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public int revertUncommittedScripts() {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int countCommittedScripts(int arg0, Transformer<Collection<String>, Collection<String>> arg1) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int commitScripts(int arg0, int arg1, Transformer<Collection<String>, Collection<String>> arg2) {
            // TODO Auto-generated method stub
            return 0;
        }
    };

    @Test
    public void testNoContraction() throws Exception {
        cleanGraph();
        createBasicGraph(false);
        ContractionResult result = getHelper().performContractionInner(REVISION_1_000000);
        Assert.assertTrue(result.getVertexMap().isEmpty());
        Assert.assertTrue(result.getEdgeMap().isEmpty());
    }

    @Test
    public void testContraction() throws Exception {
        cleanGraph();
        final double revision = createMajorRevision();

        final List<Vertex> allVerticesBefore = new ArrayList<>();
        final List<Edge> allEdgesBefore = new ArrayList<>();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex resource1 = GraphCreation.createResource(transaction, root, "Teradata DB", "Teradata", "desc",
                        layer, revision);
                Vertex r1db1 = GraphCreation.createNode(transaction, resource1, "db1", "Database", revision);
                Vertex r1db1t1 = GraphCreation.createNode(transaction, r1db1, "table1", "Table", revision);
                Vertex r1db1t1c1 = GraphCreation.createNode(transaction, r1db1t1, "t1c1", "Column", revision);
                Vertex r1db1t2 = GraphCreation.createNode(transaction, r1db1, "table2", "Table", revision);
                Vertex r1db1t2c1 = GraphCreation.createNode(transaction, r1db1t2, "t2c1", "Column", revision);
                Vertex r1db1t2c2 = GraphCreation.createNode(transaction, r1db1t2, "t2c2", "Column", revision);

                Vertex resource2 = GraphCreation.createResource(transaction, root, "Teradata DB", "Teradata", "desc",
                        layer, revision);
                Vertex r2db1 = GraphCreation.createNode(transaction, resource2, "db1", "Database", revision);
                Vertex r2db1t2 = GraphCreation.createNode(transaction, r2db1, "table2", "Table", revision);
                Vertex r2db1t2c1 = GraphCreation.createNode(transaction, r2db1t2, "t2c1", "Column", revision);
                Vertex r2db1t2c2 = GraphCreation.createNode(transaction, r2db1t2, "t2c2", "Column", revision);

                Vertex r2db2 = GraphCreation.createNode(transaction, resource2, "db2", "Database", revision);
                Vertex r2db2t3 = GraphCreation.createNode(transaction, r2db2, "table3", "Table", revision);
                Vertex r2db2t3c1 = GraphCreation.createNode(transaction, r2db2t3, "t3c1", "Column", revision);

                GraphCreation.createNodeAttribute(transaction, r1db1t2, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r2db1t2, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r2db1t2, "ba", "v2", revision);

                GraphCreation.createEdge(r1db1t1c1, r1db1t2c1, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(r1db1t1c1, r1db1t2c1, EdgeLabel.FILTER, revision);

                GraphCreation.createEdge(r1db1t2c1, r1db1t2c2, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(r2db1t2c1, r2db1t2c2, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(r2db1t2c2, r2db2t3c1, EdgeLabel.DIRECT, revision);

                Edge edge1 = GraphCreation.createEdge(r1db1t2c2, r1db1t1c1, EdgeLabel.FILTER, revision);
                edge1.setProperty("a1", "a1v1");
                edge1.setProperty("a2", "a2v1");
                Edge edge2 = GraphCreation.createEdge(r2db1t2c2, r1db1t1c1, EdgeLabel.FILTER, revision);
                edge2.setProperty("a1", "a1v1");
                edge2.setProperty("a2", "a2v2");
                edge2.setProperty("a3", "a3v1");

                // save all vertices
                for (Vertex vertex : transaction.getVertices()) {
                    allVerticesBefore.add(vertex);
                }

                // save all flow edges
                for (Edge edge : transaction.getEdges()) {
                    if (edge.getLabel().equals(EdgeLabel.DIRECT.t()) || edge.getLabel().equals(EdgeLabel.FILTER.t())) {
                        allEdgesBefore.add(edge);
                    }
                }

                return null;
            }
        });

        // perform contraction
        ContractionResult result = getHelper().performContractionInner(revision);

        Map<VertexType, ActionCounter> vertices = result.getVertexMap();
        Assert.assertEquals(3, vertices.size());
        Assert.assertEquals(1, vertices.get(VertexType.RESOURCE).getCount());
        Assert.assertEquals(4, vertices.get(VertexType.NODE).getCount());
        Assert.assertEquals(1, vertices.get(VertexType.ATTRIBUTE).getCount());

        Map<EdgeLabel, ActionCounter> edgeMap = result.getEdgeMap();
        Assert.assertEquals(2, edgeMap.size());
        Assert.assertEquals(1, edgeMap.get(EdgeLabel.DIRECT).getCount());
        Assert.assertEquals(1, edgeMap.get(EdgeLabel.FILTER).getCount());

        // Original check - checking for correct structure after contraction
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                RevisionInterval revInterval = new RevisionInterval(revision, revision);

                Assert.assertEquals(18, getVertexCount(transaction));

                Vertex root = getSuperRootHandler().getRoot(transaction);

                List<Vertex> resources = GraphOperation.getDirectChildren(root, revInterval);
                Assert.assertEquals(1, resources.size());
                Vertex resource = resources.get(0);
                Assert.assertEquals("Teradata DB", GraphOperation.getName(resource));

                List<Vertex> databases = GraphOperation.getDirectChildren(resource, revInterval);
                Assert.assertEquals(2, databases.size());
                Vertex db1 = databases.get(0);
                Vertex db2 = databases.get(1);
                Assert.assertEquals("db1", GraphOperation.getName(db1));
                Assert.assertEquals("db2", GraphOperation.getName(db2));

                List<Vertex> db1tables = GraphOperation.getDirectChildren(db1, revInterval);
                Assert.assertEquals(2, db1tables.size());
                Vertex db1t1 = db1tables.get(0);
                Vertex db1t2 = db1tables.get(1);
                Assert.assertEquals("table1", GraphOperation.getName(db1t1));
                Assert.assertEquals("table2", GraphOperation.getName(db1t2));

                List<Vertex> db1t1columns = GraphOperation.getDirectChildren(db1t1, revInterval);
                Assert.assertEquals(1, db1t1columns.size());
                List<Vertex> db1t2columns = GraphOperation.getDirectChildren(db1t2, revInterval);
                Assert.assertEquals(2, db1t2columns.size());

                List<Object> db1t2attrs = GraphOperation.getNodeAttribute(db1t2, "ba", revInterval);
                List<String> attrs = new ArrayList<>();
                attrs.add("v1");
                attrs.add("v2");
                Assert.assertEquals(attrs, db1t2attrs);

                Vertex t1c1 = db1t1columns.get(0);
                List<Edge> edges = GraphOperation.getAdjacentEdges(t1c1, Direction.IN, revInterval, EdgeLabel.FILTER);
                Assert.assertEquals(1, edges.size());
                Edge filterEdge = edges.get(0);
                Assert.assertEquals(7, filterEdge.getPropertyKeys().size()); // 2 revize + target + interpolated + 3 custom
                Assert.assertEquals("a1v1", filterEdge.getProperty("a1"));
                // property value for the property key "a2" is undefined
                Assert.assertEquals("a3v1", filterEdge.getProperty("a3"));

                // The two contracted filter flow edges had the same property key "a2" but each of them with a different value
                // One filter flow edge had for key "a2" value "a2v1", another one had for the same key "a2" a value "a2v2"
                // It is undefined what property value should be chosen, when two contracted edges have a property with a same key but a different value
                // Hence, we cannot know, whether the contracted edge should have for the key "a2" value "a2v1" or "a2v2"

                return null;
            }
        });

        
        final List<Vertex> allVerticesAfter = new ArrayList<>();
        final List<Edge> allEdgesAfter = new ArrayList<>();
        
        // Check for revision validity - all remaining vertices and edges should have end revision 1.999999
        // Additionally printing detailed information for further checking (debugging)
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                for (Vertex vertex : transaction.getVertices()) {
                    allVerticesAfter.add(vertex);
                }

                for (Edge edge : transaction.getEdges()) {
                    if (edge.getLabel().equals(EdgeLabel.DIRECT.t()) || edge.getLabel().equals(EdgeLabel.FILTER.t())) {
                        allEdgesAfter.add(edge);
                    }
                }
                
                System.out.println();
                for (Vertex vertexAfter : allVerticesAfter) {
                    boolean remained = false;
                    for (Vertex vertexBefore : allVerticesBefore) {
                        if (vertexAfter.getId().equals(vertexBefore.getId())) {
                            remained = true;
                            allVerticesBefore.remove(vertexBefore);
                            break;
                        }
                    }
                    if (remained) {
                        System.out.println("Vertex " + vertexAfter.getId() + " remained after contraction");
                    } else {
                        System.out.println("Vertex " + vertexAfter.getId() + " was newly created during contraction");
                    }
                    for (String propertyKey : vertexAfter.getPropertyKeys()) {
                        System.out.println(propertyKey + "=" + vertexAfter.getProperty(propertyKey));
                    }
                    try {
                        Edge controlEdge = GraphOperation.getControlEdge(vertexAfter);
                        System.out.println("tranStart=" + controlEdge.getProperty(EdgeProperty.TRAN_START.t()));
                        System.out.println("tranEnd=" + controlEdge.getProperty(EdgeProperty.TRAN_END.t()));
                        
                        // all remaining objects should have end revision still 1.999999
                        Assert.assertEquals(REVISION_1_999999, controlEdge.getProperty(EdgeProperty.TRAN_END.t()));
                    } catch (IllegalArgumentException exception) {
                        System.out.println("Vertex does not have control edge");
                    }
                    System.out.println();
                }

                for (Vertex vertex : allVerticesBefore) {
                    System.out.println("Vertex " + vertex.getId() + " was removed during contraction");
                    System.out.println();
                }

                for (Edge edgeAfter : allEdgesAfter) {
                    boolean remained = false;
                    for (Edge edgeBefore : allEdgesBefore) {
                        if (edgeAfter.getId().equals(edgeBefore.getId())) {
                            remained = true;
                            allEdgesBefore.remove(edgeBefore);
                            break;
                        }
                    }
                    Object sourceVertexId = edgeAfter.getVertex(Direction.OUT).getId();
                    Object targetVertexId = edgeAfter.getVertex(Direction.IN).getId();

                    if (remained) {
                        System.out.println("Edge " + sourceVertexId + "--" + edgeAfter.getLabel() + "-->"
                                + targetVertexId + " remained after contraction");
                    } else {
                        System.out.println("Edge " + sourceVertexId + "--" + edgeAfter.getLabel() + "-->"
                                + targetVertexId + " was newly created during contraction");
                    }
                    for (String propertyKey : edgeAfter.getPropertyKeys()) {
                        System.out.println(propertyKey + "=" + edgeAfter.getProperty(propertyKey));
                    }
                    System.out.println();
                }

                for (Edge edge : allEdgesBefore) {
                    Object sourceVertexId = edge.getVertex(Direction.OUT).getId();
                    Object targetVertexId = edge.getVertex(Direction.IN).getId();
                    System.out.println("Edge " + sourceVertexId + "--" + edge.getLabel() + "-->" + targetVertexId
                            + " was removed during contraction");
                    System.out.println();
                }

                return null;
            }

        });
    }

    @Test
    public void testContractionMulti() throws Exception {
        cleanGraph();
        final double revision = createMajorRevision();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");

                Vertex resource1 = GraphCreation.createResource(transaction, root, "Teradata DB", "Teradata", "desc",
                        layer, revision);
                Vertex r1db1 = GraphCreation.createNode(transaction, resource1, "db1", "Database", revision);
                Vertex r1db1t1 = GraphCreation.createNode(transaction, r1db1, "table1", "Table", revision);
                Vertex r1db1t1c1 = GraphCreation.createNode(transaction, r1db1t1, "t1c1", "Column", revision);
                Vertex r1db1t2 = GraphCreation.createNode(transaction, r1db1, "table2", "Table", revision);
                Vertex r1db1t2c1 = GraphCreation.createNode(transaction, r1db1t2, "t2c1", "Column", revision);

                Vertex resource2 = GraphCreation.createResource(transaction, root, "Teradata DB", "Teradata", "desc",
                        layer, revision);
                Vertex r2db1 = GraphCreation.createNode(transaction, resource2, "db1", "Database", revision);
                Vertex r2db1t1 = GraphCreation.createNode(transaction, r2db1, "table1", "Table", revision);
                Vertex r2db1t1c1 = GraphCreation.createNode(transaction, r2db1t1, "t1c1", "Column", revision);
                Vertex r2db1t2 = GraphCreation.createNode(transaction, r2db1, "table2", "Table", revision);
                Vertex r2db1t2c1 = GraphCreation.createNode(transaction, r2db1t2, "t2c1", "Column", revision);

                Vertex resource3 = GraphCreation.createResource(transaction, root, "Teradata DB", "Teradata", "desc",
                        layer, revision);
                Vertex r3db1 = GraphCreation.createNode(transaction, resource3, "db1", "Database", revision);
                Vertex r3db1t1 = GraphCreation.createNode(transaction, r3db1, "table1", "Table", revision);
                Vertex r3db1t1c1 = GraphCreation.createNode(transaction, r3db1t1, "t1c1", "Column", revision);
                Vertex r3db1t2 = GraphCreation.createNode(transaction, r3db1, "table2", "Table", revision);
                Vertex r3db1t2c1 = GraphCreation.createNode(transaction, r3db1t2, "t2c1", "Column", revision);

                Vertex resource4 = GraphCreation.createResource(transaction, root, "Teradata DB", "Teradata", "desc",
                        layer, revision);
                Vertex r4db1 = GraphCreation.createNode(transaction, resource4, "db1", "Database", revision);
                Vertex r4db1t1 = GraphCreation.createNode(transaction, r4db1, "table1", "Table", revision);
                Vertex r4db1t1c1 = GraphCreation.createNode(transaction, r4db1t1, "t1c1", "Column", revision);
                Vertex r4db1t2 = GraphCreation.createNode(transaction, r4db1, "table2", "Table", revision);
                Vertex r4db1t2c1 = GraphCreation.createNode(transaction, r4db1t2, "t2c1", "Column", revision);

                GraphCreation.createNodeAttribute(transaction, r1db1, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r2db1, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r3db1, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r4db1, "ba", "v1", revision);

                GraphCreation.createNodeAttribute(transaction, r1db1t1, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r2db1t1, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r3db1t1, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r4db1t1, "ba", "v1", revision);

                GraphCreation.createNodeAttribute(transaction, r1db1t1c1, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r2db1t1c1, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r3db1t1c1, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r4db1t1c1, "ba", "v1", revision);

                GraphCreation.createNodeAttribute(transaction, r1db1t2, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r2db1t2, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r3db1t2, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r4db1t2, "ba", "v1", revision);

                GraphCreation.createNodeAttribute(transaction, r1db1t2c1, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r2db1t2c1, "ba", "v1", revision);
                GraphCreation.createNodeAttribute(transaction, r3db1t2c1, "ba", "v1", revision);
                Vertex attr = GraphCreation.createNodeAttribute(transaction, r4db1t2c1, "ba", "v1", revision);
                GraphCreation.createEdge(r4db1t2c1, attr, EdgeLabel.HAS_ATTRIBUTE, revision);

                GraphCreation.createEdge(r1db1t1c1, r1db1t2c1, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(r2db1t1c1, r2db1t2c1, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(r3db1t1c1, r3db1t2c1, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(r4db1t1c1, r4db1t2c1, EdgeLabel.DIRECT, revision);

                return null;
            }
        });

        ContractionResult result = getHelper().performContractionInner(revision);
        Map<VertexType, ActionCounter> vertices = result.getVertexMap();
        Assert.assertEquals(3, vertices.size());
        Assert.assertEquals(3, vertices.get(VertexType.RESOURCE).getCount());
        Assert.assertEquals(15, vertices.get(VertexType.NODE).getCount());
        Assert.assertEquals(15, vertices.get(VertexType.ATTRIBUTE).getCount());

        Map<EdgeLabel, ActionCounter> edgeMap = result.getEdgeMap();
        Assert.assertEquals(1, edgeMap.size());
        Assert.assertEquals(3, edgeMap.get(EdgeLabel.DIRECT).getCount());
    }

    @Test
    public void testContractionMoreEdges() throws Exception {
        cleanGraph();
        final double revision = createMajorRevision();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");

                Vertex resource1 = GraphCreation.createResource(transaction, root, "Teradata DB", "Teradata", "desc",
                        layer, revision);
                Vertex db1 = GraphCreation.createNode(transaction, resource1, "db1", "Database", revision);

                Vertex i1t1 = GraphCreation.createNode(transaction, db1, "table1", "Table", revision);
                Vertex i1t1c1 = GraphCreation.createNode(transaction, i1t1, "t1c1", "Column", revision);
                Vertex i1t2 = GraphCreation.createNode(transaction, db1, "table2", "Table", revision);
                Vertex i1t2c1 = GraphCreation.createNode(transaction, i1t2, "t2c1", "Column", revision);

                Vertex i2t1 = GraphCreation.createNode(transaction, db1, "table1", "Table", revision);
                Vertex i2t1c1 = GraphCreation.createNode(transaction, i2t1, "t1c1", "Column", revision);
                Vertex i2t2 = GraphCreation.createNode(transaction, db1, "table2", "Table", revision);
                Vertex i2t2c1 = GraphCreation.createNode(transaction, i2t2, "t2c1", "Column", revision);

                Vertex i3t1 = GraphCreation.createNode(transaction, db1, "table1", "Table", revision);
                Vertex i3t1c1 = GraphCreation.createNode(transaction, i3t1, "t1c1", "Column", revision);
                Vertex i3t2 = GraphCreation.createNode(transaction, db1, "table2", "Table", revision);
                Vertex i3t2c1 = GraphCreation.createNode(transaction, i3t2, "t2c1", "Column", revision);

                Vertex i4t1 = GraphCreation.createNode(transaction, db1, "table1", "Table", revision);
                Vertex i4t1c1 = GraphCreation.createNode(transaction, i4t1, "t1c1", "Column", revision);
                Vertex i4t2 = GraphCreation.createNode(transaction, db1, "table2", "Table", revision);
                Vertex i4t2c1 = GraphCreation.createNode(transaction, i4t2, "t2c1", "Column", revision);

                GraphCreation.createEdge(i1t1c1, i1t2c1, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(i2t1c1, i2t2c1, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(i3t1c1, i3t2c1, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(i4t1c1, i4t2c1, EdgeLabel.DIRECT, revision);

                return null;
            }
        });

        ContractionResult result = getHelper().performContractionInner(revision);
        Map<VertexType, ActionCounter> vertices = result.getVertexMap();
        Assert.assertEquals(1, vertices.size());
        Assert.assertEquals(12, vertices.get(VertexType.NODE).getCount());

        Map<EdgeLabel, ActionCounter> edgeMap = result.getEdgeMap();
        Assert.assertEquals(1, edgeMap.size());
        Assert.assertEquals(3, edgeMap.get(EdgeLabel.DIRECT).getCount());

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();
                List<Edge> edges = GraphOperation.getAdjacentEdges(t1c1, Direction.OUT,
                        RevisionRootHandler.EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT);
                Assert.assertEquals(1, edges.size());
                Assert.assertEquals(String.valueOf(t2c1.getId()), edges.get(0).getProperty(EdgeProperty.TARGET_ID.t()));

                return null;
            }
        });
    }

    // test, ze titan naprosto kasle na paralelni transakce :-/
    public void testParallelEdgeModification() {
        TestDatabaseHolder databaseHolder = new TestDatabaseHolder();
        DatabaseConfiguration configuration = new DatabaseConfiguration();
        configuration.setHasNodeNameIndex(true);
        databaseHolder.setConfiguration(configuration);
        databaseHolder.setDir(TestTitanDatabaseProvider.DEFAULT_DB_DIR);
        LicenseHolder licenseHolder = new LicenseHolder(2);
        licenseHolder.loadLicense(new LicenseLoaderImpl().loadFromProperty());
        databaseHolder.setLicenseHolder(licenseHolder);
        databaseHolder.init();
        databaseHolder.truncateDatabase();
        GraphCreation.initDatabase(databaseHolder, getSuperRootHandler(), getRevisionRootHandler(),
                getSourceRootHandler());

        final double revision = createMajorRevision();

        databaseHolder.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);
                Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex res = GraphCreation.createResource(transaction, root, "res", "res", "", layer, revision);
                Vertex c1 = GraphCreation.createNode(transaction, res, "c1", "Table", revision);
                Vertex c2 = GraphCreation.createNode(transaction, res, "c2", "Table", revision);
                GraphCreation.createEdge(c1, c2, EdgeLabel.FILTER, revision);
                return null;
            }
        });

        TitanTransaction transaction1 = null;
        TitanTransaction transaction2 = null;

        try {
            transaction1 = databaseHolder.getWriteTransaction();
            transaction2 = databaseHolder.getWriteTransaction();

            Vertex t1c1 = transaction1.getVertices(NodeProperty.NODE_NAME.t(), "c1").iterator().next();
            Vertex t1c2 = transaction1.getVertices(NodeProperty.NODE_NAME.t(), "c2").iterator().next();
            LOGGER.info("id c2: {}", t1c2.getId());

            Vertex t2c2 = transaction2.getVertices(NodeProperty.NODE_NAME.t(), "c2").iterator().next();

            GraphCreation.createEdge(t1c1, t1c2, EdgeLabel.DIRECT, REVISION_1_000000);
            t2c2.remove();

            transaction1.commit();
            transaction2.commit();

        } catch (Throwable t) {
            if (transaction1 != null) {
                transaction1.rollback();
            }
            if (transaction2 != null) {
                transaction2.rollback();
            }

            throw t;
        }

        databaseHolder.runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "c1").iterator().next();

                List<Edge> edges = GraphOperation.getAdjacentEdges(c1, Direction.OUT,
                        RevisionRootHandler.EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT, EdgeLabel.FILTER);
                //Assert.assertEquals(0, edges.size());
                LOGGER.info("{}", edges.get(0));
                LOGGER.info("{}", edges.get(0).getVertex(Direction.IN));
                LOGGER.info("{}", edges.get(0).getVertex(Direction.IN).getPropertyKeys());
                LOGGER.info("{}", edges.get(0).getVertex(Direction.IN).getId());

                return null;
            }
        });
    }

    private ContractionHelper getHelper() {
        ContractionHelper helper = new ContractionHelper();
        helper.setDatabaseService(getDatabaseHolder());
        helper.setRevisionRootHandler(getRevisionRootHandler());
        helper.setSuperRootHandler(getSuperRootHandler());
        return helper;
    }

    @Test
    public void testUnification() throws Exception {
        cleanGraph();

        final double revision = createMajorRevision();
        final OneFileMerger mergerHelper = createMergerHelper();
        mergerHelper.process(new FileInputStream(new File("src/test/resources/graphForUnification.csv")), revision);
        commitRevision(revision);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                RevisionInterval revInterval = new RevisionInterval(revision);
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                List<Vertex> step1 = GraphOperation.getAdjacentVertices(t1c2, Direction.OUT, revInterval,
                        EdgeLabel.DIRECT);
                Assert.assertEquals(1, step1.size());
                Vertex adjacent1 = step1.get(0);
                Assert.assertEquals("t2c2", GraphOperation.getName(adjacent1));

                List<Vertex> step2 = GraphOperation.getAdjacentVertices(adjacent1, Direction.OUT, revInterval,
                        EdgeLabel.DIRECT);
                // ještě tu ta hrana není, nedošlo zatím k unifikaci
                Assert.assertEquals(0, step2.size());

                return null;
            }
        });

        List<Map<String, String>> sources = new ArrayList<>();
        List<Map<String, String>> targets = new ArrayList<>();
        sources.add(Collections.singletonMap(UnificationVisitor.NODE_TYPE, "Table"));
        targets.add(Collections.singletonMap(UnificationVisitor.NODE_TYPE, "View"));

        sources.add(Collections.singletonMap(UnificationVisitor.NODE_TYPE, "Function"));
        targets.add(Collections.singletonMap(UnificationVisitor.NODE_TYPE, "Procedure"));

        ContractionResult result = getHelper().performUnificationInner(revision, "Teradata", sources, targets, false,
                false, false);
        Map<VertexType, ActionCounter> vertices = result.getVertexMap();
        Assert.assertEquals(2, vertices.size());
        Assert.assertEquals(4, vertices.get(VertexType.NODE).getCount());
        Assert.assertEquals(1, vertices.get(VertexType.ATTRIBUTE).getCount());

        Map<EdgeLabel, ActionCounter> edgeMap = result.getEdgeMap();
        Assert.assertEquals(1, edgeMap.size());
        Assert.assertEquals(1, edgeMap.get(EdgeLabel.FILTER).getCount());

        testUnifiedGraph(revision, 29);
    }

    @Test
    public void testUnificationAttributes() throws Exception {
        cleanGraph();

        final double revision = createMajorRevision();
        final OneFileMerger mergerHelper = createMergerHelper();
        mergerHelper.process(new FileInputStream(new File("src/test/resources/graphForUnification2.csv")), revision);
        commitRevision(revision);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                RevisionInterval revInterval = new RevisionInterval(revision);
                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                List<Vertex> step1 = GraphOperation.getAdjacentVertices(t1c2, Direction.OUT, revInterval,
                        EdgeLabel.DIRECT);
                Assert.assertEquals(1, step1.size());
                Vertex adjacent1 = step1.get(0);
                Assert.assertEquals("t2c2", GraphOperation.getName(adjacent1));

                List<Vertex> step2 = GraphOperation.getAdjacentVertices(adjacent1, Direction.OUT, revInterval,
                        EdgeLabel.DIRECT);
                // ještě tu ta hrana není, nedošlo zatím k unifikaci
                Assert.assertEquals(0, step2.size());

                return null;
            }
        });

        List<Map<String, String>> sources = new ArrayList<>();
        Map<String, String> sourceMap = new HashMap<>();
        List<Map<String, String>> targets = new ArrayList<>();
        Map<String, String> targetMap = new HashMap<>();
        sourceMap.put(UnificationVisitor.NODE_TYPE, "View");
        sourceMap.put("a1", "a1v1");
        sources.add(sourceMap);
        targetMap.put(UnificationVisitor.NODE_TYPE, "View");
        targetMap.put("a1", "a1v2");
        targets.add(targetMap);

        sources.add(Collections.singletonMap(UnificationVisitor.NODE_TYPE, "Function"));
        targets.add(Collections.singletonMap(UnificationVisitor.NODE_TYPE, "Procedure"));

        ContractionResult result = getHelper().performUnificationInner(revision, "Teradata", sources, targets, true,
                false, false);
        Map<VertexType, ActionCounter> vertices = result.getVertexMap();
        Assert.assertEquals(1, vertices.size());
        Assert.assertEquals(4, vertices.get(VertexType.NODE).getCount());

        Map<EdgeLabel, ActionCounter> edgeMap = result.getEdgeMap();
        Assert.assertEquals(1, edgeMap.size());
        Assert.assertEquals(1, edgeMap.get(EdgeLabel.FILTER).getCount());

        testUnifiedGraph(revision, 29);
    }

    @Test
    public void testUnificationSeveralRevisions() throws Exception {
        cleanGraph();
        double revision = createMajorRevision();
        final OneFileMerger mergerHelper = createMergerHelper();
        mergerHelper.process(new FileInputStream(new File("src/test/resources/graphForUnification.csv")), revision);
        commitRevision(revision);

        revision = createMajorRevision();
        mergerHelper.process(new FileInputStream(new File("src/test/resources/graphForUnification.csv")), revision);

        List<Map<String, String>> sources = new ArrayList<>();
        List<Map<String, String>> targets = new ArrayList<>();
        sources.add(Collections.singletonMap(UnificationVisitor.NODE_TYPE, "Table"));
        targets.add(Collections.singletonMap(UnificationVisitor.NODE_TYPE, "View"));

        sources.add(Collections.singletonMap(UnificationVisitor.NODE_TYPE, "Function"));
        targets.add(Collections.singletonMap(UnificationVisitor.NODE_TYPE, "Procedure"));

        ContractionResult result = getHelper().performUnificationInner(revision, "Teradata", sources, targets, false,
                false, false);
        Map<VertexType, ActionCounter> vertices = result.getVertexMap();
        Assert.assertEquals(2, vertices.size());
        Assert.assertEquals(4, vertices.get(VertexType.NODE).getCount());
        Assert.assertEquals(1, vertices.get(VertexType.ATTRIBUTE).getCount());

        Map<EdgeLabel, ActionCounter> edgeMap = result.getEdgeMap();
        Assert.assertEquals(1, edgeMap.size());
        Assert.assertEquals(1, edgeMap.get(EdgeLabel.FILTER).getCount());

        commitRevision(revision);
        testUnifiedGraph(revision, 40);

        checkOlderRevision();
    }

    private void checkOlderRevision() {
        // jeste zkontrolovat neposkozeni
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                RevisionInterval revInterval = new RevisionInterval(REVISION_1_000000, REVISION_1_000000);

                Vertex root = getSuperRootHandler().getRoot(transaction);

                List<Vertex> resources = GraphOperation.getDirectChildren(root, revInterval);
                Assert.assertEquals(1, resources.size());
                Vertex resource = resources.get(0);
                Assert.assertEquals("Teradata", GraphOperation.getName(resource));

                List<Vertex> databases = GraphOperation.getDirectChildren(resource, revInterval);
                Assert.assertEquals(2, databases.size());
                Vertex db1 = databases.get(0);
                Assert.assertEquals("db1", GraphOperation.getName(db1));

                List<Vertex> db1elements = GraphOperation.getDirectChildren(db1, revInterval);
                Assert.assertEquals(5, db1elements.size());

                Vertex db1t1 = null;
                Vertex db1macro = null;
                Vertex db1v2 = null;
                Vertex db1t2 = null;
                Vertex db1t3 = null;
                for (Vertex v : db1elements) {
                    String name = GraphOperation.getName(v);
                    String type = GraphOperation.getType(v);
                    if (name.equals("table1")) {
                        db1t1 = v;
                    } else if (name.equals("table2") && type.equals("Table")) {
                        db1t2 = v;
                    } else if (name.equals("table2") && type.equals("View")) {
                        db1v2 = v;
                    } else if (name.equals("table2") && type.equals("Macro")) {
                        db1macro = v;
                    } else if (name.equals("table3")) {
                        db1t3 = v;
                    } else {
                        Assert.fail("Unexpected child " + name + "[" + type + "].");
                    }
                }

                Assert.assertEquals("Table", GraphOperation.getType(db1t1));
                Assert.assertEquals("Table", GraphOperation.getType(db1t2));
                Assert.assertEquals("View", GraphOperation.getType(db1v2));
                Assert.assertEquals("Table", GraphOperation.getType(db1t3));
                Assert.assertEquals("Macro", GraphOperation.getType(db1macro));

                List<Vertex> db1t1columns = GraphOperation.getDirectChildren(db1t1, revInterval);
                Assert.assertEquals(2, db1t1columns.size());
                List<Vertex> db1v2columns = GraphOperation.getDirectChildren(db1v2, revInterval);
                Assert.assertEquals(2, db1v2columns.size());
                List<Vertex> db1t2columns = GraphOperation.getDirectChildren(db1t2, revInterval);
                Assert.assertEquals(3, db1t2columns.size());
                List<Vertex> db1t3columns = GraphOperation.getDirectChildren(db1t3, revInterval);
                Assert.assertEquals(3, db1t3columns.size());

                List<Object> db1v2attrs = GraphOperation.getNodeAttribute(db1v2, "a1", revInterval);
                Assert.assertEquals(Collections.singletonList("a1v1"), db1v2attrs);
                List<Object> db1v2attrs2 = GraphOperation.getNodeAttribute(db1v2, "a2", revInterval);
                Assert.assertEquals(0, db1v2attrs2.size());

                List<Object> db1t2attrs = GraphOperation.getNodeAttribute(db1t2, "a1", revInterval);
                List<String> attrs = new ArrayList<>();
                attrs.add("a1v1");
                attrs.add("a1v2");
                Assert.assertEquals(attrs, db1t2attrs);
                List<Object> db1t2attrs2 = GraphOperation.getNodeAttribute(db1t2, "a2", revInterval);
                Assert.assertEquals(Collections.singletonList("a2v1"), db1t2attrs2);

                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                List<Vertex> step1 = GraphOperation.getAdjacentVertices(t1c2, Direction.OUT, revInterval,
                        EdgeLabel.DIRECT);
                Assert.assertEquals(1, step1.size());
                Vertex adjacent1 = step1.get(0);
                Assert.assertEquals("t2c2", GraphOperation.getName(adjacent1));

                List<Vertex> step2 = GraphOperation.getAdjacentVertices(adjacent1, Direction.OUT, revInterval,
                        EdgeLabel.DIRECT);
                // zde hrana nesmí být, jsme v původní revizi
                Assert.assertEquals(0, step2.size());

                // test podstruktur
                Vertex t2c3 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c3").iterator().next();
                List<Vertex> t2c3Children = GraphOperation.getDirectChildren(t2c3, revInterval);
                Assert.assertEquals(2, t2c3Children.size());
                return null;
            }
        });
    }

    private OneFileMerger createMergerHelper() {
        StandardMergerProcessor mergerProcessor = new StandardMergerProcessor();
        mergerProcessor.setSuperRootHandler(getSuperRootHandler());
        mergerProcessor.setSourceRootHandler(getSourceRootHandler());
        mergerProcessor.setRevisionRootHandler(getRevisionRootHandler());

        final OneFileMerger mergerHelper = new OneFileMerger(getDatabaseHolder(), getSuperRootHandler(),
                getRevisionRootHandler(), getSourceRootHandler(), mergerProcessor, getLicenseHolder(),
                scriptMetadataService, 2000, false);
        return mergerHelper;
    }

    private void testUnifiedGraph(final double revision, final int nodeCount) {
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                RevisionInterval revInterval = new RevisionInterval(revision, revision);

                Assert.assertEquals(nodeCount, getVertexCount(transaction));

                Vertex root = getSuperRootHandler().getRoot(transaction);

                List<Vertex> resources = GraphOperation.getDirectChildren(root, revInterval);
                Assert.assertEquals(1, resources.size());
                Vertex resource = resources.get(0);
                Assert.assertEquals("Teradata", GraphOperation.getName(resource));

                List<Vertex> databases = GraphOperation.getDirectChildren(resource, revInterval);
                Assert.assertEquals(2, databases.size());
                Vertex db1 = databases.get(0);
                Assert.assertEquals("db1", GraphOperation.getName(db1));

                List<Vertex> db1elements = GraphOperation.getDirectChildren(db1, revInterval);
                Assert.assertEquals(4, db1elements.size());

                Vertex db1t1 = null;
                Vertex db1macro = null;
                Vertex db1v2 = null;
                Vertex db1t3 = null;
                for (Vertex v : db1elements) {
                    String name = GraphOperation.getName(v);
                    String type = GraphOperation.getType(v);
                    if (name.equals("table1")) {
                        db1t1 = v;
                    } else if (name.equals("table2") && type.equals("View")) {
                        db1v2 = v;
                    } else if (name.equals("table2") && type.equals("Macro")) {
                        db1macro = v;
                    } else if (name.equals("table3")) {
                        db1t3 = v;
                    } else {
                        Assert.fail("Unexpected child " + name + "[" + type + "].");
                    }
                }

                Assert.assertEquals("Table", GraphOperation.getType(db1t1));
                Assert.assertEquals("View", GraphOperation.getType(db1v2));
                Assert.assertEquals("Table", GraphOperation.getType(db1t3));
                Assert.assertEquals("Macro", GraphOperation.getType(db1macro));

                List<Vertex> db1t1columns = GraphOperation.getDirectChildren(db1t1, revInterval);
                Assert.assertEquals(2, db1t1columns.size());
                List<Vertex> db1v2columns = GraphOperation.getDirectChildren(db1v2, revInterval);
                Assert.assertEquals(3, db1v2columns.size());
                List<Vertex> db1t3columns = GraphOperation.getDirectChildren(db1t3, revInterval);
                Assert.assertEquals(3, db1t3columns.size());

                // test atributů

                Set<String> db1t2attrs = new HashSet<>();
                for (Object attribute : GraphOperation.getNodeAttribute(db1v2, "a1", revInterval)) {
                    db1t2attrs.add((String) attribute);
                }

                Set<String> attrs = new HashSet<>();
                attrs.add("a1v1");
                attrs.add("a1v2");
                Assert.assertEquals(attrs, db1t2attrs);

                List<Object> db1v2attrs2 = GraphOperation.getNodeAttribute(db1v2, "a2", revInterval);
                Assert.assertEquals(Collections.singletonList("a2v1"), db1v2attrs2);

                // test hran

                Vertex t1c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c2").iterator().next();

                List<Vertex> step1 = GraphOperation.getAdjacentVertices(t1c2, Direction.OUT, revInterval,
                        EdgeLabel.DIRECT);
                Assert.assertEquals(1, step1.size());
                Vertex adjacent1 = step1.get(0);
                Assert.assertEquals("t2c2", GraphOperation.getName(adjacent1));

                List<Vertex> step2 = GraphOperation.getAdjacentVertices(adjacent1, Direction.OUT, revInterval,
                        EdgeLabel.DIRECT);
                // již tu hrana musí být, to je důvod celé unifikace
                Assert.assertEquals(1, step2.size());
                Vertex adjacent2 = step2.get(0);
                Assert.assertEquals("t3c2", GraphOperation.getName(adjacent2));

                // test podstruktur
                Vertex t2c3 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c3").iterator().next();
                List<Vertex> t2c3Children = GraphOperation.getDirectChildren(t2c3, revInterval);
                Assert.assertEquals(2, t2c3Children.size());

                return null;
            }
        });
    }

    private static class TestDatabaseHolder extends SimpleDatabaseHolder {
        public TitanTransaction getWriteTransaction() {
            return DatabaseProvider.INSTANCE.getDatabase("primary", new BaseConfiguration()).newTransaction();
        }
    }

    @Test
    @Ignore
    public void testParallelDeleting() {
        cleanGraph();
        final double revision = createMajorRevision();
        final int threadsNumber = 20;

        List<Object> edges = getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE,
                new TestCallback<List<Object>>() {

                    @Override
                    public List<Object> callMe(TitanTransaction transaction) {
                        Vertex root = getSuperRootHandler().getRoot(transaction);
                        Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                        Vertex resource = GraphCreation.createResource(transaction, root, "Teradata", "Teradata", "",
                                layer, revision);
                        Vertex a1 = GraphCreation.createNode(transaction, resource, "a1", "t", revision);
                        Vertex a2 = GraphCreation.createNode(transaction, resource, "a2", "t", revision);

                        List<Object> edges = new ArrayList<>();
                        for (int i = 0; i <= threadsNumber; i++) {
                            edges.add(GraphCreation.createEdge(a1, a2, EdgeLabel.DIRECT, revision).getId());
                        }

                        return edges;
                    }
                });

        ExecutorService executorService = Executors.newFixedThreadPool(threadsNumber + 1);
        CountDownLatch shutdownLatch = new CountDownLatch(threadsNumber);
        executorService.submit(new ExecutorServiceTerminator(executorService, shutdownLatch));
        for (int i = 0; i < threadsNumber; i++) {
            executorService.submit(new TestDeleter(edges.get(i), getDatabaseHolder(), shutdownLatch));
        }

        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.warn("Executor Service interrupted.", e);
            List<Runnable> threads = executorService.shutdownNow();
            for (Runnable thread : threads) {
                LOGGER.warn("Processor has not been started: " + thread.toString());
            }
            Thread.currentThread().interrupt();
        }

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {

            @Override
            public List<Object> callMe(TitanTransaction transaction) {
                Iterable<Edge> edgesCurrent = transaction.getEdges();
                int sum = 0;
                for (Edge e : edgesCurrent) {
                    if (e.getLabel().equals(EdgeLabel.DIRECT.t())) {
                        sum++;
                    }
                }
                Assert.assertEquals(1, sum);

                return null;
            }
        });
    }

    private static class TestDeleter extends TestCallback<Object> implements Runnable {

        private final Object edgeId;

        private final DatabaseHolder databaseHolder;

        private final CountDownLatch shutdownLatch;

        public TestDeleter(Object edgeId, DatabaseHolder databaseHolder, CountDownLatch shutdownLatch) {
            super();
            this.edgeId = edgeId;
            this.databaseHolder = databaseHolder;
            this.shutdownLatch = shutdownLatch;
        }

        @Override
        public Object callMe(TitanTransaction transaction) {
            Edge edge = transaction.getEdge(edgeId);
            edge.remove();
            return null;
        }

        @Override
        public void run() {
            databaseHolder.runInTransaction(TransactionLevel.WRITE_SHARE, this);
            shutdownLatch.countDown();
        }

    }
}
