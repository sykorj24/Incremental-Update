package eu.profinit.manta.dataflow.repository.merger.server.helper.sourcecode;

import java.io.File;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeUploadRequest;

public class SourceCodeHelperTest extends TestTitanDatabaseProvider {

    @Test(expected = NullPointerException.class)
    public void testUploadBadRequest() {
        SourceCodeHelper sourceCodeHelper = createHelper();
        sourceCodeHelper.upload(null);
    }

    @Test
    public void testUpload() throws IOException {
        if (getSourceRootHandler().getSourceCodeDir().exists()) {
            FileUtils.forceDelete(getSourceRootHandler().getSourceCodeDir());
        }
        createBasicGraph(false);
        SourceCodeHelper sourceCodeHelper = createHelper();

        SourceCodeUploadRequest uploadRequest = new SourceCodeUploadRequest();
        String fileId = "4";
        uploadRequest.setFileId(fileId);
        File localFile = new File("src/test/resources/sourceCode.txt");
        uploadRequest.setFileData(Base64.encodeBase64String(FileUtils.readFileToByteArray(localFile)));

        File uploadedFile = new File(getSourceRootHandler().getSourceCodeDir(), fileId);

        Assert.assertFalse(uploadedFile.exists());
        ResponseEntity<String> uploadResponseOK = sourceCodeHelper.upload(uploadRequest);
        Assert.assertEquals(HttpStatus.OK, uploadResponseOK.getStatusCode());
        Assert.assertTrue(uploadedFile.exists());
        Assert.assertEquals(FileUtils.readFileToString(localFile), FileUtils.readFileToString(uploadedFile));

        ResponseEntity<String> uploadResponseAlreadyUploaded = sourceCodeHelper.upload(uploadRequest);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, uploadResponseAlreadyUploaded.getStatusCode());
    }

    private SourceCodeHelper createHelper() {
        SourceCodeHelper helper = new SourceCodeHelper();
        helper.setSourceRootHandler(getSourceRootHandler());
        return helper;
    }
}
