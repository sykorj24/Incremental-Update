package eu.profinit.manta.dataflow.repository.merger.server.helper.revision;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.collections4.Transformer;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.OneFileMerger;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.StandardMergerProcessor;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.ProcessorResult.ActionCounter;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.ProcessorResult.ActionType;
import eu.profinit.manta.dataflow.repository.merger.server.helper.sourcecode.SourceCodeHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.sourcecode.TestHelper;
import eu.profinit.manta.platform.scriptmetadata.service.ScriptMetadataService;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class PruneProcessorTest extends TestTitanDatabaseProvider {

    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(PruneProcessorTest.class);

    private static final int COMMIT_COUNT = 5;

    // could not resolve null pointer exception when using mocking
    // need to initialize scriptMetadataService
    @Mocked
    private ScriptMetadataService scriptMetadataService = new ScriptMetadataService() {
        
        @Override
        public void storeScripts(Collection<String> arg0) {
            // TODO Auto-generated method stub
            
        }
        
        @Override
        public int revertUncommittedScripts() {
            // TODO Auto-generated method stub
            return 0;
        }
        
        @Override
        public int countCommittedScripts(int arg0, Transformer<Collection<String>, Collection<String>> arg1) {
            // TODO Auto-generated method stub
            return 0;
        }
        
        @Override
        public int commitScripts(int arg0, int arg1, Transformer<Collection<String>, Collection<String>> arg2) {
            // TODO Auto-generated method stub
            return 0;
        }
    };

    @Test
    public void testExecution() throws IOException {
        cleanGraph();
        if (getSourceRootHandler().getSourceCodeDir().exists()) {
            FileUtils.forceDelete(getSourceRootHandler().getSourceCodeDir());
        }
        final SourceCodeHelper sourceCodeHelper = new SourceCodeHelper();
        sourceCodeHelper.setSourceRootHandler(getSourceRootHandler());

        StandardMergerProcessor mergerProcessor = new StandardMergerProcessor();
        mergerProcessor.setSuperRootHandler(getSuperRootHandler());
        mergerProcessor.setSourceRootHandler(getSourceRootHandler());
        mergerProcessor.setRevisionRootHandler(getRevisionRootHandler());

        final OneFileMerger mergerHelper = new OneFileMerger(getDatabaseHolder(), getSuperRootHandler(),
                getRevisionRootHandler(), getSourceRootHandler(), mergerProcessor, getLicenseHolder(), scriptMetadataService, 2000, false);

        // prvni davka s commitem
        Double latestRevision;
        // create revision 1.000000
        latestRevision = createMajorRevision();
        try {
            mergerHelper.process(new FileInputStream(new File("src/test/resources/input1.csv")), latestRevision);
        } catch (FileNotFoundException e) {
            LOGGER.error("error during loading file", e);
        }
        String file1Id = TestHelper.mergeFile(getDatabaseHolder(), getSourceRootHandler(), sourceCodeHelper,
                "src/test/resources/sourceCode1.txt",
                latestRevision);
        // commit revision 1.000000
        commitRevision(latestRevision);

        // pak pět jiných commitů
        // create and commit revisions 2.000000, 3.000000, 4.000000, 5.000000 and 6.000000
        String file2Id = null;
        for (int i = 0; i < COMMIT_COUNT; i++) {
            latestRevision = createMajorRevision();
            try {
                mergerHelper.process(new FileInputStream(new File("src/test/resources/input1Temp2.csv")), latestRevision);
            } catch (FileNotFoundException e) {
                LOGGER.error("error during loading file", e);
            }
            file2Id = TestHelper.mergeFile(getDatabaseHolder(), getSourceRootHandler(), sourceCodeHelper,
                    "src/test/resources/sourceCode2.txt",
                    latestRevision);
            commitRevision(latestRevision);
        }

        Assert.assertTrue(new File(getSourceRootHandler().getSourceCodeDir(), file1Id).exists());
        Assert.assertTrue(new File(getSourceRootHandler().getSourceCodeDir(), file2Id).exists());

        // prune up to the second latest revision (5.000000)
        double endRevision = latestRevision - 1;
        LOGGER.info("Commited head: {}.", getRevisionRootHandler().getLatestCommittedRevisionNumber(getDatabaseHolder()));
        LOGGER.info("Prune interval: {}.", new RevisionInterval(0.000000, endRevision).toString());

        RevisionHelper revisionHelper = new RevisionHelper();
        revisionHelper.setDatabaseService(getDatabaseHolder());
        revisionHelper.setRevisionRootHandler(getRevisionRootHandler());
        revisionHelper.setSuperRootHandler(getSuperRootHandler());
        revisionHelper.setSourceRootHandler(getSourceRootHandler());
        ProcessorResult<PruneState> pruneResult = revisionHelper.pruneToRevision(endRevision);

        Assert.assertFalse(new File(getSourceRootHandler().getSourceCodeDir(), file1Id).exists());
        Assert.assertTrue(new File(getSourceRootHandler().getSourceCodeDir(), file2Id).exists());

        Map<ActionType<PruneState, VertexType>, ActionCounter> vertexActions = pruneResult.getVertexActions();
        LOGGER.info(vertexActions.toString());
        Assert.assertEquals(3, vertexActions.size());
        Assert.assertEquals(1, vertexActions
                .get(new ActionType<PruneState, VertexType>(PruneState.DELETE, VertexType.NODE)).getCount());
        Assert.assertEquals(1, vertexActions
                .get(new ActionType<PruneState, VertexType>(PruneState.DELETE, VertexType.SOURCE_NODE)).getCount());
        Assert.assertEquals(1, vertexActions
                .get(new ActionType<PruneState, VertexType>(PruneState.DELETE, VertexType.ATTRIBUTE)).getCount());

        Map<ActionType<PruneState, EdgeLabel>, ActionCounter> edgeActions = pruneResult.getEdgeActions();
        LOGGER.info(edgeActions.toString());
        Assert.assertEquals(1, edgeActions.size());
        Assert.assertEquals(1,
                edgeActions.get(new ActionType<PruneState, EdgeLabel>(PruneState.DELETE, EdgeLabel.DIRECT)).getCount());
    }

}
