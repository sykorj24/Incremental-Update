package eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.junit.Assert;
import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.AttributeNames;
import eu.profinit.manta.dataflow.model.NodeType;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.GraphFlowAlgorithmExecutor;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.GraphFlowAlgorithmFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.routine.RoutineDataFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.GraphFlowFilter;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.EdgeIdentification;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration.InterpolationConfiguration;
import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration.NodeNameOrigin;
import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration.SourceAndTargetBasedName;
import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration.TargetBasedName;
import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration.TransformationBasedName;
import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration.TransformationNodeDef;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.ProcessorResult.ActionCounter;

/**
 * Unit testy {@link InterpolationHelper}.
 * 
 * @author onouza
 *
 */
public class InterpolationHelperTest extends TestTitanDatabaseProvider {
    
    private static final int DIAMONT_COUNT = 100;
    private static final int MEDIAN_VERTEX_COUNT_PER_DIAMOND = 20;
    
    /** Resource business transformace */
    private static final String[] BUSINESS_TRANSFORMATIONS_RESOURCE = new String[]{"Business Transformations", "Business Transformations", "Business Transformations"};
    
    private static final String MISSING_EDGE_ERROR_MESSAGE = "Unable to find expected interpolated edge between the given nodes."
            + " Please check: 1) The edge interpolation algorithm, 2) the edge label determination, 3) and that the edge 'interpolated' attribbute is set to 'true'.";

    private static final String UNEXPECTED_EDGE_ERROR_MESSAGE = "Unable to find expected interpolated edge between the given nodes."
            + " Please check: 1) The edge interpolation algorithm, 2) the edge label determination, 3) and that the edge 'interpolated' attribbute is set to 'true'.";

    

    /**
     * Provede test zakladni interpolace hran. Overi, zda spravne funguje "klasicky motyl".
     * 
     * <p>Vychozi situace:</p>
     * 
     * <pre>
     *    *                       *         Legenda: 
     *    |                       |         o - uzel ve fyzicke vrstve
     *    o           *           o         * - uzel v business vrstve
     *     \          |          /          Cary jsou hrany, orientovane nasledovne:
     *      o----o----o----o----o             o-o (hrana typu DIRECT nebo FILTER ve vyzicke vrstve, pro zjednoduseni v obrazku nerozlisujeme) 
     *     /          |          \                 - vzdy zleva doprava
     *    o           *           o           *-o (hrana typu MAPS_TO z uzlu v business vrstve do uzlu ve fyzicke vrstve)
     *    |                       |                - vzdy z "*" do "o"
     *    *                       *
     * </pre>
     * 
     * Nazvy prislusnych uzlu v kodu:  
     * 
     * <pre>
     *              (1)       (2)     (3)     (4)       (5)
     * 
     *  (a)    b_n1a *                                   * b_n5a
     *  (a)          |               b_n3a               |
     *  (a)    p_n1a o                 *                 o p_n5a
     *                \     p_n2  p_n3 |    p_n4        /
     *                 o-------o-------o-------o-------o
     *                /                |                \
     *  (b)    p_n1b o                 *                 o p_n5b
     *  (b)          |               b_n3b               |
     *  (b)    b_n1b *                                   * b_n5b
     * </pre>
     * 
     * Pro snazsi orientaci ve schematu vyse maji nazvy uzlu tvar (v)_n(s)(r), kde:
     * <ul>
     *   <li>(v) oznacuje vrstvu. Muze byt bud "p" (je-li uzel ve fyzicke vrstve) nebo "b" (je-li v business vrstve). </li>
     *   <li>(s) oznacuje "sloupec", do ktereho schematicky spada uzel v rozlozeni vyse.</li>
     *   <li>(r) oznacuje "pulku", do ktere schematicky spada uzel v rozlozeni vyse.
     *          Muze byt "a" (spada-li do horni "pulky"), nebo "b" (spada-li do spodni).
     *          Nebo nemusi byt uvedeno vubec, pak spada "doprostred".</li>
     * </ul>
     * <p>Priklad: Vidim-li v kodu nazev uzlu "b_n3a", pak se jedna u uzel business vrstvy, v horni "pulce" a tretim "sloupci" schematu vyse.</p>
     * 
     * <p>Ocekavany vysledek interpolace:</p>
     *   
     * <pre>
     *              (1)       (2)     (3)     (4)       (5)
     * 
     *  (a)    b_n1a *----------------   ----------------* b_n5a         
     *  (a)           \               \ /               /               
     *  (a)            \    -----------*-----------    /                   
     *                  \  /         b_n3a         \  /   
     *                   \/                         \/
     *                   /\                         /\
     *                  /  \         b_n3b         /  \
     *  (b)            /    -----------*-----------    \ 
     *  (b)           /               / \               \
     *  (b)    b_n1b *----------------   ----------------* b_n5b
     * </pre>
     * 
     * <p>Pro lepsi prehled (a omezene moznosti kreleni grafu v alfanumerickem rezimu :-)) je vynechana fyzika vrstva.
     *    Orientace hran je opet zleva doprava.</p>
     */
    @Test
    public void testInterpolation() {
        final InterpolationHelper interpolationHelper = createHelper();

        cleanGraph();
        final double revision = createMajorRevision();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                // Fyzicka vrstva
                
                Vertex physicalLayer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex resource1 = GraphCreation.createResource(transaction, root, "Teradata DB", "Teradata", "desc",
                        physicalLayer, revision);
                Vertex r1db1 = GraphCreation.createNode(transaction, resource1, "db1", "Database", revision);
                Vertex r1db1t1 = GraphCreation.createNode(transaction, r1db1, "table1", "Table", revision);
                Vertex pn1a = GraphCreation.createNode(transaction, r1db1t1, "p_n1a", "Column", revision);
                Vertex pn1b = GraphCreation.createNode(transaction, r1db1t1, "p_n1a", "Column", revision);
                Vertex pn2 = GraphCreation.createNode(transaction, r1db1t1, "p_n2", "Column", revision);
                Vertex pn3 = GraphCreation.createNode(transaction, r1db1t1, "p_n3", "Column", revision);
                Vertex pn4 = GraphCreation.createNode(transaction, r1db1t1, "p_n4", "Column", revision);
                Vertex pn5a = GraphCreation.createNode(transaction, r1db1t1, "p_n5a", "Column", revision);
                Vertex pn5b = GraphCreation.createNode(transaction, r1db1t1, "p_n5b", "Column", revision);

                GraphCreation.createEdge(pn1a, pn2, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(pn1b, pn2, EdgeLabel.FILTER, revision);
                GraphCreation.createEdge(pn2, pn3, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(pn3, pn4, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(pn4, pn5a, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(pn4, pn5b, EdgeLabel.FILTER, revision);

                // Business vrstva

                Vertex businessLayer = GraphCreation.createLayer(transaction, "Business", "Business");
                Vertex resource2 = GraphCreation.createResource(transaction, root, "Business Glossary", "Business Resource", "desc",
                        businessLayer, revision);
                Vertex r2a1 = GraphCreation.createNode(transaction, resource2, "r2a1", "Application", revision);
                Vertex r2a1s1 = GraphCreation.createNode(transaction, r2a1, "r2a1s1", "Layer", revision);
                Vertex r2a1s1a1 = GraphCreation.createNode(transaction, r2a1s1, "r2a1s1a1", "Asset", revision);
                Vertex bn1a = GraphCreation.createNode(transaction, r2a1s1a1, "b_n1a", "Attribute", revision);
                Vertex bn1b = GraphCreation.createNode(transaction, r2a1s1a1, "b_n1b", "Attribute", revision);
                Vertex bn3a = GraphCreation.createNode(transaction, r2a1s1a1, "b_n3a", "Attribute", revision);
                Vertex bn3b = GraphCreation.createNode(transaction, r2a1s1a1, "b_n3b", "Attribute", revision);
                Vertex bn5a = GraphCreation.createNode(transaction, r2a1s1a1, "b_n5a", "Attribute", revision);
                Vertex bn5b = GraphCreation.createNode(transaction, r2a1s1a1, "b_n5b", "Attribute", revision);

                // Mapovani uzlu business vrstvy na uzly fyzicke vrstvy
                
                GraphCreation.createEdge(bn1a, pn1a, EdgeLabel.MAPS_TO, revision);
                GraphCreation.createEdge(bn1b, pn1b, EdgeLabel.MAPS_TO, revision);
                GraphCreation.createEdge(bn3a, pn3, EdgeLabel.MAPS_TO, revision);
                GraphCreation.createEdge(bn3b, pn3, EdgeLabel.MAPS_TO, revision);
                GraphCreation.createEdge(bn5a, pn5a, EdgeLabel.MAPS_TO, revision);
                GraphCreation.createEdge(bn5b, pn5b, EdgeLabel.MAPS_TO, revision);
                
                return null;
            }
        });
        
        // Prvni interpolace - dosud nemame zadne interpolovane hrany, proto se hrana vzdy vytvori a zadna se nepreskoci.
        
        InterpolationResult firstInterpolationResult = interpolationHelper.performInterpolationInner("Business", "Physical", revision);

        Map<EdgeLabel, ActionCounter> interpolatedEdgeMap = firstInterpolationResult.getInterpolatedEdgeMap();
        Assert.assertEquals(2, interpolatedEdgeMap.size());
        Assert.assertEquals(4, interpolatedEdgeMap.get(EdgeLabel.DIRECT).getCount());
        Assert.assertEquals(4, interpolatedEdgeMap.get(EdgeLabel.FILTER).getCount());
        Map<EdgeLabel, ActionCounter> skippedEdgeMap = firstInterpolationResult.getSkippedEdgeMap();
        Assert.assertEquals(0, skippedEdgeMap.size());

        // Kontrola, zda jsou hrany spravne interpolovane a spravneho typu
        
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                
                String errorMessage = "Unable to find expected interpolated edge between the given nodes."
                        + " Please check: 1) The edge interpolation algorithm, 2) the edge label determination, 3) and that the edge 'interpolated' attribbute is set to 'true'.";

                RevisionInterval revisionInterval = new RevisionInterval(revision, revision);

                // Mapovani na fyzicke uzly spojene pouze DIRECT hranami => Interpolovane hrany musi byt rovnez DIRECT  
                Assert.assertNotNull(errorMessage, getEdge(transaction, "b_n1a", "b_n3a", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNotNull(errorMessage, getEdge(transaction, "b_n1a", "b_n3b", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNotNull(errorMessage, getEdge(transaction, "b_n3a", "b_n5a", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNotNull(errorMessage, getEdge(transaction, "b_n3b", "b_n5a", EdgeLabel.DIRECT, true, revisionInterval));

                // Mapovani na fyzicke uzly spojene DIRECT i FILTER hranami => Interpolovane hrany musi byt FILTER
                Assert.assertNotNull(errorMessage, getEdge(transaction, "b_n1b", "b_n3a", EdgeLabel.FILTER, true, revisionInterval));
                Assert.assertNotNull(errorMessage, getEdge(transaction, "b_n1b", "b_n3b", EdgeLabel.FILTER, true, revisionInterval));
                Assert.assertNotNull(errorMessage, getEdge(transaction, "b_n3a", "b_n5b", EdgeLabel.FILTER, true, revisionInterval));
                Assert.assertNotNull(errorMessage, getEdge(transaction, "b_n3b", "b_n5b", EdgeLabel.FILTER, true, revisionInterval));

                return null;
            }
        });

        // Druha interpolace - jelikoz uz jednou probehla, vsechny interpolovane hrany by uz mely existovat.
        //                     Ocekavame, ze zadna dalsi hrana se tedy jiz nevytvori, cili vsechny se "preskoci".
        
        InterpolationResult secondInterpolationresult = interpolationHelper.performInterpolationInner("Business", "Physical", revision);

        interpolatedEdgeMap = secondInterpolationresult.getInterpolatedEdgeMap();
        Assert.assertEquals(0, interpolatedEdgeMap.size());
        skippedEdgeMap = secondInterpolationresult.getSkippedEdgeMap();
        Assert.assertEquals(2, skippedEdgeMap.size());
        Assert.assertEquals(4, skippedEdgeMap.get(EdgeLabel.DIRECT).getCount());
        Assert.assertEquals(4, skippedEdgeMap.get(EdgeLabel.FILTER).getCount());
    }
    
    /**
     * Vykonnostni test interpolace hran.
     * 
     * <p>Vychozi situace:</p>
     * 
     * <pre>
     *              #           #                   #
     *             / \         / \                 / \
     *            / . \       / . \               / . \
     *           /  .  \     /  .  \             /  .  \
     *          /   .   \   /   .   \           /   .   \
     *         /         \ /         \         /         \
     *        S-----#-----o-----#-----o  ...  o-----#-----E
     *         \    .    / \    .    /         \    .    /
     *          \   .   /   \   .   /           \   .   /
     *           \  .  /     \  .  /             \  .  /
     *            \   /       \   /               \   /
     *             \ /         \ /                 \ /
     *              #           #                   #
     * </pre>
     * 
     * <p>Obrazek predstavuje fyzickou vrstvu, kde "S", "E", "o" a "#" jsou uzly. Hrany jsou oriantovany zleva doprava.
     * Graf obsahuje {@link #DIAMONT_COUNT} "diamantu". V kazdem "diamantu" se uzel "o" (pripadne "S")
     * rozbiha do {@link #MEDIAN_VERTEX_COUNT_PER_DIAMOND} "prostrednich" uzlu "#", ktere se opet sbihaji do uzlu "o" (pripadne "E").</p>    
     * 
     * <p>Cilem testu je ukazat, ze pokud jsou v business vrstve mapovany uzly na "S" a "E" a provedeme interpolaci hran,
     * bude interpolacni algoritmus optimalni, cili nebude prochazet stejne uzly vicekrat.</p> 
     */    
    @Test
    public void testInterpolationPerformance() {
        final InterpolationHelper interpolationHelper = createHelper();

        cleanGraph();
        final double revision = createMajorRevision();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                // Fyzicka vrstva
                
                Vertex physicalLayer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex resource1 = GraphCreation.createResource(transaction, root, "Teradata DB", "Teradata", "desc",
                        physicalLayer, revision);
                Vertex r1db1 = GraphCreation.createNode(transaction, resource1, "db1", "Database", revision);
                Vertex r1db1t1 = GraphCreation.createNode(transaction, r1db1, "table1", "Table", revision);
                Vertex r1db1p1 = GraphCreation.createNode(transaction, r1db1, "proc1", "Procedure", revision);
                
                Vertex pStartColumn = GraphCreation.createNode(transaction, r1db1t1, "p_column0", "Column", revision);
                Vertex fromColumn = pStartColumn;
                Vertex pEndColumn = null;
                for (int i = 1; i <= DIAMONT_COUNT; i++) {
                    pEndColumn = GraphCreation.createNode(transaction, r1db1t1, "p_column" + i, "Column", revision);
                    for (int j = 1; j <= MEDIAN_VERTEX_COUNT_PER_DIAMOND; j++) {
                        Vertex trans = GraphCreation.createNode(transaction, r1db1p1, "p_trans" + i + "_" + j, "Column", revision);
                        GraphCreation.createEdge(fromColumn, trans, EdgeLabel.DIRECT, revision);
                        GraphCreation.createEdge(trans, pEndColumn, EdgeLabel.DIRECT, revision);
                    }
                    fromColumn = pEndColumn;
                }

                // Business vrstva

                Vertex businessLayer = GraphCreation.createLayer(transaction, "Business", "Business");
                Vertex resource2 = GraphCreation.createResource(transaction, root, "Business Glossary", "Business Resource", "desc",
                        businessLayer, revision);
                Vertex r2a1 = GraphCreation.createNode(transaction, resource2, "r2a1", "Application", revision);
                Vertex r2a1s1 = GraphCreation.createNode(transaction, r2a1, "r2a1s1", "Layer", revision);
                Vertex r2a1s1a1 = GraphCreation.createNode(transaction, r2a1s1, "r2a1s1a1", "Asset", revision);
                Vertex bFromAttribute = GraphCreation.createNode(transaction, r2a1s1a1, "b_fromAttribute", "Attribute", revision);
                Vertex bToAttribute = GraphCreation.createNode(transaction, r2a1s1a1, "b_fromAttribute", "Attribute", revision);

                // Mapovani uzlu business vrstvy na uzly fyzicke vrstvy
                
                GraphCreation.createEdge(bFromAttribute, pStartColumn, EdgeLabel.MAPS_TO, revision);
                GraphCreation.createEdge(bToAttribute, pEndColumn, EdgeLabel.MAPS_TO, revision);
                
                return null;
            }
        });
        
        // Prvni interpolace - dosud nemame zadne interpolovane hrany, proto se hrana vzdy vytvori a zadna se nepreskoci.
        
        InterpolationResult firstInterpolationResult = interpolationHelper.performInterpolationInner("Business", "Physical", revision);

        Map<EdgeLabel, ActionCounter> interpolatedEdgeMap = firstInterpolationResult.getInterpolatedEdgeMap();
        // Mame pouze DIRECT hrany
        Assert.assertEquals(1, interpolatedEdgeMap.size());
        Assert.assertEquals(1, interpolatedEdgeMap.get(EdgeLabel.DIRECT).getCount());
        Map<EdgeLabel, ActionCounter> skippedEdgeMap = firstInterpolationResult.getSkippedEdgeMap();
        Assert.assertEquals(0, skippedEdgeMap.size());
    }
    
    /**
     * Test interpolace hran s business transformacemi.
     * 
     * 
     * <pre>
     *             Fyzicka vrstva             Business vrstva po interpolaci hran  
     *             
     *        (1)  (2)   (3)   (4)   (5)          (1)  (2)   (3)   (4)   (5)
     *            
     *                  p_n3a                                b_n3a                  
     * (a)            ----#----                            ----+----
     * (a)           /         \                          /         \
     * (a)          /           \                        /           \
     * (a)         /             \                      /             \
     *       p_n1 o---------------o p_n5    =>    b_n1 *---------------* b_n5
     * (b)         \             /                      \             /
     * (b)          \           /                        \           /
     * (b)           \         /                          \         /
     * (b)            #-------#                            --------+
     *             p_n2b     p_n4b                    (b_n2b)!    b_n4b
     *                                      
     *                                           ! b_n2b chybi, protoze p_n2b
     *                                           neni posledni transformaci
     *                                           na ceste z p_n1 do p_n5
     * </pre>
     * 
     * Pro snazsi orientaci ve schematu vyse maji nazvy uzlu tvar (v)_n(s)(r), kde:
     * <ul>
     *   <li>(v) oznacuje vrstvu. Muze byt bud "p" (je-li uzel ve fyzicke vrstve) nebo "b" (je-li v business vrstve). </li>
     *   <li>(s) oznacuje "sloupec", do ktereho schematicky spada uzel v rozlozeni vyse.</li>
     *   <li>(r) oznacuje "pulku", do ktere schematicky spada uzel v rozlozeni vyse.
     *          Muze byt "a" (spada-li do horni "pulky"), nebo "b" (spada-li do spodni).
     *          Nebo nemusi byt uvedeno vubec, pak spada "doprostred".</li>
     * </ul>
     * <p>Priklad: Vidim-li v kodu nazev uzlu "b_n3a", pak se jedna u uzel business vrstvy, v horni "pulce" a tretim "sloupci" schematu vyse.</p>
     * 
     */
    @Test
    public void testBusinessTransformations() {
        final InterpolationHelper interpolationHelper = createBusinessTransformationHelper();

        cleanGraph();
        final double revision = createMajorRevision();

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                // Fyzicka vrstva
                
                Vertex physicalLayer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex resource1 = GraphCreation.createResource(transaction, root, "Teradata DB", "Teradata", "desc",
                        physicalLayer, revision);
                Vertex r1db1 = GraphCreation.createNode(transaction, resource1, "db1", "Database", revision);
                Vertex r1db1t1 = GraphCreation.createNode(transaction, r1db1, "table1", "Table", revision);
                Vertex pn1 = GraphCreation.createNode(transaction, r1db1t1, "p_n1", "Column", revision);
                Vertex r1db1t2 = GraphCreation.createNode(transaction, r1db1, "table2", "Table", revision);
                Vertex pn5 = GraphCreation.createNode(transaction, r1db1t2, "p_n5", "Column", revision);
                Vertex r1db1ppn3a = GraphCreation.createNode(transaction, r1db1, "procedure_pn3a", "Procedure", revision);
                Vertex pn3a = GraphCreation.createNode(transaction, r1db1ppn3a, "p_n3a", "Column", revision);
                Vertex r1db1ppn2b = GraphCreation.createNode(transaction, r1db1, "procedure_pn2b", "Procedure", revision);
                Vertex pn2b = GraphCreation.createNode(transaction, r1db1ppn2b, "p_n2b", "Column", revision);
                Vertex r1db1fpn4b = GraphCreation.createNode(transaction, r1db1, "function_pn4b", "Function", revision);
                Vertex pn4b = GraphCreation.createNode(transaction, r1db1fpn4b, "p_n4b", "Column", revision);

                GraphCreation.createEdge(pn1, pn3a, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(pn3a, pn5, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(pn1, pn5, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(pn1, pn2b, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(pn2b, pn4b, EdgeLabel.FILTER, revision);
                GraphCreation.createEdge(pn4b, pn5, EdgeLabel.DIRECT, revision);

                // Business vrstva

                Vertex businessLayer = GraphCreation.createLayer(transaction, "Business", "Business");
                Vertex resource2 = GraphCreation.createResource(transaction, root, "Business Glossary", "Business Resource", "desc",
                        businessLayer, revision);
                Vertex r2a1 = GraphCreation.createNode(transaction, resource2, "r2a1", "Application", revision);
                Vertex r2a1s1 = GraphCreation.createNode(transaction, r2a1, "r2a1s1", "Layer", revision);
                Vertex r2a1s1a1 = GraphCreation.createNode(transaction, r2a1s1, "Asset 1", "Asset", revision);
                Vertex bn1 = GraphCreation.createNode(transaction, r2a1s1a1, "b_n1", "Attribute", revision);
                Vertex r2a1s1a2 = GraphCreation.createNode(transaction, r2a1s1, "Asset 2", "Asset", revision);
                Vertex bn5 = GraphCreation.createNode(transaction, r2a1s1a2, "b_n5", "Attribute", revision);

                List<?> transformationMapping = new ArrayList<>(Arrays.asList(BUSINESS_TRANSFORMATIONS_RESOURCE, Collections.singletonList("function_pn4b"), "Funkce pe en ctyri be", Collections.singletonMap("Calculation Description", "no proste vypocet...")));
                GraphCreation.createNodeAttribute(transaction, bn5, AttributeNames.NODE_TRANSFORMATION, transformationMapping, revision);

                // Mapovani uzlu business vrstvy na uzly fyzicke vrstvy
                
                GraphCreation.createEdge(bn1, pn1, EdgeLabel.MAPS_TO, revision);
                GraphCreation.createEdge(bn5, pn5, EdgeLabel.MAPS_TO, revision);
                
                return null;
            }
        });
        
        // Prvni interpolace - dosud nemame zadne interpolovane hrany, proto se hrana vzdy vytvori a zadna se nepreskoci.
        
        InterpolationResult firstInterpolationResult = interpolationHelper.performInterpolationInner("Business", "Physical", revision);

        Map<EdgeLabel, ActionCounter> interpolatedEdgeMap = firstInterpolationResult.getInterpolatedEdgeMap();
        Assert.assertEquals(2, interpolatedEdgeMap.size());
        Assert.assertEquals(3, interpolatedEdgeMap.get(EdgeLabel.DIRECT).getCount());
        Assert.assertEquals(2, interpolatedEdgeMap.get(EdgeLabel.FILTER).getCount());
        Map<EdgeLabel, ActionCounter> skippedEdgeMap = firstInterpolationResult.getSkippedEdgeMap();
        Assert.assertEquals(0, skippedEdgeMap.size());
        
        // Ocekavame nove vytvorene transformacni uzly a nektere preskocene (typicky uzly vyse v hiererchii)
        Map<VertexType, ActionCounter> addedTransformationVertexMap = firstInterpolationResult.getAddedTransformationVertexMap();
        Assert.assertEquals(2, addedTransformationVertexMap.size());
        Assert.assertEquals(1, firstInterpolationResult.getSkippedTransformationVertexMap().size());
        Assert.assertEquals(0, firstInterpolationResult.getErrorTransformationVertexMap().size());

        // Kontrola, zda jsou hrany spravne interpolovane a spravneho typu
        
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                
                RevisionInterval revisionInterval = new RevisionInterval(revision, revision);

                // Mapovani na fyzicke uzly spojene pouze DIRECT hranami => Interpolovane hrany musi byt rovnez DIRECT  
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n5", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n5", "procedure_pn3a", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n5", "procedure_pn3a", "Transformation", "b_n5", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));

                // Mapovani na fyzicke uzly spojene DIRECT i FILTER hranami => Interpolovane hrany musi byt FILTER
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n5", "Funkce pe en ctyri be", "Transformation", EdgeLabel.FILTER, true, revisionInterval));
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n5", "Funkce pe en ctyri be", "Transformation", "b_n5", "Asset 2", "Asset", EdgeLabel.FILTER, true, revisionInterval));

                // Kontrola, zda nejake hrany neprebyvaji
                
                // procedure_pn2b neni posledni transformaci z p_n1 do p_n5 => nema co vytvaret business transformaci z b_n1 do b_n5
                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n5", "procedure_pn2b", "Transformation", EdgeLabel.FILTER, true, revisionInterval));
                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n5", "procedure_pn2b", "Transformation", "b_n5", "Asset 2", "Asset", EdgeLabel.FILTER, true, revisionInterval));
                
                // Nesmi existovat FILTER interpolovane hrany mezi uzly, kde se dle pravidel vytvorily DIRECT
                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n5", "Asset 2", "Asset", EdgeLabel.FILTER, true, revisionInterval));
                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n5", "procedure_pn3a", "Transformation", EdgeLabel.FILTER, true, revisionInterval));
                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n5", "procedure_pn3a", "Transformation", "b_n5", "Asset 2", "Asset", EdgeLabel.FILTER, true, revisionInterval));

                // Nesmi existovat DIRECT interpolovane hrany mezi uzly, kde se dle pravidel vytvorily FILTER
                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n5", "Funkce pe en ctyri be", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n5", "Funkce pe en ctyri be", "Transformation", "b_n5", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));
                
                // Kontrola transformacniho atributu
                Vertex transformationVertex = findNodeByNameAndParent(transaction, "b_n5", "Funkce pe en ctyri be", "Transformation", revisionInterval);
                Assert.assertNotNull(transformationVertex);
                List<Object> attribute = GraphOperation.getNodeAttribute(transformationVertex, "Calculation Description", revisionInterval);
                Assert.assertNotNull(attribute);
                Assert.assertEquals(1, attribute.size());
                Assert.assertEquals("no proste vypocet...", attribute.get(0));
                
                // Kontrola resourcu transformacniho uzlu
                
                Vertex transformationResource = GraphOperation.getResource(transformationVertex);
                Assert.assertNotNull(transformationResource);
                Assert.assertEquals("Business Transformations", GraphOperation.getName(transformationResource));
                
                return null;
            }
        });

        // Druha interpolace - jelikoz uz jednou probehla, vsechny interpolovane hrany by uz mely existovat.
        //                     Ocekavame, ze zadna dalsi hrana se tedy jiz nevytvori, cili vsechny se "preskoci".
        
        InterpolationResult secondInterpolationresult = interpolationHelper.performInterpolationInner("Business", "Physical", revision);

        interpolatedEdgeMap = secondInterpolationresult.getInterpolatedEdgeMap();
        Assert.assertEquals(0, interpolatedEdgeMap.size());
        skippedEdgeMap = secondInterpolationresult.getSkippedEdgeMap();
        Assert.assertEquals(2, skippedEdgeMap.size());
        Assert.assertEquals(3, skippedEdgeMap.get(EdgeLabel.DIRECT).getCount());
        Assert.assertEquals(2, skippedEdgeMap.get(EdgeLabel.FILTER).getCount());

        // Vsechny transformacni uzly by mely  byt taktez vytvorene => preskoci se
        Assert.assertEquals(2, secondInterpolationresult.getSkippedTransformationVertexMap().size());
        Assert.assertEquals(0, secondInterpolationresult.getAddedTransformationVertexMap().size());
        Assert.assertEquals(0, secondInterpolationresult.getErrorTransformationVertexMap().size());
    }
    
    /**
     * Test interpolace hran s business transformacemi.
     * Specialni pripad, kdy v transformacnim mapovani neni uvedeno jmeno business transformace.
     * Ocekava se pouziti fyzickeho jmena a vyplneni atributu transformace. 
     * 
     * <pre>
     *             Fyzicka vrstva             Business vrstva po interpolaci hran  
     *             
     *        (1)        (2)         (3)          (1)        (2)         (3)
     *            
     *                  p_n2                                 b_n2                  
     *       p_n1 o-------#-------o p_n3    =>    b_n1 *-------+-------* b_n3
     *                                      
     * </pre>
     * 
     * Vysvetlivky ke schematu viz {@link #testBusinessTransformations()}.
     */
    @Test
    public void testBusinessTransformationsNoBusinessTransformationName() {
        final InterpolationHelper interpolationHelper = createBusinessTransformationHelper();

        cleanGraph();
        final double revision = createMajorRevision();
        final RevisionInterval revisionInterval = new RevisionInterval(revision);

        createCommonDatabase(revision);
        
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex targetVertex = findNodeByNameAndParent(transaction, "b_n3", "Asset 2", "Asset", revisionInterval);
                // Prave jedno mapovani, bez vyplnene business transformace 
                List<?> transformationMapping = new ArrayList<>(Arrays.asList(BUSINESS_TRANSFORMATIONS_RESOURCE, new ArrayList<>(Arrays.asList("db1", "procedure_pn2")), null, Collections.singletonMap("Calculation Description", "no proste vypocet...")));
                GraphCreation.createNodeAttribute(transaction, targetVertex, AttributeNames.NODE_TRANSFORMATION, transformationMapping, revision);
                return null;
            }
        });
        
        interpolationHelper.performInterpolationInner("Business", "Physical", revision);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                // Kontrola vytvoreni ocekavane transformace a prislusnych hran  
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n3", "procedure_pn2", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n3", "procedure_pn2", "Transformation", "b_n3", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));

                // Kontrola vytvoreni ocekavaneho transformacniho atributu  
                Vertex transformationVertex = findNodeByNameAndParent(transaction, "b_n3", "procedure_pn2", "Transformation", revisionInterval);
                Assert.assertNotNull(transformationVertex);
                List<Object> attribute = GraphOperation.getNodeAttribute(transformationVertex, "Calculation Description", revisionInterval);
                Assert.assertNotNull(attribute);
                Assert.assertEquals(1, attribute.size());
                Assert.assertEquals("no proste vypocet...", attribute.get(0));

                return null;
            }
        });
        
    }
    
    /**
     * Test interpolace hran s business transformacemi.
     * Specialni pripad, kdy v transformacnim mapovani neni uvedena fyzicka transformace,
     * pricemz jine nez toto mapovani neexistuje. 
     * Ocekava se pouziti business jmena. 
     * 
     * <pre>
     *             Fyzicka vrstva             Business vrstva po interpolaci hran  
     *             
     *        (1)        (2)         (3)          (1)        (2)         (3)
     *            
     *                  p_n2                                 b_n2                  
     *       p_n1 o-------#-------o p_n3    =>    b_n1 *-------+-------* b_n3
     *                                      
     * </pre>
     * 
     * Vysvetlivky ke schematu viz {@link #testBusinessTransformations()}.
     */
    @Test
    public void testBusinessTransformationsNoPhysicalTransformationName() {
        final InterpolationHelper interpolationHelper = createBusinessTransformationHelper();

        cleanGraph();
        final double revision = createMajorRevision();
        final RevisionInterval revisionInterval = new RevisionInterval(revision, revision);

        createCommonDatabase(revision);
        
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex targetVertex = findNodeByNameAndParent(transaction, "b_n3", "Asset 2", "Asset", revisionInterval);
                // Prave jedno mapovani, bez vyplnene fyzicke transformace 
                List<?> transformationMapping = new ArrayList<>(Arrays.asList(BUSINESS_TRANSFORMATIONS_RESOURCE, Collections.emptyList(), "Procka pe en dva"));
                GraphCreation.createNodeAttribute(transaction, targetVertex, AttributeNames.NODE_TRANSFORMATION, transformationMapping, revision);
                return null;
            }
        });
        
        interpolationHelper.performInterpolationInner("Business", "Physical", revision);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                // Kontrola vytvoreni ocekavane transformace a prislusnych hran  
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n3", "Procka pe en dva", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n3", "Procka pe en dva", "Transformation", "b_n3", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));

                // Kontrola, zda se nevytvorila nezadouci transformace a hrany navic  
                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n3", "procedure_pn2", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n3", "procedure_pn2", "Transformation", "b_n3", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));
                return null;
            }
        });
        
    }
    
    /**
     * Test interpolace hran s business transformacemi.
     * Specialni pripad, kdy existuji dve duplicitni transformacni mapovani, odlisna pouze hodnotou transformacniho atributu.
     * Ocekava se vytvoreni jedne business transformace, s nastavenim obou hodnot transformacniho atributu. 
     * 
     * <pre>
     *             Fyzicka vrstva             Business vrstva po interpolaci hran  
     *             
     *        (1)        (2)         (3)          (1)        (2)         (3)
     *            
     *                  p_n2                                 b_n2                  
     *       p_n1 o-------#-------o p_n3    =>    b_n1 *-------+-------* b_n3
     *                                      
     * </pre>
     * 
     * Vysvetlivky ke schematu viz {@link #testBusinessTransformations()}.
     */
    @Test
    public void testBusinessTransformationsDuplicateMappingExpectCalculationDescription() {
        final InterpolationHelper interpolationHelper = createBusinessTransformationHelper();

        cleanGraph();
        final double revision = createMajorRevision();
        final RevisionInterval revisionInterval = new RevisionInterval(revision, revision);

        createCommonDatabase(revision);
        
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex targetVertex = findNodeByNameAndParent(transaction, "b_n3", "Asset 2", "Asset", revisionInterval);
                
                // Dve mapovani, odlisna pouze v atributu

                List<?> transformationMapping = new ArrayList<>(Arrays.asList(BUSINESS_TRANSFORMATIONS_RESOURCE, new ArrayList<>(Arrays.asList("db1", "procedure_pn2")), "Procka pe en dva", Collections.singletonMap("Calculation Description", "no proste vypocet... 1")));
                GraphCreation.createNodeAttribute(transaction, targetVertex, AttributeNames.NODE_TRANSFORMATION, transformationMapping, revision);
                
                transformationMapping = new ArrayList<>(Arrays.asList(BUSINESS_TRANSFORMATIONS_RESOURCE, new ArrayList<>(Arrays.asList("db1", "procedure_pn2")), "Procka pe en dva", Collections.singletonMap("Calculation Description", "no proste vypocet... 2")));
                GraphCreation.createNodeAttribute(transaction, targetVertex, AttributeNames.NODE_TRANSFORMATION, transformationMapping, revision);
                
                return null;
            }
        });
        
        interpolationHelper.performInterpolationInner("Business", "Physical", revision);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                // Kontrola vytvoreni ocekavane transformace a prislusnych hran  
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n3", "Procka pe en dva", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n3", "Procka pe en dva", "Transformation", "b_n3", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));

                // Kontrola atributu, zda ma nastaveny hodnoty z obou mapovani 
                Vertex transformationVertex = findNodeByNameAndParent(transaction, "b_n3", "Procka pe en dva", "Transformation", revisionInterval);
                Assert.assertNotNull(transformationVertex);
                List<Object> attribute = GraphOperation.getNodeAttribute(transformationVertex, "Calculation Description", revisionInterval);
                Assert.assertNotNull(attribute);
                Assert.assertEquals(2, attribute.size());
                Assert.assertTrue(((String)attribute.get(0)).startsWith("no proste vypocet..."));
                Assert.assertTrue(((String)attribute.get(1)).startsWith("no proste vypocet..."));
                Assert.assertNotEquals(attribute.get(0), attribute.get(1));

                return null;
            }
        });
        
    }
    
    /**
     * Test interpolace hran s business transformacemi.
     * Specialni pripad, kdy existuji dve duplicitni transformacni mapovani, odlisna pouze nazvem business transformace.
     * Ocekava se vytvoreni dvou business transformaci, s nastavenim hodnot transformacniho atributu kazde z nich. 
     * 
     * <pre>
     *             Fyzicka vrstva             Business vrstva po interpolaci hran  
     *             
     *        (1)        (2)         (3)          (1)        (2)         (3)
     *            
     *                                                       b_n2a
     *                                                   ------+------
     *                  p_n2                            /             \      
     *       p_n1 o-------#-------o p_n3    =>    b_n1 *               * b_n3
     *                                                  \             /
     *                                                   ------+------
     *                                                       b_n2b
     *                                      
     * </pre>
     * 
     * Vysvetlivky ke schematu viz {@link #testBusinessTransformations()}.
     */
    @Test
    public void testBusinessTransformationsDuplicateMappingExpectBusinessTransformation() {
        final InterpolationHelper interpolationHelper = createBusinessTransformationHelper();

        cleanGraph();
        final double revision = createMajorRevision();
        final RevisionInterval revisionInterval = new RevisionInterval(revision, revision);

        createCommonDatabase(revision);
        
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex targetVertex = findNodeByNameAndParent(transaction, "b_n3", "Asset 2", "Asset", revisionInterval);
                
                // Dve mapovani, odlisna pouze v nazvu business transformace

                List<?> transformationMapping = new ArrayList<>(Arrays.asList(BUSINESS_TRANSFORMATIONS_RESOURCE, new ArrayList<>(Arrays.asList("db1", "procedure_pn2")), "Procka pe en dva 1", Collections.singletonMap("Calculation Description", "no proste vypocet...")));
                GraphCreation.createNodeAttribute(transaction, targetVertex, AttributeNames.NODE_TRANSFORMATION, transformationMapping, revision);
                
                transformationMapping = new ArrayList<>(Arrays.asList(BUSINESS_TRANSFORMATIONS_RESOURCE, new ArrayList<>(Arrays.asList("db1", "procedure_pn2")), "Procka pe en dva 2", Collections.singletonMap("Calculation Description", "no proste vypocet...")));
                GraphCreation.createNodeAttribute(transaction, targetVertex, AttributeNames.NODE_TRANSFORMATION, transformationMapping, revision);
                
                return null;
            }
        });
        
        interpolationHelper.performInterpolationInner("Business", "Physical", revision);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                // Kontrola vytvoreni prvni ocekavane transformace a prislusnych hran  
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n3", "Procka pe en dva 1", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n3", "Procka pe en dva 1", "Transformation", "b_n3", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));

                // Kontrola ocekavaneho atributu prvni transformace  
                Vertex transformationVertex = findNodeByNameAndParent(transaction, "b_n3", "Procka pe en dva 1", "Transformation", revisionInterval);
                Assert.assertNotNull(transformationVertex);
                List<Object> attribute = GraphOperation.getNodeAttribute(transformationVertex, "Calculation Description", revisionInterval);
                Assert.assertNotNull(attribute);
                Assert.assertEquals(1, attribute.size());
                Assert.assertEquals("no proste vypocet...", attribute.get(0));

                // Kontrola vytvoreni druhe ocekavane transformace a prislusnych hran  
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n3", "Procka pe en dva 2", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n3", "Procka pe en dva 2", "Transformation", "b_n3", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));

                // Kontrola ocekavaneho atributu druhe transformace  
                transformationVertex = findNodeByNameAndParent(transaction, "b_n3", "Procka pe en dva 2", "Transformation", revisionInterval);
                Assert.assertNotNull(transformationVertex);
                attribute = GraphOperation.getNodeAttribute(transformationVertex, "Calculation Description", revisionInterval);
                Assert.assertNotNull(attribute);
                Assert.assertEquals(1, attribute.size());
                Assert.assertEquals("no proste vypocet...", attribute.get(0));

                return null;
            }
        });
        
    }
    
    /**
     * Test interpolace hran s business transformacemi.
     * Specialni pripad, kdy existuji dve fyzicke transformace a mapovani pro kazdou z nich,
     * pricemz se v techto mapovanich lisi hodnota transformacniho atributu.
     * Ocekava se vytvoreni jedna business transformace, s nastavenim transformacino atributu na hodnoty z obou mapovani. 
     * 
     * <pre>
     *             Fyzicka vrstva             Business vrstva po interpolaci hran  
     *             
     *        (1)        (2)         (3)          (1)        (2)         (3)
     *            
     *                  p_n2a                                 
     *              ------#------                             
     *             /             \                           b_n2                         
     *       p_n1 o               o p_n3    =>    b_n1 *-------+-------* b_n3
     *             \             /           
     *              ------#------            
     *                  p_n2b
     *                                      
     * </pre>
     * 
     * Vysvetlivky ke schematu viz {@link #testBusinessTransformations()}.
     */
    @Test
    public void testBusinessTransformationsPhysicalTransformationsMerge() {
        final InterpolationHelper interpolationHelper = createBusinessTransformationHelper();

        cleanGraph();
        final double revision = createMajorRevision();
        final RevisionInterval revisionInterval = new RevisionInterval(revision, revision);

        createCommonDatabase(revision);
        
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                // Druha procedura - fyzicka transformace
                Vertex r1db1 = GraphOperation.getVerticesWithName(transaction, "db1", revisionInterval, 1).get(0);
                Vertex r1db1ppn2 = GraphCreation.createNode(transaction, r1db1 , "procedure_pn2_other", "Procedure", revision);
                Vertex pn2 = GraphCreation.createNode(transaction, r1db1ppn2, "p_n2", "Column", revision);
                // a jeji spojeni hranami s fyzickymi tabulkami
                Vertex pn1 = findNodeByNameAndParent(transaction, "p_n1", "table1", "Table", revisionInterval);
                GraphCreation.createEdge(pn1, pn2, EdgeLabel.DIRECT, revision);
                Vertex pn3 = findNodeByNameAndParent(transaction, "p_n3", "table2", "Table", revisionInterval);
                GraphCreation.createEdge(pn2, pn3, EdgeLabel.DIRECT, revision);
                
                Vertex targetVertex = findNodeByNameAndParent(transaction, "b_n3", "Asset 2", "Asset", revisionInterval);
                
                // Transformacni mapovani pro prvni proceduru
                List<?> transformationMapping = new ArrayList<>(Arrays.asList(BUSINESS_TRANSFORMATIONS_RESOURCE, new ArrayList<>(Arrays.asList("db1", "procedure_pn2")), "Procka pe en dva", Collections.singletonMap("Calculation Description", "no proste vypocet... 1")));
                GraphCreation.createNodeAttribute(transaction, targetVertex, AttributeNames.NODE_TRANSFORMATION, transformationMapping, revision);
                
                // Transformacni mapovani pro druhou proceduru
                transformationMapping = new ArrayList<>(Arrays.asList(BUSINESS_TRANSFORMATIONS_RESOURCE, new ArrayList<>(Arrays.asList("db1", "procedure_pn2_other")), "Procka pe en dva", Collections.singletonMap("Calculation Description", "no proste vypocet... 2")));
                GraphCreation.createNodeAttribute(transaction, targetVertex, AttributeNames.NODE_TRANSFORMATION, transformationMapping, revision);

                return null;
            }
        });
        
        interpolationHelper.performInterpolationInner("Business", "Physical", revision);
        
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                // Kontrola vytvoreni ocekavane transformace a prislusnych hran  
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n3", "Procka pe en dva", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n3", "Procka pe en dva", "Transformation", "b_n3", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));

                // Kontrola atributu, zda ma nastaveny hodnoty z obou mapovani
                Vertex transformationVertex = findNodeByNameAndParent(transaction, "b_n3", "Procka pe en dva", "Transformation", revisionInterval);
                Assert.assertNotNull(transformationVertex);
                List<Object> attribute = GraphOperation.getNodeAttribute(transformationVertex, "Calculation Description", revisionInterval);
                Assert.assertNotNull(attribute);
                Assert.assertEquals(2, attribute.size());
                Assert.assertTrue(((String)attribute.get(0)).startsWith("no proste vypocet..."));
                Assert.assertTrue(((String)attribute.get(1)).startsWith("no proste vypocet..."));
                Assert.assertNotEquals(attribute.get(0), attribute.get(1));

                return null;
            }
        });
    }
    
    /**
     * Test interpolace hran s business transformacemi.
     * Specialni pripad, kdy v transformacnim mapovani neni vyplnena fyzicka transformace
     * a existuje dalsi mapovani tykajici se jine nez zkoumane transformace.
     * Ocekava se pouziti fyzicke transformace, bez vyplneni transformacnich atributu. 
     * 
     * <pre>
     *             Fyzicka vrstva             Business vrstva po interpolaci hran  
     *             
     *        (1)        (2)         (3)          (1)        (2)         (3)
     *            
     *                  p_n2                                 b_n2                  
     *       p_n1 o-------#-------o p_n3    =>    b_n1 *-------+-------* b_n3
     *                                      
     * </pre>
     * 
     * Vysvetlivky ke schematu viz {@link #testBusinessTransformations()}.
     */
    @Test
    public void testBusinessTransformationsNoPhysicalTransformationNameAndOtherTransformations() {
        final InterpolationHelper interpolationHelper = createBusinessTransformationHelper();

        cleanGraph();
        final double revision = createMajorRevision();
        final RevisionInterval revisionInterval = new RevisionInterval(revision, revision);

        createCommonDatabase(revision);
        
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex targetVertex = findNodeByNameAndParent(transaction, "b_n3", "Asset 2", "Asset", revisionInterval);

                // Prvni transformacni mapovani, bez vyplnene fyzicke transformace
                List<?> transformationMapping = new ArrayList<>(Arrays.asList(BUSINESS_TRANSFORMATIONS_RESOURCE, Collections.emptyList(), "Procka pe en dva"));
                GraphCreation.createNodeAttribute(transaction, targetVertex, AttributeNames.NODE_TRANSFORMATION, transformationMapping, revision);
                
                // Druhe transformacni mapovani, s vyplnenou jinou fyzickou transformaci
                transformationMapping = new ArrayList<>(Arrays.asList(BUSINESS_TRANSFORMATIONS_RESOURCE, Collections.singletonList("any_other_procedure"), "Nejaka jina procka"));
                GraphCreation.createNodeAttribute(transaction, targetVertex, AttributeNames.NODE_TRANSFORMATION, transformationMapping, revision);
                
                return null;
            }
        });
        
        interpolationHelper.performInterpolationInner("Business", "Physical", revision);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                // Kontrola vytvoreni ocekavane transformace a prislusnych hran  
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n3", "procedure_pn2", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n3", "procedure_pn2", "Transformation", "b_n3", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));

                // Kontrola, zda se nevytvorila nezadouci transformace a hrany navic  
                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n3", "Procka pe en dva", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n3", "Procka pe en dva", "Transformation", "b_n3", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));

                // Kontrola, zda nedoslo k nezadoucimu vyplneni transformacniho atributu
                Vertex transformationVertex = findNodeByNameAndParent(transaction, "b_n3", "procedure_pn2", "Transformation", revisionInterval);
                Assert.assertNotNull(transformationVertex);
                List<Object> attribute = GraphOperation.getNodeAttribute(transformationVertex, "Calculation Description", revisionInterval);
                Assert.assertNotNull(attribute);
                Assert.assertTrue(attribute.isEmpty());

                // Kontrola resourcu transformacniho uzlu
                
                Vertex transformationResource = GraphOperation.getResource(transformationVertex);
                Assert.assertNotNull(transformationResource);
                Assert.assertEquals("Business Transformations", GraphOperation.getName(transformationResource));
                
                return null;
            }
        });
        
    }
    
    /**
     * Test interpolace hran s business transformacemi.
     * Specialni pripad, kdy existuji dve transformacni mapovani bez vyplnene fyzicke transformace.
     * Ocekava se pouziti fyzicke transformace, bez vyplneni transformacnich atributu. 
     * 
     * <pre>
     *             Fyzicka vrstva             Business vrstva po interpolaci hran  
     *             
     *        (1)        (2)         (3)          (1)        (2)         (3)
     *            
     *                  p_n2                                 b_n2                  
     *       p_n1 o-------#-------o p_n3    =>    b_n1 *-------+-------* b_n3
     *                                      
     * </pre>
     * 
     * Vysvetlivky ke schematu viz {@link #testBusinessTransformations()}.
     */
    @Test
    public void testBusinessTransformationsNoPhysicalTransformationNameMultiple() {
        final InterpolationHelper interpolationHelper = createBusinessTransformationHelper();

        cleanGraph();
        final double revision = createMajorRevision();
        final RevisionInterval revisionInterval = new RevisionInterval(revision, revision);

        createCommonDatabase(revision);
        
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex targetVertex = findNodeByNameAndParent(transaction, "b_n3", "Asset 2", "Asset", revisionInterval);

                // Dve transformacni mapovani, bez vyplnene fyzicke transformace, s ruznym nazvem business transformace

                List<?> transformationMapping = new ArrayList<>(Arrays.asList(BUSINESS_TRANSFORMATIONS_RESOURCE, Collections.emptyList(), "Procka pe en dva"));
                GraphCreation.createNodeAttribute(transaction, targetVertex, AttributeNames.NODE_TRANSFORMATION, transformationMapping, revision);
                
                transformationMapping = new ArrayList<>(Arrays.asList(BUSINESS_TRANSFORMATIONS_RESOURCE, Collections.emptyList(), "Nejaka jina procka"));
                GraphCreation.createNodeAttribute(transaction, targetVertex, AttributeNames.NODE_TRANSFORMATION, transformationMapping, revision);
                
                return null;
            }
        });
        
        interpolationHelper.performInterpolationInner("Business", "Physical", revision);

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                // Kontrola vytvoreni ocekavane transformace a prislusnych hran  
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n3", "procedure_pn2", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNotNull(MISSING_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n3", "procedure_pn2", "Transformation", "b_n3", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));

                // Kontrola, zda se nevytvorily nezadouci transformace a hrany navic  

                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n3", "Procka pe en dva", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n3", "Procka pe en dva", "Transformation", "b_n3", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));

                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n1", "Asset 1", "Asset", "b_n3", "Nejaka jina procka", "Transformation", EdgeLabel.DIRECT, true, revisionInterval));
                Assert.assertNull(UNEXPECTED_EDGE_ERROR_MESSAGE, getEdge(transaction, "b_n3", "Nejaka jina procka", "Transformation", "b_n3", "Asset 2", "Asset", EdgeLabel.DIRECT, true, revisionInterval));

                // Kontrola, zda nedoslo k nezadoucimu vyplneni transformacniho atributu

                Vertex transformationVertex = findNodeByNameAndParent(transaction, "b_n3", "procedure_pn2", "Transformation", revisionInterval);
                Assert.assertNotNull(transformationVertex);
                List<Object> attribute = GraphOperation.getNodeAttribute(transformationVertex, "Calculation Description", revisionInterval);
                Assert.assertNotNull(attribute);
                Assert.assertTrue(attribute.isEmpty());

                return null;
            }
        });
        
    }
    
    // Pomocne privatni metody
    
    /**
     * Vytvori zakladni databazi pro testy.
     * 
     * @param revision Cislo revize, se kterou pracujeme.
     */
    private void createCommonDatabase(final Double revision) {
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_SHARE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex root = getSuperRootHandler().getRoot(transaction);

                // Fyzicka vrstva
                
                Vertex physicalLayer = GraphCreation.createLayer(transaction, "Physical", "Physical");
                Vertex resource1 = GraphCreation.createResource(transaction, root, "Teradata DB", "Teradata", "desc",
                        physicalLayer, revision);
                Vertex r1db1 = GraphCreation.createNode(transaction, resource1, "db1", "Database", revision);
                Vertex r1db1t1 = GraphCreation.createNode(transaction, r1db1, "table1", "Table", revision);
                Vertex pn1 = GraphCreation.createNode(transaction, r1db1t1, "p_n1", "Column", revision);
                Vertex r1db1t2 = GraphCreation.createNode(transaction, r1db1, "table2", "Table", revision);
                Vertex pn3 = GraphCreation.createNode(transaction, r1db1t2, "p_n3", "Column", revision);
                Vertex r1db1ppn2 = GraphCreation.createNode(transaction, r1db1, "procedure_pn2", "Procedure", revision);
                Vertex pn2 = GraphCreation.createNode(transaction, r1db1ppn2, "p_n2", "Column", revision);

                GraphCreation.createEdge(pn1, pn2, EdgeLabel.DIRECT, revision);
                GraphCreation.createEdge(pn2, pn3, EdgeLabel.DIRECT, revision);

                // Business vrstva

                Vertex businessLayer = GraphCreation.createLayer(transaction, "Business", "Business");
                Vertex resource2 = GraphCreation.createResource(transaction, root, "Business Glossary", "Business Resource", "desc",
                        businessLayer, revision);
                Vertex r2a1 = GraphCreation.createNode(transaction, resource2, "r2a1", "Application", revision);
                Vertex r2a1s1 = GraphCreation.createNode(transaction, r2a1, "r2a1s1", "Layer", revision);
                Vertex r2a1s1a1 = GraphCreation.createNode(transaction, r2a1s1, "Asset 1", "Asset", revision);
                Vertex bn1 = GraphCreation.createNode(transaction, r2a1s1a1, "b_n1", "Attribute", revision);
                Vertex r2a1s1a2 = GraphCreation.createNode(transaction, r2a1s1, "Asset 2", "Asset", revision);
                Vertex bn3 = GraphCreation.createNode(transaction, r2a1s1a2, "b_n3", "Attribute", revision);

                // Mapovani uzlu business vrstvy na uzly fyzicke vrstvy
                
                GraphCreation.createEdge(bn1, pn1, EdgeLabel.MAPS_TO, revision);
                GraphCreation.createEdge(bn3, pn3, EdgeLabel.MAPS_TO, revision);
                return null;
            }
        });
    }

    /**
     * @return Nove vytvorena instance {@link InterpolationHelper}.
     */
    private InterpolationHelper createHelper() {
        InterpolationHelper helper = new InterpolationHelper();
        helper.setFlowAlgorithmExecutor(createFlowAlgorithmExecutor());
        helper.setDatabaseService(getDatabaseHolder());
        helper.setRevisionRootHandler(getRevisionRootHandler());
        helper.setSuperRootHandler(getSuperRootHandler());
        InterpolationConfiguration configuration = new InterpolationConfiguration();
        helper.setConfiguration(configuration );
        return helper;
    }
    
    /**
     * @return Nove vytvorena instance {@link InterpolationHelper}
     *          s konfiguraci pro business transformace.
     */
    private InterpolationHelper createBusinessTransformationHelper() {
        InterpolationHelper helper = createHelper();
        InterpolationConfiguration configuration = new InterpolationConfiguration();
        helper.setConfiguration(configuration);
        configuration.setTransformations(Arrays.asList(NodeType.PROCEDURE.getId(), NodeType.FUNCTION.getId()));
        configuration.setTransformationNodes(Arrays.asList(
                transformationNode("Application", sourceAndTargetName("Application", "{0} to {1}")),
                transformationNode("Layer", sourceAndTargetName("Layer", "{0} to {1}")),
                transformationNode("Transformation", transformationName()),
                transformationNode("Transformation Asset", targetName("Asset")),
                transformationNode("Transformation Attribute", targetName("Attribute"))));
        return helper;
    }

    /**
     * Vytvori spoustec algoritmu toku v grafu.
     * @return Nove vytvoreny spoustec algoritmu toku v grafu.
     */
    private GraphFlowAlgorithmExecutor createFlowAlgorithmExecutor() {
        GraphFlowAlgorithmExecutor flowAlgorithmExecutor = new GraphFlowAlgorithmExecutor();
        GraphFlowAlgorithmFactoryGeneric<RoutineDataFlowAlgorithm> graphFlowAlgorithmFactory = new GraphFlowAlgorithmFactoryGeneric<RoutineDataFlowAlgorithm>(RoutineDataFlowAlgorithm.class, Collections.<GraphFlowFilter>emptyList());
        flowAlgorithmExecutor.setAlgorithmFactories(Collections.<GraphFlowAlgorithmFactory>singletonList(graphFlowAlgorithmFactory));
        return flowAlgorithmExecutor;
    }

    /**
     * Vrati hranu s danymi parametry.
     * @param transaction Aktualni transakce
     * @param sourceName Nazev zdrojoveho uzlu
     * @param targetName Nazev cilovveho uzlu
     * @param label Popisek dotazovvane hrany
     * @param interpolated priznak, zda ma byt hrana interpolovana
     * @param revisionInterval Interval revizi, ktere nas zajimaji
     * @return Nalezena hrana nebo {@code null}, pokud zadna nevyhovvuje vstupnim parametrum.
     */
    private Edge getEdge(TitanTransaction transaction, String sourceName, String targetName, EdgeLabel label, boolean interpolated, RevisionInterval revisionInterval) {
        Long sourceId = (Long)GraphOperation.getVerticesWithName(transaction, sourceName, revisionInterval, 1).get(0).getId();
        Long targetId = (Long)GraphOperation.getVerticesWithName(transaction, targetName, revisionInterval, 1).get(0).getId();
        Edge edge = GraphOperation.getEdge(transaction, new EdgeIdentification(sourceId, targetId, label), revisionInterval);
        return edge != null && Objects.equals(interpolated, edge.getProperty(EdgeProperty.INTERPOLATED.t())) ? edge : null;
    }

    /**
     * Vrati hranu s danymi parametry.
     * @param transaction Aktualni transakce
     * @param sourceName Nazev zdrojoveho uzlu
     * @param sourceParent Nazev predka zdrojoveho uzlu
     * @param sourceParentType Typ predka zdrojoveho uzlu
     * @param targetName Nazev cilovveho uzlu
     * @param targetParent Nazev predka ciloveho uzlu
     * @param targetParentType Typ predka ciloveho uzlu
     * @param label Popisek dotazovvane hrany
     * @param interpolated priznak, zda ma byt hrana interpolovana
     * @param revisionInterval Interval revizi, ktere nas zajimaji
     * @return Nalezena hrana nebo {@code null}, pokud zadna nevyhovvuje vstupnim parametrum.
     */
    private Edge getEdge(TitanTransaction transaction, String sourceName, String sourceParent, String sourceParentType,
            String targetName, String targetParent, String targetParentType, EdgeLabel label, boolean interpolated, RevisionInterval revisionInterval) {
        Vertex source = findNodeByNameAndParent(transaction, sourceName, sourceParent, sourceParentType, revisionInterval);
        Vertex target = findNodeByNameAndParent(transaction, targetName, targetParent, targetParentType, revisionInterval);
        if (source == null || target == null) {
            return null;
        }
        Edge edge = GraphOperation.getEdge(transaction, new EdgeIdentification((long)source.getId(), (long)target.getId(), label), revisionInterval);
        return edge != null && Objects.equals(interpolated, edge.getProperty(EdgeProperty.INTERPOLATED.t())) ? edge : null;
    }
    
    /**
     * Najde uzel podle jeho nazvu a predka.
     * @param transaction Aktualni transakce
     * @param nodeName Nazev uzlu
     * @param parentName Nazev predka uzlu
     * @param parentType Typ predka uzlu
     * @param revisionInterval Interval revizi, ktere nas zajimaji
     * @return Uzel vyhovujici vstupnim paramterum nebo {@code null}, pokud zadny takovy neexistuje.
     */
    private Vertex findNodeByNameAndParent(TitanTransaction transaction, String nodeName, String parentName, String parentType, RevisionInterval revisionInterval) {
        for (Vertex v : GraphOperation.getVerticesWithName(transaction, nodeName, revisionInterval, 100)) {
            for (Vertex parent : GraphOperation.getAllParent(v)) {
                if (parentName.equals(GraphOperation.getName(parent))
                        && parentType.equals(GraphOperation.getType(parent))) {
                    return v;
                }
            }
        }
        return null;
    }

    /**
     * Vytvori definici transformacniho uzlu s danymmi parametry.
     * 
     * @param nodeType Typ uzlu
     * @param nameOrigin Puvod nazvu uzlu
     * @return Nove vytvorena definice transformacniho uzlu.
     */
    private static TransformationNodeDef transformationNode(String nodeType, NodeNameOrigin nameOrigin) {
        TransformationNodeDef transformationNode = new TransformationNodeDef();
        transformationNode.setType(nodeType);
        transformationNode.setNameOrigin(nameOrigin);
        return transformationNode;
    }

    /**
     * Vytvori puvod nazvu uzlu ve zdrojovem a cilovem uzlu.
     *  
     * @param nodesType Typ zdrojoveho a ciloveho uzlu 
     * @param namePattern Sablona nazvu uzlu
     * @return Nove vytvoreny puvod nazvu uzlu ve zdrojovem a cilovem uzlu.
     */
    private static SourceAndTargetBasedName sourceAndTargetName(String nodesType, String namePattern) {
        return new SourceAndTargetBasedName(nodesType, namePattern);
    }

    /**
     * Vytvori puvod nazvu uzlu v cilovem uzlu. 
     * 
     * @param targetType Typ ciloveho uzlu
     * @return Nove vytvoreny puvod nazvu uzlu v cilovem uzlu.
     */
    private static TargetBasedName targetName(String targetType) {
        return new TargetBasedName(targetType);
    }

    /**
     * Vytvori puvod nazvu uzlu v transformacnim uzlu.
     * 
     * @return Nove vytvoreny puvod nazvu uzlu v transformacnim uzlu.
     */
    private static TransformationBasedName transformationName() {
        return new TransformationBasedName();
    }

}
