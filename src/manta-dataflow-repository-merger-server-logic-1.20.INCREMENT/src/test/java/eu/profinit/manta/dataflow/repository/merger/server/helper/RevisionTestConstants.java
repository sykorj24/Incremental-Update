package eu.profinit.manta.dataflow.repository.merger.server.helper;

public class RevisionTestConstants {
    
    // 3 ~ super root, layers, revision root, source root, revision 1, source code
    public static final int REV_1_VERTEX_COUNT = 19 + 2 + 4 + 1 + 1;
    
    // 1 ~ revision 2
    public static final int REV_2_VERTEX_COUNT = REV_1_VERTEX_COUNT + 1;
    
    // 3 ~ novy uzly W, WD a D a revizni uzel 3
    public static final int REV_3_VERTEX_COUNT = REV_2_VERTEX_COUNT + 4;
    
}
