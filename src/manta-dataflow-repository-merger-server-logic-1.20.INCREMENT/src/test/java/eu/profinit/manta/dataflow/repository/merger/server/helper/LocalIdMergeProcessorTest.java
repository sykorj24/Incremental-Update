package eu.profinit.manta.dataflow.repository.merger.server.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Set;

import org.apache.commons.collections4.Transformer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.EdgeIdentification;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.OneFileMerger;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.StandardMergerProcessor;
import eu.profinit.manta.platform.scriptmetadata.service.ScriptMetadataService;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;

/**
 * Otestovat mergování.
 * @author tfechtner
 *
 */
@RunWith(JMockit.class)
public class LocalIdMergeProcessorTest extends TestTitanDatabaseProvider {

    @Mocked
    private ScriptMetadataService scriptMetadataService = new ScriptMetadataService() {
        
        @Override
        public void storeScripts(Collection<String> arg0) {
            // TODO Auto-generated method stub
            
        }
        
        @Override
        public int revertUncommittedScripts() {
            // TODO Auto-generated method stub
            return 0;
        }
        
        @Override
        public int countCommittedScripts(int arg0, Transformer<Collection<String>, Collection<String>> arg1) {
            // TODO Auto-generated method stub
            return 0;
        }
        
        @Override
        public int commitScripts(int arg0, int arg1, Transformer<Collection<String>, Collection<String>> arg2) {
            // TODO Auto-generated method stub
            return 0;
        }
    };

    /**
     * Provést zpracování.
     * @throws FileNotFoundException neexistující soubor s testovým csv
     */
    @Test
    public void testProcess() throws FileNotFoundException {
        cleanGraph();

        StandardMergerProcessor mergerProcessor = new StandardMergerProcessor();
        mergerProcessor.setSuperRootHandler(getSuperRootHandler());
        mergerProcessor.setSourceRootHandler(getSourceRootHandler());
        mergerProcessor.setRevisionRootHandler(getRevisionRootHandler());

        OneFileMerger mergerHelper = new OneFileMerger(getDatabaseHolder(), getSuperRootHandler(),
                getRevisionRootHandler(), getSourceRootHandler(), mergerProcessor, getLicenseHolder(),
                scriptMetadataService, 2000, false);
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {

                getRevisionRootHandler().createMajorRevision(transaction);

                getRevisionRootHandler().checkMergeConditions(transaction, REVISION_1_000000);
                return null;
            }
        });

        mergerHelper.process(new FileInputStream(new File("src/test/resources/input1.csv")), REVISION_1_000000);

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(getBasicGraphVertexCount() + 4, getVertexCount(transaction));

                Set<String> names = getVertexNames(transaction.getVertices());
                Assert.assertFalse(names.contains("t1c3"));

                Vertex db = transaction.getVertices(NodeProperty.NODE_NAME.t(), "db").iterator().next();
                Vertex t2c2 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c2").iterator().next();
                Vertex t2c3 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c3").iterator().next();

                Assert.assertEquals("resName",
                        GraphOperation.getResource(db).getProperty(NodeProperty.RESOURCE_NAME.t()));
                Assert.assertEquals("  res, \"D\te\\tsc\"  ",
                        GraphOperation.getResource(db).getProperty(NodeProperty.RESOURCE_DESCRIPTION.t()));
                Assert.assertEquals("resName",
                        GraphOperation.getResource(t2c2).getProperty(NodeProperty.RESOURCE_NAME.t()));
                Assert.assertEquals("resName2",
                        GraphOperation.getResource(t2c3).getProperty(NodeProperty.RESOURCE_NAME.t()));
                Assert.assertEquals("table2", GraphOperation.getParent(t2c3).getProperty(NodeProperty.NODE_NAME.t()));

                return null;
            }
        });

        mergerHelper.process(new FileInputStream(new File("src/test/resources/input2.csv")), REVISION_1_000000);

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(getBasicGraphVertexCount() + 4 + 1, getVertexCount(transaction));

                Set<String> names = getVertexNames(transaction.getVertices());
                Assert.assertTrue(names.contains("t1c3"));

                return null;
            }
        });
    }

    /**
     * Test edge attributes versions.
     * <p>
     * First step - create revision 1.000000 with new nodes and edges (all have revision validity <1.000000, 1.999999>)
     * <p>
     * Second step - create revision 2.000000 with the same nodes and edges. The only difference is, flow edge between 
     * t1c1 and t2c1 has different attributes in the new revision 2.000000. Hence, the flow edge from revision 1.000000
     * will have revision validity <1.000000, 1.000000> and the new flow edge (flow edge with the new edge attributes) will
     * have revision validity <2.000000, 2.999999>. Rest of the nodes and edges have all revision validity <1.000000, 2.999999>.
     * 
     * @throws FileNotFoundException  Input CSV file with the input graph does not exist
     */
    @Test
    public void testVersioningAtts() throws FileNotFoundException {
        cleanGraph();

        StandardMergerProcessor mergerProcessor = new StandardMergerProcessor();
        mergerProcessor.setSuperRootHandler(getSuperRootHandler());
        mergerProcessor.setSourceRootHandler(getSourceRootHandler());
        mergerProcessor.setRevisionRootHandler(getRevisionRootHandler());

        OneFileMerger mergerHelper = new OneFileMerger(getDatabaseHolder(), getSuperRootHandler(),
                getRevisionRootHandler(), getSourceRootHandler(), mergerProcessor, getLicenseHolder(),
                scriptMetadataService, 2000, false);

        double revision = createMajorRevision();
        mergerHelper.process(new FileInputStream(new File("src/test/resources/inputAttrsR1.csv")), revision);
        commitRevision(revision);

        revision = createMajorRevision();
        mergerHelper.process(new FileInputStream(new File("src/test/resources/inputAttrsR2.csv")), revision);
        commitRevision(revision);

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                //Assert.assertEquals(5, getVertexCount(transaction));

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                Vertex t2c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t2c1").iterator().next();

                EdgeIdentification eId = new EdgeIdentification((long) t1c1.getId(), (long) t2c1.getId(),
                        EdgeLabel.DIRECT);
                Edge e1 = GraphOperation.getEdge(transaction, eId, new RevisionInterval(REVISION_1_000000));
                Assert.assertNotNull(e1);
                Edge e2 = GraphOperation.getEdge(transaction, eId, new RevisionInterval(REVISION_2_000000));
                Assert.assertNotNull(e2);

                Assert.assertEquals(2, GraphOperation.getAdjacentEdges(t1c1, Direction.OUT,
                        RevisionRootHandler.EVERY_REVISION_INTERVAL, EdgeLabel.DIRECT).size());

                Assert.assertFalse(e1.equals(e2));
                Assert.assertEquals(REVISION_INTERVAL_1_000000_1_000000, RevisionUtils.getRevisionInterval(e1));
                Assert.assertEquals("v1", e1.getProperty("k1"));
                Assert.assertEquals("v1", e1.getProperty("k2"));
                Assert.assertEquals("v1", e1.getProperty("k3"));

                Assert.assertEquals(REVISION_INTERVAL_2_000000_2_999999, RevisionUtils.getRevisionInterval(e2));
                Assert.assertEquals("v1", e2.getProperty("k1"));
                Assert.assertEquals("v2", e2.getProperty("k2"));
                Assert.assertEquals("v2", e2.getProperty("k3"));

                return null;
            }
        });
    }

}
