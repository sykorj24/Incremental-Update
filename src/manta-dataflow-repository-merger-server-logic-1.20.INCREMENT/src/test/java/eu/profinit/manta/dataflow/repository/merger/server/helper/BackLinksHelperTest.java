package eu.profinit.manta.dataflow.repository.merger.server.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.predicate.ParentTypePredicate;
import eu.profinit.manta.dataflow.repository.connector.titan.predicate.VertexTypePredicate;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.helper.backlink.BackLinkConfiguration;
import eu.profinit.manta.dataflow.repository.merger.server.helper.backlink.BackLinkHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.OneFileMerger;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.StandardMergerProcessor;
import eu.profinit.manta.platform.scriptmetadata.service.ScriptMetadataService;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class BackLinksHelperTest extends TestTitanDatabaseProvider {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(BackLinksHelperTest.class);

    @Mocked
    private ScriptMetadataService scriptMetadataService;

    /**
     * Testovaný graf je vygenerován ze skriptu src/test/resources/input3.sql
     */
    @Test
    public void testCreateBackLinksOracle() throws FileNotFoundException {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        mergeInput("src/test/resources/input3.csv");

        BackLinkConfiguration backLinkConfiguration = getOracleBackLinkConfiguration();

        BackLinkHelper backLinksHelper = new BackLinkHelper();
        backLinksHelper.setDatabaseService(getDatabaseHolder());
        backLinksHelper.setSuperRootHandler(getSuperRootHandler());
        backLinksHelper.setBackLinkConfigurationSet(Collections.singleton(backLinkConfiguration));

        Map<String, Object> result = backLinksHelper.createBackLinksForResourceName("oracle", "Oracle", getUsageStatsCollector(),
                REVISION_INTERVAL_1_000000_1_000000);
        Assert.assertEquals(16, result.get("backlinks"));

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                List<String> edgeNames = new ArrayList<String>();

                collectEdges(transaction, edgeNames);

                List<String> expected = new ArrayList<String>();
                expected.addAll(getExpectedEdges("src/test/resources/edgesBaseGraphOracle.txt"));
                expected.addAll(getExpectedEdges("src/test/resources/edgesBackLinksOracle.txt"));
                Assert.assertEquals(printEdges(expected), printEdges(edgeNames));

                return null;
            }

        });
    }

    @Test
    public void testCreateBackLinksTeradata() throws FileNotFoundException {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        mergeInput("src/test/resources/input3Tera.csv");

        BackLinkConfiguration backLinkConfiguration = getTeradataBackLinkConfiguration();

        BackLinkHelper backLinksHelper = new BackLinkHelper();
        backLinksHelper.setDatabaseService(getDatabaseHolder());
        backLinksHelper.setSuperRootHandler(getSuperRootHandler());
        backLinksHelper.setBackLinkConfigurationSet(Collections.singleton(backLinkConfiguration));

        Map<String, Object> result = backLinksHelper.createBackLinksForResourceName("teradata", "Teradata", getUsageStatsCollector(),
                REVISION_INTERVAL_1_000000_1_000000);
        Assert.assertEquals(11, result.get("backlinks"));

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                List<String> edgeNames = new ArrayList<String>();

                collectEdges(transaction, edgeNames);

                List<String> expected = new ArrayList<String>();
                expected.addAll(getExpectedEdges("src/test/resources/edgesBaseGraphTeradata.txt"));
                expected.addAll(getExpectedEdges("src/test/resources/edgesBackLinksTeradata.txt"));
                Assert.assertEquals(printEdges(expected), printEdges(edgeNames));

                return null;
            }

           

        });
    }
    
    @Test
    public void testCreateBackLinksByTypeTeradata() throws FileNotFoundException {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        mergeInput("src/test/resources/input3Tera.csv");

        BackLinkConfiguration backLinkConfiguration = getTeradataBackLinkConfiguration();

        BackLinkHelper backLinksHelper = new BackLinkHelper();
        backLinksHelper.setDatabaseService(getDatabaseHolder());
        backLinksHelper.setSuperRootHandler(getSuperRootHandler());
        backLinksHelper.setBackLinkConfigurationSet(Collections.singleton(backLinkConfiguration));

        Map<String, Object> result = backLinksHelper.createBackLinksForResourceType("teradata", "Teradata", getUsageStatsCollector(),
                REVISION_INTERVAL_1_000000_1_000000);
        Assert.assertEquals(11, result.get("backlinks"));

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                List<String> edgeNames = new ArrayList<String>();
                collectEdges(transaction, edgeNames);

                List<String> expected = new ArrayList<String>();
                expected.addAll(getExpectedEdges("src/test/resources/edgesBaseGraphTeradata.txt"));
                expected.addAll(getExpectedEdges("src/test/resources/edgesBackLinksTeradata.txt"));
                Assert.assertEquals(printEdges(expected), printEdges(edgeNames));

                return null;
            }

        });
    }
    
    private void collectEdges(TitanTransaction transaction, List<String> edgeNames) {
        for (Edge e : transaction.getEdges()) {
            if (e.getLabel().equals(EdgeLabel.DIRECT.t())) {
                Vertex sourceV = e.getVertex(Direction.OUT);
                Vertex targetV = e.getVertex(Direction.IN);
                edgeNames.add(GraphOperation.getName(sourceV) + "("
                        + GraphOperation.getName(GraphOperation.getParent(sourceV)) + ")->"
                        + GraphOperation.getName(targetV) + "("
                        + GraphOperation.getName(GraphOperation.getParent(targetV)) + ")");
            }
        }
    }

    private String printEdges(List<String> edges) {
        Collections.sort(edges);
        return StringUtils.join(edges, "\n");
    }

    private List<String> getExpectedEdges(String filePath) {
        List<String> edges = new ArrayList<String>();

        BufferedReader reader = null;
        try {

            reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(filePath)), "utf-8"));
            String line;
            while ((line = reader.readLine()) != null) {
                edges.add(line);
            }
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Cannot read expected edges.", e);
        } catch (FileNotFoundException e) {
            LOGGER.error("Cannot read expected edges.", e);
        } catch (IOException e) {
            LOGGER.error("Cannot read expected edges.", e);
        } finally {
            IOUtils.closeQuietly(reader);
        }

        return edges;
    }

    private BackLinkConfiguration getOracleBackLinkConfiguration() {
        BackLinkConfiguration configuration = new BackLinkConfiguration();
        configuration.setConfigurationId("oracle");
        configuration.setCreateViewPredicate(new ParentTypePredicate(new HashSet<String>(Arrays.asList(
                "PLSQL CreateView", "Table", "View", "PLSQL Synonym"))));
        configuration.setSourceColumnPredicate(new ParentTypePredicate(Collections.singleton("Table")));
        configuration.setViewColumnPredicate(new ParentTypePredicate(new HashSet<String>(Arrays.asList("View",
                "PLSQL Synonym"))));
        configuration.setViewPredicate(new VertexTypePredicate(new HashSet<String>(Arrays.asList("View",
                "PLSQL Synonym"))));

        return configuration;
    }

    private BackLinkConfiguration getTeradataBackLinkConfiguration() {
        BackLinkConfiguration configuration = new BackLinkConfiguration();
        configuration.setConfigurationId("teradata");
        configuration.setCreateViewPredicate(new ParentTypePredicate(Collections.singleton("BTEQ CreateView")));
        configuration.setSourceColumnPredicate(new ParentTypePredicate(Collections.singleton("Table")));
        configuration.setViewColumnPredicate(new ParentTypePredicate(Collections.singleton("View")));
        configuration.setViewPredicate(new VertexTypePredicate(Collections.singleton("View")));

        return configuration;
    }

    private void mergeInput(String inputName) throws FileNotFoundException {
        StandardMergerProcessor mergerProcessor = new StandardMergerProcessor();
        mergerProcessor.setSuperRootHandler(getSuperRootHandler());
        mergerProcessor.setRevisionRootHandler(getRevisionRootHandler());

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                getRevisionRootHandler().createMajorRevision(transaction);
                return null;
            }
        });

        OneFileMerger mergerHelper = new OneFileMerger(getDatabaseHolder(), getSuperRootHandler(),
                getRevisionRootHandler(), getSourceRootHandler(), mergerProcessor, getLicenseHolder(), scriptMetadataService, 2000, false);
        mergerHelper.process(new FileInputStream(new File(inputName)), REVISION_1_000000);
    }
}
