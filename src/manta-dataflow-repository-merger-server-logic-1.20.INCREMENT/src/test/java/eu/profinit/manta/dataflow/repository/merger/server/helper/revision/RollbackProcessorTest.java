package eu.profinit.manta.dataflow.repository.merger.server.helper.revision;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.Collator;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.collections4.Transformer;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.OneFileMerger;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.StandardMergerProcessor;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.ProcessorResult.ActionCounter;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.ProcessorResult.ActionType;
import eu.profinit.manta.dataflow.repository.merger.server.helper.sourcecode.SourceCodeHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.sourcecode.TestHelper;
import eu.profinit.manta.platform.scriptmetadata.service.ScriptMetadataService;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class RollbackProcessorTest extends TestTitanDatabaseProvider {
    private static final int EXPECTED_VER_2 = 20;

    private static final int EXPECTED_VER_3 = EXPECTED_VER_2 + 8;
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(RollbackProcessorTest.class);

    public static final class CaseInsensitiveComparator {

        private CaseInsensitiveComparator() {
        }

        /**
         * Komparátor porovnávající String case insensitive s ohledem na defaultní locale.
         */
        public static final Comparator<? super String> INSTANCE;

        static {
            Collator collator = Collator.getInstance();
            collator.setStrength(Collator.SECONDARY);
            collator.setDecomposition(Collator.CANONICAL_DECOMPOSITION);
            INSTANCE = collator;
        }
    }
    
    // could not resolve null pointer exception when using mocking
    // need to initialize scriptMetadataService
    @Mocked
    private ScriptMetadataService scriptMetadataService = new ScriptMetadataService() {
        
        @Override
        public void storeScripts(Collection<String> arg0) {
            // TODO Auto-generated method stub
            
        }
        
        @Override
        public int revertUncommittedScripts() {
            // TODO Auto-generated method stub
            return 0;
        }
        
        @Override
        public int countCommittedScripts(int arg0, Transformer<Collection<String>, Collection<String>> arg1) {
            // TODO Auto-generated method stub
            return 0;
        }
        
        @Override
        public int commitScripts(int arg0, int arg1, Transformer<Collection<String>, Collection<String>> arg2) {
            // TODO Auto-generated method stub
            return 0;
        }
    };

    @Test
    public void testExecution() throws IOException {
        cleanGraph();
        if (getSourceRootHandler().getSourceCodeDir().exists()) {
            FileUtils.forceDelete(getSourceRootHandler().getSourceCodeDir());
        }
        SourceCodeHelper sourceCodeHelper = new SourceCodeHelper();
        sourceCodeHelper.setSourceRootHandler(getSourceRootHandler());

        Map<String, Map<String, String>> a = new TreeMap<String, Map<String, String>>(
                CaseInsensitiveComparator.INSTANCE);
        a.get(null);

        StandardMergerProcessor mergerProcessor = new StandardMergerProcessor();
        mergerProcessor.setSuperRootHandler(getSuperRootHandler());
        mergerProcessor.setSourceRootHandler(getSourceRootHandler());
        mergerProcessor.setRevisionRootHandler(getRevisionRootHandler());

        final OneFileMerger mergerHelper = new OneFileMerger(getDatabaseHolder(), getSuperRootHandler(),
                getRevisionRootHandler(), getSourceRootHandler(), mergerProcessor, getLicenseHolder(), scriptMetadataService, 2000, false);

        // prvni davka s commitem
        final Double commitedRev = createMajorRevision();
        try {
            mergerHelper.process(new FileInputStream(new File("src/test/resources/input1.csv")), commitedRev);
        } catch (FileNotFoundException e) {
            LOGGER.error("error during loading file", e);
        }
        String file1Id = TestHelper.mergeFile(getDatabaseHolder(), getSourceRootHandler(), sourceCodeHelper,
                "src/test/resources/sourceCode1.txt",
                commitedRev);
        commitRevision(commitedRev);

        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(EXPECTED_VER_2, getVertexCount(transaction));

                Set<String> expected = new HashSet<String>(Arrays.asList("t1c1", "t1c2"));
                Assert.assertEquals(expected, table1ChildrenNames(commitedRev, transaction));
                return null;
            }
        });

        // druha davka neni commitla
        final Double uncommitedRev = createMajorRevision();
        try {
            mergerHelper.process(new FileInputStream(new File("src/test/resources/input1Temp.csv")), uncommitedRev);
        } catch (FileNotFoundException e) {
            LOGGER.error("error during loading file", e);
        }
        String file1Idb = TestHelper.mergeFile(getDatabaseHolder(), getSourceRootHandler(), sourceCodeHelper,
                "src/test/resources/sourceCode1.txt",
                uncommitedRev);
        String file2Id = TestHelper.mergeFile(getDatabaseHolder(), getSourceRootHandler(), sourceCodeHelper,
                "src/test/resources/sourceCode2.txt",
                uncommitedRev);
        Assert.assertEquals(file1Id, file1Idb);

        // ted kontrola necommitnute verze
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(EXPECTED_VER_3, getVertexCount(transaction));

                Set<String> expected = new HashSet<String>(Arrays.asList("t1c1", "t1c3"));
                Assert.assertEquals(expected, table1ChildrenNames(uncommitedRev, transaction));
                return null;
            }

        });

        Assert.assertTrue(new File(getSourceRootHandler().getSourceCodeDir(), file1Id).exists());
        Assert.assertTrue(new File(getSourceRootHandler().getSourceCodeDir(), file2Id).exists());
        
        // rollback

        RevisionHelper revisionHelper = new RevisionHelper();
        revisionHelper.setDatabaseService(getDatabaseHolder());
        revisionHelper.setRevisionRootHandler(getRevisionRootHandler());
        revisionHelper.setSuperRootHandler(getSuperRootHandler());
        revisionHelper.setSourceRootHandler(getSourceRootHandler());
        ProcessorResult<RollbackState> rollbackResult = revisionHelper.rollbackRevision(uncommitedRev);

        Map<ActionType<RollbackState, VertexType>, ActionCounter> vertexActions = rollbackResult.getVertexActions();
        LOGGER.info(vertexActions.toString());

        Assert.assertEquals(5, vertexActions.size());
        Assert.assertEquals(1, vertexActions
                .get(new ActionType<RollbackState, VertexType>(RollbackState.DELETE, VertexType.RESOURCE)).getCount());
        Assert.assertEquals(1,
                vertexActions
                        .get(new ActionType<RollbackState, VertexType>(RollbackState.DELETE, VertexType.SOURCE_NODE))
                        .getCount());
        Assert.assertEquals(2, vertexActions
                .get(new ActionType<RollbackState, VertexType>(RollbackState.DELETE, VertexType.ATTRIBUTE)).getCount());
        Assert.assertEquals(2, vertexActions
                .get(new ActionType<RollbackState, VertexType>(RollbackState.DELETE, VertexType.NODE)).getCount());

        Map<ActionType<RollbackState, EdgeLabel>, ActionCounter> edgeActions = rollbackResult.getEdgeActions();
        LOGGER.info(edgeActions.toString());

        Assert.assertEquals(9, edgeActions.size());
        Assert.assertEquals(1, edgeActions
                .get(new ActionType<RollbackState, EdgeLabel>(RollbackState.DELETE, EdgeLabel.DIRECT)).getCount());
        Assert.assertEquals(1, edgeActions
                .get(new ActionType<RollbackState, EdgeLabel>(RollbackState.DELETE, EdgeLabel.FILTER)).getCount());
        Assert.assertEquals(1, edgeActions
                .get(new ActionType<RollbackState, EdgeLabel>(RollbackState.ROLLBACK, EdgeLabel.DIRECT)).getCount());
        Assert.assertEquals(1, edgeActions
                .get(new ActionType<RollbackState, EdgeLabel>(RollbackState.ROLLBACK, EdgeLabel.FILTER)).getCount());
        Assert.assertEquals(1,
                edgeActions.get(new ActionType<RollbackState, EdgeLabel>(RollbackState.ROLLBACK, EdgeLabel.HAS_SOURCE))
                        .getCount());
        Assert.assertEquals(3,
                edgeActions
                        .get(new ActionType<RollbackState, EdgeLabel>(RollbackState.ROLLBACK, EdgeLabel.HAS_RESOURCE))
                        .getCount());
        Assert.assertEquals(6,
                edgeActions.get(new ActionType<RollbackState, EdgeLabel>(RollbackState.ROLLBACK, EdgeLabel.HAS_PARENT))
                        .getCount());
        Assert.assertEquals(1,
                edgeActions
                        .get(new ActionType<RollbackState, EdgeLabel>(RollbackState.ROLLBACK, EdgeLabel.HAS_ATTRIBUTE))
                        .getCount());

        Assert.assertTrue(new File(getSourceRootHandler().getSourceCodeDir(), file1Id).exists());
        Assert.assertFalse(new File(getSourceRootHandler().getSourceCodeDir(), file2Id).exists());
        
        // kontrola, ze to vypada jako predtim
        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Assert.assertEquals(EXPECTED_VER_2, getVertexCount(transaction));

                Set<String> expected = new HashSet<String>(Arrays.asList("t1c1", "t1c2"));
                Assert.assertEquals(expected, table1ChildrenNames(commitedRev, transaction));
                return null;
            }
        });

    }

    /**
     * @param uncommitedRev
     * @param transaction
     */
    private Set<String> table1ChildrenNames(final Double uncommitedRev, TitanTransaction transaction) {
        Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
        List<Vertex> vertices = GraphOperation.getDirectChildren(table1, new RevisionInterval(uncommitedRev, uncommitedRev));

        Set<String> names = new HashSet<String>();
        for (Vertex v : vertices) {
            names.add(GraphOperation.getName(v));
        }

        return names;
    }
}
