package eu.profinit.manta.dataflow.repository.merger.server.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.helper.propagation.EdgePropagationHelper;
import junit.framework.Assert;

/**
 * Testování propagace hran.
 * @author tfechtner
 *
 */
public class EdgePropagationHelperTest extends TestTitanDatabaseProvider {
    /** Test propagace hrany z nelistoveho do nelistoveho.  */
    @Test
    public void testNonList2NonList() {
        cleanGraph();
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Map<String, Vertex> vertices = createPropagationGraph(transaction);
                GraphCreation.createEdge(vertices.get("table1"), vertices.get("table2"), EdgeLabel.DIRECT, REVISION_1_000000);
                return null;
            }
        });
        Map<String, Object> result = propagate();
        Assert.assertEquals(1, result.get("transformedEdges"));
        Assert.assertEquals(2 + 2, result.get("newEdges"));
        Assert.assertEquals(0, result.get("errorEdges"));

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                List<Vertex> adjacentVertices = GraphOperation.getAdjacentVertices(t1c1, Direction.OUT,
                        REVISION_INTERVAL_1_000000_1_000000, EdgeLabel.DIRECT);

                Assert.assertEquals(2, adjacentVertices.size());
                Assert.assertEquals("t2c1", GraphOperation.getName(adjacentVertices.get(0)));
                Assert.assertEquals("t2c2", GraphOperation.getName(adjacentVertices.get(1)));

                return null;
            }
        });
    }

    /** Test propagace hrany z nelistoveho do nelistoveho pri rozdeleni revizi.  */
    @Test
    public void testNonList2NonList2Revisions() {
        cleanGraph();
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Map<String, Vertex> vertices = createPropagationGraph2(transaction);
                GraphCreation.createEdge(vertices.get("table1"), vertices.get("table2"), EdgeLabel.DIRECT, REVISION_1_000000);
                GraphCreation.createEdge(vertices.get("table1"), vertices.get("table2"), EdgeLabel.DIRECT, REVISION_2_000000);
                return null;
            }
        });

        Map<String, Object> result = propagate(1000, REVISION_2_000000);
        Assert.assertEquals(1, result.get("transformedEdges"));
        Assert.assertEquals(2 + 2, result.get("newEdges"));
        Assert.assertEquals(0, result.get("errorEdges"));

        getDatabaseHolder().runInTransaction(TransactionLevel.READ, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex table1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "table1").iterator().next();
                List<Vertex> adjacentVerticesTableR1 = GraphOperation.getAdjacentVertices(table1, Direction.OUT,
                        REVISION_INTERVAL_1_000000_1_000000, EdgeLabel.DIRECT);
                Assert.assertEquals(1, adjacentVerticesTableR1.size());
                Assert.assertEquals("table2", GraphOperation.getName(adjacentVerticesTableR1.get(0)));

                List<Vertex> adjacentVerticesTableR2 = GraphOperation.getAdjacentVertices(table1, Direction.OUT,
                        REVISION_INTERVAL_2_000000_2_000000, EdgeLabel.DIRECT);
                Assert.assertEquals(0, adjacentVerticesTableR2.size());

                Vertex t1c1 = transaction.getVertices(NodeProperty.NODE_NAME.t(), "t1c1").iterator().next();
                List<Vertex> adjacentVerticesColumnR1 = GraphOperation.getAdjacentVertices(t1c1, Direction.OUT,
                        REVISION_INTERVAL_1_000000_1_000000, EdgeLabel.DIRECT);
                Assert.assertEquals(0, adjacentVerticesColumnR1.size());

                List<Vertex> adjacentVerticesColumnR2 = GraphOperation.getAdjacentVertices(t1c1, Direction.OUT,
                        REVISION_INTERVAL_2_000000_2_000000, EdgeLabel.DIRECT);
                Assert.assertEquals(2, adjacentVerticesColumnR2.size());
                Assert.assertEquals("t2c1", GraphOperation.getName(adjacentVerticesColumnR2.get(0)));
                Assert.assertEquals("t2c2", GraphOperation.getName(adjacentVerticesColumnR2.get(1)));

                return null;
            }
        });
    }

    /** Test propagace hrany z nelistoveho do nelistoveho pri spatne revizi.  */
    @Test(expected = RevisionException.class)
    public void testNonList2NonListWrongRevision() {
        cleanGraph();
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Map<String, Vertex> vertices = createPropagationGraph(transaction);
                GraphCreation.createEdge(vertices.get("table1"), vertices.get("table2"), EdgeLabel.DIRECT, REVISION_1_000000);
                return null;
            }
        });

        propagate(1000, 0);
    }

    /** Test propagace hrany z nelistoveho do nelistoveho. Pri malem maximu.  */
    @Test
    public void testNonList2NonListMaximum2() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Map<String, Vertex> vertices = createPropagationGraph(transaction);
                GraphCreation.createEdge(vertices.get("table1"), vertices.get("table2"), EdgeLabel.DIRECT, REVISION_1_000000);
                return null;
            }
        });
        Map<String, Object> result = propagate(1, REVISION_1_000000);
        Assert.assertEquals(0, result.get("transformedEdges"));
        Assert.assertEquals(0, result.get("newEdges"));
        Assert.assertEquals(1, result.get("errorEdges"));

    }

    /** Test propagace hrany z listoveho do nelistoveho.  */
    @Test
    public void testList2NonList() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Map<String, Vertex> vertices = createPropagationGraph(transaction);
                GraphCreation.createEdge(vertices.get("t1c1"), vertices.get("table2"), EdgeLabel.DIRECT, REVISION_1_000000);
                return null;
            }
        });
        Map<String, Object> result = propagate();
        Assert.assertEquals(1, result.get("transformedEdges"));
        Assert.assertEquals(2, result.get("newEdges"));
        Assert.assertEquals(0, result.get("errorEdges"));

    }

    /** Test propagace hrany z nelistoveho do listoveho.  */
    @Test
    public void testNonList2List() {
        GraphCreation.initDatabase(getDatabaseHolder(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());
        getDatabaseHolder().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Map<String, Vertex> vertices = createPropagationGraph(transaction);
                GraphCreation.createEdge(vertices.get("table1"), vertices.get("t2c2"), EdgeLabel.DIRECT, REVISION_1_000000);
                return null;
            }
        });

        Map<String, Object> result = propagate();
        Assert.assertEquals(1, result.get("transformedEdges"));
        Assert.assertEquals(2, result.get("newEdges"));
        Assert.assertEquals(0, result.get("errorEdges"));

    }

    private Map<String, Object> propagate() {
        return propagate(1000, REVISION_1_000000);
    }

    private Map<String, Object> propagate(int maxLeafEdges, double revision) {
        EdgePropagationHelper edgePropagationHelper = new EdgePropagationHelper();
        edgePropagationHelper.setMaximumLeafEdges(maxLeafEdges);
        return edgePropagationHelper.propagateEdges(getDatabaseHolder(), getSuperRootHandler(),
                getRevisionRootHandler(), getUsageStatsCollector(), revision);
    }

    private Map<String, Vertex> createPropagationGraph(TitanTransaction transaction) {
        getRevisionRootHandler().createMajorRevision(transaction);
        Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
        Vertex teradata = GraphCreation.createResource(transaction, getSuperRootHandler().getRoot(transaction),
                "Teradata", "Teradata", "Teradata", layer, REVISION_1_000000);
        Vertex table1 = GraphCreation.createNode(transaction, teradata, "table1", "Table", REVISION_1_000000);
        Vertex table2 = GraphCreation.createNode(transaction, teradata, "table2", "Table", REVISION_1_000000);

        Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", REVISION_1_000000);
        Vertex t1c2 = GraphCreation.createNode(transaction, table1, "t1c2", "Column", REVISION_1_000000);
        Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", REVISION_1_000000);
        Vertex t2c2 = GraphCreation.createNode(transaction, table2, "t2c2", "Column", REVISION_1_000000);

        Map<String, Vertex> vertices = new HashMap<String, Vertex>();
        vertices.put("teradata", teradata);
        vertices.put("table1", table1);
        vertices.put("table2", table2);
        vertices.put("t1c1", t1c1);
        vertices.put("t1c2", t1c2);
        vertices.put("t2c1", t2c1);
        vertices.put("t2c2", t2c2);

        getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);

        return vertices;
    }
    
    private Map<String, Vertex> createPropagationGraph2(TitanTransaction transaction) {
        RevisionInterval revInterval = new RevisionInterval(REVISION_1_000000, REVISION_2_000000);
        
        getRevisionRootHandler().createMajorRevision(transaction);
        getRevisionRootHandler().commitRevision(transaction, REVISION_1_000000);
        getRevisionRootHandler().createMajorRevision(transaction);
        getRevisionRootHandler().commitRevision(transaction, REVISION_2_000000);
        
        Vertex layer = GraphCreation.createLayer(transaction, "Physical", "Physical");
        Vertex teradata = GraphCreation.createResource(transaction, getSuperRootHandler().getRoot(transaction),
                "Teradata", "Teradata", "Teradata", layer, revInterval);
        Vertex table1 = GraphCreation.createNode(transaction, teradata, "table1", "Table", revInterval);
        Vertex table2 = GraphCreation.createNode(transaction, teradata, "table2", "Table", revInterval);

        Vertex t1c1 = GraphCreation.createNode(transaction, table1, "t1c1", "Column", revInterval);
        Vertex t1c2 = GraphCreation.createNode(transaction, table1, "t1c2", "Column", revInterval);
        Vertex t2c1 = GraphCreation.createNode(transaction, table2, "t2c1", "Column", revInterval);
        Vertex t2c2 = GraphCreation.createNode(transaction, table2, "t2c2", "Column", revInterval);

        Map<String, Vertex> vertices = new HashMap<String, Vertex>();
        vertices.put("teradata", teradata);
        vertices.put("table1", table1);
        vertices.put("table2", table2);
        vertices.put("t1c1", t1c1);
        vertices.put("t1c2", t1c2);
        vertices.put("t2c1", t2c1);
        vertices.put("t2c2", t2c2);

        

        return vertices;
    }
}
