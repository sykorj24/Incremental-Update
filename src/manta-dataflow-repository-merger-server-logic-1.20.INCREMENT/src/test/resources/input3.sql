create table t1 (
    t1c1 varchar(10),
    t1c2 varchar(10),
    t1c3 varchar(10)
);

create table t2 (
    t2c1 varchar(10),
    t2c2 varchar(10)
);

create table t3 (
    t3c1 varchar(10),
    t3c2 varchar(10),
    t3c3 varchar(10)
);

create SYNONYM s1 for t1;

create view v1 as select t1c1,t1c2,t1c3 from t1;

create SYNONYM s2 for v1;
create SYNONYM s3 for s2;

create view v2 as select s3.t1c1 + t2.t2c1 as t1c1,t2.t2c2 from s3,t2;
create view v3 as select t1c1,t1c2 from s1 where t1c3=2;
create view v4 as select t2c2 from v2;

insert into v3 values (t1c1) select t3c1 from t3;
update v2 set (t1c1,t2c2) = (select t3c1, t3c2 from t3);
update s3 set t1c3 = (select t3c3 from t3);