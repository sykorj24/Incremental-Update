package eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor;

import java.util.List;

/**
 * Processor responsible for merging single objects from the input file to the metadata repository.
 * 
 * @author tfechtner
 *
 */
public interface MergerProcessor {
    
    /**
     * Zamerguje jeden objekt.
     * @param itemParts jednotlivé části objektu k zamergování
     * @param context kontext procesoru, který se modifikuje v rámci běhu
     * @return Výsledek zpracování. Obecně může být výsledkem merge žádného objektu
     *   (např. pokud se pouze uložil do paměti pro pozdější zpracování), nebo více objektů
     *   (např. pokud se objekt zamergeoval po předchozím uložení do paměti).
     */
    List<MergerProcessorResult> mergeObject(String[] itemParts, ProcessorContext context); 
}
