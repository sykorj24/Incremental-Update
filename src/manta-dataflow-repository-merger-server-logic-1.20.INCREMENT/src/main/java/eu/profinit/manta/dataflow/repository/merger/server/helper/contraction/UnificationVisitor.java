package eu.profinit.manta.dataflow.repository.merger.server.helper.contraction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.concurrent.NotThreadSafe;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.TransactionAware;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Visitor provádějící unifikaci uzlů s určenými typy a stejným názvem.
 * Visitor očekává, že pojede jen v jednom vláknu a proto neřeší žádné zamykání databáze.
 * @author tfechtner
 * 
 */
@NotThreadSafe
public class UnificationVisitor implements GraphVisitor, TransactionAware {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(UnificationVisitor.class);

    public static final String NODE_TYPE = "_NODE_TYPE";

    public static final String RESOURCE_NAME = "_RESOURCE_NAME";

    public static final String RESOURCE_TYPE = "_RESOURCE_TYPE";

    /** Seznam predikátů na zdroje, kde jsou v mapě vždy predikáty na atributy.
     *  Klíč atributu nebo {@value #NODE_TYPE} pro typ uzlu, {@value #RESOURCE_NAME} pro jméno resource,
     *  nebo {@value #RESOURCE_TYPE} pro typ resource na regulární výraz na hodnotu atributu.
     */
    private final List<Map<String, String>> sources;
    /** Seznam predikátů na zdroje, kde jsou v mapě vždy predikáty na atributy.
     *  Klíč atributu nebo {@value #NODE_TYPE} pro typ uzlu, {@value #RESOURCE_NAME} pro jméno resource,
     *  nebo {@value #RESOURCE_TYPE} pro typ resource na regulární výraz na hodnotu atributu.
     */
    private final List<Map<String, String>> targets;
    /** Zda se unifikují uzly bez ohledu na velikost písmen.*/
    private final boolean caseInsensitive;
    /** Zda se má provést unifikace zdrojů, pokud je víc cílů nebo není žádný cíl. */
    private final boolean forceMergeSources;
    /** Zda se má provést unifikace cílů, pokud jich je víc. */
    private final boolean forceMergeTargets;
    /** Interval revizí, na které se pracuje. */
    private final RevisionInterval revisionInterval;
    /** First preceding revision of the revisionInterval */
    private final Double previousRevision;
    /** Výsledek zpracování. */
    private final UnificationResult result = new UnificationResult();
    /** Aktuální transakce, ve které se momentálně pracuje. */
    private TitanTransaction transaction;

    /**
     * @param revisionInterval interval revizí, na které se unifikace provádí
     * @param types množina převáděných typů, kde klíče je zdrojový typ a hodnota cílový typ
     */
    public UnificationVisitor(RevisionInterval revisionInterval, Double previousRevision, List<Map<String, String>> sources,
            List<Map<String, String>> targets, boolean caseInsensitive, boolean forceMergeSources,
            boolean forceMergeTargets) {
        if (sources == null || targets == null || sources.size() != targets.size()) {
            throw new IllegalArgumentException("Source and target maps must not be null and must have the same sizes.");
        }
        this.revisionInterval = revisionInterval;
        this.previousRevision = previousRevision;
        this.sources = sources;
        this.targets = targets;
        this.caseInsensitive = caseInsensitive;
        this.forceMergeSources = forceMergeSources;
        this.forceMergeTargets = forceMergeTargets;
    }

    private boolean testVertex(Vertex vertex, Map<String, String> tests) {
        for (Map.Entry<String, String> test : tests.entrySet()) {
            if (NODE_TYPE.equals(test.getKey())) {
                String value = GraphOperation.getType(vertex);
                if (value == null || !value.matches(test.getValue())) {
                    return false;
                }
            } else if (RESOURCE_NAME.equals(test.getKey())) {
                String value = GraphOperation.getName(GraphOperation.getResource(vertex));
                if (value == null || !value.matches(test.getValue())) {
                    return false;
                }
            } else if (RESOURCE_TYPE.equals(test.getKey())) {
                String value = GraphOperation.getType(GraphOperation.getResource(vertex));
                if (value == null || !value.matches(test.getValue())) {
                    return false;
                }
            } else {
                List<Object> values = GraphOperation.getNodeAttribute(vertex, test.getKey(), getRevisionInterval());
                boolean matches = false;
                for (Object value : values) {
                    if (value != null && ((String) value).matches(test.getValue())) {
                        matches = true;
                    }
                }
                if (!matches) {
                    return false;
                }
            }
        }
        return true;
    }

    private Vertex findTargetVertex(List<Vertex> targetVertices) {
        return Collections.min(targetVertices, new Comparator<Vertex>() {
            @Override
            public int compare(Vertex vertex1, Vertex vertex2) {
                int result = GraphOperation.getName(vertex1).compareTo(GraphOperation.getName(vertex2));
                if (result != 0) {
                    return result;
                }
                result = GraphOperation.getType(vertex1).compareTo(GraphOperation.getType(vertex2));
                if (result != 0) {
                    return result;
                }
                return GraphOperation.getName(GraphOperation.getResource(vertex1))
                        .compareTo(GraphOperation.getName(GraphOperation.getResource(vertex2)));
            }
        });
    }

    private void resolveSurroundingsInternal(Vertex vertex, int index) {
        List<Vertex> children = GraphOperation.getDirectChildren(vertex, getRevisionInterval());
        Map<String, String> sourceTests = sources.get(index);
        Map<String, String> targetTests = targets.get(index);

        Map<String, List<Vertex>> sourceVerticesMap = new TreeMap<String, List<Vertex>>(
                caseInsensitive ? CaseInsensitiveComparator.INSTANCE : null);
        Map<String, List<Vertex>> targetVerticesMap = new TreeMap<String, List<Vertex>>(
                caseInsensitive ? CaseInsensitiveComparator.INSTANCE : null);

        for (Vertex child : children) {
            if (testVertex(child, sourceTests)) {
                String name = GraphOperation.getName(child);
                List<Vertex> vertexList = sourceVerticesMap.get(name);
                if (vertexList == null) {
                    vertexList = new ArrayList<Vertex>();
                    sourceVerticesMap.put(name, vertexList);
                }
                vertexList.add(child);
            }
            if (testVertex(child, targetTests)) {
                String name = GraphOperation.getName(child);
                List<Vertex> vertexList = targetVerticesMap.get(name);
                if (vertexList == null) {
                    vertexList = new ArrayList<Vertex>();
                    targetVerticesMap.put(name, vertexList);
                }
                vertexList.add(child);
            }
        }

        for (Map.Entry<String, List<Vertex>> sourceVerticesEntry : sourceVerticesMap.entrySet()) {
            List<Vertex> sourceVertices = sourceVerticesEntry.getValue();
            List<Vertex> targetVertices = targetVerticesMap.get(sourceVerticesEntry.getKey());

            Vertex targetVertex;
            // sourceVertices.size() > 0
            if (CollectionUtils.size(targetVertices) == 1) {
                targetVertex = targetVertices.get(0);
            } else if (CollectionUtils.size(targetVertices) == 0 && CollectionUtils.size(sourceVertices) > 1 && forceMergeSources) {
                targetVertex = findTargetVertex(sourceVertices);
            } else if (CollectionUtils.size(targetVertices) > 1 && (forceMergeSources || forceMergeTargets)) {
                targetVertex = findTargetVertex(targetVertices);
            } else {
                continue;
            }

            if (CollectionUtils.size(targetVertices) == 1 || forceMergeSources) {
                for (Vertex sourceVertex : sourceVertices) {
                    if (sourceVertex != targetVertex) {
                        contractTwoNode(sourceVertex, targetVertex);
                        result.incUnifiedObjects();
                    }
                }
            }

            if (targetVertices.size() > 1 && forceMergeTargets) {
                for (Vertex sourceVertex : targetVertices) {
                    if (sourceVertex != targetVertex) {
                        contractTwoNode(sourceVertex, targetVertex);
                        result.incUnifiedObjects();
                    }
                }
            }
        }
    }

    private void resolveSurroundings(Vertex vertex) {
        for (int index = 0; index < targets.size(); ++index) {
            resolveSurroundingsInternal(vertex, index);
        }
    }

    /**
     * Zkontrahuje dva uzly.
     * @param sourceVertex zdrojový uzel, který nebude mít v této revizi platnost
     * @param targetVertex cílový uzel, do kterého se merguje
     */
    private void contractTwoNode(Vertex sourceVertex, Vertex targetVertex) {
        if (targetVertex.equals(sourceVertex)) {
            LOGGER.error("Two same vertices cannot be contracted {}.", targetVertex.getId());
            return;
        }

        contractAttributes(sourceVertex, targetVertex);
        contractChildren(sourceVertex, targetVertex);
        ElementsMerger.unifyTwoVertices(sourceVertex, targetVertex, getRevisionInterval(), getPreviousRevision(), getResult());
    }

    /**
     * Zmerguje atributy dvou uzlů.
     * @param sourceVertex zdrojový vrchol, odkdu se merguje
     * @param targetVertex cílový vrchol, do kterého se merguje
     */
    private void contractAttributes(Vertex sourceVertex, Vertex targetVertex) {
        Map<String, List<Object>> targetAttributes = GraphOperation.getAllNodeAttributes(targetVertex,
                getRevisionInterval());

        List<Edge> sourceAttributeEdges = GraphOperation.getAdjacentEdges(sourceVertex, Direction.OUT,
                getRevisionInterval(), EdgeLabel.HAS_ATTRIBUTE);
        for (Edge edge : sourceAttributeEdges) {
            Vertex attr = edge.getVertex(Direction.IN);

            String key = GraphOperation.getAttributeKey(attr);
            Object value = GraphOperation.getAttributeValue(attr);
            List<Object> existedTargetValues = targetAttributes.get(key);
            if (existedTargetValues == null) {
                existedTargetValues = new ArrayList<>();
                targetAttributes.put(key, existedTargetValues);
            }

            RevisionInterval edgeInterval = RevisionUtils.getRevisionInterval(edge);
            if (existedTargetValues.contains(value)) {
                if (edgeInterval.getStart() < getRevisionInterval().getStart()) {
                    RevisionUtils.setEdgeTransactionEnd(edge, getPreviousRevision());
                } else {
                    attr.remove();
                }
                getResult().saveContractedVertex(attr);
            } else {
                existedTargetValues.add(value);

                if (edgeInterval.getStart() < getRevisionInterval().getStart()) {
                    RevisionUtils.setEdgeTransactionEnd(edge, getPreviousRevision());
                    GraphCreation.createNodeAttribute(transaction, targetVertex, key, value, getRevisionInterval());
                } else {
                    GraphCreation.createEdge(targetVertex, attr, EdgeLabel.HAS_ATTRIBUTE, getRevisionInterval());
                    edge.remove();
                }
            }
        }
    }

    /**
     * Zmerguje potomky dvou uzlů.
     * @param sourceVertex zdrojový vrchol, odkdu se merguje
     * @param targetVertex cílový vrchol, do kterého se merguje
     */
    private void contractChildren(Vertex sourceVertex, Vertex targetVertex) {
        List<Vertex> targetChildren = GraphOperation.getDirectChildren(targetVertex, getRevisionInterval());
        Map<VertexIdentification, Vertex> targetChildrenMap = new HashMap<>();
        for (Vertex child : targetChildren) {
            targetChildrenMap.put(new VertexIdentification(child, caseInsensitive), child);
        }

        List<Vertex> sourceChildren = GraphOperation.getDirectChildren(sourceVertex, getRevisionInterval());
        for (Vertex sourceChild : sourceChildren) {
            Edge controlEdge = GraphOperation.getControlEdge(sourceChild);
            RevisionInterval childInterval = RevisionUtils.getRevisionInterval(controlEdge);

            VertexIdentification childIdentification = new VertexIdentification(sourceChild, caseInsensitive);
            Vertex targetChild = targetChildrenMap.get(childIdentification);
            if (targetChild != null) {
                if (childInterval.getStart() < getRevisionInterval().getStart()) {
                    contractTwoNode(sourceChild, targetChild);
                    RevisionUtils.setEdgeTransactionEnd(controlEdge, getPreviousRevision());
                } else {
                    contractTwoNode(sourceChild, targetChild);
                    sourceChild.remove();
                }
            } else {
                if (childInterval.getStart() < getRevisionInterval().getStart()) {
                    GraphCreation.copySubTree(transaction, sourceChild, targetVertex, getRevisionInterval());
                    RevisionUtils.setEdgeTransactionEnd(controlEdge, getPreviousRevision());
                } else {
                    EdgeLabel edgeLabel = EdgeLabel.parseFromDbType(controlEdge.getLabel());
                    GraphCreation.createEdge(sourceChild, targetVertex, edgeLabel, getRevisionInterval());
                    controlEdge.remove();
                }
            }
        }
    }

    @Override
    public void visitLayer(Vertex layer) {
        // NOOP
    }

    @Override
    public void visitResource(Vertex resource) {
        resolveSurroundings(resource);
    }

    @Override
    public void visitNode(Vertex node) {
        resolveSurroundings(node);
    }

    @Override
    public void visitAttribute(Vertex attribute) {
        // NOOP
    }

    @Override
    public void visitEdge(Edge edge) {
        // NOOP
    }

    public RevisionInterval getRevisionInterval() {
        return revisionInterval;
    }
    
    public Double getPreviousRevision() {
        return previousRevision;
    }

    public UnificationResult getResult() {
        return result;
    }

    @Override
    public void setTransaction(TitanTransaction transaction) {
        this.transaction = transaction;
    }
}
