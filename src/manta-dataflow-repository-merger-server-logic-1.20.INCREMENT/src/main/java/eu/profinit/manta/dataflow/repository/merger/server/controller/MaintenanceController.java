package eu.profinit.manta.dataflow.repository.merger.server.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterGroupProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;
import eu.profinit.manta.dataflow.repository.merger.server.helper.backlink.BackLinkHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.contraction.ContractionHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.delete.DeleteHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.propagation.EdgePropagationHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.RevisionHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.usage.ContentCollectorHelper;
import eu.profinit.manta.dataflow.repository.merger.server.security.MergerRoles;
import eu.profinit.manta.dataflow.repository.utils.VersionHolder;
import eu.profinit.manta.platform.automation.MantaVersion;
import eu.profinit.manta.platform.automation.licensing.LicenseException;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;
import eu.profinit.manta.platform.web.core.security.SecurityHelper;

/**
 * Kontroler pro zjišťování a práci s databázi.
 * @author tfechtner
 *
 */
@RequestMapping("/api")
@Controller
public class MaintenanceController {
    /** Název akce pro usage stats pro smazání db. */
    private static final String FLOW_REPOSITORY_TRUNCATE = "flow_repository_truncate";
    /** Název akce pro usage stats pro zjištění obsahu repository. */
    private static final String FLOW_REPOSITORY_CONTENT = "flow_repository_content";
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(MaintenanceController.class);
    /** Název merger modulu. */
    public static final String MODULE_NAME = "maintanance_controller";

    @Autowired
    private DatabaseHolder databaseService;

    @Autowired
    private SuperRootHandler superRootHandler;

    @Autowired
    private SourceRootHandler sourceRootHandler;

    @Autowired
    private RevisionRootHandler revisionRootHandler;

    @Autowired
    private EdgePropagationHelper edgePropagationHelper;

    @Autowired
    private BackLinkHelper viewBackLinksHelper;

    @Autowired
    private UsageStatsCollector usageStatsCollector;

    @Autowired
    private ContentCollectorHelper contentCollectorHelper;

    @Autowired
    private TechnicalAttributesHolder terchnicalAttributes;

    @Autowired
    private ContractionHelper contractionHelper;

    @Autowired
    private HorizontalFilterProvider horizontalFilterProvider;

    @Autowired
    private HorizontalFilterGroupProvider horizontalFilterGroupProvider;
    
    @Autowired
    private DeleteHelper deleteHelper;
    
    @Autowired
    private VersionHolder versionHolder;

    /**
     * Zobrazí info o databázi pro posledni commitnutou revizi.
     * Uzly prochází stromově
     * @return JSON mapa s počty objektů v db
     */
    @RequestMapping(value = "/inspect", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> inspectGraphForLastRevision() {
        final Double revision = revisionRootHandler.getLatestCommittedRevisionNumber(databaseService);
        if (revision != null) {
            Map<String, Object> result = contentCollectorHelper.collectContent(databaseService, superRootHandler,
                    terchnicalAttributes, revision);

            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
        } else {
            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("result", "There is no commited version.");
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
        }

    }

    /**
     * Zobrazí info o databázi. Uzly prochází stromově.
     * @param revision číslo revize, která se prochází
     * @return JSON mapa s počty objektů v db
     */
    @RequestMapping(value = "/inspect/{revision}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> inspectGraph(@PathVariable("revision") final Double revision) {
        Map<String, Object> result = contentCollectorHelper.collectContent(databaseService, superRootHandler,
                terchnicalAttributes, revision);
        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
    }

    /**
     * Vymaže celý graf.
     * @return ftl šablona
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/truncate", method = RequestMethod.GET)
    public String truncateGraph() {
        databaseService.truncateDatabase();
        sourceRootHandler.truncateSourceCodeFiles();
        GraphCreation.initDatabase(databaseService, superRootHandler, revisionRootHandler, sourceRootHandler);
        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REPOSITORY_TRUNCATE);
        return "redirect:inspect";
    }

    /**
     * Provede propagaci hran do listů.
     * @param revision číslo revize, která se prochází
     * @return výsledek propagace jako JSON
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/propagate-edges/{revision}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> propagateEdges(@PathVariable("revision") final Double revision) {
        return edgePropagationHelper.propagateEdges(databaseService, superRootHandler, revisionRootHandler,
                usageStatsCollector, revision);
    }

    /**
     * Provede propagaci hran do listů pro head revizi.
     * @return výsledek propagace jako JSON
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/propagate-edges", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> propagateEdges() {
        final Double revision = revisionRootHandler.getLatestRevisionNumber(databaseService);
        if (revision == null) {
            LOGGER.error("The database is not correctly initialized.");
            return Collections.<String, Object> singletonMap("error", "The database is not initialized correctly.");
        }

        return edgePropagationHelper.propagateEdges(databaseService, superRootHandler, revisionRootHandler,
                usageStatsCollector, revision);
    }

    /**
     * Aktualizuje horizontální filtry.
     * @return výsledek operace jako JSON
     */
    @RequestMapping(value = "/refresh-filters", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> refreshFilters() {
        Map<String, Object> result = new HashMap<>();
        result.put("filters", horizontalFilterProvider.refreshFilters());
        result.put("groups", horizontalFilterGroupProvider.refreshGroups());
        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
    }

    /**
     * Spočte statistiky o repository.
     * @return výsledek operace jako JSON
     */
    @RequestMapping(value = "/consolidation", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> collectStats() {
        final Double revision = revisionRootHandler.getLatestRevisionNumber(databaseService);
        if (revision == null) {
            LOGGER.error("The database is not correctly initialized.");
            Map<String, Object> errorMessage = Collections.<String, Object> singletonMap("error",
                    "The database is not initialized correctly.");
            return new ResponseEntity<Map<String, Object>>(errorMessage, HttpStatus.NO_CONTENT);
        }

        Map<String, Object> params = contentCollectorHelper.collectContent(databaseService, superRootHandler,
                terchnicalAttributes, revision);

        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REPOSITORY_CONTENT, params);

        return new ResponseEntity<Map<String, Object>>(HttpStatus.OK);
    }

    /**
     * Spočte statistiky o repository.
     * @param revision číslo revize, která se prochází
     * @return výsledek operace jako JSON
     */
    @RequestMapping(value = "/consolidation/{revision}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> collectStats(@PathVariable("revision") final Double revision) {
        Map<String, Object> params = contentCollectorHelper.collectContent(databaseService, superRootHandler,
                terchnicalAttributes, revision);

        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REPOSITORY_CONTENT, params);

        return new ResponseEntity<Map<String, Object>>(HttpStatus.OK);
    }

    /**
     * Provede zpětné propojení u view, které jsou použity jako cíl toku.
     * @param configurationId id konfigurace k použití
     * @param resourceName název resource, kde se mají hledat view
     * @param revision číslo revize, která se prochází
     * @return výsledek propagace jako JSON
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/backlink-views/{configurationId}/{resourceName}/{revision}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> backlinkViewsForId(@PathVariable("configurationId") String configurationId,
            @PathVariable("resourceName") String resourceName, @PathVariable("revision") final Double revision) {
        return viewBackLinksHelper.createBackLinksForResourceName(configurationId, resourceName, usageStatsCollector,
                new RevisionInterval(revision, revision));
    }

    /**
     * Provede zpětné propojení u view, které jsou použity jako cíl toku.
     * @param configurationId id konfigurace k použití
     * @param resourceName název resource, kde se mají hledat view
     * @return výsledek propagace jako JSON
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/backlink-views/{configurationId}/{resourceName}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> backlinkViewsForId(@PathVariable("configurationId") String configurationId,
            @PathVariable("resourceName") String resourceName) {
        final Double revision = revisionRootHandler.getLatestRevisionNumber(databaseService);
        if (revision == null) {
            LOGGER.error("The database is not correctly initialized.");
            return Collections.<String, Object> singletonMap("error", "The database is not initialized correctly.");
        }
        return viewBackLinksHelper.createBackLinksForResourceName(configurationId, resourceName, usageStatsCollector,
                new RevisionInterval(revision, revision));
    }

    /**
     * Provede zpětné propojení u view, které jsou použity jako cíl toku.
     * @param configurationId id konfigurace k použití
     * @param resourceType typ resource, které se mají projít
     * @param revision číslo revize, která se prochází
     * @return výsledek propagace jako JSON
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/backlink-views-all/{configId}/{resourceType}/{revision}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> backlinkViewsForType(@PathVariable("configId") String configurationId,
            @PathVariable("resourceType") final String resourceType, @PathVariable("revision") final Double revision) {
        return viewBackLinksHelper.createBackLinksForResourceType(configurationId, resourceType, usageStatsCollector,
                new RevisionInterval(revision, revision));
    }

    /**
     * Provede zpětné propojení u view, které jsou použity jako cíl toku.
     * @param configurationId id konfigurace k použití
     * @param resourceType typ resource, které se mají projít
     * @return výsledek propagace jako JSON
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/backlink-views-all/{configId}/{resourceType}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> backlinkViewsForType(@PathVariable("configId") String configurationId,
            @PathVariable("resourceType") final String resourceType) {
        final Double revision = revisionRootHandler.getLatestRevisionNumber(databaseService);
        if (revision == null) {
            LOGGER.error("The database is not correctly initialized.");
            return Collections.<String, Object> singletonMap("error", "The database is not initialized correctly.");
        }
        return viewBackLinksHelper.createBackLinksForResourceType(configurationId, resourceType, usageStatsCollector,
                new RevisionInterval(revision, revision));
    }

    /**
     * Provede kontrakci duplikovaných uzlů z optimistického mergu.
     * @return výsledek operace jako json
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/contraction", method = RequestMethod.GET)
    public ResponseEntity<Object> performContraction() {
        return contractionHelper.performContraction(usageStatsCollector);
    }

    /**
     * Provede unifikaci uzlů s požadovanými typy v aktuální revizi.
     * @param resourceName název resource, kde se operace provádí
     * @param types množina převáděných typů, kde klíč je zdrojový typ a hodnota cílový typ
     * @return výsledek operace jako json
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/unification", method = RequestMethod.POST)
    public ResponseEntity<Object> performUnification(@RequestBody UnificationRequest request) {
        if (CollectionUtils.isEmpty(request.getSources()) || CollectionUtils.isEmpty(request.getTargets())
                || request.getSources().size() != request.getTargets().size()) {
            return new ResponseEntity<Object>(
                    "The unification source and target maps must not be empty and must have the same size.",
                    HttpStatus.BAD_REQUEST);
        }

        return contractionHelper.performUnification(usageStatsCollector, request.getResourceName(),
                request.getSources(), request.getTargets(), request.isCaseInsensitive(), request.isForceMergeSources(),
                request.isForceMergeTargets());
    }

    /**
     * Zavře databází, díky čemuž se perzistuje všechen stav z paměti.
     * @return výsledek operace
     */
    @RequestMapping(value = "/shutdown", method = RequestMethod.GET)
    public ResponseEntity<String> shutdown() {
        databaseService.closeDatabase();
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }
    
    /**
     * Delete vertices in repository by specifying their absolute path in repository.
     * 
     * @param file  file containing objects to be deleted
     * @param revision  revision number in which the deletion is performed
     * @param mantaVersionString  Manta client version number
     * @return result of the delete operation
     */
    @RequestMapping(value = "/delete-vertices", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> deleteVertices(@RequestParam("file") final MultipartFile file,
            @RequestParam(value = "revision", required = false) final Double revision,
            @RequestParam(value = "mantaVersion", required = false) final String mantaVersionString) {

        if (revision == null && revisionRootHandler.isVersionSystemOn()) {
            return deleteHelper.createErrorResponse(HttpStatus.BAD_REQUEST,
                    RevisionHelper.ENABLED_VERSION_SYSTEM_ERROR);
        }

        if (revision != null && !revisionRootHandler.isVersionSystemOn()) {
            return deleteHelper.createErrorResponse(HttpStatus.BAD_REQUEST,
                    RevisionHelper.DISABLED_VERSION_SYSTEM_ERROR);
        }

        if (mantaVersionString != null) {
            MantaVersion mantaVersionClient = MantaVersion.createVersionFromText(mantaVersionString);
            if (!versionHolder.isCompatible(mantaVersionClient)) {
                return deleteHelper.createErrorResponse(HttpStatus.BAD_REQUEST,
                        versionHolder.generateError(mantaVersionClient));
            }
        } else {
            LOGGER.warn("The Manta Client version wasn't sent.");
        }

        if (revisionRootHandler.isVersionSystemOn()) {
            try {
                return deleteHelper.executeDelete(file, revision);
            } catch (RevisionException e) {
                return deleteHelper.createErrorResponse(HttpStatus.BAD_REQUEST,
                        "Error during delete operation. " + e.getMessage());
            }
        } else {
            // When versioning is disabled, there is only one committed revision 0.0 (technical revision)
            // and all operations are performed only within this revision
            Double latestCommittedRevision = revisionRootHandler.getLatestCommittedRevisionNumber(databaseService);
            if (latestCommittedRevision == null) {
                LOGGER.error("The database is not correctly initialized.");
                return deleteHelper.createErrorResponse(HttpStatus.BAD_GATEWAY,
                        "The database is not initialized correctly.");
            }
            return deleteHelper.executeDelete(file, latestCommittedRevision);
        }

    }

    /**
     * Request na unifikaci.
     * @author tfechtner
     *
     */
    public static class UnificationRequest {
        /** Název resource, kde se má unifikace provádět. */
        private String resourceName;
        /** Seznam predikátů na zdroje, kde jsou v mapě vždy predikáty na atributy.
         *  Klíč atributu (nebo _TYPE pro typ uzlu) na regulární výraz na hodnotu atributu.
         */
        private List<Map<String, String>> sources;
        /** Seznam predikátů na cíle, kde jsou v mapě vždy predikáty na atributy.
         *  Klíč atributu (nebo _TYPE pro typ uzlu) na regulární výraz na hodnotu atributu.
         */
        private List<Map<String, String>> targets;
        /** Zda se unifikují uzly bez ohledu na velikost písmen.*/
        private boolean caseInsensitive;
        /** Zda se má provést unifikace zdrojů, pokud je víc cílů nebo není žádný cíl. */
        private boolean forceMergeSources;
        /** Zda se má provést unifikace cílů, pokud jich je víc. */
        private boolean forceMergeTargets;

        /**
         * @return Název resource, kde se má unifikace provádět
         */
        public String getResourceName() {
            return resourceName;
        }

        /**
         * @param resourceName Název resource, kde se má unifikace provádět
         */
        public void setResourceName(String resourceName) {
            this.resourceName = resourceName;
        }

        public List<Map<String, String>> getSources() {
            return sources;
        }

        public void setSources(List<Map<String, String>> sources) {
            this.sources = sources;
        }

        public List<Map<String, String>> getTargets() {
            return targets;
        }

        public void setTargets(List<Map<String, String>> targets) {
            this.targets = targets;
        }

        public boolean isCaseInsensitive() {
            return caseInsensitive;
        }

        public void setCaseInsensitive(boolean caseInsensitive) {
            this.caseInsensitive = caseInsensitive;
        }

        public boolean isForceMergeSources() {
            return forceMergeSources;
        }

        public void setForceMergeSources(boolean forceMergeSources) {
            this.forceMergeSources = forceMergeSources;
        }

        public boolean isForceMergeTargets() {
            return forceMergeTargets;
        }

        public void setForceMergeTargets(boolean forceMergeTargets) {
            this.forceMergeTargets = forceMergeTargets;
        }

    }

}
