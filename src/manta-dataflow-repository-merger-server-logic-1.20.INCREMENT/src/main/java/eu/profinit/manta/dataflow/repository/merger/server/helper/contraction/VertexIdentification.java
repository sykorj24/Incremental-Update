package eu.profinit.manta.dataflow.repository.merger.server.helper.contraction;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;

/**
 * Unikátní identifikace vrcholu v rámci dané revizi pro daného předka. 
 * @author tfechtner
 *
 */
public class VertexIdentification {
    /** Název vrcholu. */
    private final String name;
    /** Typ vrcholu. */
    private final String type;

    /**
     * @param vertex vrchol
     */
    VertexIdentification(Vertex vertex) {
        this(vertex, false);
    }

    /**
     * @param vertex vrchol
     * @param caseInsensitive zda se jména vrcholů porovnávají case insensitive
     */
    VertexIdentification(Vertex vertex, boolean caseInsensitive) {
        if (caseInsensitive) {
            this.name = StringUtils.lowerCase(GraphOperation.getName(vertex));
        } else {
            this.name = GraphOperation.getName(vertex);
        }
        this.type = GraphOperation.getType(vertex);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(name).append(type).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        VertexIdentification rhs = (VertexIdentification) obj;
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(name, rhs.name);
        builder.append(type, rhs.type);
        return builder.isEquals();
    }

    @Override
    public String toString() {
        return name + "(" + type + ")";
    }

}