package eu.profinit.manta.dataflow.repository.merger.server.helper.revision;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;

/**
 * Třída nesoucí informace o provedené operaci.
 * @author tfechtner
 *
 * @param <O> typ možných operací
 */
public class ProcessorResult<O extends Enum<?>> {

    /** Mapa s provedenými operacemi nad vrcholy. */
    private final Map<ActionType<O, VertexType>, ActionCounter> vertexActionMap = 
            new HashMap<ActionType<O, VertexType>, ActionCounter>();
    /** Mapa s provedenými operacemi nad hranami. */
    private final Map<ActionType<O, EdgeLabel>, ActionCounter> edgeActionMap = 
            new HashMap<ActionType<O, EdgeLabel>, ActionCounter>();

    /**
     * Uloží provedenou operaci nad vrcholem.
     * @param operationType typ operace
     * @param vertexType typ vrcholu
     */
    public void saveVertexAction(O operationType, VertexType vertexType) {
        saveAction(vertexActionMap, operationType, vertexType);
    }

    /**
     * Uloží provedenou operaci nad hranou.
     * @param operationType typ provedené operace
     * @param edgeLabel typ hrany
     */
    public void saveEdgeAction(O operationType, EdgeLabel edgeLabel) {
        saveAction(edgeActionMap, operationType, edgeLabel);
    }

    /**
     * Uloží akci daného typu nad elementem daného typu.
     * @param map mapa akcí, kam se má akce uložit
     * @param operationType typ operace
     * @param elementType typ elementu
     */
    private <E extends Enum<?>> void saveAction(Map<ActionType<O, E>, ActionCounter> map, 
            O operationType, E elementType) {
        ActionType<O, E> type = new ActionType<O, E>(operationType, elementType);
        ActionCounter actionCounter = map.get(type);
        if (actionCounter == null) {
            actionCounter = new ActionCounter();
            map.put(type, actionCounter);
        }

        actionCounter.inc();
    }

    /**
     * @return aktuální stav provedenýcj operací nad vrcholy
     */
    public Map<ActionType<O, VertexType>, ActionCounter> getVertexActions() {
        return Collections.unmodifiableMap(vertexActionMap);
    }
    
    /**
     * @return stav provedených operací nad vrcholy jako string
     */
    public String printVertexActionsAsString() {
        return vertexActionMap.toString();
    }

    /**
     * @return aktuální stav provedenýcj operací nad hranami
     */
    public Map<ActionType<O, EdgeLabel>, ActionCounter> getEdgeActions() {
        return Collections.unmodifiableMap(edgeActionMap);
    }
    
    /**
     * @return provedených operací nad hrany jako string
     */
    public String printEdgeActionsAsString() {
        return edgeActionMap.toString();
    }
    
    @Override
    public String toString() {
        return "{vertices:" + vertexActionMap.toString() + ", edges:" + edgeActionMap.toString() + "}";
    }



    /**
     * Typ konkrétní akce sestávající se z dvojice typ operace a typ elementu.
     * @author tfechtner
     *
     * @param <O> enum typ pro definování operace
     * @param <E> enum typ pro definování elementu
     */
    public static class ActionType<O extends Enum<?>, E extends Enum<?>> {
        /** Typ elementu pro který se operace provádí. */
        private final E elementType;
        /** Typ operace. */
        private final O operationType;

        /**
         * @param operationType Typ operace.
         * @param elementType Typ elementu pro který se operace provádí.
         */
        public ActionType(O operationType, E elementType) {
            super();
            this.operationType = operationType;
            this.elementType = elementType;
        }

        /**
         * @return Typ operace.
         */
        public O getOperationType() {
            return operationType;
        }

        /**
         * @return Typ elementu pro který se operace provádí.
         */
        public E getElementType() {
            return elementType;
        }

        @Override
        public String toString() {
            return "[" + elementType + ":" + operationType + "]";
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((elementType == null) ? 0 : elementType.hashCode());
            result = prime * result + ((operationType == null) ? 0 : operationType.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            ActionType<?, ?> other = (ActionType<?, ?>) obj;
            if (elementType == null) {
                if (other.elementType != null) {
                    return false;
                }
            } else if (!elementType.equals(other.elementType)) {
                return false;
            }
            if (operationType == null) {
                if (other.operationType != null) {
                    return false;
                }
            } else if (!operationType.equals(other.operationType)) {
                return false;
            }
            return true;
        }        
    }

    /**
     * Počítadlo akcí.
     * @author tfechtner
     *
     */
    public static class ActionCounter {
        /** Počet proběhlých akcí. */
        private int count = 0;

        /**
         * Zvýší počítadlo o jedna. 
         */
        public void inc() {
            count++;
        }

        /**
         * @return aktuální stav počítadla
         */
        public int getCount() {
            return count;
        }

        @Override
        public String toString() {
            return String.valueOf(count);
        }
    }

}
