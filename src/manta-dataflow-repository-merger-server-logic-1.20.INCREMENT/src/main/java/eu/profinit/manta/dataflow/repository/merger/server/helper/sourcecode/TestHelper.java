package eu.profinit.manta.dataflow.repository.merger.server.helper.sourcecode;

import java.io.File;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.thinkaurelius.titan.core.TitanVertex;
import com.thinkaurelius.titan.core.TitanVertexQuery;
import com.thinkaurelius.titan.core.attribute.Cmp;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.TestTitanDatabaseProvider;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeUploadRequest;

public class TestHelper extends TestTitanDatabaseProvider {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(TestHelper.class);

    public static String mergeFile(DatabaseHolder databaseHolder, final SourceRootHandler sourceRootHandler,
            final SourceCodeHelper sourceCodeHelper, final String localName, final Double rev) {

        return databaseHolder.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TestCallback<String>() {
            @Override
            public String callMe(TitanTransaction transaction) {
                Vertex root = sourceRootHandler.getRoot(transaction);
                TitanVertexQuery query = ((TitanVertex) root).query()
                        .has(EdgeProperty.SOURCE_LOCAL_NAME.t(), GraphOperation.processValueForLocalName(localName))
                        .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, rev)
                        .direction(Direction.OUT).labels(EdgeLabel.HAS_SOURCE.t());

                Vertex sourceCodeVertex = query.vertices().iterator().next();

                SourceCodeUploadRequest uploadRequest = new SourceCodeUploadRequest();
                String fileId = sourceRootHandler.getSourceNodeId(sourceCodeVertex);
                uploadRequest.setFileId(fileId);
                try {
                    uploadRequest
                            .setFileData(Base64.encodeBase64String(FileUtils.readFileToByteArray(new File(localName))));
                } catch (IOException e) {
                    LOGGER.error("Error during reading file.", e);
                }
                sourceCodeHelper.upload(uploadRequest);

                return fileId;
            }
        });




    }
}
