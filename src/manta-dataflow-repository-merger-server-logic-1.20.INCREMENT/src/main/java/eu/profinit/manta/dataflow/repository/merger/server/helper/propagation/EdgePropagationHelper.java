package eu.profinit.manta.dataflow.repository.merger.server.helper.propagation;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.Validate;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionLockedOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionModel;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.AccessLevel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorPrefix;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverser;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverserParallel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverserSerial;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;
import eu.profinit.manta.platform.web.core.security.SecurityHelper;

/**
 * Třída pro provedení propagace hran do listů.
 * @author tfechtner
 *
 */
public class EdgePropagationHelper {
    /** Název akce pro usage stats pro propagaci hran. */
    private static final String FLOW_REPOSITORY_EDGE_PROPAGATE = "flow_repository_edgePropagate";

    /** Maximální počet hran zpropagovaných z jedné konkrétní hrany. */
    private int maximumLeafEdges;
    /** Počet paralelních vláken. */
    private int parallelThreadNumber = 4;

    /**
     * @return Maximální počet hran zpropagovaných z jedné konkrétní hrany.
     */
    public int getMaximumLeafEdges() {
        return maximumLeafEdges;
    }

    /**
     * @param maximumLeafEdges Maximální počet hran zpropagovaných z jedné konkrétní hrany.
     */
    public void setMaximumLeafEdges(int maximumLeafEdges) {
        this.maximumLeafEdges = maximumLeafEdges;
    }

    /**
     * Provede propagaci hran do listů.
     * @param superRootHandler super root stromu, ve kterém se má provést propagace
     * @param revisionRootHandler revision root handler
     * @param databaseService transakce k databázi, kde se provádí propagace 
     * @param usageStats collector pro usage statsitky
     * @param revision revize, která se má zpracovat
     * @return výsledný model s údaji
     */
    public Map<String, Object> propagateEdges(final DatabaseHolder databaseService,
            final SuperRootHandler superRootHandler, final RevisionRootHandler revisionRootHandler,
            final UsageStatsCollector usageStats, final double revision) {

        PropagatorVisitor visitor = revisionRootHandler
                .executeRevisionReadOperation(new RevisionLockedOperation<PropagatorVisitor>() {

                    @Override
                    public PropagatorVisitor call() {
                        Double latestRevisionNumber = databaseService.runInTransaction(TransactionLevel.READ, new TransactionCallback<Double>() {
                            @Override
                            public String getModuleName() {
                                return MergerController.MODULE_NAME;
                            }
                            
                            @Override
                            public Double callMe(TitanTransaction transaction) {
                                return revisionRootHandler.getLatestRevisionNumber(transaction);
                            }
                        });

                        if (revision != latestRevisionNumber) {
                            throw new RevisionException(
                                    "It is possible to propagate edges only for the latest revision.");
                        }

                        RevisionInterval revisionInterval = new RevisionInterval(revision, revision);
                        
                        // we may need previous revision number while visiting edges and possibly changing their end revision to the previous revision
                        Double previousRevisionNumber = databaseService.runInTransaction(TransactionLevel.READ, new TransactionCallback<Double>() {
                            @Override
                            public String getModuleName() {
                                return MergerController.MODULE_NAME;
                            }
                            
                            @Override
                            public Double callMe(TitanTransaction transaction) {
                                return revisionRootHandler.getPreviousRevisionNumber(transaction, revision);
                            }
                        });
                        
                        Object startVertexId = superRootHandler.getRoot(databaseService);

                        TraverserProcessorFactory processorFactory = new TraverserProcessorFactoryGeneric<TraverserProcessorPrefix>(
                                TraverserProcessorPrefix.class, MergerController.MODULE_NAME, new SearchOrderDfs());

                        GraphScalableTraverser traverser;
                        if (parallelThreadNumber > 1) {
                            traverser = new GraphScalableTraverserParallel(databaseService, processorFactory,
                                    parallelThreadNumber);
                        } else {
                            traverser = new GraphScalableTraverserSerial(databaseService, processorFactory);
                        }
                        
                        
                        PropagatorVisitor visitor = new PropagatorVisitor(revisionInterval, previousRevisionNumber);
                        visitor.setMaximumLeafEdges(getMaximumLeafEdges());

                        traverser.traverse(visitor, startVertexId, AccessLevel.WRITE_SHARE, revisionInterval);
                        return visitor;
                    }
                });

        Map<String, Object> resultModel = new HashMap<String, Object>();
        resultModel.put("transformedEdges", visitor.getTransformedEdges());
        resultModel.put("newEdges", visitor.getNewEdges());
        resultModel.put("errorEdges", visitor.getErrorEdges());

        Map<String, Object> usageMap = new HashMap<String, Object>(resultModel);
        usageMap.put("revision", revision);
        usageStats.saveAction(SecurityHelper.getUserName(), FLOW_REPOSITORY_EDGE_PROPAGATE, usageMap);

        return resultModel;
    }

    /**
     * @param parallelThreadNumber Počet paralelních vláken. 
     */
    public void setParallelThreadNumber(int parallelThreadNumber) {
        Validate.isTrue(parallelThreadNumber > 0, "Number of parallel threads must be greater than zero.");
        this.parallelThreadNumber = parallelThreadNumber;
    }

}
