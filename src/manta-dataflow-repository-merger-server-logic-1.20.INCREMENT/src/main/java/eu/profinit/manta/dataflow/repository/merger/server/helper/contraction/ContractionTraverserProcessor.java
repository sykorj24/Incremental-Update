package eu.profinit.manta.dataflow.repository.merger.server.helper.contraction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.connection.RollbackAware;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserPostProcessing;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorPrefix;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;

/**
 * Speciální traverser pro kontrakce. Kromě toho, že navštěvuje i super root, řeší hlavně potřebu 
 * mít vlastní frontu vrcholů ke zpracování, které nejsou viditelné z jiných vláken. To je z důvodu
 * neviditelnosti změn v této transakci pro ostatní vlákna. 
 * <br />
 * Do této privátní fronty přijdou všichni potomci takových vrcholů, které mají alespoň jednoho kontrahovaného potomka.
 * Tím se zajistí, že ostatní vlákna nebudou zpracovávat již zkontarhované a smazané objekty, který však ještě nebyly commitnuty.
 * <br /><br />  
 * Super root je navštíven stejnou metodou jako resource.
 * 
 * @author tfechtner
 *
 */
public class ContractionTraverserProcessor extends TraverserProcessorPrefix
        implements TraverserPostProcessing, RollbackAware<Boolean> {
    // TODO nahradit zámkem či podobným systémem
    /** Magická konstanta pro zajištění, že traverser nepřijme novjěší package, kvůli přeplánování. */
    private static final int TRANSACTION_TIMEOUT = 100;
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ContractionTraverserProcessor.class);
    /** Privátní fronta vrcholů ke zpracování. */
    private Deque<Object> privateIdsToProcess = new LinkedList<Object>();
    /** Seznam id zpracovaných vrcholů pro případný rollback. */
    private List<Long> processedVertices = new ArrayList<>();
    /** Čas v milisekundách, kdy začal traverser pracovat. */
    private long time;

    @Override
    public Object callMe(TitanTransaction transaction) {
        processedVertices.clear();
        time = System.currentTimeMillis();

        int objectsProcessed = 0;
        // neskončíme dokud nezpracujeme všechny vrcholy z privátní fronty
        while (objectsProcessed < getObjectsPerTransaction()) {
            Object objectToProcess = getSearchOrder().getNext(getIdsToProcess());
            if (objectToProcess == null) {
                break;
            }

            if (objectToProcess instanceof PrivatePackage) {
                // package musí být starší než je začátek práce traverseru = začátek jeho transakce
                PrivatePackage privatePackage = (PrivatePackage) objectToProcess;
                if (privatePackage.getTime() > time + TRANSACTION_TIMEOUT) {
                    super.getIdsToProcess().addLast(privatePackage);
                    LOGGER.debug("Early commit after {} objects.", objectsProcessed);
                    break;
                }

                for (Object id : privatePackage.getIds()) {
                    privateIdsToProcess.addLast(id);
                }
            } else {
                Vertex vertex = transaction.getVertex(objectToProcess);
                if (vertex != null && vertex.getPropertyKeys().size() > 0) {
                    if (!getVisitor().rememberVertex(vertex) || getVisitor().isRemoved(vertex)) {
                        continue;
                    }
                    processedVertices.add((Long) objectToProcess);
                    processVertex(vertex);
                } else {
                    LOGGER.warn("Vertex with id {} does not exist.", objectToProcess);
                }
                objectsProcessed++;
            }
        }

        return objectsProcessed;
    }

    @Override
    public void postProcess() {
        if (!privateIdsToProcess.isEmpty()) {
            super.getIdsToProcess().addLast(new PrivatePackage(privateIdsToProcess));
        }
    }

    @Override
    public Boolean rollback(TitanTransaction transaction) {
        int rollbackProssed = 0;
        int rollbackNulls = 0;
        int rollbackEmpties = 0;
        for (Long vertexId : processedVertices) {
            Vertex vertex = transaction.getVertex(vertexId);
            if (vertex != null) {
                if (vertex.getPropertyKeys().size() > 0 && vertex.getProperty(NodeProperty.VERTEX_TYPE.t()) != null) {
                    processVertex(vertex);
                    rollbackProssed++;
                } else {
                    rollbackEmpties++;
                }
            } else {
                rollbackNulls++;
            }
        }

        LOGGER.info("Rollback result: processed {}, empties {}, nulls {}.", rollbackProssed, rollbackEmpties,
                rollbackNulls);

        return true;
    }

    @Override
    protected void processSuperRoot(Vertex superRoot) {
        getVisitor().visitResource(superRoot);
        Deque<Object> specificDeque = resolveRightQueue(superRoot);

        List<Vertex> children = GraphOperation.getAdjacentVertices(superRoot, Direction.IN, getRevisionInterval(),
                EdgeLabel.HAS_RESOURCE);
        for (Vertex child : children) {
            specificDeque.addLast(child.getId());
        }
    }

    @Override
    protected void processResource(Vertex vertex) {
        processLayerOfResource(vertex);
        getVisitor().visitResource(vertex);
        Deque<Object> specificDeque = resolveRightQueue(vertex);

        List<Vertex> children = GraphOperation.getAdjacentVertices(vertex, Direction.IN, getRevisionInterval(),
                EdgeLabel.HAS_RESOURCE);

        // množina id vrcholů, které jsme již přidali do fronty, ochrana pro vícenásobným hranám do stejného potomka
        for (Vertex child : children) {
            if (GraphOperation.getParent(child) == null) {
                specificDeque.addLast(child.getId());
            }
        }
    }

    @Override
    protected void processNode(Vertex vertex) {
        getVisitor().visitNode(vertex);
        Deque<Object> specificDeque = resolveRightQueue(vertex);

        List<Vertex> children = GraphOperation.getAdjacentVertices(vertex, Direction.IN, getRevisionInterval(),
                EdgeLabel.HAS_PARENT);

        // množina id vrcholů, které jsme již přidali do fronty, ochrana pro vícenásobným hranám do stejného potomka
        for (Vertex child : children) {
            specificDeque.addLast(child.getId());
        }
    }

    /**
     * Zjistí, do které fronty se mají přidat potomci pro daný vrchol. 
     * @param parentVertex vrchol, pro jehož potomky se hledá fronta
     * @return příslušná fronta pro potomky daného předka
     */
    private Deque<Object> resolveRightQueue(Vertex parentVertex) {
        if (getVisitor().isVertexContractedChildren(parentVertex)) {
            return privateIdsToProcess;
        } else {
            return super.getIdsToProcess();
        }
    }

    @Override
    protected Deque<Object> getIdsToProcess() {
        if (!privateIdsToProcess.isEmpty()) {
            return privateIdsToProcess;
        } else {
            return super.getIdsToProcess();
        }
    }

    @Override
    protected ContractionVisitor getVisitor() {
        return (ContractionVisitor) super.getVisitor();
    }

    /**
     * Balík obsahující seznam id vrcholů ke zpracování.
     * Tyto vrcholy byly v privátní forntě nějaké procesoru, který skončil kvůli limitu na počet v transakci.
     * Balík si nese timestamp, aby bylo zajištěno, že ho zpracuje procesor, který má již novější transakci. 
     * @author tfechtner
     *
     */
    private static class PrivatePackage {
        /** ID vrcholů ke zpracování. */
        private final List<Object> ids;
        /** Čas vzniku balíku. */
        private final long time;

        /**
         * @param ids ID vrcholů ke zpracování
         */
        public PrivatePackage(Collection<Object> ids) {
            super();
            this.ids = Collections.unmodifiableList(new ArrayList<>(ids));
            this.time = System.currentTimeMillis();
        }

        /**
         * @return ID vrcholů ke zpracování
         */
        public List<Object> getIds() {
            return ids;
        }

        /**
         * @return Čas vzniku balíku
         */
        public long getTime() {
            return time;
        }

    }
}
