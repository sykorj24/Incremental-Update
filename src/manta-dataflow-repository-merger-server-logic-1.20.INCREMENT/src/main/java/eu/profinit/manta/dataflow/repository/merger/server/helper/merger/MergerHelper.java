package eu.profinit.manta.dataflow.repository.merger.server.helper.merger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionLockedOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionModel;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.controller.InterpolationController;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;
import eu.profinit.manta.dataflow.repository.merger.server.helper.AbstractControllerHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.MergerProcessor;
import eu.profinit.manta.platform.licensing.LicenseHolder;
import eu.profinit.manta.platform.scriptmetadata.service.ScriptMetadataService;

/**
 * Helper pro merger controller.
 * @author tfechtner
 *
 */
public class MergerHelper extends AbstractControllerHelper {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(MergerHelper.class);

    /** Počet nových objektů, než dojde ke commitu. */
    private int newObjectsPerCommit;
    /** True, jestliže je merger paralelní. */
    private boolean isMergerParallel = true;

    @Autowired
    private MergerProcessor mergerProcessor;
    @Autowired
    private LicenseHolder licenseHolder;
    @Autowired
    private ScriptMetadataService scriptMetadataService;
    
    /**
     * Provede merge pro daná data nad danou revizi.
     * 
     * @param file  soubor obsahující data pro mergování
     * @param revision  revize, kam se verzuje
     * @return výsledek merge
     */
    public ResponseEntity<Map<String, Object>> executeMerge(final MultipartFile file, final Double revision) {
        Map<String, Object> resultMap = new HashMap<String, Object>();

        long startTime = System.currentTimeMillis();
        final OneFileMerger processor = new OneFileMerger(getDatabaseService(), getSuperRootHandler(),
                getRevisionRootHandler(), getSourceRootHandler(), mergerProcessor, licenseHolder, scriptMetadataService,
                newObjectsPerCommit, isMergerParallel);
        try {
            if (getRevisionRootHandler().isVersionSystemOn()) {
                resultMap.putAll(executeMergeWithVcs(file, revision, processor));
            } else {
                resultMap.putAll(executeMergeWithoutVcs(file, revision, processor));
            }
        } catch (ProcessorException e) {
            LOGGER.error("Error during reading input file.", e);
            return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Error during reading input file.");
        }

        long proccessingTime = System.currentTimeMillis() - startTime;
        resultMap.put("proccessingTime", proccessingTime);
        return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
    }

    /**
     * Vykonná merge pro daný soubor s daným procesorem bez použití version control system.
     * @param file soubor ke zpracování
     * @param revision revize, do které se soubor merguje
     * @param processor procesor provádějící merge
     * @return výsledek merge
     */
    protected Map<String, Object> executeMergeWithoutVcs(final MultipartFile file, final Double revision,
            final OneFileMerger processor) {
        try {
            return processor.process(file.getInputStream(), revision);
        } catch (IOException e) {
            throw new ProcessorException(e);
        }
    }

    /**
     * Vykonná merge pro daný soubor s daným procesorem s využití version control system.
     * @param file soubor ke zpracování
     * @param revision revize, do které se soubor merguje
     * @param processor procesor provádějící merge
     * @return výsledek merge
     */
    protected Map<String, Object> executeMergeWithVcs(final MultipartFile file, final Double revision,
            final OneFileMerger processor) {

        return getRevisionRootHandler()
                .executeRevisionReadOperation(new RevisionLockedOperation<Map<String, Object>>() {
                    @Override
                    public Map<String, Object> call() {
                        TransactionLevel level = isMergerParallel ? TransactionLevel.WRITE_SHARE
                                                                  : TransactionLevel.WRITE_EXCLUSIVE;
                        getDatabaseService().runInTransaction(level, new TransactionCallback<Object>() {
                            @Override
                            public Object callMe(final TitanTransaction transaction) {
                                getRevisionRootHandler().checkMergeConditions(transaction, revision);
                                return null;
                            }

                            @Override
                            public String getModuleName() {
                                return MergerController.MODULE_NAME;
                            }
                        });

                        try {
                            return processor.process(file.getInputStream(), revision);
                        } catch (IOException e) {
                            throw new ProcessorException(e);
                        }
                    }
                });

    }

    /**
     * Provede přídavný merge do poslední revize.
     * Tato revize musí být již commitnutá a nesmí existovat novější snapshot.
     * 
     * @param file soubor s daty k mergi
     * @return výsledek operace
     */
    public ResponseEntity<Map<String, Object>> executeAdditionalMerge(final MultipartFile file) {
        Map<String, Object> resultMap = getRevisionRootHandler()
                .executeRevisionReadOperation(new RevisionLockedOperation<Map<String, Object>>() {
                    @Override
                    public Map<String, Object> call() {
                        Double latestUncommittedRevision = getDatabaseService().runInTransaction(TransactionLevel.READ,
                                new TransactionCallback<Double>() {
                                    @Override
                                    public String getModuleName() {
                                        return MergerController.MODULE_NAME;
                                    }

                                    @Override
                                    public Double callMe(TitanTransaction transaction) {
                                        return getRevisionRootHandler().getLatestUncommittedRevisionNumber(transaction);
                                    }
                                });

                        Double latestCommittedRevision = getDatabaseService().runInTransaction(TransactionLevel.READ,
                                new TransactionCallback<Double>() {
                                    @Override
                                    public String getModuleName() {
                                        return MergerController.MODULE_NAME;
                                    }

                                    @Override
                                    public Double callMe(TitanTransaction transaction) {
                                        return getRevisionRootHandler().getLatestCommittedRevisionNumber(transaction);
                                    }
                                });
                        
                        if(latestUncommittedRevision == null) {
                            if(latestCommittedRevision == null) {
                                throw new RevisionException("No revision exists in the database.");
                            }
                        } else {
                            // there has to exist an uncommitted revision => latest revision is uncommitted
                            throw new RevisionException("Additional merge is possible only into the commited revision.");
                        }
                        
                        OneFileMerger processor = new OneFileMerger(getDatabaseService(), getSuperRootHandler(),
                                getRevisionRootHandler(), getSourceRootHandler(), mergerProcessor,
                                licenseHolder, scriptMetadataService, newObjectsPerCommit, isMergerParallel);

                        try {
                            return processor.process(file.getInputStream(), latestCommittedRevision);
                        } catch (IOException e) {
                            throw new ProcessorException(e);
                        }
                    }
                });

        return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);

    }

    /**
     * @return Počet nových objektů, než dojde ke commitu.
     */
    public int getNewObjectsPerCommit() {
        return newObjectsPerCommit;
    }

    /**
     * @param newObjectsPerCommit Počet nových objektů, než dojde ke commitu.
     */
    public void setNewObjectsPerCommit(int newObjectsPerCommit) {
        this.newObjectsPerCommit = newObjectsPerCommit;
    }

    /**
     * @param isMergerParallel True, jestliže je merger paralelní. 
     */
    public void setIsMergerParallel(boolean isMergerParallel) {
        this.isMergerParallel = isMergerParallel;
    }

    /**
     * Výjimka značící problém v procesoru při načítání vstupu.
     * @author tfechtner
     *
     */
    private static class ProcessorException extends RuntimeException {
        private static final long serialVersionUID = -3187442903130066862L;

        /**
         * @param cause výjima způsbující problém
         */
        ProcessorException(Throwable cause) {
            super(cause);
        }
    }
}
