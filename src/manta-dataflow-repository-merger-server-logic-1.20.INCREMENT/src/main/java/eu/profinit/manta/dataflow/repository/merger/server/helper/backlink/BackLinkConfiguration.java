package eu.profinit.manta.dataflow.repository.merger.server.helper.backlink;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.predicate.FalsePredicate;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.VertexPredicate;

/**
 * Konfigurace napojování zpětný linků pro view.
 * Specifické pro dialekt. 
 * @author tfechtner
 *
 */
public class BackLinkConfiguration {
    /** Id konfigurace pro vyhledávání. */
    private String configurationId;
    /** Predikt pro zjištění jestli je daný vertex view. */
    private VertexPredicate viewPredicate = FalsePredicate.getInstance();
    /** Predikát pro zjištění, jestli je daný vertex sloupec ve view. */
    private VertexPredicate viewColumnPredicate = FalsePredicate.getInstance();
    /** Predikát pro zjištění, jestli je daný vertex sloupec v příkazu create view. */
    private VertexPredicate createViewPredicate = FalsePredicate.getInstance();
    /** Predikát pro zjištění, jestli je daný vertex sloupec ve zdroji (např. tabulka). */
    private VertexPredicate sourceColumnPredicate = FalsePredicate.getInstance();
    /** Predikát pro zjištění, jestli je daný vertex sloupec v aliasu (např. plsql synonymum). */
    private VertexPredicate aliasColumnPredicate = FalsePredicate.getInstance();

    /**
     * Zjistí jestli je vrchol view.
     * @param vertex vrchol, který se testuje
     * @param revisionInterval interval revizí, pro které se predikát ověřuje
     * @return true, jestli vrchol odpovídá podmínce
     */
    public boolean isView(Vertex vertex, RevisionInterval revisionInterval) {
        return viewPredicate.evaluate(vertex, revisionInterval);
    }

    /**
     * Zjistí jestli je vrchol sloupec ve view.
     * @param vertex vrchol, který se testuje
     * @param revisionInterval interval revizí, pro které se predikát ověřuje
     * @return true, jestli vrchol odpovídá podmínce
     */
    public boolean isViewColumn(Vertex vertex, RevisionInterval revisionInterval) {
        return viewColumnPredicate.evaluate(vertex, revisionInterval);
    }

    /**
     * Zjistí jestli je vrchol sloupec v příkazu create view.
     * @param vertex vrchol, který se testuje
     * @param revisionInterval interval revizí, pro které se predikát ověřuje
     * @return true, jestli vrchol odpovídá podmínce
     */
    public boolean isCreateViewColumn(Vertex vertex, RevisionInterval revisionInterval) {
        return createViewPredicate.evaluate(vertex, revisionInterval);
    }

    /**
     * Zjistí jestli je vrchol sloupec ve zdroji (např. tabulka).
     * @param vertex vrchol, který se testuje
     * @param revisionInterval interval revizí, pro které se predikát ověřuje
     * @return true, jestli vrchol odpovídá podmínce
     */
    public boolean isSourceColumn(Vertex vertex, RevisionInterval revisionInterval) {
        return sourceColumnPredicate.evaluate(vertex, revisionInterval);
    }

    /**
     * Zjistí jestli je vrchol sloupec v aliasu (např. plsql synonymum).
     * @param vertex vrchol, který se testuje
     * @param revisionInterval interval revizí, pro které se predikát ověřuje
     * @return true, jestli vrchol odpovídá podmínce
     */
    public boolean isAliasColumn(Vertex vertex, RevisionInterval revisionInterval) {
        return aliasColumnPredicate.evaluate(vertex, revisionInterval);
    }

    /**
     * @param viewPredicate Predikt pro zjištění jestli je daný vertex view. 
     */
    public void setViewPredicate(VertexPredicate viewPredicate) {
        this.viewPredicate = viewPredicate;
    }

    /**
     * @param viewColumnPredicate Predikát pro zjištění, jestli je daný vertex sloupec ve view. 
     */
    public void setViewColumnPredicate(VertexPredicate viewColumnPredicate) {
        this.viewColumnPredicate = viewColumnPredicate;
    }

    /**
     * @param createViewPredicate Predikát pro zjištění, jestli je daný vertex sloupec v příkazu create view. 
     */
    public void setCreateViewPredicate(VertexPredicate createViewPredicate) {
        this.createViewPredicate = createViewPredicate;
    }

    /**
     * @param sourceColumnPredicate Predikát pro zjištění, jestli je daný vertex sloupec ve zdroji
     *  (např. tabulka). 
     */
    public void setSourceColumnPredicate(VertexPredicate sourceColumnPredicate) {
        this.sourceColumnPredicate = sourceColumnPredicate;
    }

    /**
     * @param aliasColumnPredicate Predikát pro zjištění, jestli je daný vertex sloupec v aliasu
     * (např. plsql synonymum). 
     */
    public void setAliasColumnPredicate(VertexPredicate aliasColumnPredicate) {
        this.aliasColumnPredicate = aliasColumnPredicate;
    }

    /**
     * @return Id konfigurace pro vyhledávání. 
     */
    public String getConfigurationId() {
        return configurationId;
    }

    /**
     * @param configurationId Id konfigurace pro vyhledávání. 
     */
    public void setConfigurationId(String configurationId) {
        this.configurationId = configurationId;
    }
}
