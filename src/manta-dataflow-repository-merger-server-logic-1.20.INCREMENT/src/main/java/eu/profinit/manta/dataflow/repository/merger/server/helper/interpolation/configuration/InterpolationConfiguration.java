package eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.Validate;

/**
 * Konfigurae interpolace hran.
 *  
 * @author onouza
 */
public class InterpolationConfiguration {

    /**
     * Typy uzlu, ktere jsou pokladany za transformacni v MAPOVANE vrstve.
     */
    private List<String> transformations = new ArrayList<>();
    /**
     * Definice struktury uzlu transformaci v INTERPOLOVANE vrstve.
     * Seznam je usporadan sestupne podle urovne hierarchii uzlu.
     * Napr. pokud bude obsahovat definice uzlu "Application", "Layer", "Transformation", "Transformation Asset"
     * a "Transfromation Attribute", znamena to, ze pri vytvareni transformace v INTERPOLOVANE vrstve se
     * bude na vrcholu uzel typu "Application", pod nim uzel typu "Layer", atd., az po uzel typu
     * "Transfromation Attribute", ktery bude listem.   
     */
    private List<TransformationNodeDef> transformationNodes = new ArrayList<>();

    // gettery / settery
    
    /**
     * @return Typy uzlu, ktere jsou pokladany za transformacni v MAPOVANE vrstve.
     */
    public List<String> getTransformations() {
        return new ArrayList<>(transformations);
    }

    /**
     * @param transformations Typy uzlu, ktere jsou pokladany za transformacni v MAPOVANE vrstve.
     */
    public void setTransformations(List<String> transformations) {
        Validate.notNull(transformations, "'transformations' must not be null");
        this.transformations = new ArrayList<>(transformations);
    }

    /**
     * @return Definice struktury uzlu transformaci v INTERPOLOVANE vrstve.
     *          Seznam je usporadan sestupne podle urovne hierarchii uzlu.
     *          Napr. pokud bude obsahovat definice uzlu "Application", "Layer", "Transformation", "Transformation Asset"
     *          a "Transfromation Attribute", znamena to, ze pri vytvareni transformace v INTERPOLOVANE vrstve se
     *          bude na vrcholu uzel typu "Application", pod nim uzel typu "Layer", atd., az po uzel typu
     *          "Transfromation Attribute", ktery bude listem.
     */
    public List<TransformationNodeDef> getTransformationNodes() {
        return new ArrayList<>(transformationNodes);
    }

    /**
     * @param transformationNodes Definice struktury uzlu transformaci v INTERPOLOVANE vrstve.
     *          Seznam je usporadan sestupne podle urovne hierarchii uzlu.
     *          Napr. pokud bude obsahovat definice uzlu "Application", "Layer", "Transformation", "Transformation Asset"
     *          a "Transfromation Attribute", znamena to, ze pri vytvareni transformace v INTERPOLOVANE vrstve se
     *          bude na vrcholu uzel typu "Application", pod nim uzel typu "Layer", atd., az po uzel typu
     *          "Transfromation Attribute", ktery bude listem.   
     */
    public void setTransformationNodes(List<TransformationNodeDef> transformationNodes) {
        Validate.notNull(transformationNodes, "'transformationNodes' must not be null");
        this.transformationNodes = new ArrayList<>(transformationNodes);
    }
    
}
