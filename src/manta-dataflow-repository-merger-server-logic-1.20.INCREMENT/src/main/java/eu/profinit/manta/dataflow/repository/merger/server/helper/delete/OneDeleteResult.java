package eu.profinit.manta.dataflow.repository.merger.server.helper.delete;

/**
 * Class representing result of one vertex delete operation (and its subtree structure)
 * 
 * @author jsykora
 *
 */
public class OneDeleteResult {
    /**
     * Result of the delete operation
     * 
     * @author jsykora
     * 
     */
    public enum ResultType {
        /** Delete was successful */
        SUCCESSFUL,
        /** Delete was NOT successful, an error occurred */
        ERROR;
    }

    /** Original line representing vertex path from the input file */
    private String line;
    /** Result of the delete operation */
    private ResultType resultType;
    private String errorMessage;

    public OneDeleteResult(String line, ResultType resultType) {
        super();
        this.line = line;
        this.resultType = resultType;
    }

    public OneDeleteResult(String line, ResultType resultType, String errorMessage) {
        super();
        this.line = line;
        this.resultType = resultType;
        this.errorMessage = errorMessage;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public ResultType getResultType() {
        return resultType;
    }

    public void setResultType(ResultType resultType) {
        this.resultType = resultType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        String output = "OneDeleteResult [line=" + line + " || resultType=" + resultType + "]";
        if (ResultType.ERROR.equals(resultType)) {
            output += "\n" + errorMessage;
        }
        return output;
    }

}
