package eu.profinit.manta.dataflow.repository.merger.server.helper.backlink;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.AccessLevel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorPrefix;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverser;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverserSerial;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;
import eu.profinit.manta.platform.web.core.security.SecurityHelper;

/**
 * Helper pro doplnění zpětých hran pro view, do který teče nějaký datový tok.
 * @author tfechtner
 *
 */
public class BackLinkHelper {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(BackLinkHelper.class);
    /** Název akce pro usage stats pro tvorbu zpětných hran. */
    private static final String FLOW_REPOSITORY_BACK_LINKS = "flow_repository_backLinks";

    /** Množina podporovaných konfigurací. */
    private Set<BackLinkConfiguration> backLinkConfigurationSet;

    @Autowired
    private DatabaseHolder databaseService;

    @Autowired
    private SuperRootHandler superRootHandler;

    /**
     * Doplní zpětné hrany pro view, do kterých tečen nějaký datový tok jiný než z create view.
     * @param configurationId id konfigurace, která se má pro doplňování použít
     * @param resourceName název resource, kde se mají view hledat
     * @param usageStatsCollector collector pro usage statsitky
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     * @return mapa s výsledkem
     */
    public Map<String, Object> createBackLinksForResourceName(final String configurationId, final String resourceName,
            final UsageStatsCollector usageStatsCollector, final RevisionInterval revisionInterval) {

        BackLinkConfiguration backLinkConfiguration = getConfigurationByName(configurationId);

        if (backLinkConfiguration == null) {
            LOGGER.warn("The requested dialect " + configurationId + " does not exist.");
            return Collections.<String, Object> singletonMap("error",
                    "The requested dialect " + configurationId + " does not exist.");
        }

        Object resourceId = getResourceIdByName(resourceName, revisionInterval);
        if (resourceId == null) {
            String message = "The requested resource with name " + resourceName + " does not exist.";
            LOGGER.warn(message);
            return Collections.<String, Object> singletonMap("error", message);
        }

        int backLinksCount = createBacklinksForResourceId(resourceId, revisionInterval, backLinkConfiguration);

        Map<String, Object> usageMap = new HashMap<String, Object>();
        usageMap.put("revisionInterval", revisionInterval);
        usageMap.put("backLinks", backLinksCount);
        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REPOSITORY_BACK_LINKS, usageMap);

        return Collections.<String, Object> singletonMap("backlinks", backLinksCount);
    }

    /**
     * Doplní zpětné hrany pro view, do kterých teče nějaký datový tok jiný než z create view.
     * @param configurationId id konfigurace, která se má pro doplňování použít
     * @param resourceType typ resource, kde se mají view hledat
     * @param usageStatsCollector collector pro usage statsitky
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     * @return mapa s výsledkem
     */
    public Map<String, Object> createBackLinksForResourceType(final String configurationId, final String resourceType,
            final UsageStatsCollector usageStatsCollector, final RevisionInterval revisionInterval) {

        BackLinkConfiguration backLinkConfiguration = getConfigurationByName(configurationId);

        if (backLinkConfiguration == null) {
            LOGGER.warn("The requested dialect " + configurationId + " does not exist.");
            return Collections.<String, Object> singletonMap("error",
                    "The requested dialect " + configurationId + " does not exist.");
        }

        Set<Object> idSet = getResourceIdsByType(resourceType, revisionInterval);
        int backLinkNumber = 0;
        for (Object id : idSet) {
            backLinkNumber += createBacklinksForResourceId(id, revisionInterval, backLinkConfiguration);
        }

        Map<String, Object> usageMap = new HashMap<String, Object>();
        usageMap.put("revisionInterval", revisionInterval);
        usageMap.put("backLinks", backLinkNumber);
        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REPOSITORY_BACK_LINKS, usageMap);

        return Collections.<String, Object> singletonMap("backlinks", backLinkNumber);
    }

    /**
     * Doplní zpětné hrany pro view, do kterých teče nějaký datový tok jiný než z create view.
     * @param resourceId id resource, kde se mají 
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     * @param backLinkConfiguration konfigurace, která se má pro doplňování použít
     * @return počet vytvořených zpětných hran
     */
    private int createBacklinksForResourceId(Object resourceId, final RevisionInterval revisionInterval,
            BackLinkConfiguration backLinkConfiguration) {
        TraverserProcessorFactory processorFactory = new TraverserProcessorFactoryGeneric<TraverserProcessorPrefix>(
                TraverserProcessorPrefix.class, MergerController.MODULE_NAME, new SearchOrderDfs());
        GraphScalableTraverser traverser = new GraphScalableTraverserSerial(databaseService, processorFactory);

        BackLinkDatabaseHelper databaseHelper = new BackLinkDatabaseHelper(backLinkConfiguration);

        BackLinkGraphVisitor visitor = new BackLinkGraphVisitor(databaseService, backLinkConfiguration, databaseHelper,
                revisionInterval);
        traverser.traverse(visitor, resourceId, AccessLevel.READ_NON_EXCLUSIVE, revisionInterval);
        return visitor.getBackLinksCount();
    }

    /**
     * Získá id resource s daným jménem.
     * @param resourceName jméno hledaného resource
     * @param revisionInterval interval revizí, pro který se resource hledá
     * @return id vrcholu pro daný resource, nebo null, když takový resource neexistuje
     */
    private Object getResourceIdByName(final String resourceName, final RevisionInterval revisionInterval) {
        return databaseService.runInTransaction(TransactionLevel.READ, new TransactionCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex superRoot = superRootHandler.getRoot(transaction);
                List<Vertex> resources = GraphOperation.getAdjacentVertices(superRoot, Direction.IN, revisionInterval,
                        EdgeLabel.HAS_RESOURCE);
                for (Vertex v : resources) {
                    if (GraphOperation.getName(v).equals(resourceName)) {
                        return v.getId();
                    }
                }
                return null;
            }

            @Override
            public String getModuleName() {
                return MergerController.MODULE_NAME;
            }
        });
    }

    /**
     * Získá id resource s daným jménem.
     * @param resourceName jméno hledaného resource
     * @param revisionInterval interval revizí, pro který se resource hledá
     * @return id vrcholu pro daný resource, nebo null, když takový resource neexistuje
     */
    private Set<Object> getResourceIdsByType(final String resourceType, final RevisionInterval revisionInterval) {
        return databaseService.runInTransaction(TransactionLevel.READ, new TransactionCallback<Set<Object>>() {
            @Override
            public Set<Object> callMe(TitanTransaction transaction) {
                Vertex superRoot = superRootHandler.getRoot(transaction);
                List<Vertex> resources = GraphOperation.getAdjacentVertices(superRoot, Direction.IN, revisionInterval,
                        EdgeLabel.HAS_RESOURCE);

                Set<Object> idSet = new HashSet<Object>();
                for (Vertex v : resources) {
                    if (GraphOperation.getType(v).equals(resourceType)) {
                        idSet.add(v.getId());
                    }
                }
                return idSet;
            }

            @Override
            public String getModuleName() {
                return MergerController.MODULE_NAME;
            }
        });
    }

    /**
     * Vyhledá konfiguraci s daným id.
     * @param configurationId id konfigurace pro vyhledání 
     * @return konfigurace s daným id nebo null
     */
    private BackLinkConfiguration getConfigurationByName(String configurationId) {
        if (backLinkConfigurationSet != null) {
            for (BackLinkConfiguration configuration : backLinkConfigurationSet) {
                if (configuration.getConfigurationId().equals(configurationId)) {
                    return configuration;
                }
            }
        }

        return null;
    }

    /**
     * @param databaseService služba pro práci s databází
     */
    public void setDatabaseService(DatabaseHolder databaseService) {
        this.databaseService = databaseService;
    }

    /**
     * @param superRootHandler držák na super root
     */
    public void setSuperRootHandler(SuperRootHandler superRootHandler) {
        this.superRootHandler = superRootHandler;
    }

    /**
     * @param backLinkConfigurationSet množina konfigurací pro back linky
     */
    public void setBackLinkConfigurationSet(Set<BackLinkConfiguration> backLinkConfigurationSet) {
        this.backLinkConfigurationSet = backLinkConfigurationSet;
    }
}
