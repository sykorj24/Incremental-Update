package eu.profinit.manta.dataflow.repository.merger.server.helper.revision;

import com.tinkerpop.blueprints.Edge;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;

/**
 * Rollback stav hrany značící, co se má provést.
 * @author tfechtner
 *
 */
public enum RollbackState {
    /** Vrchol připojený hranou se má smazat.*/
    DELETE,
    /** Snížit koncovou revizi hrany před rollbackovanou verzi.*/
    ROLLBACK,
    /** Není třeba provádět změnu.*/
    NOT_CHANGE;

    /**
     * Zjistit stav pro danou hranu.
     * @param edge zkoumaná hrana
     * @param revisionInterval rollbackovaný interval
     * @return výsledný rollback stav
     */
    public static RollbackState findState(Edge edge, RevisionInterval revisionInterval) {
        Double tranStart = edge.getProperty(EdgeProperty.TRAN_START.t());
        Double tranEnd = edge.getProperty(EdgeProperty.TRAN_END.t());
        if (tranEnd < revisionInterval.getStart()) {
            return NOT_CHANGE;
        } else if (tranStart >= revisionInterval.getStart()) {
            return DELETE;
        } else {
            return ROLLBACK;
        }
    }
}