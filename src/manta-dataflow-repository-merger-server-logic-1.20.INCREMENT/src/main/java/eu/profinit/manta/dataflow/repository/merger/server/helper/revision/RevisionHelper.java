package eu.profinit.manta.dataflow.repository.merger.server.helper.revision;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionLockedOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionModel;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.AccessLevel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorPostfix;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverser;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverserSerial;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;
import eu.profinit.manta.dataflow.repository.merger.server.helper.AbstractControllerHelper;

/**
 * @author tfechtner
 *
 */
public class RevisionHelper extends AbstractControllerHelper {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(RevisionHelper.class);
    /** Zpráva při pokusu o neverzovanou službu při zapnutém verzování. */
    public static final String ENABLED_VERSION_SYSTEM_ERROR = "The version system is enabled, "
            + "therefore the methods must be used with specific revision.";
    /** Zpráva při pokusu o verzovanou službu s vypnutým verozváním. */
    public static final String DISABLED_VERSION_SYSTEM_ERROR = "The version system is disabled.";

    /**
     * Vykoná danou operaci, přičemž ošetří případné chybové stavy.
     * @param operation operace, který se má vykonat
     * @param revision číslo revize, pro kterou se operace vykonává
     * @return výsledek operace jako response přímo k odeslání
     * 
     * @param <T> návratový typ převolávané operace
     */
    public <T> ResponseEntity<Map<String, Object>> executeRevisionOperationAsResponse(
            final RevisionControllerCallback<T> operation, final Double revision) {
        Map<String, Object> resultMap = executeRevisionOperationAsMap(operation, revision);
        if (resultMap.containsKey("error")) {
            return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
        }
    }

    /**
     * Vykoná danou operaci, přičemž ošetří případné chybové stavy.
     * @param operation operace, který se má vykonat
     * @param revision číslo revize, pro kterou se operace vykonává
     * @return výsledek operace jako mapa atributů pro model
     * 
     * @param <T> návratový typ převolávané operace
     */
    public <T> Map<String, Object> executeRevisionOperationAsMap(final RevisionControllerCallback<T> operation,
            final Double revision) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        if (!getRevisionRootHandler().isVersionSystemOn()) {
            resultMap.put("error", RevisionHelper.DISABLED_VERSION_SYSTEM_ERROR);
            return resultMap;
        }

        try {
            T result = getDatabaseService().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TransactionCallback<T>() {
                @Override
                public T callMe(TitanTransaction transaction) {
                    return operation.call(transaction, revision);
                }

                @Override
                public String getModuleName() {
                    return MergerController.MODULE_NAME;
                }
            });

            resultMap.put(operation.getResultName(), result);
            resultMap.put("success", "Operation " + operation.getOperationName() + " has been successful.");
        } catch (RevisionException e) {
            resultMap.put("error", "Error during " + operation.getOperationName() + ". " + e.getMessage());
        }

        return resultMap;
    }

    /**
     * Rollbackne danou revizi.
     * @param revision číslo revize k rollbackování
     * @return výsledek rollbacku
     */
    public ProcessorResult<RollbackState> rollbackRevision(final Double revision) {
        return getRevisionRootHandler()
                .executeRevisionWriteOperation(new RevisionLockedOperation<ProcessorResult<RollbackState>>() {
                    @Override
                    public ProcessorResult<RollbackState> call() {
                        if (revision == null) {
                            throw new IllegalArgumentException("Revision must not be null.");
                        }

                        RevisionModel revisionModel = getRevisionRootHandler().getSpecificModel(getDatabaseService(),
                                revision);
                        if (revisionModel == null) {
                            throw new IllegalArgumentException("Revision " + revision + " does not exist.");
                        }
                        if (revisionModel.isCommitted()) {
                            throw new IllegalArgumentException("Revision " + revision + " is already committed.");
                        }

                        TraverserProcessorFactory processorFactory;
                        processorFactory = new TraverserProcessorFactoryGeneric<TraverserProcessorPostfix>(
                                TraverserProcessorPostfix.class, MergerController.MODULE_NAME, new SearchOrderDfs());
                        GraphScalableTraverser traverser = new GraphScalableTraverserSerial(getDatabaseService(),
                                processorFactory);

                        RevisionInterval revisionInterval = new RevisionInterval(revision);
                        Object startVertexId = getSuperRootHandler().getRoot(getDatabaseService());

                        ProcessorResult<RollbackState> processorResult = new ProcessorResult<RollbackState>();
                        RollbackGraphVisitor visitor = new RollbackGraphVisitor(revisionInterval, processorResult);

                        traverser.traverse(visitor, startVertexId, AccessLevel.WRITE, revisionInterval);

                        visitor.processSourceCodeVertices(getDatabaseService(), getSourceRootHandler());

                        getRevisionRootHandler().removeRevisionNode(getDatabaseService(), revision);

                        return visitor.getProcessorResult();
                    }
                });
    }

    /**
     * Prořeže staré revize. 
     * @param revision číslo revize až do které se maže
     * @return výsledek prořezání
     */
    public ProcessorResult<PruneState> pruneToRevision(final Double revision) {
        return getRevisionRootHandler()
                .executeRevisionWriteOperation(new RevisionLockedOperation<ProcessorResult<PruneState>>() {
                    @Override
                    public ProcessorResult<PruneState> call() {
                        if (revision == null) {
                            throw new IllegalArgumentException("Revision must not be null.");
                        }

                        RevisionModel oldestRevisionModel = getRevisionRootHandler()
                                .getOldestModel(getDatabaseService());
                        if (oldestRevisionModel.getRevision() > revision) {
                            LOGGER.info(
                                    "Revision {} is smaller than the oldest existed revision {}. "
                                            + "The prune was not executed.",
                                    revision, oldestRevisionModel.getRevision());
                            return new ProcessorResult<PruneState>();
                        }

                        RevisionModel revisionModel = getRevisionRootHandler().getSpecificModel(getDatabaseService(),
                                revision);
                        if (revisionModel == null) {
                            throw new IllegalArgumentException("Revision " + revision + " does not exist.");
                        }

                        Double latestCommittedRevision = getRevisionRootHandler().getLatestCommittedRevisionNumber(getDatabaseService());
                        if (latestCommittedRevision <= revisionModel.getRevision()) {
                            throw new IllegalArgumentException("It is not possible to prune the latest committed revision.");
                        }

                        TraverserProcessorFactory processorFactory;
                        processorFactory = new TraverserProcessorFactoryGeneric<TraverserProcessorPostfix>(
                                TraverserProcessorPostfix.class, MergerController.MODULE_NAME, new SearchOrderDfs());
                        GraphScalableTraverser traverser = new GraphScalableTraverserSerial(getDatabaseService(),
                                processorFactory);

                        RevisionInterval revisionIntervalToDel = new RevisionInterval(0, revision);
                        Object startVertexId = getSuperRootHandler().getRoot(getDatabaseService());

                        ProcessorResult<PruneState> processorResult = new ProcessorResult<PruneState>();
                        PruneGraphVisitor visitor = new PruneGraphVisitor(revisionIntervalToDel, processorResult);

                        traverser.traverse(visitor, startVertexId, AccessLevel.WRITE, revisionIntervalToDel);

                        visitor.processSourceCodeVertices(getDatabaseService(), getSourceRootHandler());

                        getRevisionRootHandler().removeRevisionInterval(getDatabaseService(), revisionIntervalToDel);

                        return visitor.getProcessorResult();
                    }
                });
    }

    /**
     * Smaže staré revize, aby zůstal jen daný počet.
     * @param preservedRevisions počet commitnutých revizí, které se mají zachovat
     * @return výsledek prořezání
     */
    public ProcessorResult<PruneState> pruneOldestRevisions(final int preservedRevisions) {
        Double latestDeletedRevisionNumber = getRevisionRootHandler().executeRevisionWriteOperation(new RevisionLockedOperation<Double>() {
                    @Override
                    public Double call() {
                        Double latestCommittedRevNumber = getRevisionRootHandler().getLatestCommittedRevisionNumber(getDatabaseService());
                        RevisionModel latestCommittedRevModel = getRevisionRootHandler().getSpecificModel(getDatabaseService(), latestCommittedRevNumber);
                        Double previousRevNumber = latestCommittedRevModel.getPreviousRevision();
                        for (int i = 0; i < preservedRevisions - 1; i++) {
                            if(previousRevNumber == null) {
                                // nothing to prune
                                return -1.0;
                            }
                            RevisionModel revModel = getRevisionRootHandler().getSpecificModel(getDatabaseService(), previousRevNumber);
                            previousRevNumber = revModel.getPreviousRevision();
                        }
                        if(previousRevNumber == null) {
                            // nothing to prune
                            return -1.0;
                        }
                        return previousRevNumber;
                    }
                });

        return pruneToRevision(latestDeletedRevisionNumber);
    }

    /**
     * Vypíše model pro všechny revize i necommitnuté.
     * @return model obsahující všechny revize
     */
    public List<RevisionModel> getAllRevisionModels() {
        return getDatabaseService().runInTransaction(TransactionLevel.READ,
                new TransactionCallback<List<RevisionModel>>() {
            @Override
            public List<RevisionModel> callMe(TitanTransaction transaction) {
                List<RevisionModel> resultModel = new ArrayList<RevisionModel>();

                Vertex revisionRoot = getRevisionRootHandler().getRoot(transaction);
                List<Vertex> revisions = GraphOperation.getAdjacentVertices(revisionRoot, Direction.OUT,
                        RevisionRootHandler.EVERY_REVISION_INTERVAL, EdgeLabel.HAS_REVISION);
                for (Vertex rev : revisions) {
                    resultModel.add(new RevisionModel(rev));
                }

                return resultModel;
            }

            @Override
            public String getModuleName() {
                return MergerController.MODULE_NAME;
            }
        });
    }
}
