package eu.profinit.manta.dataflow.repository.merger.server.helper.usage;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.Validate;

import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.AccessLevel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorPrefix;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverser;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverserParallel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverserSerial;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;

/**
 * Helper pro spočtení statistik v repository.
 * @author tfechtner
 *
 */
public class ContentCollectorHelper {

    /** Počet paralelních vláken. */
    private int parallelThreadNumber = 4;

    /**
     * @param databaseService slu6ba pro práci s databází
     * @param superRootHandler držák na super root
     * @param terchnicalAttributes držák na technické atributy
     * @param revision číslo revize, pro které se repository analyzuje
     * @return mapa obsahující výsledky
     */
    public Map<String, Object> collectContent(final DatabaseHolder databaseService,
            final SuperRootHandler superRootHandler, final TechnicalAttributesHolder terchnicalAttributes,
            final double revision) {

        TraverserProcessorFactory processorFactory = new TraverserProcessorFactoryGeneric<TraverserProcessorPrefix>(
                TraverserProcessorPrefix.class, MergerController.MODULE_NAME, new SearchOrderDfs());

        GraphScalableTraverser traverser;
        if (parallelThreadNumber > 1) {
            traverser = new GraphScalableTraverserParallel(databaseService, processorFactory, parallelThreadNumber);
        } else {
            traverser = new GraphScalableTraverserSerial(databaseService, processorFactory);
        }
        ContentCollectorVisitor visitor = new ContentCollectorVisitor(terchnicalAttributes);
        
        RevisionInterval revisionInterval = new RevisionInterval(revision, revision);
        Object startVertexId = superRootHandler.getRoot(databaseService);
        traverser.traverse(visitor, startVertexId, AccessLevel.READ, revisionInterval);

        Map<String, Object> resultModel = new HashMap<String, Object>();
        resultModel.put("layers", visitor.getLayerTypes());
        resultModel.put("resources", visitor.getResourceTypes());
        resultModel.put("nodes", visitor.getNodeTypes());
        resultModel.put("edges", visitor.getEdgeTypes());
        resultModel.put("attributes", visitor.getAttrTypes());
        resultModel.put("revision", revision);
        return resultModel;
    }

    /**
     * @param parallelThreadNumber Počet paralelních vláken. 
     */
    public void setParallelThreadNumber(int parallelThreadNumber) {
        Validate.isTrue(parallelThreadNumber > 0, "Number of parallel threads must be greater than zero.");
        this.parallelThreadNumber = parallelThreadNumber;
    }
}
