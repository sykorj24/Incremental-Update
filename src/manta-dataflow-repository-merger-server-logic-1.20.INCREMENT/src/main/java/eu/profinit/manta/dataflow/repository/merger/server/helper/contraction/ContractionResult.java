package eu.profinit.manta.dataflow.repository.merger.server.helper.contraction;

import java.util.HashMap;
import java.util.Map;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.ProcessorResult.ActionCounter;

/**
 * Datová třída pro držení výsledků kontrakce.
 * @author tfechtner
 *
 */
public class ContractionResult {
    /** Mapa počtů kontrahovaných vrcholů klíčovaných jejich typy. */
    private final Map<VertexType, ActionCounter> vertexMap = new HashMap<>();
    /** Mapa počtů kontrahovaných hren klíčovaných jejich typy. */
    private final Map<EdgeLabel, ActionCounter> edgeMap = new HashMap<>();


    /**
     * Zaznamená kontrakci vrcholu.
     * @param vertex vrchol, který byl kontrahován
     */
    public synchronized void saveContractedVertex(Vertex vertex) {
        VertexType type = VertexType.getType(vertex);
        ActionCounter actionCounter = vertexMap.get(type);
        if (actionCounter == null) {
            actionCounter = new ActionCounter();
            vertexMap.put(type, actionCounter);
        }

        actionCounter.inc();
    }

    /**
     * Zaznamená kontrakci hrany.
     * @param edge hrana, která byla kontrahován
     */
    public synchronized void saveContractedEdge(Edge edge) {
        EdgeLabel type = EdgeLabel.parseFromDbType(edge.getLabel());
        ActionCounter actionCounter = edgeMap.get(type);
        if (actionCounter == null) {
            actionCounter = new ActionCounter();
            edgeMap.put(type, actionCounter);
        }

        actionCounter.inc();
    }

    /**
     * @return Mapa počtů kontrahovaných vrcholů klíčovaných jejich typy
     */
    public Map<VertexType, ActionCounter> getVertexMap() {
        return vertexMap;
    }

    /**
     * @return Mapa počtů kontrahovaných hren klíčovaných jejich typy
     */
    public Map<EdgeLabel, ActionCounter> getEdgeMap() {
        return edgeMap;
    }

}
