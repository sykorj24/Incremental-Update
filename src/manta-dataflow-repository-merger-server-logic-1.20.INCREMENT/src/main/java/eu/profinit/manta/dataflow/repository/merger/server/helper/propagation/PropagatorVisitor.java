package eu.profinit.manta.dataflow.repository.merger.server.helper.propagation;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Visitor provádějící propagaci hran do listů.
 * @author tfechtner
 *
 */
public class PropagatorVisitor implements GraphVisitor {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(PropagatorVisitor.class);
    /** Maximální počet hran zpropagovaných z jedné konkrétní hrany. */
    private int maximumLeafEdges;
    /** Počet převedených hran, co nebyly v listech. */
    private int transformedEdges;
    /** Počet nově vytvořených hran v listech. */
    private int newEdges;
    /** Počet hran, které nebyly zpropagovány kvůli přiliš velkému počtu listů. */
    private int errorEdges;
    /** Revize, vzhledem ke kter prohledává. */
    private RevisionInterval revisionInterval;
    /** Number of the previous revision of the revision interval
     *  (first revision before the start revision of the revision interval)
     */
    private Double previousRevisionNumber;

    /**
     * @param revisionInterval Revize, vzhledem ke které prohledává. 
     */
    public PropagatorVisitor(RevisionInterval revisionInterval, Double previousRevisionNumber) {
        this.revisionInterval = revisionInterval;
        this.previousRevisionNumber = previousRevisionNumber;
    }

    /**
     * @return počet nově vytvořených hran v listech
     */
    public int getNewEdges() {
        return newEdges;
    }

    /**
     * @return počet převedených hran, co nebyly v listech
     */
    public int getTransformedEdges() {
        return transformedEdges;
    }

    /**
     * @return Počet hran, které nebyly zpropagovány kvůli přiliš velkému počtu listů.
     */
    public int getErrorEdges() {
        return errorEdges;
    }

    /**
     * @return Maximální počet hran zpropagovaných z jedné konkrétní hrany.
     */
    public int getMaximumLeafEdges() {
        return maximumLeafEdges;
    }

    /**
     * @param maximumLeafEdges Maximální počet hran zpropagovaných z jedné konkrétní hrany.
     */
    public void setMaximumLeafEdges(int maximumLeafEdges) {
        this.maximumLeafEdges = maximumLeafEdges;
    }

    @Override
    public void visitEdge(Edge edge) {
        if (!GraphOperation.isList(edge.getVertex(Direction.OUT))
                || !GraphOperation.isList(edge.getVertex(Direction.IN))) {
            propagateEdge(edge);
        }
    }

    /**
     * Zpropaguje jednu hranu. <br>
     * Vytvoří nové hrany ze všech startovních listů do všech koncových listů původních vrcholů.
     * @param edge hrana k propagaci
     */
    private void propagateEdge(Edge edge) {
        List<Vertex> startNodes = findListNodes(edge.getVertex(Direction.OUT));
        List<Vertex> endNodes = findListNodes(edge.getVertex(Direction.IN));

        int bottomLevelEdgeCount = startNodes.size() * endNodes.size();
        if (bottomLevelEdgeCount <= maximumLeafEdges) {
            
            Double origStartRev = edge.getProperty(EdgeProperty.TRAN_START.t());
            Double origEndRev = edge.getProperty(EdgeProperty.TRAN_END.t());
            String stringLabel = edge.getLabel();
            EdgeLabel label = EdgeLabel.parseFromDbType(stringLabel);
            
            if (label == null) {
                LOGGER.error("The edge to propagation has unknown label: {}.", stringLabel);
                return;
            }
            
            if (origStartRev == null) {
                LOGGER.error("The edge has not start revision.");
                return;
            }
            
            if (origEndRev == null) {
                LOGGER.error("The edge has not end revision.");
                return;
            }
            
            double newStartRev;
            double newEndRev = origEndRev;
            if (origStartRev < revisionInterval.getStart()) {
                newStartRev = revisionInterval.getStart();
                // set end revision of the top-level, propagated edge to the previous revision 
                // of the start revision of the newly created bottom-level edges
                edge.setProperty(EdgeProperty.TRAN_END.t(), previousRevisionNumber);                
            } else {
                newStartRev = origStartRev;
                // hranu odstranit
                edge.remove();                
            }
            
            RevisionInterval newRevisionInterval = new RevisionInterval(newStartRev, newEndRev);
            
            // pro všechny startovní uzly
            for (Vertex start : startNodes) {
                // pro všechny koncové uzly
                for (Vertex end : endNodes) {                    
                    GraphCreation.createEdge(start, end, label, newRevisionInterval);
                    synchronized (this) {
                        newEdges++;
                    }
                }
            }
            synchronized (this) {
                transformedEdges++;
            }
        } else {
            synchronized (this) {
                errorEdges++;
            }
            LOGGER.warn("The edge has not been propagated, too many result edges in the bottom level ("
                    + bottomLevelEdgeCount + ").");
        }
    }

    /**
     * Najde listové uzly pro zpropagování hrany. <br>
     * Výsledek je buďto aktuální uzel (pokud je list) nebo seznam jeho listů.
     * @param actualVertex uzel, pro který jsou heldané listové uzly
     * @return seznam koncových uzlů, nikdy null
     */
    private List<Vertex> findListNodes(Vertex actualVertex) {
        // zjistit koncové uzly            
        List<Vertex> listNodes = GraphOperation.getAllLists(actualVertex, revisionInterval);
        if (listNodes.size() > 0) {
            return listNodes;
        } else {
            return Collections.singletonList(actualVertex);
        }
    }

    @Override
    public void visitLayer(Vertex layer) {
        // NOOP        
    }

    @Override
    public void visitNode(Vertex node) {
        // NOOP
    }

    @Override
    public void visitResource(Vertex resource) {
        // NOOP
    }

    @Override
    public void visitAttribute(Vertex attribute) {
        // NOOP
    }
}