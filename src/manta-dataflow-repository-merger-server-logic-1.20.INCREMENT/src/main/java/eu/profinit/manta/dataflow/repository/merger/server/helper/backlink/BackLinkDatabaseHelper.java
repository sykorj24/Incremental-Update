package eu.profinit.manta.dataflow.repository.merger.server.helper.backlink;

import java.util.List;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Třídap ro práci s dtabází v rámci heldání zpětných linků pro view.
 * @author tfechtner
 *
 */
public class BackLinkDatabaseHelper {

    /** Konfigurace pro daný běh zpětných view. */
    private final BackLinkConfiguration configuration;

    /**
     * @param configuration Konfigurace pro daný běh zpětných view.
     */
    public BackLinkDatabaseHelper(BackLinkConfiguration configuration) {
        super();
        this.configuration = configuration;
    }

    /**
     * Získá seznam sloupců view.
     * @param vertex vrchol odpovídající veiw pro který se hledají sloupce
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     * @return seznam sloupců, nikdy null
     */
    public List<Vertex> getColumns(Vertex vertex, RevisionInterval revisionInterval) {
        return GraphOperation.getAllLists(vertex, revisionInterval);
    }

    /**
     * Ověří jestli má view sloupec nějaký relevantní příchozí datový tok.
     * @param column sloupec k ověření
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return true, jestliže existuje relevantní příchozí tok
     */
    public boolean hasIncomingEdges(Vertex column, RevisionInterval revisionInterval) {
        // pro všechny vrcholy, z kterých je příchozí hrana
        List<Vertex> adjacentVertices = GraphOperation.getAdjacentVertices(column, Direction.IN, revisionInterval,
                EdgeLabel.DIRECT);
        for (Vertex adjacentVertex : adjacentVertices) {
            if (configuration.isCreateViewColumn(adjacentVertex, revisionInterval)) {
                // tok z create view se nepočítá, tak bylo view vytvořeno
                continue;
            } else if (configuration.isAliasColumn(adjacentVertex, revisionInterval)) {
                if (hasAliasIncomingEdges(adjacentVertex, column, revisionInterval)) {
                    return true;
                }
            } else {
                // všechno ostatní se počítá - jsou to inserty, updaty, merge apod
                return true;
            }
        }
        return false;
    }

    /**
     * Ověří jestli má alias sloupec nějaký relevantní příchozí datový tok.
     * @param column sloupec k ověření
     * @param revisionInterval interval revizí, pro které se metoda vyhodnocuje
     * @return true, jestliže existuje relevantní příchozí tok
     */
    private boolean hasAliasIncomingEdges(Vertex aliasColumn, Vertex sourceColumn, RevisionInterval revisionInterval) {
        // jde o alias, musíme ověřit jestli teče něco do aliasu 
        List<Vertex> adjacentVertices = GraphOperation.getAdjacentVertices(aliasColumn, Direction.IN, revisionInterval,
                EdgeLabel.DIRECT);
        for (Vertex adjacentVertex : adjacentVertices) {
            if (adjacentVertex.getId().equals(sourceColumn.getId())) {
                continue;
            } else if (configuration.isAliasColumn(adjacentVertex, revisionInterval)) {
                if (hasAliasIncomingEdges(adjacentVertex, aliasColumn, revisionInterval)) {
                    return true;
                }
            } else if (!configuration.isViewColumn(adjacentVertex, revisionInterval)) {
                // nezajímá nás view, protože odtud jsme přišli
                return true;
            }
        }

        return false;
    }
}
