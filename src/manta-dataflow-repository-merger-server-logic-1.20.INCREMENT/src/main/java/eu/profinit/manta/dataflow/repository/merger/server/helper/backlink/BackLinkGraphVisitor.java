package eu.profinit.manta.dataflow.repository.merger.server.helper.backlink;

import java.util.Collections;
import java.util.List;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.EdgeTypeTraversing;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowTraverser;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowTraverserDfs;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowTraverserFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowTraverserFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;

/**
 * Vertikální visitor zajišťující tvorbu zpětných hran pro view.
 * Hledá view a na každém sloupci spouští traverser propagující zpětné hrany.
 * @author tfechtner
 *
 */
public class BackLinkGraphVisitor implements GraphVisitor {
    /** Držák na databázi. */
    private final DatabaseHolder databaseService;
    /** Konfigurace tvorby zpětných hran. */
    private final BackLinkConfiguration configuration;
    /** Kontext pro udržování stavu práce. */
    private final BackLinkContext context;
    /** Helper pro přístup k databázi. */
    private final BackLinkDatabaseHelper databaseHelper;
    /** Továrna pro tvorbu traverseru sledující datový tok a tvořící zpětné hrany. */
    private final FlowTraverserFactory flowTraverserFactory = new FlowTraverserFactoryGeneric<FlowTraverserDfs>(
            FlowTraverserDfs.class);
    /** Počet vytvořených zpětných hran. */
    private int backLinksCount = 0;
    /** Interval revizí, ve kterých má traverser procházet. */
    private final RevisionInterval revisionInterval;

    /**
     * @param databaseService Držák na databázi.
     * @param configuration Konfigurace tvorby zpětných hran.
     * @param databaseHelper Helper pro přístup k databázi.
     * @param revisionInterval interval revizí, ve kterých má traverser procházet
     */
    public BackLinkGraphVisitor(DatabaseHolder databaseService, BackLinkConfiguration configuration,
            BackLinkDatabaseHelper databaseHelper, RevisionInterval revisionInterval) {
        this.databaseService = databaseService;
        this.configuration = configuration;
        this.databaseHelper = databaseHelper;
        this.context = new BackLinkContext();
        this.revisionInterval = revisionInterval;
    }

    @Override
    public void visitNode(Vertex vertex) {
        // je to view?
        if (configuration.isView(vertex, revisionInterval)) {
            // pro všechny sloupce daného view
            List<Vertex> columnList = databaseHelper.getColumns(vertex, revisionInterval);
            for (Vertex column : columnList) {
                if (!context.isColumnModified(column) && databaseHelper.hasIncomingEdges(column, revisionInterval)) {
                    backLinksCount += processColumn((Long) column.getId());
                }
            }
        }
    }

    /**
     * Zpracuje sloupeček view. To znamená, že nastartuje zapisovací transakci a spustí flow traverser.
     * @param startNodeId id startovního vrcholu
     * @return počet nových zpětných hran v rámci zpracování tohoto sloupce
     */
    private Integer processColumn(final Long startNodeId) {
        return databaseService.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TransactionCallback<Integer>() {
            @Override
            public Integer callMe(TitanTransaction writeTransaction) {
                BackLinkFlowVisitor visitor = new BackLinkFlowVisitor(context, configuration, revisionInterval);

                FlowTraverser flowTraverser = flowTraverserFactory.createFlowTraverser();
                flowTraverser.addVisitor(visitor);
                flowTraverser.traverse(writeTransaction, Collections.<Object> singleton(startNodeId),
                        EdgeTypeTraversing.DIRECT, Direction.IN, revisionInterval);

                return visitor.getBackLinksCount();
            }

            @Override
            public String getModuleName() {
                return MergerController.MODULE_NAME;
            }
        });
    }

    /**
     * @return Počet vytvořených zpětných hran.
     */
    public int getBackLinksCount() {
        return backLinksCount;
    }

    @Override
    public void visitLayer(Vertex layer) {
        // NOOP        
    }

    @Override
    public void visitResource(Vertex resource) {
        // NOOP        
    }

    @Override
    public void visitAttribute(Vertex attribute) {
        // NOOP
    }

    @Override
    public void visitEdge(Edge edge) {
        // NOOP
    }
}
