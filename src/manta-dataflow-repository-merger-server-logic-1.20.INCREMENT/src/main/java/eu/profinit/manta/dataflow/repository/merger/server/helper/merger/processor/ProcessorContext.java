package eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.core.model.EdgeIdentification;
import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeMapping;

/**
 * Kontext procesoru sloužící k udržování informace pro mapování mezi stavem databáze a zpracovávaných objektů.
 * @author tfechtner
 *
 */
public class ProcessorContext {
    /** Transakční objekt do databáze.*/
    private final TitanTransaction dbTransaction;
    /** Převodní mapa lokálního id uzlu na titan id odpovídajícího vertexu.*/
    private final Map<String, Object> mapNodeIdToDbId;
    /** Převodní mapa lokálního id resource na titan id odpovídajícího vertexu.*/
    private final Map<String, Object> mapResourceIdToDbId;
    /** Převodní mapa lokálního id vrstvy na její textovou reprezentaci na vstupu mergeru. */
    private final Map<String, String[]> mapLayerIdToLayer;
    /** Převodní mapa lokálního id hrany na titan id odpovídající hrany v db.*/
    private final Map<String, EdgeIdentification> mapEdgeIdToDbId;
    /** Převodní mapa lokálního id source code na source code id udržované v titanu.*/
    private final Map<String, Object> mapSourceCodeIdToDbId;
    /** Množina požadovaných souborů na nahrání. */
    private final Set<SourceCodeMapping> requestedSourceCodes = new HashSet<>();

    /** Specifická data pro konkrétní request předaná merger procesoru. */
    private final SpecificDataHolder specificDataHolder;
    /** Specifická revize pro merger procesoru. */
    private final double revision;

    /**
     * @param dbTransaction Transakční objekt do databáze.
     * @param mapNodeIdToDbId Převodní mapa lokálního id uzlu na titan id odpovídajícího vertexu.
     * @param mapResourceIdToDbId Převodní mapa lokálního id resource na titan id odpovídajícího vertexu.
     * @param mapEdgeIdToDbId Převodní mapa lokálního id hrany na titan id odpovídající hrany v db.
     * @param mapSourceCodeIdToDbId Převodní mapa lokálního id source code na source code id udržované v titanu.
     * @param specificDataHolder specifická data pro konkrétní request předaná merger procesoru
     * @param revision číslo revize, které se bude používat v daném kontextu 
     */
    public ProcessorContext(TitanTransaction dbTransaction, Map<String, Object> mapNodeIdToDbId,
            Map<String, Object> mapResourceIdToDbId,
            Map<String, String[]> mapLayerIdToLayer, Map<String, EdgeIdentification> mapEdgeIdToDbId,
            Map<String, Object> mapSourceCodeIdToDbId, SpecificDataHolder specificDataHolder, double revision) {
        super();
        this.dbTransaction = dbTransaction;
        this.mapNodeIdToDbId = mapNodeIdToDbId;
        this.mapResourceIdToDbId = mapResourceIdToDbId;
        this.mapLayerIdToLayer = mapLayerIdToLayer;
        this.mapEdgeIdToDbId = mapEdgeIdToDbId;
        this.mapSourceCodeIdToDbId = mapSourceCodeIdToDbId;
        this.specificDataHolder = specificDataHolder;
        this.revision = revision;
    }

    /**
     * @return Transakční objekt do databáze.
     */
    public TitanTransaction getDbTransaction() {
        return dbTransaction;
    }

    /**
     * @return Převodní mapa lokálního id uzlu na titan id odpovídajícího vertexu.
     */
    public Map<String, Object> getMapNodeIdToDbId() {
        return mapNodeIdToDbId;
    }

    /**
     * @return Převodní mapa lokálního id resource na titan id odpovídajícího vertexu.
     */
    public Map<String, Object> getMapResourceIdToDbId() {
        return mapResourceIdToDbId;
    }

    /**
     * @return Převodní mapa lokálního id hrany na titan id odpovídající hrany v db.
     */
    public Map<String, EdgeIdentification> getMapEdgeIdToDbId() {
        return mapEdgeIdToDbId;
    }

    /**
     * @return Převodní mapa lokálního id source code na source code id udržované v titanu.
     */
    public Map<String, Object> getMapSourceCodeIdToDbId() {
        return mapSourceCodeIdToDbId;
    }

    /**
     * @return specifická data pro konkrétní request předaná merger procesoru
     */
    public SpecificDataHolder getSpecificDataHolder() {
        return specificDataHolder;
    }

    /**
     * @return číslo revize používané v daném contextu
     */
    public double getRevision() {
        return revision;
    }

    /**
     * @param sourceCodeMapping přidat source code k požadovaným
     */
    public void addRequestedSourceCode(SourceCodeMapping sourceCodeMapping) {
        requestedSourceCodes.add(sourceCodeMapping);
    }

    /**
     * @return Množina požadovaných source code na nahrání
     */
    public Set<SourceCodeMapping> getRequestedSourceCodes() {
        return requestedSourceCodes;
    }

    public Map<String, String[]> getMapLayerIdToLayer() {
        return mapLayerIdToLayer;
    }
}
