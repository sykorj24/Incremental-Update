package eu.profinit.manta.dataflow.repository.merger.server.helper.contraction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.EdgeIdentification;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Třída implementující mergující metody používané v kontrakci a unifikaci.
 * @author tfechtner
 *
 */
public class ElementsMerger {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ElementsMerger.class);

    /**
     * Zmerguje atributy dvou hran.
     * @param sourceEdge zdrojová hrana
     * @param targetEdge cílová hrana, kam se atributy mergují
     */
    public static void mergeEdgeAttributes(Edge sourceEdge, Edge targetEdge) {
        for (String key : sourceEdge.getPropertyKeys()) {
            // property pro index mající jako hodnotu id cíle -> ignore
            if (key.equals(EdgeProperty.TARGET_ID.t())) {
                continue;
            }

            Object mergingValue = sourceEdge.getProperty(key);
            Object originalValue = targetEdge.getProperty(key);

            if (originalValue == null) {
                targetEdge.setProperty(key, mergingValue);
            } else if (!originalValue.equals(mergingValue)) {
                LOGGER.error("Different attributes for key {}: {} - {}", key, originalValue, mergingValue);
            }
        }
    }

    /**
     * Zkontrahuje dva vrcholy.
     * @param sourceVertex odsraňovaný vrchol
     * @param targetVertex cílový vrchol, do kterého se merguje
     * @param revisionInterval interval revizí, ve kterém se pracuje.
     * @param result výsledek kontrakce
     */
    public static void contractTwoVertices(Vertex sourceVertex, Vertex targetVertex,
            final RevisionInterval revisionInterval, ContractionResult result) {
        mergeTwoVertices(sourceVertex, targetVertex, revisionInterval, result, false, null);
    }

    /**
     * Zunifikuje dva vrcholy.
     * @param sourceVertex odsraňovaný vrchol
     * @param targetVertex cílový vrchol, do kterého se merguje
     * @param revisionInterval interval revizí, ve kterém se pracuje.
     * @param previousRevision  first preceding revision of the revisionInterval
     * @param result výsledek kontrakce
     */
    public static void unifyTwoVertices(Vertex sourceVertex, Vertex targetVertex,
            final RevisionInterval revisionInterval, Double previousRevision, ContractionResult result) {
        mergeTwoVertices(sourceVertex, targetVertex, revisionInterval, result, true, previousRevision);
    }

    /**
     * Provede merge dvou vrcholů.
     * @param sourceVertex odsraňovaný vrchol
     * @param targetVertex cílový vrchol, do kterého se merguje
     * @param revisionInterval interval revizí, ve kterém se pracuje.
     * @param result výsledek kontrakce
     * @param solveRevisions true, jesliže duplikovaná hrana není nutně špatně a má se zachovat
     * @param previousRevision  First preceding revision of the revisionInterval
     */
    private static void mergeTwoVertices(Vertex sourceVertex, Vertex targetVertex,
            final RevisionInterval revisionInterval, ContractionResult result, boolean solveRevisions, Double previousRevision) {
        List<Edge> targetEdgeSet = GraphOperation.getAdjacentEdges(targetVertex, Direction.BOTH, revisionInterval,
                EdgeLabel.DIRECT, EdgeLabel.FILTER);
        Map<EdgeIdentification, Edge> targetEdgeMap = new HashMap<>();
        for (Edge currentEdge : targetEdgeSet) {
            EdgeIdentification edgeId = new EdgeIdentification(currentEdge);
            targetEdgeMap.put(edgeId, currentEdge);
        }

        List<Edge> mergingEdgeSet = GraphOperation.getAdjacentEdges(sourceVertex, Direction.BOTH, revisionInterval,
                EdgeLabel.DIRECT, EdgeLabel.FILTER);
        for (Edge mergingEdge : mergingEdgeSet) {
            Vertex eqEdgeSourceVertex;
            Vertex eqEdgeTargetVertex;
            if (mergingEdge.getVertex(Direction.OUT).equals(sourceVertex)) {
                eqEdgeSourceVertex = targetVertex;
                eqEdgeTargetVertex = mergingEdge.getVertex(Direction.IN);
            } else {
                eqEdgeSourceVertex = mergingEdge.getVertex(Direction.OUT);
                eqEdgeTargetVertex = targetVertex;
            }

            EdgeIdentification mergingEdgeId = new EdgeIdentification((Long) eqEdgeSourceVertex.getId(),
                    (Long) eqEdgeTargetVertex.getId(), EdgeLabel.parseFromDbType(mergingEdge.getLabel()));

            RevisionInterval mergingEdgeInterval = RevisionUtils.getRevisionInterval(mergingEdge);

            Edge existedEdge = targetEdgeMap.get(mergingEdgeId);
            if (existedEdge == null) {
                existedEdge = GraphCreation.copyEdge(mergingEdge, eqEdgeSourceVertex, eqEdgeTargetVertex,
                        revisionInterval);
                targetEdgeMap.put(mergingEdgeId, existedEdge);
            } else {
                result.saveContractedEdge(mergingEdge);
                mergeEdgeAttributes(mergingEdge, existedEdge);
            }

            if (solveRevisions && mergingEdgeInterval.getStart() < revisionInterval.getStart()) {
                RevisionUtils.setEdgeTransactionEnd(mergingEdge, previousRevision);
            } else {
                mergingEdge.remove();
            }
        }

        result.saveContractedVertex(sourceVertex);

        Edge controlEdge = GraphOperation.getControlEdge(sourceVertex);
        RevisionInterval sourceVertexRevInterval = RevisionUtils.getRevisionInterval(controlEdge);
        if (solveRevisions && sourceVertexRevInterval.getStart() < revisionInterval.getStart()) {
            controlEdge.setProperty(EdgeProperty.TRAN_END.t(), previousRevision);
        } else {
            sourceVertex.remove();
        }
    }
}
