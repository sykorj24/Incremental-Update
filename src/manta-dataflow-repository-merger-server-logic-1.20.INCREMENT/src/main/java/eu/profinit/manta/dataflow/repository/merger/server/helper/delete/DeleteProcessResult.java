package eu.profinit.manta.dataflow.repository.merger.server.helper.delete;

import java.util.ArrayList;
import java.util.List;

import eu.profinit.manta.dataflow.repository.merger.server.helper.delete.OneDeleteResult.ResultType;

/**
 * Class representing result of the delete operation over all input vertices
 * 
 * @author jsykora
 *
 */
public class DeleteProcessResult {
    private List<OneDeleteResult> allResults = new ArrayList<>();
    private int deletedCount = 0;
    private int errorCount = 0;

    public void add(OneDeleteResult oneResult) {
        allResults.add(oneResult);
        if(ResultType.SUCCESSFUL.equals(oneResult.getResultType())) {
            deletedCount++;
        } else {
            errorCount++;
        }
    }

    @Override
    public String toString() {
        String output = "DeleteProcessResult:\n";
        for (OneDeleteResult oneResult : allResults) {
            output += oneResult.toString() + "\n"; 
        }
        output += "Summary: deleted=" + deletedCount + ", error=" + errorCount;
        return output;
    }

    public List<OneDeleteResult> getAllResults() {
        return allResults;
    }

    public void setAllResults(List<OneDeleteResult> allResults) {
        this.allResults = allResults;
    }

    public int getDeletedCount() {
        return deletedCount;
    }

    public void setDeletedCount(int deletedCount) {
        this.deletedCount = deletedCount;
    }

    public int getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(int errorCount) {
        this.errorCount = errorCount;
    }
    
    
    
}
