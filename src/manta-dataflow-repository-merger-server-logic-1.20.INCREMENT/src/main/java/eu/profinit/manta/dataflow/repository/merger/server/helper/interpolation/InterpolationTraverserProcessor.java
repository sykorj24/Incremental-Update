package eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorPrefix;

/**
 * Traverser procesor specificky pro interpolaci hran.
 * Prochazi pouze uzly v interpolovane vrstve, ostatni ignoruje. 
 * @author onouza
 */
public class InterpolationTraverserProcessor extends TraverserProcessorPrefix {
    
    @Override
    protected void processResource(Vertex vertex) {
        Vertex layer = GraphOperation.getLayer(vertex);
        if (layer != null && getVisitor().getInterpolatedLayer().equals(GraphOperation.getName(layer))) {
            super.processResource(vertex);
        }
    }

    @Override
    protected InterpolationVisitor getVisitor() {
        return (InterpolationVisitor)super.getVisitor();
    }
    
}
