package eu.profinit.manta.dataflow.repository.merger.server.helper.merger;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

/**
 * CSV reader, ktery uklada prectene radky do stopy za ucelem se k nim pozdeji vratit.
 * Ma tri stavy:
 * <ol>
 *   <li>Otevrena stopa - radek se precte ze streamu a ulozi do stopy.</li>
 *   <li>Uzavrena neprectena stopa - radek se precte ze stopy</li>
 *   <li>Uzavrena prectena stopa - radek se precte ze streamu, ale do stopy se jiz neuklada</li>
 * </ol>
 * 
 * @author ONouza
 *
 */
final class FootprintCsvReader {
    /** CSV reader, ze ktereho se primarne ctou radky */
    private final CSVReader csvReader;
    /** Stopa s prectenymi radky */
    private final List<String[]> footprint = new LinkedList<>();
    /** Priznak, zda je stopa uzavrena */
    private boolean footprintClosed;
    /** Priznak, zda je stopa prectena */
    private boolean footprintRead;
    /** Priznak, zda byl precten vstupni stream */
    private boolean streamRead;
    /** Iterator stopy */
    private Iterator<String[]> footprintIterator;

    /**
     * Konstruktor
     * @param csvReader CSV reader, ze ktereho se primarne ctou radky
     */
    FootprintCsvReader(CSVReader csvReader) {
        this.csvReader = csvReader;
    }
    
    /**
     * Precte dalsi radek. Zpusob cteni zavisi na stavu readeru - viz dokumentace tridy.
     * @return Precteny radek
     * @throws IOException Pokud nastane chyba pri cteni radku z puvodniho streamu
     */
    String[] readNext() throws IOException {
        if (footprintClosed) {
            if (footprintIterator.hasNext()) {
                return footprintIterator.next();
            } else {
                footprintRead = true;
                if (streamRead) {
                    return null;
                }
                String[] line = csvReader.readNext();
                if (line == null) {
                    streamRead = true;
                    return null;
                } else {
                    return line;
                }
            }
        } else {
            if (streamRead) {
                return null;
            }
            String[] line = csvReader.readNext();
            if (line == null) {
                streamRead = true;
                return null;
            } else {
                footprint.add(line);
                return line;
            }
        }
    }

    /**
     * Uzavre stopu.
     */
    void closeFootprint() {
        this.footprintClosed = true;
        footprintIterator = footprint.iterator();
    }

    /**
     * @return Priznak, zda je stopa prectena
     */
    boolean isFootprintRead() {
        return footprintRead;
    }
}