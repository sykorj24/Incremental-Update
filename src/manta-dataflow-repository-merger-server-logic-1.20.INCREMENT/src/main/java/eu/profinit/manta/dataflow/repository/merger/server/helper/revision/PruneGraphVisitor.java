package eu.profinit.manta.dataflow.repository.merger.server.helper.revision;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Visitor pro provedení prune operace.
 * @author tfechtner
 *
 */
public class PruneGraphVisitor extends AbstractRevisionGraphVisitor<PruneState> {

    /**
     * @param revisionInterval Interval revizí, které visitor upravuje.
     * @param processorResult Třída nesoucí informace o provedených operací.
     */
    public PruneGraphVisitor(RevisionInterval revisionInterval, ProcessorResult<PruneState> processorResult) {
        super(revisionInterval, processorResult);
    }

    @Override
    public void visitLayer(Vertex layer) {
        Edge edge = GraphOperation.getLayerControlEdge(layer);
        PruneState state = PruneState.findState(edge, getRevisionInterval());
        switch (state) {
        case NOT_CHANGE:
            // NOOP
            break;
        case DELETE:
            deleteLayer(layer);
            break;
        default:
            throw new IllegalArgumentException("Unknown rollback state " + state + ".");
        }
    }

    @Override
    public void visitResource(Vertex resource) {
        Edge edge = GraphOperation.getResourceControlEdge(resource);
        PruneState state = PruneState.findState(edge, getRevisionInterval());
        switch (state) {
        case NOT_CHANGE:
            // NOOP
            break;
        case DELETE:
            deleteResource(resource);
            break;
        default:
            throw new IllegalArgumentException("Unknown rollback state " + state + ".");
        }
    }

    @Override
    public void visitNode(Vertex node) {
        Edge edge = GraphOperation.getNodeControlEdge(node);
        PruneState state = PruneState.findState(edge, getRevisionInterval());
        switch (state) {
        case NOT_CHANGE:
            // NOOP
            break;
        case DELETE:
            deleteNode(node);
            break;
        default:
            throw new IllegalArgumentException("Unknown rollback state " + state + ".");
        }
    }

    @Override
    public void visitAttribute(Vertex attribute) {
        Edge edge = GraphOperation.getAttributeControlEdge(attribute);
        PruneState state = PruneState.findState(edge, getRevisionInterval());
        switch (state) {
        case NOT_CHANGE:
            // NOOP
            break;
        case DELETE:
            deleteAttribute(attribute);
            break;
        default:
            throw new IllegalArgumentException("Unknown rollback state " + state + ".");
        }
    }

    @Override
    public void visitEdge(Edge edge) {
        PruneState state = PruneState.findState(edge, getRevisionInterval());
        switch (state) {
        case NOT_CHANGE:
            // NOOP
            break;
        case DELETE:
            deleteEdge(edge);
            break;
        default:
            throw new IllegalArgumentException("Unknown rollback state " + state + ".");
        }
    }

    @Override
    protected void processSourceCodeEdge(SourceRootHandler sourceRootHandler, Edge edge) {
        PruneState state = PruneState.findState(edge, getRevisionInterval());
        switch (state) {
        case NOT_CHANGE:
            // NOOP
            break;
        case DELETE:
            deleteSourceCodeNode(sourceRootHandler, edge.getVertex(Direction.IN));
            break;
        default:
            throw new IllegalArgumentException("Unknown rollback state " + state + ".");
        }
    }

    @Override
    protected PruneState deleteOperationType() {
        return PruneState.DELETE;
    }
}
