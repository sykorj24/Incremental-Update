package eu.profinit.manta.dataflow.repository.merger.server.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.MergerHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.RevisionHelper;
import eu.profinit.manta.dataflow.repository.merger.server.security.MergerRoles;
import eu.profinit.manta.dataflow.repository.utils.VersionHolder;
import eu.profinit.manta.platform.automation.MantaVersion;
import eu.profinit.manta.platform.automation.licensing.LicenseException;

/**
 * Kontroler pro obhodospodaření mergování s lokálními id.
 * @author tfechtner
 *
 */
@Controller
public class MergerController {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(MergerController.class);

    /** Název merger modulu. */
    public static final String MODULE_NAME = "mr_merger";

    @Autowired
    private DatabaseHolder databaseService;

    @Autowired
    private RevisionRootHandler revisionRootHandler;

    @Autowired
    private MergerHelper mergerHelper;

    @Autowired
    private VersionHolder versionHolder;

    /**
     * Zobrazení odesílacího formuláře.
     * @return adresa ftl šablony
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/merge", method = RequestMethod.GET)
    public String printPage() {
        return "manta-dataflow-repository-merger/merge";
    }

    /**
     * Zpracování standardního merge.
     * Umí jak merge na serveru s verzováním i bez verzování.
     * @param file soubor ke zpracování
     * @param revision číslo revize, která se prochází
     * @return výsledek
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/api/merge", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> handleMerge(@RequestParam("file") final MultipartFile file,
            @RequestParam(value = "revision", required = false) final Double revision,
            @RequestParam(value = "mantaVersion", required = false) final String mantaVersionString) {

        if (revision == null && revisionRootHandler.isVersionSystemOn()) {
            return mergerHelper
                    .createErrorResponse(HttpStatus.BAD_REQUEST, RevisionHelper.ENABLED_VERSION_SYSTEM_ERROR);
        }

        if (revision != null && !revisionRootHandler.isVersionSystemOn()) {
            return mergerHelper.createErrorResponse(HttpStatus.BAD_REQUEST,
                    RevisionHelper.DISABLED_VERSION_SYSTEM_ERROR);
        }
        
        if (mantaVersionString != null) {
            MantaVersion mantaVersionClient = MantaVersion.createVersionFromText(mantaVersionString);
            if (!versionHolder.isCompatible(mantaVersionClient)) {
                return mergerHelper.createErrorResponse(HttpStatus.BAD_REQUEST,
                        versionHolder.generateError(mantaVersionClient));
            }
        } else {
            LOGGER.warn("The Manta Client version wasn't sent.");
        }

        try {
            if (revisionRootHandler.isVersionSystemOn()) {
                try {
                    return mergerHelper.executeMerge(file, revision);
                } catch (RevisionException e) {
                    return mergerHelper.createErrorResponse(HttpStatus.BAD_REQUEST,
                            "Error during merge operation. " + e.getMessage());
                }
            } else {
                // When versioning is disabled, there is only one committed revision 0.0 (technical revision)
                // and all operations are performed only within this revision
                Double latestCommittedRevision = revisionRootHandler.getLatestCommittedRevisionNumber(databaseService);
                if (latestCommittedRevision == null) {
                    LOGGER.error("The database is not correctly initialized.");
                    return mergerHelper.createErrorResponse(HttpStatus.BAD_GATEWAY,
                            "The database is not initialized correctly.");
                }
                return mergerHelper.executeMerge(file, latestCommittedRevision);
            }
        } catch (LicenseException e) {
            LOGGER.error("Insufficient license.", e);
            return mergerHelper.createErrorResponse(HttpStatus.PAYMENT_REQUIRED,
                    "Insufficient license. " + e.getMessage());
        }
    }

    /**
     * Doplňující merge, dodávající data do již commitnuté revize.
     * Musí jít o server s revizemi a musí být zapnut přepínač toto umožňující.
     * Data se přidávají do poslední commitnuté revize. Nelze přidávat, pokud již existuje nový snapshot.
     * @param file soubor obsahující data
     * @return výsledek mergu
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/api/merge-add", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> handleAdditionalMerge(@RequestParam("file") final MultipartFile file) {
        if (!revisionRootHandler.isVersionSystemOn()) {
            String errorMsg = "Additional merge is only for the server with revision control.";
            LOGGER.error(errorMsg);
            return mergerHelper.createErrorResponse(HttpStatus.BAD_REQUEST, errorMsg);
        }

        if (!revisionRootHandler.isAdditionalMergeOn()) {
            String errorMsg = "Additional merge is not allowed on this server.";
            LOGGER.error(errorMsg);
            return mergerHelper.createErrorResponse(HttpStatus.BAD_REQUEST, errorMsg);
        }

        try {
            return mergerHelper.executeAdditionalMerge(file);
        } catch (RevisionException e) {
            return mergerHelper.createErrorResponse(HttpStatus.BAD_REQUEST,
                    "Error during addtional merge operation. " + e.getMessage());
        } catch (LicenseException e) {
            LOGGER.error("Insufficient license.", e);
            return mergerHelper.createErrorResponse(HttpStatus.PAYMENT_REQUIRED,
                    "Insufficient license. " + e.getMessage());
        }

    }
}
