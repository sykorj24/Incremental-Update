package eu.profinit.manta.dataflow.repository.merger.server.helper.revision;

import com.thinkaurelius.titan.core.TitanTransaction;

/**
 * Callback pro operace prováděné v rámci controlleru pro revize.
 * @author tfechtner
 *
 * @param <T> návratová hodnota call operace
 */
public interface RevisionControllerCallback<T> {

    /**
     * Název operace používaný pro logy a zprávy.
     * @return název dané operace
     */
    String getOperationName();

    /**
     * Vnitřní logika operace.
     * @param transaction transakce pro přístup k db
     * @param revision číslo revize pro kterou se operace vykonává 
     * @return výsledek operace
     */
    T call(TitanTransaction transaction, Double revision);

    /**
     * Název property použitý pro výsledek.
     * @return název výsledku
     */
    String getResultName();
}