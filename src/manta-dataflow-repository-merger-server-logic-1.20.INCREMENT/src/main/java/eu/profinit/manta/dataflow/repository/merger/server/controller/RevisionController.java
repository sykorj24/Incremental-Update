package eu.profinit.manta.dataflow.repository.merger.server.controller;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.connector.titan.filter.model.HorizontalFilterProvider;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.ProcessorResult;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.PruneState;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.RevisionControllerCallback;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.RevisionHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.RollbackState;
import eu.profinit.manta.dataflow.repository.merger.server.security.MergerRoles;
import eu.profinit.manta.dataflow.repository.utils.VersionHolder;
import eu.profinit.manta.platform.automation.MantaVersion;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;
import eu.profinit.manta.platform.web.core.security.SecurityHelper;

/**
 * Controller pro obsluhu operací spojenými se správou verzí.
 * @author tfechtner
 *
 */
@Controller
public class RevisionController {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RevisionController.class);
    /** Název akce pro usage stats pro založení nové revize. */
    private static final String FLOW_REVISION_NEW = "flow_revision_new";
    /** Název akce pro usage stats pro commit revize. */
    private static final String FLOW_REVISION_COMMIT = "flow_revision_commit";
    /** Název akce pro usage stats pro rollback snapshot revize. */
    private static final String FLOW_REVISION_ROLLBACK = "flow_revision_rollback";
    /** Název akce pro usage stats pro prune ke konkrétní revizi. */
    private static final String FLOW_REVISION_PRUNE_TO = "flow_revision_pruneTo";
    /** Název akce pro usage stats pro prune starých revizí. */
    private static final String FLOW_REVISION_PRUNE_OLDEST = "flow_revision_pruneOldest";

    @Autowired
    private DatabaseHolder databaseService;

    @Autowired
    private SuperRootHandler superRootHandler;

    @Autowired
    private RevisionRootHandler revisionRootHandler;

    @Autowired
    private SourceRootHandler sourceRootHandler;

    @Autowired
    private RevisionHelper revisionHelper;

    @Autowired
    private UsageStatsCollector usageStatsCollector;

    @Autowired
    private VersionHolder versionHolder;

    @Autowired
    private HorizontalFilterProvider horizontalFilterProvider;

    /**
     * Create new major revision and return the newly created revision number
     * 
     * @return revision number
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/api/revision/create-major", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createMajorRevision(
            @RequestParam(value = "mantaVersion", required = false) final String mantaVersionString) {

        if (mantaVersionString != null) {
            MantaVersion mantaVersionClient = MantaVersion.createVersionFromText(mantaVersionString);
            if (!versionHolder.isCompatible(mantaVersionClient)) {
                Map<String, Object> body = Collections.<String, Object> singletonMap("error",
                        versionHolder.generateError(mantaVersionClient));
                return new ResponseEntity<Map<String, Object>>(body, HttpStatus.BAD_REQUEST);
            }
        } else {
            LOGGER.warn("The Manta Client version wasn't sent.");
        }
        
        GraphCreation.initDatabase(databaseService, superRootHandler, revisionRootHandler, sourceRootHandler);

        return revisionHelper.executeRevisionOperationAsResponse(new RevisionControllerCallback<Double>() {
            @Override
            public Double call(TitanTransaction transaction, Double revisionParam) {
                Double snapshotRevisionNumber = revisionRootHandler.createMajorRevision(transaction);
                usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REVISION_NEW,
                        Collections.singletonMap("revision", snapshotRevisionNumber));

                return snapshotRevisionNumber;
            }

            @Override
            public String getOperationName() {
                return "create new major revision";
            }

            @Override
            public String getResultName() {
                return "revision";
            }
        }, (Double) null);
    }
    
    /**
     * Create new minor revision and return the newly created revision number
     * 
     * @return revision number
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/api/revision/create-minor", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createMinorRevision(
            @RequestParam(value = "mantaVersion", required = false) final String mantaVersionString) {

        if (mantaVersionString != null) {
            MantaVersion mantaVersionClient = MantaVersion.createVersionFromText(mantaVersionString);
            if (!versionHolder.isCompatible(mantaVersionClient)) {
                Map<String, Object> body = Collections.<String, Object> singletonMap("error",
                        versionHolder.generateError(mantaVersionClient));
                return new ResponseEntity<Map<String, Object>>(body, HttpStatus.BAD_REQUEST);
            }
        } else {
            LOGGER.warn("The Manta Client version wasn't sent.");
        }
        
        GraphCreation.initDatabase(databaseService, superRootHandler, revisionRootHandler, sourceRootHandler);

        return revisionHelper.executeRevisionOperationAsResponse(new RevisionControllerCallback<Double>() {
            @Override
            public Double call(TitanTransaction transaction, Double revisionParam) {
                Double snapshotRevisionNumber = revisionRootHandler.createMinorRevision(transaction);
                usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REVISION_NEW,
                        Collections.singletonMap("revision", snapshotRevisionNumber));

                return snapshotRevisionNumber;
            }

            @Override
            public String getOperationName() {
                return "create new minor revision";
            }

            @Override
            public String getResultName() {
                return "revision";
            }
        }, (Double) null);
    }

    /**
     * Tries to fetch the latest uncommitted revision (the latest snapshot). 
     * 
     * @exception IllegalStateException  if no revision exists
     * @exception IllegalStateException  if no uncommitted revision exists
     * 
     * @return latest uncommitted revision
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/api/revision/snapshot", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> fetchSnapshotNumber() {
        return revisionHelper.executeRevisionOperationAsResponse(new RevisionControllerCallback<Double>() {
            @Override
            public Double call(TitanTransaction transaction, Double revisionParam) {
                Double latestCommittedRevision = revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                Double latestUncommittedRevision = revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                if(latestUncommittedRevision == null) {
                    if(latestCommittedRevision == null) {
                        throw new IllegalStateException("No revision exists");
                    } else {
                        throw new IllegalStateException("The latest revision has been already committed (no uncommitted revision exists).");
                    }
                } else {
                    return latestUncommittedRevision;
                }
            }

            @Override
            public String getOperationName() {
                return "fetch snapshot number";
            }

            @Override
            public String getResultName() {
                return "revision";
            }
        }, (Double) null);
    }

    /**
     * Commit specified revision. In case the commit was successful, 
     * the revision number of the committed revision is included in the returned result.
     * 
     * @param revision  number of the revision to be committed
     * @return result of the commit process
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/api/revision/commit/{revision:.+}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> commitRevision(@PathVariable Double revision) {
        ResponseEntity<Map<String, Object>> response = revisionHelper.executeRevisionOperationAsResponse(new RevisionControllerCallback<Double>() {
            @Override
            public Double call(TitanTransaction transaction, Double revisionParam) {
                revisionRootHandler.commitRevision(transaction, revisionParam);
                usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REVISION_COMMIT,
                        Collections.singletonMap("revision", revisionParam));
                horizontalFilterProvider.refreshFilters();
                return revisionParam;
            }

            @Override
            public String getOperationName() {
                return "commit revision";
            }

            @Override
            public String getResultName() {
                return "revision";
            }
        }, revision);

        // přinucení zapsání dat z cache v paměti, díky čemuž se databáze nerozbije při násilném vypnutí
        databaseService.closeDatabase();

        return response;
    }

    /**
     * Rollback verze.
     * 
     * @param revision číslo revize, která se má rollbacknout
     * @return výsledek zpracování
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/api/revision/rollback/{revision:.+}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> rollbackRevision(@PathVariable Double revision) {
        ProcessorResult<RollbackState> result = revisionHelper.rollbackRevision(revision);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("revision", revision);
        params.put("result", result);
        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REVISION_ROLLBACK, params);

        return new ResponseEntity<Map<String, Object>>(params, HttpStatus.OK);
    }

    /**
     * Rollback aktuálního snapshotu.
     * 
     * @return výsledek zpracování
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/api/revision/rollback-current-revision", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> rollbackCurrentRevision() {
        ProcessorResult<RollbackState> result;
        Double revision = revisionRootHandler.getLatestRevisionNumber(databaseService);
        try {
            result = revisionHelper.rollbackRevision(revision);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<Map<String, Object>>(
                    Collections.<String, Object> singletonMap("error", e.getMessage()), HttpStatus.BAD_REQUEST);
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("revision", revision);
        params.put("result", result);
        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REVISION_ROLLBACK, params);

        return new ResponseEntity<Map<String, Object>>(params, HttpStatus.OK);
    }

    /**
     * Zobrazí seznam všech revizí. 
     * 
     * @param model  spring model
     * @return název view
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/revision/prune", method = RequestMethod.GET)
    public String pruneView(Model model) {
        model.addAttribute("revisions", revisionHelper.getAllRevisionModels());
        return "manta-dataflow-repository-merger/prune";
    }

    /**
     * Prořezat staré verze až do dané revize včetně.
     * 
     * @param revision  číslo revize, až do které se mají smazat
     * @param model  spring model
     * @return výsledek zpracování
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/revision/prune", method = RequestMethod.POST)
    public String pruneToRevision(@RequestParam Double revision, Model model) {
        ProcessorResult<PruneState> result = revisionHelper.pruneToRevision(revision);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pruneToRevision", revision);
        params.put("result", result);
        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REVISION_PRUNE_TO, params);

        model.addAllAttributes(params);
        model.addAttribute("revisions", revisionHelper.getAllRevisionModels());
        return "manta-dataflow-repository-merger/prune";
    }

    /**
     * Prořezat staré verze, aby zbyl jen daný počet commitlých revizí.
     * 
     * @param revisionCount  počet revizí, které se mají zachovat
     * @return výsledek zpracování
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/api/revision/prune-preserve", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> pruneOldestRevisions(@RequestParam Integer revisionCount) {

        ProcessorResult<PruneState> result = revisionHelper.pruneOldestRevisions(revisionCount);

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("preservedRevisionCount", revisionCount);
        parameters.put("latestCommittedRevision", revisionRootHandler.getLatestCommittedRevisionNumber(databaseService));
        parameters.put("result", result);
        usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REVISION_PRUNE_OLDEST, parameters);

        return new ResponseEntity<Map<String, Object>>(parameters, HttpStatus.OK);
    }

    /**
     * MEtoda pro odchycení RevisionException výjimek.
     * @param exception odchytávaná výjimka
     * @return response s textací chyby
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(RevisionException.class)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> revisionException(RevisionException exception) {
        Map<String, Object> params = Collections.<String, Object> singletonMap("error", exception.getMessage());
        return new ResponseEntity<Map<String, Object>>(params, HttpStatus.BAD_REQUEST);
    }

}
