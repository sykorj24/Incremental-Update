package eu.profinit.manta.dataflow.repository.merger.server.helper.delete;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.thinkaurelius.titan.util.system.IOUtils;
import com.tinkerpop.blueprints.Vertex;

import au.com.bytecode.opencsv.CSVReader;
import cern.colt.Arrays;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionModel;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MaintenanceController;
import eu.profinit.manta.dataflow.repository.merger.server.helper.delete.OneDeleteResult.ResultType;
import eu.profinit.manta.dataflow.repository.utils.CsvHelper;

/**
 * OneFileDeleter is responsible for deleting all target vertices specified in a file. 
 * 
 * Each line in the file represents path to a target vertex to be deleted.
 * Basic line format: vertexName/vertexName/.../vertexName/targetVertexName
 * Optional line format: vertexName/vertexName/.../vertexName/targetVertexName,vertexType/vertexType/.../vertexType/targetVertexType
 * 
 * Vertex can have more children with the same name. In that case, use secondary information about its type because
 * one vertex cannot have two child vertices with the same name and with the same type.
 * 
 * When specifying both names and types, comma ',' is used as a delimiter between these two paths (top level).
 * When specifying name items or type items, forward slash '/' is used as a delimiter (nested level).
 * 
 * Use double quotation mark - " - when wrapping items to avoid problems with special characters.
 * For example: "res/'$ourceName"/databaseName/schemaName
 * 
 * Use escape char '\' when applying quotation marks in a nested level.
 * For example: "\"res/'$ourceName\"/databaseName/"schemaName,"resourceType/databaseType/schemaType"
 * 
 * When using types, it is possible to use a special character '*' (asterisk) representing any type. 
 * This may be used when user does not want to specify types of all vertices, but only types of some vertices. 
 * For example: resourceName/databaseName/schemaName/tableName,"*"/"*"/"*"/tableType 
 * -> this way, user could specify only type of the last object (i.e. of the object to be deleted)
 * 
 * @author jsykora
 *
 */
public class OneFileDeleter {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(OneFileDeleter.class);
    /** Service for the access to the database*/
    private final DatabaseHolder databaseService;
    /** Super root handler for the access to the main repository tree */
    private final SuperRootHandler superRootHandler;
    /** Revision root handler for the access to the revision tree */
    private final RevisionRootHandler revisionRootHandler;
    /** Source root handler for the access to the source code tree */
    private final SourceRootHandler sourceRootHandler;

    /** Delimiter used in name paths and type paths */
    private final char PATH_DELIMITER = '/';
    /** Escape character used in name paths and type paths */
    private final char PATH_ESCAPE_CHAR = CsvHelper.ESCAPE_CHAR;
    /** Quote character used in name paths and type paths */
    private final char PATH_QUOTE_CHAR = CsvHelper.QUOTE_CHAR;
    /** Character representing any type */
    private final char ANY_TYPE = '*';

    /** List of all lines in the original form */
    private List<String> lines = new ArrayList<>();
    /** List of all paths using vertex names */
    private List<String[]> namesPathList = new ArrayList<String[]>();
    /** List of all paths using vertex types */
    private List<String[]> typesPathList = new ArrayList<String[]>();

    public OneFileDeleter(DatabaseHolder databaseService, SuperRootHandler superRootHandler,
            RevisionRootHandler revisionRootHandler, SourceRootHandler sourceRootHandler) {
        super();
        this.databaseService = databaseService;
        this.superRootHandler = superRootHandler;
        this.revisionRootHandler = revisionRootHandler;
        this.sourceRootHandler = sourceRootHandler;
    }

    /**
     * Process input file with vertices to be deleted.
     * 
     * First, read file to check format. File has to be in correct format, otherwise no vertex is deleted.
     * Second, all vertices read from the input file are deleted.
     * 
     * @param inputStream
     * @param revision  revision number within which the delete is performed
     * @return
     */
    public Map<String, Object> process(InputStream inputStream, Double revision) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        TransactionLevel level = TransactionLevel.WRITE_EXCLUSIVE;
        DeleteProcessResult deleteProcessResult = new DeleteProcessResult();
        
        try {
            readFile(inputStream);
        } catch (IOException e) {
            LOGGER.error("Error when reading file.", e);
            resultMap.put("error", "Error when reading input file.");
            resultMap.put("processingReport", "Error when reading input file.");
            return resultMap;
        }

        for (int i = 0; i < namesPathList.size(); i++) {
            String[] names = namesPathList.get(i);
            String[] types = typesPathList.get(i);

            // search for the target vertex specified by its path (name/type) and delete it
            final ProcessPathTransactionCallback pathProcessor = new ProcessPathTransactionCallback(lines.get(i), revision, names,
                    types);
            OneDeleteResult oneDeleteResult = (OneDeleteResult) databaseService.runInTransaction(level, pathProcessor);
            deleteProcessResult.add(oneDeleteResult);
        }

        resultMap.put("processingReport", deleteProcessResult);
        return resultMap;
    }

    /**
     * Read file line by line and save names path and types path from each line.
     * 
     * @param inputStream  input stream
     * @throws IOException
     */
    private void readFile(InputStream inputStream) throws IOException {
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new InputStreamReader(inputStream));
            // line reader uses comma as a delimiter
            CSVReader csvLineReader = CsvHelper.getCSVReader(reader);
            String[] line;

            // read file line by line
            while ((line = csvLineReader.readNext()) != null) {
                CSVReader csvNamePathReader = null;
                CSVReader csvTypePathReader = null;
                String[] names = null;
                String[] types = null;

                // get either path of names or both path of names and path of types
                if (line.length == 1) {
                    String namesPath = line[0];
                    csvNamePathReader = CsvHelper.getCSVReader(new StringReader(namesPath), PATH_DELIMITER,
                            PATH_QUOTE_CHAR, PATH_ESCAPE_CHAR);
                    names = csvNamePathReader.readNext();
                } else if (line.length == 2) {
                    String namesPath = line[0];
                    String typesPath = line[1];
                    csvNamePathReader = CsvHelper.getCSVReader(new StringReader(namesPath), PATH_DELIMITER,
                            PATH_QUOTE_CHAR, PATH_ESCAPE_CHAR);
                    csvTypePathReader = CsvHelper.getCSVReader(new StringReader(typesPath), PATH_DELIMITER,
                            PATH_QUOTE_CHAR, PATH_ESCAPE_CHAR);
                    names = csvNamePathReader.readNext();
                    types = csvTypePathReader.readNext();

                    if (names.length != types.length) {
                        throw new IOException(
                                "Error in line: " + line + "\n" + "Number of items in name path (" + names.length
                                        + ") is not equal to the number of items in type path (" + types.length + ")");
                    }
                } else {
                    throw new IOException("Error in line: " + line + "\n"
                            + "Line has to be in format: path/to/the/vertex/by/names[,corresponding/path/with/vertex/types]");
                }
                
                // save line in the original form (as one String)
                lines.add(Arrays.toString(line));

                // save single items from line for further processing
                namesPathList.add(names);
                typesPathList.add(types);
            }
        } catch (IOException e) {
            throw e;
        } finally {
            IOUtils.closeQuietly(reader);
        }
    }

    /**
     * Delete one vertex (and its subtree structure).
     * 
     * First, target vertex has to be found. This is done by traversing from super root down to the target vertex
     * following its direct parent nodes specified by name (or name+type). 
     * 
     * 
     * @author jsykora
     *
     */
    private final class ProcessPathTransactionCallback implements TransactionCallback<Object> {
        private String line;
        private final Double revision;
        private final String[] names;
        private final String[] types;

        public ProcessPathTransactionCallback(String line, Double revision, String[] names, String[] types) {
            super();
            this.line = line;
            this.revision = revision;
            this.names = names;
            this.types = types;
        }

        @Override
        public String getModuleName() {
            return MaintenanceController.MODULE_NAME;
        }

        @Override
        public OneDeleteResult callMe(TitanTransaction transaction) {
            long deleteTimeStart = System.currentTimeMillis();
            Vertex parentVertex = superRootHandler.getRoot(transaction);
            String parentName = "superRoot";
            Vertex childVertex = null;
            RevisionInterval revisionInterval = new RevisionInterval(revision, revision);

            for (int i = 0; i < names.length; i++) {
                String childName = names[i];
                List<Vertex> children = GraphOperation.getChildrenWithName(parentVertex, childName, revisionInterval);
                try {
                    childVertex = getChildVertex(children, parentName, childName, i);
                } catch (FindVertexException e) {
                    LOGGER.error("Did not delete target vertex. ", e.getMessage());
                    return new OneDeleteResult(line, ResultType.ERROR, e.getMessage());
                }
                parentName = childName;
                parentVertex = childVertex;
            }

            // last child vertex is the target vertex to be deleted
            Vertex targetVertex = childVertex;

            if (targetVertex == null) {
                LOGGER.error("Did not delete target. No vertex specified.");
                return new OneDeleteResult(line, ResultType.ERROR, "No vertex specified");
            }

            // delete vertex by setting its whole subtree transaction end to the previous revision
            RevisionModel revisionModel = revisionRootHandler.getSpecificModel(transaction, revision);
            Double newEndRevision = revisionModel.getPreviousRevision();
            GraphOperation.setSubtreeTransactionEnd(targetVertex, revision, newEndRevision);

            long deleteTime = System.currentTimeMillis() - deleteTimeStart;
            LOGGER.info("Delete of an object {} performed in {} ms", names[names.length - 1], deleteTime);
            return new OneDeleteResult(line, ResultType.SUCCESSFUL);
        }

        /**
         * 
         * @param children
         * @param parentName
         * @param childName
         * @param pathIndex  position in the path starting from first vertex (from root)
         * @return
         * @throws FindVertexException
         */
        private Vertex getChildVertex(final List<Vertex> children, final String parentName, final String childName,
                final int pathIndex) throws FindVertexException {
            Vertex vertex = null;
            String vertexType = null;

            if (children.size() == 0) {
                throw new FindVertexException("Vertex " + parentName + " does not have a child with name " + childName
                        + " in revision " + revision);
            } else if (children.size() == 1) {
                if (types == null || types[pathIndex].equals(ANY_TYPE)) {
                    // there is one and only one child with the correct name
                    return children.get(0);
                } else {
                    // check, whether specified type matches
                    vertex = children.get(0);

                    if (pathIndex == 0) {
                        // first vertex is always resource -> check for resourceType
                        vertexType = vertex.getProperty(NodeProperty.RESOURCE_TYPE.t());
                    } else {
                        // all other vertices are nodes -> check for nodeType
                        vertexType = vertex.getProperty(NodeProperty.NODE_TYPE.t());
                    }

                    if (types[pathIndex].equals(vertexType)) {
                        // there is one and only one child with the correct name and type
                        return children.get(0);
                    } else {
                        throw new FindVertexException("Vertex " + parentName + " does not have a child with name "
                                + childName + " and type " + vertexType + " in revision " + revision);
                    }
                }
            } else if (children.size() > 1) {
                if (types == null || types[pathIndex].equals(ANY_TYPE)) {
                    throw new FindVertexException("Vertex" + parentName + " has more than one child (" + children.size()
                            + ") with name " + childName + " in revision " + revision
                            + ". Hint: specify vertex type to distinguish vertices with the same name");
                } else {
                    // try to find vertex with the specified type
                    for (Vertex child : children) {
                        if (pathIndex == 0) {
                            // first vertex is always resource -> check for resourceType
                            vertexType = child.getProperty(NodeProperty.RESOURCE_TYPE.t());
                        } else {
                            // all other vertices are nodes -> check for nodeType
                            vertexType = child.getProperty(NodeProperty.NODE_TYPE.t());
                        }
                        if (types[pathIndex].equals(vertexType)) {
                            // there is one and only one child with the correct name and type
                            return child;
                        }
                    }
                    throw new FindVertexException("Vertex " + parentName + " does not have a child with name "
                            + childName + " and type " + vertexType + " in revision " + revision);
                }
            }

            // we should never get here
            throw new FindVertexException("Unknown error. Could not get a child vertex with name " + childName
                    + " from parent vertex " + parentName);
        }

    }

    public class FindVertexException extends Exception {
        private static final long serialVersionUID = -33614453478208788L;

        public FindVertexException(String message) {
            super(message);
        }

    }
}
