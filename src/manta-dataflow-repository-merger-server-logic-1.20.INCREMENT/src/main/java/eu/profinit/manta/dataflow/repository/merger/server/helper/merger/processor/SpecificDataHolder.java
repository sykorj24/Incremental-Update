package eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor;

/**
 * Marker pro označení datové třídy předáváná merger procesoru a obsahující specifická data
 * pro zpracování daných objektů.
 * @author tfechtner
 *
 */
public interface SpecificDataHolder {

}
