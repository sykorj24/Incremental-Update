package eu.profinit.manta.dataflow.repository.merger.server.helper.contraction;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.EdgeIdentification;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Visitor provádějící kontrakci vrcholů po optimistickém mergi.
 * @author tfechtner
 *
 */
public class ContractionVisitor implements GraphVisitor {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ContractionVisitor.class);

    /** Interval revizí, na které se pracuje. */
    private final RevisionInterval revisionInterval;
    /** Výsledek zpracování. */
    private final ContractionResult result = new ContractionResult();
    /** Množina id vrcholů, jejichž alespoň jeden potomek nebo vrchol vrstvy byl zkontrahován. */
    private final Set<Long> withContractedChildrensOrLayer = Collections.synchronizedSet(new HashSet<Long>());
    /** Množina hran k mergování. */
    private final Set<EdgesToContract> edgesToMerge = Collections.synchronizedSet(new HashSet<EdgesToContract>());
    /** Množina id zpracovaných vrcholů pro zajištění, že každý bude navštíven nejvýše jednou. */
    private final Set<Long> rememberedVertices = new HashSet<Long>();
    /** Mapa id odstraňovaných vrcholů na ekvivalentní. */
    private final Map<Long, Long> contractionPairs = Collections.synchronizedMap(new HashMap<Long, Long>());

    /**
     * Zapamatuje si zpracovaný vrchol.
     * @param vertex vrchol k zapamatování
     * @return true, jestliže vrchol ještě nebyl zpracován
     */
    public synchronized boolean rememberVertex(Vertex vertex) {
        return rememberedVertices.add((Long) vertex.getId());
    }

    /**
     * Zjistí, zda daný vrchol byl poznačen k odstranění.
     * @param vertex zkoumaný vrchol
     * @return true, jestliže byl vrchol poznačen k odstranění
     */
    public boolean isRemoved(Vertex vertex) {
        return contractionPairs.containsKey(vertex.getId());
    }

    /**
     * @param revisionInterval  Interval revizí, na které se pracuje
     */
    public ContractionVisitor(RevisionInterval revisionInterval) {
        super();
        this.revisionInterval = revisionInterval;
    }

    @Override
    public void visitLayer(Vertex layer) {
        // NOOP
    }

    @Override
    public void visitResource(Vertex resource) {
        resolveSurroundings(resource);
        if (resolveLayers(resource)) {
            // poznačit, že tento vrchol má zkontrahovanou vrstvu 
            withContractedChildrensOrLayer.add((Long) resource.getId());
        }
    }

    @Override
    public void visitNode(Vertex node) {
        resolveSurroundings(node);
    }

    @Override
    public void visitAttribute(Vertex attribute) {
        // NOOP
    }

    @Override
    public void visitEdge(Edge edge) {
        // NOOP
    }

    /**
     * Ověři jestli alespoň jeden potomek daného vrcholu byl zkotnrahovan.
     * @param vertex zkoumaný vrchol
     * @return true, jestliže potomci daného vrcholu byly zkontrahováni
     */
    public boolean isVertexContractedChildren(Vertex vertex) {
        return withContractedChildrensOrLayer.contains(vertex.getId());
    }

    /**
     * @return výsledek zpracování
     */
    public ContractionResult getResult() {
        return result;
    }

    /**
     * @return množina hran k mergování
     */
    public Set<EdgesToContract> getEdgesToMerge() {
        return edgesToMerge;
    }

    /** 
     * @return mapa id odstraňovaných vrcholů na ekvivalentní 
     */
    public Map<Long, Long> getVerticesToMerge() {
        return contractionPairs;
    }

    /**
     * Vyřeší komplet okolí pro daný vrchol.
     * @param vertex vrchol, pro který okolí ověřuje
     */
    private void resolveSurroundings(Vertex vertex) {
        if (isRemoved(vertex)) {
            return;
        }

        if (resolveChildren(vertex)) {
            // poznačit, že tento vrchol má zkontrahovaného potomka 
            withContractedChildrensOrLayer.add((Long) vertex.getId());
        }
        resolveEdges(vertex);
        resolveAttributes(vertex);
    }

    /**
     * Vyřeší potomky pro daný vrchol.
     * @param vertex vrchol, pro který se potomci ověřují
     * @return true, jestliže alespoň jeden potomek byl zkontrahován
     */
    private boolean resolveChildren(Vertex vertex) {
        List<Vertex> children = GraphOperation.getDirectChildren(vertex, revisionInterval);
        return contractVertices(children);
    }

    /**
     * Vyresi vrstvy, ve kterych lezi dany vrchol 
     * @param vertex Zkoumany vrchol
     * @return {@code true}, pokud alespon jedna vrstva byla zkontrahovana, jinak {@code false}. 
     */
    private boolean resolveLayers(Vertex vertex) {
        List<Vertex> layers = GraphOperation.getAdjacentVertices(vertex, Direction.OUT, revisionInterval,
                EdgeLabel.IN_LAYER);
        return contractVertices(layers);
    }
    
    private boolean contractVertices(List<Vertex> vertices) {
        boolean isSomethingContracted = false;
        Map<VertexIdentification, Vertex> resolvedVertices = new HashMap<>();
        for (Vertex vertex : vertices) {
            VertexIdentification childId = new VertexIdentification(vertex);
            Vertex resolvedVertex = resolvedVertices.get(childId);
            if (resolvedVertex == null) {
                resolvedVertices.put(childId, vertex);
            } else if (resolvedVertex != vertex) {
                contractTwoNodes(vertex, resolvedVertex);
                isSomethingContracted = true;
            }
        }
        return isSomethingContracted;
    }

    /**
     * Vyřeší hrany pro daný vrchol.
     * @param vertex vrchol, pro který se hrany ověřují
     */
    private void resolveEdges(Vertex vertex) {
        Map<EdgeIdentification, Edge> savedEdgeSet = new HashMap<>();
        List<Edge> allEdges = GraphOperation.getAdjacentEdges(vertex, Direction.OUT, revisionInterval, EdgeLabel.DIRECT,
                EdgeLabel.FILTER);
        allEdges.addAll(GraphOperation.getAdjacentEdges(vertex, Direction.OUT, revisionInterval, EdgeLabel.HAS_PARENT,
                EdgeLabel.HAS_RESOURCE, EdgeLabel.HAS_ATTRIBUTE));
        for (Edge currentEdge : allEdges) {
            EdgeIdentification edgeId = new EdgeIdentification(currentEdge);
            Edge savedEdge = savedEdgeSet.get(edgeId);
            if (savedEdge == null) {
                savedEdgeSet.put(edgeId, currentEdge);
            } else {
                contractTwoEdges(currentEdge, savedEdge);
            }
        }
    }

    /**
     * Vyřeší atributy pro daný vrchol.
     * @param vertex vrchol, pro který se atributy ověřují 
     */
    private void resolveAttributes(Vertex vertex) {
        Map<String, Set<Object>> existedAttributes = new HashMap<>();
        List<Vertex> attributes = GraphOperation.getAdjacentVertices(vertex, Direction.OUT, revisionInterval,
                EdgeLabel.HAS_ATTRIBUTE);
        Set<Long> checkedAttrs = new HashSet<>();

        for (Vertex attr : attributes) {
            if (!checkedAttrs.add((Long) attr.getId())) {
                // tento atribut uz jsme kontrolovali = vedou dve hrany do stejneho vrcholu
                continue;
            }

            String key = GraphOperation.getName(attr);
            Object value = attr.getProperty(NodeProperty.ATTRIBUTE_VALUE.t());

            Set<Object> existedValues = existedAttributes.get(key);
            if (existedValues == null) {
                existedValues = new HashSet<>();
                existedValues.add(value);
                existedAttributes.put(key, existedValues);
            } else {
                if (!existedValues.add(value)) {
                    result.saveContractedVertex(attr);
                    attr.remove();
                }
            }
        }
    }

    /**
     * Zkontrahuje dva vrcholy.
     * @param mergingVertex mergovaný vrchol, který zmizí
     * @param targetVertex originální vrchol
     */
    protected void contractTwoNodes(Vertex mergingVertex, Vertex targetVertex) {
        if (targetVertex.equals(mergingVertex)) {
            LOGGER.error("Two same vertices cannot be contracted {}.", targetVertex.getId());
            return;
        }

        List<Edge> childrenEdges = GraphOperation.getAdjacentEdges(mergingVertex, Direction.IN, revisionInterval,
                EdgeLabel.HAS_PARENT, EdgeLabel.HAS_RESOURCE);
        copyEdges(targetVertex, childrenEdges, Direction.IN);

        List<Edge> attrEdges = GraphOperation.getAdjacentEdges(mergingVertex, Direction.OUT, revisionInterval,
                EdgeLabel.HAS_ATTRIBUTE);
        copyEdges(targetVertex, attrEdges, Direction.OUT);
        
        contractionPairs.put((Long) mergingVertex.getId(), (Long) targetVertex.getId());
    }

    /**
     * Zkontrahuje dvě hrany.
     * @param sourceEdge mergovaná hrana, která zmizí
     * @param targetEdge originální hrana
     */
    protected void contractTwoEdges(Edge sourceEdge, Edge targetEdge) {
        edgesToMerge.add(new EdgesToContract(new EdgeIdentification(sourceEdge), new EdgeIdentification(targetEdge),
                revisionInterval));
    }

    /**
     * Zkopíruje hrany do daného vrcholu.
     * @param targetVertex vrchol, kam se kopírují hrany
     * @param edges hrany ke kopírování
     * @param direction směr hran vůči vrcholu, ze kterého se kopíruje
     */
    protected final void copyEdges(Vertex targetVertex, List<Edge> edges, Direction direction) {
        for (Edge edge : edges) {
            Vertex otherEnd = edge.getVertex(direction.opposite());
            if (direction == Direction.IN) {
                GraphCreation.copyEdge(edge, otherEnd, targetVertex, getRevisionInterval());
            } else {
                GraphCreation.copyEdge(edge, targetVertex, otherEnd, getRevisionInterval());
            }
            edge.remove();
        }
    }

    /**
     * @return Interval revizí, na které se pracuje
     */
    public RevisionInterval getRevisionInterval() {
        return revisionInterval;
    }

}
