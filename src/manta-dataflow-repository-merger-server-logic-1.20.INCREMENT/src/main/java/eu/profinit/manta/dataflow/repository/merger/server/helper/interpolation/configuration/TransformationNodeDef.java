package eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.transformation.TransformationMapping;

/**
 * Definice transformacniho uzlu pro INTERPOLOVANOU vrstvu.
 * Zapouzdruje typ uzlu a puvod nazvu uzlu.
 * Pozice v hierarchii se zde neresi - ta se definuje mimo. 
 * 
 * @author onouza
 */
public class TransformationNodeDef {

    /**
     * Typ uzlu.
     */
    private String type;
    /**
     * Puvod nazvu uzlu.
     */
    private NodeNameOrigin nameOrigin;
    
    /**
     * Vrati nazev uzlu na zaklade uzlu zdrojoveho, ciloveho a transformacniho.
     * 
     * @param source Zdrojovy uzel
     * @param target Cilovy uzel
     * @param transformation Transformacni uzel (z MAPOVANE vrstvy)
     * @param transformationMapping Transformacni mapovani. Obsahuje informaci o mapovani nove vytvareneho transformacniho uzlu
     *          v INTERPOLOVANE vrstve na odpovidajici existujici transformacni uzel v MAPOVANE vrstve.
     *          Muze byt i {@code null} - v takovem pripade se vlastnosti uzlu vytvareneho v INTERPOLOVANE vrstve preberou z uzlu v MAPOVANE vrstve.
     * @return Nazev uzlu na zaklade uzlu zdrojoveho, ciloveho a transformacniho.
     */
    public String getNodeName(Vertex source, Vertex target, Vertex transformation, TransformationMapping transformationMapping) {
        if (nameOrigin == null) {
            throw new IllegalStateException("'nameOrigin' must be set");
        }
        return nameOrigin.getNodeName(source, target, transformation, transformationMapping);
    }
    
    // gettery / settery
    
    /**
     * @return Typ uzlu.
     */
    public String getType() {
        return type;
    }

    /**
     * @param type Typ uzlu.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return Puvod nazvu uzlu.
     */
    public NodeNameOrigin getNameOrigin() {
        return nameOrigin;
    }

    /**
     * @param nameOrigin Puvod nazvu uzlu.
     */
    public void setNameOrigin(NodeNameOrigin nameOrigin) {
        this.nameOrigin = nameOrigin;
    }

}
