package eu.profinit.manta.dataflow.repository.merger.server.helper.delete;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.terracotta.modules.ehcache.async.exceptions.ProcessingException;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionLockedOperation;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MaintenanceController;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;
import eu.profinit.manta.dataflow.repository.merger.server.helper.AbstractControllerHelper;

/**
 * Helper for deleting objects specified by their paths.
 * 
 * @author jsykora
 *
 */
public class DeleteHelper extends AbstractControllerHelper {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteHelper.class);
    
    /**
     * Execute delete of objects.
     * 
     * @param file  file containing objects to be deleted
     * @param revision  number of revision within which the deletion is performed
     * @return  result of the delete operation
     */
    public ResponseEntity<Map<String, Object>> executeDelete(final MultipartFile file, final Double revision) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        long startTime = System.currentTimeMillis();
        final OneFileDeleter oneFileDeleter = new OneFileDeleter(getDatabaseService(), getSuperRootHandler(), getRevisionRootHandler(), getSourceRootHandler());

        try {
            if (getRevisionRootHandler().isVersionSystemOn()) {
                resultMap.putAll(executeDeleteWithVcs(file, revision, oneFileDeleter));
            } else {
                resultMap.putAll(executeDeleteWithoutVcs(file, revision, oneFileDeleter));
            }
        } catch (DeleteHelperException e) {
            LOGGER.error("Error during reading input file.", e);
            return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Error during reading input file.");
        }

        long proccessingTime = System.currentTimeMillis() - startTime;
        resultMap.put("proccessingTime", proccessingTime);
        return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
    }
    
    /**
     * Delete objects from a specified revision.
     *       
     * @param file
     * @param revision
     * @param oneFileDeleter
     * @return
     */
    protected  Map<String, Object> executeDeleteWithVcs(final MultipartFile file, final Double revision, final OneFileDeleter oneFileDeleter) {
        return getRevisionRootHandler()
                .executeRevisionReadOperation(new RevisionLockedOperation<Map<String, Object>>() {
                    @Override
                    public Map<String, Object> call() {
                        TransactionLevel level = TransactionLevel.WRITE_EXCLUSIVE;
                        getDatabaseService().runInTransaction(level, new TransactionCallback<Object>() {
                            @Override
                            public Object callMe(final TitanTransaction transaction) {
                                getRevisionRootHandler().checkDeleteConditions(transaction, revision);
                                return null;
                            }

                            @Override
                            public String getModuleName() {
                                return MaintenanceController.MODULE_NAME;
                            }
                        });

                        try {
                            return oneFileDeleter.process(file.getInputStream(), revision);
                        } catch (IOException e) {
                            throw new DeleteHelperException(e);
                        }
                    }
                });
    }
    
    /**
     * Erase objects from the repository (permanently)
     * 
     * @param file
     * @param oneFileDeleter
     * @return
     */
    protected  Map<String, Object> executeDeleteWithoutVcs(final MultipartFile file, final Double revision, final OneFileDeleter oneFileDeleter) {
        try {
            oneFileDeleter.process(file.getInputStream(), revision);
            throw new IOException();
        } catch(IOException e) {
            throw new DeleteHelperException(e);
        }
    }
    
    private static class DeleteHelperException extends RuntimeException {
        private static final long serialVersionUID = 6790283956377822512L;

        DeleteHelperException(Throwable cause) {
            super(cause);
        }
    }
}

