package eu.profinit.manta.dataflow.repository.merger.server.helper.usage;

import java.util.Map;
import java.util.Set;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.TechnicalAttributesHolder;
import eu.profinit.manta.dataflow.repository.utils.NodeTypeDef;
import eu.profinit.manta.dataflow.repository.utils.OccuranceCountHolder;

/**
 * Visitor počítající statistiky v repository.
 * @author tfechtner
 *
 */
public class ContentCollectorVisitor implements GraphVisitor {
    private final OccuranceCountHolder<String> layerTypes = new OccuranceCountHolder<>();
    /** Počty typů resource v repository. */
    private final OccuranceCountHolder<String> resourceTypes = new OccuranceCountHolder<>();
    /** Počty typů uzlů v repository. */
    private final OccuranceCountHolder<NodeTypeDef> nodeTypes = new OccuranceCountHolder<>();
    /** Počty typů hran v repository. */
    private final OccuranceCountHolder<EdgeLabel> edgeTypes = new OccuranceCountHolder<>();
    /** Počty atributů v repository. */
    private final OccuranceCountHolder<AttributeType> attrTypes = new OccuranceCountHolder<>();
    /** Služba pro zjištení, jestli jde o technický parametr. */
    private final TechnicalAttributesHolder terchnicalAttributes;

    /**
     * @param terchnicalAttributes Služba pro zjištení, jestli jde o technický parametr.
     */
    public ContentCollectorVisitor(TechnicalAttributesHolder terchnicalAttributes) {
        super();
        this.terchnicalAttributes = terchnicalAttributes;
    }

    @Override
    public void visitLayer(Vertex layer) {
        String type = GraphOperation.getType(layer);
        layerTypes.addOccurance(type);
    }

    @Override
    public void visitResource(Vertex resource) {
        String type = GraphOperation.getType(resource);
        resourceTypes.addOccurance(type);
    }

    @Override
    public void visitNode(Vertex node) {
        String type = GraphOperation.getType(node);
        String resource = GraphOperation.getType(GraphOperation.getResource(node));
        nodeTypes.addOccurance(new NodeTypeDef(type, resource));
    }

    @Override
    public void visitAttribute(Vertex attribute) {
        attrTypes.addOccurance(AttributeType.VERTEX);
    }

    @Override
    public void visitEdge(Edge edge) {
        EdgeLabel edgeLabel = EdgeLabel.parseFromDbType(edge.getLabel());
        edgeTypes.addOccurance(edgeLabel);

        Set<String> properties = edge.getPropertyKeys();
        for (String key : properties) {
            if (!terchnicalAttributes.isEdgeAttributeTechnical(key)) {
                attrTypes.addOccurance(AttributeType.EDGE);
            }
        }
    }

    /**
     * @return Počty typů vrstev v repository. 
     */
    public Map<String, Integer> getLayerTypes() {
        return layerTypes.getCounts();
    }

    /**
     * @return Počty typů resource v repository. 
     */
    public Map<String, Integer> getResourceTypes() {
        return resourceTypes.getCounts();
    }

    /**
     * @return Počty typů uzlů v repository. 
     */
    public Map<NodeTypeDef, Integer> getNodeTypes() {
        return nodeTypes.getCounts();
    }

    /**
     * @return Počty typů hran v repository. 
     */
    public Map<EdgeLabel, Integer> getEdgeTypes() {
        return edgeTypes.getCounts();
    }

    /**
     * @return Počty atributů v repository.
     */
    public Map<AttributeType, Integer> getAttrTypes() {
        return attrTypes.getCounts();
    }

    /**
     * Typy atributů.
     * @author tfechtner
     *
     */
    public enum AttributeType {
        /** Atribut na hraně. */
        EDGE,
        /** Atribut na vrcholu. */
        VERTEX;
    }
}
