package eu.profinit.manta.dataflow.repository.merger.server.helper.contraction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionLockedOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.collection.CollectionParallelProcessor;
import eu.profinit.manta.dataflow.repository.connector.titan.service.collection.CollectionProcessor;
import eu.profinit.manta.dataflow.repository.connector.titan.service.collection.CollectionProcessorCallback;
import eu.profinit.manta.dataflow.repository.connector.titan.service.collection.CollectionSerialProcessor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.VisitedPart;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.AccessLevel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorPrefix;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverser;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverserParallel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverserSerial;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;
import eu.profinit.manta.platform.web.core.security.SecurityHelper;

/**
 * Helper pro kontrakci vrcholů po optimistickém mergi.
 * @author tfechtner
 *
 */
public class ContractionHelper {
    /** Název akce pro usage stats pro kontrakci uzlů. */
    private static final String FLOW_REPOSITORY_CONTRACTION = "flow_repository_contraction";
    /** Název akce pro usage stats pro unifikaci uzlů. */
    private static final String FLOW_REPOSITORY_UNIFICATION = "flow_repository_unification";
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ContractionHelper.class);

    /** Držák na databázi. */
    @Autowired
    private DatabaseHolder databaseService;
    /** Držák na super root. */
    @Autowired
    private SuperRootHandler superRootHandler;
    /** Držák na root revizí. */
    @Autowired
    private RevisionRootHandler revisionRootHandler;
    /** True, jestliže je merger paralelní. */
    private boolean isMergerParallel = true;
    /** Počet paralelních vláken. */
    private int parallelThreadNumber = 4;

    /**
     * Provede kontrakci vrcholů v aktuální necommitnuté revizi.
     * 
     * @param usageStatsCollector  sběrač usage stats
     * @return výsledek kontrakce
     */
    public ResponseEntity<Object> performContraction(final UsageStatsCollector usageStatsCollector) {
        if (!isMergerParallel) {
            return new ResponseEntity<Object>("Merger isn't parallel - nothing to contract.", HttpStatus.OK);
        }

        return revisionRootHandler.executeRevisionWriteOperation(new RevisionLockedOperation<ResponseEntity<Object>>() {

            @Override
            public ResponseEntity<Object> call() {
                Double latestUncommittedRevision = databaseService.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE,
                        new TransactionCallback<Double>() {
                            @Override
                            public String getModuleName() {
                                return MergerController.MODULE_NAME;
                            }

                            @Override
                            public Double callMe(TitanTransaction transaction) {
                                return revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                            }
                        });

                Double latestCommittedRevision = databaseService.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE,
                        new TransactionCallback<Double>() {
                            @Override
                            public String getModuleName() {
                                return MergerController.MODULE_NAME;
                            }

                            @Override
                            public Double callMe(TitanTransaction transaction) {
                                return revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                            }
                        });

                if (latestUncommittedRevision == null) {
                    if (latestCommittedRevision == null) {
                        return new ResponseEntity<Object>("No revision exists in the database.",
                                HttpStatus.BAD_REQUEST);
                    } else {
                        return new ResponseEntity<Object>("Contraction can be done only in an uncommited version.",
                                HttpStatus.BAD_REQUEST);
                    }
                }

                ContractionResult result = performContractionInner(latestUncommittedRevision);
                usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REPOSITORY_CONTRACTION, result);
                return new ResponseEntity<Object>(result, HttpStatus.OK);
            }

        });
    }

    /**
     * Provede kontrakci pro danou revizi.
     * 
     * Revision interval used during contraction explained:
     * Nodes and edges may be copied during contraction. Contraction is performed within a specified revision.
     * When a node or an edge is copied, their new revision interval validity is a result of intersection of 
     * their original revision interval and the specified contraction revision. This would always lead to an
     * unwanted removal of the copied object (result of the intersection). Therefore, it is necessary to
     * extend the contraction revision up to the maximum revision number. Since contraction is performed only 
     * during the latest uncommitted revision, there is no risk of inconsistency (latest uncommitted revision 
     * represents always the highest revision number).
     * 
     * @param revision revize, který se řeší
     * @return výsledek kontrakce s počty kontrahovaných objektů
     */
    protected ContractionResult performContractionInner(Double revision) {
        List<VisitedPart> visitedParts = new ArrayList<>();
        visitedParts.add(VisitedPart.SELF);
        visitedParts.add(VisitedPart.SUCCESSORS);

        TraverserProcessorFactory processorFactory = new TraverserProcessorFactoryGeneric<>(
                ContractionTraverserProcessor.class, MergerController.MODULE_NAME,
                TraverserProcessorFactoryGeneric.DEFAULT_OBJECTS_PER_TRAN, new SearchOrderDfs(), visitedParts);

        GraphScalableTraverser traverser = new GraphScalableTraverserParallel(databaseService, processorFactory,
                parallelThreadNumber);

        Double endRevision = RevisionUtils.getMaxMinorRevisionNumber(revision);
        // set revision interval to: <latestUncommittedRevision, latestUncommittedRevisionMajor.9999999>
        // for the explanation see the method description
        RevisionInterval revisionInterval = new RevisionInterval(revision, endRevision);

        ContractionVisitor visitor = new ContractionVisitor(revisionInterval);
        Long rootId = superRootHandler.getRoot(databaseService);
        long time = System.currentTimeMillis();
        traverser.traverse(visitor, rootId, AccessLevel.WRITE_SHARE, revisionInterval);
        LOGGER.info("Contraction traversing in {} ms.", System.currentTimeMillis() - time);

        ContractionResult result = visitor.getResult();

        time = System.currentTimeMillis();
        mergeEdges(visitor.getEdgesToMerge(), result);
        LOGGER.info("Deleted {} edges in {} ms.", visitor.getEdgesToMerge().size(), System.currentTimeMillis() - time);

        time = System.currentTimeMillis();
        mergeVertices(revisionInterval, visitor.getVerticesToMerge(), result);
        LOGGER.info("Contracted {} vertices in {} ms.", visitor.getVerticesToMerge().size(),
                System.currentTimeMillis() - time);

        return result;
    }

    /**
     * Provede unifikaci vrcholů v aktuální necommitnuté revizi.
     * 
     * @param usageStatsCollector sběrač usage stats
     * @param resourceName název resource, kde se operace provádí
     * @param types množina převáděných typů, kde klíče je zdrojový typ a hodnota cílový typ
     * @return výsledek kontrakce
     */
    public ResponseEntity<Object> performUnification(final UsageStatsCollector usageStatsCollector,
            final String resourceName, final List<Map<String, String>> sources, final List<Map<String, String>> targets,
            final boolean caseInsensitive, final boolean forceMergeSources, final boolean forceMergeTargets) {
        return revisionRootHandler.executeRevisionWriteOperation(new RevisionLockedOperation<ResponseEntity<Object>>() {

            @Override
            public ResponseEntity<Object> call() {
                Double latestUncommittedRevision = databaseService.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE,
                        new TransactionCallback<Double>() {
                            @Override
                            public String getModuleName() {
                                return MergerController.MODULE_NAME;
                            }

                            @Override
                            public Double callMe(TitanTransaction transaction) {
                                return revisionRootHandler.getLatestUncommittedRevisionNumber(transaction);
                            }
                        });

                Double latestCommittedRevision = databaseService.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE,
                        new TransactionCallback<Double>() {
                            @Override
                            public String getModuleName() {
                                return MergerController.MODULE_NAME;
                            }

                            @Override
                            public Double callMe(TitanTransaction transaction) {
                                return revisionRootHandler.getLatestCommittedRevisionNumber(transaction);
                            }
                        });

                if (latestUncommittedRevision == null) {
                    if (latestCommittedRevision == null) {
                        return new ResponseEntity<Object>("No revision exists in the database.",
                                HttpStatus.BAD_REQUEST);
                    } else {
                        return new ResponseEntity<Object>("Contraction can be done only in an uncommited version.",
                                HttpStatus.BAD_REQUEST);
                    }
                }

                long timeStart = System.currentTimeMillis();
                ContractionResult result = performUnificationInner(latestUncommittedRevision, resourceName, sources,
                        targets, caseInsensitive, forceMergeSources, forceMergeTargets);
                long timeSpent = System.currentTimeMillis() - timeStart;
                LOGGER.info("Unification for {} was done in {} ms.", resourceName, timeSpent);

                Map<String, Object> params = new HashMap<>();
                params.put("resourceName", resourceName);
                params.put("sources", sources);
                params.put("targets", targets);
                params.put("caseInsensitive", caseInsensitive);
                params.put("forceMergeSources", forceMergeSources);
                params.put("forceMergeTargets", forceMergeTargets);
                params.put("result", result);
                params.put("time", timeSpent);
                usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REPOSITORY_UNIFICATION, params);

                return new ResponseEntity<Object>(result, HttpStatus.OK);
            }

        });
    }

    /**
     * Provede samotnou unifikaci.
     * @param revision číslo revize, ve které se pracuje
     * @param resourceName jméno resource, kde se provádí unifikace
     * @param types množina převáděných typů, kde klíče je zdrojový typ a hodnota cílový typ
     * @return výsledek unifikace
     */
    protected UnificationResult performUnificationInner(Double revision, String resourceName,
            List<Map<String, String>> sources, List<Map<String, String>> targets, boolean caseInsensitive,
            boolean forceMergeSources, boolean forceMergeTargets) {
        List<VisitedPart> visitedParts = new ArrayList<>();
        visitedParts.add(VisitedPart.SELF);
        visitedParts.add(VisitedPart.SUCCESSORS);

        TraverserProcessorFactory processorFactory = new TraverserProcessorFactoryGeneric<>(
                TraverserProcessorPrefix.class, MergerController.MODULE_NAME,
                TraverserProcessorFactoryGeneric.DEFAULT_OBJECTS_PER_TRAN, new SearchOrderDfs(), visitedParts);

        GraphScalableTraverserSerial traverser = new GraphScalableTraverserSerial(databaseService, processorFactory);

        Double endRevision = RevisionUtils.getMaxMinorRevisionNumber(revision);
        // set revision interval to: <latestUncommittedRevision, latestUncommittedRevisionMajor.9999999>
        // for the explanation see the method description of performContractionInner (same logic)
        RevisionInterval revisionInterval = new RevisionInterval(revision, endRevision);
        Double previousRevision = revisionRootHandler.getPreviousRevisionNumber(databaseService,
                revisionInterval.getStart());

        UnificationVisitor visitor = new UnificationVisitor(revisionInterval, previousRevision, sources, targets,
                caseInsensitive, forceMergeSources, forceMergeTargets);

        Long resourceId = getResourceId(resourceName, revisionInterval);
        if (resourceId == null) {
            LOGGER.error("The resource with name {} doesn't exist.", resourceName);
            return visitor.getResult();
        }
        traverser.traverse(visitor, resourceId, AccessLevel.WRITE, revisionInterval);
        return visitor.getResult();
    }

    /**
     * Nalezne id pro daný resource.
     * @param resourceName hledaný resource
     * @param revisionInterval interval revizí, na kterém se pracuje
     * @return db id daného resource, nebo null při neexistenci
     */
    private Long getResourceId(final String resourceName, final RevisionInterval revisionInterval) {
        return databaseService.runInTransaction(TransactionLevel.READ, new TransactionCallback<Long>() {
            @Override
            public String getModuleName() {
                return MergerController.MODULE_NAME;
            }

            @Override
            public Long callMe(TitanTransaction transaction) {
                Vertex superRoot = superRootHandler.getRoot(transaction);
                List<Vertex> children = GraphOperation.getChildrenWithName(superRoot, resourceName, revisionInterval);

                if (children.size() == 0) {
                    return null;
                } else if (children.size() == 1) {
                    return (Long) children.get(0).getId();
                } else {
                    LOGGER.warn("There are more resources ({}) with the name {}.", children.size(), resourceName);
                    return (Long) children.get(0).getId();
                }
            }
        });
    }

    /**
     * Zmerguje dvojice vrcholů.
     * @param revisionInterval interval revizí, ve kterém se pracuje.
     * @param contractionMap mapa dvojic ke kontrakci, klíče jsou k odstranění, hodnoty jsou zústávající vrcholy.
     * @param result výsledek kontrakce
     */
    private void mergeVertices(final RevisionInterval revisionInterval, final Map<Long, Long> contractionMap,
            final ContractionResult result) {

        CollectionProcessor processor = new CollectionSerialProcessor();
        CollectionProcessorCallback<Entry<Long, Long>, Object> callback = new CollectionProcessorCallback<Map.Entry<Long, Long>, Object>() {

            @Override
            public void processElement(TitanTransaction transaction, Entry<Long, Long> pairToContract,
                    Object resultHolder) {
                Vertex removingVertex = transaction.getVertex(pairToContract.getKey());
                if (removingVertex == null) {
                    LOGGER.info("Vertex to remove from contraction pair doesn't exist.", pairToContract.getKey());
                    return;
                }
                Vertex targetVertex = transaction.getVertex(pairToContract.getValue());
                if (targetVertex == null) {
                    LOGGER.error("Target vertex from contraction pair doesn't exist.", pairToContract.getValue());
                    return;
                }

                ElementsMerger.contractTwoVertices(removingVertex, targetVertex, revisionInterval, result);
            }
        };

        processor.processCollection(databaseService, TransactionLevel.WRITE_SHARE, contractionMap.entrySet(), callback,
                new Object());
    }

    /**
     * Provede merge hran.
     * @param edgesToMergeSet množina dvojic hran k mergování
     * @param result 
     */
    private void mergeEdges(final Set<EdgesToContract> edgesToMergeSet, final ContractionResult result) {
        CollectionParallelProcessor processor = new CollectionParallelProcessor();
        processor.setThreadNumber(parallelThreadNumber);

        CollectionProcessorCallback<EdgesToContract, Object> callback = new CollectionProcessorCallback<EdgesToContract, Object>() {

            @Override
            public void processElement(TitanTransaction titanTransaction, EdgesToContract element,
                    Object resultHolder) {
                element.merge(titanTransaction, result);
            }
        };

        processor.processCollection(databaseService, TransactionLevel.WRITE_SHARE, edgesToMergeSet, callback,
                new Object());
    }

    /**
     * @param databaseService držák na databázi
     */
    public void setDatabaseService(DatabaseHolder databaseService) {
        this.databaseService = databaseService;
    }

    /**
     * @param superRootHandler držák na super root
     */
    public void setSuperRootHandler(SuperRootHandler superRootHandler) {
        this.superRootHandler = superRootHandler;
    }

    /**
     * @param revisionRootHandler držák na revision root
     */
    public void setRevisionRootHandler(RevisionRootHandler revisionRootHandler) {
        this.revisionRootHandler = revisionRootHandler;
    }

    /**
     * @param isMergerParallel True, jestliže je merger paralelní. 
     */
    public void setIsMergerParallel(boolean isMergerParallel) {
        this.isMergerParallel = isMergerParallel;
    }

    /**
     * @param parallelThreadNumber Počet paralelních vláken. 
     */
    public void setParallelThreadNumber(int parallelThreadNumber) {
        Validate.isTrue(parallelThreadNumber > 0, "Number of parallel threads must be greater than zero.");
        this.parallelThreadNumber = parallelThreadNumber;
    }

}
