package eu.profinit.manta.dataflow.repository.merger.server.helper.contraction;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorPrefix;

/**
 * Upravený traverser, který navštěvuje i super root. 
 * Super root je navštíven stejnou metodou jako resource.
 * @author tfechtner
 *
 */
public class TraverserProcessorPrefixWithRoot extends TraverserProcessorPrefix {

    @Override
    protected void processSuperRoot(Vertex superRoot) {
        getVisitor().visitResource(superRoot);
        super.processSuperRoot(superRoot);
    }

}
