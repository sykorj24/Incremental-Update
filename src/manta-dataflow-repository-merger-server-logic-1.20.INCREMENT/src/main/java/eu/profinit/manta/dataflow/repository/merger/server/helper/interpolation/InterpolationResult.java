package eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation;

import java.util.HashMap;
import java.util.Map;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.merger.server.helper.revision.ProcessorResult.ActionCounter;

/**
 * Datova trida pro drzeni vysledku interpolace hran
 *
 * @author onouza
 */
public class InterpolationResult {

    /** Mapa poctu interpolovanych hran klicovanych jejich typy. */
    private final Map<EdgeLabel, ActionCounter> interpolatedEdgeMap = new HashMap<>();
    /** Mapa poctu preskocenych hran klicovanych jejich typy. */
    private final Map<EdgeLabel, ActionCounter> skippedEdgeMap = new HashMap<>();
    /** Mapa poctu pridanych transformacnich uzlu klicovanych jejich typy */
    private final Map<VertexType, ActionCounter> addedTransformationVertexMap = new HashMap<>();
    /** Mapa poctu preskocenych transformacnich uzlu klicovanych jejich typy */
    private final Map<VertexType, ActionCounter> skippedTransformationVertexMap = new HashMap<>();
    /** Mapa poctu chyb pri pokusu o pridani transformacnich uzlu klicovanych jejich typy */
    private final Map<VertexType, ActionCounter> errorTransformationVertexMap = new HashMap<>();

    /**
     * Zaznamena intrpolaci hrany.
     * @param edge hrana, ktera byla interpolovana
     */
    public synchronized void saveInterpolatedEdge(Edge edge) {
        saveEdge(edge, interpolatedEdgeMap);
    }

    /**
     * Zaznamena preskoceni hrany.
     * @param edge hrana, ktera byla preskocena
     */
    public synchronized void saveSkippedEdge(Edge edge) {
        saveEdge(edge, skippedEdgeMap);
    }
    
    /**
     * Zaznamena pridani transformacniho uzlu.
     * @param vertex Uzel, ktery byl pridan.
     */
    public synchronized void saveAddedTransformationVertex(Vertex vertex) {
        saveVertex(vertex, addedTransformationVertexMap);
    }
    
    /**
     * Zaznamena preskoceni transformacniho uzlu.
     * @param vertex Uzel, ktery byl preskocen.
     */
    public synchronized void saveSkippedTransformationVertex(Vertex vertex) {
        saveVertex(vertex, skippedTransformationVertexMap);
    }
    
    /**
     * Zaznamena chybu pri pokusu o pridani transformacniho uzlu,
     * @param vertexType Typ uzlu, ktery se nepodarilo pridat.
     */
    public synchronized void saveErrorTransformationVertex(VertexType vertexType) {
        saveVertex(vertexType, errorTransformationVertexMap);
    }
    
    // pomocne privatni metody
    
    /**
     * Zaznamena uzel.
     * @param vertex Uzel, ktery se ma zaznamenat.
     * @param targetMap Mapa poctu uzlu klicovanych na jejich typy.
     */
    private void saveVertex(Vertex vertex, Map<VertexType, ActionCounter> targetMap) {
        VertexType vertexType = VertexType.getType(vertex);
        saveVertex(vertexType, targetMap);
    }

    /**
     * Zaznamena uzel.
     * @param vertexType Typ uzlu, ktery se ma zaznamenat.
     * @param targetMap Mapa poctu uzlu klicovanych na jejich typy.
     */
    private void saveVertex(VertexType vertexType, Map<VertexType, ActionCounter> targetMap) {
        ActionCounter actionCounter = targetMap.get(vertexType);
        if (actionCounter == null) {
            actionCounter = new ActionCounter();
            targetMap.put(vertexType, actionCounter);
        }
        actionCounter.inc();
    }

    /**
     * Zaznamena hranu.
     * @param edge Hrana, ktera se ma zaznamenat
     * @param targetMap Mapa poctu hran klicovanych na jejich typy
     */
    private void saveEdge(Edge edge, Map<EdgeLabel, ActionCounter> targetMap) {
        EdgeLabel type = EdgeLabel.parseFromDbType(edge.getLabel());
        ActionCounter actionCounter = targetMap.get(type);
        if (actionCounter == null) {
            actionCounter = new ActionCounter();
            targetMap.put(type, actionCounter);
        }

        actionCounter.inc();
    }

    // gettery
    
    /**
     * @return Mapa poctu interpolovanych hran klicovanych jejich typy.
     */
    public Map<EdgeLabel, ActionCounter> getInterpolatedEdgeMap() {
        return interpolatedEdgeMap;
    }

    /**
     * @return Mapa poctu preskocenych hran klicovanych jejich typy.
     */
    public Map<EdgeLabel, ActionCounter> getSkippedEdgeMap() {
        return skippedEdgeMap;
    }

    /**
     * @return Mapa poctu pridanych transformacnich uzlu klicovanych jejich typy
     */
    public Map<VertexType, ActionCounter> getAddedTransformationVertexMap() {
        return addedTransformationVertexMap;
    }

    /**
     * @return Mapa poctu preskocenych transformacnich uzlu klicovanych jejich typy
     */
    public Map<VertexType, ActionCounter> getSkippedTransformationVertexMap() {
        return skippedTransformationVertexMap;
    }

    /**
     * @return Mapa poctu chyb pri pokusu o pridani transformacnich uzlu klicovanych jejich typy
     */
    public Map<VertexType, ActionCounter> getErrorTransformationVertexMap() {
        return errorTransformationVertexMap;
    }

}
