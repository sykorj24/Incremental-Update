package eu.profinit.manta.dataflow.repository.merger.server.helper.contraction;

import java.text.Collator;
import java.util.Comparator;

/**
 * Obsahuje instanci komparátoru porovnávající String case insensitive s ohledem na defaultní locale.
 * @author lhermann
 *
 */
public final class CaseInsensitiveComparator {

    private CaseInsensitiveComparator() {
    }

    /**
     * Komparátor porovnávající String case insensitive s ohledem na defaultní locale.
     */
    public static final Comparator<? super String> INSTANCE;

    static {
        Collator collator = Collator.getInstance();
        collator.setStrength(Collator.SECONDARY);
        collator.setDecomposition(Collator.CANONICAL_DECOMPOSITION);
        INSTANCE = collator;
    }
}
