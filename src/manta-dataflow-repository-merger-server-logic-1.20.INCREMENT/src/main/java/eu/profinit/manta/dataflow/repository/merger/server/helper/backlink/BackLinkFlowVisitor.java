package eu.profinit.manta.dataflow.repository.merger.server.helper.backlink;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterResult;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowItem;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.flow.FlowVisitor;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Horizontální visitor zajišťující tvorbu zpětných hran pro view.
 * @author tfechtner
 *
 */
public class BackLinkFlowVisitor implements FlowVisitor {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(BackLinkFlowVisitor.class);
    /** Kontext pro udržování stavu v rámci práce. */
    private final BackLinkContext context;
    /** Konfigurace pro tvorbu zpětných linků. */
    private final BackLinkConfiguration configuration;
    /** Počet vytvořených zpětných hran. */
    private int backLinksCount = 0;
    /** Interval revizí, pro které visitor pracuje. */
    private final RevisionInterval revisionInterval;

    /**
     * @param context Kontext pro udržování stavu v rámci práce. 
     * @param configuration Konfigurace pro tvorbu zpětných linků.
     * @param revisionInterval Interval revizí, pro které visitor pracuje.
     */
    public BackLinkFlowVisitor(BackLinkContext context, BackLinkConfiguration configuration,
            RevisionInterval revisionInterval) {
        super();
        this.context = context;
        this.configuration = configuration;
        this.revisionInterval = revisionInterval;
    }

    @Override
    public FilterResult visitAndContinue(Vertex node, FlowItem flowItem) {
        Vertex incomingVertex = null;
        if (flowItem.getIncomingEdge() != null) {
            incomingVertex = flowItem.getIncomingEdge().getVertex(Direction.IN);
        }

        if (configuration.isViewColumn(node, revisionInterval)) {
            // ještě nedošlo k doplnění hran
            if (!context.isColumnModified(node)) {
                // hranu vytvořímě jen když to není start traverseru
                if (incomingVertex != null) {
                    // zdroj nesmí být alias, protože s aliasem už je view obousměrně spojené 
                    if (!configuration.isAliasColumn(incomingVertex, revisionInterval)) {
                        createBackLink(flowItem);
                    }
                    context.setColumnAsModified(node);
                }
                return FilterResult.OK_CONTINUE;
            } else {
                return FilterResult.NOK_STOP;
            }
        } else if (configuration.isSourceColumn(node, revisionInterval)) {
            // alias s tabulkou už je obousměrně spojené
            if (!configuration.isAliasColumn(incomingVertex, revisionInterval)) {
                createBackLink(flowItem);
            }
            return FilterResult.NOK_STOP;
        } else if (configuration.isCreateViewColumn(node, revisionInterval)) {
            createBackLink(flowItem);
            return FilterResult.OK_CONTINUE;
        } else if (configuration.isAliasColumn(node, revisionInterval)) {
            // alias s view už je obousměrně spojené
            if (!configuration.isViewColumn(incomingVertex, revisionInterval)
                    && !configuration.isAliasColumn(incomingVertex, revisionInterval)) {
                createBackLink(flowItem);
            }
            return FilterResult.OK_CONTINUE;
        } else {
            return FilterResult.NOK_STOP;
        }
    }

    /**
     * Vytvoří zpětnou hranu.
     * @param flowItem příchozí objekt v rámci traversu, pro který se vytváří zpětná hrana
     */
    private void createBackLink(FlowItem flowItem) {
        Vertex flowSourceVertex = flowItem.getIncomingEdge().getVertex(Direction.IN);
        Vertex flowTargetVertex = flowItem.getIncomingEdge().getVertex(Direction.OUT);
        GraphCreation.createEdge(flowSourceVertex, flowTargetVertex, EdgeLabel.DIRECT, revisionInterval.getEnd());

        LOGGER.debug("Creating backlink from {}({}) into {}({}).", GraphOperation.getName(flowSourceVertex),
                GraphOperation.getName(GraphOperation.getParent(flowSourceVertex)),
                GraphOperation.getName(flowTargetVertex),
                GraphOperation.getName(GraphOperation.getParent(flowTargetVertex)));

        backLinksCount++;
    }

    /**
     * @return Počet vytvořených zpětných hran.
     */
    public Integer getBackLinksCount() {
        return backLinksCount;
    }

}
