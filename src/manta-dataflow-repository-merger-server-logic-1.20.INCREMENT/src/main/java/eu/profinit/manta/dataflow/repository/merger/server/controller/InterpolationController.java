package eu.profinit.manta.dataflow.repository.merger.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.InterpolationHelper;
import eu.profinit.manta.dataflow.repository.merger.server.security.MergerRoles;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;

/**
 * Kontroler zajistujici interpolaci hran
 * 
 * @author onouza
 */
@Controller
public class InterpolationController {
    
    public static final String MODULE_NAME = "mr_interpolation";

    @Autowired
    private InterpolationHelper interpolationHelper;
    @Autowired
    private UsageStatsCollector usageStatsCollector;

    /**
     * Zpracuje pozadavek na interpolaci hran v dane vrstve.
     * @param interpolatedLayer Nazev vrstvy, ve ktere se ma provest interpolace hran. 
     * @param mappedLayer Nazev vrstvy, vuci ktere se interpolace provadi.
     * @return Odpoved na pozadavek.
     */
    @Secured(MergerRoles.MERGER)
    @RequestMapping(value = "/api/interpolation/{interpolatedLayer}/to/{mappedLayer}", method = RequestMethod.GET)
    public ResponseEntity<Object> interpolate(@PathVariable("interpolatedLayer") String interpolatedLayer, @PathVariable("mappedLayer") String mappedLayer) {
        return interpolationHelper.performInterpolation(interpolatedLayer, mappedLayer, usageStatsCollector);
    }
    
}
