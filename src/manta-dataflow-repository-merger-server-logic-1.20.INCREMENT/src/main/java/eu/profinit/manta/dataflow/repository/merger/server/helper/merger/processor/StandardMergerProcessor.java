package eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinkaurelius.titan.core.TitanVertex;
import com.thinkaurelius.titan.core.TitanVertexQuery;
import com.thinkaurelius.titan.core.attribute.Cmp;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.AttributeFormat;
import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.EdgeFormat;
import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.Flag;
import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.ItemTypes;
import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.LayerFormat;
import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.NodeFormat;
import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.ResourceFormat;
import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.SourceCodeFormat;
import eu.profinit.manta.dataflow.repository.core.model.EdgeIdentification;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeMapping;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.ProcessingResult.ResultType;
import eu.profinit.manta.dataflow.repository.utils.Base64AttributeCodingHelper;
import eu.profinit.manta.dataflow.repository.utils.CsvHelper;

/**
 * Inner merger processor running within a single transaction. Full update and incremental update follow the same merging
 * process. The only exception is in the node flag - incremental update uses flags on nodes for additional operations during 
 * the merge process.
 * <p>
 * If a node has flag REMOVE, then the whole subtree, starting from this specially marked node, is removed. That means,
 * all vertices and edges in this subtree (including data flows) have their tranEnd property set to the latest committed revision number. 
 * After the subgraph removal is completed, merging continues from this specially marked node.
 * <p>
 * If the equivalent object of the merged object does NOT exist in the latest committed revision, then the object is created
 * in the new revision. Start revision (tranStart) is the new revision. End revision (tranEnd) is the maximum minor revision
 * of the new revision (major.999999).
 * 
 * Example of full update (latestCommittedRevision = 1.012345, newMajorRevision=2.000000)
 *
 *    (Before merge)        merge into           (After merge)
 *      <N/A, N/A>       revision 2.000000    <2.000000, 2.999999>    
 *     non existing           ------>              mergedObject
 * 
 * Example of incremental update (latestCommittedRevision = 1.012345, newMinorRevision=1.012346)
 *
 *    (Before merge)        merge into           (After merge)
 *      <N/A, N/A>       revision 1.012346    <1.012346, 1.999999>    
 *     non existing           ------>              mergedObject
 * 
 * <p>
 * If the equivalent object of the merged object exists in the latest committed revision, then its end revision is set to the 
 * maximum minor revision of the new revision (major.999999). Additionally, subtree removal may be performed during incremental
 * update. 
 * 
 * Example of full update (latestCommittedRevision = 1.012345, newMajorRevision=2.000000)
 *
 *    (Before merge)         merge into           (After merge)
 * <1.011111, 1.999999>   revision 2.000000    <1.011111, 2.999999>    
 *      mergedObject           ------>             mergedObject
 *     
 * Example of incremental update (latestCommittedRevision = 1.012345, newMinorRevision=1.012346)
 *
 *    (Before merge)         merge into           (After merge)
 * <1.011111, 1.999999>   revision 1.012346    <1.011111, 1.999999>    
 *      mergedObject          ------>              mergedObject
 *      
 * Example of incremental update + subtree removal (latestCommittedRevision = 1.012345, newMinorRevision=1.012346)
 *
 *    (Before merge)       subtree    (removed in subtree)       merge into          (After merge)
 * <1.011111, 1.999999>    removal    <1.011111, 1.012345>   revision 1.012346   <1.011111, 1.999999>
 *      mergedObject       ------>        mergedObject            ------>             mergedObject     
 * 
 * @author tfechtner
 * @author jsykora
 */
public class StandardMergerProcessor implements MergerProcessor {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(StandardMergerProcessor.class);
    /** Název atributu hrany pro label v reprezentaci titanu. */
    private static final String LABEL_KEY_WORD = "label";
    /** Název atributu, kam se ukládá umístění. */
    public static final String SOURCE_LOCATION = "sourceLocation";
    /** Nazev technickeho atributu oznacujiciho mapovani uzlu na jiny uzel */
    public static final String MAPS_TO = "mapsTo";

    /** Objekt spravující super root databáze.*/
    @Autowired
    private SuperRootHandler superRootHandler;
    /** Objekt spravující source root databáze.*/
    @Autowired
    private SourceRootHandler sourceRootHandler;
    /** Revision root handler for the management of revision tree */
    @Autowired
    private RevisionRootHandler revisionRootHandler;

    /** Sluzba pro praci s metadaty skriptu. */

    @Override
    public List<MergerProcessorResult> mergeObject(String[] itemParameters, ProcessorContext context) {
        ItemTypes itemType = ItemTypes.parseString(itemParameters[0]);
        if (itemType != null) {
            switch (itemType) {
            case LAYER:
                // Vrstvu si odlozime do pameti a zpracujeme ji az s resourcem
                preprocessLayer(itemParameters, context);
                return new ArrayList<>();
            case NODE:
                return Arrays.asList(new MergerProcessorResult(processNode(itemParameters, context), itemType));
            case EDGE:
                return Arrays.asList(new MergerProcessorResult(processEdge(itemParameters, context), itemType));
            case RESOURCE:
                // Ocekavame obecne vice mergeovanych prvku: resource, vrstvu a hranu mezi nimi
                Map<ItemTypes, ResultType> processResourceResult = processResource(itemParameters, context);
                List<MergerProcessorResult> result = new ArrayList<>();
                for (Entry<ItemTypes, ResultType> entry : processResourceResult.entrySet()) {
                    result.add(new MergerProcessorResult(entry.getValue(), entry.getKey()));
                }
                return result;
            case NODE_ATTRIBUTE:
                return Arrays.asList(processNodeAttribute(itemParameters, context));
            case EDGE_ATTRIBUTE:
                return Arrays
                        .asList(new MergerProcessorResult(processEdgeAttribute(itemParameters, context), itemType));
            case SOURCE_CODE:
                return Arrays.asList(new MergerProcessorResult(processSourceCode(itemParameters, context), itemType));
            default:
                LOGGER.warn("Unknown item type " + itemParameters[0] + ".");
                return Arrays.asList(new MergerProcessorResult(ResultType.ERROR, null));
            }
        } else {
            LOGGER.warn("Unknown item type " + itemParameters[0] + ".");
            return Arrays.asList(new MergerProcessorResult(ResultType.ERROR, null));
        }
    }

    /**
     * Predzpacuje vrstvu pred jejim mergem. Ten se provede az s meregem resourcu,
     * ktery do ni patri - kvuli spravnemu urceni rozsahu verzi.
     *
     * @param itemParameters Raw parametry vrstvy.
     * @param context Stav pocesoru, ktery se modifikuje v ramci behu.
     */
    private void preprocessLayer(String[] itemParameters, ProcessorContext context) {
        if (itemParameters.length == ItemTypes.LAYER.getSize()) {
            String layerId = itemParameters[LayerFormat.ID.getIndex()];
            context.getMapLayerIdToLayer().put(layerId, itemParameters);
        } else {
            LOGGER.warn("Incorrect layer record: " + StringUtils.join(itemParameters, CsvHelper.DELIMITER));
        }
    }

    /**
     * Process (merge) resource. Together with resource is processed (merged) its layer.
     * <p>
     * If resource does not exist, a new resource vertex is created, {@link EdgeLabel#HAS_RESOURCE} edge is created and
     * connected from resource to the super root and the revision validity of the resource is set to the new revision.
     * <p>
     * If resource exists, its revision validity is increased up to the new revision.
     * <p>
     * If resource's layer does not exist, then a new layer vertex is created, {@link EdgeLabel#IN_LAYER} edge is created and
     * connected from resource to the layer, and the validity revision of the layer is set to the new revision.
     * <p>
     * If resource's layer exists, then the layer's revision validity is increased up to the new revision.
     * 
     * @param itemParameters  data of the inserted record
     * @param context  processor state that is being modified during the run
     * @return result of the process
     */
    protected Map<ItemTypes, ResultType> processResource(String[] itemParameters, ProcessorContext context) {
        Map<ItemTypes, ResultType> result = new LinkedHashMap<>();
        if (itemParameters.length == ItemTypes.RESOURCE.getSize()) {
            String resId = itemParameters[ResourceFormat.ID.getIndex()];
            String resName = itemParameters[ResourceFormat.NAME.getIndex()];
            String resType = itemParameters[ResourceFormat.TYPE.getIndex()];
            String resDescription = itemParameters[ResourceFormat.DESCRIPTION.getIndex()];
            String layerLocalId = itemParameters[ResourceFormat.LAYER.getIndex()];

            Vertex root = superRootHandler.getRoot(context.getDbTransaction());

            Double newRevision = context.getRevision();
            Double newEndRevision = RevisionUtils.getMaxMinorRevisionNumber(newRevision);
            Double latestCommitedRevision = revisionRootHandler
                    .getLatestCommittedRevisionNumber(context.getDbTransaction());

            // Najdeme vrstvu
            String[] layer = context.getMapLayerIdToLayer().get(layerLocalId);
            if (layer == null) {
                LOGGER.warn("For resource " + StringUtils.join(itemParameters, CsvHelper.DELIMITER)
                        + " does not exist layer");
                result.put(ItemTypes.LAYER, ResultType.ERROR);
                result.put(ItemTypes.RESOURCE, ResultType.ERROR);
                return result;
            }
            TitanVertexQuery query = ((TitanVertex) root).query()
                    .has(EdgeProperty.CHILD_NAME.t(), GraphOperation.processValueForChildName(resName))
                    .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, latestCommitedRevision)
                    .direction(Direction.IN);

            Iterator<Vertex> vertexIterator = query.vertices().iterator();
            Vertex existingResourceVertex = null;
            while (vertexIterator.hasNext()) {
                Vertex checkedVertex = vertexIterator.next();

                if (checkedVertex.getProperty(NodeProperty.RESOURCE_NAME.t()).equals(resName)
                        && checkedVertex.getProperty(NodeProperty.RESOURCE_TYPE.t()).equals(resType)) {

                    existingResourceVertex = checkedVertex;
                    context.getMapResourceIdToDbId().put(resId, checkedVertex.getId());
                    // equivalent resource was found
                    RevisionUtils.setVertexTransactionEnd(existingResourceVertex, newEndRevision);
                    break;
                }
            }

            String layerName = layer[LayerFormat.NAME.getIndex()];
            String layerType = layer[LayerFormat.TYPE.getIndex()];

            if (existingResourceVertex == null) {
                // Pokud neexistuje resource, vytvorime uzel resourcu a vrstvy a propojime je
                LOGGER.debug("Resource does not exist => new one will be created.");
                Vertex newLayerVertex = GraphCreation.createLayer(context.getDbTransaction(), layerName, layerType);
                Vertex newResourceVertex = GraphCreation.createResource(context.getDbTransaction(), root, resName,
                        resType, resDescription, newLayerVertex, newRevision);
                context.getMapResourceIdToDbId().put(resId, newResourceVertex.getId());
                result.put(ItemTypes.LAYER, ResultType.NEW_OBJECT);
                result.put(ItemTypes.RESOURCE, ResultType.NEW_OBJECT);
                return result;
            } else {
                // vime ze resource existuje, ale musime zjistit, zda existuje i jeho vrstva
                TitanVertexQuery layerVertexQuery = ((TitanVertex) existingResourceVertex).query()
                        .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, latestCommitedRevision)
                        .direction(Direction.OUT).labels(EdgeLabel.IN_LAYER.t());

                boolean layerNotExistsYet = true;
                Iterator<Vertex> layerVertexIterator = layerVertexQuery.vertices().iterator();

                while (layerNotExistsYet && layerVertexIterator.hasNext()) {
                    Vertex checkedLayerVertex = layerVertexIterator.next();
                    if (checkedLayerVertex.getProperty(NodeProperty.LAYER_NAME.t()).equals(layerName)) {
                        // Nasli jsme uzel vrstvy se stejnym nazvem
                        String checkedLayerVertexType = checkedLayerVertex.getProperty(NodeProperty.LAYER_TYPE.t());

                        if (!checkedLayerVertexType.equals(layerType)) {
                            LOGGER.warn("Layer with name '" + layerName + "' already exists, but the type is different."
                                    + " The existing type '" + checkedLayerVertexType + "' is used and the new type '"
                                    + layerType + "' is ignored.");
                        }

                        layerNotExistsYet = false;
                        // equivalent layer vertex was found
                        RevisionUtils.setVertexTransactionEnd(checkedLayerVertex, newEndRevision);
                        break;
                    }
                }
                if (layerNotExistsYet) {
                    // Vrstva neexistuje => vyrobime ji
                    LOGGER.debug("Layer does not exist => new one will be created.");
                    Vertex layerVertex = GraphCreation.createLayer(context.getDbTransaction(), layerName, layerType);
                    // a propojime ji s resourcem 
                    Edge edge = existingResourceVertex.addEdge(EdgeLabel.IN_LAYER.t(), layerVertex);
                    edge.setProperty(EdgeProperty.TRAN_START.t(), newRevision);
                    edge.setProperty(EdgeProperty.TRAN_END.t(), newEndRevision);
                    result.put(ItemTypes.LAYER, ResultType.NEW_OBJECT);
                } else {
                    // Vrstva pro resource jiz existuje => neni co resit
                    LOGGER.debug("Layer already eexists => nothing to be done");
                    result.put(ItemTypes.LAYER, ResultType.ALREADY_EXIST);
                }
                result.put(ItemTypes.RESOURCE, ResultType.ALREADY_EXIST);
                return result;
            }
        } else {
            LOGGER.warn("Incorrect resource record: " + StringUtils.join(itemParameters, CsvHelper.DELIMITER));
            result.put(ItemTypes.LAYER, ResultType.ERROR);
            result.put(ItemTypes.RESOURCE, ResultType.ERROR);
            return result;
        }
    }

    /**
     * Zpracuje uzel. Vloží nový vertex do grafu, pokud již takový neexistuje. <br>
     * Pokud nemá předka, je nový vertex spojen hranou {@link EdgeLabel#HAS_RESOURCE} se svým resource. <br>
     * Pokud má předka, je nový vertex spojen hranou {@link EdgeLabel#HAS_PARENT} se svým předkem. <br>
     * 
     * @param itemParameters  data vkládaného záznamu
     * @param context  stav procesoru, který se modifikuje v rámci běhu
     * @return výsledek, dle zpracování objektu
     */
    protected ResultType processNode(String[] itemParameters, ProcessorContext context) {
        if (itemParameters.length == ItemTypes.NODE.getSize()
                || itemParameters.length == ItemTypes.NODE.getSize() - 1) {
            String nodeId = itemParameters[NodeFormat.ID.getIndex()];
            String nodeType = itemParameters[NodeFormat.TYPE.getIndex()];
            String name = itemParameters[NodeFormat.NAME.getIndex()];
            String resourceId = itemParameters[NodeFormat.RESOURCE_ID.getIndex()];
            String parentLocalId = itemParameters[NodeFormat.PARENT_ID.getIndex()];
            String flag = null;
            if (itemParameters.length == ItemTypes.NODE.getSize()) {
                flag = itemParameters[NodeFormat.FLAG.getIndex()];
            }

            Double newRevision = context.getRevision();
            Double newEndRevision = RevisionUtils.getMaxMinorRevisionNumber(newRevision);
            Double latestCommittedRevision = revisionRootHandler
                    .getLatestCommittedRevisionNumber(context.getDbTransaction());

            Object resourceVertexId = context.getMapResourceIdToDbId().get(resourceId);
            if (resourceVertexId == null) {
                LOGGER.warn("For node " + StringUtils.join(itemParameters, CsvHelper.DELIMITER)
                        + " does not exist resource");
                return ResultType.ERROR;
            }
            Vertex resourceVertex = context.getDbTransaction().getVertex(resourceVertexId);
            if (resourceVertex == null) {
                LOGGER.error("For node " + StringUtils.join(itemParameters, CsvHelper.DELIMITER)
                        + " is saved incorrect resource " + resourceVertexId.toString() + ".");
                return ResultType.ERROR;
            }

            Vertex parentVertex = null;
            EdgeLabel parentEdgeType = null;
            if (parentLocalId == null || parentLocalId.isEmpty()) {
                parentEdgeType = EdgeLabel.HAS_RESOURCE;
                parentVertex = resourceVertex;
            } else {
                parentEdgeType = EdgeLabel.HAS_PARENT;
                Object parentVertexId = context.getMapNodeIdToDbId().get(parentLocalId);
                if (parentVertexId == null) {
                    LOGGER.warn("For node " + StringUtils.join(itemParameters, CsvHelper.DELIMITER)
                            + " does not exist parent.");
                    return ResultType.ERROR;
                }
                parentVertex = context.getDbTransaction().getVertex(parentVertexId);
                if (parentVertex == null) {
                    LOGGER.error("For node " + StringUtils.join(itemParameters, CsvHelper.DELIMITER)
                            + " is saved incorrect parent " + parentVertexId.toString() + ".");
                    return ResultType.ERROR;
                }
            }

            TitanVertexQuery query = ((TitanVertex) parentVertex).query()
                    .has(EdgeProperty.CHILD_NAME.t(), GraphOperation.processValueForChildName(name))
                    .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, latestCommittedRevision)
                    .direction(Direction.IN).labels(parentEdgeType.t());

            Iterator<Vertex> vertexIterator = query.vertices().iterator();
            boolean notYetExist = true;
            while (vertexIterator.hasNext()) {
                Vertex checkedVertex = vertexIterator.next();
                // equals je jen přes jméno a typ uzlu a jeho předky, již se nebere jeho přímý resource
                // -> neumožňujume dvojici se stejným jménem a typem pod stejným předkem a pouze s jinými resourcy 
                if (checkedVertex.getProperty(NodeProperty.NODE_NAME.t()).equals(name)
                        && checkedVertex.getProperty(NodeProperty.NODE_TYPE.t()).equals(nodeType)) {
                    // equivalent node was found
                    notYetExist = false;
                    context.getMapNodeIdToDbId().put(nodeId, checkedVertex.getId());

                    if (Flag.REMOVE_MYSELF.t().equals(flag)) {
                        // remove node with its whole subtree and do NOT recover this node after (do NOT set its transaction end to the new revision number)
                        GraphOperation.setSubtreeTransactionEnd(checkedVertex, latestCommittedRevision, latestCommittedRevision);
                        break;
                    }

                    if (Flag.REMOVE.t().equals(flag)) {
                        // remove node with its whole subtree but recover this node after (set its transaction end to the new revision number)
                        GraphOperation.setSubtreeTransactionEnd(checkedVertex, latestCommittedRevision, latestCommittedRevision);
                        RevisionUtils.setVertexTransactionEnd(checkedVertex, newEndRevision);
                        break;
                    }

                    // node has no flag, just set it transaction end to the new revision number
                    RevisionUtils.setVertexTransactionEnd(checkedVertex, newEndRevision);
                    break;
                }
            }
            if (notYetExist) {
                Vertex newNode;
                Vertex resource = GraphOperation.getResource(parentVertex);
                if (resource.equals(resourceVertex)) {
                    newNode = GraphCreation.createNode(context.getDbTransaction(), parentVertex, name, nodeType,
                            newRevision);
                } else {
                    newNode = GraphCreation.createNode(context.getDbTransaction(), parentVertex, resourceVertex, name,
                            nodeType, newRevision);
                }
                context.getMapNodeIdToDbId().put(nodeId, newNode.getId());

                return ResultType.NEW_OBJECT;
            } else {
                return ResultType.ALREADY_EXIST;
            }
        } else {
            LOGGER.warn("Incorrect node record: " + StringUtils.join(itemParameters, CsvHelper.DELIMITER));
            return ResultType.ERROR;
        }
    }

    /**
     * Zpracuje hranu.
     * Vkládá hranu, pouze pokud již taková neexistuje.
     * 
     * @param itemParameters  data vkládaného záznamu
     * @param context  stav procesoru, který se modifikuje v rámci běhu
     * @return výsledek, dle zpracování objektu
     */
    protected ResultType processEdge(String[] itemParameters, ProcessorContext context) {
        if (itemParameters.length == ItemTypes.EDGE.getSize()) {
            String edgeId = itemParameters[EdgeFormat.ID.getIndex()];

            Double newRevision = context.getRevision();
            Double newEndRevision = RevisionUtils.getMaxMinorRevisionNumber(newRevision);
            Double latestCommittedRevision = revisionRootHandler
                    .getLatestCommittedRevisionNumber(context.getDbTransaction());

            Object nodeSourceId = context.getMapNodeIdToDbId().get(itemParameters[EdgeFormat.SOURCE.getIndex()]);
            if (nodeSourceId == null) {
                LOGGER.warn("For edge " + StringUtils.join(itemParameters, CsvHelper.DELIMITER)
                        + " does not exist source vertex.");
                return ResultType.ERROR;
            }
            Vertex nodeSource = context.getDbTransaction().getVertex(nodeSourceId);
            if (nodeSource == null) {
                LOGGER.error("For edge " + StringUtils.join(itemParameters, CsvHelper.DELIMITER)
                        + " is saved incorrect source vertex with id " + nodeSourceId + ".");
                return ResultType.ERROR;
            }

            Object nodeTargetId = context.getMapNodeIdToDbId().get(itemParameters[EdgeFormat.TARGET.getIndex()]);
            if (nodeTargetId == null) {
                LOGGER.warn("For edge " + StringUtils.join(itemParameters, CsvHelper.DELIMITER)
                        + " does not exist target vertex.");
                return ResultType.ERROR;
            }
            Vertex nodeTarget = context.getDbTransaction().getVertex(nodeTargetId);
            if (nodeTarget == null) {
                LOGGER.error("For edge " + StringUtils.join(itemParameters, CsvHelper.DELIMITER)
                        + " is saved incorrect target vertex with id " + nodeTargetId + ".");
                return ResultType.ERROR;
            }

            EdgeLabel type = EdgeLabel.parseFromGraphType(itemParameters[EdgeFormat.TYPE.getIndex()]);
            if (type == null) {
                LOGGER.warn("Unkown edge type found in: " + StringUtils.join(itemParameters, CsvHelper.DELIMITER));
                return ResultType.ERROR;
            }

            Iterator<Edge> existedEdges = nodeSource.query().has(EdgeProperty.TARGET_ID.t(), nodeTargetId)
                    .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, latestCommittedRevision)
                    .direction(Direction.OUT).labels(type.t()).edges().iterator();

            boolean notExistYet = true;
            // zjistit jestli existuje už taková hrana            
            while (existedEdges.hasNext()) {
                Edge existedEdge = existedEdges.next();
                if (existedEdge.getVertex(Direction.IN).getId().equals(nodeTarget.getId())) {
                    // increase end revision
                    RevisionUtils.setEdgeTransactionEnd(existedEdge, newEndRevision);
                    notExistYet = false;
                    context.getMapEdgeIdToDbId().put(edgeId, new EdgeIdentification(existedEdge));
                    break;
                }
            }
            // hrana jeste neexistuje -> vytvorit
            if (notExistYet) {
                Edge newEdge = GraphCreation.createEdge(nodeSource, nodeTarget, type, newRevision);
                context.getMapEdgeIdToDbId().put(edgeId, new EdgeIdentification(newEdge));
                return ResultType.NEW_OBJECT;
            } else {
                return ResultType.ALREADY_EXIST;
            }
        } else {
            LOGGER.warn("Incorrect edge record: " + StringUtils.join(itemParameters, CsvHelper.DELIMITER));
            return ResultType.ERROR;
        }
    }

    /**
     * Zpracuje atribut hrany.
     * Atributy jsou vkládány jako property dané hraně. 
     * Pokud již má takový atribut, dojde k přepsání na novou hodnotu.
     * 
     * @param itemParameters  data vkládaného záznamu
     * @param context  stav procesoru, který se modifikuje v rámci běhu
     * @return výsledek, dle zpracování objektu
     */
    protected ResultType processEdgeAttribute(String[] itemParameters, ProcessorContext context) {
        if (itemParameters.length == ItemTypes.EDGE_ATTRIBUTE.getSize()) {
            String objectId = itemParameters[AttributeFormat.OBJECT_ID.getIndex()];
            String key = itemParameters[AttributeFormat.KEY.getIndex()];
            String value = itemParameters[AttributeFormat.VALUE.getIndex()];

            Double newRevision = context.getRevision();
            Double latestCommittedRevision = revisionRootHandler
                    .getLatestCommittedRevisionNumber(context.getDbTransaction());

            EdgeIdentification edgeId = context.getMapEdgeIdToDbId().get(objectId);
            if (edgeId == null) {
                LOGGER.warn("Edge for edge attribute does not exist: " + objectId);
                return ResultType.ERROR;
            }

            Edge edge = GraphOperation.getEdge(context.getDbTransaction(), edgeId,
                    new RevisionInterval(latestCommittedRevision, newRevision));

            if (edge == null) {
                LOGGER.error("For target of edge attribute is saved incorrect id: " + edgeId.toString());
                return ResultType.ERROR;
            }
            // atribut se nesmí jmenovat label, tím by se změnil charakter hrany
            if (LABEL_KEY_WORD.equalsIgnoreCase(key)) {
                key = "_" + key;
                LOGGER.info("Edge attribute with name " + LABEL_KEY_WORD + " and value " + value + " changed to " + key
                        + ".");
            }

            Object actualPropertyValue = edge.getProperty(key);
            if (actualPropertyValue == null) {
                edge.setProperty(key, value);
                return ResultType.NEW_OBJECT;
            } else if (!actualPropertyValue.equals(value)) {
                RevisionInterval currentEdgeInterval = RevisionUtils.getRevisionInterval(edge);
                if (currentEdgeInterval.getStart() < newRevision) {
                    // current edge is from the previous revision (latest committed revision)
                    // a new edge has to be created in the new revision
                    Edge newEdge = copyEdge(edge, newRevision);
                    context.getMapEdgeIdToDbId().put(objectId, new EdgeIdentification(newEdge));
                    newEdge.setProperty(key, value);
                    // close end revision of the old edge with the old edge attribute
                    edge.setProperty(EdgeProperty.TRAN_END.t(), latestCommittedRevision);
                } else {
                    edge.setProperty(key, value);
                }
                return ResultType.NEW_OBJECT;
            } else {
                return ResultType.ALREADY_EXIST;
            }
        } else {
            LOGGER.warn("Incorrect edge attr record: " + StringUtils.join(itemParameters, CsvHelper.DELIMITER));
            return ResultType.ERROR;
        }
    }

    /**
     * Zkopíruje hranu do dané revize včetně všech atributů.
     * 
     * @param originalEdge originální hrana ke zkopírování
     * @param revision revize pro novou hranu
     * @return nově vytvořená hrana
     */
    private Edge copyEdge(Edge originalEdge, double revision) {
        Edge newEdge = GraphCreation.createEdge(originalEdge.getVertex(Direction.OUT),
                originalEdge.getVertex(Direction.IN), EdgeLabel.parseFromDbType(originalEdge.getLabel()), revision);

        for (String key : originalEdge.getPropertyKeys()) {
            // only do not copy identification properties (targetId, tranStart and tranEnd)
            if (key.equals(EdgeProperty.TARGET_ID.t()) || key.equals(EdgeProperty.TRAN_START.t())
                    || key.equals(EdgeProperty.TRAN_END.t())) {
                continue;
            }

            Object originalValue = originalEdge.getProperty(key);
            newEdge.setProperty(key, originalValue);
        }

        return newEdge;
    }

    /**
     * Zpracuje atribut uzlu.
     * Atributy jsou vkládány jako nové vertexy spojené uzlem hranou {@link EdgeLabel#HAS_ATTRIBUTE}.
     * 
     * @param itemParameters data vkládaného záznamu
     * @param context stav procesoru, který se modifikuje v rámci běhu
     * @return výsledek, dle zpracování objektu
     */
    @SuppressWarnings("unchecked")
    protected MergerProcessorResult processNodeAttribute(String[] itemParameters, ProcessorContext context) {
        if (itemParameters.length == ItemTypes.NODE_ATTRIBUTE.getSize()) {
            String objectId = itemParameters[AttributeFormat.OBJECT_ID.getIndex()];
            String key = itemParameters[AttributeFormat.KEY.getIndex()];
            Object value = null;

            Double newRevision = context.getRevision();
            Double newEndRevision = RevisionUtils.getMaxMinorRevisionNumber(newRevision);
            Double latestCommittedRevision = revisionRootHandler
                    .getLatestCommittedRevisionNumber(context.getDbTransaction());

            try {
                value = Base64AttributeCodingHelper.decodeAttribute(itemParameters[AttributeFormat.VALUE.getIndex()]);
            } catch (IOException e) {
                LOGGER.warn("Error decoding binary attribute " + key + " for id: " + objectId + ". I/O error occurred",
                        e);
                return new MergerProcessorResult(ResultType.ERROR, ItemTypes.NODE_ATTRIBUTE);
            } catch (ClassNotFoundException e) {
                LOGGER.warn("Error decoding binary attribute " + key + " for id: " + objectId + ". Class not found", e);
                return new MergerProcessorResult(ResultType.ERROR, ItemTypes.NODE_ATTRIBUTE);
            }

            Object nodeVertexId = context.getMapNodeIdToDbId().get(objectId);
            if (nodeVertexId == null) {
                LOGGER.warn("Object for node attribute does not exist: " + objectId);
                return new MergerProcessorResult(ResultType.ERROR, ItemTypes.NODE_ATTRIBUTE);
            }
            Vertex nodeVertex = context.getDbTransaction().getVertex(nodeVertexId);
            if (nodeVertex == null) {
                LOGGER.warn("For target of node attribute is saved incorrect id: " + objectId);
                return new MergerProcessorResult(ResultType.ERROR, ItemTypes.NODE_ATTRIBUTE);
            }

            if (MAPS_TO.equals(key)) {
                // Zpracovavany uzel (zdrojovy) mapuje jiny uzel (cilovy), odkazovany hodnotou atributu "mapsTo"
                // Hodnota je ocekavana jako seznam trojic (cesta k uzlu, typ uzlu, zdroj)
                if (!(value instanceof List<?>)) {
                    LOGGER.warn(
                            MessageFormat.format("Incorrect type of MAPS_TO atribute value. Expected {0} but was {1}",
                                    List.class, value != null ? value.getClass() : null));
                    return new MergerProcessorResult(ResultType.ERROR, ItemTypes.EDGE);
                }
                ResultType nodeMappingResult = processNodeMapping(context, nodeVertex, (List<List<String>>) value);
                return new MergerProcessorResult(nodeMappingResult, ItemTypes.EDGE);
            } else if (SOURCE_LOCATION.equals(key)) {
                Object sourceCodeId = context.getMapSourceCodeIdToDbId().get(value.toString());
                if (sourceCodeId == null) {
                    LOGGER.warn("Source code for node does not exist: " + objectId);
                    return new MergerProcessorResult(ResultType.ERROR, ItemTypes.NODE_ATTRIBUTE);
                }
                value = sourceCodeId;
            }

            Iterator<Vertex> vertexIterator = nodeVertex.query()
                    .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, latestCommittedRevision)
                    .direction(Direction.OUT).labels(EdgeLabel.HAS_ATTRIBUTE.t()).vertices().iterator();

            boolean notExistYet = true;
            while (vertexIterator.hasNext()) {
                Vertex checkedVertex = vertexIterator.next();
                if (checkedVertex.getProperty(NodeProperty.ATTRIBUTE_NAME.t()).equals(key)
                        && checkedVertex.getProperty(NodeProperty.ATTRIBUTE_VALUE.t()).equals(value)) {
                    RevisionUtils.setVertexTransactionEnd(checkedVertex, newEndRevision);
                    notExistYet = false;
                    break;
                }
            }

            if (notExistYet) {
                GraphCreation.createNodeAttribute(context.getDbTransaction(), nodeVertex, key, value, newRevision);
                return new MergerProcessorResult(ResultType.NEW_OBJECT, ItemTypes.NODE_ATTRIBUTE);
            } else {
                return new MergerProcessorResult(ResultType.ALREADY_EXIST, ItemTypes.NODE_ATTRIBUTE);
            }
        } else {
            LOGGER.warn("Incorrect node attr record: " + StringUtils.join(itemParameters, CsvHelper.DELIMITER));
            return new MergerProcessorResult(ResultType.ERROR, ItemTypes.NODE_ATTRIBUTE);
        }
    }

    /**
     * Zpracuje source code.
     * 
     * @param itemParameters data vkládaného záznamu
     * @param context stav procesoru, který se modifikuje v rámci běhu
     * @return výsledek, dle zpracování objektu
     */
    protected ResultType processSourceCode(String[] itemParameters, ProcessorContext context) {
        if (itemParameters.length != ItemTypes.SOURCE_CODE.getSize()) {
            LOGGER.warn("Incorrect source code record: " + StringUtils.join(itemParameters, CsvHelper.DELIMITER));
            return ResultType.ERROR;
        }

        String scId = itemParameters[SourceCodeFormat.ID.getIndex()];
        String scLocalName = itemParameters[SourceCodeFormat.LOCAL_NAME.getIndex()];
        String scHash = itemParameters[SourceCodeFormat.HASH.getIndex()];

        Double newRevision = context.getRevision();
        Double newEndRevision = RevisionUtils.getMaxMinorRevisionNumber(newRevision);
        Double latestCommittedRevision = revisionRootHandler
                .getLatestCommittedRevisionNumber(context.getDbTransaction());

        Vertex sourceRoot = sourceRootHandler.getRoot(context.getDbTransaction());

        // vyhledat vertex reprezentující soubor s daným názvem a má platnost v poslední commitlé revizi
        TitanVertexQuery query = ((TitanVertex) sourceRoot).query()
                .has(EdgeProperty.SOURCE_LOCAL_NAME.t(), GraphOperation.processValueForLocalName(scLocalName))
                .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, latestCommittedRevision)
                .direction(Direction.OUT).labels(EdgeLabel.HAS_SOURCE.t());

        // prohledat danou množinu
        Iterator<Vertex> vertexIterator = query.vertices().iterator();
        boolean notExistYet = true;
        while (vertexIterator.hasNext()) {
            Vertex checkedVertex = vertexIterator.next();
            if (checkedVertex.getProperty(NodeProperty.SOURCE_NODE_LOCAL.t()).equals(scLocalName)
                    && checkedVertex.getProperty(NodeProperty.SOURCE_NODE_HASH.t()).equals(scHash)) {
                RevisionUtils.setVertexTransactionEnd(checkedVertex, newEndRevision);
                notExistYet = false;
                String scDbId = sourceRootHandler.getSourceNodeId(checkedVertex);
                context.getMapSourceCodeIdToDbId().put(scId, scDbId);
                break;
            }
        }

        if (notExistYet) {
            // vertex ještě neexistuje -> vytvořit
            Vertex sourceCodeVertex = GraphCreation.createSourceCode(context.getDbTransaction(), sourceRoot,
                    newRevision, scLocalName, scHash);

            String scDbId = sourceRootHandler.getSourceNodeId(sourceCodeVertex);
            context.getMapSourceCodeIdToDbId().put(scId, scDbId);
            context.addRequestedSourceCode(new SourceCodeMapping(scId, scDbId));
            return ResultType.NEW_OBJECT;
        } else {
            return ResultType.ALREADY_EXIST;
        }
    }

    /**
     * Zpracuje mapovani uzlu
     * 
     * @param context  Stav procesoru, ktery se modifikuje v ramci behu
     * @param node  Uzel, ktery mapuje (zdrojovy)
     * @param mappedNodeQualifiedName  Kvalifikovane jmeno mapovaneho uzlu (ciloveho)
     * @return Vysledek, dle zpracovani objektu
     */
    private ResultType processNodeMapping(ProcessorContext context, Vertex node,
            List<List<String>> mappedNodeQualifiedName) {
        Vertex root = superRootHandler.getRoot(context.getDbTransaction());
        Double newRevision = context.getRevision();
        Double newEndRevision = RevisionUtils.getMaxMinorRevisionNumber(newRevision);
        Double latestCommittedRevision = revisionRootHandler
                .getLatestCommittedRevisionNumber(context.getDbTransaction());
        RevisionInterval newRevisionInterval = new RevisionInterval(newRevision, newRevision);
        // Ziskame mapovany uzel podle kvalifikovaneho jmena
        Vertex mappedNode = GraphOperation.getVertexByQualifiedName(root, mappedNodeQualifiedName, newRevisionInterval);
        if (mappedNode == null) {
            LOGGER.warn(MessageFormat.format("Unable to find node of resource with qualified name {0}.",
                    mappedNodeQualifiedName));
            return ResultType.ERROR;
        }
        // Zjistime, zda jiz existuje mapovaci hrana mezi uzly 
        EdgeIdentification edgeId = new EdgeIdentification((Long) node.getId(), (Long) mappedNode.getId(),
                EdgeLabel.MAPS_TO);
        Edge existingMapsToEdge = GraphOperation.getEdge(context.getDbTransaction(), edgeId,
                new RevisionInterval(latestCommittedRevision, newRevision));
        if (existingMapsToEdge != null) {
            RevisionUtils.setEdgeTransactionEnd(existingMapsToEdge, newEndRevision);
            return ResultType.ALREADY_EXIST;
        } else {
            // Pokud ne, vytvorime ji
            GraphCreation.createEdge(node, mappedNode, EdgeLabel.MAPS_TO, newRevision);
            return ResultType.NEW_OBJECT;
        }
    }

    /**
     * @return Objekt spravující super root databáze
     */
    public SuperRootHandler getSuperRootHandler() {
        return superRootHandler;
    }

    /**
     * @param superRootHandler Objekt spravující super root databáze
     */
    public void setSuperRootHandler(SuperRootHandler superRootHandler) {
        this.superRootHandler = superRootHandler;
    }

    /**
     * @return Objekt spravující source root databáze
     */
    public SourceRootHandler getSourceRootHandler() {
        return sourceRootHandler;
    }

    /**
     * @param sourceRootHandler Objekt spravující source root databáze
     */
    public void setSourceRootHandler(SourceRootHandler sourceRootHandler) {
        this.sourceRootHandler = sourceRootHandler;
    }

    /** 
     * @return Revision root handler for the management of revision tree
     */
    public RevisionRootHandler getRevisionRootHandler() {
        return revisionRootHandler;
    }

    /**
     * 
     * @param revisionRootHandler  Revision root handler for the management of revision tree
     */
    public void setRevisionRootHandler(RevisionRootHandler revisionRootHandler) {
        this.revisionRootHandler = revisionRootHandler;
    }

}