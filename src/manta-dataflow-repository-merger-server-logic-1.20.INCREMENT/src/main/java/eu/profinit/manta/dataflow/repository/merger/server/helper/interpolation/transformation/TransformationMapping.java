package eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.transformation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Mapovani nove vytvareneho transformacniho uzlu v INTERPOLACNI vrstve
 * na existujici transformacni uzel v MAPOVANE vrstve
 * 
 * @author onouza
 *
 */
public class TransformationMapping {
    
    /** Pocet polozek v seznamu pro transformacni mapovani za predpokladu, ze vsechny nepovinne jsou uvedeny */
    private static final int TRANSFORMATION_MAPPING_FULL_SIZE = 4;
    /** Pocet polozek v seznamu pro transformacni mapovani za predpokladu, ze zadna z nepovinnych neni uvedena */
    private static final int TRANSFORMATION_MAPPING_MIN_SIZE = 3;
    /** Index polozky predstaujici cestu resource transformacnich uzlu v INTERPOLVOANE vrstve */
    private static final int INTERPOLATED_TRANSFORMATION_RESOURCE_INDEX = 0;
    /** Index polozky predstaujici cestu k transformacnimu uzlu v MAPOVANE vrstve */
    private static final int MAPPED_TRANSFORMATION_INDEX = 1;
    /** Index polozky predstaujici nazev transformacniho uzlu v INTERPOLVOANE vrstve */
    private static final int INTERPOLATED_TRANSFORMATION_INDEX = 2;
    /** Index polozky predstaujici mapu atributu transformacniho uzlu v INTERPOLVOANE vrstve */
    private static final int TRANSFORMATION_ATTRIBUTES_INDEX = 3;
    /** Pocet prvku pole transformacniho resourcu */
    private static final int RESOURCE_SIZE = 3;
    /** Index prvku predstavujici nazev transformacniho resourcu */
    private static final int RESOURCE_NAME_INDEX = 0;
    /** Index prvku predstavujici typ transformacniho resourcu */
    private static final int RESOURCE_TYPE_INDEX = 1;
    /** Index prvku predstavujici popis transformacniho resourcu */
    private static final int RESOURCE_DESCRIPTION_INDEX = 2;
    
    /** Nazev resource transformacnich uzlu v INTERPOLACNI vrstve */
    private final Resource interpolatedTransformationResource;
    /** Cesta k transformacnimu uzlu v mapovane vrstve */
    private final List<String> mappedTransformation;
    /** Nazev transformacniho uzlu v INTERPOLACNI vrstve */
    private final String interpolatedTransformation;
    /** 
     * Mapa atributu vytvareneho transformacniho uzlu.
     * Klicem jsou nazvy atributu, hodnoty jsou hodnoty atrbutu.
     */
    private Map<String, ?> transformationAttributes;

    /**
     * Konstruktor.
     * 
     * @param interpolatedTransformationResource Nazev resource transformacnich uzlu v INTERPOLACNI vrstve
     */
    private TransformationMapping(Resource interpolatedTransformationResource) {
        this(interpolatedTransformationResource, Collections.<String>emptyList(),
                null, Collections.<String, Object>emptyMap());
    }

    /**
     * Konstruktor.
     * 
     * @param interpolatedTransformationResource Nazev resource transformacnich uzlu v INTERPOLACNI vrstve 
     * @param mappedTransformation Cesta k transformacnimu uzlu v mapovane vrstve
     * @param interpolatedTransformation Nazev transformacniho uzlu v INTERPOLACNI vrstve
     * @param transformationAttributes Mapa atributu vytvareneho transformacniho uzlu.
     *          Klicem jsou nazvy atributu, hodnoty jsou hodnoty atrbutu.
     */
    private  TransformationMapping(Resource interpolatedTransformationResource, List<String> mappedTransformation,
            String interpolatedTransformation, Map<String, ?> transformationAttributes) {
        this.interpolatedTransformationResource = interpolatedTransformationResource;
        this.mappedTransformation = new ArrayList<>(mappedTransformation);
        this.interpolatedTransformation = interpolatedTransformation;
        this.transformationAttributes = new HashMap<>(transformationAttributes);
    }

    /**
     * Vytvori transformacni mapovani z predanych dat.
     * Pokud data nejsou ve spravnem formatu, vrati {@code null}.
     * 
     * Ocekavany format dat:
     * 
     * <pre>
     * (cesta k transformacnimu uzlu v MAPOVANE vrstve,
     *  nazev transformacniho uzlu v INTERPOLOVANE vrstve,
     *  atributy transformacniho uzlu v INTERPOLOVANE vrstve) : List&lt;?>
     * 
     * cesta k transformacnimu uzlu v MAPOVANE vrstve : List&lt;String>
     * nazev transformacniho uzlu v INTERPOLOVANE vrstve : String
     * atributy transformacniho uzlu v INTERPOLOVANE vrstve : Map&lt;String, ?>, nepovinny 
     * </pre>
     * 
     * @param rawTranformationMapping Data transformacniho mapovani
     * @return Nove vytvorene transformacni mapovani nebo {@code null}, pokud nejsou vstupni data ve spravnem formatu. 
     */
    public static TransformationMapping of(Object rawTranformationMapping) {
        if (rawTranformationMapping instanceof List<?>) {
            return fromRawMapping((List<?>) rawTranformationMapping);
        } else {
            return null;
        }
    }
    
    // privatni metody
    
    /**
     * Vytvori transformacni mapovani z predanych dat.
     * Pokud je resource transformace ve spravnem formatu a ostatni data ve spravnem formatu nejsou,
     * vrati prazdne mapovani obsahujici pouze informaci o resource.
     * Pokud neni ve spravnem formatu ani resource, vrati {@code null}.
     * 
     * Ocekavany format dat viz {@link #of(Object)}
     * 
     * @param rawTranformationMapping Data transformacniho mapovani
     * 
     * @return Nove vytvorene transformacni mapovani nebo {@code null}, pokud nejsou vstupni data ve spravnem formatu. 
     */ 
    private static TransformationMapping fromRawMapping(List<?> rawTranformationMapping) {
        if (rawTranformationMapping.isEmpty()) {
            return null;
        }
        
        // Resource transformaci v INTERPOLOVANE vrstve
        Object rawInterpolatedTransformationResource = rawTranformationMapping.get(INTERPOLATED_TRANSFORMATION_RESOURCE_INDEX);
        if (!(rawInterpolatedTransformationResource instanceof String[])
                || ((String[])rawInterpolatedTransformationResource).length < RESOURCE_SIZE) {
            return null;
        }
        Resource interpolatedTransformationResource = new Resource(
                ((String[])rawInterpolatedTransformationResource)[RESOURCE_NAME_INDEX],
                ((String[])rawInterpolatedTransformationResource)[RESOURCE_TYPE_INDEX],
                ((String[])rawInterpolatedTransformationResource)[RESOURCE_DESCRIPTION_INDEX]);
        
        // Transformacni mapovani obsahujici pouze resource transformace.
        // Bude vraceno pokud odstatni data transformace jsou nevalidni.  
        final TransformationMapping resourceOnlyTransformationMapping = new TransformationMapping(interpolatedTransformationResource);

        if (rawTranformationMapping.size() < TRANSFORMATION_MAPPING_MIN_SIZE) {
            return resourceOnlyTransformationMapping;
        }
        
        // Transformace v MAPOVANE vrstve
        Object rawMappedTransformation = rawTranformationMapping.get(MAPPED_TRANSFORMATION_INDEX);
        if (rawMappedTransformation != null && !(rawMappedTransformation instanceof List<?>)) {
            return resourceOnlyTransformationMapping;
        }
        List<String> mappedTransformation = fromRawMappedTransformation((List<?>)rawMappedTransformation);
        if (mappedTransformation == null) {
            return resourceOnlyTransformationMapping;
        }
        
        // Transformace v INTERPOLOVANE vrstve
        Object rawInterpolatedTransformation = rawTranformationMapping.get(INTERPOLATED_TRANSFORMATION_INDEX);
        if (rawInterpolatedTransformation != null && !(rawInterpolatedTransformation instanceof String)) {
            return resourceOnlyTransformationMapping;
        }
        String interpolatedTransformation = (String)rawInterpolatedTransformation;

        // Atributy transformace v INTERPOLOVANE vrstve
        Map<String, ?> transformationAttributes = new HashMap<>();
        if (rawTranformationMapping.size() >= TRANSFORMATION_MAPPING_FULL_SIZE) {
            Object rawTransformationAttributes = rawTranformationMapping.get(TRANSFORMATION_ATTRIBUTES_INDEX);
            if (rawTransformationAttributes != null && !(rawTransformationAttributes instanceof Map<?, ?>)) {
                return resourceOnlyTransformationMapping;
            }
            transformationAttributes = fromRawTransformationAttributes((Map<?, ?>)rawTransformationAttributes);
            if (transformationAttributes == null) {
                return resourceOnlyTransformationMapping;
            }
        }
        return new TransformationMapping(interpolatedTransformationResource, mappedTransformation, interpolatedTransformation, transformationAttributes);
    }

    /**
     * Vytvori transformaci v MAPOVANE vrstve z predanych dat.
     * Pokud data nejsou ve spravnem formatu, vrati {@code null}.
     * 
     * Ocekavany format dat je seznam retezcu.
     * 
     * @param rawMappedTransformation Data transformace v MAPOVANE vrstve
     * @return Nove vytvorene transformace v MAPOVANE vrstve nebo {@code null}, pokud nejsou vstupni data ve spravnem formatu.
     */
    private static List<String> fromRawMappedTransformation(List<?> rawMappedTransformation) {
        List<String> result = new ArrayList<>();
        if (rawMappedTransformation != null) {
            for (Object entry : rawMappedTransformation) {
                if (!(entry instanceof String)) {
                    return null;
                }
                result.add((String)entry);
            }
        }
        return result;
    }
    
    /**
     * Vytvori mapu atributu transformace v INTERPOLOVANE vrstve z predanych dat.
     * Pokud nejsou data ve spravnem formatu, vrati {@code null}.
     * 
     * Ocekavany format dat je mapa, kde klice jsou nazvy atributu a hodnoty jsou jejich hodnoty. 
     * 
     * @param rawTransformationAttributes Data atributu transformace v INTERPOLOVANE vrstve.
     * @return Nove vytvorene atributy transformace v INTERPOLOVANE vrstve nebo {@code null}, pokud nejsou vstupni data ve spravnem formatu.
     */
    private static Map<String, ?> fromRawTransformationAttributes(Map<?, ?> rawTransformationAttributes) {
        Map<String, Object> result = new HashMap<>();
        if (rawTransformationAttributes != null) {
            for (Map.Entry<?, ?> entry : rawTransformationAttributes.entrySet()) {
                if (!(entry.getKey() instanceof String)) {
                    return null;
                }
                result.put((String)entry.getKey(), entry.getValue());
            }
        }
        return result;
    }

    // gettery / settery
    
    /**
     * @return Nazev resource transformacnich uzlu v INTERPOLACNI vrstve
     */
    public Resource getInterpolatedTransformationResource() {
        return interpolatedTransformationResource;
    }

    /**
     * @return Cesta k transformacnimu uzlu v mapovane vrstve
     */
    public List<String> getMappedTransformation() {
        return new ArrayList<>(mappedTransformation);
    }
    
    /**
     * @return Nazev transformacniho uzlu v INTERPOLACNI vrstve
     */
    public String getInterpolatedTransformation() {
        return interpolatedTransformation;
    }
    
    /**
     * @return Mapa atributu vytvareneho transformacniho uzlu.
     *         Klicem jsou nazvy atributu, hodnoty jsou hodnoty atrbutu.
     */
    public Map<String, ?> getTransformationAttributes() {
        return new HashMap<>(transformationAttributes);
    }
    
    // vnitrni tridy
    
    /**
     * Datova trida resource transformace
     * 
     * @author onouza
     *
     */
    public static class Resource {
        /** Nazev resource */
        private final String name;
        /** Typ resource */
        private final String type;
        /** Popis resource */
        private final String description;
        
        /**
         * Konstruktor.
         * 
         * @param name Nazev resource
         * @param type Typ resource
         * @param description Popis resource
         */
        private Resource(String name, String type, String description) {
            this.name = name;
            this.type = type;
            this.description = description;
        }
        
        // gettery / settery
        
        /**
         * @return Nazev resource
         */
        public String getName() {
            return name;
        }
        
        /**
         * @return Typ resource
         */
        public String getType() {
            return type;
        }

        /**
         * @return Popis resource
         */
        public String getDescription() {
            return description;
        }
    }
    
}
