package eu.profinit.manta.dataflow.repository.merger.server.helper.revision;

import com.tinkerpop.blueprints.Edge;

import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;

/**
 * Prune stav hrany značící, co se má provést.
 * @author tfechtner
 *
 */
public enum PruneState {
    /** Vrchol připojený hranou se má smazat.*/
    DELETE,
    /** Není třeba provádět změnu.*/
    NOT_CHANGE;

    /**
     * Zjistit stav pro danou hranu.
     * @param edge zkoumaná hrana
     * @param revisionInterval mazaný interval
     * @return výsledný prune stav
     */
    public static PruneState findState(Edge edge, RevisionInterval revisionInterval) {
        Double tranEnd = edge.getProperty(EdgeProperty.TRAN_END.t());
        if (tranEnd <= revisionInterval.getEnd()) {
            return DELETE;
        } else {
            return NOT_CHANGE;
        }
    }
}