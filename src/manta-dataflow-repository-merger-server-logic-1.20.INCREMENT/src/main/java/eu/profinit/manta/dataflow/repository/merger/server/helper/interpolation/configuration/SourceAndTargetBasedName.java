package eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.transformation.TransformationMapping;

/**
 * Nazev uzlu zalozeny na zdrojovem a cilovem uzlu.
 * Pro oba uzly najde ve vzestupne hierarchii nejblizsi uzly pozadovaneho typu
 * a z nazvu techto uzlu sestavi vysledek.
 * 
 * @author onouza
 */
public final class SourceAndTargetBasedName implements NodeNameOrigin {
    
    /**
     * Typ zdrojoveho uzlu, ze ktereho bude prebiran nazev.
     */
    private String sourceType;
    /**
     * Typ ciloveho uzlu, ze ktereho bude prebiran nazev.
     */
    private String targetType;
    /**
     * Sablona nazvu uzlu. 
     * Ridi se pravidly pro {@link MessageFormat#format(String, Object...)},
     * kde parametr <code>{0}</code> je nazev zdrojoveho uzlu
     * a parametr <code>{1}</code> je nazev ciloveho uzlu.
     * Pokud jsou oba nazvy stejne, sablona se neaplikuje a vysledek se shoduje
     * s nazvem zdrojoveho (a tim i ciloveho) uzlu. 
     */
    private String namePattern;

    /**
     * Konstruktor.
     * 
     * @param nodesType Typ zdrojoveho a ciloveho uzlu, ze ktereho bude prebiran nazev.
     * @param namePattern Sablona nazvu uzlu. 
     *          Ridi se pravidly pro {@link MessageFormat#format(String, Object...)},
     *          kde parametr <code>{0}</code> je nazev zdrojoveho uzlu
     *          a parametr <code>{1}</code> je nazev ciloveho uzlu.
     *          Pokud jsou oba nazvy stejne, sablona se neaplikuje a vysledek se shoduje
     *          s nazvem zdrojoveho (a tim i ciloveho) uzlu. 
     */
    public SourceAndTargetBasedName(String nodesType, String namePattern) {
        this(nodesType, nodesType, namePattern);
    }

    /**
     * Konstruktor.
     * 
     * @param sourceType Typ zdrojoveho, ze ktereho bude prebiran nazev.
     * @param targetType Typ ciloveho uzlu, ze ktereho bude prebiran nazev.
     * @param namePattern Sablona nazvu uzlu. 
     *          Ridi se pravidly pro {@link MessageFormat#format(String, Object...)},
     *          kde parametr <code>{0}</code> je nazev zdrojoveho uzlu
     *          a parametr <code>{1}</code> je nazev ciloveho uzlu.
     *          Pokud jsou oba nazvy stejne, sablona se neaplikuje a vysledek se shoduje
     *          s nazvem zdrojoveho (a tim i ciloveho) uzlu. 
     */
    public SourceAndTargetBasedName(String sourceType, String targetType, String namePattern) {
        Validate.notNull(sourceType, "'sourceType' must not be null");
        Validate.notNull(targetType, "'targetType' must not be null");
        Validate.notNull(namePattern, "'namePattern' must not be null");
        this.sourceType = sourceType;
        this.targetType = targetType;
        this.namePattern = namePattern;
    }

    @Override
    public String getNodeName(Vertex source, Vertex target, Vertex transformation, TransformationMapping transformationMapping) {
        Vertex sourceNode = GraphOperation.getClosestNodeByType(source, sourceType);
        Vertex targetNode = GraphOperation.getClosestNodeByType(target, targetType);
        if (sourceNode == null || targetNode == null) {
            // Nenalezen uzel pozadovaneho typu pro zdrojovy nebo cilovy uzel => nelze sestavit nazev
            return null;
        }
        String sourceName = GraphOperation.getName(sourceNode);
        String targetName = GraphOperation.getName(targetNode);
        if (StringUtils.equalsIgnoreCase(sourceName, targetName)) {
            // Nazvy jsou shodne => vratime nazev uzlu tak jak je
            return targetName;
        } else {
            // Nazvy jsou ruzne => aplikujeme je na sablonu vysledneho nazvu
            return MessageFormat.format(namePattern, sourceName, targetName);
        }
    }

}
