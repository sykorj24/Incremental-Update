package eu.profinit.manta.dataflow.repository.merger.server.helper.sourcecode;

import java.io.File;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeUploadRequest;

/**
 * Helper pro práci se source code soubory.
 * @author tfechtner
 *
 */
public class SourceCodeHelper {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SourceCodeHelper.class);
    /** Source code root handler. */
    @Autowired
    private SourceRootHandler sourceRootHandler;

    /**
     * Pokusí se nahrát source code soubor.
     * @param request dotaz obsahující data i metadata o source code souboru.
     * @return výsledek nahrávání
     */
    public ResponseEntity<String> upload(final SourceCodeUploadRequest request) {
        Validate.notNull(request, "The request must not be null.");

        // test na existenci adresáře
        if (!sourceRootHandler.getSourceCodeDir().exists()) {
            try {
                FileUtils.forceMkdir(sourceRootHandler.getSourceCodeDir());
            } catch (IOException e) {
                LOGGER.error("Cannot create directory for the source code files.", e);
                return new ResponseEntity<String>("Error during saving source code file.",
                        HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        // test na neexistenci uploadovaného souboru
        File sourceCodeFile = new File(sourceRootHandler.getSourceCodeDir(), request.getFileId());
        if (sourceCodeFile.exists()) {
            return new ResponseEntity<String>("The source code with id " + request.getFileId() + " already exists.",
                    HttpStatus.BAD_REQUEST);
        }

        // Zapsat soubor na disk.
        byte[] fileData = Base64.decodeBase64(request.getFileData());
        try {
            FileUtils.writeByteArrayToFile(sourceCodeFile, fileData);
        } catch (IOException e) {
            LOGGER.error("IO exception during saving source code file.", e);
            return new ResponseEntity<String>("Error during saving source code file.",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<String>("Source code uploaded ok.", HttpStatus.OK);
    }

    /**
     * @return Source code root handler.
     */
    public SourceRootHandler getSourceRootHandler() {
        return sourceRootHandler;
    }

    /**
     * @param sourceRootHandler Source code root handler.
     */
    public void setSourceRootHandler(SourceRootHandler sourceRootHandler) {
        this.sourceRootHandler = sourceRootHandler;
    }
}
