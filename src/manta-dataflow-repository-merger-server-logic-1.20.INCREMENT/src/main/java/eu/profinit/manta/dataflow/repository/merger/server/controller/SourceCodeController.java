package eu.profinit.manta.dataflow.repository.merger.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeUploadRequest;
import eu.profinit.manta.dataflow.repository.merger.server.helper.sourcecode.SourceCodeHelper;

/**
 * Kontroler pro správu souborů s zdrojovým kódem pro metadata.
 * @author tfechtner
 *
 */
@Controller
public class SourceCodeController {

    @Autowired
    private SourceCodeHelper sourceCodeHelper;

    /**
     * Nahraje nový source code soubor.
     * @param request request obsahující soubor k nahrání včetně metadat
     * @return http message o výsledku zpracování
     */
    @RequestMapping(value = "/api/sourcecode/upload")
    public ResponseEntity<String> uploadSourceCode(@RequestBody SourceCodeUploadRequest request) {
        return sourceCodeHelper.upload(request);
    }
}
