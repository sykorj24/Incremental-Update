package eu.profinit.manta.dataflow.repository.merger.server.helper.merger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.thinkaurelius.titan.util.system.IOUtils;

import au.com.bytecode.opencsv.CSVReader;
import eu.profinit.manta.dataflow.repository.connector.titan.connection.RepositoryRollbackException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.ItemTypes;
import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.SourceCodeFormat;
import eu.profinit.manta.dataflow.repository.core.model.EdgeIdentification;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.MergerProcessor;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.MergerProcessorResult;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.ProcessingResult;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.ProcessorContext;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.SpecificDataHolder;
import eu.profinit.manta.dataflow.repository.utils.CsvHelper;
import eu.profinit.manta.platform.automation.licensing.LicenseException;
import eu.profinit.manta.platform.licensing.LicenseHolder;
import eu.profinit.manta.platform.scriptmetadata.service.ScriptMetadataService;
import eu.profinit.manta.platform.scriptprocessing.model.ScriptMetadataConfig;

/**
 * Procesor pro zpracování merge jednoho grafu do db.
 * @author tfechtner
 *
 */
public class OneFileMerger {
    /** Výchozí hodnota pro počet objektů na commit. */
    private static final int DEFAULT_OBJECTS_PER_COMMIT = 500;
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(OneFileMerger.class);
    /** Používané kódování. */
    private static final String ENCODING = "utf-8";
    /** Zamek kontroly skriptu */
    private static final Object SCRIPT_LOCK = new Object(); 

    /** Počet nových objektů, než dojde ke commitu. */
    private int newObjectsPerCommit = DEFAULT_OBJECTS_PER_COMMIT;
    /** Převodní mapa lokálního id uzlu na titan id odpovídajícího vertexu.*/
    private Map<String, Object> mapNodeIdToDbId;
    /** Převodní mapa lokálního id resource na titan id odpovídajícího vertexu.*/
    private Map<String, Object> mapResourceIdToDbId;
    /** Převodní mapa lokálního id vrstvy na její textovou reprezentaci na vstupu mergeru. */
    private Map<String, String[]> mapLayerIdToLayer;
    /** Převodní mapa lokálního id hrany na titan id odpovídající hrany v db.*/
    private Map<String, EdgeIdentification> mapEdgeIdToDbId;
    /** Převodní mapa lokálního id source code na source code id udržované v titanu.*/
    private Map<String, Object> mapSourceCodeIdToDbId;
    /** Service obstarávající přístup do db.*/
    private final DatabaseHolder databaseService;
    /** Objekt spravující super root databáze.*/
    private final SuperRootHandler superRootHandler;
    /** Objekt spravující revision root databáze.*/
    private final RevisionRootHandler revisionRootHandler;
    /** Objekt spravující source root databáze.*/
    private final SourceRootHandler sourceRootHandler;
    /** Procesor pro obstarani samotneho mergu do db. */
    private final MergerProcessor mergerProcessor;
    /** Sluzba pro praci s metadaty skriptu.  */
    private final ScriptMetadataService scriptMetadataService;
    /** Drzak licence. */
    private LicenseHolder licenseHolder;
    /** True, jestliže je merger paralelní. */
    private final boolean isMergerParallel;
    
    /**
     * @param databaseService service poskytující připojení do db
     * @param superRootHandler handler pro super root databáze
     * @param revisionRootHandler handler pro root revizí
     * @param sourceRootHandler handler pro root source code
     * @param mergerProcessor procesor pro zpracování mergovaných záznamů
     * @param scriptMetadataService 
     * @param newObjectsPerCommit Počet nových objektů, než dojde ke commitu.
     * @param isMergerParallel true, jestli6e je merger paralelní
     */
    public OneFileMerger(DatabaseHolder databaseService, SuperRootHandler superRootHandler,
            RevisionRootHandler revisionRootHandler, SourceRootHandler sourceRootHandler,
            MergerProcessor mergerProcessor, LicenseHolder licenseHolder, ScriptMetadataService scriptMetadataService,
            int newObjectsPerCommit, boolean isMergerParallel) {
        super();
        this.databaseService = databaseService;
        this.superRootHandler = superRootHandler;
        this.revisionRootHandler = revisionRootHandler;
        this.sourceRootHandler = sourceRootHandler;
        this.mergerProcessor = mergerProcessor;
        this.licenseHolder = licenseHolder;
        this.scriptMetadataService = scriptMetadataService;
        this.newObjectsPerCommit = newObjectsPerCommit;
        this.isMergerParallel = isMergerParallel;
    }

    /**
     * Provede zamergování grafu v inputstreamu do db.
     * @param inputStream vstupní stream obsahující linearizovaný graf pro zmergování
     * @param revision revize číslo revize, do které se merguje
     * @return mapa pro view 
     */
    public Map<String, Object> process(InputStream inputStream, Double revision) {
        return process(inputStream, null, revision);
    }

    /**
     * Provede zamergování grafu v inputstreamu do db.
     * @param inputStream vstupní stream obsahující linearizovaný graf pro zmergování
     * @param dataHolder speifická data pro konrkténí request předaná merger procesoru
     * @param revision revize číslo revize, do které se merguje 
     * @return mapa pro view 
     */
    public Map<String, Object> process(InputStream inputStream, SpecificDataHolder dataHolder, Double revision) {
        Map<String, Object> resultMap = new HashMap<String, Object>();

        initWorkspace();

        TransactionLevel level = isMergerParallel ? TransactionLevel.WRITE_SHARE : TransactionLevel.WRITE_EXCLUSIVE;

        ProcessingResult processingResultSummary = new ProcessingResult();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(inputStream, ENCODING));
            CSVReader csvReader = CsvHelper.getCSVReader(reader);
            FootprintCsvReader footprintCsvReader = new FootprintCsvReader(csvReader);
            storeScripts(footprintCsvReader);
            footprintCsvReader.closeFootprint();
            final ProcessLinesTransactionCallback callbackProcessor = new ProcessLinesTransactionCallback(revision,
                    dataHolder, footprintCsvReader);
            ProcessingResult processingResultPart = null;
            do {
                int rollbackNumber = 0;
                try {
                    processingResultPart = databaseService.runInTransaction(level, callbackProcessor);
                } catch (RepositoryRollbackException e) {
                    rollbackNumber++;
                    boolean okProcessed = false;
                    do {
                        try {
                            processingResultPart = databaseService
                                    .runInTransaction(level, new TransactionCallback<ProcessingResult>() {
                                        @Override
                                        public String getModuleName() {
                                            return MergerController.MODULE_NAME;
                                        }

                                        @Override
                                        public ProcessingResult callMe(TitanTransaction transaction) {
                                            return callbackProcessor.reRun(transaction);
                                        }
                                    });
                            okProcessed = true;
                        } catch (RepositoryRollbackException eInner) {
                            rollbackNumber++;
                        }
                    } while (!okProcessed);
                    processingResultPart.saveRollbacks(rollbackNumber);
                }

                if (processingResultPart != null) {
                    processingResultSummary.add(processingResultPart);
                } else {
                    LOGGER.error("Processing result is null.");
                }
            } while (processingResultPart != null && processingResultPart.getProcessedObjects() > 0);
        } catch (IOException e) {
            LOGGER.error("Error during read input stream.", e);
            resultMap.put("error", "Error during read input stream.");
        } finally {
            IOUtils.closeQuietly(reader);
        }

        resultMap.put("processingReport", processingResultSummary);
        return resultMap;
    }

    /**
     * Vlozi otisky sktiptu do lokalni databaze.
     * <p>
     * Pokud je verzovaci system vypnuty, zkontroluje zaroven, zda pocet zpracovanych skriptu bude po merge
     * splnovat licencni podminky. Jestlize ano, oznaci otisky v databazi jako commintute, v opacnem pripade
     * nove otisky z databaze opet smaze a vyhodi licencni vyjimku.
     * </p>
     * <p>
     * Pri zapnutem verzovacim systemu se kontrola licencnich podminek neresi zde, ale az v okamziku commitu verze.
     * </p>
     * @param footprintCsvReader vstupni stream obsahujici linearizovany graf pro zmergovani
     * @throws LicenseException Pokud by po ulozeni skriptu nebyly dodrzeny licencni podminky
     * @throws IOException Pokud pri cteni ze streamu dojde k chybe
     */
    private void storeScripts(FootprintCsvReader footprintCsvReader) throws IOException, LicenseException {
        Set<String> scriptHashes = new HashSet<>();
        String[] itemParameters;
        while ((itemParameters = footprintCsvReader.readNext()) != null) {
            ItemTypes itemType = ItemTypes.parseString(itemParameters[0]);
            if (!ItemTypes.SOURCE_CODE.equals(itemType)) {
                // Skripty jsou v CSV jako prvni => pokud jsme precetli neco jineho,
                // znamena to, ze by uz zadne skripty na vstupu byt nemely (a pokud jsou, pak je to chyba) 
                break;
            }
            if (itemParameters.length != ItemTypes.SOURCE_CODE.getSize()) {
                continue;
            }
            String scHash = itemParameters[SourceCodeFormat.HASH.getIndex()];
            scriptHashes.add(scHash);
        }
        
        if (scriptHashes.isEmpty()) {
            // zadne skripty => neni co kontrolovat
            return;
        }
        
        synchronized (SCRIPT_LOCK) {
            scriptMetadataService.storeScripts(scriptHashes);
            if (!revisionRootHandler.isVersionSystemOn()) {
                int limitWithTolerance = licenseHolder.getLicense().getMaxScriptCount(ScriptMetadataConfig.SCRIPT_COUNT_EXCEED_TOLERANCE);
                int storageResult = scriptMetadataService.commitScripts(limitWithTolerance, ScriptMetadataConfig.LAST_SCRIPT_COUNT_PERIOD, null);
                if (storageResult != 0) {
                    scriptMetadataService.revertUncommittedScripts();
                    int limit = licenseHolder.getLicense().getMaxScriptCount();
                    throw new LicenseException(MessageFormat.format("Scripts cannot be processed because script count ({0}) would exceed the limit {1}.", storageResult, limit));
                }
            }
        }
    }

    /**
     * Inicializuje proměnné používané v tomto běhu merge.
     */
    private void initWorkspace() {
        GraphCreation.initDatabase(databaseService, superRootHandler, revisionRootHandler, sourceRootHandler);
        mapNodeIdToDbId = new HashMap<>();
        mapResourceIdToDbId = new HashMap<>();
        mapLayerIdToLayer = new HashMap<>();
        mapEdgeIdToDbId = new HashMap<>();
        mapSourceCodeIdToDbId = new HashMap<>();
    }

    /**
     * @return Počet nových objektů, než dojde ke commitu. 
     */
    public int getNewObjectsPerCommit() {
        return newObjectsPerCommit;
    }

    /**
     * Zpracuje záznamy z readeru dokud jich není vloženo dostatek pro commit.
     * @author tfechtner
     *
     */
    private final class ProcessLinesTransactionCallback implements TransactionCallback<ProcessingResult> {
        /** Normální merge time pro jeden objekt, po přesažení tohoto limitu dojde k zalogování na info úrovni. */
        private static final long NORMAL_MERGE_TIME = 1000;
        /** Číslo revize. */
        private final Double revision;
        /** Specifická data pro konkrétní request předaná merger procesoru. */
        private final SpecificDataHolder dataHolder;
        /** Reader pro čtení záznamů objektů. */
        private final FootprintCsvReader footprintCsvReader;
        /** Journal, kde se uchovává zpracované záznamy z poslední standardní transakce. */
        private final List<String[]> journal = new ArrayList<>();

        /**
         * @param revision číslo revize
         * @param dataHolder specifická data pro konkrétní request předaná merger procesoru
         * @param footprintCsvReader reader pro čtení záznamů objektů
         */
        private ProcessLinesTransactionCallback(Double revision,
                SpecificDataHolder dataHolder, FootprintCsvReader footprintCsvReader) {
            this.revision = revision;
            this.dataHolder = dataHolder;
            this.footprintCsvReader = footprintCsvReader;
        }

        @Override
        public String getModuleName() {
            return MergerController.MODULE_NAME;
        }

        @Override
        public ProcessingResult callMe(TitanTransaction transaction) {
            ProcessorContext context = new ProcessorContext(transaction, mapNodeIdToDbId, mapResourceIdToDbId,
                    mapLayerIdToLayer, mapEdgeIdToDbId, mapSourceCodeIdToDbId, dataHolder, revision);

            ProcessingResult processingResult = new ProcessingResult();
            journal.clear();
            try {
                String[] itemParameters = null;
                while (processingResult.getProcessedObjects() < newObjectsPerCommit) {
                    // podmínku pro jistotu rozdělíme, aby se nepřeskakovaly objekty
                    itemParameters = footprintCsvReader.readNext();
                    if (itemParameters == null) {
                        break;
                    }
                    if (footprintCsvReader.isFootprintRead()) {
                        ItemTypes itemType = ItemTypes.parseString(itemParameters[0]);
                        if (ItemTypes.SOURCE_CODE.equals(itemType)) {
                            // Pokud jsme na skript mimo stopu, znamena to, ze skript je ve vstupnim
                            // streamu na neocekavane pozici, coz je chyba
                            throw new IllegalStateException("Unexpected source code position.");
                        }
                    }

                    journal.add(itemParameters);

                    long mergeTimeStart = System.currentTimeMillis();
                    List<MergerProcessorResult> mergerResults = mergerProcessor.mergeObject(itemParameters, context);
                    long mergeTime = System.currentTimeMillis() - mergeTimeStart;
                    if (mergeTime > NORMAL_MERGE_TIME) {
                        LOGGER.info("Merge time of element {} was {} ms.", ArrayUtils.toString(itemParameters),
                                mergeTime);
                    } else {
                        LOGGER.debug("Merge time of element {} was {} ms.", ArrayUtils.toString(itemParameters),
                                mergeTime);
                    }
                    for (MergerProcessorResult mergerResult : mergerResults) {
                        if (mergerResult.getItemType() != null) {
                            processingResult.saveResult(mergerResult.getResultType(), mergerResult.getItemType());
                        } else {
                            processingResult.increaseUknownObject();
                        }
                    }
                }
            } catch (IOException e) {
                LOGGER.error("Error during reading input stream.", e);
            }

            processingResult.setRequestedSourceCodes(context.getRequestedSourceCodes());
            return processingResult;
        }

        /**
         * Znovu zpracuje journal.
         * @param transaction transakce pro provedení
         * @return výsledek zpracování
         */
        public ProcessingResult reRun(TitanTransaction transaction) {
            ProcessingResult processingResult = new ProcessingResult();
            ProcessorContext context = new ProcessorContext(transaction, mapNodeIdToDbId, mapResourceIdToDbId,
                    mapLayerIdToLayer, mapEdgeIdToDbId, mapSourceCodeIdToDbId, dataHolder, revision);
            for (String[] itemParameters : journal) {
                List<MergerProcessorResult> mergerResults = mergerProcessor.mergeObject(itemParameters, context);

                for (MergerProcessorResult mergerResult: mergerResults) {
                    if (mergerResult.getItemType() != null) {
                        processingResult.saveResult(mergerResult.getResultType(), mergerResult.getItemType());
                    } else {
                        processingResult.increaseUknownObject();
                    }
                }
            }

            processingResult.setRequestedSourceCodes(context.getRequestedSourceCodes());
            return processingResult;
        }
    }
}
