package eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Objects;
import com.thinkaurelius.titan.core.TitanTransaction;
import com.thinkaurelius.titan.core.TitanVertex;
import com.thinkaurelius.titan.core.TitanVertexQuery;
import com.thinkaurelius.titan.core.attribute.Cmp;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.model.AttributeNames;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphCreation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionUtils;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.GraphFlowAlgorithmExecutor;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.GraphFlowAlgorithmFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.SimpleGraphFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithm;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.model.GraphFlowAlgorithmException;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.FilterResult;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.filter.GraphFlowFilter;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.TransactionAware;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.NodeProperty;
import eu.profinit.manta.dataflow.repository.core.model.EdgeIdentification;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration.InterpolationConfiguration;
import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration.TransformationNodeDef;
import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.transformation.TransformationMapping;

/**
 * Visitor provadejici interpolaci hran.
 * 
 * @author onouza
 *
 */
public class InterpolationVisitor implements GraphVisitor, TransactionAware {
    /** SLF4J logger.*/
    private static final Logger LOGGER = LoggerFactory.getLogger(InterpolationVisitor.class);

    /** Nazev vrstvy, ve ktere dochazi k interpolaci hran */
    private String interpolatedLayer;
    /** Nazev mapovane vrstvy, vuci ktere se interpolace provadi */
    private String mappedLayer;

    /** Rozpracovana revize */
    private double workingRevision;
    /** Interval zahrnujici pouze rozpracovanou revizi */
    private RevisionInterval workingRevisionOnly;
    /** Interval od posledni commitnute verze po rozpracovanou revizi */
    private RevisionInterval sinceLastCommitedRevision;
    
    /** Transakce, v ramci niz probiha interpolace hran */
    private TitanTransaction transaction;
    /** Vysledek interpolace */
    private final InterpolationResult result = new InterpolationResult();
    /** Konfigurae interpolace hran.  */
    private final InterpolationConfiguration configuration;
    /** Drzak na super root */
    private final SuperRootHandler superRootHandler;
    /** Executor pro spusteni dataflow algortimu. */
    private final GraphFlowAlgorithmExecutor flowAlgorithmExecutor;
    

    /**
     * Vytvori novou instanci visitoru s danymi parametry.
     * 
     * @param interpolatedLayer Nazev vrstvy, ve ktere dochazi k interpolaci hran 
     * @param mappedLayer Nazev mapovane vrstvy, vuci ktere se interpolace provadi
     * @param workingRevision Rozpracovana revize
     * @param superRootHandler Drzak na super root
     * @param configuration Konfigurae interpolace hran
     */
    public InterpolationVisitor(String interpolatedLayer, String mappedLayer, double workingRevision, SuperRootHandler superRootHandler,
            InterpolationConfiguration configuration, GraphFlowAlgorithmExecutor flowAlgorithmExecutor) {
        super();
        
        this.interpolatedLayer = interpolatedLayer;
        this.mappedLayer = mappedLayer;
        this.workingRevision = workingRevision;
        this.superRootHandler = superRootHandler;
        this.configuration = configuration;
        this.flowAlgorithmExecutor = flowAlgorithmExecutor;
        this.workingRevisionOnly = new RevisionInterval(workingRevision);
        this.sinceLastCommitedRevision = new RevisionInterval(workingRevision - 1, workingRevision);
    }

    @Override
    public void visitLayer(Vertex layer) {
        // NOOP
    }

    @Override
    public void visitResource(Vertex resource) {
        // NOOP
    }

    @Override
    public void visitNode(Vertex source) {
        interpolate(source);
    }

    @Override
    public void visitAttribute(Vertex attribute) {
        // NOOP
    }

    @Override
    public void visitEdge(Edge edge) {
        // NOOP
    }

    // gettery / settery

    @Override
    public void setTransaction(TitanTransaction transaction) {
        this.transaction = transaction;
    }
    
    protected InterpolationResult getResult() {
        return result;
    }
    
    public String getInterpolatedLayer() {
        return interpolatedLayer;
    }

    // privatni metody

    /**
     * Provede interpolaci hran pro dany vychozi uzel.
     * Vysledkem jsou nove vytvorene hrany vedouci z daneho vychoziho uzlu v interpolovane vrstve,
     * odpovidajici hranam vedoucim z prislusneho uzlu (popripade prislusnych uzlu) v mapovane vrstve. 
     *  
     * @param source Vychozi uzel v interpolovane vrstve. 
     */
    private void interpolate(Vertex source) {
        // Uzly, na ktere se vychozi uzel mapuje
        List<Vertex> mappedNodes = getMappedNodes(source);
        for (Vertex mappedNode : mappedNodes) {
            
            // Mapa vsech koncovych uzlu na jejich transformacni uzly.
            // Klicem jsou vsechny koncove uzly. Jedna se o uzly, do kterych se lze z uzlu mapovaneho startovnim uzlem
            // dostat po hranach typu DIRECT nebo FILTER, a zaroven jsou mapovany alespon jednim uzlem z interpolovane vrstvy
            // Pro kazdy KLIC je hodnota je seznam uzlu, ktere MAPOVANY uzel (PRIMO ci NEPRIMO) transformuji na KLIC.
            Map<Vertex, Set<Vertex>> allTerminalMappedNodes = findTerminalNodes(mappedNode, true);
            
            // Mapa primych koncovych uzlu na jejich prime transformacni uzly.
            // Klicem jsou prime koncove uzly. Jedna se o uzly, do kterych se lze z uzlu mapovaneho startovnim uzlem
            // dostat pouze po hranach typu DIRECT a zaroven jsou mapovany alespon jednim uzlem z interpolovane vrstvy
            // Pro kazdy KLIC je hodnota je seznam uzlu, ktere MAPOVANY uzel PRIMO transformuji na KLIC.
            Map<Vertex, Set<Vertex>> directTerminalMappedNodes = findTerminalNodes(mappedNode, false);
            
            for ( Entry<Vertex, Set<Vertex>> terminalMappedNodeWithTransformations : allTerminalMappedNodes.entrySet()) {
                // Cilove uzly, cili uzly, ktere se mapuji na nektery z koncovych uzlu nalezenych vyse.
                Vertex terminalMappedNode = terminalMappedNodeWithTransformations.getKey();
                List<Vertex> targets = getInterpolatedNodes(terminalMappedNode);
                for (Vertex target : targets) {
                    // Propojime vychozi uzel a nalezeny cilovy uzel interpolovanou hranou (pripadne hranami)
                    // a s ohledem na transformace v MAPOVANE vrstve pripadne rozdelime interpolovane hrany umelymi transformacnimi uzly.
                    Set<Vertex> transformationNodes = terminalMappedNodeWithTransformations.getValue();
                    for (Vertex transformationNode : transformationNodes) {
                        // Projdeme vsechny transformacni uzly mezi protejsky zdroje a cile v MAPOVANE vrstve
                        // Typ hrany zvolime podle toho, zda je cesta mezi protejsky zdroje a cile v MAPOVANE vrstve prima ci nikoliv
                        EdgeLabel edgeLabel = EdgeLabel.FILTER;
                        if (directTerminalMappedNodes.keySet().contains(terminalMappedNode)
                                && directTerminalMappedNodes.get(terminalMappedNode).contains(transformationNode)) {
                            edgeLabel = EdgeLabel.DIRECT;
                        }
                        if (transformationNode != null) {
                            // protejsek zdroje a cile v MAPOVANE vrstve jsou propojeny pres transformacni uzlel
                            // => vytvorime umely transformacni uzel mezi zdrojem a cilem, cili provedeme
                            // (zdroj) -> interpolovana hrana -> umely transformacni uzel -> interpolovana hrana -> (cil) 
                            addInterpolatedEdgeWithTransformation(source, target, transformationNode, edgeLabel);
                        } else {
                            // protejsek zdroje a cile v MAPOVANE vrstve jsou propojeny bez transformacniho uzlu
                            // => propojime zdroj a cil obycejnou hranou
                            addInterpolatedEdge(source, target, edgeLabel);
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Vlozi mezi zdrojovy a cilovy uzel interpolovanou hranu predelenou umelym transformacnim uzlem.
     * Pokud kterykoliv z potencialne vytvarenych prvku jiz existuje, duplicity se nevytvari, ale prepouzivaji.
     *  
     * @param source Zdrojovy uzel.
     * @param target Cilovy uzel.
     * @param transformation Referencni transformace v MAPOVANE vrstve.
     * @param edgeLabel Typ vytvarete hraty.
     */
    private void addInterpolatedEdgeWithTransformation(Vertex source, Vertex target, Vertex transformation, EdgeLabel edgeLabel) {
        
        if (configuration.getTransformationNodes().isEmpty()) {
            // Transformace nejsou nakonfigurovane => nevime jak je vytvaret => spojime uzly obycejnou hranou
            addInterpolatedEdge(source, target, edgeLabel);
            return;
        }
    
        // Zjistime transformacni mapovani a rozdelime je do kategorii

        // Informace o mapovanich transformaci vazanych na cilovy uzel 
        final List<Object> rawTransformationMappings = GraphOperation.getNodeAttribute(target, AttributeNames.NODE_TRANSFORMATION, workingRevisionOnly);
        // Transformacni mapovani, kde nazev MAPOVANEHO uzlu odpovida zpracovavanemu transformacnimu uzlu v MAPOVANE vrstve
        final List<TransformationMapping> matchingMappings = new ArrayList<>();
        // Transformacni mapovani, kde nazev MAPOVANEHO neni uveden
        final List<TransformationMapping> noNameMappings = new ArrayList<>();
        // Pocet preskocenych transformacnich mapovani - jsou to ta mapovani, ktera se urcite netykaji zpracovavaneho uzlu
        int skippedMappings = 0;
        // Defaultni resource transformace, pokud se nenajdou zadna vyhovujici mapovani.
        // V takovem pripade se vezme transformace z prvniho mapovani
        TransformationMapping.Resource defaultTransformationResource = null;
        for (Object rawTransformationMapping : rawTransformationMappings) {
            TransformationMapping transformationMapping = TransformationMapping.of(rawTransformationMapping);
            if (transformationMapping != null) {
                if (defaultTransformationResource == null) {
                    defaultTransformationResource = transformationMapping.getInterpolatedTransformationResource();
                }
                if (nodeMatchesName(transformation, transformationMapping.getMappedTransformation())) {
                    matchingMappings.add(transformationMapping);
                } else if (transformationMapping.getMappedTransformation().isEmpty()) {
                    noNameMappings.add(transformationMapping);
                } else {
                    skippedMappings++;
                }
            }
        }
        
        if (defaultTransformationResource == null) {
            // Pokud neni zadne validni transformacni mapovani, ze ktereho by sel vycist zdroj transformaci, je nekdy chyba  
            LOGGER.warn("No valid mapping containing transformation resource found");
            result.saveErrorTransformationVertex(VertexType.NODE);
            return;
        }
        
        // Z transformacnich mapovani vybereme ta, ktera vysla jako "nejlepsi"
        final List<TransformationMapping> selectedMappings;
        if (!matchingMappings.isEmpty()) {
            // Primarni prednost maji mapovani s presnou shodou nazvu MAPOVANE transformace
            selectedMappings = matchingMappings;
        } else if (noNameMappings.size() == 1 && skippedMappings == 0) {
            // Pokud zadne takove neni, vezmeme mapovani bez specifikovaneho nazvu MAPOVANE transformace,
            // ale pouze pokud je jedine a zadne jine mapovani vazane na cilovy uzel neexistuje
            selectedMappings = noNameMappings;
        } else {
            // V ostatnich pripadech vhodne mapovani neexistuje - vybereme fiktivni "prazdne",
            // abychom snadno vytvorili jeden transformacni uzel 
            selectedMappings = Collections.singletonList(null);
            LOGGER.warn("No matching transformation mapping found => default values of some transformation's attributes (e.g. name, resource) will be used.");
        }
        
        // Overime, zda jsme schopni na zaklade konfigurace zjistit nazvy vsech umelych uzlu v hierarchii transformace 
        for (TransformationMapping transformationMapping : selectedMappings) {
            for (TransformationNodeDef transformationNodeDef : configuration.getTransformationNodes()) {
                String nodeName = transformationNodeDef.getNodeName(source, target, transformation, transformationMapping);
                if (nodeName == null) {
                    // Nepodarilo se ziskat nazev uzlu => transformacni uzel nelze vytvorit
                    result.saveErrorTransformationVertex(VertexType.NODE);
                    return;
                }
            }
        }

        // Pokud ano, pro kazde transformacni mapovani vytvorime hierarchii umeleho transformacniho uzlu
        Vertex targetLayer = GraphOperation.getLayer(target); 
        for (TransformationMapping transformationMapping : selectedMappings) {
            TransformationMapping.Resource transformationResource;
            if (transformationMapping != null) {
                transformationResource = transformationMapping.getInterpolatedTransformationResource();
            } else {
                transformationResource = defaultTransformationResource;
            }
            // Transformacni vrstvu chceme stejnou jako cilovou
            Vertex transformationResourceVertex = createTransformationResouceVertexIfNotExists(transformationResource, targetLayer);
    
            Vertex parent = null;
            Vertex transformationNode = null;
            
            for (TransformationNodeDef transformationNodeDef : configuration.getTransformationNodes()) {
                String nodeName = transformationNodeDef.getNodeName(source, target, transformation, transformationMapping);
                transformationNode = createTransformationNodeIfNotExists(nodeName, transformationNodeDef.getType(), parent, transformationResourceVertex);
                parent = transformationNode;
            }
            
            // Propojime umely transformacni uzel se zdrojem a cilem
            addInterpolatedEdge(source, transformationNode, edgeLabel);
            addInterpolatedEdge(transformationNode, target, edgeLabel);
            
            // a pripadne vyplnime atributy transformacniho uzlu  
            if (transformationMapping != null) {
                for (Map.Entry<String, ?> transformationAttribute : transformationMapping.getTransformationAttributes().entrySet()) {
                    if (transformationAttribute.getValue() != null) {
                        createTransformationAttributeIfNotExists(transformationNode, transformationAttribute.getKey(), transformationAttribute.getValue());
                    }
                }
            }
        }
    }
    
    /**
     * Vytvori resource vrchol trasformace v pozadovane vrstve, pokud zadny se stejnym nazvem a typem neexistuje. 
     * @param resource Udaje o pozadovanem resource
     * @param layer Vrstva, kde ma byt vrchol vytvoren
     * @return Nove vytvoreny resource nebo existujici resource se stejnym nazvem a typem
     */
    private Vertex createTransformationResouceVertexIfNotExists(TransformationMapping.Resource resource, Vertex layer) {
        Vertex root = superRootHandler.getRoot(transaction);

        // Dotaz na deti superkorene od minule revize
        TitanVertexQuery query = ((TitanVertex) root).query()
                .has(EdgeProperty.CHILD_NAME.t(), GraphOperation.processValueForChildName(resource.getName()))
                .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, sinceLastCommitedRevision.getStart())
                .direction(Direction.IN);

        // Zkusime mezi detmi superkorene najit resource
        Iterator<Vertex> vertexIterator = query.vertices().iterator();
        Vertex existingResourceVertex = null;
        while (vertexIterator.hasNext()) {
            Vertex checkedVertex = vertexIterator.next();

            if (checkedVertex.getProperty(NodeProperty.RESOURCE_NAME.t()).equals(resource.getName())
                    && checkedVertex.getProperty(NodeProperty.RESOURCE_TYPE.t()).equals(resource.getType())) {

                // Resource nalezen
                existingResourceVertex = checkedVertex;

                // kontrola, zda nebyl resource pridan v ramci jednoho merge
                if (!RevisionUtils.isVertexInRevisionInterval(checkedVertex, workingRevisionOnly)) {
                    RevisionUtils.setVertexTransactionEnd(checkedVertex, workingRevision);
                }
            }
        }
        
        String layerName = GraphOperation.getName(layer);
        String layerType = GraphOperation.getType(layer);
        if (existingResourceVertex == null) {
            // Pokud neexistuje resource, vytvorime uzel resourcu a propojime jej na vrstvu.
            LOGGER.debug("Resource does not exist => new one will be created.");
            Vertex newLayerVertex = GraphCreation.createLayer(transaction, layerName, layerType);
            Vertex newResourceVertex = GraphCreation.createResource(transaction, root, resource.getName(), resource.getType(),
                    resource.getDescription(), newLayerVertex, workingRevision);
            return newResourceVertex;
        } else {
            // vime ze resource existuje, ale musime zjistit, zda existuje i jeho vrstva
            TitanVertexQuery layerVertexQuery = ((TitanVertex)existingResourceVertex).query()
                    .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, sinceLastCommitedRevision.getStart())
                    .direction(Direction.OUT)
                    .labels(EdgeLabel.IN_LAYER.t());
            
            boolean layerNotExistsYet = true;
            Iterator<Vertex> layerVertexIterator = layerVertexQuery.vertices().iterator();
            
            while (layerVertexIterator.hasNext()) {
                Vertex checkedLayerVertex = layerVertexIterator.next();
                if (checkedLayerVertex.getProperty(NodeProperty.LAYER_NAME.t()).equals(layerName)) {
                    // Nasli jsme uzel vrstvy se stejnym nazvem
                    String checkedLayerVertexType = checkedLayerVertex.getProperty(NodeProperty.LAYER_TYPE.t());
                    
                    if (!checkedLayerVertexType.equals(layerType)) {
                        LOGGER.warn("Layer with name '" + layerName + "' already exists, but the type is different."
                                + " The existing type '" + checkedLayerVertexType + "' is used and the new type '"
                                + layerType + "' is ignored.");
                    }
                    
                    layerNotExistsYet = false;

                    // kontrola, zda nebyla vrstva pridana v ramci jednoho merge
                    if (!RevisionUtils.isVertexInRevisionInterval(checkedLayerVertex, workingRevisionOnly)) {
                        RevisionUtils.setVertexTransactionEnd(checkedLayerVertex, workingRevision);
                    }
                    
                    break;
                }
            }
            if (layerNotExistsYet) {
                // Vrstva neexistuje => vyrobime ji
                LOGGER.debug("Layer does not exist => new one will be created.");
                Vertex layerVertex = GraphCreation.createLayer(transaction, layerName, layerType);
                // a propojime ji s resourcem 
                Edge edge = existingResourceVertex.addEdge(EdgeLabel.IN_LAYER.t(), layerVertex);
                edge.setProperty(EdgeProperty.TRAN_START.t(), workingRevision);
                edge.setProperty(EdgeProperty.TRAN_END.t(), workingRevision);
            } else {
                // Vrstva pro resource jiz existuje => neni co resit
                LOGGER.debug("Layer already eexists => nothing to be done");
            }
            return existingResourceVertex;
        }
    }

    /**
     * Zjisti, zda dany uzel odpovida danemu nazvu.
     *
     * @param node Zkoumany uzel
     * @param name Zkoumany (castecne kvalifikovany) nazev
     * @return {@code true} pokud dany uzel odpovida danemu nazvu, jinak {@code false}
     */
    private boolean nodeMatchesName(Vertex node, List<String> name) {
        ListIterator<String> iterator = name.listIterator(name.size());
        if (!iterator.hasPrevious()) {
            // nazev je prazdny => neni co porovnavat
            return false;
        }
        while (iterator.hasPrevious()) {
            // Porovname uzel s nazvem a pripadne predky uzlu se slozkami kvalifikovaneho nazvu
            String pathEntry = iterator.previous();
            if (node == null || !Objects.equal(GraphOperation.getName(node), pathEntry)) {
                // Narazili jsme na neshodu => uzel neodpovida nazvu
                return false;
            }
            node = GraphOperation.getParent(node);
        }
        // Na zadnou neshodu jsme nenarazili => uzel odpovida nazvu
        return true;
    }

    /**
     * Vytvori umely transformaci uzel, pokud neeexistuje.
     * 
     * @param nodeName Nazev uzlu, ktery chceme vytvorit.
     * @param nodeType Typ uzlu, ktery chceme vytvorit.
     * @param parent Rodic uzlu, ktery chceme vytvorit. Muze byt {@code null}.
     * @param resource Resource uzlu, ktery chceme vytvorit. Muze byt {@code null}.
     * @return Nove vytvoreny uzel nebo existujici uzel s odpovidajicimi parametry.
     */
    private Vertex createTransformationNodeIfNotExists(String nodeName, String nodeType, Vertex parent, Vertex resource) {
        // Rodicovsky uzel a hrana k nemu
        Vertex parentVertex = (parent != null) ? parent : resource;
        EdgeLabel parentEdgeType = (parent != null) ? EdgeLabel.HAS_PARENT : EdgeLabel.HAS_RESOURCE;
        
        // Dotaz na existenci uzlu od minule verze
        TitanVertexQuery query = ((TitanVertex) parentVertex).query()
                .has(EdgeProperty.CHILD_NAME.t(), GraphOperation.processValueForChildName(nodeName))
                .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, sinceLastCommitedRevision.getStart())
                .direction(Direction.IN).labels(parentEdgeType.t());

        Iterator<Vertex> vertexIterator = query.vertices().iterator();
        while (vertexIterator.hasNext()) {
            Vertex checkedVertex = vertexIterator.next();
            // equals je jen přes jméno a typ uzlu a jeho předky, již se nebere jeho přímý resource
            // -> neumožňujume dvojici se stejným jménem a typem pod stejným předkem a pouze s jinými resourcy 
            if (checkedVertex.getProperty(NodeProperty.NODE_NAME.t()).equals(nodeName)
                    && checkedVertex.getProperty(NodeProperty.NODE_TYPE.t()).equals(nodeType)) {

                // kontrola, zda nebyl node pridan v ramci jednoho merge
                if (!RevisionUtils.isVertexInRevisionInterval(checkedVertex, workingRevisionOnly)) {
                    // Pokud ne, roztahneme platnost uzlu do soucasne revize
                    RevisionUtils.setVertexTransactionEnd(checkedVertex, workingRevision);
                }
                // Zapiseme informaci o existujicim uzlu a vratime jej
                result.saveSkippedTransformationVertex(checkedVertex);
                return checkedVertex;
            }
        }
        // Uzel dosud neexistoval => vytvorime novy
        Vertex newVertex = GraphCreation.createNode(transaction, parentVertex, nodeName, nodeType, workingRevision);
        result.saveAddedTransformationVertex(newVertex);
        return newVertex;
    }

    /**
     * Vytvori atribut daneho transformacniho uzlu, pokud atribut daneho jmena s danou hodnotou neexistuje.
     * 
     * @param transformationNode Transformacni uzel, jehoz atribut se ma vytvorit
     * @param attributeName Nazev vytvareneho atributu
     * @param attributeValue Hodnota vytvareneho atributu
     */
    private void createTransformationAttributeIfNotExists(Vertex transformationNode, String attributeName, Object attributeValue) {
        // Zjistime atributy uzlu platne v minule revizi
        Iterator<Vertex> vertexIterator = transformationNode.query()
                .has(EdgeProperty.TRAN_END.t(), Cmp.GREATER_THAN_EQUAL, sinceLastCommitedRevision.getStart())
                .direction(Direction.OUT).labels(EdgeLabel.HAS_ATTRIBUTE.t()).vertices().iterator();

        // Zkusime mezi nimmi najit atribut s hledanym nazvem a hodnotou
        while (vertexIterator.hasNext()) {
            Vertex checkedVertex = vertexIterator.next();
            if (checkedVertex.getProperty(NodeProperty.ATTRIBUTE_NAME.t()).equals(attributeName)
                    && checkedVertex.getProperty(NodeProperty.ATTRIBUTE_VALUE.t()).equals(attributeValue)) {

                // Pokud ho najdeme, zjistime, zda ma platnost i v pacovni revizi
                if (!RevisionUtils.isVertexInRevisionInterval(checkedVertex, workingRevisionOnly)) {
                    // Pokud ne, jednoduse mu platnost prodlouzime
                    RevisionUtils.setVertexTransactionEnd(checkedVertex, workingRevision);
                }
                // Atribut s danym nazvem a hodnotou jiz existuje => neni treba vtvaret novy
                result.saveSkippedTransformationVertex(checkedVertex);
                return;
            }
        }
        // Atribut s danym nazvem a hodnotou dosud neexistuje => vytvorime jej
        Vertex newVertex = GraphCreation.createNodeAttribute(transaction, transformationNode, attributeName, attributeValue, workingRevision);
        result.saveAddedTransformationVertex(newVertex);
    }

    
    /**
     * Vytvori interpolovanou hranu mezi dvema uzly, pokud neexistuje.
     * @param source Zdrojovy uzel
     * @param target Cilovy uzel
     * @param edgeLabel Typ hrany
     */
    private void addInterpolatedEdge(Vertex source, Vertex target, EdgeLabel edgeLabel) {
        EdgeIdentification edgeId = new EdgeIdentification((Long)source.getId(), (Long)target.getId(), edgeLabel);
        Edge edge = GraphOperation.getEdge(transaction, edgeId, sinceLastCommitedRevision);
        if (edge == null) {
            // Pokud hrana jeste neexistuje, vytvorime ji
            edge = GraphCreation.createEdge(source, target, edgeLabel, true, workingRevisionOnly);
            // Zapiseme zaznam o vytvorene hrane
            result.saveInterpolatedEdge(edge);
        } else {
            // Pokud existuje, zjistime, zda nebyla pridana v ramci soucasne revize
            if (!RevisionUtils.isEdgeInRevisionInterval(edge, workingRevisionOnly)) {
                // a pokud ne, roztahneme jeji platnost
                RevisionUtils.setEdgeTransactionEnd(edge, workingRevision);
            }
            // Zapiseme zaznam o existenci hrany
            result.saveSkippedEdge(edge);
        }
    }

    /**
     * Vrati uzly v MAPOVANE vrstve, do kterych vede hrana typu {@link EdgeLabel#MAPS_TO} z daneho uzlu v INTERPOLOVANE vrstve.
     * @param interpolatedNode Vztazny uzel v INTERPOLOVANE vrstve.
     * @return Nalezene uzly v MAPOVANE vrstve vyhovujici vstupnim parametrum.
     */
    private List<Vertex> getMappedNodes(Vertex interpolatedNode) {
        return GraphOperation.getAdjacentVerticesInLayer(interpolatedNode, Direction.OUT, workingRevisionOnly, mappedLayer, EdgeLabel.MAPS_TO);
    }
    
    /**
     * Vrati uzly v INTERPOLOVANE vrstve, ze kterych vede hrana typu {@link EdgeLabel#MAPS_TO} do daneho uzlu v MAPOVANE vrstve.
     * @param mappedNode Vztazny uzel v MAPOVANE vrstve.
     * @return Nalezene uzly v INTERPOLOVANE vrstve vyhovujici vstupnim parametrum.
     */
    private List<Vertex> getInterpolatedNodes(Vertex mappedNode) {
        return GraphOperation.getAdjacentVerticesInLayer(mappedNode, Direction.IN, workingRevisionOnly, interpolatedLayer, EdgeLabel.MAPS_TO);
    }
    
    /**
     * Pro dany startovni uzel najde mapu koncovych uzlu na transformacni uzly.
     * Klici v teto mape jsou koncove uzly, do kterych se lze dostat z daneho startovniho uzlu po hranach urcenych typu.
     * Koncovy uzel je takovy, do nehoz vede alespon jedna hrana typu {@link EdgeLabel#MAPS_TO} z interpolovane vrstvy.
     * Pro kazdy KLIC ve vysledne mape je hodnota seznam uzlu, ktere zdrojovy transformuji na KLIC.
     * Pokud je v tomto seznamu {@code null}, znamena to, ze mezi zdrojovym uzlem a KLICem existuje cesta bez transformacnich uzlu.   
     * 
     * @param startNode Startovni uzel
     * @param edgeLabels {@code true} pokud se maji prochazet hrany typu {@link EdgeLabel#DIRECT} i {@link EdgeLabel#FILTER},
     *          {@code false} pokud pouze hrany typu {@link EdgeLabel#DIRECT}.   
     * @return Mapu koncovych uzlu na transformacni uzly.
     */
    private Map<Vertex, Set<Vertex>> findTerminalNodes(Vertex startNode, boolean withFitlerEdges) {
        // Zjistime ID uzlu, ze kterych se lze dostat do koncovych
        GraphFlowFilter filter = new NodeMappingPathNodesFollowFilter();
        Set<Object> nodeMappingPathNodesIds;
        try {
            nodeMappingPathNodesIds = flowAlgorithmExecutor.findAccessibleVertexIds(
                    Collections.singleton(startNode),
                    transaction,
                    Direction.OUT,
                    withFitlerEdges,
                    Collections.singletonList(filter),
                    workingRevisionOnly);
        } catch (GraphFlowAlgorithmException e) {
            LOGGER.error("Error during finding forward flows.", e);
            return Collections.emptyMap();
        }
        
        // Zjistime ID koncovych uzlu
        filter = new NodeMappingFollowFilter();
        GraphFlowAlgorithmFactoryGeneric<SimpleGraphFlowAlgorithm> endNodeAlgorithmFactory = new GraphFlowAlgorithmFactoryGeneric<>(
                SimpleGraphFlowAlgorithm.class, Collections.singletonList((GraphFlowFilter)filter));
        GraphFlowAlgorithm dataflowAlgorithm = endNodeAlgorithmFactory.createGraphFlowAlgorithm(withFitlerEdges);
        Set<Object> terminalNodesIds;
        try {
            terminalNodesIds = dataflowAlgorithm.findNodesByAlgorithm(
                    Collections.singleton(startNode),
                    nodeMappingPathNodesIds,
                    transaction,
                    Direction.OUT,
                    workingRevisionOnly);
        } catch (GraphFlowAlgorithmException e) {
            LOGGER.error("Error during finding forward flows.", e);
            return Collections.emptyMap();
        }
        
        // a na jejich zaklade vyrobime vysledek
        Map<Vertex, Set<Vertex>> result = new HashMap<>();
        for (Object terminalNodeId : terminalNodesIds) {
            // Pro kazde ID dotahneme koncovy uzel 
            Vertex terminalNode = transaction.getVertex(terminalNodeId);
            if (terminalNode != null) {
                // a k nemu uzly transformacni
                Set<Vertex> transformations = ((NodeMappingFollowFilter)filter).getLastTransformationNodes(terminalNodeId);
                result.put(terminalNode, transformations);
            } else {
                LOGGER.warn(MessageFormat.format("No vertex found for ID {0}", terminalNodeId));
            }
        }
        return result;
    }
    
    // Vnitnri tridy
    /**
     * Filtr pro nalezeni uzlu na ceste k tem,
     * ktere maji alespon protejsek v interpolovane vrstve.
     *  
     * @author onouza
     */
    private class NodeMappingPathNodesFollowFilter implements GraphFlowFilter {
        @Override
        public FilterResult testVertex(Vertex vertex, Edge incomingEdge) {
            if (incomingEdge != null && !getInterpolatedNodes(vertex).isEmpty()) {
                // Uzel ma protejsek v interpolovane vrstve a zaroven se nejedna o startovni 
                // uzel lezici mimo cyklus => Uzel zaradime do vysledku a uz z nej nepokracujeme
                return FilterResult.OK_STOP;
            } else {
                // V opacnem pripade uzel take zaradime, ale pokracujeme z nej dal.
                return FilterResult.OK_CONTINUE;
            }
        }
        
    }
    
    /**
     * Filtr pro nalezeni uzlu, ktere maji alespon protejsek v interpolovane vrstve.
     *  
     * @author onouza
     */
    private class NodeMappingFollowFilter implements GraphFlowFilter {
        /**
         * Klici v teto mape jsou ID koncovych uzlu.
         * Pro kazdy KLIC je hodnota seznam uzlu, ktere jsou poslednimi transformacemi na vsech cestech ke KLICi.
         * Pokud exituje ke KLICi cesta ktere neprochazi pres transformaci, obsahuje seznam null.
         * Pokud KLIC sam je transformaci, obsajuje v seznamu svych transformaci pouze sam sebe. 
         */
        private Map<Object, Set<Vertex>> lastTransformationNodes = new HashMap<>();

        @Override
        public FilterResult testVertex(Vertex vertex, Edge incomingEdge) {
            // Nejprve pro uzel najdeme posledni transformace na ceste zakoncene prichozi hranou
            findLastTransformationNodes(vertex, incomingEdge);
            if (incomingEdge != null && !getInterpolatedNodes(vertex).isEmpty()) {
                // Uzel ma protejsek v interpolovane vrstve a zaroven se nejedna o startovni 
                // uzel lezici mimo cyklus => Uzel zaradime do vysledku a uz z nej nepokracujeme
                return FilterResult.OK_STOP;
            } else {
                // V opacnem pripade uzel preskocime, ale pokracujeme z nej dal.
                return FilterResult.NOK_CONTINUE;
            }
        }
        
        /**
         * Najde pro dany uzel posledni transformace na ceste zakoncene prichozi hranou. 
         * 
         * @param node Zkoumeny uzel.
         * @param incomingEdge Hrana, po ktere jsme do uzlu prisli. Je {@code null} pokud je uzel startovni.
         */
        private void findLastTransformationNodes(Vertex node, Edge incomingEdge) {
            // Zjistime nejblizsi transformacni uzel ve vzestupne hierarchii
            Vertex closestTransformationNode = findClosestTransformationNode(node);
            if (closestTransformationNode != null) {
                // Pokud jsme jej nasli, je zkoumany uzel sam transformaci
                // => je posledni a jedinou transformaci pro sebe sama
                setLastTransformationNode(node.getId(), closestTransformationNode);
            } else {
                // Zkoumany uzel neni transformaci
                if (incomingEdge != null) {
                    // Uzel je vysledkem transformace uzlu, ze ktereho jsme do nej prisli,
                    // spolecne s pripadnymi dosud zjistenymi transformacemi
                    Vertex sourceNode = incomingEdge.getVertex(Direction.OUT);
                    Set<Vertex> sourceTransformations = getLastTransformationNodes(sourceNode.getId());
                    addLastTransformationNodes(node.getId(), sourceTransformations);
                } else {
                    // Pokud je zkoumany uzel startovni, neni vysledkem zadne transformace
                    setLastTransformationNode(node.getId(), null);
                }
            }
        }
        
        /**
         * Vrati posledni transformacni uzly pro uzel s dannym ID.
         * @param nodeId ID zkoumeneho uzlu
         * @return Posledni transformacni uzly pro uzel s dannym ID.
         */
        private Set<Vertex> getLastTransformationNodes(Object nodeId) {
            Set<Vertex> lastTransformations = lastTransformationNodes.get(nodeId);
            if (lastTransformations == null) {
                lastTransformations = new HashSet<>();
                lastTransformationNodes.put(nodeId, lastTransformations);
            }
            return lastTransformations;
        }

        /**
         * Prida k uzlu s danym ID posledni transformacni uzly.
         * @param nodeId ID uzlu, ke kteremu se maji pridat transformacni uzly.
         * @param transformationNodes Transformacni uzly, ktere se maji pridat.
         */
        private void addLastTransformationNodes(Object nodeId, Set<Vertex> transformationNodes) {
            getLastTransformationNodes(nodeId).addAll(transformationNodes);
        }

        /**
         * Nastavi pro uzel s danym ID posledni transformacni uzel.
         * Pokud uz mel uzel nejake transformacni uzly prirazene, budou odebrany. 
         * @param nodeId ID uzlu, kteremu se ma nastavit transformacni uzel.
         * @param transformationNode Transformacni uzel, ktery se ma nastavit.
         */
        private void setLastTransformationNode(Object nodeId, Vertex transformationNode) {
            lastTransformationNodes.put(nodeId, new HashSet<Vertex>());
            getLastTransformationNodes(nodeId).add(transformationNode);
        }

        /**
         * Najde pro dany uzel nejblizsi transformacni uzel ve vzestupne hierarchii.
         * Pokud je dany uzel sam transformacni, bude vracen on.
         * @param node Zkoumany uzel.
         * @return Nejblizsi transformacni uzel ve vzestupne hierarchii nebo {@code null}, pokud se zadny nepodari nalezt.
         */
        private Vertex findClosestTransformationNode(Vertex node) {
            if (isTransformation(node)) {
                return node;
            }
            List<Vertex> ancestors = new ArrayList<>(GraphOperation.getAllParent(node));
            for (Vertex ancesstor : ancestors) {
                if (isTransformation(ancesstor)) {
                    return ancesstor;
                }
            }
            return null;
        }

        /**
         * Zjisti, zda je dany uzel transformacni.
         * @param node Zkoumany uzel.
         * @return {@code true} pokud je dany uzel transformacni, jinak {@code false}
         */
        private boolean isTransformation(Vertex node) {
            String nodeType = GraphOperation.getType(node);
            for (String transformationType : configuration.getTransformations()) {
                if (Objects.equal(transformationType, nodeType)) {
                    return true;
                }
            }
            return false;
        }

    }
    
}
