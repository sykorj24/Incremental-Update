package eu.profinit.manta.dataflow.repository.merger.server.helper;

import java.util.Collections;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SuperRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;

/**
 * Společný předek pro helpery controllerů.
 * @author tfechtner
 *
 */
public class AbstractControllerHelper {

    @Autowired
    private DatabaseHolder databaseService;

    @Autowired
    private SuperRootHandler superRootHandler;

    @Autowired
    private RevisionRootHandler revisionRootHandler;

    @Autowired
    private SourceRootHandler sourceRootHandler;

    /**
     * Vytvoří odpověď pro chybu.
     * @param statusCode kód odpovědi
     * @param error textace chyby
     * @return vytvořená odpověď s danými parametry
     */
    public ResponseEntity<Map<String, Object>> createErrorResponse(HttpStatus statusCode, String error) {
        return new ResponseEntity<Map<String, Object>>(Collections.<String, Object> singletonMap("error", error),
                statusCode);
    }

    /**
     * @return držák na databázi
     */
    public DatabaseHolder getDatabaseService() {
        return databaseService;
    }

    /**
     * @param databaseService držák na databázi
     */
    public void setDatabaseService(DatabaseHolder databaseService) {
        this.databaseService = databaseService;
    }

    /**
     * @return držák na super root
     */
    public SuperRootHandler getSuperRootHandler() {
        return superRootHandler;
    }

    /**
     * @param superRootHandler držák na super root
     */
    public void setSuperRootHandler(SuperRootHandler superRootHandler) {
        this.superRootHandler = superRootHandler;
    }

    /**
     * @return držák na revision root
     */
    public RevisionRootHandler getRevisionRootHandler() {
        return revisionRootHandler;
    }

    /**
     * @param revisionRootHandler držák na revision root
     */
    public void setRevisionRootHandler(RevisionRootHandler revisionRootHandler) {
        this.revisionRootHandler = revisionRootHandler;
    }

    /**
     * @return držák na source root
     */
    public SourceRootHandler getSourceRootHandler() {
        return sourceRootHandler;
    }

    /**
     * @param sourceRootHandler držák na source root
     */
    public void setSourceRootHandler(SourceRootHandler sourceRootHandler) {
        this.sourceRootHandler = sourceRootHandler;
    }
}
