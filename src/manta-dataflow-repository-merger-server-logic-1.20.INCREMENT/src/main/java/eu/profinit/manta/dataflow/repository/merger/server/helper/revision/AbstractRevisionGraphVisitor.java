package eu.profinit.manta.dataflow.repository.merger.server.helper.revision;

import java.util.List;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.connector.titan.service.VertexType;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.GraphVisitor;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseHolder;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;

/**
 * Společný visitor pro revizní operace modifikující graf jako jsou rollback a prune.
 * @author tfechtner
 *
 * @param <R> typ prováděných operací
 */
public abstract class AbstractRevisionGraphVisitor<R extends Enum<?>> implements GraphVisitor {
    /** Interval revizí, které visitor upravuje. */
    private final RevisionInterval revisionInterval;
    /** Třída nesoucí informace o provedených operací. */
    private final ProcessorResult<R> processorResult;

    /**
     * @param revisionInterval Interval revizí, které visitor upravuje.
     * @param processorResult Třída nesoucí informace o provedených operací.
     */
    public AbstractRevisionGraphVisitor(RevisionInterval revisionInterval, ProcessorResult<R> processorResult) {
        super();
        this.revisionInterval = revisionInterval;
        this.processorResult = processorResult;
    }

    /**
     * @return Interval revizí, které visitor upravuje.
     */
    public RevisionInterval getRevisionInterval() {
        return revisionInterval;
    }

    /**
     * @return Třída nesoucí informace o provedených operací.
     */
    public ProcessorResult<R> getProcessorResult() {
        return processorResult;
    }

    /**
     * Vrátí typ operace pro mazání.
     * @return typ operace pro samazání elementu
     */
    protected abstract R deleteOperationType();

    /**
     * Smaže uzel.
     * @param vertex vrchol ke smazání
     */
    protected final void deleteNode(Vertex vertex) {
        vertex.remove();
        processorResult.saveVertexAction(deleteOperationType(), VertexType.NODE);
    }

    /**
     * Smaže vrstvu.
     * @param vertex vrstva ke smazání
     */
    protected final void deleteLayer(Vertex vertex) {
        vertex.remove();
        processorResult.saveVertexAction(deleteOperationType(), VertexType.LAYER);
    }

    /**
     * Smaže resource.
     * @param vertex vrchol ke smazání
     */
    protected final void deleteResource(Vertex vertex) {
        vertex.remove();
        processorResult.saveVertexAction(deleteOperationType(), VertexType.RESOURCE);
    }

    /**
     * Smaže hranu.
     * @param edge vrchol ke smazání
     */
    protected final void deleteEdge(Edge edge) {
        edge.remove();
        processorResult.saveEdgeAction(deleteOperationType(), EdgeLabel.parseFromDbType(edge.getLabel()));
    }

    /**
     * Smaže atribut.
     * @param attr atribut ke smazání
     */
    protected final void deleteAttribute(Vertex attr) {
        attr.remove();
        processorResult.saveVertexAction(deleteOperationType(), VertexType.ATTRIBUTE);
    }

    /**
     * Provede revision operaci na vrcholech reprezentující source code.
     * @param databaseService drák na databázi
     * @param sourceRootHandler držák na source code root
     */
    public final void processSourceCodeVertices(DatabaseHolder databaseService,
            final SourceRootHandler sourceRootHandler) {
        databaseService.runInTransaction(TransactionLevel.WRITE_EXCLUSIVE, new TransactionCallback<Object>() {
            @Override
            public Object callMe(TitanTransaction transaction) {
                Vertex sourceRoot = sourceRootHandler.getRoot(transaction);
                List<Edge> edges = GraphOperation.getAdjacentEdges(sourceRoot, Direction.OUT, revisionInterval,
                        EdgeLabel.HAS_SOURCE);
                for (Edge edge : edges) {
                    processSourceCodeEdge(sourceRootHandler, edge);
                }
                return null;
            }

            @Override
            public String getModuleName() {
                return MergerController.MODULE_NAME;
            }

        });

    }

    /**
     * Smaže source code vrchol.
     * @param sourceRootHandler držák na source code root
     * @param sourceCodeNode vrchol ke smazání
     */
    protected final void deleteSourceCodeNode(SourceRootHandler sourceRootHandler, Vertex sourceCodeNode) {
        sourceRootHandler.removeSourceCodeNode(sourceCodeNode);
        processorResult.saveVertexAction(deleteOperationType(), VertexType.SOURCE_NODE);
    }

    /**
     * Zpracuje hranu příslušně podle revision operace.
     * @param sourceRootHandler držák na source code root
     * @param edge operace ke zpracování
     */
    protected abstract void processSourceCodeEdge(SourceRootHandler sourceRootHandler, Edge edge);
}
