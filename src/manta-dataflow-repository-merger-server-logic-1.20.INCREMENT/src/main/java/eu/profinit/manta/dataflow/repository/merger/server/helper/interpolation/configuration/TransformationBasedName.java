package eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration;

import com.google.common.base.Strings;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.transformation.TransformationMapping;

/**
 * Nazev uzlu zalozeny na transformaci.
 * Jestlize je definovano transformacni mapovani, vrati ve vysledku nazev transformacniho uzlu v INTERPOLOVANE vrstve, pokud je specifikovan.
 * V ostatnich pripadech vrati nazev transformacniho uzlu v MAPOVANE vrstve.
 * 
 * @author onouza
 */
public final class TransformationBasedName implements NodeNameOrigin {
    
    @Override
    public String getNodeName(Vertex source, Vertex target, Vertex transformation, TransformationMapping transformationMapping) {
        String mappedTransformationName = GraphOperation.getName(transformation);
        if (transformationMapping == null || 
                Strings.isNullOrEmpty(transformationMapping.getInterpolatedTransformation())
                || transformationMapping.getInterpolatedTransformation().trim().isEmpty()) {
            return mappedTransformationName;
        }
        return transformationMapping.getInterpolatedTransformation();
    }

}
