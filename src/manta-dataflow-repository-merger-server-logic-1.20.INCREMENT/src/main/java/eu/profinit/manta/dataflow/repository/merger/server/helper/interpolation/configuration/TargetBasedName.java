package eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration;

import org.apache.commons.lang3.Validate;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.transformation.TransformationMapping;

/**
 * Nazvu uzlu zalozeny na cilovem uzlu.
 * Pro cilovy najde ve vzestupne hierarchii nejblizsi uzel pozadovaneho typu
 * a ve vysledku vrati jeho nazev.
 * 
 * @author onouza
 */
public final class TargetBasedName implements NodeNameOrigin  {
    
    /**
     * Typ ciloveho uzlu, ze ktereho bude prebiran nazev.
     */
    private String targetType;

    /**
     * Konstruktor.
     * @param targetType Typ ciloveho uzlu, ze ktereho bude prebiran nazev.
     */
    public TargetBasedName(String targetType) {
        Validate.notNull(targetType, "'targetType' must not be null");
        this.targetType = targetType;
    }

    @Override
    public String getNodeName(Vertex source, Vertex target, Vertex transformation, TransformationMapping transformationMapping) {
        Vertex targetNode = GraphOperation.getClosestNodeByType(target, targetType);
        return targetNode != null ? GraphOperation.getName(targetNode) : null;
    }
}
