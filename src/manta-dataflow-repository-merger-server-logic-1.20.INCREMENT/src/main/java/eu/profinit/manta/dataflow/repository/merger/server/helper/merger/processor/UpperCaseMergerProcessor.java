package eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor;

import java.util.Collections;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.ItemTypes;
import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.NodeFormat;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.ProcessingResult.ResultType;
import eu.profinit.manta.dataflow.repository.utils.CsvHelper;

/**
 * Merger pro uppercasování jmen uzlů z určitých resourců.
 * @author tfechtner
 *
 */
public class UpperCaseMergerProcessor extends StandardMergerProcessor {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(UpperCaseMergerProcessor.class);

    /** Množina typů resourců, jejichž uzly se mají uppercasovat. */
    private Set<String> upperCaseResources = Collections.emptySet();
    
    @Override
    protected ResultType processNode(String[] itemParameters, ProcessorContext context) {
        if (itemParameters.length == ItemTypes.NODE.getSize()) {
            String resourceId = itemParameters[NodeFormat.RESOURCE_ID.getIndex()];
            
            Object resourceVertexId = context.getMapResourceIdToDbId().get(resourceId);
            if (resourceVertexId == null) {
                LOGGER.warn("For node " + StringUtils.join(itemParameters, CsvHelper.DELIMITER)
                        + " does not exist resource");
                return ResultType.ERROR;
            }
            Vertex resourceVertex = context.getDbTransaction().getVertex(resourceVertexId);
            if (resourceVertex == null) {
                LOGGER.error("For node " + StringUtils.join(itemParameters, CsvHelper.DELIMITER)
                        + " is saved incorrect resource " + resourceVertexId.toString() + ".");
                return ResultType.ERROR;
            }
            
            if (upperCaseResources.contains(GraphOperation.getType(resourceVertex))) {
                String name = itemParameters[NodeFormat.NAME.getIndex()];
                if (name != null) {
                    itemParameters[NodeFormat.NAME.getIndex()] = name.toUpperCase();
                }
            }            
            
            return super.processNode(itemParameters, context);
        } else {
            LOGGER.warn("Incorrect node record: " + StringUtils.join(itemParameters, CsvHelper.DELIMITER));
            return ResultType.ERROR;
        }
    }

    /**
     * @return množina typů resourců, jejichž uzly se mají uppercasovat
     */
    public Set<String> getUpperCaseResources() {
        return upperCaseResources;
    }

    /**
     * @param upperCaseResources množina typů resourců, jejichž uzly se mají uppercasovat
     */
    public void setUpperCaseResources(Set<String> upperCaseResources) {
        if (upperCaseResources != null) {
            this.upperCaseResources = upperCaseResources;
        } else {
            this.upperCaseResources = Collections.emptySet();
        }
    }
}
