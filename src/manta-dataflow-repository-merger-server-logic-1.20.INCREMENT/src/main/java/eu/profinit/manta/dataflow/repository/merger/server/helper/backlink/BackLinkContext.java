package eu.profinit.manta.dataflow.repository.merger.server.helper.backlink;

import java.util.HashSet;
import java.util.Set;

import com.tinkerpop.blueprints.Vertex;

/**
 * Třída pro udržování kontextu v rámci hledání zpětných linků.
 * @author tfechtner
 *
 */
public class BackLinkContext {

    /** Id sloupců, pro které byly vygenerovány zpětné linky.*/
    private final Set<Long> processedcolumnIds = new HashSet<Long>();
    
    /**
     * Ověří, jestli už byl daný sloupec v rámci přidávání linků upraven.
     * @param column sloupec k ověření
     * @return true, jestliže byl sloupec upraven
     */
    public synchronized boolean isColumnModified(Vertex column) {
        return processedcolumnIds.contains((Long) column.getId());
    }

    /**
     * Nastaví, že daný sloupec už byl v rámci hledání zpětných linků upraven.
     * @param column sloupec k poznamenání
     */
    public void setColumnAsModified(Vertex column) {
        processedcolumnIds.add((Long) column.getId());        
    }

}
