package eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.thinkaurelius.titan.core.TitanTransaction;

import eu.profinit.manta.dataflow.repository.connector.titan.service.RevisionLockedOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.dataflow.algorithm.GraphFlowAlgorithmExecutor;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.VisitedPart;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.AccessLevel;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.SearchOrderDfs;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactory;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.processor.TraverserProcessorFactoryGeneric;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverser;
import eu.profinit.manta.dataflow.repository.connector.titan.visitor.graph.scalable.traverser.GraphScalableTraverserSerial;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;
import eu.profinit.manta.dataflow.repository.core.model.TransactionCallback;
import eu.profinit.manta.dataflow.repository.core.model.TransactionLevel;
import eu.profinit.manta.dataflow.repository.merger.server.controller.InterpolationController;
import eu.profinit.manta.dataflow.repository.merger.server.controller.MergerController;
import eu.profinit.manta.dataflow.repository.merger.server.helper.AbstractControllerHelper;
import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration.InterpolationConfiguration;
import eu.profinit.manta.platform.usage.model.UsageStatsCollector;
import eu.profinit.manta.platform.web.core.security.SecurityHelper;

/**
 * Pomocnik pro interpolaci hran.
 * 
 * @author onouza
 *
 */
public class InterpolationHelper extends AbstractControllerHelper {

    /** Nazev akce pro usage stats pro interpolaci hran. */
    private static final String FLOW_REPOSITORY_INTERPOLATION = "flow_repository_interpolation";
    /** Konfigurace interpolace hran. */
    private InterpolationConfiguration configuration;
    /** Executor pro spusteni dataflow algortimu. */
    private GraphFlowAlgorithmExecutor flowAlgorithmExecutor;
    
    /**
     * Provede interpolaci hran v dane vrstve a vysledek zabali do HTTP odpovedi.
     * @param interpolatedLayer Nazev vrstvy, ve ktere se ma provest interpolace hran. 
     * @param mappedLayer Nazev vrstvy, vuci ktere se interpolace provadi.
     * @param usageStatsCollector Sberac statistik pouziti
     * @return Vysledek interpolace zabaleny v HTTP odpovedi.
     */
    public ResponseEntity<Object> performInterpolation(final String interpolatedLayer, final String mappedLayer, final UsageStatsCollector usageStatsCollector) {
        return getRevisionRootHandler().executeRevisionWriteOperation(new RevisionLockedOperation<ResponseEntity<Object>>() {

            @Override
            public ResponseEntity<Object> call() {
                Double latestUncommittedRevision = getDatabaseService().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE,
                        new TransactionCallback<Double>() {
                            @Override
                            public String getModuleName() {
                                return InterpolationController.MODULE_NAME;
                            }

                            @Override
                            public Double callMe(TitanTransaction transaction) {
                                return getRevisionRootHandler().getLatestUncommittedRevisionNumber(transaction);
                            }
                        });

                Double latestCommittedRevision = getDatabaseService().runInTransaction(TransactionLevel.WRITE_EXCLUSIVE,
                        new TransactionCallback<Double>() {
                            @Override
                            public String getModuleName() {
                                return InterpolationController.MODULE_NAME;
                            }

                            @Override
                            public Double callMe(TitanTransaction transaction) {
                                return getRevisionRootHandler().getLatestCommittedRevisionNumber(transaction);
                            }
                        });
                
                if(latestUncommittedRevision == null) {
                    if(latestCommittedRevision == null) {
                        return new ResponseEntity<Object>("No revision exists in the database.", 
                                HttpStatus.BAD_REQUEST);
                    } else {
                        return new ResponseEntity<Object>("Contraction can be done only in an uncommited version.", 
                                HttpStatus.BAD_REQUEST);
                    }
                }

                InterpolationResult result = performInterpolationInner(interpolatedLayer, mappedLayer, latestUncommittedRevision);
                usageStatsCollector.saveAction(SecurityHelper.getUserName(), FLOW_REPOSITORY_INTERPOLATION, result);
                return new ResponseEntity<Object>(result, HttpStatus.OK);
            }
        });
    }

    /**
     * Provede interpolaci hran v dane vrstve.
     * @param interpolatedLayer Nazev vrstvy, ve ktere se ma provest interpolace hran. 
     * @param mappedLayer Nazev vrstvy, vuci ktere se interpolace provadi.
     * 
     * @param interpolatedLayer
     * @param mappedLayer
     * @param revision
     * @return Vysledek interpolace
     */
    protected InterpolationResult performInterpolationInner(final String interpolatedLayer, final String mappedLayer, final Double revision) {
        List<VisitedPart> visitedParts = new ArrayList<>();
        visitedParts.add(VisitedPart.SELF);
        visitedParts.add(VisitedPart.SUCCESSORS);

        TraverserProcessorFactory processorFactory = new TraverserProcessorFactoryGeneric<>(
                InterpolationTraverserProcessor.class, InterpolationController.MODULE_NAME,
                TraverserProcessorFactoryGeneric.DEFAULT_OBJECTS_PER_TRAN, new SearchOrderDfs(), visitedParts);

        final GraphScalableTraverser traverser = new GraphScalableTraverserSerial(getDatabaseService(), processorFactory);
        final InterpolationVisitor interpolationVisitor = new InterpolationVisitor(interpolatedLayer, mappedLayer, revision, getSuperRootHandler(), configuration, flowAlgorithmExecutor);
        final Long rootId = getSuperRootHandler().getRoot(getDatabaseService());
        traverser.traverse(interpolationVisitor, rootId, AccessLevel.WRITE, new RevisionInterval(revision, revision));
        return interpolationVisitor.getResult();
    }

    // gettery / settery

    /**
     * @return Konfigurace interpolace hran.
     */
    public InterpolationConfiguration getConfiguration() {
        return configuration;
    }

    /**
     * @param configuration Konfigurace interpolace hran.
     */
    public void setConfiguration(InterpolationConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * @param flowAlgorithmExecutor Executor pro spusteni dataflow algortimu.
     */
    public void setFlowAlgorithmExecutor(GraphFlowAlgorithmExecutor flowAlgorithmExecutor) {
        this.flowAlgorithmExecutor = flowAlgorithmExecutor;
    }
}
