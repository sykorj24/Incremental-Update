package eu.profinit.manta.dataflow.repository.merger.server.security;

/**
 * Seznam rolí specifických pro modul merger.
 * @author tfechtner
 *
 */
public final class MergerRoles {

    private MergerRoles() {
    }

    /**
     * Role mergeru, který upravuje repository.
     */
    public static final String MERGER = "ROLE_MERGER";
}
