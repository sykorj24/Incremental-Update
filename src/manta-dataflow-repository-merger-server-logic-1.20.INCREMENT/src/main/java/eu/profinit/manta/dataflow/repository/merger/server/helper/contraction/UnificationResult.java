package eu.profinit.manta.dataflow.repository.merger.server.helper.contraction;

/**
 * Datová třída pro držení výsledků unifikace.
 * @author tfechtner
 *
 */
public class UnificationResult extends ContractionResult {
    /** Počet unifikovaných objektů. */
    private int unifiedObjects;

    /**
     * Připočte další unifikovaný objekt.
     */
    public void incUnifiedObjects() {
        unifiedObjects++;
    }

    /**
     * @return Počet unifikovaných objektů
     */
    public int getUnifiedObjects() {
        return unifiedObjects;
    }
}
