package eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.configuration;

import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.merger.server.helper.interpolation.transformation.TransformationMapping;

/**
 * Rozhrani puvodu nazvu uzlu.
 * 
 * @author onouza
 */
public interface NodeNameOrigin {
 
    /**
     * Vrati nazev uzlu na zaklade uzlu zdrojoveho, ciloveho a transformacniho.
     * 
     * @param source Zdrojovy uzel
     * @param target Cilovy uzel
     * @param transformation Transformacni uzel (z MAPOVANE vrstvy)
     * @param transformationMapping Transformacni mapovani. Obsahuje informaci o mapovani nove vytvareneho transformacniho uzlu
     *          v INTERPOLOVANE vrstve na odpovidajici existujici transformacni uzel v MAPOVANE vrstve.
     *          Muze byt i {@code null} - v takovem pripade se vlastnosti uzlu vytvareneho v INTERPOLOVANE vrstve preberou z uzlu v MAPOVANE vrstve.
     * @return Nazev uzlu na zaklade uzlu zdrojoveho, ciloveho a transformacniho.
     */
    String getNodeName(Vertex source, Vertex target, Vertex transformation, TransformationMapping transformationMapping);
}
