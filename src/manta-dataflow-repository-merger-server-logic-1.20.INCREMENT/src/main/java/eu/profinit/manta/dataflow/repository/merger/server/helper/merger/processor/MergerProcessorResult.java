package eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor;

import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.ItemTypes;
import eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor.ProcessingResult.ResultType;

/**
 * Výsledek práce merger procesoru.
 * @author tfechtner
 *
 */
public final class MergerProcessorResult {
    
    /** Typ akce provedené s objektem. */
    private final ResultType resultType;
    /** Typ zpracovaného objektu. */
    private final ItemTypes itemType;

    /**
     * @param resultType typ akce provedené s objektem
     * @param itemType typ zpracovaného objektu
     */
    public MergerProcessorResult(ResultType resultType, ItemTypes itemType) {
        super();
        this.resultType = resultType;
        this.itemType = itemType;
    }

    /**
     * @return typ akce provedené s objektem
     */
    public ResultType getResultType() {
        return resultType;
    }

    /**
     * @return typ zpracovaného objektu
     */
    public ItemTypes getItemType() {
        return itemType;
    } 
    
    
}
