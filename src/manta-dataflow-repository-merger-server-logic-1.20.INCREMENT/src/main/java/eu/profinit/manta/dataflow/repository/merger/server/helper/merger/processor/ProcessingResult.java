package eu.profinit.manta.dataflow.repository.merger.server.helper.merger.processor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import eu.profinit.manta.dataflow.repository.core.model.DataflowObjectFormats.ItemTypes;
import eu.profinit.manta.dataflow.repository.merger.model.SourceCodeMapping;

/**
 * Třída udržující přehled o výsledku zpracování.
 * @author tfechtner
 *
 */
public class ProcessingResult {
    /**
     * Výsledek zpracování objektu.
     * @author tfechtner
     *
     */
    public enum ResultType {
        /** Vytvořen nový objekt.*/
        NEW_OBJECT, 
        /** Objekt už existoval. */
        ALREADY_EXIST, 
        /** Chyba při zpracování. */
        ERROR;
    }
    
    /** Počty nově vytvořených objektů, indexovaných {@link ItemTypes}. */
    private int[] newObjects = new int[ItemTypes.values().length];
    /** Počty chybných objektů, indexovaných {@link ItemTypes}. */
    private int[] errorObjects = new int[ItemTypes.values().length];
    /** Počty již existujících zovuvkládaných objektů, indexovaných {@link ItemTypes}. */
    private int[] existedObjects = new int[ItemTypes.values().length];
    /** Počet obdržených neznámých typů.*/
    private int unknownTypes;
    /** Výsledek posledního zpracování. */
    private ResultType lastResultType;
    /** Počet rollbacků v rámci zpracování. */
    private int rollbackNumber;
    /** Největší počet rollbacků v rámci jednoho pokusu. */
    private int longestRollbackChain;
    /** Celkový počet zpracovaných objektů. */
    private int processedObjects;
    /** Množina požadovaných source code. */
    private Set<SourceCodeMapping> requestedSourceCodes = new HashSet<>();

    /**
     * Zvetší počet neznámých objektů.
     */
    public void increaseUknownObject() {
        processedObjects++;
        unknownTypes++;
    }

    /**
     * Uloží výsledek zpracování objektu.
     * @param processResource výsledek zpracování
     * @param itemType typ objektu
     */
    public void saveResult(ResultType processResource, ItemTypes itemType) {
        int index = itemType.ordinal();
        lastResultType = processResource;
        processedObjects++;
        switch (processResource) {
        case ERROR:
            errorObjects[index]++;
            break;
        case NEW_OBJECT:
            newObjects[index]++;
            break;
        case ALREADY_EXIST:
            existedObjects[index]++;
            break;
        default:
            throw new IllegalArgumentException("Unkown type of result type.");
        }
    }

    /**
     * Uloží počet rollbacků z jednoho pokusu.
     * @param newRollbacks počet rollbacků z jednoho pokusu
     */
    public void saveRollbacks(int newRollbacks) {
        rollbackNumber += newRollbacks;
        if (longestRollbackChain < newRollbacks) {
            longestRollbackChain = newRollbacks;
        }
    }

    /**
     * Přičte všechny výsledy z daného výsledku.
     * @param otherResult instance, jejíž výsledek se přičítá
     */
    public void add(ProcessingResult otherResult) {
        unknownTypes += otherResult.getUnknownTypes();
        processedObjects += otherResult.getProcessedObjects();
        lastResultType = otherResult.getLastResultType();
        rollbackNumber += otherResult.getRollbackNumber();

        if (this.longestRollbackChain < otherResult.getLongestRollbackChain()) {
            longestRollbackChain = otherResult.getLongestRollbackChain();
        }

        for (ItemTypes type : ItemTypes.values()) {
            newObjects[type.ordinal()] += otherResult.newObjects[type.ordinal()];
            errorObjects[type.ordinal()] += otherResult.errorObjects[type.ordinal()];
            existedObjects[type.ordinal()] += otherResult.existedObjects[type.ordinal()];
        }

        requestedSourceCodes.addAll(otherResult.requestedSourceCodes);

    }

    /**
     * Převede pole počtů objektů na mapu indexovanou {@link ItemTypes}.
     * @param array pole pro převdení
     * @return mapa s převedenými hodnotami
     */
    private Map<String, Integer> transformArrayToMap(int[] array) {
        Map<String, Integer> resultMap = new HashMap<String, Integer>();
        for (ItemTypes itemType : ItemTypes.values()) {
            resultMap.put(itemType.getText(), array[itemType.ordinal()]);
        }
        return resultMap;
    }

    /**
     * @return Počet rollbacků v rámci zpracování. 
     */
    public int getRollbackNumber() {
        return rollbackNumber;
    }

    /**
     * @return Největší počet rollbacků v rámci jednoho pokusu
     */
    public int getLongestRollbackChain() {
        return longestRollbackChain;
    }

    /** 
     * @return Celkový počet zpracovaných objektů
     */
    public int getProcessedObjects() {
        return processedObjects;
    }

    /**
     * @return Počty nově vytvořených objektů
     */
    public Map<String, Integer> getNewObjects() {
        return transformArrayToMap(newObjects);
    }

    /**
     * @return Počty chybných objektů
     */
    public Map<String, Integer> getErrorObjects() {
        return transformArrayToMap(errorObjects);
    }

    /**
     * @return Počty již existujících zovuvkládaných objektů
     */
    public Map<String, Integer> getExistedObjects() {
        return transformArrayToMap(existedObjects);
    }

    /**
     * @return počet neznámých objektů
     */
    public int getUnknownTypes() {
        return unknownTypes;
    }

    /**
     * Zjistí celkový počet nově přidaných objektů.
     * @return počet všech nových objektů
     */
    public int getNewObjectsCount() {
        int total = 0;
        for (int count : newObjects) {
            total += count;
        }

        return total;
    }

    /**
     * @return Výsledek posledního zpracování. 
     */
    public ResultType getLastResultType() {
        return lastResultType;
    }

    /**
     * @return Množina požadovaných source code
     */
    public Set<SourceCodeMapping> getRequestedSourceCodes() {
        return requestedSourceCodes;
    }

    /**
     * @param requestedSourceCodes Množina požadovaných source code
     */
    public void setRequestedSourceCodes(Set<SourceCodeMapping> requestedSourceCodes) {
        this.requestedSourceCodes = requestedSourceCodes;
    }

    @Override
    public String toString() {
        return "ProcessingResult [newObjects=" + Arrays.toString(newObjects) + ", errorObjects="
                + Arrays.toString(errorObjects) + ", existedObjects=" + Arrays.toString(existedObjects)
                + ", unknownTypes=" + unknownTypes + ", lastResultType=" + lastResultType + "]";
    }
}