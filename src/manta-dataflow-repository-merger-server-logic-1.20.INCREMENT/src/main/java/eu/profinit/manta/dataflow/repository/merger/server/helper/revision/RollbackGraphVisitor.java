package eu.profinit.manta.dataflow.repository.merger.server.helper.revision;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.connector.titan.service.SourceRootHandler;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeLabel;
import eu.profinit.manta.dataflow.repository.core.model.DatabaseStructure.EdgeProperty;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Visitor pro provedení rollback operace.
 * @author tfechtner
 *
 */
public class RollbackGraphVisitor extends AbstractRevisionGraphVisitor<RollbackState> {

    /**
     * @param revisionInterval Interval revizí, které visitor upravuje.
     * @param processorResult Třída nesoucí informace o provedených operací.
     */
    public RollbackGraphVisitor(RevisionInterval revisionInterval, ProcessorResult<RollbackState> processorResult) {
        super(revisionInterval, processorResult);
    }

    @Override
    public void visitLayer(Vertex layer) {
        Edge edge = GraphOperation.getLayerControlEdge(layer);
        RollbackState state = RollbackState.findState(edge, getRevisionInterval());

        switch (state) {
        case NOT_CHANGE:
            // NOOP
            break;
        case ROLLBACK:
            rollbackEdge(edge);
            break;
        case DELETE:
            deleteLayer(layer);
            break;
        default:
            throw new IllegalArgumentException("Unknown rollback state " + state + ".");
        }

    }

    @Override
    public void visitResource(Vertex resource) {
        Edge edge = GraphOperation.getResourceControlEdge(resource);
        RollbackState state = RollbackState.findState(edge, getRevisionInterval());

        switch (state) {
        case NOT_CHANGE:
            // NOOP
            break;
        case ROLLBACK:
            rollbackEdge(edge);
            break;
        case DELETE:
            deleteResource(resource);
            break;
        default:
            throw new IllegalArgumentException("Unknown rollback state " + state + ".");
        }

    }

    @Override
    public void visitNode(Vertex node) {
        Edge edge = GraphOperation.getNodeControlEdge(node);
        RollbackState state = RollbackState.findState(edge, getRevisionInterval());

        switch (state) {
        case NOT_CHANGE:
            // NOOP
            break;
        case ROLLBACK:
            rollbackEdge(edge);
            break;
        case DELETE:
            deleteNode(node);
            break;
        default:
            throw new IllegalArgumentException("Unknown rollback state " + state + ".");
        }

    }

    @Override
    public void visitAttribute(Vertex attribute) {
        Edge edge = GraphOperation.getAttributeControlEdge(attribute);
        RollbackState state = RollbackState.findState(edge, getRevisionInterval());
        switch (state) {
        case NOT_CHANGE:
            // NOOP
            break;
        case ROLLBACK:
            rollbackEdge(edge);
            break;
        case DELETE:
            deleteAttribute(attribute);
            break;
        default:
            throw new IllegalArgumentException("Unknown rollback state " + state + ".");
        }

    }

    @Override
    public void visitEdge(Edge edge) {
        RollbackState state = RollbackState.findState(edge, getRevisionInterval());

        switch (state) {
        case NOT_CHANGE:
            // NOOP
            break;
        case ROLLBACK:
            rollbackEdge(edge);
            break;
        case DELETE:
            deleteEdge(edge);
            break;
        default:
            throw new IllegalArgumentException("Unknown rollback state " + state + ".");
        }

    }

    @Override
    protected RollbackState deleteOperationType() {
        return RollbackState.DELETE;
    }

    @Override
    protected void processSourceCodeEdge(SourceRootHandler sourceRootHandler, Edge edge) {
        RollbackState state = RollbackState.findState(edge, getRevisionInterval());

        switch (state) {
        case NOT_CHANGE:
            // NOOP
            break;
        case ROLLBACK:
            rollbackEdge(edge);
            break;
        case DELETE:
            deleteSourceCodeNode(sourceRootHandler, edge.getVertex(Direction.IN));
            break;
        default:
            throw new IllegalArgumentException("Unknown rollback state " + state + ".");
        }
    }

    /**
     * Zrollbackuje hranu.
     * @param edge hrana ke zpracování
     */
    private void rollbackEdge(Edge edge) {
        edge.setProperty(EdgeProperty.TRAN_END.t(), getRevisionInterval().getStart() - 1);
        getProcessorResult().saveEdgeAction(RollbackState.ROLLBACK, EdgeLabel.parseFromDbType(edge.getLabel()));
    }
}
