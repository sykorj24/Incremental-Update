package eu.profinit.manta.dataflow.repository.merger.server.helper.contraction;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkaurelius.titan.core.TitanTransaction;
import com.tinkerpop.blueprints.Edge;

import eu.profinit.manta.dataflow.repository.connector.titan.service.GraphOperation;
import eu.profinit.manta.dataflow.repository.core.model.EdgeIdentification;
import eu.profinit.manta.dataflow.repository.core.model.RevisionInterval;

/**
 * Dvojice hran ke kontrakci.
 * @author tfechtner
 *
 */
public final class EdgesToContract {
    /** SL4J logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(EdgesToContract.class);
    /** Id mergované hrany, která po mergi zanikne. */
    private final EdgeIdentification sourceEdgeId;
    /** Id originální hrany, na kterou se merguje. */
    private final EdgeIdentification targetEdgeId;
    /** Revizní interval, kde se kontrahuje. */
    private final RevisionInterval revisionInterval;

    /**
     * @param sourceEdgeId Id mergované hrany, která po mergi zanikne
     * @param targetEdgeId Id originální hrany, na kterou se merguje
     * @param revisionInterval Revizní interval, kde se kontrahuje
     */
    public EdgesToContract(EdgeIdentification sourceEdgeId, EdgeIdentification targetEdgeId,
            RevisionInterval revisionInterval) {
        Validate.notNull(sourceEdgeId);
        Validate.notNull(targetEdgeId);
        Validate.notNull(revisionInterval);

        this.sourceEdgeId = sourceEdgeId;
        this.targetEdgeId = targetEdgeId;
        this.revisionInterval = revisionInterval;
    }

    /**
     * Zmerguje danou dvojici.
     * @param transaction transakce pro přístup k db
     */
    public void merge(TitanTransaction transaction, ContractionResult result) {
        Edge sourceEdge = GraphOperation.getEdge(transaction, sourceEdgeId, revisionInterval);
        if (sourceEdge == null) {
            LOGGER.error("o {} {}", sourceEdgeId, revisionInterval);
            return;
        }

        Edge targetEdge = GraphOperation.getEdge(transaction, targetEdgeId, revisionInterval);
        if (targetEdge == null) {
            LOGGER.error("m {} {}", targetEdgeId, revisionInterval);
            return;
        }

        ElementsMerger.mergeEdgeAttributes(sourceEdge, targetEdge);
        result.saveContractedEdge(sourceEdge);
        sourceEdge.remove();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(sourceEdgeId);
        builder.append(sourceEdgeId.getRelationId());
        builder.append(targetEdgeId);
        builder.append(targetEdgeId.getRelationId());
        return builder.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        EdgesToContract other = (EdgesToContract) obj;
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(sourceEdgeId, other.sourceEdgeId);
        builder.append(sourceEdgeId.getRelationId(), other.sourceEdgeId.getRelationId());
        builder.append(targetEdgeId, other.targetEdgeId);
        builder.append(targetEdgeId.getRelationId(), other.targetEdgeId.getRelationId());
        return builder.isEquals();
    }
}
