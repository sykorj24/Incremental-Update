function processLogFile {
    param([string]$logFile)
    # Write-Host 'LogFile' $logFile;

    $pattern = '"proccessingTime":\d+';
    $timeSum = 0; 

    # get time from each process report and sum all these times
    Get-Content $logFile | Select-String -Pattern $pattern | ForEach-Object {
        $time = ($_.Matches[0].Value).split(':')[1];
        # Write-Host "processingTime:" $time;
        $timeSum += $time;
    }
    return $timeSum;
}


$runs = 0;
$logs = '.\case6\medium\bigRemove\logs';
$rev1ProcessTimeSum = 0;
$rev2ProcessTimeSum = 0;

# iterate through all test runs
Get-ChildItem -Path $logs -Directory | ForEach-Object {
    $runs += 1;
    Write-Host 'Run' $runs;

    $revision = 0;
    $runFolder = "$logs" + "\" + "$_";
    # Write-Host "RunFolder" $runFolder;

    $oneRevRun = "$runFolder" + "\rev*";

    # iterate through all revisions (within one test run)
    Get-ChildItem -Path $oneRevRun -Directory | ForEach-Object {
        $revision += 1;
        $revisionFolder = "$_";
        # Write-Host 'Revision' $revision '(' $revisionFolder ')';

        # process each log file in revision folder
        $fullUpdateLog = "oraclePlsqlDataflowMasterScenario_Oracle.properties*.log";
        $incrUpdateAddLog = "oraclePlsqlDataflowIncrementalMasterScenario_Oracle.properties*.log";
        $incrUpdateDeleteLog = "oraclePlsqlDeleteIncrementalMasterScenario_Oracle.properties*.log"
        $DeleteVerticesLog = "deleteVerticesScenario*.log";

        $processTime = 0;
        Write-Host "Revision $revision";

        if (Test-Path -Path "$revisionFolder\*" -Include "$fullUpdateLog") {
            # full update was performed
            $processTime = processLogFile "$revisionFolder\$fullUpdateLog" $revision;
            Write-Host "Full update time: $processTime";
        } else {
            # incremental update was performed
            $addTime = 0;
            $deleteTime = 0;
            if (Test-Path -Path "$revisionFolder\*" -Include "$incrUpdateAddLog") {
                $addTime = processLogFile "$revisionFolder\$incrUpdateAddLog";
                Write-Host "Add time: $addTime";        
            }
            if(Test-Path -Path "$revisionFolder\*" -Include "$incrUpdateDeleteLog") {
                $deleteTime = processLogFile "$revisionFolder\$incrUpdateDeleteLog";    
                Write-Host "Delete time: $deleteTime";
            }
            if(Test-Path -Path "$revisionFolder\*" -Include "$DeleteVerticesLog") {
                $deleteTime = processLogFile "$revisionFolder\$DeleteVerticesLog";
                Write-Host "Delete by path time: $deleteTime";
            }
            $processTime = $addTime + $deleteTime;
        }

        If ($revision -eq 1) {
            $rev1ProcessTimeSum += $processTime;
        } ElseIf ( $revision -eq 2) {
            $rev2ProcessTimeSum += $processTime;
        }
        
    }
    Write-Host;
}

$rev1ProcessTimeAvg = $rev1ProcessTimeSum / $runs;
$rev2ProcessTimeAvg = $rev2ProcessTimeSum / $runs;

Write-Host 'Total runs:' $runs;
Write-Host "Revision 1 average process time:" $rev1ProcessTimeAvg;
Write-Host "Revision 2 average process time:" $rev2ProcessTimeAvg;
