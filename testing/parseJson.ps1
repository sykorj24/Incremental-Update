function Get-ObjectMembers {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$True, ValueFromPipeline=$True)]
        [PSCustomObject]$obj
    )
    $obj | Get-Member -MemberType NoteProperty | ForEach-Object {
        $key = $_.Name
        [PSCustomObject]@{Key = $key; Value = $obj."$key"}
    }
}
$file = "case1\big\inspectRevision1.json";

$json = Get-Content -Path $file | ConvertFrom-Json;
Write-Host "$file";

$json | Get-ObjectMembers | foreach {
    $sum = 0;
    # Write-Host $_.Key;

    $_.Value | Get-ObjectMembers | foreach {
        # Write-Host $_.Key $_.Value;
        $sum += $_.Value;
    }
    Write-Host $_.Key "sum = $sum";
}