﻿$content = "";
$scriptsToDelete = ".\case5\medium\bigRemove\inputRevision2\delete\*";
$fileName = ".\case6\medium\bigRemove\inputRevision2\delete\deleteVertices.txt";

# get names of scripts to be removed 
Get-ChildItem -Path $scriptsToDelete | ForEach-Object {
    $scriptName = $_ | Select-Object -ExpandProperty Name;
    if($scriptName -ne "") {
        $content += '"\"Oracle PL/SQL\"/Oracle/' + "$scriptName" + '"' + "`r`n";
    }
}

# write content to the "deleteVertices.txt" input file
$content | Set-Content "$fileName";